# PRML-21-SPRING

复旦大学2021年春季 <模式识别与机器学习> 课程网站

- 授课老师：邱锡鹏
- 课程助教：郑逸宁
- 评分占比：3次课程作业（共60%），期末项目（35%），出勤考核（5%）

## 日程安排

- 第三周：[作业1](https://gitee.com/fnlp/prml-21-spring/tree/master/assignment-1) 发布
- 第五周：[作业1](https://gitee.com/fnlp/prml-21-spring/tree/master/assignment-1)  截止 (4月1日 周四 23:59)
- 第七周：[作业2](https://gitee.com/fnlp/prml-21-spring/tree/master/assignment-2)  发布
- 第九周：[作业2](https://gitee.com/fnlp/prml-21-spring/tree/master/assignment-2)  截止 (5月2日 周日 23:59)
- 第十三周：[作业3](https://gitee.com/fnlp/prml-21-spring/tree/master/assignment-3)  发布
- 第十五周：[作业3](https://gitee.com/fnlp/prml-21-spring/tree/master/assignment-3)  截止 (6月14日 周一 23:59)

## 作业指南

- 课程作业使用 Python3 进行编程，推荐使用 miniconda 为每个作业创建单独的环境。
- 课程作业报告可以使用中文或英文，在评分上没有区别。
- **每次作业的报告只能使用 markdown 编写，命名为 `README.md`，放置在以你学号命名的文件夹中，具体的文件结构见[提交指南](#提交指南)。**
- Gitee 支持 latex 的公式（由于网站的 bug，你需要用`\\\\`来替换 latex 公式中的 `\\`），助教会在 Gitee 网页上查看作业，看到的效果与你在网页上看到的相同。
- 如果你发现任何关于课程作业的问题，你可以使用 Pull Request  的方式帮助我们进行修改，Pull Request 的标题以`[fix]`开头。
- 请用 [Issue](https://gitee.com/fnlp/prml-21-spring/issues) 的方式进行提问. 适当的问题标签  (比如 `assignment-1`) 可以让其他同学也能看到问题的讨论过程。
- 有其它问题，请先邮件联系助教(ynzheng19@fudan.edu.cn)。

## 提交指南

和往年的课程要求不同，我们默认拔尖班的同学都精通 Git 操作。

- 首先，你需要注册一个 Gitee 账号，并在 Gitee 上 fork 我们的仓库，然后将自己的仓库 clone 到本地编辑。
- 每次作业发布时，你需要将我们的更新拉取到你的仓库，然后在此基础上 commit 你的作业。 拉取的方式主要有两种:
  - 使用 Gitee 提供的强制性同步。如果你有尚未合并到当前仓库的提交，则会被覆盖。
  - 在本地使用命令 `git pull git@gitee.com:fnlp/prml-21-spring.git` 拉取。
- 当你完成作业后，你需要使用 Pull Request 操作将你的作业合并到我们的仓库。这篇[文章](https://gitee.com/help/articles/4128)可能会有帮助。
- Pull Request 的标题按照这种方式命名 `[submission] assignment-1 of {your student ID}`，[这](https://gitee.com/fnlp/prml-21-spring/pulls/1) 是一个 PR 的样例。

每次作业的文件结构与下图类似

```text
.
├── assignment-1/
|   ├── submission/
│       ├── 15307130115/
|           ├── img/
|               └── xxx.png
│           ├── README.md
│           └── source.py
|   └── handout/
|       └── ....
|   └── README.md
```

- 以你学号命名的文件夹(例如 `15307130115` )放置在 `assignment-1/submission/` 文件夹下，里面包含命名为 `README.md` 的课程报告和根据作业要求实现的若干 Python 文件。
- 书写报告所需的图存放在学号文件夹下的 `img` 文件夹内，图片默认使用 `png` 格式，但其它 markdown 支持的图片格式都可以使用。
- 我们提供的课程作业附件，放在 `assignment-1/handout/` 文件夹下，请结合 `assignment-1/` 文件夹下的`README.md` 一起查看。

## 往年课程

- [2020年春季](https://github.com/xuyige/PRML-Spring20-FDU)
- [2019年春季](https://github.com/ichn-hu/PRML-Spring19-Fudan)

> 模式识别与机器学习 / 复旦大学 / 2021年春
