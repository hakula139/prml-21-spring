# Assignment 1: KNN

## Description

KNN(k-Nearest Neighbor) is a simple algorithm in ML. You are required to implement a KNN classifier and do some experiments.

## Submission Files

- README.md: The report of this assignment.
- source.py: The code of this assignment.

For instructions of submission, please also see the [guidelines](https://gitee.com/fnlp/prml-21-spring).

## Details

You are going to implement a `KNN` class like the one in `handout/source.py`. So that other program can import this class to trian model by its method `fit` and predict target by its method `predict`. **You can write data processing and selection of K in the method `fit`**.

```Python
class KNN:

    def __init__(self):
        pass

    def fit(self, train_data, train_label):
        pass

    def predict(self, test_data):
        pass
```

The parameter `train_data` and `train_label` of method `fit` are `numpy.ndarray` . The size of `train_data` is `(N, K)` and the size of `train_label` is `(N,)`. `N` is the number of data and the `K` is the dimension of the data.

The parameter `test_data` of method `predict` is like `train_data`, and the output of method `predict` is like `train_label`. You can use `handout/tester_demo.py` to test your code simply.

### Experiments(80%)

1. You are going to use `np.random.multivariate_normal` to generate serval(e.g. three) point sets sampled from the 2D gaussian distribution, and label each set with a different number.
2. Randomly split the data into 80%-size training set and 20%-size testing set. So that we have `train_data`, `train_label`, `test_data`, and `test_label`.
3. You should use the implemented KNN model to train and test on the generated dataset, and use figure and table to show your result. You may need package `matplotlib`.
4. You can modify your dataset and do more experiments.

The code to experiment should be included in `source.py`. TA will grade your work according to both code and report.

### Automatic Testing(20%)

We will use automatic script to test your `KNN` class in your `source.py`. If the program breaks, you will lost this 20% grade. We will provide severl groups of (`train_data`, `train_label`, `test_data`,  `test_label`) . The accuracy of model will not influence the grade.

The testing envirnment is built by following instructions.

```bash
conda create -n assignment-1 python=3.8 -y
conda activate assignment-1
pip install numpy
pip install matplotlib
```

> Pattern Recognition and Machine Learning / Fudan University / 2021 Spring
