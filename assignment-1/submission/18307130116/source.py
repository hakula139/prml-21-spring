import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def distance(point1, point2, method="Euclid"):
    """
    suppose dimention of points is m * 1
    """
    if point1.ndim == 1:
        point1 = np.expand_dims(point1, axis=1)
    if point2.ndim == 1:
        point2 = np.expand_dims(point2, axis=1)
    if point1.shape[0] == 1:
        point1 = point1.reshape(-1, 1)
    if point2.shape[0] == 1:
        point2 = point2.reshape(-1, 1)
    dimention_num = point1.shape[0]
    result = 0
    if(method == "Euclid"):
        if dimention_num != point1.size:
            print("error")
            return -1
        for iter in range(dimention_num):
            result += (point1[iter, 0]-point2[iter, 0])**2
        return pow(result, 0.5)
    if(method == "Manhattan"):
        if dimention_num != point1.size:
            print("error")
            return -1
        for iter in range(dimention_num):
            result += abs(point1[iter, 0]-point2[iter, 0])
        return result

def dis(dis_label):
    return dis_label[0]

def nearest_k_label_max(point, point_arr, label_arr, k):
    distance_arr = []
    for iter in range(len(point_arr)):
        distance_arr.append((distance(point, point_arr[iter]), label_arr[iter]))
    distance_arr.sort(key=dis)
    result = []
    for iter in range(k):
        result.append(distance_arr[iter][1])
    return max(result, key=result.count)

class KNN:

    def __init__(self):
        pass

    def fit(self, train_data, train_label):
        num = train_data.shape[0]
        dimention_num = train_data.shape[1]
        self.train_data = train_data
        self.train_label = train_label
        dev_num = int(num * 0.1)
        dev_data = train_data[:dev_num]
        dev_label = train_label[:dev_num]
        train_data = train_data[dev_num:]
        train_label = train_label[dev_num:]
        correct_cout_max = 0
        k_max = 0
        accu = []
        if dev_num == 0:
            print("points number too few, so we choose k = 1")
            self.k = 1
            return 
        
        for iter in range(1, min(num-dev_num, 10)):#find the best k
            correct_count = 0
            for j in range(len(dev_data)):
                predict_label = nearest_k_label_max(dev_data[j], train_data, train_label, iter)
                if(predict_label == dev_label[j]):
                    correct_count += 1
            if correct_count > correct_cout_max:
                correct_cout_max = correct_count
                k_max = iter
            accu.append(correct_count/dev_num)
        x = range(1, min(num-dev_num, 10))
        #this part is only for experiment, so I commented it for auto test
        # plt.plot(x,accu)
        # plt.show()
        self.k = k_max
        print("choose k=", k_max)

    def predict(self, test_data):
        result = []
        for iter in range(len(test_data)):
            result.append(nearest_k_label_max(test_data[iter,:], self.train_data, self.train_label, self.k))
        return np.array(result)

#here we need some utils
def data_generate_and_save(class_num, mean_list, cov_list, num_list, save_path = ""):
    """
    class_num: the number of class
    mean_list: mean_list[i] stand for the mean of class[i]
    cov_list: similar to mean_list, stand for the covariance
    num_list: similar to mean_list, stand for the number of points in class[i]
    save_path: the data storage path, end with slash.
    """
    data = np.random.multivariate_normal(mean_list[0], cov_list[0], (num_list[0],))
    label = np.zeros((num_list[0],),dtype=int)
    total = num_list[0]
    
    for iter in range(1, class_num):
        temp = np.random.multivariate_normal(mean_list[iter], cov_list[iter], (num_list[iter],))
        label_temp = np.ones((num_list[iter],),dtype=int)*iter
        data = np.concatenate([data, temp])
        label = np.concatenate([label, label_temp])
        total += num_list[iter]
    
    idx = np.arange(total)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    train_num = int(total * 0.8)
    train_data = data[:train_num, ]
    test_data = data[train_num:, ]
    train_label = label[:train_num, ]
    test_label = label[train_num:, ]
    # print(test_label.size)
    np.save(save_path+"data.npy", ((train_data, train_label), (test_data, test_label)))

def data_load(path = ""):
    (train_data, train_label), (test_data, test_label) = np.load(path+"data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

def visualize(data, label, class_num = 1, test_data=[]):
    data_x = {}
    data_y = {}
    for iter in range(class_num):
        data_x[iter] = []
        data_y[iter] = []
    for iter in range(len(label)):
        data_x[label[iter]].append(data[iter, 0])
        data_y[label[iter]].append(data[iter, 1])
    colors = cm.rainbow(np.linspace(0, 1, class_num))

    for class_idx, c in zip(range(class_num), colors):
        plt.scatter(data_x[class_idx], data_y[class_idx], color=c)
    if(len(test_data) != 0):
        plt.scatter(test_data[:, 0], test_data[:, 1], marker='+')
    plt.show()

#experiment begin
if __name__ == "__main__":
    mean_list = [(1, 4), (2, 3), (2, 3)]
    cov_list = [np.array([[10, 0], [0, 2]]), np.array([[7, 0], [0, 1]]), np.array([[1, 0], [0, 1]])]
    num_list = [200, 200, 200]
    save_path = ""
    data_generate_and_save(3, mean_list, cov_list, num_list, save_path)
    # (train_data, train_label), (test_data, test_label) = data_load()
    # visualize(train_data, train_label, 3)