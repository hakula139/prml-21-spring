
import time
import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as la

np.random.seed(16)


# distance metrics
def euclidean_dist(x, y):
    assert x.shape[0] == y.shape[0], \
        'The dimensions of the input arrays are not equal.'
    return la.norm(x - y)


def manhattan_dist(x, y):
    assert x.shape[0] == y.shape[0], \
        'The dimensions of the input arrays are not equal.'
    return la.norm(x - y, ord=1)


def chebyshev_dist(x, y):
    assert x.shape[0] == y.shape[0], \
        'The dimensions of the input arrays are not equal.'
    return la.norm(x - y, ord=np.inf)


def l_p_dist(x, y, p=2):
    assert x.shape[0] == y.shape[0], \
        'The dimensions of the input arrays are not equal.'
    return la.norm(x - y, ord=p)


def chi_sq_dist(x, y):
    assert x.shape[0] == y.shape[0], \
        'The dimensions of the input arrays are not equal.'
    return np.sum(np.divide(np.square(x - y), x + y))


# distance functions to be utilized
dists = {'euclidean': euclidean_dist,
         'manhattan': manhattan_dist,
         'chebyshev': chebyshev_dist,
         'chi_square': chi_sq_dist}

# the choices of weight
# TODO add more weight method
weights = ['uniform', 'distance']


# split data
def train_test_split(data_, label_, ratio):
    assert data_.shape[0] == label_.shape[0], 'Dimension Error!'

    # the size of training data
    num = data_.shape[0]
    cut = int(num * ratio)

    train_data, test_data = data_[:cut, ], data_[cut:, ]
    train_label, test_label = label_[:cut, ], label_[cut:, ]

    return train_data, train_label, test_data, test_label


class KNN(object):
    def __init__(self, K=3, dist_func='euclidean', weight='uniform'):
        # hyper parameter
        self.K = K

        # data
        self.X = None  # training data
        self.Y = None  # training label

        # parameter
        self.dim = None  # the dimension of data
        self.num_class = None  # number of classes

        # predicting methods
        assert dist_func in dists.keys(), '%s is undefined!' % dist_func
        assert weight in weights, '%s is undefined!' % weight
        self.dict_func = dists[dist_func]
        self.weight = weight

    def label_to_one_hot(self, label):
        """convert a label to an one-hot array.
        :param label: a integer in [0, num_class-1]
        :return: num_class-dimensional array, one-hot
        """
        if self.num_class is None or label not in range(self.num_class):
            raise Exception
        ary = np.zeros(self.num_class)
        ary[label] = 1
        return ary

    def fit(self, train_data, train_label):
        self.X = train_data  # shape of train_data: (num_data, dim_data)
        self.Y = train_label  # shape of train_label: (num_data, )
        self.dim = train_data.shape[1]  # the dimension of data
        # use the number of unique elements in training labels as the number of
        # class
        self.num_class = len(np.unique(train_label))

    def one_predict(self, single_data):
        dist_to_train = np.zeros(self.Y.shape[0])

        # find the index of K nearest neighbors
        for index in range(self.X.shape[0]):
            dist_to_train[index] = self.dict_func(single_data, self.X[index])
            # index of K nearest neighbors
        k_neighbors = np.argsort(dist_to_train)[: self.K]

        # vote
        pred = np.zeros(self.num_class)
        if self.weight == 'uniform':
            for index in k_neighbors:
                pred += self.label_to_one_hot(self.Y[index]) / self.K
        elif self.weight == 'distance':
            for index in k_neighbors:
                pred += self.label_to_one_hot(self.Y[index]) / \
                        dist_to_train[index]
                # to avoid overflow
                pred -= np.mean(pred)

        return np.argmax(pred)

    def predict(self, test_data, k=None):
        # update K
        if k:
            self.K = k

        sz = test_data.shape[0]
        pred = np.zeros(sz)
        for i in range(sz):
            pred[i] = self.one_predict(test_data[i])
        # pred = [self.one_predict(test_datum) for test_datum in test_data]
        # pred = np.array(pred)

        return pred

    def visual_boundary(self):
        """applicable dimension: 2"""

        # edges
        x_min, x_max = int(self.X[:, 0].min()) - 2, int(self.X[:, 0].max()) + 2
        y_min, y_max = int(self.X[:, 1].min()) - 2, int(self.X[:, 1].max()) + 2
        x, y = np.meshgrid(
            np.arange(
                x_min, x_max, 0.2), np.arange(
                y_min, y_max, 0.2))
        dt = np.c_[x.ravel(), y.ravel()]
        lb = self.predict(dt)
        for i in range(dt.shape[0]):
            plt.scatter(dt[i, 0], dt[i, 1], marker='+',
                        color=colors[int(lb[i])], alpha=0.38, s=11)

    def cross_validation(self, data_, label_, k, p=10):
        assert data_.shape[0] == label_.shape[0]
        accuracy = []
        # 每份数据个数
        fold_size = data_.shape[0] // p

        # 单次验证， 共p次
        for fold in range(p):
            begin = fold_size * fold  # 验证/测试集的开始位置
            end = fold_size * (fold + 1)  # 验证/测试集的结束位置

            # 分出测试集
            test_data = data_[begin: end]
            test_label = label_[begin: end]
            train_data = np.vstack([data_[: begin], data_[end:]])
            train_label = np.concatenate([label_[: begin], label_[end:]])

            # 测试
            self.fit(train_data, train_label)
            pred_label = self.predict(test_data, k)
            accuracy.append(self.accuracy(test_label, pred_label))

        return np.array(accuracy).mean(axis=0)

    def k_expr_VC(self, data_, label_, VC=False):
        """
        在不同的K的取值之下测试模型的分类效果，打印结果并作图.
        NOTE: 当VC=False时，不采用交叉验证方式来求分类准确率，且data_与label_即为测试数据集，
              并将储存在self.X与self.Y中的数据作为训练集.
              当VC=True时，采用交叉验证方式来求分类准确率，data_与label_为整体的待分割的样本，
              在程序运行中有分割步骤.data_与label_ 同self.x self.y来自相同分布.
        :param data_: (num, dim)
        :param label_: (num, )
        :param VC: bool
        :return: None
        """
        # acc: list storing accuracy
        acc = []
        train_acc = []

        # size of training data
        if VC:
            N = data_.shape[0] // 5 * 4
        else:
            assert self.X is not None
            N = self.X.shape[0]

        N_over_K = np.arange(1, N)
        Ks = N // N_over_K
        N_over_K = []
        Ks, index = np.unique(Ks, return_index=True)
        for i in range(Ks.shape[0]):
            k = Ks[i]
            N_over_K.append(int(index[i]) + 1)
            train_acc.append(self.accuracy(self.Y, self.predict(self.X, k)))

        for i in range(Ks.shape[0]):
            k = Ks[i]
            # N_over_K.append(int(index[i]) + 1)
            if VC:
                acc.append(self.cross_validation(data_, label_, k))
                # train_acc.append(self.accuracy(self.Y, self.predict(self.X, k)))
            else:
                pred_label = self.predict(data_, k)
                acc.append(self.accuracy(label_, pred_label))
                # train_acc.append(self.accuracy(self.Y, self.predict(self.X, k)))

        plt.plot(N_over_K, acc, color=colors[0], linestyle='-', label='test accuracy')
        plt.plot(N_over_K, train_acc, color=colors[1], linestyle='-', label='train accuracy')
        # plt.xticks(N_over_K)
        plt.title('Accuracy Under Different N/k')
        plt.xlabel('Degrees of freedom -- N/k')
        plt.ylabel('Accuracy')
        plt.ylim(int(min(acc) * 10) / 10, 1)
        plt.legend()
        plt.grid(True)
        print("N=", N)
        for i in range(len(acc)):
            print("N/k=", N_over_K[i], "\tk= ", Ks[i],
                  "\ttest_accuracy=", acc[i], "\ttrain_accuracy=", train_acc[i])

        plt.savefig("k_expr_%d.png" % int(time.time()), dpi=500, bbox_inches='tight')
        plt.show()

    @staticmethod
    def accuracy(test_label, pred_label):
        """calculate the accuracy of prediction on testing data"""
        assert test_label.shape[0] == pred_label.shape[0]
        return np.mean(np.equal(test_label, pred_label))


"""utils for data and tests"""

# some types of distribution
distributions = ['gaussian', 'mixed gaussian']


# generate data
# TODO： finish it
def gen_data(dim=2, num=100, distribution='gaussian'):
    assert distribution in distributions, 'invalid distribution!'


def gen_gaussian(mean_lst, cov_lst, num_lst):
    assert mean_lst.shape[0] == cov_lst.shape[0] == num_lst.shape[0], 'size error!'
    data_ = np.array([])
    label_ = np.array([])

    # generate data/labels
    for i in range(mean_lst.shape[0]):
        d = np.random.multivariate_normal(mean_lst[i], cov_lst[i], num_lst[i])
        lb = np.ones(num_lst[i], dtype=int) * i
        if i == 0:
            data_, label_ = d, lb
        else:
            data_ = np.concatenate([data_, d])
            label_ = np.concatenate([label_, lb])

    # shuffle
    idx = np.arange(data_.shape[0])
    np.random.shuffle(idx)
    data_ = data_[idx]
    label_ = label_[idx]

    return data_, label_


def gen_mixed_gaussian(sample_num, mixture_num, mean, mean_cov, cov):
    """
    Simulate mixed Gaussian distribution.
    模拟(采样)方式：先随机生成 mixture_num 个服从均值为 mean,协方差为 mean_cov
    的数据 , 再分别将它们作为均值, cov作为协方差,1/mixture_num 作为样本来自各个
    Gaussian分布的概率, 随机生成服从Gaussian分布的数据. 最后一步骤重复sample_num次.
    :param sample_num: int, the number of samples to be generated
    :param mixture_num: int, the number of gaussian distribution to be mixed
    :param mean: NdArray, (dim, ), the mean of the mean of the distributions
    :param mean_cov: NdArray, (dim, dim), the cov of the mean
    :param cov: NdArray, (dim, dim), the cov of the each gaussian distribution
    :return: NdArray, (sample_num, dim)
    """
    # mean list, (mixture_num, dim)
    mean_lst = np.random.multivariate_normal(
        mean, mean_cov, (mixture_num,), check_valid='warn')

    samples = []
    for _ in range(sample_num):
        # generate sample from which distribution
        # index in [0, mixture_num-1]
        index = np.random.randint(0, mixture_num, size=1)[0]
        sample = np.random.multivariate_normal(mean_lst[index], cov, 1)
        sample = sample.reshape(sample.shape[1])
        samples.append(sample)
    return np.array(samples)


"""
experiments
"""
# a list of colors that I like and would be used in plt
# NOTE: These colors may not be enough in use.
colors = ['lightskyblue', 'sandybrown', 'mediumpurple', 'olivedrab',
          'gold', 'hotpink']


def scatter(data_, label_, s=20):
    assert data_.shape[0] == label_.shape[0]
    for i in range(data_.shape[0]):
        plt.scatter(data_[i, 0], data_[i, 1], color=colors[int(label_[i])], s=s)


'''basic experiments'''


def basic(mean_lst, cov_lst, num_lst, ratio, VC=False, bound=False):
    # NOTE: 默认数据维数为2

    assert mean_lst.shape[0] == cov_lst.shape[0] == num_lst.shape[0], 'size error!'
    # size of training set
    # N = int(np.sum(num_lst) * ratio)
    # initialization
    train_data = np.array([])
    train_label = np.array([])
    test_data = np.array([])
    test_label = np.array([])

    # generate and split dataset and visualize the dataset
    for i in range(mean_lst.shape[0]):
        mean = mean_lst[i]
        cov = cov_lst[i]
        num = num_lst[i]
        data_ = np.random.multivariate_normal(mean, cov, num)
        label_ = np.ones(num, dtype=int) * i

        # visualization
        plt.scatter(data_[:, 0], data_[:, 1], color=colors[i], s=15)

        train_d, train_l, test_d, test_l = train_test_split(
            data_, label_, ratio)
        if i == 0:
            train_data, train_label, test_data, test_label = train_d, train_l, test_d, test_l
        else:
            train_data = np.concatenate([train_data, train_d])
            train_label = np.concatenate([train_label, train_l])
            test_data = np.concatenate([test_data, test_d])
            test_label = np.concatenate([test_label, test_l])

    data_ = np.concatenate([train_data, test_data])
    label_ = np.concatenate([train_label, test_label])
    # shuffle
    idx = np.arange(data_.shape[0])
    np.random.shuffle(idx)
    data_ = data_[idx]
    label_ = label_[idx]

    # visualize the whole dataset
    plt.title('All the Samples')
    plt.savefig("basic_whole.png", dpi=500, bbox_inches='tight')
    plt.show()

    # classify
    clf = KNN()
    clf.fit(train_data, train_label)

    # draw boundary
    if bound:
        scatter(data_, label_, 15)  # s is alterable
        clf.visual_boundary()

        plt.savefig("basic_boundary_%d" % int(time.time()), dpi=500, bbox_inches='tight')
        plt.show()

    # various K
    if VC:
        clf.k_expr_VC(data_, label_, VC)
    else:
        clf.k_expr_VC(test_data, test_label, VC)


'''mixed gaussian distribution'''


def gen_mixed_gaussian_expr(dim, num_0=200, num_1=200):
    a, b = np.zeros(dim), np.zeros(dim)
    a[0] = 1
    b[1] = 1
    c = np.eye(dim)
    data_ = np.concatenate([gen_mixed_gaussian(num_0, 10, a, c, c / 5),
                            gen_mixed_gaussian(num_1, 10, b, c, c / 5)])
    label_ = np.concatenate([np.zeros(num_0, dtype=int),
                             np.ones(num_1, dtype=int)])
    if data_.shape[0] != label_.shape[0]:
        print("size error!")
        return None
    # shuffle
    idx = np.arange(data_.shape[0])
    np.random.shuffle(idx)
    data_ = data_[idx]
    label_ = label_[idx]
    return data_, label_


'''dimension experiment'''


def show_2D_GM():
    np.random.seed(19)
    data_, label_ = gen_mixed_gaussian_expr(2)
    scatter(data_, label_, 20)
    plt.title("2D Mixed Gaussian Distribution")
    plt.savefig("mixed_gaussian_200_200.png", dpi=500, bbox_inches='tight')
    plt.show()


def GM_expr():
    """探究异常点干扰对模型的影响"""
    np.random.seed(18)
    data_, label_ = gen_mixed_gaussian_expr(2)
    clf = KNN()
    clf.fit(data_, label_)
    # clf.visual_boundary()
    # scatter(data_, label_, 16)
    # plt.title("2D Mixed Gaussian Distribution")
    # plt.savefig("boundary_GM.png", dpi=500, bbox_inches='tight')

    # plt.figure()
    clf.k_expr_VC(data_, label_, True)


# 用于实验的维度
dims = range(2, 50, 3)
# 用于维度实验的K值
K_dim_expr = [3, 9, 21, 45]

"""
def dim_expr():
    np.random.seed(18)
    values = np.zeros((4, len(dims)))
    for i in range(values.shape[1]):
        data_, label_ = gen_mixed_gaussian_expr(dims[i], 50, 50)
        train_data, train_label, test_data, test_label = train_test_split(data_, label_, 0.5)
        clf = KNN()
        clf.fit(train_data, train_label)
        for j in range(values.shape[0]):
            pred_label = clf.predict(test_data, K_dim_expr[j])
            values[j, i] = clf.accuracy(test_label, pred_label)

    # visualize
    for i in range(values.shape[0]):
        plt.plot(dims, values[i], color=colors[i], linestyle='-', label="k=" + str(K_dim_expr[i]))
    plt.legend()
    plt.title("Accuracy Under Different Dimensions")
    # plt.xticks(dims)
    plt.xlabel("Dimension")
    plt.ylabel("Accuracy")
    plt.savefig("dim_expr.png", dpi=500, bbox_inches='tight')
    plt.show()
    return values
"""


def shuffle(data1, data2):
    assert data1.shape[0] == data2.shape[0]
    idx = np.arange(data1.shape[0])
    np.random.shuffle(idx)
    data1 = data1[idx]
    data2 = data2[idx]
    return data1, data2


# 异常点影响
def exception_expr():
    np.random.seed(18)
    data_, label_ = gen_mixed_gaussian_expr(2)
    clf = KNN()

    # singular data
    exc_data = np.random.multivariate_normal(np.array([3, 2]), np.eye(2), 50)
    exc_label = np.random.choice([0, 1], 50)
    data_ = np.concatenate([data_, exc_data])
    label_ = np.concatenate([label_, exc_label])
    # shuffle
    data_, label_ = shuffle(data_, label_)
    clf.fit(data_, label_)

    # visualize
    scatter(data_, label_, 16)
    plt.title("Mixed Gaussian with Singular Data")
    plt.savefig("exception_expr1.png", dpi=500, bbox_inches='tight')
    plt.show()

    # predict
    clf.k_expr_VC(data_, label_, True)


# 距离函数
K_dist_expr = [3, 9, 15, 21]
dist_funcs = ['euclidean', 'manhattan', 'chebyshev', 'chi_square']


def data_num_expr(VC=False):
    np.random.seed(18)
    nums = np.arange(100, 4001, 100)
    acc = []
    for num in nums:
        data_, label_ = gen_mixed_gaussian_expr(2, num, num)
        train_data, train_label, test_data, test_label = train_test_split(data_, label_, 0.8)
        clf = KNN()
        if VC:
            acc.append(clf.cross_validation(data_, label_, 3))
        else:
            clf.fit(train_data, train_label)
            acc.append(clf.accuracy(test_label, clf.predict(test_data)))

    # visualize
    plt.plot(nums, acc, linestyle='-')
    plt.title("Accuracy vs Data Size")
    plt.xlabel("Size of Data Set")
    plt.ylabel("Accuracy")
    plt.savefig("data_num_expr.png", dpi=500, bbox_inches='tight')
    plt.show()


def dist_func_expr(num):
    np.random.seed(18)
    data_, label_ = gen_mixed_gaussian_expr(2, num, num)
    train_data, train_label, test_data, test_label = train_test_split(data_, label_, 0.8)
    acc = np.zeros((4, 3))

    for i in range(3):
        clf = KNN(dist_func=dist_funcs[i])
        clf.fit(train_data, train_label)
        for j in range(4):
            pred = clf.predict(test_data, K_dist_expr[j])
            acc[j, i] = clf.accuracy(test_label, pred)

    # visualize
    for i in range(4):
        plt.plot([1, 2, 3], acc[i], linestyle='-',
                 color=colors[i], label='K=' + str(K_dist_expr[i]))

    plt.title("Accuracy under Different Distance Function")
    plt.xlabel("distance function")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.savefig("dist_func_expr.png", dpi=500, bbox_inches='tight')
    plt.show()
    print(acc)


def num_vs_dist_expr():
    """探究数据集大小与距离函数对模型效果的共同影响"""
    np.random.seed(18)

    # 每类数据的量从100以间隔100增加到2000
    # nums = list(range(100, 2001, 100))
    nums = np.arange(100, 2001, 100)
    for i in range(3):
        clf = KNN(dist_func=dist_funcs[i])
        acc = []
        for num in nums:
            data_, label_ = gen_mixed_gaussian_expr(2, num, num)
            train_data, train_label, test_data, test_label = train_test_split(data_, label_, 0.8)
            clf.fit(train_data, train_label)
            acc.append(clf.accuracy(test_label, clf.predict(test_data)))
        plt.plot(nums, acc, linestyle='-', color=colors[i], label=dist_funcs[i])
        print('acc:', acc)

    plt.xlabel('Size of Data Set')
    plt.ylabel('Accuracy')
    plt.title("Accuracy Under Different Data Size & Distance Function")
    plt.legend()
    plt.savefig("dist_func_expr.png", dpi=500, bbox_inches='tight')
    plt.show()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    np.random.seed(16)
    """
    a = np.array([1, 2])
    b = np.array([[1, 0], [0, 1]])
    c = b / 5
    d = gen_mixed_gaussian(100, 10, a, b, c)
    t = gen_mixed_gaussian(100, 10, a, b, c)
    plt.scatter(d[:, 0], d[:, 1])
    plt.scatter(t[:, 0], t[:, 1], color='orange')
    plt.show()
    """
    # basic experiment

    # 3 classes
    means = np.array([[-3, -1], [0, 3.5], [3, 0]])
    cov_l = np.array([[[2, 0], [0, 2]], [[3, 1], [1, 3]], [[5, 0], [0, 5]]])
    nums = np.array([100, 100, 100])
    basic(means, cov_l, nums, 0.8, VC=False, bound=True)
    basic(means, cov_l, nums, 0.8, VC=True, bound=False)
    # 5 classes
    means = np.array([[-3, -1], [0, 3.5], [3, 0], [0, 0], [0, -2]])
    cov_l = np.array([[[2, 0], [0, 2]], [[3, 1], [1, 3]], [[5, 0], [0, 5]], [[5, 0], [0, 1]], [[2, 0], [0, 2]]])
    nums = np.array([80, 80, 80, 80, 80])
    basic(means, cov_l, nums, 0.8, VC=False, bound=True)
    basic(means, cov_l, nums, 0.8, VC=True, bound=False)

    # 高斯分布差异对模型影响
    means1 = np.array([[-3, -1], [0, 3.5], [3, 0]])
    means2 = means1 / 3
    means3 = means1 * 2
    cov_l1 = np.array([[[2, 0], [0, 2]], [[3, 1], [1, 3]], [[5, 0], [0, 5]]])
    cov_l2 = cov_l1 / 4
    cov_l3 = cov_l1 * 2
    nums = np.array([300, 300, 300])

    """
    a1 = np.array([1, 0])
    a2 = np.array([0, 1])
    b = np.array([[1, 0], [0, 1]])
    c = b / 5
    d = gen_mixed_gaussian(100, 10, a1, b, c)
    t = gen_mixed_gaussian(100, 10, a2, b, c)
    data_ = np.concatenate([d, t])
    label_ = np.concatenate([np.zeros(100), np.ones(100)])

    scatter(data_, label_, 20)
    plt.show()
    """

    # dist_func_expr(600)
    num_vs_dist_expr()

    # 数据集规模的影响
    # data_num_expr()

    # 混合高斯分布
    show_2D_GM()
    GM_expr()
    exception_expr()
