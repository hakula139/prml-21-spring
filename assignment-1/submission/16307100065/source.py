#算法代码
import numpy as np
from collections import Counter
class KNN:

    def __init__(self,k):
        self._k = k
        self._train_data=None
        self._train_label = None
        self._test_data=None
        
    def fit(self,train_data,train_label):
        self._train_data=train_data
        self._train_label=train_label
        
        
    def predict(self,test_data):
        self._test_data=test_data
        predicts_ =[]
        #遍历测试集
        for i in self._test_data:
            #对测试集中的数据求距离每一个训练集中数据的欧氏距离
            distances_ = [np.sum((i-x)**2)**0.5 for x in self._train_data]
            distances = np.array(distances_)
            #用Counter函数求距离前k个
            sorted_distances = np.argsort(distances)
            topK = [self._train_label[j] for j in sorted_distances[0:self._k]]
            votes = Counter(topK)
            #预测结果
            predict = votes.most_common(1)[0][0]
            predicts_.append(predict)
        predicts = np.array(predicts_)
        return predicts
    

