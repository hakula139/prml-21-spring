#实验代码
import numpy as np
from lknn import KNN
import matplotlib.pyplot as plt
#每一维均值为0，方差为10，并且两维独立，创建1000个数据
cov = [[10,0],[0,10]]
data = np.around(np.random.multivariate_normal((0,0),cov,1000),2)
#对应分类随机取0或1
label = np.random.choice([0,1],size=1000,replace=True)
#按8：2的比例分为训练集和测试集
n = len(data)//5
train_data = data[0:4*n]
train_label = label[0:4*n]
test_data = data[4*n:]
test_label = label[4*n:]
#调用KNN类，k赋值5，将训练集输入模型
model = KNN(5)
model.fit(train_data, train_label)
#绘制分类图
#第一维和第二维分别作为x,y轴
x_show = train_data[:,0]
y_show = train_data[:,1]
x_min,x_max=x_show.min(),x_show.max()
y_min,y_max=y_show.min(),y_show.max()
xx,yy = np.meshgrid(np.linspace(x_min,x_max,200),np.linspace(y_min,y_max,200))
#将网格放入模型预测，预测每一个网格的分类
z1 = np.c_[xx.ravel(),yy.ravel()]
z = np.array(z1)
pred = model.predict(z)
pred = pred.reshape(xx.shape)
#绘制网格分类图和训练集的散点图
plt.pcolormesh(xx,yy,pred,cmap=plt.cm.Pastel1)
plt.scatter(x_show,y_show,s=80,c=train_label,cmap=plt.cm.spring,edgecolors='k')
plt.xlim(xx.min(),xx.max())
plt.ylim(yy.min(),yy.max())
plt.show()
#计算acc
res = model.predict(test_data)
print("acc =",np.mean(np.equal(res, test_label)))