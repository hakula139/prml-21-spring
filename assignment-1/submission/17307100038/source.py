import sys
import numpy as np
import matplotlib.pyplot as plt

class KNN:
    def __init__(self):
        self.X = None
        self.y = None
        self.k = None
        self.cate = None  # 距离计算公式
        self.metric = None  # 评分方式，如accuracy
        self.preprocess = None
        self.min = None
        self.max =None
        self.mean = None
        self.std = None

    def distance(self, d1, d2):
        '''计算距离，如欧式距离、曼哈顿距离等'''
        if self.cate == 'euclidean':
            dist = np.sum(np.square(d1 - d2))
        elif self.cate == 'manhattan':
            dist = np.sum(np.abs(d1-d2))
        return dist

    def score(self, y_pred, test_label):
        '''分数评估如accuracy、macro_f1、micro_f1等'''
        if self.metric == 'accuracy':
            cnt = 0
            for i in range(len(y_pred)):
                if y_pred[i] == test_label[i]:
                    cnt += 1
            score = cnt / len(y_pred)
        return score

    def fit(self, X, y,cate = 'euclidean',metric='accuracy',preprocess =None):
        '''包含K值的选择、建立模型'''
        self.cate = cate
        self.metric = metric
        self.preprocess = preprocess

        # 1、preprocessing
        if preprocess == 'Min_Max':   #标准化
            self.min =  X.min(axis = 0)
            self.max = X.max(axis = 0)
            X = (X -self.min)/(self.max - self.min)
        elif preprocess == 'Z_score':    # 归一化
            self.mean = X.mean(axis=0)
            self.std = X.std(axis=0)
            X = (X - self.mean) / self.std
        else:
            X = X

        # 2、打乱顺序
        random_index = np.random.permutation(len(X))
        X = X[random_index]
        y= y[random_index]

        # 3、分为train_data，dev_data
        N = X.shape[0]
        cut = int(N * 0.8)  # 防止非整数情况
        train_data, dev_data = X[:cut, ], X[cut:, ]
        train_label, dev_label = y[:cut, ], y[cut:, ]

        # 4、训练K值
        max_score = 0
        max_score_K = 0
        for k in range(2, 15):
            '''计算每个k下的accuracy：
                1、对每个dev_data，计算其与train_data的距离
                2、排序得到距离最近的k个index
                3、获取该dev_data的y_pred
                4、计算accuracy
                '''
            y_pred = []
            for i in range(len(dev_data)):
                dist_arr = [self.distance(dev_data[i], train_data[j]) for j in range(len(train_data))] # 每个测试点距离训练集各个点的距离列表
                sorted_index = np.argsort(dist_arr)   # arg 排序各个距离的大小，得到index
                first_k_index = sorted_index[:k]      # 最小的k个index
                first_k_label = train_label[first_k_index]
                y_pred.append(np.argmax(np.bincount(first_k_label)))  # 取众数为预测值
            y_pred = np.array(y_pred)
            score = self.score(y_pred, dev_label)

            if score > max_score:
                max_score, max_score_K = score, k

        # 5、确立参数
        self.X = X
        self.y = y
        self.k = max_score_K
        # print('k:%d' % self.k)

    def predict(self, test_data):
        # preprocessing
        if self.preprocess == 'Min_Max':   #标准化
            test_data = (test_data -self.min)/(self.max - self.min)
        elif self.preprocess == 'Z_score':    # 归一化
            test_data = (test_data - self.mean) / self.std
        else:
            test_data = test_data

        y_pred = []
        for i in range(len(test_data)):
            dist_arr = [self.distance(test_data[i], self.X[j]) for j in range(len(self.X))]
            first_k_index = np.argsort(dist_arr)[:self.k]
            first_k_label = self.y[first_k_index]
            y_pred.append(np.argmax(np.bincount(first_k_label)))
        return np.array(y_pred)


def generate():
    mean = (2, 5)
    cov = np.array([[20, 0], [0, 750]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (10, -60)
    cov = np.array([[25, 0], [0, 2500]])
    y = np.random.multivariate_normal(mean, cov, (600,))

    mean = (-5, 72)
    cov = np.array([[10, 0], [0, 650]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    idx = np.arange(2400)
    np.random.shuffle(idx)
    data = np.concatenate([x, y, z])
    label = np.concatenate([
        np.zeros((800,), dtype=int),
        np.ones((600,), dtype=int),
        np.ones((1000,), dtype=int) * 2
    ])
    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:1920, ], data[1920:, ]
    train_label, test_label = label[:1920, ], label[1920:, ]
    np.save("data.npy", (
        (train_data, train_label), (test_data, test_label)
    ))


def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy", allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)


def display(data, label, name):
    datas = [[], [], []]
    for i in range(len(data)):
        datas[label[i]].append(data[i])

    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()


'''测试改变方差对结果的影响'''
def generate_ball(r=1):
    mean = (2, 5)
    cov = np.array([[40, 0], [0, 30]])
    x = np.random.multivariate_normal(mean, cov*r, (800,))

    mean = (20, 16)
    cov = np.array([[25, 0], [0, 35.1]])
    y = np.random.multivariate_normal(mean, cov*r, (600,))

    mean = (-5, 22)
    cov = np.array([[30, 0], [0, 25]])
    z = np.random.multivariate_normal(mean, cov*r, (1000,))

    idx = np.arange(2400)
    np.random.shuffle(idx)
    data = np.concatenate([x, y, z])
    label = np.concatenate([
        np.zeros((800,), dtype=int),
        np.ones((600,), dtype=int),
        np.ones((1000,), dtype=int) * 2
    ])
    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:1920, ], data[1920:, ]
    train_label, test_label = label[:1920, ], label[1920:, ]
    return train_data, train_label, test_data, test_label

def change_cov():
    acc_1 = []
    acc_2 = []
    for each in np.arange(1, 2.1, 0.1):
        train_data, train_label, test_data, test_label = generate_ball(r=each)
        # euclidean
        model = KNN()
        model.fit(train_data, train_label, cate='euclidean', metric='accuracy')
        res = model.predict(test_data)
        acc1 = np.mean(np.equal(res, test_label))
        acc_1.append(acc1)
        # manhattan
        model = KNN()
        model.fit(train_data, train_label, cate='manhattan', metric='accuracy')
        res = model.predict(test_data)
        acc2 = np.mean(np.equal(res, test_label))
        acc_2.append(acc2)
    plt.plot(np.arange(1,2.1,0.1), acc_1,color = 'r')
    plt.plot(np.arange(1,2.1,0.1), acc_2,color = 'b')
    plt.title('accuracy at different cov')
    plt.legend(['euclidean','manhattan'])
    plt.savefig('change_cov.png')


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "g":
        generate()
    if len(sys.argv) > 1 and sys.argv[1] == "d":
        (train_data, train_label), (test_data, test_label) = read()
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        # 选择距离计算公式、评估公式
        model.fit(train_data, train_label, cate='manhattan',metric='accuracy')
        res = model.predict(test_data)
        print("acc =", np.mean(np.equal(res, test_label)))