# 课程报告

## KNN类实现

### fit()函数

fit(X, y,cate = 'euclidean',metric='accuracy',preprocess =None)

X: 训练集

y:训练集标签

cate：距离计算方式，如euclidean、manhattan距离

metric:模型评估方式，如accuracy

preprocess:预处理方式，包含min_max归一化、z_score标准化、不处理



fit函数包含以下功能:

​	1、预处理；

​	2、随机打乱数据集顺序 

​	3、以8:2的比例划分train_data，dev_data,训练选出评估结果最优的k值

### predict()函数

predict用于预测测试集样本

### 辅助函数

distance( d1, d2，cate ='eulidean')

d1,d2表示计算距离的点，cate默认为euclidean距离，可以选择manhattan距离



## 实验1

### Group1：各个类别相差较大，成较为明显的线性位置

$$
\Sigma = 
 \left[
 \begin{matrix}
   52 & 0 \\
   0 & 22 
  \end{matrix}
  \right]
 \Sigma = 
 \left[
 \begin{matrix}
   21.1 & 0 \\
   0 & 32.1 
  \end{matrix}
  \right]
  \Sigma = 
 \left[
 \begin{matrix}
   10 & 0 \\
   0 & 10 
  \end{matrix}
  \right]
$$

$$
\mu =
  \left[
 \begin{matrix}
   2 &5
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   20 & -5
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -5 & 22
  \end{matrix}
  \right]
$$

train_data

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/train_g1.png" alt="test_g1" style="zoom:67%;" />

测试集

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/test_g1.png" style="zoom:67%;" />

测试在两种距离下的准确率如下：

| k    | distance  | acc     |
| ---- | --------- | ------- |
| 8    | euclidean | 96.250% |
| 9    | euclidean | 95.625% |
| 3    | euclidean | 95.833% |
| 13   | euclidean | 96.458% |
| 3    | manhattan | 95.417% |
| 13   | manhattan | 96.250% |
| 5    | manhattan | 95.625% |
| 5    | manhattan | 95.625% |

### Group2：各个类别之间相差较大，成较为明显的分散位置

$$
\Sigma = 
 \left[
 \begin{matrix}
   52 & 0 \\
   0 & 22 
  \end{matrix}
  \right]
 \Sigma = 
 \left[
 \begin{matrix}
   21.1 & 0 \\
   0 & 32.1 
  \end{matrix}
  \right]
  \Sigma = 
 \left[
 \begin{matrix}
   10 & 0 \\
   0 & 10 
  \end{matrix}
  \right]
$$

$$
\mu =
  \left[
 \begin{matrix}
   2 &5
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   20 & 16
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -5 & 22
  \end{matrix}
  \right]
$$

train_data:

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/train_g2.png" alt="train_g2" style="zoom:67%;" />

test_data:

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/test_g2.png" style="zoom:67%;" />

测试在两种距离下的准确率如下：

| k    | distance  | acc     |
| ---- | --------- | ------- |
| 7    | euclidean | 96.875% |
| 7    | euclidean | 96.875% |
| 9    | euclidean | 97.083% |
| 8    | euclidean | 97.083% |
| 12   | manhattan | 97.708% |
| 14   | manhattan | 97.500% |
| 5    | manhattan | 97.083% |
| 12   | manhattan | 97.708% |

*可见不同群之间的几何分布类型对knn的效果影响不明显*

## 实验2

控制均值不变，倍数扩大协方差的各个数值至2倍
$$
\Sigma = 
 \left[
 \begin{matrix}
   52 & 0 \\
   0 & 22 
  \end{matrix}
  \right]
 \Sigma = 
 \left[
 \begin{matrix}
   21.1 & 0 \\
   0 & 32.1 
  \end{matrix}
  \right]
  \Sigma = 
 \left[
 \begin{matrix}
   10 & 0 \\
   0 & 10 
  \end{matrix}
  \right]
$$

$$
\left[
 \begin{matrix}
   2 &5
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   20 & 16
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -5 & 22
  \end{matrix}
  \right]
$$

得到准确率改变如下图：

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/change_cov.png" alt="change_cov" style="zoom:67%;" />

*方差对于KNN的准确率影响显著，随着方差增大，模型准确率下降*

## 实验3

对比采用归一化、标准化前后
$$
\Sigma = 
 \left[
 \begin{matrix}
   20 & 0 \\
   0 & 1250 
  \end{matrix}
  \right]
 \Sigma = 
 \left[
 \begin{matrix}
   25 & 0 \\
   0 & 2500
  \end{matrix}
  \right]
  \Sigma = 
 \left[
 \begin{matrix}
   10 & 0 \\
   0 & 950 
  \end{matrix}
  \right]
$$

$$
\mu=
\left[
 \begin{matrix}
   2 &5
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   10 & -60
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -5 & 72
  \end{matrix}
  \right]
$$

无预处理：

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/data_original.png" alt="data_original" style="zoom:67%;" />

min_max 归一化：

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/data_minmax.png" alt="data_minmax" style="zoom:67%;" />

Z_score标准化：

<img src="https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-1/submission/17307100038/img/data_zscore.png" alt="data_zscore" style="zoom:67%;" />

得到对应的准确率如下：

| preprocessing | accuracy |
| ------------- | -------- |
| None          | 82.917%  |
| min_max       | 83.542%  |
| z_score       | 84.17%   |

通过变小均值和方差的差距，重新实验得到如下结果：
$$
\Sigma = 
 \left[
 \begin{matrix}
   20 & 0 \\
   0 & 750 
  \end{matrix}
  \right]
 \Sigma = 
 \left[
 \begin{matrix}
   25 & 0 \\
   0 & 1200
  \end{matrix}
  \right]
  \Sigma = 
 \left[
 \begin{matrix}
   10 & 0 \\
   0 & 650 
  \end{matrix}
  \right]
$$

$$
\mu=
\left[
 \begin{matrix}
   2 &5
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   10 & -50
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -5 & 55
  \end{matrix}
  \right]
$$

| preprocessing | accuracy |
| ------------- | -------- |
| None          | 90.417%  |
| min_max       | 90.625#  |
| z_score       | 90.833%  |

*标准化、归一化对于KNN模型的准确率有一定提升，数据集各个feature的数量级差别越大，效果越明显*

## 总结

1、KNN模型中不同类别点的几何分布类型对模型预测准确率影响不明显

2、方差对于KNN的准确率影响显著，随着方差增大，模型准确率下降

3、标准化、归一化对于KNN模型的准确率有一定提升，数据集各个feature的数量级差别越大，效果越明显；在数量级相差不大的情况下，性能提升不明显