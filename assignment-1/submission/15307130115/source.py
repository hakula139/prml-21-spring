import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier

class KNN:

    def __init__(self):
        pass

    def fit(self, train_data, train_label):
        N = train_data.shape[0]
        cut = N//5*4

        train_data, dev_data = train_data[:cut,], train_data[cut:,]
        train_label, dev_label = train_label[:cut,], train_label[cut:,]

        max_score = 0
        max_score_K = 0
        for k in range(2,6):
            clf = KNeighborsClassifier(n_neighbors=k)
            clf.fit(train_data, train_label)
            score = clf.score(dev_data, dev_label)
            if score > max_score:
                max_score, max_score_K = score, k

        self.clf = KNeighborsClassifier(n_neighbors=max_score_K)
        self.clf.fit(
            np.concatenate([train_data,dev_data]),
            np.concatenate([train_label, dev_label])
        )

    def predict(self, test_data):
        return self.clf.predict(test_data)


def generate():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))  
    
    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))  

    mean = (10, 22)
    cov = np.array([[10,5],[5,10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))  

    idx = np.arange(2000)
    np.random.shuffle(idx)
    data = np.concatenate([x,y,z])
    label = np.concatenate([
        np.zeros((800,),dtype=int),
        np.ones((200,),dtype=int),
        np.ones((1000,),dtype=int)*2
    ])
    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:1600,], data[1600:,]
    train_label, test_label = label[:1600,], label[1600:,]
    np.save("data.npy",(
        (train_data, train_label), (test_data, test_label)
    ))


def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

def display(data, label, name):
    datas =[[],[],[]]
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    
    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "g":      
        generate()
    if len(sys.argv) > 1 and sys.argv[1] == "d":      
        (train_data, train_label), (test_data, test_label) = read()
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("acc =",np.mean(np.equal(res, test_label)))