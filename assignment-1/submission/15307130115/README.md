# 课程报告

这是一个课程报告的样例，我的代码在 [source.py](./source.py) 中。

我使用了 `sklearn` 中的 `KNeighborsClassifier`，所以我的代码无法通过限定依赖包的自动测试，但我仍可以获得 80% 分数中的大部分。

我以如下参数生成了数据集（由于 Gitee 网站的 bug，你需要用`\\\\`来替换 latex 公式中的 `\\`）

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
73 & 0 \\\\
0 & 22
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
1 & 2
\end{array}\right]
\end{array}
$$

这是我生成的训练集：

![训练集](./img/train.png)

这是我生成的测试集（查看源码了解如何控制图片的大小）

<img src='./img/test.png' width="200" alt="测试集"></img>

我可以通过表格或者图片报告我的实验结果

Algo | Acc  |
-----| ---- |
KNN  | 0.94 |

## 代码使用方法

```bash
python source.py g  # 生成数据集
python source.py d  # 展示数据集
python source.py    # 训练和测试
```
