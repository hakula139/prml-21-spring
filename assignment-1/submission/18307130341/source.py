import numpy as np
import matplotlib.pyplot as plt

class KNN:

    def __init__(self):
        self.data = []
        self.label = []
        self.num = 0
        self.k = 1

    def fit(self, train_data, train_label):
        self.num, _ = train_data.shape

        ratio = 0.8
        idx = np.random.permutation(self.num)
        train_data = train_data[idx]
        train_label = train_label[idx]

        train_set_num = (int)(ratio * self.num)
        train_set_data = train_data[:train_set_num]
        train_set_label = train_label[:train_set_num]
        dev_set_data = train_data[train_set_num:]
        dev_set_label = train_label[train_set_num:]

        self.data = train_set_data
        self.label = train_set_label
        self.num = train_set_num
        max_acc = -1
        max_k = 15
        
        acc_k = []

        for k in range(1,max_k):
            self.k = k
            predict_label = self.predict(dev_set_data)
            acc = np.mean(np.equal(predict_label, dev_set_label))
            acc_k.append(acc)
            if acc >= max_acc + 0.01:
                max_acc = acc
                select_k = k
        
        # Graph_Plot(acc_k, "k_acc")

        self.k = select_k
        self.num , _ = train_data.shape
        self.data = train_data
        self.label = train_label

    def predict(self, test_data):
        predict_label = []
        for x in test_data:
            dis = np.array([np.sqrt(sum((x-y)**2)) for y in self.data])
            knn = np.argsort(np.array(dis))[:self.k]
            result = np.argmax(np.bincount(self.label[knn]))
            predict_label.append(result)
        return predict_label

def data_generate(num):
    mean = [(6,4), (11, 14), (22, 6)]
    cov = [[35, 4], [4, 11]],[[21, 6], [6, 24]],[[25, 5], [5, 10]]
    data0 = np.random.multivariate_normal(mean[0], cov[0], num)
    data1 = np.random.multivariate_normal(mean[1], cov[1], num)
    data2 = np.random.multivariate_normal(mean[2], cov[2], num)
    data = np.concatenate([data0,data1,data2])
    label = np.array([0]*num + [1]*num + [2]*num)
    
    idx = np.random.permutation(3*num)

    data = data[idx]
    label = label[idx]

    return data, label

def Graph_Plot(acc, name):
    plt.plot(acc)
    plt.xlabel('k')
    plt.ylabel('acc')
    plt.savefig(f'img/{name}')
    plt.close()

def Graph_Scatter(data, label, name):
    points =[[],[],[]]
    for i in range(len(data)):
        points[label[i]].append(data[i])
    for points_set in points:
        points_set = np.array(points_set)
        plt.scatter(points_set[:, 0], points_set[:, 1])
    # plt.show()
    plt.savefig(f'img/{name}')
    plt.close()
    

if __name__ == "__main__":
    num = 400
    data, label = data_generate(num)
    train_num = (int)(num* 3 * 0.8)
    train_data = data[:train_num]
    train_label = label[:train_num]
    test_data = data[train_num:]
    test_label = label[train_num:]

    # Graph_Scatter(train_data, train_label, "train_data")
    # Graph_Scatter(test_data, test_label,"test_data")

    model = KNN()
    model.fit(train_data, train_label)
    test_predict = model.predict(test_data)
    acc = np.mean(np.equal(test_predict, test_label))
    print("k = ", model.k)
    print("acc = ", acc)

