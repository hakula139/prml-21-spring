

[TOC]

我只使用了numpy和matplotlib两个库实现了KNN的，所以代码应该可以完成在额外的任务。

#### 一、模型的实现

对于单个测试数据的knn分类网络的实现：

```python
inX=test_data[i]
#计算欧氏距离
diffMat=np.tile(inX,(dataSetSize,1))-tra_data
sqDifMat=diffMat**2
sqDistances=sqDifMat.sum(axis=1)
distances=sqDistances**0.5
#对计算得到的距离对应的序号进行排序
sortedDistIndicies=distances.argsort()
#统计最近的k个点中各个类别的数量
classCount={}
for j in range(k):
	Label = tra_label[sortedDistIndicies[j]]
	classCount[Label] = classCount.get(Label,0) + 1
#得到对于类别的预测
max_count=0
for key,value in classCount.items():
if value >max_count :
	max_count = value
	pred_label=key
```

对与KNN fit() 部分的实现：

将数据集按照8:2的比例进行划分后，对K进行拟合，多次重复在训练集上进行单次的knn分类预测，选出正确率最高的K值。

~~~python
def fit(self, train_data, train_label):
        self.train_data=train_data
        self.train_label=train_label
        N=train_data.shape[0]
        cut=int(N*0.8)

        tra_data, test_data = train_data[:cut,], train_data[cut:,]
        tra_label, test_label = train_label[:cut,], train_label[cut:,]

        dataSetSize=tra_data.shape[0]
        test_number=test_data.shape[0]

        best_k=0
        max_score=0
        if N<6 :
            k_range=N
        else :
            k_range=20 

        for k in range(2,k_range):
            total_correct=0
            for i in range(0,test_number):
                inX=test_data[i]
                #计算欧氏距离
                diffMat=np.tile(inX,(dataSetSize,1))-tra_data
                sqDifMat=diffMat**2
                sqDistances=sqDifMat.sum(axis=1)
                distances=sqDistances**0.5
                #对计算得到的距离对应的序号进行排序
                sortedDistIndicies=distances.argsort()
                #统计最近的k个点中各个类别的数量
                classCount={}
                for j in range(k):
                    Label = tra_label[sortedDistIndicies[j]]
                    classCount[Label] = classCount.get(Label,0) + 1
                #得到对于类别的预测
                max_count=0
                for key,value in classCount.items():
                    if value >max_count :
                        max_count = value
                        pred_label=key
                
                if pred_label ==test_label[i]:
                    total_correct=total_correct+1
			#选择正确率最高的K值
            score=total_correct*1.0/test_number
            if score>max_score:
                max_score=score
                best_k=k
        print("Best K: %d"%(best_k))
        self.k=best_k
~~~

#### 二、实验数据的生成

通过 np.random.multivariate_normal() 生成数据集

~~~python
	mean = (1, 1)
    cov = np.array([[10, 0], [0, 10]])
    x1 = np.random.multivariate_normal(mean, cov, (400,))

    mean = (1, 12)
    x2 = np.random.multivariate_normal(mean, cov, (400,))

    mean = (12, 1)
    x3 = np.random.multivariate_normal(mean, cov, (400,))

    mean = (12, 12)
    x4 = np.random.multivariate_normal(mean, cov, (400,))
~~~

生成的训练集：![Figure_1](img/Figure_1.png)

生成的测试集：![Figure_2](img/Figure_2.png)

训练结果：

| K    | 准确率   |
| ---- | :------- |
| 10   | 0.884375 |

#### 三、探究实验

##### 1. 修改mean参数

首先修改成如下形式，使得四类点分布的更开

~~~python
mean = (1, 1) mean = (1, 15) mean = (15, 1) mean = (15, 15)
~~~

得到的训练集：![Figure_3](img/Figure_3.png)

得到的测试集：![Figure_4](img/Figure_4.png)

训练结果：

| K    | 准确率   |
| ---- | -------- |
| 11   | 0.978125 |

可以看到不同的数据分布更开后，准确率大大提升。



修改成以下的形式后，不同的数据更加集中：

~~~python
ean = (1, 1) mean = (1, 8) mean = (8, 1) mean = (8, 8)
~~~

生成的训练集：![Figure_5](img/Figure_5.png)

生成的测试集：![Figure_6](img/Figure_6.png)

训练结果：

| K    | 准确率 |
| ---- | ------ |
| 18   | 0.7    |

可以看到准确率大大降低。

结论：KNN分类算法的准确性依赖于数据的分布，当不同种类的数据越集中（重合的部分越多时），效果越差；

反之，重合部分越少，分类结果越好。

##### 2.对cov参数的修改

修改cov,使得方差更大，同类数据更离散:

~~~python
cov = np.array([[30, 0], [0, 30]])
~~~

生成的训练集：![Figure_7](img/Figure_7.png)

生成的测试集：![Figure_8](img/Figure_8.png)

测试结果：

| K    | 准确率   |
| ---- | -------- |
| 18   | 0.690625 |

可以看到数据更离散后，准确率下降



再次修改cov，减少方差，使得同类数据更集中

```python
cov = np.array([[5, 0], [0, 5]])
```

生成的训练集：![Figure_9](img/Figure_9.png)

生成的测试集：![Figure_10](img/Figure_10.png)

训练结果：

| K    | 准确率 |
| ---- | ------ |
| 5    | 0.9875 |

可以看到同类数据更集中，准确率越高。

##### 探讨与结论

可以和实验一得到相同的结论，不同类数据间分布的重合区域越小，KNN分类方法的准确率越高；重合区域越大，准确率越低。



同时在上述实验中观察到的另一有趣现象是准确率越低时，K的最佳取指偏大；准确率越高时，K的取值偏小。但是在修改参数cov来控制正确率，希望得到准确率和K的关系时，却发现最佳K的选取还是具有一定的随机性，没有找到一般规律。

