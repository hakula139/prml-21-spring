import numpy as np
import matplotlib.pyplot as plt

class KNN:

    def __init__(self):
        pass
    
    #def settled_knn(self,test_data,train_data,train_label) :

    def fit(self, train_data, train_label):
        self.train_data=train_data
        self.train_label=train_label
        N=train_data.shape[0]
        cut=int(N*0.8)

        tra_data, test_data = train_data[:cut,], train_data[cut:,]
        tra_label, test_label = train_label[:cut,], train_label[cut:,]

        dataSetSize=tra_data.shape[0]
        test_number=test_data.shape[0]

        best_k=0
        max_score=0
        if N<6 :
            k_range=N
        else :
            k_range=20 

        for k in range(2,k_range):
            total_correct=0
            for i in range(0,test_number):
                inX=test_data[i]

                diffMat=np.tile(inX,(dataSetSize,1))-tra_data

                sqDifMat=diffMat**2
                sqDistances=sqDifMat.sum(axis=1)
                distances=sqDistances**0.5

                sortedDistIndicies=distances.argsort()

                classCount={}
                for j in range(k):
                    Label = tra_label[sortedDistIndicies[j]]
                    classCount[Label] = classCount.get(Label,0) + 1
                max_count=0
                for key,value in classCount.items():
                    if value >max_count :
                        max_count = value
                        pred_label=key
                
                if pred_label ==test_label[i]:
                    total_correct=total_correct+1

            score=total_correct*1.0/test_number
            if score>max_score:
                max_score=score
                best_k=k
        print("Best K: %d"%(best_k))
        self.k=best_k

    def predict(self, test_data):
        dataSetSize=self.train_data.shape[0]
        test_number=test_data.shape[0]
        ans_label=np.array([])
        for i in range(0,test_number):
                inX=test_data[i]

                diffMat=np.tile(inX,(dataSetSize,1))-self.train_data

                sqDifMat=diffMat**2
                sqDistances=sqDifMat.sum(axis=1)
                distances=sqDistances**0.5

                sortedDistIndicies=distances.argsort()
                classCount={}
                for j in range(self.k):
                    voteIlabel = self.train_label[sortedDistIndicies[j]]
                    classCount[voteIlabel] = classCount.get(voteIlabel,0) + 1

                max_count=0
                pred_label=self.train_label[0]
                for key,value in classCount.items():
                    if value >max_count :
                        max_count = value
                        pred_label=key

                ans_label=np.append(ans_label,pred_label)
        return ans_label
        
def generate():
    mean = (1, 1)
    cov = np.array([[10, 0], [0, 10]])
    x1 = np.random.multivariate_normal(mean, cov, (400,))

    mean = (1, 12)
    cov = np.array([[10, 0], [0, 10]])
    x2 = np.random.multivariate_normal(mean, cov, (400,))

    mean = (12, 1)
    cov = np.array([[10, 0], [0, 10]])
    x3 = np.random.multivariate_normal(mean, cov, (400,))

    mean = (12, 12)
    cov = np.array([[10, 0], [0, 10]])
    x4 = np.random.multivariate_normal(mean, cov, (400,))

    X = np.concatenate([x1, x2, x3, x4])
    Y = np.concatenate([
        np.zeros((400,),dtype=int),
        np.ones((400,),dtype=int),
        np.ones((400,),dtype=int)*2,
        np.ones((400,),dtype=int)*3
    ])
    shuffled_indices = np.random.permutation(1600)
    data=X[shuffled_indices]
    label=Y[shuffled_indices]
    total=1600
    cut=int(total*0.8)
    train_data, test_data = data[:cut,], data[cut:,]
    train_label, test_label = label[:cut,], label[cut:,]

    np.save("data.npy",(
         (train_data, train_label), (test_data, test_label)
    ))

def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

def display(data,label,name):
    datas=[[],[],[],[]]
    for i in range(len(data)):
        
        datas[label[i]].append(data[i])

    for each in datas:
        each=np.array(each)
        plt.scatter(each[:,0],each[:,1])
    #plt.savefig(f'imag/{name}')
    plt.show()


if __name__ == '__main__':
    generate()
    (train_data, train_label), (test_data, test_label) = read()
    display(train_data, train_label, 'train')
    display(test_data, test_label, 'test')

    model = KNN()
    model.fit(train_data, train_label)
    res = model.predict(test_data)
    print("acc =",np.mean(np.equal(res, test_label)))