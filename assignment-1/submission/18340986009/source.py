#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import numpy as np
import matplotlib.pyplot as plt


# ## Define Global Functions

# In[139]:


# Generate Training and Testing Sets
def generate(Ns, Means, Covs, train_frac):

    # Generate 2-D data of N class
    data = list()
    label = list()
    
    for i in range(0,len(Ns)):
        Ci = np.random.multivariate_normal(Means[i], Covs[i], Ns[i])
        data.append(Ci)
        label.append([i]*Ns[i])

    data = np.array([v for subl in data for v in subl])
    label = np.array([v for subl in label for v in subl])
    
    #Assign random number
    idx = np.arange(sum(Ns))
    np.random.shuffle(idx)
    
    data = data[idx]
    label = label[idx]

    # Split into training and testing set
    split_point = int(label.size * train_frac)
    train_data, test_data = data[:split_point,], data[split_point:,]
    train_label, test_label = label[:split_point,], label[split_point:,]
    
    np.save("data.npy",((train_data, train_label), 
                        (test_data, test_label)))

    return train_data, train_label, test_data, test_label


# Read in saved data
def read():
    (train_data, train_label), (test_data, test_label) = np.load(
        "data.npy", allow_pickle = True)
    return (train_data, train_label), (test_data, test_label)


# Create scatter plot of different categories
def display(data, colorby, name, title):
    colors = ['red','grey','blue']
    datas =[[],[],[]]
    
    for i in range(len(data)):
        datas[colorby[i]].append(data[i])
        
    for i in range(len(datas)):
        each = np.array(datas[i])
        if len(each) == 0:
            continue
        plt.scatter(each[:, 0], each[:, 1], 
                    marker = 'o',
                    color = colors[i],
                    alpha = 0.7)
        
    plt.xlabel("X1")
    plt.ylabel("X2")
    plt.title(title)
    plt.savefig(f'img/{name}')
    plt.show()


# ## Define Class KNN

# In[140]:


class KNN:

    def __init__(self):
        
        self.K = None
        self.Dist = None
        self.data = None
        self.label = None
    
    
    # Calculate distance between two given points
    def get_distance(self, x, y, dist_type = "Euclidean"):
        dist = 0.0
        if "Euclidean" == dist_type:
            distance = 0.0
            for i in range(len(x)):
                distance += (x[i] - y[i])**2
            dist = np.sqrt(distance)
            
        if "Manhattan" == dist_type:
            distance = 0.0
            for i in range(len(x)):
                distance += np.abs(x[i] - y[i])
            dist = distance
            
        return dist
    
    
    # Make a prediction for one point
    def predict_for_one(self, K, Dist, target, train_data, train_label):
        # Calculate distances between target point and other points
        dists = []
        neighbors = []

        for i in range(len(train_data)):
            dist = self.get_distance(target, train_data[i], Dist)
            dists.append((train_data[i], train_label[i], dist))

        # Get the K nearest neighbors
        dists.sort(key = lambda e: e[-1])
        neighbors = dists[1:K+1]

        # Make prediction based on conditional probabilities
        neighbors_class = [e[-2] for e in neighbors]
        prediction = max(neighbors_class, key = neighbors_class.count)

        return prediction

    
    # Calculate model accuracy
    def calc_accuracy(self, K, Dist, train_data, train_label):
        predictions = []
        # Make predictions for the training data
        for i in range(len(train_label)):
            target = train_data[i]
            prediction = self.predict_for_one(
                K, Dist, target, train_data, train_label
            )
            predictions.append(prediction)
        
        correct = 0
        for i in range(len(predictions)):
            if train_label[i] == predictions[i]:
                correct += 1
        accuracy = correct / len(predictions) * 100

        return accuracy
        
    
    # Find the Optimal K & Distance combination 
    def fit(self, K_list, Dist_list, train_data, train_label):
    
        # Loop through the given options for K and distance methods
        accuracy_list = []
        for i in range(len(Dist_list)):
            Dist = Dist_list[i]
            dum_list = []
            for j in range(len(K_list)):
                K = K_list[j]
                accuracy = self.calc_accuracy(
                K, Dist, train_data, train_label
                )
                dum_list.append(accuracy)
            accuracy_list.append(dum_list)
        
        # Find the K & Distance method that gives the highest accuracy
        ac_array = np.array(accuracy_list)
        global_max = max([max(subl) for subl in accuracy_list])
        params = np.where(ac_array == global_max)
        
        # Assign the optimal parameters to KNN object
        # Randomly choice one if there exist more than one highest accuracy
        Dist_idx = np.random.choice(np.array(params[0]))
        K_idx = np.random.choice(np.array(params[1]))
        
        self.Dist = Dist_list[Dist_idx]
        self.K = K_list[K_idx]
        self.data = train_data
        self.label = train_label
        
        return ac_array
        
    
    def predict(self, test_data):
        # If test data has been inputed & Model has been obtained 
        predictions = []
            # For every point(target) in test data
        for i in range(len(test_data)):
            target = test_data[i]
            prediction = self.predict_for_one(
                self.K, self.Dist, 
                target, 
                self.data, 
                self.label)
            predictions.append(prediction)
            
        return np.array(predictions)


# ## Start of Program

# In[143]:


if __name__ == '__main__':

    if len(sys.argv) > 1 and sys.argv[1] == "g":
        generate(
            Ns = [100, 250, 150],

            Means = [[50,50], 
                     [60,20], 
                     [20,60]], 

            Covs = [[[60,-50],[-50,140]], 
                    [[130,10],[10,100]], 
                    [[120,20],[20,90]]],

            train_frac = 0.8
        )
        
    elif len(sys.argv) > 1 and sys.argv[1] == "d":
        (train_data, train_label), (test_data, test_label) = read()
        
        display(train_data, train_label, 
                'train', 'Scatter Plot of Training Data')
        display(test_data, test_label, 
                'test', 'Scatter Plot of Testing Data')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        
        model.fit(
            K_list = [15, 20, 25], 
            Dist_list = ["Euclidean", "Manhattan"],
            train_data = train_data, 
            train_label = train_label)

        res = model.predict(test_data)

        print("acc =",np.mean(np.equal(res, test_label)))
        

