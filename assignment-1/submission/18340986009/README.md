# KNN Classification

This report includes two parts:
1. Find a KNN model that maximize accuracy rate with given dataset. (Distribution type of each class = Gaussian, distribution parameters chosen at random)
2. Assess how distribution parameters affects model accuracy using the model built in part 1.


## 1. Model Generation

### 1.1 Overview of Mock Data

Generate 3 classes of 2-dimension Gaussian Distribution.

$
  N_0 = 150 \hspace{1cm}
  C_0 \sim \mathcal{N}(\mu = \begin{bmatrix}50\\\\50\end{bmatrix},\sigma^{2} = \begin{bmatrix}60 & -50\\\\-50 & 140\end{bmatrix})
$

$
  N_1 = 250 \hspace{1cm}
  C_1 \sim \mathcal{N}(\mu = \begin{bmatrix}60\\\\20\end{bmatrix},\sigma^{2} = \begin{bmatrix}130 & 10\\\\10 & 100\end{bmatrix})
$

$
  N_2 = 100 \hspace{1cm}
  C_2 \sim \mathcal{N}(\mu = \begin{bmatrix}20\\\\60\end{bmatrix},\sigma^{2} = \begin{bmatrix}120 & 20\\\\20 & 90\end{bmatrix})
$

Mock Data 1 Overview:

<img src="img/Figure 1.png" width=450 height=300/>

500 points are then split randomly into training set (80%) and testing set (20%).

### 1.2 Model Accuracy with Different K and Distance Method

Since a rule of thumb is to let $K = \sqrt{N}$, where $ N = N_0 + N_1 + N_2$, we first try some Ks around $\sqrt{400} = 20$ using both Euclidean and Manhattan distance.

|       \      | K = 10 | K = 15 | K = 20 | K = 25 | K = 30 |
| ------------ |:------:|:------:|:------:|:------:|:------:|
| **Euclidean**    |83.0|82.0|83.0|81.0|80.0|
| **Manhattan**    |83.0|82.0|81.0|81.0|81.0|

The KNN model with $K = 10$ gives the best prediction result of 83% for both distance methods, so we consider choosing $K_{0} = 10$ as a starting point for model optimization. Below is a scatter plot showing the prediction result of the chosen model ($K = 10$, Euclidean Distance). Each red dot represents a mis-classification.

*Noticed model accuracy using different distance method doesn't show much difference for this dataset. 

<img src="img/Figure 2.png" width=450 height=300/>

### 1.3 Model Optimization

General Idea: $K_{i+1} = \lceil{K_{i} + Step_{i+1}}\rceil$

Detailed steps:

 - For each $K_{i+1}$, calculate its accuracy rate $R_{i+1}$.
 - If $R_{i+1} > R_{0}$, a better model is find. End our optimization. Else:
     - If $R_{i+1} > R_{i}$, let $Step_{i+1} =  \frac{1}{C} Step_{i} $, where $C = (R_{i+1} - R_{i}) / R_{i}$. 
     Which is, if model accuracy improves, continue in this direction with a smaller step. The step size is negatively related to the percentage of improvement.
     - If $R_{i+1} <= R_{i}$, let $Step_{i+1} = - \frac{1}{2} Step_{i}$. 
   Which is, if the new K does not improve model accuracy, try a smaller step in reverse direction.

The model from 1.2 gives K = 10 and Euclidean distance. Using this model as the starting point, define the first step $Step_{0} = \frac{1}{100}N = 5$.

Optimization process:
    
|       \      | K = 10 | K = 5 | K = 8 |
| ------------ |:------:|:------:|:------:|
| **Accuracy rate (%)**    |83.0|83.0|85.0|

 After three iterations, a higher accuracy rate of 85% is reached when K is adjusted to 8. Thus, our final KNN model will use K = 8 and Euclidean distance.

Prediction result evaluation:

<img src="img/Figure 3.png" width=450 height=300/>

Compared with the model before optimization, two points on the top is now classified correctly.

## 2. Distribution Parameters & Model Accuracy

From inuition, we hypothesis that any change that results in a more balanced mixture of all classes will make classification harder, thereby decrease model accuracy. Below, we modify the parameters of Gaussian distributions to test our hypothesis.

### 2.1 Change of Variance and Covariance

Let the means stay the same. Modify the variance-covariance matrix for each class to increase overlapping between each class:

$
  N_0 = 150 \hspace{1cm}
  C_0 \sim \mathcal{N}(\mu = \begin{bmatrix}50\\\\50\end{bmatrix},\sigma^{2} = \begin{bmatrix}300 & 0\\\\0 & 200\end{bmatrix}) 
$

$
  N_1 = 250 \hspace{1cm}
  C_1 \sim \mathcal{N}(\mu = \begin{bmatrix}60\\\\20\end{bmatrix},\sigma^{2} = \begin{bmatrix}250 & 0\\\\0 & 150\end{bmatrix})
$

$
  N_2 = 100 \hspace{1cm}
  C_2 \sim \mathcal{N}(\mu = \begin{bmatrix}20\\\\60\end{bmatrix},\sigma^{2} = \begin{bmatrix}150 & 0\\\\0 & 150\end{bmatrix})
$

Mock Data 2 Overview:

<img src="img/Figure 4.png" width=450 height=300/>

Prediction result evaluation:

<img src="img/Figure 5.png" width=450 height=300/>

Accuracy of our model drop from 85% to 79% as expected.

### 2.2 Change of Mean

Let other parameters stay the same, decrease the distance between the means of each class to increase overlapping:

$
  N_0 = 150 \hspace{1cm}
  C_0 \sim \mathcal{N}(\mu = \begin{bmatrix}50\\\\50\end{bmatrix},\sigma^{2} = \begin{bmatrix}60 & -50\\\\-50 & 140\end{bmatrix})
$

$
  N_1 = 250 \hspace{1cm}
  C_1 \sim \mathcal{N}(\mu = \begin{bmatrix}50\\\\40\end{bmatrix},\sigma^{2} = \begin{bmatrix}130 & 10\\\\10 & 100\end{bmatrix}) 
$

$
  N_2 = 100 \hspace{1cm}
  C_2 \sim \mathcal{N}(\mu = \begin{bmatrix}40\\\\60\end{bmatrix},\sigma^{2} = \begin{bmatrix}120 & 20\\\\20 & 90\end{bmatrix})
$

Mock Data 3 Overview:

<img src="img/Figure 6.png" width=450 height=300/>

Prediction result evaluation:

<img src="img/Figure 7.png" width=450 height=300/>

Accuracy of our model drop from 85% to 73% as expected.

### 2.3 N & Model Accuracy

In attempts to increase model accuracy, we try double the Ns in proportion to Data 3. With $N_{total} = 1000$, we expect some increase on model accuracy.

Mock Data 4 Overview:

<img src="img/Figure 8.png" width=450 height=300/>

Prediction result evaluation:

<img src="img/Figure 9.png" width=450 height=300/>

Model accuracy decreases from 73% to 62.5% even though our data size doubled. This suggests sample size contributes much less to model accuracy compared with distribution parameters. This makes sense because if the data labeled by different categories does indeed come from the same distribution, increasing N should provide more evidence of the similarity between these different categories.

## Summary

The main takeaways for this exercise:

Model accuracy depends more on distribution parameters and the choice of K. Distance method have little influence on model accuracy, and whether an increase of N improves model accuracy or not depends on if the true distributions of all categories are significantly different (Might be able to use p-value from a statistical test to evaluate).
