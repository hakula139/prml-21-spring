class KNN:

    def __init__(self):
        self.train_data = None
        self.train_labels = None
        self.k = None
        
    def train_test_split(self,x,y,rate):
        shuffled_indexes = np.random.permutation(len(x))
        test_size = int(len(x) * rate)
        train_index = shuffled_indexes[test_size:]
        test_index = shuffled_indexes[:test_size]
        return x[train_index], x[test_index], y[train_index], y[test_index]

    def distance(self,v1,v2):
    
        weight_array = (v1-v2)**2
        weight_array_sum = np.sum(weight_array)
        return weight_array_sum**(0.5)

    def fit(self, train_data, train_label):
        
        # 归一化
        mu = np.mean(train_data, axis=0)
        sigma = np.std(train_data, axis=0)
        train_data = (train_data - mu) / sigma
        
        # 划分训练集/验证集 with rate =0.3
        X_train, X_test, Y_train, Y_test = self.train_test_split(train_data,train_label,0.3)
        
        # 对于不同的K[1-20]，计算验证集到训练集的欧氏距离
        best_k=0
        k_candi=0;
        for k in range(1,20):

            true_couter=0
            for test_counter in range(0,len(X_test)):
                pos_vec_list=[]
                
                for train_counter in range(0,len(X_train)):
                    pos_vec = np.array([self.distance(X_test[test_counter],X_train[train_counter]),Y_train[train_counter]])
                    pos_vec_list.append(pos_vec)     
                    
                #对距离list根据距离排序
                pos_vec_list = np.array(pos_vec_list)
                pos_vec_list_sorted = pos_vec_list[np.lexsort(pos_vec_list[:,::-1].T)]
                #k-近邻结果列表
                result_list = pos_vec_list_sorted[:k][:,1]
            

            
                #test预测结果
                label = int(result_list[np.argmax(result_list)])
            
            
                #检验本次test在给定k下是否正确
                if (label == Y_test[test_counter] ):
                    true_couter=true_couter+1
            

            #最优K取值
            if (true_couter >= best_k):
                best_k = true_couter
                k_candi = k
                
        # print(k_candi)
        self.k = k_candi
        self.train_data = train_data
        self.train_labels = train_label
        return self.k

    def predict(self, test_data):
        test_label=[]
        result_list=[]
        
        # 归一化
        mu = np.mean(test_data, axis=0)
        sigma = np.std(test_data, axis=0)
        test_data = (test_data - mu) / sigma
        #test_data = test_data / np.sqrt(np.sum(test_data**2))
        
        for i in range (0,len(test_data)):
            pos_vec_list=[]    
            for m in range(0,len(self.train_data)):
                pos_vec = np.array([self.distance(self.train_data[m],test_data[i]),self.train_labels[m]])
                pos_vec_list.append(pos_vec)
            
            
            
            pos_vec_list = np.array(pos_vec_list)
            pos_vec_list_sorted = pos_vec_list[np.lexsort(pos_vec_list[:,::-1].T)]

            result_list = pos_vec_list_sorted[:(self.k)][:,1]
            test_label.append(result_list[np.argmax(result_list)])
            
        return test_label
    
def generate (amount_1,amount_2,amount_3):
    
    
    mean = (2, 2)
    cov = np.array([[1,0], [0, 1]])
    x = np.random.multivariate_normal(mean, cov, (amount_1,))  
    
    mean = (4, 6)
    cov = np.array([[2, 0], [0, 2]])
    y = np.random.multivariate_normal(mean, cov, (amount_2,))  

    mean = (10, 10)
    cov = np.array([[2,1],[1,3]])
    z = np.random.multivariate_normal(mean, cov, (amount_3,))  
    
    
    data = np.concatenate([x,y,z])

    label = np.concatenate([
            np.zeros((amount_1,),dtype=int),
            np.ones((amount_2,),dtype=int),
            np.ones((amount_3,),dtype=int)*2
        ])

    return model.train_test_split(data,label,0.2)


def display(x,y):
    type1_x = []; type1_y = []
    type2_x = []; type2_y = []
    type3_x = []; type3_y = []

    plt.figure(figsize=(8,6))

    for i in range(0,len(x)):
        if(y[i]==0):
            type1_x.append(x[i][0])
            type1_y.append(x[i][1])
        if(y[i]==1):
            type2_x.append(x[i][0])
            type2_y.append(x[i][1])
        if(y[i]==2):
            type3_x.append(x[i][0])
            type3_y.append(x[i][1])
        
    fig = plt.figure(figsize = (10, 6))
    ax = fig.add_subplot(111)

    type1 = ax.scatter(type1_x, type1_y, s = 30, c = 'brown')
    type2 = ax.scatter(type2_x, type2_y, s = 30, c = 'lime')
    type3 = ax.scatter(type3_x, type3_y, s = 30, c = "darkviolet")



    ax.legend((type1, type2, type3), ("A", "B", "C"), loc = 0)

    plt.show()
