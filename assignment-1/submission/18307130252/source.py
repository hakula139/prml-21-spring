import numpy as np
import matplotlib.pyplot as plt
# from tqdm import tqdm

# Provide several different types of distances:
# 0 for Manhattan distance
# 1 for Euclid distance
# 2 for Chebyshev distance
dis_type = 1

def draw_acc_pic(manhattan_acc, euclid_acc, chebyshev_acc, i):
    plt.xlabel("K value")
    plt.ylabel("accuracy")
    print(manhattan_acc)
    print(euclid_acc)
    print(chebyshev_acc)
    plt.plot(manhattan_acc, color = 'r', label="manhattan_distance")
    plt.plot(euclid_acc, color = 'g', label="euclid_distance")
    plt.plot(chebyshev_acc, color = 'b', label="chebyshev_acc")
    plt.savefig("acc_pic" + str(i) + ".jpg")
    plt.close()

def draw_train_data(dataset, num):
    X = [[], [], []]
    Y = [[], [], []]

    sz = len(dataset)
    for i in range(0, sz):
        X[dataset[i][1]].append(dataset[i][0][0])
        Y[dataset[i][1]].append(dataset[i][0][1])
    plt.scatter(X[0], Y[0], s = 10, c = "r")
    plt.scatter(X[1], Y[1], s = 10, c = "g")
    plt.scatter(X[2], Y[2], s = 10, c = "b")
    plt.savefig("fig_train" + str(num) + ".jpg")
    plt.close()

def draw_test_data(dataset, num):
    
    X = [[], [], []]
    Y = [[], [], []]

    sz = len(dataset)
    for i in range(0, sz):
        X[dataset[i][1]].append(dataset[i][0][0])
        Y[dataset[i][1]].append(dataset[i][0][1])
    plt.scatter(X[0], Y[0], s = 10, c = "r")
    plt.scatter(X[1], Y[1], s = 10, c = "g")
    plt.scatter(X[2], Y[2], s = 10, c = "b")
    plt.savefig("fig_test" + str(num) + ".jpg")
    plt.close()

class KNN:

    def __init__(self):
        self.K_neighbors = -1
        self.K_max = 200
        self.K_folder = 5
        self.train_data = []
        self.train_label = []
        self.dataset = []
        self.cross_validation = 0
        pass

    def cross_validation_fit(self, train_data, train_label):
        dataset = list(zip(train_data, train_label))
        self.dataset = dataset
        data_size = len(dataset)
        np.random.shuffle(dataset)
        train_size = (int)(data_size * 0.8)
        dev_size = data_size - train_size
        best_acc = -1
        acc_array = []
        # for k in tqdm(range(1, self.K_max)):
        for k in range(1, self.K_max):
            tag = 0
            for fold in range(1, self.K_folder):
                dev_begin = dev_size * fold
                dev_end = min(data_size, dev_size * (fold + 1))
                devset = dataset[dev_begin:dev_end]
                if dev_end < data_size:
                    trainset = np.vstack([
                        dataset[:dev_begin],
                        dataset[dev_end:]
                    ])
                else:
                    trainset = dataset[:dev_begin]
                for (dev_data, dev_label) in devset:
                    dis = []
                    for (train_data, train_label) in trainset:
                        dis.append((distance(dev_data, train_data), train_label))
                    dis.sort(key= lambda k:k[0])
                    dis = dis[:k]
                    dis = [(x) for (_, x) in dis]
                    tag += (int)(max(dis, key = dis.count) == dev_label)
            if best_acc < tag:
                best_acc = tag
                self.K_neighbors = k
            acc_array.append(tag/(self.K_folder * dev_size))
        
        # print("cross_validation:  ", self.K_neighbors)
        return acc_array
    
    def simple_validation_fit(self, train_data, train_label):
        dataset = list(zip(train_data, train_label))
        self.dataset = dataset
        data_size = len(dataset)
        np.random.shuffle(dataset)
        train_size = (int)(data_size * 0.8)
        dev_size = data_size - train_size
        trainset = dataset[ : train_size]
        devset = dataset[train_size : ]
        best_acc = -1
        acc_array = []
        
        # for k in tqdm(range(1, self.K_max)):
        
        for k in range(1, self.K_max):
            tag = 0
            for (dev_data, dev_label) in devset:
                dis = []
                for (train_data, train_label) in trainset:
                    dis.append((distance(dev_data, train_data), train_label))
                dis.sort(key= lambda k:k[0])
                dis = dis[:k]
                dis = [(x) for (_, x) in dis]
                tag += (int)(max(dis, key = dis.count) == dev_label)
            if best_acc < tag:
                best_acc = tag
                self.K_neighbors = k
            acc_array.append(tag/dev_size)
        
        # print("simple_validation:  ", self.K_neighbors)
        return acc_array

    def fit(self, train_data, train_label):
        self.train_data = train_data
        self.train_label = train_label
        if self.cross_validation == 0:
            return self.simple_validation_fit(train_data, train_label)
        else:
            return self.cross_validation_fit(train_data, train_label)
        

    def predict(self, test_data):
        if self.K_neighbors == -1:
            print("Error: KNN.fit(train_data, train_label) must be called before KNN.predict(test_data).")
            return None
        pred_label = []
        for cur in test_data:
            dis = []
            for (train_data, train_label) in self.dataset:
                dis.append((distance(cur, train_data), train_label))
            dis.sort(key= lambda k:k[0])
            dis = dis[:self.K_neighbors]
            dis = [(x) for (_, x) in dis]
            pred_label.append(max(dis, key = dis.count))
        return pred_label

def distance(A, B):
    if dis_type == 0:
        return manhattan_distance(A, B)  
    elif dis_type == 1:
        return euclid_distance(A, B)
    elif dis_type == 2:
        return chebyshev_distance(A, B)
    else:
        print("dis_type is not implemented.")

def chebyshev_distance(A, B):
    return max(abs(A[0] - B[0]), abs(A[1] - B[1]))

def manhattan_distance(A, B):
    return abs(A[0] - B[0]) + abs(A[1] - B[1])

def euclid_distance(A, B):
    return (A[0] - B[0]) * (A[0] - B[0]) + (A[1] - B[1]) * (A[1] - B[1])

    

def generate(mean, cov, prior, sz, num):
    tot = [int(sz * prior[i]) for i in range(3)]
    dataset = [(list(x), 0) for x in np.random.multivariate_normal(mean[0], cov[0], size=tot[0])]
    dataset.extend([(list(x), 1) for x in np.random.multivariate_normal(mean[1], cov[1], size=tot[1])])
    dataset.extend([(list(x), 2) for x in np.random.multivariate_normal(mean[2], cov[2], size=tot[2])])
    dataset_size = len(dataset)
    np.random.shuffle(dataset)
    train_size = (int)(dataset_size * 0.8)
    train_data, train_label = zip(*(dataset[ : train_size]))
    test_data, test_label = zip(*(dataset[train_size : ]))

    
    draw_train_data(dataset[ : train_size], num)
    draw_test_data(dataset[train_size : ], num)
    
    np.save("data.npy", ((train_data, train_label), (test_data, test_label)))
    

def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

if __name__ == "__main__":
    
    mean = [[0, 0], [-2, -2], [3, -2]]
    cov = [[[1, 0], [0, 1]], [[1, 0], [0, 2]], [[2, 0], [0, 1]]]
    prior = [0.3, 0.3, 0.4]
    sz = 3000
    generate(mean, cov, prior, sz, 1)
    (train_data, train_label), (test_data, test_label) = read()

    model = KNN()
    
    dis_type = 0
    manhattan_acc = model.fit(train_data, train_label)
    dis_type = 1
    euclid_acc = model.fit(train_data, train_label)
    dis_type = 2
    chebyshev_acc = model.fit(train_data, train_label)
    draw_acc_pic(manhattan_acc, euclid_acc, chebyshev_acc, i)
    res = model.predict(test_data)

    dis_type = 0

    model = KNN()
    normal_acc = []
    kfolder_acc = []
    
    for i in range(1, 6):
        generate(mean, cov, prior, sz, 10 + i)
        (train_data, train_label), (test_data, test_label) = read()
        model.cross_validation = 0
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("id: ", i, " normal_acc: ", np.mean(np.equal(res, test_label)))
        normal_acc.append(np.mean(np.equal(res, test_label)))
        model.cross_validation = 1
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("id: ", i, " kfolder_acc: ", np.mean(np.equal(res, test_label)))
        kfolder_acc.append(np.mean(np.equal(res, test_label)))

    print(normal_acc)
    print(kfolder_acc)
    
    mean = [
        [[0, 0], [-1, -1], [1, 1]],
        [[0, 0], [-3, -3], [3, 3]],
        [[0, 0], [-1, -1], [1.5, -1]],
        [[0, 0], [-2, -2], [3, -2]]
    ]
    cov = [[[1, 0], [0, 1]], [[1, 0], [0, 2]], [[2, 0], [0, 1]]]
    prior = [0.3, 0.3, 0.4]
    sz = 3000
    for i in range(0, 4):
        generate(mean[i], cov, prior, sz, 20 + i)
        (train_data, train_label), (test_data, test_label) = read()
        model.cross_validation = 0
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("id: ", i, " normal_acc: ", np.mean(np.equal(res, test_label)))
        normal_acc.append(np.mean(np.equal(res, test_label)))
    print(normal_acc)
    
    mean = [[0, 0], [-2, -2], [3, -2]]
    cov = [
        [[[1, 0], [0, 1]], [[1, 0], [0, 2]], [[2, 0], [0, 1]]],
        [[[2, 0], [0, 2]], [[2, 0], [0, 4]], [[4, 0], [0, 2]]],
        [[[10, 5], [5, 10]], [[5, 3], [3, 2]], [[4, 2], [2, 8]]],
        [[[5, 2.5], [2.5, 5]], [[2.5, 1.5], [1.5, 1]], [[2, 1], [1, 4]]]
    ]
    prior = [0.3, 0.3, 0.4]
    sz = 3000
    for i in range(0, 4):
        generate(mean, cov[i], prior, sz, 30 + i)
        (train_data, train_label), (test_data, test_label) = read()
        model.cross_validation = 0
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("id: ", i, " kfolder_acc: ", np.mean(np.equal(res, test_label)))
        kfolder_acc.append(np.mean(np.equal(res, test_label)))

    mean = [[0, 0], [-2, -2], [3, -2]]
    cov = [[[1, 0], [0, 1]], [[1, 0], [0, 2]], [[2, 0], [0, 1]]]
    prior = [
        [0.3, 0.3, 0.4],
        [0.2, 0.2, 0.8],
        [0.1, 0.5, 0.4]
    ]
    sz = 3000
    for i in range(0, 3):
        generate(mean, cov, prior[i], sz, 40 + i)
        (train_data, train_label), (test_data, test_label) = read()
        model.cross_validation = 0
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("id: ", i, " normal_acc: ", np.mean(np.equal(res, test_label)))
        normal_acc.append(np.mean(np.equal(res, test_label)))
    print(normal_acc)