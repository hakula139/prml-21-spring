import numpy as np
import matplotlib.pyplot as plt
import random
import sys

NUM_OF_LABELS = 4
DIMENSION = 2
# 用来控制数据集的样本个数
NUM_BASIS = 200



def set_random_seed(seed):
    np.random.seed(seed)
    random.seed(seed)


def dis_function(x, y, mode='L2'):
    d = y.shape[1]
    x = x.reshape(1, -1)
    y = y.reshape(-1, d)
    if mode == 'L2':
        return np.linalg.norm(x - y, axis=1)
    elif mode == 'L1':
        return np.linalg.norm(x - y, axis=1, ord=1)
    elif mode == 'L_inf':
        return np.linalg.norm(x - y, axis=1, ord=np.inf)
    elif mode == 'Gaussian':
        gamma = 1e-3
        var = lambda x_i, x_j: np.exp(- gamma * np.square(np.linalg.norm(x_i - x_j, axis=1)))
        return np.sqrt(var(x, x) - 2 * var(x, y) + var(y, y))



class KNN:

    def __init__(self):
        self.k = 1
        self.data = None
        self.label = None
        self.mean = 0
        self.std = 0

    def fit(self, train_data, train_label):
        n, d = train_data.shape
        acc_best = 0
        k_best = self.k
        k_max = min(30, int(n * 0.8))

        # 归一化
        self.mean = np.mean(train_data, axis=0)
        self.std = np.std(train_data, axis=0)
        train_data = (train_data - self.mean) / self.std

        idx = np.arange(n)
        np.random.shuffle(idx)

        train_data = train_data[idx]
        train_label = train_label[idx]

        acc_best = 0
        k_best = 0

        for j in range(5):
            val_start_point = int(n * j * .2)
            val_end_point = int(n * (j + 1) * .2)

            temp_val_data = train_data[
                val_start_point: val_end_point, :
            ]
            temp_val_label = train_label[
                val_start_point: val_end_point
            ]

            temp_train_data = np.vstack(
                [
                    train_data[:val_start_point, :],
                    train_data[val_end_point:, :]
                ]
            )

            temp_train_label = np.concatenate(
                [
                    train_label[:val_start_point],
                    train_label[val_end_point:]
                ]
            )

            # temp_train_data, temp_val_data = train_data[:int(
            #     n * 0.8), :], train_data[int(n * 0.8):, :]
            # temp_train_label, temp_val_label = train_label[:int(
            #     n * 0.8)], train_label[int(n * 0.8):]
            self.data = temp_train_data
            self.label = temp_train_label

            for i in range(1, k_max, 2):
                self.k = i
                res = self.predict(temp_val_data, normalization=False)
                acc = np.mean(np.equal(res, temp_val_label))
                # print(i, acc)
                if acc > acc_best:
                    acc_best = acc
                    k_best = i
                    # print(acc_best, k_best)
                self.k = k_best
            # print(f'j={j}')
        self.data = train_data
        self.label = train_label

    def predict(self, test_data, normalization=True, mode='Gaussian'):
        n, d = test_data.shape
        if normalization:
            test_data = (test_data - self.mean) / self.std
        res = []
        for i in range(n):
            temp = test_data[i, :]
            # dis = np.linalg.norm(temp - self.data, axis=1)  # 使用欧式距离
            # 使用核函数
            dis = dis_function(temp, self.data, mode=mode)
            idx = np.argpartition(dis, self.k)[:self.k]
            # print(self.data[idx], self.label[idx], np.argmax(np.bincount(self.label[idx])))
            res.append(np.argmax(np.bincount(self.label[idx])))
        return np.array(res)


def generate_cov(dimension):
    # 用于生成随机的数据集
    A = np.abs(np.random.randn(dimension, dimension))
    B = np.dot(A, A.T)
    return B


def generate():
    global NUM_OF_LABELS
    global DIMENSION
    # 通过控制系数调整mean和cov
    mean = [(13 * 1, 4 * 1), (1 * 1, 14 * 1), (-8 * 1, -3 * 1), (-15 * 1, 10 * 1)]
    # mean = [tuple(np.random.randn(DIMENSION,) * mean_factor)
            # for i in range(NUM_OF_LABELS)]

    cov = [
        np.array([[35, 0], [0, 23]]) * 1,
        np.array([[12, 1], [1, 35]]) * 1,
        np.array([[33, 5], [5, 9]]) * 1,
        np.array([[25, 6], [6, 18]]) * 1,
    ]
    # cov = [
    #     generate_cov(DIMENSION) * cov_factor for i in range(NUM_OF_LABELS)
    # ]
    dataset = [None] * NUM_OF_LABELS
    data_num = []
    num_of_examples = 0
    for i in range(NUM_OF_LABELS):
        data_num.append((i + 1) * 2 * NUM_BASIS)
        num_of_examples += (i + 1) * 2 * NUM_BASIS
        dataset[i] = np.random.multivariate_normal(
            mean[i], cov[i], ((i + 1) * 2 * NUM_BASIS, ))
    # print(data_num)
    idx = np.arange(num_of_examples)
    np.random.shuffle(idx)
    data = np.concatenate([item for item in dataset])
    label = np.concatenate(
        # [
        #     np.zeros((data_num[0],), dtype=int),
        #     np.ones((data_num[1],), dtype=int),
        #     np.ones((data_num[2],), dtype=int) * 2,
        #     np.ones((data_num[3],), dtype=int) * 3
        # ]
        [np.ones((data_num[i],), dtype=int) * i for i in range(NUM_OF_LABELS)]
    )

    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:int(
        num_of_examples * 0.8), :], data[int(num_of_examples * 0.8):, :]
    train_label, test_label = label[:int(
        num_of_examples * 0.8)], label[int(num_of_examples * 0.8):]

    np.save(
        'data.npy',
        (
            (train_data, train_label),
            (test_data, test_label)
        )
    )


def read():
    (train_data, train_label), (test_data, test_label) = np.load(
        'data.npy', allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)


def display(data, label, name):
    global NUM_OF_LABELS
    datas = [[] for i in range(NUM_OF_LABELS)]
    for i in range(len(data)):
        datas[label[i]].append(data[i])

    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

def experiment():
    generate()
    data = read()
    train_data, train_label = data[0][0], data[0][1]
    test_data, test_label = data[1][0], data[1][1]
    # display(train_data, train_label, 'train_divide')
    # display(test_data, test_label, 'test_divide')
    model = KNN()
    model.fit(train_data, train_label)
    res = model.predict(test_data, normalization=True)
    acc = np.mean(np.equal(res, test_label))
    print(model.k)
    print("acc =", acc)
    return model.k, acc, compute_index(res, test_label)

def compute_index(res, label):
    res_bin = np.bincount(res)
    label_bin = np.bincount(label)
    return res_bin, label_bin


if __name__ == '__main__':

    if len(sys.argv) > 1 and sys.argv[1] == "g":
        generate()
    elif len(sys.argv) > 1 and sys.argv[1] == "d":
        (train_data, train_label), (test_data, test_label) = read()
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("acc =",np.mean(np.equal(res, test_label)))


