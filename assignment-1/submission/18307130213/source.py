import math
import heapq
import numpy as np
import random
import matplotlib.pyplot as plt
import sys

class KNN:

    def __init__(self):
        self.__data = None
        self.__lable = None
        self.__num = None
        self.__dim = None
        self.__k = None

    def fit(self, train_data, train_label):
        if type(train_data) != np.ndarray:
            print('error: wrong type of train_data')
            return
        if len(train_data.shape) != 2:
            print('error: wrong shape of train_data')
            return
        if type(train_label) != np.ndarray:
            print('error: wrong type of train_label')
            return
        if len(train_label.shape) != 1:
            print('error: wrong shape of train_label')
            return
        num_data, dim_data = train_data.shape
        num_label, = train_label.shape
        if num_data != num_label:
            print('error: shape of train_data and train_label can not match')
            return
        if num_data < 1:
            print('error: less than 1 data')
            return

        label_k = len(np.unique(train_label))

        self.__data = train_data
        self.__label = train_label
        self.__num = num_data
        self.__dim = dim_data
        self.__k = min(num_data, math.floor(math.log(num_data, 2)), label_k + 1)

        print('finish: fit')
        return

    def predict(self, test_data):
        if self.__k == None:
            print('error: not fit yet')
            return
        if type(test_data) != np.ndarray:
            print('error: wrong type of test_data')
            return
        if len(test_data.shape) != 2:
            print('error: wrong shape of test_data')
            return

        test_data_num, test_data_dim = test_data.shape
        if test_data_dim != self.__dim:
            print('error: wrong dimention of test_data')
            return
        
        tmp_ans = []
        for i in range(test_data_num):
            tmp_inum = [j for j in range(self.__num)]
            closest = heapq.nsmallest(self.__k, tmp_inum, key = lambda s: np.linalg.norm(test_data[i]-self.__data[s]))
            tmp_dict = {}
            lab, cnt = -1, 0
            for j in range(self.__k):      
                tmp_cnt = tmp_dict[self.__label[closest[j]]] = tmp_dict.get(self.__label[closest[j]], 0) + 1               
                if tmp_cnt > cnt:
                    lab, cnt = self.__label[closest[j]], tmp_cnt
            tmp_ans.append(lab)

        return np.array(tmp_ans)

def generate(n):
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
    if n <= 0:
        print('error: n <= 0')
        return
    r = n/max(1, math.log(n, 2))
    sizs = []
    xs = []
    for i in range(n):
        theta = i*(2*math.pi/n)
        mean = (r*math.cos(theta) , r*math.sin(theta))
        rand_mat = np.random.rand(2, 2)
        cov = rand_mat.transpose()*rand_mat
        siz = random.randint(200, 1000)
        sizs.append(siz)
        x = np.random.multivariate_normal(mean, cov, (siz, ))
        xs.append(x)
    siz = sum(sizs)
    idx = np.arange(siz)
    np.random.shuffle(idx)
    data = np.concatenate(xs)
    label = np.concatenate([np.ones((sizs[j], ), dtype=int)*j for j in range(n)])
    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:(siz//n)*(n-1),], data[(siz//n)*(n-1):,]
    train_label, test_label = label[:(siz//n)*(n-1),], label[(siz//n)*(n-1):,]

    np.save("data.npy",(
        (train_data, train_label), (test_data, test_label)
    ))

def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

def genimg(n, data, label, name):
    datas =[[] for i in range(n)]
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    
    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.close()
    # plt.show()

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'g':
        try:
            n = int(sys.argv[2])
            generate(n)
        except:
            print('error: wrong n')
    elif len(sys.argv) > 1 and sys.argv[1] == 'd':      
        (train_data, train_label), (test_data, test_label) = read()
        try:
            n = int(sys.argv[2])
            genimg(n, train_data, train_label, 'train')
            genimg(n, test_data, test_label, 'test')
        except:
            print('somthing goes wrong!')
    else:
        (train_data, train_label), (test_data, test_label) = read()
        model = KNN()
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("acc =",np.mean(np.equal(res, test_label)))