import numpy as np
import matplotlib.pyplot as plt
import sys
class KNN:

    def __init__(self, norm='zscore', metric='euclidean', k=3):
        self.train_data = None
        self.train_label = None
        self.num_of_class = None
        self.k = k
        self.norm = norm
        self.metric = metric
        self.min = None
        self.max = None
        self.mean = None
        self.std = None
    def get_params(self, deep=True):
        return dict({'norm': self.norm, 'metric': self.metric, 'k': self.k})
    def manhattan_(self, x, y):
        return np.sum(np.abs(x - y))
            
    def euclidean_(self, x, y):
        return np.sqrt(np.sum((x - y)**2))
        
    def cosine(self, x, y):
        return 1- np.dot(x, y)/(np.linalg.norm(x)* np.linalg.norm(y))
    
    def zscore_norm_(self, data):
        if self.mean is None:
            self.mean = np.mean(data, axis=0)
        if self.std is None:
            self.std = np.std(data, axis=0)
        temp = list(self.std)
        for i in range(len(temp)):
            if temp[i]==0:
                temp[i]=1
        
        return (data - self.mean)/np.array(temp)
    
    def minmax_norm_(self, data):
        if self.min is None:
            self.min = np.min(data, axis=0)
        if self.max is None:
            self.max = np.max(data, axis=0)

        temp = list(self.max-self.min)
        for i in range(len(temp)):
            if temp[i]==0:
                temp[i]=1

        return (data - self.min)/np.array(temp)
        
    def fit(self, train_data, train_label):
        #print("training data shape: ", train_data.shape)
        if self.norm == 'zscore':
            self.train_data = self.zscore_norm_(train_data)
        elif self.norm == 'minmax':
            self.train_data = self.minmax_norm_(train_data)
        else:
            self.train_data = train_data

        self.train_label = train_label
        self.num_of_class = len(set(train_label.tolist()))
        #print("number of classes: ", self.num_of_class)

    def predict(self, test_data):
        #print("testing data shape: ", test_data.shape)
        self.test_label = np.array([])
        if self.norm == 'zscore':
            self.test_data = self.zscore_norm_(test_data)
        elif self.norm == 'minmax':
            self.test_data = self.minmax_norm_(test_data)
        else:
            self.test_data = test_data
            

        if self.metric== 'euclidean':
            self.distance_matrix_ = np.array([[self.euclidean_(x, y) for y in self.train_data] for x in self.test_data])
        elif self.metric== 'manhattan':
            self.distance_matrix_ = np.array([[self.manhattan_(x, y) for y in self.train_data] for x in self.test_data])
        elif self.metric== 'cosine':
            self.distance_matrix_ = np.array([[self.cosine(x, y) for y in self.train_data] for x in self.test_data])
        
        else:
            self.distance_matrix_ = np.array([[self.euclidean_(x, y) for y in self.train_data] for x in self.test_data])

        k_ = np.argpartition(self.distance_matrix_, self.k)[:, 0:self.k]
        
        self.test_label = np.argmax(np.array([np.bincount(self.train_label[k_][i], minlength = self.num_of_class) for i in range(k_.shape[0])]), axis=1)
        
        return self.test_label
    def score(self, test_data, test_label):
        y_pred = self.predict(test_data)
        acc = np.mean(np.equal(y_pred, test_label))
        return acc
    
def generate():
    n = 20
    mean1 = [0,0]
    mean2 = [20,20]
    mean3 = [-20,20]
    cov1 = np.diag(np.random.randint(0,100,2))
    cov2 = np.diag(np.random.randint(0,100,2))
    cov3 = np.diag(np.random.randint(0,100,2))
    c1_x = np.random.multivariate_normal(mean = mean1, cov = cov1, size= 600)
    c2_x = np.random.multivariate_normal(mean = mean2, cov = cov2, size= 400)
    c3_x = np.random.multivariate_normal(mean = mean3, cov = cov3, size= 500)
    x_data = np.concatenate([c1_x, c2_x, c3_x], axis=0)
    x_data.shape

    y_data = np.concatenate([[0]*c1_x.shape[0], [1]*c2_x.shape[0], [2]*c3_x.shape[0]], axis=0)
    y_data.shape

    idx = [i for i in range(len(x_data))]
    np.random.shuffle(idx)

    x_data = x_data[idx]
    y_data = y_data[idx]

    train_data = x_data[:int(0.8*x_data.shape[0])]
    test_data = x_data[int(0.8*x_data.shape[0]): ]
    train_label = y_data[:int(0.8*x_data.shape[0])]
    test_label = y_data[int(0.8*x_data.shape[0]):]
    np.save(
        'data.npy',
        (
            (train_data, train_label),
            (test_data, test_label)
        )
    )

def read():
    (train_data, train_label), (test_data, test_label) = np.load(
        'data.npy', allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

def display(data, label, name):
    num_of_class = len(list(set(label.tolist())))
    datas = {}
    for i in set(label.tolist()):
        datas[i] = data[label==i,:]
        plt.scatter(datas[i][:,0], datas[i][:,1], label=str(i))
    
    plt.savefig(f'img/{name}')
    plt.show()
    
if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "g":      
        generate()
    if len(sys.argv) > 1 and sys.argv[1] == "d":      
        (train_data, train_label), (test_data, test_label) = read()
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN(k=9, metric='euclidean', norm='zscore')
        # 选择距离计算公式、评估公式
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("acc =", np.mean(np.equal(res, test_label)))