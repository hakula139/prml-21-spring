import sys
import numpy as np
import matplotlib.pyplot as plt

class KNN:

    def __init__(self):
        self.train_data = None
        self.train_label = None
        self.k = None

    def fit(self, train_data, train_label):
        self.train_data = train_data
        self.train_label = train_label
        # 将训练集打乱
        data_size = self.train_data.shape[0]
        shuffled_indices = np.random.permutation(data_size)
        shuffled_data = train_data[shuffled_indices]
        shuffled_label = train_label[shuffled_indices]
        # test_ratio为测试集所占的百分比，划分训练集和验证集
        test_ratio = 0.2
        test_set_size = int(data_size * test_ratio)
        valid_data = shuffled_data[:test_set_size]
        valid_label = shuffled_label[:test_set_size]
        training_data = shuffled_data[test_set_size:]
        training_label = shuffled_label[test_set_size:]
        # 在验证集上对不同的K值进行测试
        record ={}
        if training_data.shape[0] < 20:
            k_number = training_data.shape[0]
        else:
            k_number = 20
        for k in range(1,k_number,2):
            data_size = training_data.shape[0]
            predict_result = np.array([])
            for i in range(valid_data.shape[0]):
                diff = np.tile(valid_data[i], (data_size, 1)) - training_data
                sqdiff = diff ** 2
                squareDist = np.sum(sqdiff, axis=1)
                dist = squareDist ** 0.5
                # test_data到其它点的距离
                sorteddiffdist = np.argsort(dist)
                # 对这些距离从小到大排序
                classCount = {}
                for j in range(k):
                    Label = training_label[sorteddiffdist[j]]
                    classCount[Label] = classCount.get(Label, 0) + 1
                # 统计距离中前K个值中各个类别的数量
                maxCount = 0
                for key, value in classCount.items():
                    if value > maxCount:
                        maxCount = value
                        result = key
                predict_result = np.append(predict_result, result)
            acc = np.mean(np.equal(predict_result, valid_label))
            record[k] = acc
        # 取验证准确率最高的K值作为K值
        maxCount = 0
        for key, value in record.items():
            if value > maxCount:
                maxCount = value
                k_result = key
        print("k=",k_result)
        self.k = k_result

    def predict(self, test_data):
        data_size = self.train_data.shape[0]
        predict_result = np.array([])
        for i in range(test_data.shape[0]):
            diff = np.tile(test_data[i],(data_size,1)) - self.train_data
            sqdiff = diff **2
            squareDist = np.sum(sqdiff, axis =1)
            dist = squareDist **0.5
            # test_data到其它点的距离
            sorteddiffdist = np.argsort(dist)
            # 对这些距离从小到大排序
            classCount ={}
            for j in range(self.k):
                Label = self.train_label[sorteddiffdist[j]]
                classCount[Label] = classCount.get(Label,0) + 1
            # 统计距离中前K个值中各个类别的数量
            maxCount = 0
            for key, value in classCount.items():
                if value > maxCount:
                    maxCount = value
                    result = key
            predict_result = np.append(predict_result,result)
        # 数量最多的就是预测的结果
        return predict_result


def generate():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))
    #x = np.random.multivariate_normal(mean, cov, (1600,))

    mean = (16, -5)
    #mean = (30, -20)
    #mean = (15, 0)
    cov = np.array([[21.2, 0], [0, 32.1]])
    #cov = np.array([[73, 0], [0, 22]])
    y = np.random.multivariate_normal(mean, cov, (200,))
    #y = np.random.multivariate_normal(mean, cov, (400,))

    mean = (10, 22)
    #mean = (10,10)
    cov = np.array([[10, 5], [5, 10]])
    #cov = np.array([[73, 0], [0, 22]])
    z = np.random.multivariate_normal(mean, cov, (1000,))
    #z = np.random.multivariate_normal(mean, cov, (2000,))

    idx = np.arange(2000)
    #idx = np.arange(2800)
    np.random.shuffle(idx)
    data = np.concatenate([x, y, z])
    label = np.concatenate([
        np.zeros((800,), dtype=int),
        np.ones((200,), dtype=int),
        np.ones((1000,), dtype=int) * 2
    ])
    # label = np.concatenate([
    #     np.zeros((1600,), dtype=int),
    #     np.ones((200,), dtype=int),
    #     np.ones((1000,), dtype=int) * 2
    # ])
    # data = data[idx]
    # label = label[idx]

    train_data, test_data = data[:1600, ], data[1600:, ]
    train_label, test_label = label[:1600, ], label[1600:, ]
    # train_data, test_data = data[:2240, ], data[2240:, ]
    # train_label, test_label = label[:2240, ], label[2240:, ]
    np.save("data.npy", (
        (train_data, train_label), (test_data, test_label)
    ))


def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy", allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)


def display(data, label, name):
    datas = [[], [], []]
    for i in range(len(data)):
        datas[label[i]].append(data[i])

    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'./{name}')
    plt.show()


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "g":
        generate()
    if len(sys.argv) > 1 and sys.argv[1] == "d":
        (train_data, train_label), (test_data, test_label) = read()
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("acc =", np.mean(np.equal(res, test_label)))



