# 课程报告

## KNN实现

### 距离的计算

$$
\begin{array}\\\\
assume\ test\ matrix\ P: M\times D,\ train\ matrix\ C:N\times D. D\ is\ the\ dimension\ of\ data.\\\\
let\ P_i\ is\ ith\ row\ in\ P,\ C_j\ is\ jth\ row\ in\ C.\\\\
distance\ between\ P_i\ and\ C_j:\\\\
d(P_i,C_j) = \sqrt{\sum_{k=1}^{D}(P_{ik}-C_{jk})^2}\\\\
=\sqrt{\sum_{k=1}^DP_{ik}^2+\sum_{k=1}^DC_{jk}^2-2\sum_{k=1}^D(P_{ik}C_{jk})}\\\\
=\sqrt{||P_i||^2+||C_j||^2-2P_iC_j^T}\\\\
then\ we\ can\ calculate\ the\ whole\ distance\ matrix\ only\ with\ matrix\ operations.
\end{array}
$$

### 数据预处理：归一化

将数据归一化到[0, 1]，在服从正态分布的数据集上测试时表现不佳（详见实验部分）。最终代码中有实现归一化（normalize中），但是并未应用。

### k的取值

fit函数中，k在[2, 6]中取最优值。fit函数先把train_data划分为train_data和dev_data，比例为1：4；然后计算出距离矩阵；最后k遍历[2, 6]，找到在dev_data上测试所得到最高正确率的k值，应用于最后的预测。

## 实验

### 实验一 正则化

此实验目的是探究KNN中数据正则化的影响。

实验了多组数据，只有在测试数据参数为

$$
\begin{array}{l}
&\Sigma_1&=&\left[\begin{array}{cc}
3 & 0 \\\\
0 & 70
\end{array}\right] \qquad
&\Sigma_2&=&\left[\begin{array}{cc}
4 & 0 \\\\
0 & 65
\end{array}\right] \qquad
&\Sigma_3&=&\left[\begin{array}{cc}
2 & 0 \\\\
0 & 75
\end{array}\right]\\\\
&\mu_1&=&\left[\begin{array}{ll}
4 & -20
\end{array}\right]
&\mu_2&=&\left[\begin{array}{ll}
5 & 0
\end{array}\right]
&\mu_3&=&\left[\begin{array}{ll}
6 & 20
\end{array}\right]
\end{array}
$$

时，使用正则化取得更好的结果。

训练集：

<img src="./img/train_1.png" alt="train1" style="zoom:72%;" />

测试集：

<img src="./img/test_1.png" alt="test1" style="zoom:72%;" />

| k                | 2      | 3      | 4      | 5      | 6      |
| ---------------- | ------ | ------ | ------ | ------ | ------ |
| acc_dev 无归一化 | 87.81% | 91.88% | 91.25% | 91.25% | 91.25% |
| acc_dev 有归一化 | 87.81% | 91.88% | 91.25% | 91.25% | 91.25% |

最佳k值都为3，无归一化时，在test上为准确率：88.25%；有归一化时，在test上准确率为89.25%。

在其他使用正态分布生成的数据中，都是不使用归一化准确率更高。在上例中，使用归一化正确率提升仅1%，而在其他数据上，不使用归一化正确率提高会高得多。所以在最终的代码中并未对数据进行归一化处理。在本系列实验中，归一化与否并不影响k的最佳取值。

实验结论：首先，归一化并不适合在正态分布上的KNN分类。其次，归一化不影响最佳k值。

### 实验二 改变分布之间的距离

使用两个正态分布探究不同高斯分布之间距离的影响。先保持高斯分布的协方差矩阵不变，改变均值之间的距离。

训练集：

![train_2](./img/train_2.jpg)

测试集：

![train_2](./img/test_2.jpg)

| 序号    | 1      | 2      | 3      | 4      | 5      | 6      |
| ------- | ------ | ------ | ------ | ------ | ------ | ------ |
| 准确率  | 97.75% | 98.25% | 98.50% | 92.25% | 87.75% | 85.00% |
| 最佳k值 | 2      | 3      | 5      | 5      | 5      | 6      |

可以看出，两个分布的数据点范围开始重叠时，准确率开始下降，重叠范围越大，准确率越低，k值也在相应增大。

接下来，保持两个分布均值距离不变，仅改变高斯分布的协方差矩阵。

训练集：

<img src="./img/train_3.jpg" alt="train_2" style="zoom:25%;" />

测试集：

<img src="./img/test_3.jpg" alt="train_2" style="zoom:25%;" />

| 序号    | 1      | 2      | 3      | 4      |
| ------- | ------ | ------ | ------ | ------ |
| 准确率  | 96.75% | 96.25% | 95.00% | 92.50% |
| 最佳k值 | 5      | 5      | 6      | 3      |

类似地，准确率随着分布的重叠而降低。

## 代码使用方法

```bash
python source.py g  # 生成数据集
python source.py d  # 展示数据集
python source.py    # 训练和测试
```

# 参考

距离的计算：https://blog.csdn.net/IT_forlearn/article/details/100022244?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&dist_request_id=&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control

