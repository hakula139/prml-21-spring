import sys
import numpy as np
import matplotlib.pyplot as plt

class KNN:

    def __init__(self):
        self.train_data = None
        self.train_label = None
        self.k = 2

    def fit(self, train_data, train_label):
        # train_data = self.normalize(train_data)
        self.train_data = train_data
        self.train_label = train_label

        N = train_data.shape[0]
        cut = N // 5 * 4
        train_data, dev_data = train_data[:cut,], train_data[cut:,]
        train_label, dev_label = train_label[:cut,], train_label[cut:,]

        dists = self.compute_distances(train_data, dev_data)

        max_acc = 0
        max_acc_k = 2
        for k in range(2,7):
            res = self.get_label(dists, train_label, k)
            acc = np.mean(np.equal(res, dev_label))
            print("k = %d, acc = %f" % (k, acc))
            if acc > max_acc:
                max_acc = acc
                max_acc_k = k
        print("best k = %d" % max_acc_k)
        self.k = max_acc_k

    def predict(self, test_data):
        # test_data = self.normalize(test_data)
        dists = self.compute_distances(self.train_data, test_data)
        return self.get_label(dists, self.train_label, self.k)

    def compute_distances(self, train_data, test_data):
        num_train = train_data.shape[0]
        num_test = test_data.shape[0]
        dists = np.zeros((num_test, num_train))

        train_square = np.sum(np.square(train_data), axis=1).reshape(1, num_train)
        test_square = np.sum(np.square(test_data), axis=1).reshape(num_test, 1)
        dists = np.sqrt(train_square + test_square - 2 * np.dot(test_data, train_data.T))
        
        return dists

    def get_label(self, dists, train_label, k):
        num_test = dists.shape[0]
        y_predict = np.zeros(num_test, dtype=train_label.dtype)
        for i in range(num_test):
            closest_y = list(train_label[np.argsort(dists[i,:])[:k]])
            y_predict[i] = max(closest_y, key = closest_y.count)
        return y_predict

    def normalize(self, data):
        if len(data) == 0:
            return data
        return (data - np.min(data)) / (np.max(data) - np.min(data))


def generate():
    # mean = (1, 2)
    # cov = np.array([[73, 0], [0, 22]])
    mean = (-17, 2)
    cov = np.array([[103, 0],[0, 22]])
    x = np.random.multivariate_normal(mean, cov, (1200,))

    # mean = (16, -5)
    # cov = np.array([[21.2, 0], [0, 32.1]])
    mean = (10, -5)
    cov = np.array([[101.2, 0],[0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (800,))  

    # mean = (10, 22)
    # cov = np.array([[10,5],[5,10]])
    # z = np.random.multivariate_normal(mean, cov, (1000,))  

    idx = np.arange(2000)
    np.random.shuffle(idx)
    # data = np.concatenate([x,y,z])
    data = np.concatenate([x,y])
    label = np.concatenate([
        # np.zeros((800,),dtype=int),
        # np.ones((200,),dtype=int),
        # np.ones((1000,),dtype=int)*2
        np.zeros((1200,),dtype=int),
        np.ones((800,),dtype=int),
    ])
    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:1600,], data[1600:,]
    train_label, test_label = label[:1600,], label[1600:,]
    np.save("data.npy",(
        (train_data, train_label), (test_data, test_label)
    ))

def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy", allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

def display(data, label, name):
    # datas = [[], [], []]
    datas = [[], []]
    for i in range(len(data)):
        datas[label[i]].append(data[i])

    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "g":      
        generate()
    elif len(sys.argv) > 1 and sys.argv[1] == "d":      
        (train_data, train_label), (test_data, test_label) = read()
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("acc =",np.mean(np.equal(res, test_label)))
