import numpy as np
from collections import Counter
import matplotlib.pyplot as plt

class KNN:
        k_n=[1,2,3,4,5]
        k_select=0
        X=[]
        y=[]
        def __init__(self):
            pass
        
        def train_test_split(self,X,y):
            offset=int(len(X)*0.8)
            X_train=X[:offset]
            X_test=X[offset:]
            y_train=y[:offset]
            y_test=y[offset:]
  
            return np.array(X_train),np.array(X_test),np.array(y_train),np.array(y_test)
        
        def distance(self,instance1,instance2):
            dist = np.sqrt(sum((instance1 - instance2)**2))
            return dist
    
        def predict1(self,test_data,k):
            predict=[]
            for instance in test_data:
                distances=np.array([self.distance (x,instance) for x in self.X])
                
                kneighbors=np.argsort(np.array(distances))[:k]
                count = Counter(self.y[kneighbors])
                predict.append(count.most_common()[0][0])
            return predict
        
        def fit(self, train_data, train_label):
            X_train,X_test,y_train,y_test=self.train_test_split(train_data, train_label)
            self.X=np.array(X_train)
            self.y=np.array(y_train)
            max_accurate=0
            best_k=0
            for k in self.k_n:
                accurate=0
                train_predict=self.predict1(X_test,k)
                correct = np.count_nonzero((train_predict==y_test)==True)

                accurate=correct/len(X_test)
                if (accurate>max_accurate):
                    max_accurate=accurate
                    best_k=k
            self.k_select=best_k

        def predict(self, test_data):
            return self.predict1(test_data,self.k_select)

def generate():
    X1 = np.random.multivariate_normal([1,50], [[1,0],[0,10]], 100)
    X2 = np.random.multivariate_normal([3,50], [[1,0],[0,10]], 100)
    X3 = np.random.multivariate_normal([5,50], [[1,0],[0,10]], 100)
    X  = np.concatenate([X1,X2,X3])
    y  = np.array([0]*100 + [1]*100 +[2]*100)
    idx = np.arange(300)
    np.random.shuffle(idx)
    
    data=X=X[idx]
    label=y=y[idx]
    
    X_train=X[:240]
    X_test=X[240:]
    y_train=y[:240]
    y_test=y[240:]
    return np.array(X_train),np.array(X_test),np.array(y_train),np.array(y_test)

def display(data, label, name):
    datas =[[],[],[]]
    colors=['b','r','y']
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    for i,each in enumerate(datas):
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1],c=colors[i])
    plt.show()

if __name__ == '__main__':
    X_train,X_test,y_train,y_test=generate()
    model=KNN()
    model.fit(X_train,y_train)
    predict=model.predict(X_test)
    display(X_train,y_train,'train')
    display(X_test,y_test,'test')
    display(X_test,predict,'predict')
    correct = np.count_nonzero((predict==y_test))
    accurate=correct/len(X_test)
    print('accu=',accurate)