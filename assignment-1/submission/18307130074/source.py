import numpy as np
import matplotlib.pyplot as mp

class KNN:

    def __init__(self):
        
        self.train_data_num = 0
        self.valid_data_num = 0
        self.train_data_dimension = 0

        self.train_data = None
        self.valid_data = None
        self.train_label = None
        self.valid_label = None

        self.K = 20


    def partition(self, data, label):

        length = len(data)
        list1 = []
        list2 = []
        list3 = []
        
        for i in range(0, length):
            list1.append([data[i], label[i]])
        
        np.random.shuffle(list1)

        for each in list1:
            list2.append(each[0])
            list3.append(each[1])

        list2 = np.array(list2)
        list3 = np.array(list3)

        return list2, list3

    
    def distance(self, s, t, p='Euclid'):
        """
            表示两个点之间的距离，s和t的类型应该为
            p表示距离的种类，目前已实现的有曼哈顿距离(Manhattan)，欧几里得(Euclid), 上确界距离(upper)
        """
        ans = 0

        if p == 'Euclid':
            for each in range(self.train_data_dimension):
                ans += (s[each] - t[each])**2
        
        elif p == 'Manhattan':
            for each in range(self.train_data_dimension):
                ans += abs(s[each] - t[each])
        
        elif p == 'upper':
            for each in range(self.train_data_dimension):
                ans = max(abs(s[each] - t[each]), ans)
        
        return ans


    def predict_label(self, k, data1, label1, data2, p='Euclid'):
        
        result = []
        for point in data2:

            dist = []
            
            for i in range(len(data1)):
                dist.append(self.distance(point, data1[i], p))
                
            dist = np.array(dist)
            indices = np.argpartition(dist, k)[:k]

            counter = {}
            temp = []
            maxc = 0
            
            for each in indices:
                if label1[int(each)] in counter:
                    counter[label1[int(each)]] += 1
                else:
                    counter[label1[int(each)]] = 1

                if maxc < counter[label1[int(each)]]:
                    maxc = counter[label1[int(each)]]
            
            for each in counter:
                if counter[each] == maxc:
                    temp.append(each)
            
            result.append(np.random.choice(temp))
        
        result = np.array(result)
        return result


    def fit(self, train_data, train_label):
        
        # 确定训练集和验证集的数量和数据维度
        self.valid_data_num = max(1, train_data.shape[0] // 5)
        self.train_data_num = train_data.shape[0] - self.valid_data_num
        self.train_data_dimension = train_data.shape[1]

        # 将数据集随机划分
        temp_data, temp_label = self.partition(train_data, train_label)
        self.train_data, self.train_label = temp_data[:self.train_data_num], temp_label[:self.train_data_num]
        self.valid_data, self.valid_label = temp_data[self.train_data_num:], temp_label[self.train_data_num:]
        
        max_re, best_k, max_k = 0, 0, min(20, self.train_data_num)

        for i in range(1, max_k):
            result = self.predict_label(i, self.train_data, self.train_label, self.valid_data)
            re = np.mean(np.equal(result, self.valid_label))
            if re > max_re:
                max_re = re
                best_k = i
        
        self.K = best_k
        print("acc =",max_re)
        print("k=", best_k)


    def predict(self, test_data):
        return self.predict_label(self.K, self.train_data, self.train_label, test_data)


def data_maker(id, mean, cov, num):

    data = None
    label = None

    data = np.concatenate([np.random.multivariate_normal(mean[i], cov[i], (num[i],)) for i in range(3)])
    label = np.concatenate([np.ones((num[i],), dtype=int) * i for i in range(3)])

    length = len(data)
    test_data_num = length // 5

    data, label = KNN().partition(data, label)
    
    test_data = data[:test_data_num]
    test_label = label[:test_data_num]
    train_data = data[test_data_num:]
    train_label = label[test_data_num:]

    np.save(str(id) + 'data.npy', (train_data, train_label, test_data, test_label))


def data_reader(id):
    return np.load(str(id) + 'data.npy', allow_pickle=True)


def data_painter(id, data, label, name):

    length = len(data)
    painter = {}
    
    for i in range(length):
        if label[i] in painter:
            painter[label[i]].append(data[i])
        else:
            painter[label[i]] = [data[i]]
    
    for each in painter:
        temp = np.array(painter[each])
        mp.scatter(temp[:, 0], temp[:, 1])

    mp.savefig(f'./{str(id) + name}')
    mp.show()
    mp.close()


if __name__ == "__main__":    
    mean = [(10, 10), (5, 15), (15, 5)]
    cov = [np.array([[10, 5], [5, 10]]), np.array([[10, 4], [4, 10]]), np.array([[10, 3], [3, 10]])]
    num = [800, 800, 800]
    id = 5
    # data_maker(id, mean, cov, num)
    train_data, train_label, test_data, test_label = data_reader(id)
    # data_painter(id, train_data, train_label, 'train')
    # data_painter(id, test_data, test_label, 'test')
    model = KNN()
    for i in range(5):
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        print("acc =",np.mean(np.equal(res, test_label)))