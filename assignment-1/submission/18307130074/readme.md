# KNN Classifier

[TOC]



## 1. Code Introduction

所有代码都在**source.py**中展示

引用库包括**numpy**&**matplotlib**

```
import numpy as np
import matplotlib.pyplot as mp
```

**KNN**类包括如下几个函数**partition**, **distance**, **predict_label**, **fit**, **predict**

首先需要初始化KNN类中的成员变量

```
def __init__(self):
        
        self.train_data_num = 0
        self.valid_data_num = 0
        self.train_data_dimension = 0

        self.train_data = None
        self.valid_data = None
        self.train_label = None
        self.valid_label = None

        self.K = 20
```

其中train_data_num表示训练集数量，valid_data_num表示验证集数量，train_data_dimension表示数据维度（在本次实验中我们默认你使用二维数据），（train_data,valid_data,train_label,valid_label）分别表示（训练集数据，验证集数据，训练集标签，验证集标签），K表示通过训练所得出来的最优K值

**partition**函数输入的**参数**包括：数据集和标签集，**作用**是：将输入的数据和标签随机取80%作为训练集的数据和标签，其余20%作为验证集的数据和标签。

**distance**函数输入的**参数**包括：点1，点2，计算距离的方式（默认为Euclid即欧几里得距离），**作用**是：以给定的方式计算两个点之间的距离

**predict_label**函数输入的**参数**包括：k值，数据集1，标签集1，数据集2，计算距离的方式， **作用**是：给定k值，以数据集1和标签集1作为已知的点集，通过k近邻算法计算并返回数据集2所对应的标签集

**fit**函数输入的**参数**包括：数据集和标签集，**作用**是：首先利用**partition**函数将数据集和标签集按照8：2的比例分为训练集和验证集，然后枚举k的值，并通过**predict_label**函数预测训练集的标签集，并对比该结果和给定的训练集标签，得到准确率。选取准确率最高的k值作为模型的k值。

**predict**函数输入的**参数**包括：数据集，**作用**是：将该数据集作为测试集，利用已经训练好的KNN模型返回测试集的标签，一般配合**fit**函数使用。

**一些其他的函数**：

1. **data_maker**函数输入的**参数**包括：id，平均值，协方差矩阵，数量，**作用**是：通过np.random.multivariate_normal函数以及给定的参数生成数据，并将数据分为训练集和测试集，通过np.save储存

2. **data_reader**函数输入的**参数**包括：id，**作用**是：通过np.read读取**data_maker**生成的数据

3. **data_painter**函数输入的**参数**包括：id，数据集，标签集，name，**作用**是：通过matplotlib.pyplot.scatter来画出数据的散点图，便于直观观察数据的分布

## 2. Experiment

### 1. Initial Data

数据集参数
$$
\mu_x=[10,10] ,\Sigma_x=\begin{bmatrix}30&5 \\\\ 5&10\end{bmatrix},|x|=400\\\\
\mu_y=[5,15],\Sigma_y=\begin{bmatrix}20&4 \\\\ 4&10\end{bmatrix},|y|=400\\\\
\mu_z=[15,5],\Sigma_z=\begin{bmatrix}20&3 \\\\ 3&4\end{bmatrix},|z|=400
$$
训练集和测试集散点图

<center>
    <img src="./img/1train.png" alt="train" style="zoom: 60%;" />
    <img src="./img/1test.png" alt="test" style="zoom: 60%;" />
</center>

实验结果（重复实验5次）

| k      | train  | test   |
| ------ | ------ | ------ |
| 18     | 0.8490 | 0.8667 |
| 16     | 0.8333 | 0.8542 |
| 5      | 0.8021 | 0.8458 |
| 8      | 0.8646 | 0.8333 |
| 10     | 0.8490 | 0.8542 |
| 平均值 | 0.8396 | 0.8508 |

### 2. Research

#### 1.研究重叠度对准确率的影响

##### 重叠度增大

数据集参数
$$
\mu_x=[10,10] ,\Sigma_x=\begin{bmatrix}40&5 \\\\ 5&40\end{bmatrix},|x|=400\\\\
\mu_y=[5,15],\Sigma_y=\begin{bmatrix}40&4 \\\\ 4&40\end{bmatrix},|y|=400\\\\
\mu_z=[15,5],\Sigma_z=\begin{bmatrix}40&3 \\\\ 3&40\end{bmatrix},|z|=400
$$
训练集和测试集散点图

<center>
    <img src="./img/2train.png" alt="train" style="zoom: 60%;" />
    <img src="./img/2test.png" alt="test" style="zoom: 60%;" />
</center>

实验结果（重复实验5次）

| k      | train  | test   |
| ------ | ------ | ------ |
| 19     | 0.6146 | 0.5917 |
| 14     | 0.6510 | 0.5708 |
| 18     | 0.5990 | 0.5875 |
| 11     | 0.6563 | 0.5875 |
| 6      | 0.6719 | 0.575  |
| 平均值 | 0.6386 | 0.5825 |

##### 重叠度减小

数据集参数
$$
\mu_x=[10,10] ,\Sigma_x=\begin{bmatrix}10&5 \\\\ 5&10\end{bmatrix},|x|=400\\\\
\mu_y=[5,15],\Sigma_y=\begin{bmatrix}10&4 \\\\ 4&10\end{bmatrix},|y|=400\\\\
\mu_z=[15,5],\Sigma_z=\begin{bmatrix}10&3 \\\\ 3&10\end{bmatrix},|z|=400
$$
训练集和测试集散点图

<center>
    <img src="./img/3train.png" alt="train" style="zoom: 60%;" />
    <img src="./img/3test.png" alt="test" style="zoom: 60%;" />
</center>

实验结果（重复实验5次）

| k      | train  | test   |
| ------ | ------ | ------ |
| 12     | 0.9271 | 0.8875 |
| 7      | 0.9167 | 0.875  |
| 6      | 0.9375 | 0.8625 |
| 6      | 0.9271 | 0.8792 |
| 10     | 0.9635 | 0.8792 |
| 平均值 | 0.9344 | 0.8767 |

##### 结论

随着不同标签的点集重叠度增大，KNN分类器的准确率降低

#### 2.研究点集数量对准确率的影响

##### 数据减少

数据集参数
$$
\mu_x=[10,10] ,\Sigma_x=\begin{bmatrix}10&5 \\\\ 5&10\end{bmatrix},|x|=200\\\\
\mu_y=[5,15],\Sigma_y=\begin{bmatrix}10&4 \\\\ 4&10\end{bmatrix},|y|=200\\\\
\mu_z=[15,5],\Sigma_z=\begin{bmatrix}10&3 \\\\ 3&10\end{bmatrix},|z|=200
$$
训练集和测试集散点图

<center>
    <img src="./img/4train.png" alt="train" style="zoom: 60%;" />
    <img src="./img/4test.png" alt="test" style="zoom: 60%;" />
</center>

实验结果（重复实验5次）

| k      | train  | test   |
| ------ | ------ | ------ |
| 6      | 0.8958 | 0.9    |
| 8      | 0.9479 | 0.8833 |
| 18     | 0.9063 | 0.8917 |
| 7      | 0.9688 | 0.8833 |
| 14     | 0.875  | 0.8833 |
| 平均值 | 0.9188 | 0.8883 |

##### 数据增多

数据集参数
$$
\mu_x=[10,10] ,\Sigma_x=\begin{bmatrix}10&5 \\\\ 5&10\end{bmatrix},|x|=800\\\\
\mu_y=[5,15],\Sigma_y=\begin{bmatrix}10&4 \\\\ 4&10\end{bmatrix},|y|=800\\\\
\mu_z=[15,5],\Sigma_z=\begin{bmatrix}10&3 \\\\ 3&10\end{bmatrix},|z|=800
$$
训练集和测试集散点图

<center>
    <img src="./img/5train.png" alt="train" style="zoom: 60%;" />
    <img src="./img/5test.png" alt="test" style="zoom: 60%;" />
</center>

实验结果（重复实验5次）

| k      | train  | test   |
| ------ | ------ | ------ |
| 9      | 0.8958 | 0.9188 |
| 9      | 0.9115 | 0.925  |
| 18     | 0.9323 | 0.9083 |
| 10     | 0.9193 | 0.9208 |
| 18     | 0.9089 | 0.9188 |
| 平均值 | 0.9136 | 0.9183 |

##### 结论

数据的多少很有可能影响着KNN分类器的准确率，但是影响效果不明显，准确率随着数据的增多而提高

#### 3.研究计算距离的方式对准确率的影响

##### 曼哈顿距离

数据集参数
$$
\mu_x=[10,10] ,\Sigma_x=\begin{bmatrix}10&5 \\\\ 5&10\end{bmatrix},|x|=800\\\\
\mu_y=[5,15],\Sigma_y=\begin{bmatrix}10&4 \\\\ 4&10\end{bmatrix},|y|=800\\\\
\mu_z=[15,5],\Sigma_z=\begin{bmatrix}10&3 \\\\ 3&10\end{bmatrix},|z|=800
$$
训练集和测试集散点图

<center>
    <img src="./img/5train.png" alt="train" style="zoom: 60%;" />
    <img src="./img/5test.png" alt="test" style="zoom: 60%;" />
</center>

实验结果（重复实验5次）

| k      | train  | test   |
| ------ | ------ | ------ |
| 17     | 0.8906 | 0.9167 |
| 7      | 0.9010 | 0.9083 |
| 13     | 0.9167 | 0.9271 |
| 9      | 0.9115 | 0.9208 |
| 17     | 0.9219 | 0.9292 |
| 平均值 | 0.9083 | 0.9204 |

##### 上确界距离

数据集参数
$$
\mu_x=[10,10] ,\Sigma_x=\begin{bmatrix}10&5 \\\\ 5&10\end{bmatrix},|x|=800\\\\
\mu_y=[5,15],\Sigma_y=\begin{bmatrix}10&4 \\\\ 4&10\end{bmatrix},|y|=800\\\\
\mu_z=[15,5],\Sigma_z=\begin{bmatrix}10&3 \\\\ 3&10\end{bmatrix},|z|=800
$$
训练集和测试集散点图

<center>
    <img src="./img/5train.png" alt="train" style="zoom: 60%;" />
    <img src="./img/5test.png" alt="test" style="zoom: 60%;" />
</center>

实验结果（重复实验5次）

| k      | train  | test   |
| ------ | ------ | ------ |
| 13     | 0.9036 | 0.9083 |
| 17     | 0.9115 | 0.9167 |
| 16     | 0.9089 | 0.9229 |
| 9      | 0.9089 | 0.9125 |
| 14     | 0.9167 | 0.9229 |
| 平均值 | 0.9099 | 0.9167 |

##### 结论

三种计算距离的方式所得到的结果相差不多，说明在数据量足够大且分类明确的情况下，欧几里得距离、上确界距离、曼哈顿距离的效果相近