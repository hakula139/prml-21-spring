import numpy as np
import matplotlib.pyplot as plt

class KNN:

    def __init__(self,n_neighbors=-1,p=2): #初始化
        self.n_neighbors = n_neighbors  #n_neighbors: 超参数，根据最接近的n_neighbors个点预测
        self.x_train = None     #训练集输入
        self.y_train = None     #训练集label
        self.p = p          #闵可夫斯基距离参数p
        

    def fit(self, train_data, train_label):   #训练：对于指定n_neighbors 的 knn，将训练集保存起来就可以了
        assert train_data.shape[0] >= self.n_neighbors
        assert train_data.shape[0] == train_label.shape[0]
        self.x_train = train_data
        self.y_train = train_label
        
        if self.n_neighbors == -1: #此时需要自动选择k值
            #选择k值
            def single_predict(x_train,y_train, x, k):  #预测一个训练样本点，指定k值
                dif = x_train - x
                dist = (np.sum(dif ** self.p,axis=1)) ** (1/self.p)  #计算x与训练集的闵可夫斯基距离

                near_list = np.argsort(dist)
                topk = [y_train[i] for i in near_list[0:k]]  #找到k最近邻居
                # print(topk)

                count_dic = {}
                for x in topk:
                    if x in count_dic.keys():
                        count_dic[x] += 1
                    else:
                        count_dic[x] = 1

                count_item = sorted(count_dic.items(),key=lambda x:x[1],reverse=True)  #按照在topk中出现的次数排序
                # print(count_item)
                topest = []       #topest表示出现次数最多的样本点，因为有可能重复，所以是一个列表
                maxcon = 0
                for x in count_item:
                    if x[1] >= maxcon:
                        topest.append(x[0])
                    else :
                        break
                
                # print(topest)
                i = np.random.randint(len(topest))    #如果出现次数最多的样本点不唯一，随机取其中一类。
                return topest[i]

            def predict(x_train,y_train, test_data,k):  #对于test_data集合中的每个元素，调用_predict做出预测
                return np.array([single_predict(x_train,y_train, i,k) for i in test_data])


            #划分训练集和测试集
            #将数据打乱（索引）
            count = train_data.shape[0]
            idx = np.arange(count)
            np.random.shuffle(idx)
            # print(idx)
            # print((count))
            #划分训练集测试集
            ratio = 0.8  #测试集占20%
            count1 = int(count * ratio)
            train_data_1 = train_data[idx[:count1],]
            train_label_1 = train_label[idx[:count1]]
            test_data = train_data[idx[count1:],]
            test_label = train_label[idx[count1:]]
            # print(train_label_1[10:20])

            k_con = min(20,train_label_1.shape[0])
            k_acc = [0 for _ in range(k_con + 1)]
            for k in range(1,k_con + 1):
                k_predict = predict(train_data_1,train_label_1,test_data,k)
                acc = np.mean(np.equal(k_predict, test_label))
                # print("k = {},acc = {}".format(k, acc  ))
                k_acc[k] = acc

            maxacc = 0
            for i in range(len(k_acc)):
                if k_acc[i] > maxacc:
                    k_max = i
                    maxacc = k_acc[i]
            self.n_neighbors = k_max
            # print('1111111111111111111111')
            print('Choosed n_neighbor: ' + str(k_max))
            return self
        

    def predict(self, test_data):  #对于test_data集合中的每个元素，调用_predict做出预测
        assert self.x_train is not None and self.y_train is not None
        assert self.x_train.shape[1] == test_data.shape[1]

        return np.array([self._predict(i) for i in test_data])

    def _predict(self , x):  #预测一个训练样本点
        dif = self.x_train - x
        dist = (np.sum(dif ** self.p,axis=1)) ** (1/self.p)  #计算x与训练集的闵可夫斯基距离

        near_list = np.argsort(dist)
        topk = [self.y_train[i] for i in near_list[0:self.n_neighbors]]  #找到k最近邻居

        count_dic = {}
        for x in topk:
            if x in count_dic.keys():
                count_dic[x] += 1
            else:
                count_dic[x] = 1

        count_item = sorted(count_dic.items(),key=lambda x:x[1],reverse=True)  #按照在topk中出现的次数排序
        # print(count_item)
        topest = []       #topest表示出现次数最多的样本点，因为有可能重复，所以是一个列表
        maxcon = 0
        for x in count_item:
            if x[1] >= maxcon:
                topest.append(x[0])
            else :
                break
        
        # print(topest)
        i = np.random.randint(len(topest))    #如果出现次数最多的样本点不唯一，随机取其中一类。
        return topest[i]

def test(mean_val,divergence_ratio_val):
    train_set = []
    #生成随机数据
    divergence_ratio = divergence_ratio_val
    mean_val = mean_val

    mean = (mean_val,mean_val)
    cov = np.array([[mean_val ** 2 * divergence_ratio,0],[0,mean_val ** 2 * divergence_ratio]])
    train_set.append(np.random.multivariate_normal(mean, cov, (600,)))

    mean = (-mean_val,mean_val)
    cov = np.array([[mean_val ** 2 * divergence_ratio,0],[0,mean_val ** 2 * divergence_ratio]])
    train_set.append(np.random.multivariate_normal(mean, cov, (600,)))

    mean = (-mean_val,-mean_val)
    cov = np.array([[mean_val ** 2 * divergence_ratio,0],[0,mean_val ** 2 * divergence_ratio]])
    train_set.append(np.random.multivariate_normal(mean, cov, (600,)))

    mean = (mean_val,-mean_val)
    cov = np.array([[mean_val ** 2 * divergence_ratio,0],[0,mean_val ** 2 * divergence_ratio]])
    train_set.append(np.random.multivariate_normal(mean, cov, (600,)))

    ######################################################################################
    # mean = (1, 2)
    # cov = np.array([[73, 0], [0, 22]])
    # train_set.append( np.random.multivariate_normal(mean, cov, (800,))  )
    # #x = np.random.multivariate_normal(mean, cov, (1600,))

    # mean = (16, -5)
    # #mean = (30, -20)
    # #mean = (15, 0)
    # cov = np.array([[21.2, 0], [0, 32.1]])
    # #cov = np.array([[73, 0], [0, 22]])
    # train_set.append( np.random.multivariate_normal(mean, cov, (200,)) )
    # #y = np.random.multivariate_normal(mean, cov, (400,))

    # mean = (10, 22)
    # #mean = (10,10)
    # cov = np.array([[10, 5], [5, 10]])
    # #cov = np.array([[73, 0], [0, 22]])
    # train_set.append( np.random.multivariate_normal(mean, cov, (1000,)) )
    ######################################################################################

    #标签
    label = []
    for i in range(len(train_set)):
        con = train_set[i].shape[0]
        for _ in range(con):
            label.append(i)
    label = np.array(label)

    data = np.concatenate([x for x in train_set], axis=0)
    # print(data)

    #将数据打乱（索引）
    count = data.shape[0]
    idx = np.arange(count)
    np.random.shuffle(idx)
    # print(idx)
    # print(count)
    # print('---')
    #划分训练集测试集
    ratio = 0.8  #测试集占20%
    count1 = int(count * ratio)
    train_data = data[idx[:count1],]
    train_label = label[idx[:count1]]
    test_data = data[idx[count1:],]
    test_label = label[idx[count1:]]

    display(train_data, train_label,'1')
    display(test_data, test_label,'2')

    model = KNN(n_neighbors=-1)
    model.fit(train_data, train_label)
    res = model.predict(test_data)
    acc = np.mean(np.equal(res, test_label))
    print("divergencr = {}".format(divergence_ratio_val) )
    print("acc = {}".format( acc  ))
    print('-----------------------------------------------')
    return acc


def display(data, label, fname):
    kind_con = 0
    for x in label:
        if x > kind_con:
            kind_con = x
    # print(kind_con)
    datas = [[] for _ in range(kind_con + 1)]
    for i in range(len(data)):
        datas[label[i]].append(data[i])

    for d in datas:
        d = np.array(d)
        # print(each)
        plt.scatter(d[:,0], d[:,1])
    plt.savefig('./pic/{}'.format(fname))
    plt.show()

if __name__ == "__main__":
    # ratio_list = []
    # acc_list = []
    # for i in range(10):
    #     ratio = i * 0.1
    #     acc = test(mean_val=100,divergence_ratio_val=ratio)
    #     acc_list.append(acc)
    #     ratio_list.append(ratio)
    # plt.scatter(np.array(ratio_list),np.array(acc_list))
    # plt.savefig('./pic/{}'.format('result'))
    # plt.show()
    test(mean_val=100,divergence_ratio_val=0.3)

        