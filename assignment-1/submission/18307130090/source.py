import random
import sys
import time

import matplotlib.pyplot as plt
import numpy as np


class KNN:

    def __init__(self):
        self.train_data = None
        self.train_labels = None
        self.k = None

    # func=bf表示使用O(nlogn)的算法 func=opt表示使用O(n)的算法
    def get_predict_labels(self, k, train_data, train_labels, valid_data, func='bf'):

        # 确保func字段只能取bf或者opt
        assert func in {'bf', 'opt'}

        predict_labels = np.array([])
        for valid_dot in valid_data:

            # 计算每个train_dot与valid_dot之间的距离
            dist = np.linalg.norm(train_data - valid_dot, axis=1)

            # 计算距离最小的k个train_dot的下标
            dist_index = np.argsort(dist)[:k] if func == 'bf' else np.argpartition(dist, k)[:k]

            # 计算数量最多的标签
            count_dict = {}
            max_count = 0
            for index in dist_index:
                index = int(index)
                train_label = train_labels[index]
                count_dict[train_label] = count_dict.get(train_label, 0) + 1
                max_count = max(max_count, count_dict[train_label])
            predict_label = np.array([])
            for train_label, count in count_dict.items():
                if max_count != count: continue
                predict_label = np.append(predict_label, train_label)
            predict_labels = np.append(predict_labels, np.random.choice(predict_label))

        return predict_labels

    def fit(self, input_data, input_labels):
        self.train_data = input_data
        self.train_labels = input_labels

        # 将数据打乱
        shuffled_data, shuffled_labels = shuffle(input_data, input_labels)

        # 划分为训练集和验证集
        ratio, data_size = 0.2, shuffled_data.shape[0]
        valid_size = int(data_size * ratio)
        train_size = data_size - valid_size
        valid_data, valid_labels = shuffled_data[:valid_size], shuffled_labels[:valid_size]
        train_data, train_labels = shuffled_data[valid_size:], shuffled_labels[valid_size:]

        # 枚举k，求出最佳参数
        k_size = min(25, train_size)
        max_acc, best_k = -1, 0
        for k in range(1, k_size):
            predict_labels = self.get_predict_labels(k, train_data, train_labels, valid_data, func='opt')
            acc = np.mean(np.equal(predict_labels, valid_labels))
            # print(f'k={k} acc={acc}')
            if acc > max_acc:
                max_acc = acc
                best_k = k
        print(f'k={best_k} train_acc={max_acc}')
        self.k = best_k

    def predict(self, test_data):
        return self.get_predict_labels(self.k, self.train_data, self.train_labels, test_data, func='opt')


def generate_data(mean, cov, nums):
    n = len(mean)
    assert n == len(cov) and n == len(nums)
    data = np.concatenate([np.random.multivariate_normal(mean[i], cov[i], int(nums[i])) for i in range(n)])
    labels = np.concatenate([np.ones(int(nums[i]), dtype=int) * i for i in range(n)])

    data, labels = shuffle(data, labels)

    ratio, data_size = 0.2, len(data)
    test_size = int(ratio * data_size)
    test_data, test_label = data[:test_size], labels[:test_size]
    train_data, train_label = data[test_size:], labels[test_size:]
    np.save('data.npy', (train_data, train_label, test_data, test_label))


def shuffle(data, labels):
    data_size = len(data)
    assert data_size == len(labels)

    indices = np.random.permutation(data_size)
    return data[indices], labels[indices]


def save_plot(data, labels, name):
    data_size = len(data)
    assert data_size == len(labels)
    total = {}
    for i in range(data_size):
        label = labels[i]
        if label not in total:
            total[label] = []
        else:
            total[label].append(data[i])
    for category in total.values():
        if category == []: continue
        category = np.array(category)
        plt.scatter(category[:, 0], category[:, 1])
    plt.title(name)
    plt.savefig(f'./img/{name}')
    plt.close()


def read():
    return np.load('data.npy', allow_pickle=True)


def generate_control(nums, length):
    n = len(nums)
    labels = [i for i in range(n)]
    return random.choices(labels, nums, k=length)


def train(mean, cov, nums, generate, ratio=(1, 1, 1)):
    if generate:
        generate_data(mean * ratio[0], cov * ratio[1], nums * ratio[2])
    train_data, train_label, test_data, test_label = read()
    save_plot(train_data, train_label, 'train')
    save_plot(test_data, test_label, 'test')
    knn = KNN()
    start_time = time.time()
    knn.fit(train_data, train_label)
    end_time = time.time()
    training_time = end_time - start_time
    # print(f'training stime={training_time} s')
    ans = knn.predict(test_data)
    control_group = generate_control(nums, len(test_label))
    test_acc = np.mean(np.equal(ans, test_label))
    control = np.mean(np.equal(control_group, test_label))
    return test_acc, control


if __name__ == '__main__':
    nums = np.array([1600, 400, 2000], dtype=int)
    mean = np.array([[5, 5], [10, 15], [20, 5]])
    cov = np.array([
        [[34, 5], [5, 10]],
        [[20, 5], [5, 24]],
        [[30, 5], [5, 10]]
    ])
    generate = True if len(sys.argv) > 1 and sys.argv[1] == 'g' else False
    acc, control = train(mean, cov, nums, generate)
    print(f'acc={acc} control={control}')
    pass
