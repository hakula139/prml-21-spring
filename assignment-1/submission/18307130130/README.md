# Assignment-1 Report

<p align="right">------李睿琛 18307130130</p>
## 一、数据集的生成与分割

### **数据集生成**

`GenerateData`封装数据生成逻辑函数，修改参数数组，即可自定义分布特点。

> N：定义点的维度
>
> means：定义分布均值
>
> covs：定义分布协方差矩阵
>
> nums：定义分布包含的点的数目

```python
# define two-dimension Gaussian distribution
N = 2
means = [(1, 2), (1, 9), (4, 10), (9, 5), (7, 20)]
# define the number of each distribution
nums = [1000, 1000, 1000, 1000, 1000]
covs = [np.eye(N), np.eye(N), np.eye(N), np.eye(N), np.eye(N)]

def GenerateData(N, nums, means, covs):
    ...
	GetGaussionSet(N, nums[i], means[i], covs[i])
    # concatenate the data and corresponding label
    dataset[i] = np.concatenate((tmp, zeros), axis=1)
    ...
```

`GetGaussionSet`根据给定数组生成高斯分布：

```python
def GetGaussionSet(N, num, mean, cov):
    """
    N refer to number of dimensions, mean refer to mean of Gaussion,
    number refer to number of data
    """
    x = np.random.multivariate_normal(mean, cov, num, 'raise')
    return x
```

`GetGaussionSet`生成一个**以mean为均值，cov为协方差矩阵，包含num个点**的二维高斯分布的点集。

### **数据集分割**

```python
#  Randomly divide the data into 80% training set and 20% test set
np.random.shuffle(DataSet)
length = DataSet.shape[0]
train_len = round(length * 4 / 5)
train_data = DataSet[:train_len,:]
test_data = DataSet[train_len:,:]
```

打乱数据后，将数据随机划分为 80% 的训练集和 20% 的测试集。

## 二、KNN模型的拟合与预测

### 模型拟合

使用**交叉验证**确定合适**超参K**， 即把数据划分为训练集、验证集和测试集。一般的划分比例为6：2：2。 

对80%的训练集进一步按6:2:2划分为60%训练集，20%验证集。

k值一般偏小，所以**遍历**确定正确性最高对应的k值：

```python
v_ratio = 0.25
length = train_data.shape[0]
label_len = data_len = round(length * (1 - v_ratio) )
self.train_data = t_data = train_data[:data_len,:]
self.train_label = t_label = train_label[:label_len]
v_data = train_data[data_len:,:]
v_label = train_label[label_len:]

# find the k with highest accuracy
for k in range(1, 20):
    res = self._predict_by_k(t_data, t_label, v_data, k)
```
输出如下：
```
k : 1  acc:  0.984375
k : 2  acc:  0.9828125
...
k : 8  acc:  0.9890625
k : 9  acc:  0.9890625
k : 10  acc:  0.990625
k : 11  acc:  0.9890625
k : 12  acc:  0.990625
k : 13  acc:  0.990625
k : 14  acc:  0.9890625
...
k : 18  acc:  0.9890625
k : 19  acc:  0.9890625
select k:  13
```

即超参k设置为13时，在验证集上有最高准确性。

### **模型预测**

```python
diff = t_data - d
dist = (np.sum(diff ** self.p, axis=1))**(1 / self.p)
topk = [t_label[x] for x in np.argsort(dist)[:k]]
```

 使用闵可夫斯基距离作为衡量：

![img](https://img-blog.csdn.net/20170701211052055) 

```python
topk = [t_label[x] for x in np.argsort(dist)[:k]]
...
i = np.random.randint(len(top_cnt))
res.append(top_cnt[i])
```

根据距离从小到大排序，去前K个label，其中出现最频繁的即为预测结果。若有结果有多个，随机选取一个作为最终结果。

## 三、模型结果可视化

> N = 2
>
> means = [(1, 2), (1, 9), (4, 10), (9, 5), (7, 20)]
>
> nums = [1000, 1000, 1000, 1000, 1000]
>
> covs = [np.eye(N), np.eye(N), np.eye(N), np.eye(N), np.eye(N)]

K的选取：

```
k : 1  acc:  0.962
k : 2  acc:  0.964
k : 3  acc:  0.972
k : 4  acc:  0.971
k : 5  acc:  0.974
k : 6  acc:  0.971
k : 7  acc:  0.971
k : 8  acc:  0.97
k : 9  acc:  0.971
k : 10  acc:  0.971
k : 11  acc:  0.97
k : 12  acc:  0.971
k : 13  acc:  0.971
k : 14  acc:  0.969
k : 15  acc:  0.971
k : 16  acc:  0.972
k : 17  acc:  0.971
k : 18  acc:  0.971
k : 19  acc:  0.971
select k:  5
```

预测结果：左图可视化**训练集**分布，右图可视化**测试集**分布。星星符号标记了**预测错误**的点。

![1616737604559](img/6.png)

<center>图一</center>
## 四、实验探究

**设置对照组：**

>means = [(1, 2), (1, 9), (4, 10)]
>
>nums = [1000, 1000, 1000]
>
>covs = [np.eye(N), np.eye(N), np.eye(N)]

**acc =  0.9733333333333334**

## ![1616738677599](img/5.png)

### 分布的距离

从图一中已经可以看到，黄色和绿色点集由于距离较近，预测错误率明显大于其他点。

而黄的与红色距离较远，基本没有出现预测错误的情况。

### 分布的方差

修改方差为：

```python
covs = [np.array([ [5, 3], [3, 5] ]), np.eye(N), np.eye(N)]
```

**acc =  0.9583333333333334**

![1616739122313](img/1.png)

在二维高斯分布的协方差矩阵中：

**正对角线代表每个变量的方差**，方差越大，数据波动越大；

**副对角线代表不同变量之间的协方差**，协方差绝对值越大，变量相关性越大。

由图可见，`np.array([ [5, 3], [3, 5] ])`矩阵中方差为红色点集属性，方差为5，**数据波动，分布离散**；协方差为3，**数据分布呈现正相关**。这也导致数据混淆概率变大，预测准确率下降。

### 分布的数量

修改分布数量为：

```
nums = [1000, 5000, 1000]
```

![1616739948335](img/2.png)

再次修改分布数量为：

```
nums = [1000, 10000, 1000]
```

![1616740079147](img/3.png)

由于**绿色分布**数据量的增加，对于和**蓝色分布**重叠部分点的影响力增强，容易导致**过拟合**。

## 五、绘制KNN区域图

![1616741381345](img/4.png)

```python
h = .02
x_min, x_max = DataSet[:, 0].min() - 1, DataSet[:, 0].max() + 1
y_min, y_max = DataSet[:, 1].min() - 1, DataSet[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                np.arange(y_min, y_max, h))
```

**acc =  0.9775**

生成网格点坐标，对每个点进行预测并绘制对应颜色。可以看见在区域交界处，颜色会出现部分**抖动**。