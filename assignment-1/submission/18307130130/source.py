import numpy as np
import matplotlib.pyplot as plt

class KNN:

    def __init__(self, k=5, p=2):
        """
        When p = 1, this is equivalent to using manhattan_distance
        , and euclidean_distance for p = 2. 
        """
        self.k = 5
        self.p = p
        self.train_data = None
        self.train_label = None

    def fit(self, train_data, train_label):
    
        
        train_label = train_label.reshape(-1, 1)
        data = np.concatenate((train_data, train_label), axis=1)
        np.random.shuffle(data)

        train_data = data[:, 0: -1]
        train_label = data[:, -1]
        
        # cross-validation
        v_ratio = 0.25
        length = train_data.shape[0]
        label_len = data_len = round(length * (1 - v_ratio) )
        self.train_data = t_data = train_data[:data_len,:]
        self.train_label = t_label = train_label[:label_len]
        v_data = train_data[data_len:,:]
        v_label = train_label[label_len:]

        acc_dict = dict()
        for k in range(1, 20):
            res = self._predict_by_k(t_data, t_label, v_data, k)
            acc = np.mean(np.equal(res, v_label))
            print("k :", k, " acc: ", acc)
            acc_dict[k] = acc
        Max = 0
        select_k = 1
        for k, acc in acc_dict.items():
            if acc >= Max:
                select_k = k
                Max = acc
        self.k = select_k
        print("select k: ", select_k)

    def predict(self, test_data):
        res = self._predict_by_k(self.train_data, self.train_label, test_data, self.k)
        return np.array(res)

    def _predict_by_k(self, t_data, t_label, data, k):
        res = []
        for d in data:
            diff = t_data - d
            dist = (np.sum(diff ** self.p, axis=1))**(1 / self.p)
            topk = [t_label[x] for x in np.argsort(dist)[:k]]
            cnt_dict = dict()
            for x in topk:
                if x in cnt_dict:
                    cnt_dict[x] += 1
                else:
                    cnt_dict[x] = 1
            sort_cnt = sorted(cnt_dict.items(), key=lambda x: x[1], reverse=True)
            top_cnt = sort_cnt[0][0]
            Max_cnt = sort_cnt[0][1]
            for number, cnt in sort_cnt:
                if cnt >= Max_cnt:
                    top_cnt = number
                    Max_cnt = cnt
                else:
                    break
            res.append(top_cnt)
        return res
            
def GetGaussionSet(N, num, mean, cov):
    """
    N refer to number of dimensions, mean refer to mean of Gaussion,
    number refer to number of data
    """
    x = np.random.multivariate_normal(mean, cov, num, 'raise')
    return x

def GenerateData(N, nums, means, covs):
    """
    Generate data according to nums and means
    """
    dataset = dict() 
    for i in range(len(nums)):
        # get the number as label
        zeros = np.zeros((nums[i], 1)) + i
        tmp = GetGaussionSet(N, nums[i], means[i], covs[i])
        # concatenate the data and corresponding label
        dataset[i] = np.concatenate((tmp, zeros), axis=1)
    
    ret = dataset[0]
    for value in list(dataset.values())[1:]:
        ret = np.concatenate((ret, value), axis=0)

    return ret

def ShowFigure(dataset):
    global nums
    cmap = plt.cm.get_cmap("hsv", len(nums))

    h = .02
    x_min, x_max = DataSet[:, 0].min() - 1, DataSet[:, 0].max() + 1
    y_min, y_max = DataSet[:, 1].min() - 1, DataSet[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                        np.arange(y_min, y_max, h))

    model = KNN()
    model.fit(DataSet[:, [0, 1]], DataSet[:, 2])
    test_data = np.c_[xx.ravel(), yy.ravel()]
    Z = model.predict(test_data)
    Z = Z.reshape(xx.shape)

    plt.figure(12)

    plt.subplot(1, 2, 1)
    plt.pcolormesh(xx, yy, Z, cmap=cmap, shading="auto")

    ax = plt.subplot(1,2,2)   #界面只需显示一个视图
    ax.set_title('KNN separable data set')  #视图名称，这里简单统一定这个名称吧
    plt.xlabel('X')    #坐标轴名称
    plt.ylabel('Y')

    for i in range(len(nums)):
        idx = np.where(dataset[:, 2] == i)
        ax.scatter(dataset[idx,0], dataset[idx,1], marker='o', label=i, color=cmap(i), s=10)
    #plt.scatter(dataset[:,0], dataset[:,1], marker='o', c=dataset[:,2], cmap=cmap, s=10)

    plt.legend(loc = 'upper right')   #图例显示位置
    plt.show()

def ShowAcc(train_data, test_data, Z):
    global nums
    cmap = plt.cm.get_cmap("hsv", len(nums)+1)
    plt.figure(12)
    ax = plt.subplot(1, 2, 1)

    ax.set_title('KNN train dataset') 
    plt.xlabel('X')    #坐标轴名称
    plt.ylabel('Y')

    for i in range(len(nums)):
        idx = np.where(train_data[:, 2] == i)
        ax.scatter(train_data[idx, 0], train_data[idx, 1], marker='o', label=i, color=cmap(i), s=10)
    
    plt.legend(loc = 'upper right')   #图例显示位置
    
    ax = plt.subplot(1, 2, 2)
    ax.set_title('KNN test dataset') 
    plt.xlabel('X')    #坐标轴名称
    plt.ylabel('Y')
    for i in range(len(nums)):    
        idx = np.where(test_data[:, 2] == i)
        ax.scatter(test_data[idx,0], test_data[idx,1], marker='o', label=i, color=cmap(i), s=10)

    wrong_point = []
    for i in range(test_data.shape[0]):
        if test_data[i][2] != Z[i]:
            wrong_point.append([test_data[i][0], test_data[i][1]])
    if wrong_point != []:
        wrong_point = np.array(wrong_point)
        ax.scatter(wrong_point[:,0], wrong_point[:,1], marker='*', s=30)

    plt.legend(loc = 'upper right')   #图例显示位置
    plt.show()

if __name__ == "__main__":
    N = 2
    # define two-dimension Gaussian distribution
    means = [(1, 2), (1, 9), (4, 10), (9, 5), (7, 20)]
    # define the number of each distribution
    nums = [400, 400, 400, 400, 400]
    covs = [np.eye(N), np.eye(N), np.eye(N), np.eye(N), np.eye(N)]

    # Generate DataSet according to N, nums, and means
    DataSet = GenerateData(N, nums, means, covs)

    #  Randomly divide the data into 80% training set and 20% test set
    np.random.shuffle(DataSet)
    length = DataSet.shape[0]
    train_len = round(length * 4 / 5)
    train_data = DataSet[:train_len,:]
    test_data = DataSet[train_len:,:]

    # start training and predict
    model = KNN()
    model.fit(train_data[:, [0, 1]], train_data[:, 2])
    Z = model.predict(test_data[:, [0, 1]])

    # calculate the accuracy
    print("acc = ", np.mean(np.equal(Z, test_data[:, 2])))

    model.train_label = model.train_label.reshape(-1, 1)
    train_data = np.concatenate((model.train_data, model.train_label), axis=1)
    # visualize the accuracy of KNN
    ShowAcc(train_data, test_data, Z)

    # visualize the meshgraph of KNN
    ShowFigure(DataSet)