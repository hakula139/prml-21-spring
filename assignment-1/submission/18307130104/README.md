

18307130104

# 课程报告

这是 prml 的 assignment-1 课程报告，我的代码在 [source.py](./source.py) 中。

在 assignment-1 中，用 python 实现了一个 KNN 类，调用该类下 fit() 函数可以对数据进行训练，调用 predict() 可以对多组目标数据进行预测，返回一个 list 对应每组数据的结果。

## KNN 类实现

### fit() 函数

fit() 函数的任务有两个：将用于训练的点按照类别分别进行储存；选择合适的 K，也就是用于预测的最近点个数。

接下来说明 K 数值的选择方法。对于输入的数据，选择前 $\frac 3 4$ 的数据作为训练集，后 $\frac 1 4$ 的数据作为验证集。逐一尝试 1～14 作为 K 值时模型在验证集上的正确率，选取其中正确率最高的 K 作为模型的 K 值保存下来。

选择 1~14 是因为训练数据的规模为 2000，如果训练数据的规模进行了修改，这一个范围也可以进行修改，不过这一范围对大部分数据规模都比较适用。

### predict() 函数

predict() 函数会根据模型中存储的数据和选定的 K 对给定数据进行预测。

采用欧拉距离作为两个点的距离数值，选择距离最近的 K 个点进行投票，获票最多的类别就是预测结果。对于获票相同的情况选择编号比较小的类别。

## 测试与展示

```shell
python source.py g // 生成数据
python source.py   // 进行测试
python source.py d // 生成展示图片
```

generate() 和 display() 函数均从示例代码中获得。其中 display() 函数中增加了对某种类别预测结果为空的判断防止报错。

> 需要保证运行环境中有 img 文件夹，否则程序无法正确运行。（由于不能用 os 包所以不知道怎么判断是否存在文件夹）
>
> 另外，如果使用 wsl 等环境会导致输出图像有重叠。

## 探究性实验

## 实验1

采用以下参数生成 3 组数据。
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
73 & 0 \\\\
0 & 22
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
1 & 2
\end{array}\right]
\end{array}
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
21.2 & 0 \\\\
0 & 32.1
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
16 & -5
\end{array}\right]
\end{array}
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 5 \\\\
5 & 10
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
10 & 22
\end{array}\right]
\end{array}
$$

训练数据，测试数据，测试结果如下三图

<img src='./img/train-1.png' width="33%" alt="训练集"></img><img src='./img/test-1.png' width="33%" alt="测试集"></img><img src='./img/test-1.png' width="33%" alt="测试结果"></img>

程序输出如下（之后的实验输出均采用如下的输出格式）

| K      | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    | 10   | 11   | 12   | 13   | 14   |
| ------ | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 正确率 | 0.93 | 0.92 | 0.93 | 0.94 | 0.94 | 0.95 | 0.95 | 0.95 | 0.95 | 0.95 | 0.96 | 0.96 | 0.96 | 0.96 |

| 选取 K | 正确率 |
| -- | -- |
| 14     | 0.96 |

将实验1作为基准，对不同数据集上的模型效果进行对比。这组数据的特点在于虽然不同种类之间的点有交集，但是区分仍然非常明显，比较符合实际中的分类问题的特征。

## 实验2

采用以下参数生成 3 组数据。
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
6 & 0 \\\\
0 & 4
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
1 & 2
\end{array}\right]
\end{array}
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
5 & 0 \\\\
0 & 7
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
16 & -5
\end{array}\right]
\end{array}
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
9 & 5 \\\\
0 & 5
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
10 & 22
\end{array}\right]
\end{array}
$$

训练数据，测试数据，测试结果如下三图

<img src='./img/train-2.png' width="33%" alt="训练集"></img><img src='./img/test-2.png' width="33%" alt="测试集"></img><img src='./img/test-2.png' width="33%" alt="测试结果"></img>

程序输出如下

| K      | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    | 10   | 11   | 12   | 13   | 14   |
| ------ | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 正确率 | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  | 1.0  |


| 选取 K | 正确率 |
| -- | -- |
|1|acc = 1.0|

实验2的数据集中数据的协方差比较小，对应的，数据比较集中，数据集中区域的交叉比较小，所以对应的，模型的准确度非常高，这种情况的分类非常简单，因此模型表现优秀也在预期之中。

## 实验3

采用以下参数生成 3 组数据。
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
73 & 0 \\\\
0 & 22
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
13 & -2
\end{array}\right]
\end{array}
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
21.2 & 0 \\\\
0 & 32.1
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
16 & -5
\end{array}\right]
\end{array}
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 5 \\\\
5 & 10
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
18 & -7
\end{array}\right]
\end{array}
$$

训练数据，测试数据，测试结果如下三图

<img src='./img/train-3.png' width="33%" alt="训练集"></img><img src='./img/test-3.png' width="33%" alt="测试集"></img><img src='./img/test-3.png' width="33%" alt="测试结果"></img>

程序输出如下

| K      | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    | 10   | 11   | 12   | 13   | 14   |
| ------ | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 正确率 | 0.67 | 0.76 | 0.73 | 0.75 | 0.73 | 0.74 | 0.74 | 0.76 | 0.74 | 0.75 | 0.74 | 0.74 | 0.74 | 0.74 |

| 选取 K | 正确率 |
| -- | -- |
|2|acc = 0.71|

相比于实验1，虽然数据的协方差没有变化，但是数据的中心点比较靠近，具体表现出来，数据集中区域的重合部分非常大，非常难以区别。可以看到正确率也有非常大幅度的下滑。

如果再加大协方差，测试准确率也会进一步下降。

## 结论

可以看到对于数据集中不同类别区分度比较大的情况，KNN 有着非常优秀的表现。对于数据重叠情况比较大的情况，KNN 的效果也并不理想。
