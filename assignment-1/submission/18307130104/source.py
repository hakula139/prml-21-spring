import sys
import numpy as np
import matplotlib.pyplot as plt

class KNN:

    def __init__(self):
        self.ldata = {}
        self.K = 10
        self.cnt = 0

    def fit(self, train_data, train_label):
        totsz = len(train_data)
        pretrainsz = totsz * 3 // 4
        for i in range(0, pretrainsz):
            if train_label[i] in self.ldata:
                self.ldata[train_label[i]].append(train_data[i])
            else:
                self.ldata[train_label[i]] = [train_data[i]]
        pretraindata = train_data[pretrainsz : totsz]
        pretrainlabel = train_label[pretrainsz : totsz]
        maxAcc = 0
        takeK = 3
        for preK in range(1, 15):
            pretrainres = []
            self.K = preK
            for d in pretraindata:
                pretrainres.append(self.predict_one(d))
            acc = np.mean(np.equal(pretrainres, pretrainlabel))
            print(acc)
            if acc > maxAcc:
                maxAcc = acc
                takeK = preK
        self.K = takeK
        print("take K", takeK)
        self.ldata.clear()
        for (d, l) in zip(train_data, train_label):
            if(l in self.ldata):
                self.ldata[l].append(d)
            else:
                self.ldata[l] = [d]

    def dist(self, s1, s2):
        sum = 0
        for (k1, k2) in zip(s1, s2):
            sum += (k1 - k2) ** 2
        return sum
    def takeFirst(self, elem):
        return elem[0]
    def predict_one(self, data):
        result = None
        tmpl = []
        for l in self.ldata:
            for s in self.ldata[l]:
                tmpl.append([self.dist(s, data), l])
        tmpl.sort(key=self.takeFirst)
        num = {}
        for i in self.ldata:
            num[i] = 0
        cnt = 0
        # for l in tmpl:
        #     print(l)
        # print(' ')
        for l in tmpl:
            num[l[1]] += 1
            cnt += 1
            if(cnt >= self.K):
                break
        maxi = -1
        for i in self.ldata:
            # print(i)
            if num[i] > maxi:
                maxi = num[i]
                result = i
        # print(result)
        return result
    
    def predict(self, test_data):
        result = []
        for x in test_data:
            result.append(self.predict_one(x))
        return result

def generate():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))  
    
    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))  

    mean = (10, 22)
    cov = np.array([[10,5],[5,10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))   

    idx = np.arange(2000)
    np.random.shuffle(idx)
    data = np.concatenate([x,y,z])
    label = np.concatenate([
        np.zeros((800,),dtype=int),
        np.ones((200,),dtype=int),
        np.ones((1000,),dtype=int)*2
    ])
    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:1600,], data[1600:,]
    train_label, test_label = label[:1600,], label[1600:,]
    np.save("data.npy",(
        (train_data, train_label), (test_data, test_label)
    ))

def display(data, label, name):
    datas =[[],[],[]]
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    
    for each in datas:
        each = np.array(each)
        if(each.size > 0):
            plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "g":
        print("generate")
        generate()
    if len(sys.argv) > 1 and sys.argv[1] == "d":      
        (train_data, train_label), (test_data, test_label) = read()
        # for l in test_label:
        #     print(l)
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        model.fit(train_data, train_label)
        res = model.predict(test_data)
        display(test_data, res, 'test_res')
        print("acc =",np.mean(np.equal(res, test_label)))