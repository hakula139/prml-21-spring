# 课程报告

## 说明

我使用的包为numpy,在class KNN中：


a.使用函数euclidean进行向量间欧式距离的计算


b.使用closest函数进行逐个向量输入，分别计算它与全部train data的欧氏距离，并输出距它最近k个点出现次数最多train label。当最近k个点不存在出现次数最多train label（如出现次数均等），将进行label随机输出


c.使用predict函数将全部test data逐个输入，得到预测结果


d.使用choose函数，将预测结果与test label进行比对，结果相同取值为1，不同为0，进行准确率计算。k值选择范围根据训练与测试集数量决定（最小值为2，最大值为数据量的10%），从中选取使预测结果准确率最高k值，并输出对准确率预测


## 数据生成 实验探究

我使用以下参数生成了如下三个二维高斯分布，label分别为0，1，2


        label=0
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 0 \\\\
0 & 10
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
20 & 25
\end{array}\right]
\end{array}
$$


        label=1
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
23 & 0 \\\\
0 & 22
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
16 & -5
\end{array}\right]
\end{array}
$$


        label=2
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 5 \\\\
5 & 10
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
20 & 25
\end{array}\right]
\end{array}
$$

这是我生成的训练集：


<img src='https://images.gitee.com/uploads/images/2021/0323/220231_8ac75bbc_8850706.png' width="300" alt="训练集"></img>


这是我生成的测试集：


<img src='https://images.gitee.com/uploads/images/2021/0323/220830_c4caaa49_8850706.png' width="300" alt="测试集"></img>


可以通过如下表格来报告我的实验结果

Algo |kvalue|Acc    |
-----| ---- |----   |
KNN  | 5    |0.6225 |




由于label=0和label=2的对应高斯分布较靠近，导致训练准确性为62.25%。


为进一步探究高斯分布距离对预测准确性影响，我使用如下参数进行分布生成：

        label=0
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 2.1 \\\\
2.1 & 12
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
20 & 25
\end{array}\right]
\end{array}
$$


        label=1
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
23 & 0 \\\\
0 & 22
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
20 & 25
\end{array}\right]
\end{array}
$$


        label=2
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 5 \\\\
5 & 10
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
20 & 25
\end{array}\right]
\end{array}
$$

这是我生成的训练集：


<img src='https://images.gitee.com/uploads/images/2021/0324/211752_41180436_8850706.png' width="300" alt="训练集"></img>

这是我生成的测试集：


<img src='https://images.gitee.com/uploads/images/2021/0324/211809_decc954a_8850706.png' width="300" alt="测试集"></img>


可以通过如下表格来报告我的实验结果

Algo |kvalue|Acc    |
-----| ---- |----   |
KNN  | 12   |0.485  |

此时3个高斯分布距离彼此都很近，进行不同k值选取，实验的准确性最高达到48.5%。

|k    |Acc     |
----- | ----   |
| 2   | 0.4525 |
| 3   | 0.4375 |
| 4   | 0.4475 |
| 5   | 0.4300 |
| 6   | 0.4675 |
| 7   | 0.4525 |
| 8   | 0.4775 |
| 9   | 0.4450 |
| 10  | 0.4650 |
| 11  | 0.4700 |
| 12  | 0.4850 |
| 13  | 0.4750 |
| 14  | 0.4650 |
| 15  | 0.4625 |
| 16  | 0.4775 |
| 17  | 0.4650 |
| 18  | 0.4800 |
| 19  | 0.4700 |
| 20  | 0.4725 |


改变高斯分布距离，我使用以下参数生成高斯分布。


        label=0
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 2.1 \\\\
2.1 & 12
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
20 & 25
\end{array}\right]
\end{array}
$$


        label=1
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
23 & 0 \\\\
0 & 22
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
16 & -5
\end{array}\right]
\end{array}
$$


        label=2
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 5 \\\\
5 & 10
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
3 & 5
\end{array}\right]
\end{array}
$$

这是我生成的训练集：


<img src='https://images.gitee.com/uploads/images/2021/0324/101631_7ff0453f_8850706.png' width="300" alt="训练集"></img>


这是我生成的测试集：


<img src='https://images.gitee.com/uploads/images/2021/0324/101657_ceefbdc0_8850706.png' width="300" alt="测试集"></img>


可以通过如下表格来报告我的实验结果

Algo |kvalue|Acc    |
-----| ---- |----   |
KNN  | 2    |0.9975 |


此时3个高斯分布距离较远，通过较少的k值即可得到较为准确的判断。增加高斯分布间的距离可以提升实验的准确性。

## 代码使用方法

```bash
改变mode数值：
mode=0         #数据生成
mode=1         #数据可视化
mode取非0-1值   #训练和测试
```
