import matplotlib.pyplot as plt
import numpy as np

class KNN():
    def euclidean(self,v1,v2):
       return np.sqrt(np.sum(np.square(v1 - v2)))
    def fit(self, X_train, Y_train):
        self.train_data = train_data
        self.train_label = train_label
    def predict(self, train_data,k):
        predictions = []
        for item in train_data:
            label = self.closest(item,k)
            predictions.append(label)
        return predictions

    def closest(self, item,k):
        min_ind = 0
        distlst=[]
        idxlst=list(range(len(self.train_data)))
        #get distance between test_data with train_data
        for i in range(0,len(self.train_data)):
            distlst.append(self.euclidean(item, self.train_data[i]))
        #make up a dictionary with distance and index
        distdict=dict(zip(idxlst,distlst))
        distdict=dict(sorted(distdict.items(),key=lambda item:item[1]))
        #get first K nearest position
        min_ind=list(dict(list(distdict.items())[:k]).keys())
        min_dist=[self.train_label[i] for i in min_ind]
        return max(min_dist,key=min_dist.count)
 
    def choose(self,test_data,test_label):
        acclst=[]
        for k in range(2,7):
            res=self.predict(test_data,k)
            acc=np.mean(np.equal(res, test_label))
            acclst.append(acc)
        max_acc=max(acclst)
        max_k=acclst.index(max_acc)+2
        return max_k,max_acc
        

def generate():
    mean = (20, 25)
    cov = np.array([[10,2.1], [2.1, 12]])
    x = np.random.multivariate_normal(mean, cov, (800,))  
    
    mean = (16, -5)
    cov = np.array([[23, 0], [0, 22]])
    y = np.random.multivariate_normal(mean, cov, (200,))  

    mean = (3, 5)
    cov = np.array([[10,5],[5,10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))  

    idx = np.arange(2000)
    np.random.shuffle(idx)
    data = np.concatenate([x,y,z])
    label = np.concatenate([
        np.zeros((800,),dtype=int),
        np.ones((200,),dtype=int),
        np.ones((1000,),dtype=int)*2
    ])
    data = data[idx]
    label = label[idx]

    train_data, test_data = data[:1600,], data[1600:,]
    train_label, test_label = label[:1600,], label[1600:,]
    np.save("data.npy",((train_data, train_label), (test_data, test_label)
    ))
    
def display(data, label, name):
    datas =[[],[],[]]
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    
    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    label=[str(i) for i in list(range(len(datas)))]
    plt.legend(['label '+i for i in label])
    plt.show()

def read():
    (train_data, train_label), (test_data, test_label) = np.load("data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)


if __name__ == "__main__":
    mode=0
    if mode == 0:      
        generate()     
    if mode == 1:
        (train_data, train_label), (test_data, test_label) = read()
        display(train_data, train_label, 'train')
        display(test_data, test_label, 'test')
    else:
        (train_data, train_label), (test_data, test_label) = read()

        model = KNN()
        model.fit(train_data, train_label)
        k ,acc = model.choose(test_data,test_label)
        print("k=",k,"acc=",acc*100,"%")