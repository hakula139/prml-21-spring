import sys
import numpy as np
import matplotlib.pyplot as plt

class KNN:
    def __init__(self, k=5, weights=0, norm='N', dist='euc', cv_records=False, verbose=False):
        # init models' hyperparameters
        self.k = k
        self.weights = weights
        self.norm = norm
        self.dist = dist
        self.cv_records = cv_records
        self.verbose = verbose


    def fit(self, train_data, train_label):
        # check data input
        assert train_data.shape[0] == train_label.shape[0]
        assert train_data.shape[1]

        # init train data and train labels
        self.train_data = train_data
        self.train_label = train_label

        # search the best hyperparameters set and optimize the model if requested
        # hyperparameters which can be optimized (k, weights, norm, dist)
        best_hyperparam = self.grid_search_cv()

        # modify the knn model's hyperparameters 
        if best_hyperparam:
            for hp in best_hyperparam:
                setattr(self, hp[0], hp[1])


    def predict(self, test_data):
        train_data_indices = np.arange(self.train_data.shape[0])
        return self.predict_cv(test_data, train_data_indices, self.k, self.weights, self.norm, self.dist)


    def grid_search_cv(self, cv=10):
        best_hyperparam = []

        # construct hyperparams for grid search
        hyperparams = dict()
        if self.k == 0:
            k_up_bound = 20
            if self.train_data.shape[0] < 20:
                k_up_bound = self.train_data.shape[0]
            hyperparams['k'] = [k for k in range(1, k_up_bound)]
        if self.norm == 'auto':
            hyperparams['norm'] = ['N', 'min_max', 'standard']
        if self.weights == -1:
            hyperparams['weights'] = [0, 0.1, 0.2, 0.5, 1, 2]
        if self.dist == 'auto':
            hyperparams['dist'] = ['euc', 'manhattan']

        # return if optimize is not requested
        if not hyperparams:
            return best_hyperparam 

        # construct parameters conmbination
        p_names, p_values = zip(*sorted(hyperparams.items()))
        hps = [tuple(hp) for hp in p_values]
        combns = [[]]
        for hp in hps:
            combns = [x + [y] for x in combns for y in hp]

        # grid search for the best hyperparameters
        accuracy_scores = []
        for comb in combns:
            params = dict(zip(p_names, comb))
            # cross validate to evaluate model
            score = self.cross_validation(cv, params)
            accuracy_scores.append(score)

        best_comb = sorted(zip(combns, accuracy_scores), key=lambda x: x[1], reverse=True)[0]
        best_hyperparam = list(zip(p_names, best_comb[0]))
        return best_hyperparam

          
    def cross_validation(self, cv, hyperparam):
        # shuffle the train data
        shuffled_indexs = np.random.permutation(self.train_data.shape[0])

        # k-fold split train data 
        kfolds = np.array_split(shuffled_indexs, cv)
        kfolds = [f for f in kfolds if len(f) != 0]
        
        accuracy_score = 0
        for i in range(len(kfolds)):
            val_data = self.train_data[kfolds[i]]
            val_label = self.train_label[kfolds[i]]
            train_data_indices = np.concatenate(kfolds[:i] + kfolds[i+1:]).flatten()

            k = hyperparam.get('k', self.k)
            weights = hyperparam.get('weights', self.weights)
            norm = hyperparam.get('norm', self.norm)
            dist = hyperparam.get('dist', self.dist)
            predict_label = self.predict_cv(val_data, train_data_indices, k, weights, norm, dist)

            accuracy_score += np.mean(np.equal(predict_label, val_label))

        return accuracy_score / len(kfolds)
        

    def predict_cv(self, test_data, train_data_indices, k, weights, norm, dist):
        # normalize data 
        train_data = self.train_data[train_data_indices]
        train_label = self.train_label[train_data_indices]
        normparams = self.calc_normparams(train_data, norm)
        norm_train_data, norm_test_data = self.normalize_data(train_data, test_data, norm, normparams)

        # find k nearest neighbors
        predict_labels = []
        nn_labels_list, nn_distances_list = self.get_nearest_neighbors(norm_test_data, norm_train_data, train_label, k, dist)
        
        if (nn_labels_list >= 0).all() and not weights:
            for labels in nn_labels_list:
                pred = np.bincount(labels).argmax()
                predict_labels.append(pred)
        else:
            d_weights = np.exp(-weights * nn_distances_list ** 2)
            for i in range(test_data.shape[0]):
                votes = {}
                labels = nn_labels_list[i]
                for j in range(k):
                    l = labels[j]
                    if l in votes:
                        votes[l] += d_weights[i][j]
                    else:
                        votes[l] = d_weights[i][j]
                pred = sorted(votes.items(), key=lambda x: x[1], reverse=True)[0][0]
                predict_labels.append(pred)

        return np.array(predict_labels)
        
    
    def get_nearest_neighbors(self, test_data, train_data, train_label, k, dist):
        # calc distances 
        distances_list = np.vstack([self.get_distance(d, train_data, dist) for d in test_data])

        # find the k nearest neighbors
        nn_indices_list = np.argsort(distances_list, axis=-1)[:, :k]
        nn_labels_list = train_label[nn_indices_list]
        nn_distances_list = np.sort(distances_list, axis=-1)[:, :k]

        return nn_labels_list, nn_distances_list


    def get_distance(self, x1, x2, dist='euc'):
        if dist == 'manhattan':
            distance = np.sum(np.absolute(x1 - x2), axis=-1)
        else: # default: euclidean distance
            distance = np.sqrt(np.sum((x1 - x2)**2, axis=-1))
        return distance


    def normalize_data(self, train_data, test_data, norm, normparams):
        if norm == 'min_max':
            norm_train_data = (train_data - normparams['f_min']) / normparams['denom']
            norm_test_data = (test_data - normparams['f_min']) / normparams['denom']
        elif norm == 'standard': 
            norm_train_data = (train_data - normparams['mean']) / normparams['sigma']
            norm_test_data = (test_data - normparams['mean']) / normparams['sigma']
        else: # default no normalization
            norm_train_data = train_data
            norm_test_data = test_data
        return norm_train_data, norm_test_data


    def calc_normparams(self, data, norm):
        params = dict()
        if norm == 'min_max':
            feature_max = data.max(axis=0)
            feature_min = data.min(axis=0)
            denom = feature_max - feature_min
            params['f_min'] = feature_min
            params['denom'] = denom
        if norm == 'standard':
            mean = np.mean(data, axis=0)
            sigma = np.std(data, axis=0)
            params['mean'] = mean
            params['sigma'] = sigma
        return params

"""
------------------ below is code for experiement ------------------
"""

def get_KLdiv(mean_1, cov_1, mean_2, cov_2):
    assert len(mean_1) == len(mean_2)
    num_dims = len(mean_1)
    mu1 = np.array(mean_1)
    mu2 = np.array(mean_2)
    cov2_inv = np.linalg.inv(cov_2)

    logd = np.log(np.linalg.det(cov_2) / np.linalg.det(cov_1))
    trace_cov = np.trace(np.matmul(cov2_inv, cov_1))
    mean_cov = (mu2 - mu1).T.dot(cov2_inv).dot((mu2 - mu1))
    kldiv = 1/2 * (logd + trace_cov + mean_cov - num_dims)
    return kldiv

def guassian_kernel(source, target, kernel_mul=2.0, kernel_num=5, fix_sigma=None):
    import torch
    n_samples = int(source.size()[0])+int(target.size()[0])
    total = torch.cat([source, target], dim=0)
    total0 = total.unsqueeze(0).expand(int(total.size(0)), int(total.size(0)), int(total.size(1)))
    total1 = total.unsqueeze(1).expand(int(total.size(0)), int(total.size(0)), int(total.size(1)))
    L2_distance = ((total0-total1)**2).sum(2) 
    if fix_sigma:
        bandwidth = fix_sigma
    else:
        bandwidth = torch.sum(L2_distance.data) / (n_samples**2-n_samples)
    bandwidth /= kernel_mul ** (kernel_num // 2)
    bandwidth_list = [bandwidth * (kernel_mul**i) for i in range(kernel_num)]
    kernel_val = [torch.exp(-L2_distance / bandwidth_temp) for bandwidth_temp in bandwidth_list]
    return sum(kernel_val)

def mmd(source, target, kernel_mul=2.0, kernel_num=5, fix_sigma=None):
    import torch
    batch_size = int(source.size()[0])
    kernels = guassian_kernel(source, target,
        kernel_mul=kernel_mul, kernel_num=kernel_num, fix_sigma=fix_sigma)
    XX = kernels[:batch_size, :batch_size]
    YY = kernels[batch_size:, batch_size:]
    XY = kernels[:batch_size, batch_size:]
    YX = kernels[batch_size:, :batch_size]
    loss = torch.mean(XX + YY - XY -YX)
    return loss

def wasserstein_distance(x, y):
    from scipy.spatial.distance import cdist
    from scipy.optimize import linear_sum_assignment
    d = cdist(x, y)
    assignment = linear_sum_assignment(d)
    return d[assignment].sum() / len(x) 

def maximum_mean_discrepancy(x, y):
    import torch
    from torch.autograd import Variable
    X = Variable(torch.Tensor(x))
    Y = Variable(torch.Tensor(y))
    return mmd(X,Y).item()

def cluster_data(means, covs, n_data):
    ds = []
    ls = []
    for i in range(len(means)):
        d = np.random.multivariate_normal(means[i], covs[i], n_data[i])
        ds.append(d)
        ls.append(np.ones((n_data[i],), dtype=int) * i)
    return ds, ls

def combine_all_data(ds, ls):
    data = np.concatenate(ds)
    lable = np.concatenate(ls)
    return data, lable

def generate_data(data, label, rate=0.2):
    idx = np.arange(len(data))
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    split = int(len(data) * (1 - rate))
    train_data, test_data = data[:split,], data[split:,]
    train_label, test_label = label[:split,], label[split:,]
    return (train_data, train_label), (test_data, test_label)

def run_lab(means, covs, n_data, ks, weights, norm, dist):
    ds, ls = cluster_data(means, covs, n_data)
    data, label = combine_all_data(ds, ls)
    (train_data, train_label), (test_data, test_label) = generate_data(data, label)
    models = []
    for k in ks:
        model = dict()
        knn = KNN(k, weights, norm, dist)
        knn.fit(train_data, train_label) 
        predict_label =  knn.predict(test_data)
        acc = np.mean(np.equal(predict_label, test_label)) 
        model['model'] = knn
        model['acc'] = acc 
        models.append(model)
    return np.array(models), ds, data, label

def load_lab_config(lab_num):
    l_config = [
        {   # 0
            'means': [ [1, 50], [15, 10], [10, 20] ],
            'covs':  [
                [ [1,   0], [0, 10] ],
                [ [10,  15], [15, 40] ],
                [ [20, 0], [0, 30] ]
            ],
            'n_data': [ 400, 400, 400 ],
            'ks': [1, 3, 5, 7, 9, 15, 20], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 1
            'means': [ [1, 20], [2, 20], [2, 25] ],
            'covs':  [
                [ [1,   0], [0, 10] ],
                [ [10,  15], [15, 40] ],
                [ [20, 0], [0, 30] ]
            ],
            'n_data': [ 400, 400, 400 ],
            'ks': [1, 3, 5, 7, 9, 15, 20], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 2
            'means': [ [1, 50], [15, 10], [10, 20] ],
            'covs':  [
                [ [20,   0], [0, 40] ],
                [ [40,  15], [15, 80] ],
                [ [30, 0], [0, 50] ]
            ],
            'n_data': [ 400, 400, 400 ],
            'ks': [1, 3, 5, 7, 9, 15, 20], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 3
            'means': [ [1, 50], [15, 10], [10, 20], [25, 25], [40, 5] ],
            'covs':  [
                [ [1,   0], [0, 10] ],
                [ [10,  15], [15, 40] ],
                [ [20, 0], [0, 30] ],
                [ [10, 0], [0, 10] ],
                [ [10, 0], [0, 10] ]
            ],
            'n_data': [ 240, 240, 240, 240, 240 ],
            'ks': [1, 3, 5, 7, 9, 15, 20], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 4
            'means': [ ], 'covs':  [ ], 'n_data': [ 400, 400, 400 ],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 5
            'means': [ ], 'covs':  [ ], 'n_data': [ 600, 400, 200 ],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 6
            'means': [ ], 'covs':  [ ], 'n_data': [ 800, 200, 200 ],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 7
            'means': [ ], 'covs':  [ ], 'n_data': [ 900, 200, 100 ],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 8
            'means': [ ], 'covs':  [ ], 'n_data': [240,240,240,240,240],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 9
            'means': [ ], 'covs':  [ ], 'n_data': [400,300,250,125,125], 
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 10
            'means': [ ], 'covs':  [ ], 'n_data': [500,300,200,100,100],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 11
            'means': [ ], 'covs':  [ ], 'n_data': [600,400,100,70,30],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 12 
            'means': [ ], 'covs':  [ ], 'n_data': [170,170,170,170,170,175,175], 
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 13 
            'means': [ ], 'covs':  [ ], 'n_data': [300,200,200,150,150,150,50],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 14 
            'means': [ ], 'covs':  [ ], 'n_data': [400,300,200,100,100,80,20], 
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 15 
            'means': [ ], 'covs':  [ ], 'n_data': [500,400,100,100,70,20,10],
            'ks': [1, 3, 5, 7, 9, 13, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 16 
            'means': [ [1, 40], [5, 30], [10, 20] ],
            'covs':  [
                [ [20,   0], [0, 1500] ],
                [ [2,  0], [0, 1000] ],
                [ [5, 0], [0, 500] ]
            ],
            'n_data': [ 400, 400, 400 ],
            'ks': [1, 3, 5, 7, 9, 15, 20], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 17
            'means': [ [1, 30], [2, 30] ],
            'covs':  [
                [ [1,   0], [0, 10] ],
                [ [1,   0], [0, 10] ]
            ],
            'n_data': [ 100, 100 ],
            'ks': [1, 3, 5, 7, 9, 13, 15, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 18
            'means': [ [1, 30], [5, 20], [15, 15] ],
            'covs':  [
                [ [1,  0], [0, 10] ],
                [ [10, 15], [15, 40] ],
                [ [20, 0], [0, 30] ]
            ],
            'n_data': [ 100, 100, 100 ],
            'ks': [1, 3, 5, 7, 9, 13, 15, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 19 
            'means': [ [1, 20], [5, 20], [20, 30], [30, 25], [25, 25] ],
            'covs':  [
                [ [1,   0], [0, 10] ],
                [ [10,  15], [15, 40] ],
                [ [20, 0], [0, 30] ],
                [ [10, 0], [0, 30] ],
                [ [2, 5], [5, 50] ]
            ],
            'n_data': [ 100, 100, 100, 100, 100 ],
            'ks': [1, 3, 5, 7, 9, 13, 15, 17, 19], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 20 
            'means': [ ], 'covs':  [ ], 'n_data': [ 1000, 1000 ],
            'ks': [1, 3, 5, 10, 20, 50], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        },
        {   # 21 
            'means': [ ], 'covs':  [ ], 'n_data': [ 1000, 1000 ],
            'ks': [1, 3, 5, 10, 20, 50], 'weights': 0, 'norm': 'N', 'dist': 'euc'
        }
    ]
    n_data = l_config[lab_num]['n_data']
    means = l_config[lab_num]['means']
    covs = l_config[lab_num]['covs']
    cluster = len(n_data)
    if len(means) == 0: # random set means
        means = []
        for i in range(cluster):
            mean = np.random.uniform(-50, 50, 2)
            means.append(mean)
    if len(covs) == 0: # random set covs
        covs = []
        for i in range(cluster):
            cov = np.zeros((2, 2))
            cov[0, 0] = np.random.uniform(0, 100, 1)
            cov[1, 1] = np.random.uniform(0, 100, 1)
            x = np.sqrt(cov[0, 0] * cov[1, 1])
            cov[1, 0] = cov[0, 1]
            covs.append(cov)
    ks = l_config[lab_num]['ks']
    weights = l_config[lab_num]['weights']
    norm = l_config[lab_num]['norm']
    dist = l_config[lab_num]['dist']
    return means, covs, n_data, ks, weights, norm, dist

def plot_data(data, labels, title='data', save=False, 
colours=['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:grey', 'tab:olive', 'tab:cyan']):
    assert data.shape[0] == labels.shape[0]

    num_samples = data.shape[0]
    label_record = sorted(set(labels))
    assert len(colours) >= len(label_record)
    label_dict = {k: v for v, k in enumerate(label_record)}
    data_record = [[] for l in label_record]
    for i in range(num_samples):
        data_record[label_dict[labels[i]]].append(data[i])
    plt.title(title)
    for t in range(len(label_record)):
        data_t = np.array(data_record[t])
        plt.scatter(data_t[:, 0], data_t[:, 1], c=colours[t])
    if save:
        plt.savefig(f'data_plotted_{title}')
    plt.show()

def plot_distance(lab_num, type='wasserstein'):
    dists = []
    accss = []
    for i in range(100):
        means, covs, n_data, ks, weights, norm, dist = load_lab_config(lab_num)
        models, ds, data, labels = run_lab(means, covs, n_data, ks, weights, norm, dist)
        if type == 'wasserstein':
            dists.append(wasserstein_distance(ds[0], ds[1]))
        else:
            dists.append(maximum_mean_discrepancy(ds[0], ds[1]))
        accs = max([m['acc'] for m in models])
        accss.append(accs)
    dtoa = zip(dists, accss)
    dtoa = dict(sorted(dtoa, key=lambda x: x[0]))
    fig, ax = plt.subplots()
    ax.plot(dtoa.keys(), dtoa.values())
    plt.show()

def plot_kl_divergence(lab_num):
    dists = []
    accss = []
    for i in range(100):
        means, covs, n_data, ks, weights, norm, dist = load_lab_config(lab_num)
        models, ds, data, labels = run_lab(means, covs, n_data, ks, weights, norm, dist)
        dists.append(get_KLdiv(means[0], covs[0], means[1], covs[1]))
        accs = [m['acc'] for m in models]
        accss.append(accs)
    accss = np.array(accss)
    for i in range(accss.shape[1]):
        dtoa = zip(dists, accss[:, i])
        dtoa = dict(sorted(dtoa, key=lambda x: x[0]))
        fig, ax = plt.subplots()
        ax.plot(dtoa.keys(), dtoa.values())
        plt.show()

def plot_decision_boundary(lab_num, save=False,
colours=['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:grey', 'tab:olive', 'tab:cyan']):
    means, covs, n_data, ks, weights, norm, dist = load_lab_config(lab_num)
    models, ds, data, labels = run_lab(means, covs, n_data, ks, weights, norm, dist)
    
    assert data.shape[0] == labels.shape[0]
    label_record = sorted(set(labels))
    num_classes = len(label_record)
    assert len(colours) >= len(label_record)
    label_dict = {k: v for v, k in enumerate(label_record)}

    x_min, x_max = np.min(data[:, 0]) - 1, np.max(data[:, 0]) + 1
    y_min, y_max = np.min(data[:, 1]) - 1, np.max(data[:, 1]) + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.1), np.arange(y_min, y_max, 0.1))

    assert len(ks) == 9

    fig, axs = plt.subplots(3, 3, sharex='col', sharey='row', figsize=(15,12))
    indices = [(x, y) for x in [0,1,2] for y in [0,1,2]]
    titles = ['KNN (k=%d)' % k for k in ks]
    knns = [m['model'] for m in models]

    for idx, knn, title in zip(indices, knns, titles):
        Z = knn.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        axs[idx[0], idx[1]].contourf(xx, yy, Z, alpha=0.5)
        colour_labels = [colours[label_dict[label]] for label in labels]
        axs[idx[0], idx[1]].scatter(data[:,0], data[:,1], s=20, c=colour_labels, edgecolors=colour_labels)
        axs[idx[0], idx[1]].set_title(title)

    if save:
        plt.savefig(f'decision_boundary_plotted_{num_classes}classes')

    plt.show()

if __name__ == '__main__':
    # parse lab num from user input
    if len(sys.argv) > 1:
        lab_num = int(sys.argv[1])
    else:
        print('Usage: python source lab_num (0 ~ 20)')
        sys.exit(-1) 

    if lab_num > 20 or lab_num < 0:
        print('Usage: python source lab_num (0 ~ 20)')
        sys.exit(-1) 

    # running lab job to get data
    means, covs, n_data, ks, weights, norm, dist = load_lab_config(lab_num)
    models, ds, data, label = run_lab(means, covs, n_data, ks, weights, norm, dist)
    
    # draw diagram if needed
    if lab_num > 16 and lab_num < 20:
        plot_decision_boundary(lab_num)
    # kl_divergence to accuracy diagrams
    elif lab_num == 20:
        plot_kl_divergence(lab_num)
    # need torch and scipy library
    # elif lab_num == 21:
        # plot_distance(lab_num)
    else:
        plot_data(data, label)