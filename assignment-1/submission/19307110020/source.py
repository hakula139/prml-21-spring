import numpy as np
import matplotlib.pyplot as plt
class KNN:
    def __init__(self):
        pass

    def fit(self, train_data, train_label):
        self.len=train_data.shape[0]
        #standardize
        train_data=train_data.astype(np.float64)
        train_data=train_data.T
        self.channel=train_data.shape[0]
        self.mins=[]
        self.maxs=[]
        for data in train_data:
            self.mins.append(np.min(data))
            self.maxs.append(np.max(data))
            for i in range(data.shape[0]):
                data[i] = (data[i] - np.min(data)) / (np.max(data) - np.min(data))
        self.train_data=train_data.T
        self.train_label=train_label

        #grid search for K
        maxk, maxacc=0, 0
        for k in range(1,min(17,self.len)):
            acc=0
            for d in range(self.len):
                dists = []
                indexs = np.arange(self.len)
                for i in range(self.len):
                    dists.append(self.euclidean(self.train_data[d], self.train_data[i]))
                dic = dict(zip(indexs, dists))
                dic = dict(sorted(dic.items(), key=lambda item: item[1]))
                min_indexs = list(dict(list(dic.items())[1:k+1]).keys())
                min_dists = [self.train_label[i] for i in min_indexs]
                if max(min_dists, key=min_dists.count) == self.train_label[d]:
                    acc+=1
            if acc>maxacc:
                maxk, maxacc=k, acc
        self.K=maxk

        #credibility
        self.cred=[]
        for d in range(self.len):
            dists = []
            indexs = np.arange(self.len)
            for i in range(self.len):
                dists.append(self.euclidean(self.train_data[d], self.train_data[i]))
            dic = dict(zip(indexs, dists))
            dic = dict(sorted(dic.items(), key=lambda item: item[1]))
            min_indexs = list(dict(list(dic.items())[1:self.K+1]).keys())
            min_dists = [self.train_label[i] for i in min_indexs]
            self.cred.append(float(min_dists.count(max(min_dists, key=min_dists.count)))/self.K)

    def predict(self, test_data):
        test_data=test_data.astype(np.float64)
        test_data=test_data.T
        for i in range(self.channel):
            for j in range(test_data.shape[1]):
                test_data[i][j] = (test_data[i][j] - self.mins[i]) / (self.maxs[i] - self.mins[i])
        test_data = test_data.T
        ans=[]
        for d in range(test_data.shape[0]):
            dists = []
            indexs = np.arange(self.len)
            for i in range(self.len):
                dists.append(self.euclidean(test_data[d], self.train_data[i]))
            dic = dict(zip(indexs, dists))
            dic = dict(sorted(dic.items(), key=lambda item: item[1]))
            min_indexs = list(dict(list(dic.items())[:self.K]).keys())
            min_dict={}
            for i in min_indexs:
                min_dict[self.train_label[i]]=min_dict.get(self.train_label[i],0)+(self.cred[i]-0.2)*(2.1-dic[i]/list(dic.items())[self.K-1][1])
            ans.append(max(min_dict, key=lambda k: min_dict[k]))
        return ans


    def euclidean(self, a, b):
        return np.sqrt(np.sum(np.square(a-b)))

    def manhattan(self, a, b):
        return np.sum(abs(a-b))


def dataset(mean1,mean2,cov1,cov2,mean3=None,cov3=None):
    mean=mean1
    cov=cov1
    x=np.random.multivariate_normal(mean, cov, (100,))
    mean=mean2
    cov=cov2
    y=np.random.multivariate_normal(mean, cov, (100,))
    num=300
    if mean3 is not None and cov3 is not None:
        mean=mean3
        cov=cov3
        z=np.random.multivariate_normal(mean, cov, (100,))
    idx=np.arange(num)
    np.random.shuffle(idx)
    if mean3 is not None and cov3 is not None:
        data=np.concatenate([x, y, z])
        label=np.concatenate([np.zeros((100,), dtype=np.int8), np.ones((100,), dtype=np.int8), np.ones((100,), dtype=np.int8) * 2])
    else:
        data=np.concatenate([x, y])
        label=np.concatenate([np.zeros((100,), dtype=np.int8), np.ones((100,), dtype=np.int8)])
    data=data[idx]
    label=label[idx]
    split=int(num*0.8)
    train_data, test_data=data[:split,:], data[split:,:]
    train_label, test_label=label[:split], label[split:]
    np.save("train_data.npy",train_data)
    np.save("test_data.npy",test_data)
    np.save("train_label.npy",train_label)
    np.save("test_label.npy",test_label)

def dataload():
    train_data, train_label, test_data, test_label = np.load("train_data.npy",allow_pickle=True),np.load("train_label.npy",allow_pickle=True),np.load("test_data.npy",allow_pickle=True),np.load("test_label.npy",allow_pickle=True)
    return train_data, train_label, test_data, test_label

if __name__ == '__main__':
    dataset((1,2),(4,5),np.array([[10, 0], [0, 2]],dtype=np.float64),np.array([[7, 3], [15, 1]],dtype=np.float64),(-2,6),np.array([[0, 1], [1, 2]],dtype=np.float64))
    train_data, train_label, test_data, test_label=dataload()
    model=KNN()
    model.fit(train_data,train_label)
    res=model.predict(test_data)
    print("acc =",np.mean(np.equal(res, test_label)))
