import numpy as np

from source import KNN

train_data = np.array([
    [1, 2, 3, 4],
    [4, 2, 3, 1],
    [12, 12, 13, 14],
    [14, 12, 13, 11],
    [12, 14, 15, 16]
])
train_label = np.array([0, 0, 1, 1, 1])

test_data = np.array([
    [3, 4, 4, 2],
    [18, 14, 15, 16]
])
test_label = np.array([0, 1])

model = KNN()
model.fit(train_data, train_label)
res = model.predict(test_data)

print("acc =", np.mean(np.equal(res, test_label)))
