# PRML-21-SPRING

Course website for PRML(Pattern Recognition and Machine Learning) Spring 2021 at Fudan University.

- Instructor: Xipeng Qiu
- Teaching Assistants: Yining Zheng
- Grading: 3 assignments with a total 60% weight, and a 35% final project, and 5% for class.

## Schedule

- Week3: Release of assignment-1
- Week5: Deadline of assignment-1 (Thu Apr 1 23:59)
- Week7: Release of assignment-2
- Week9: Deadline of assignment-2 (Sun May 2 23:59)
- Week13: Release of assignment-3
- Week15: Deadline of assignment-3 (Mon Jun 14 23:59)

## Coursework Guidelines

- You should use Python3 as your programming language, we would recommend you to use miniconda to make an independent enviroment for each assignment.
- Both English and Chinese are acceptable, and there will be no difference in terms of marking as long as you can make yourself clear with your report.
- **The report of each assignment should be written in markdown, named `README.md`, and placed in the directory named by your student ID. Read [Submission Guidelines](#submission-guidelines) for more details.**
- Gitee support latex math equation, but you should use `\\\\` to replace `\\` in latex equations. TA will judge your report in Gitee website, and see the same as you see on browser.
- If you find any mistakes in the coursework, you are encouraged to correct it with a pull request whose title should be started with `[fix]`.
- Please use the [issue system](https://gitee.com/fnlp/prml-21-spring/issues) to ask questions about the coursework or discuss about the course content. Proper tags (e.g. assignment-1) can make the questions answered by the instructor, TA, or others be valuable for other students.
- For any feedback, please consider emailing the TA(ynzheng19@fudan.edu.cn) first.

## Submission Guidelines

Different from previous years' guidelines, we assume you are proficient in Git operations.

- Firstly, you should register a Gitee account and fork our repository online.
- Once we release a new assignment, you should pull the changes from our repository to yours. There are two common ways:
  - Use Gitee's Sync. This way may overwrite your changes which have not been merged into our repository.
  - Use `git pull git@gitee.com:fnlp/prml-21-spring.git` in your local command line.
- After you commit your work, you can make a pull request to merge them into this repository. This [article](https://gitee.com/help/articles/4128#article-header0) may help you.
- The title of pull request should be like `[submission] assignment-1 of {your student ID}`. [This](https://gitee.com/fnlp/prml-21-spring/pulls/1) is an example of PR.

For each assignment, files should be structured like this

```text
.
├── assignment-1/
|   ├── submission/
│       ├── 15307130115/
|           ├── img/
|               └── xxx.png
│           ├── README.md
│           └── source.py
|   └── handout/
|       └── ....
|   └── README.md
```

- The directory named by your student ID (like `15307130115` )should be placed in `assignment-1/submission/` . It should contain your report named `README.md` and some Python files required.
- The images you need in report should be placed in directory `img` under your student ID directory. `.png` format is recommended.
- The `assignment-1/handout/` directory contains the facilities provided for you to accomplish the assignment. You can read the `assignment-1/README.md` to know how to use them.

## Previous offerings

- [Spring 2020](https://github.com/xuyige/PRML-Spring20-FDU)
- [Spring 2019](https://github.com/ichn-hu/PRML-Spring19-Fudan)

> Pattern Recognition and Machine Learning / Fudan University / 2021 Spring
