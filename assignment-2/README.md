# 作业-2

本次作业有两个选题，大家可以任选其一完成。

## 选题1：前馈神经网络

本次作业要求使用 NumPy 实现一种简单的前馈神经网络。你需要先实现 Matmul, Relu, Log, Softmax 等支持前馈计算和反向传播的算子，再利用这些算子构建一个简单的神经网络。最后，你需要使用这个模型在 MNIST 数据集上进行训练和测试，并完成实验报告。

本次作业会考察你使用 NumPy 和 PyTorch 编程的能力以及对前馈神经网络背后数学知识的理解。

### 更新 2021.4.25

1. 修复了 torch_mnist.py 中把 softmax/log 算子输出的梯度当成了输入的梯度的问题。 现在 NumpyModel 中的 softmax_grad, log_grad 等均为算子反向传播的梯度（ loss 关于算子输入的偏导）
2. 更新了 numpy_fnn.py, numpy_mnist.py, tester_demo.py 中的训练模型代码的结构，把 loss 的计算和反向传播的代码集中到 NumpyLoss 类中，便于理解。

**已经完成作业的同学需要修改 numpy_fnn.py 中的 NumpyModel.backward 方法，并参考我们更新的代码来使用 NumpyLoss 类** 

> 以上问题的反馈来自学号尾号为 116, 154, 213 的同学

### 提供材料

你可以在 `handout-1/` 文件夹下找到如下文件，你需要将这些文件复制到自己的工作目录下进行作业，不过你最终只需要提交其中的部分文件。

- numpy_fnn.py: 这个文件给出了你需要实现的算子和模型的框架，你只需要完成填空即可
- tester_demo.py: 这是一个自动测试文件，它会测试你在 `numpy_fnn.py` 中实现的代码
- torch_mnist.py: 使用 PyTorch 实现神经网络的代码，你需要按照其中定义的网络结构在 `numpy_fnn.py` 中构建网络
- numpy_mnist.py: 使用 NumPy 搭建的神经网络进行训练测试的代码，如果 `numpy_fnn.py` 实现正确，它是可以直接运行的
- utils.py: 实现了一些辅助性的函数，如果你想修改这些函数，请在 `numpy_mnist.py` 中重写

### 提交内容

- README.md: 课程作业报告
- numpy_fnn.py: 你用 NumPy 实现的算子和模型，我们会对该代码进行自动测试
- numpy_mnist.py: 你进行训练和测试的代码，我们会结合你的报告和代码进行评分

提交方法参见[提交指南](https://gitee.com/fnlp/prml-21-spring#提交指南)

### 实验环境
 
```bash
conda create -n assignment-2 python=3.8 -y
conda activate assignment-2
pip install numpy
pip install matplotlib
pip install torch
pip install torchvision
```

### 实现模型

你首先需要参考课内知识在 `numpy_fnn.py` 中实现 Matmul, Relu, Log, Softmax 等支持前馈计算和反向传播的算子。在实现过程中，你可以查看 `tester_demo.py` 中的测试代码，了解你需要实现的算子与 PyTorch 中的哪个算子相对应。 在安装好实验环境之后，你可以在工作目录下运行 `tester_demo.py` 来评测自己实现的算子是否符合要求。注意，在实现反向传播时，你可以使用 `self.memory` 中存下来的相关变量的值。

在实现基础算子之后，你需要按照 `torch_mnist.py` 中定义的网络结构在 `numpy_fnn.py` 中补全 `NumpyModel` 的代码。注意，在反向传播时，你需要更新 `self.x1_grad`, `self.W1_grad` 等全部梯度的值。你可以查看 `tester_demo.py` 中的代码来了解评测标准，并尝试运行它进行评测。

我们最终会使用自动评测代码来测试你提交的 `numpy_fnn.py` 文件，测试代码与 `tester_demo.py` 基本一致，但测试环境中会屏蔽 NumPy 之外的第三方包，你需要确保自己的 `numpy_fnn.py` 中只使用了 NumPy。

除此之外，你还需要在报告中写出你实现算子反向传播的公式推导，你实现的代码需要和你推导的公式对应。

### 进行实验

在 `numpy_fnn.py` 中补全 `NumpyModel` 之后，你需要修改 `numpy_mnist.py` 中的代码，进行模型的训练和测试。其中，你需要使用 NumPy 实现 `mini_batch` 函数，替换 `utils.py` 中使用 PyTorch 实现的 `mini_batch` 函数。

你可以使用现有模型进行若干实验，并在报告中介绍。因为这只是一个简单的作业，我们不推荐你尝试更多复杂的网络结构。如果你一定要尝试，**请勿修改 `numpy_fnn.py` 中的模型结构**，因为这样会影响你在自动测试中的得分；你可以在 `numpy_mnist.py` 中编写你的其他模型并进行实验，但这并不会让你获得更多的分数。

我们提供了两个方向进行扩展探究，每个方向可以获得最多 10% 的附加分。如果你进行了拓展探究，请在报告中单独列出。

探究1：实现 momentum、Adam 等其它优化方法并进行对比实验

探究2：调研 PyTorch 中权重初始化的方法，并实现代码替换 `get_torch_initialization` 函数

### 评分细则

自动测试的七个测试点对应 `numpy_fnn.py` 中的七个空，附加分和其它部分的总和不超过 100%。

- 自动测试（60%）
  - Matmul 的反向传播 (5%)
  - Relu 的反向传播 (5%)
  - Log 的反向传播 (5%)
  - Softmax 的前向计算 (5%)
  - Softmax 的反向传播 (10%)
  - FNN 的前向计算 (10%)
  - FNN 的反向传播 (20%)
- 实验与报告 (40%)
  - 模仿 `torch_mnist.py` 的代码，在 `numpy_mnist.py` 中进行模型的训练和测试，并在报告中介绍你的实验过程与结果 (20%)
  - 在 `numpy_mnist.py` 中只用 NumPy 实现 `mini_batch` 函数，替换 `utils.py` 中使用 PyTorch 实现的 `mini_batch` 函数 (10%)
  - 在报告中推导 `numpy_fnn.py` 中实现算子的反向传播计算公式 (10%)
- 附加分 (20%)
  - 实现 momentum、Adam 等其它优化方法并进行对比实验  (10%)
  - 调研 PyTorch 中权重初始化的方法，并实现代码替换 `get_torch_initialization` 函数 (10%)

## 选题2

理论和实验证明，一个两层的 ReLU 网络可以模拟任何有界闭集函数。

### 提交内容

- README.md: 课程作业报告
- source.py: 你的实验代码

### 具体要求

你首先需要在报告中用理论证明这个结论，并给出你在证明过程中所引用定理的出处和证明过程（如果出处中有证明过程则可以省略）。

其次，你需要使用 NumPy 或 PyTorch 编写实验来证明这个结论，你至少需要拟合 3 个不同类型的函数。最好可以写成可以接受任意函数做为输入，然后进行拟合的模式。

### 评分细则

- 课程报告 (50%)
- 实验代码 (50%)
  - 只使用 NumPy 实现模型 (10%)
  - 拟合 3 个不同类型的函数的实验代码 (3*10%)
  - 实现接受任意函数做为输入进行拟合的函数 (10%)
- 附加分 (20%)
  - 其它可以证明这一结论的实验，每个10%，附加分不超过20%，总分不超过 100%

> 模式识别与机器学习 / 复旦大学 / 2021年春
