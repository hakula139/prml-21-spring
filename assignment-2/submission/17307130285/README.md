# Assignment2

**17307130285 王茹**

## Formulas

#### matmul

$$
Y = X*W
$$
矩阵求导:
$$
\frac{\partial Y}{\partial X} = W^{T}
$$

$$
\frac{\partial Y}{\partial W} = X^{T}
$$

```python
def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #
        ####################
        grad_W = np.matmul(self.memory['x'].T, grad_y)
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        
        return grad_x, grad_W
```

#### Relu

$$
Y_{ij}=\begin{cases}
X_{ij}&X_{ij}\ge0\\\\
0&\text{otherwise}
\end{cases}
$$
求导：
$$
\frac{\partial Y_{ij}}{\partial X_{ij}}=\begin{cases}
1&X_{ij}>0\\\\
0&\text{otherwise}
\end{cases}
$$

```python
def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        #若x大于0， grad_x的结果就是传入的grad_y。否则结果为0
        x = self.memory['x']
        grad_x = np.where(x > 0, grad_y, np.zeros_like(grad_y))
        
        return grad_x
```

#### Log

$$
Y=\ln(X+\epsilon)
$$
求导：
$$
\frac{\partial Y}{\partial X} = \frac1{X+\epsilon}
$$

```python
def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        grad_x = np.multiply(1./(self.memory['x'] + self.epsilon), grad_y)
        return grad_x
```



#### Softmax

![sm](README.assets/sm.png)

![softmax](README.assets/softmax.png)

```python
def forward(self, x):
        """
        x: shape(N, c)
        """
        
        ####################
        #      code 4      #
        ####################
        out = []
        for i in range(x.shape[0]):
            t = x[i]
            t = t - max(t)
            t = np.exp(t)
            out.append(t/sum(t))
        out = np.array(out)
        self.memory['out'] = out
        return out
      
def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 5      #
        ####################
        out = self.memory['out']
        j = np.array([np.diag(i) - np.outer(i, i) for i in out])

        grad_y = grad_y[:, np.newaxis, :]
        grad_x = np.matmul(grad_y, j).squeeze(axis=1)
        
        return grad_x

```



## Experiments

- mini_batch

```python
def mini_batch(dataset, batch_size=32):
    x_train = np.array([np.array(d[0]) for d in dataset])
    y_train = np.array([d[1] for d in dataset])
    # 打乱数据集
    size = x_train.shape[0]
    idx = np.arange(size)
    np.random.shuffle(idx)
    # 防止batch_size不整除
    num = int(np.ceil(size / batch_size)) * batch_size
    batches = [(x_train[idx[i:i + batch_size]], y_train[idx[i:i + batch_size]]) for i in range(0, num, batch_size)]
    return batches
```



### Before implementing mini_batch with numpy

#### Experiment 1

- epoch num: 3
- learning rate: 0.1
- batch size = 128

[0] Accuracy: 0.9465
[1] Accuracy: 0.9606
[2] Accuracy: 0.9674

![torch_batch](README.assets/torch_batch.png)

### After

#### Experiment 2

- epoch num: 3
- learning rate: 0.1
- batch size = 128

[0] Accuracy: 0.9492
[1] Accuracy: 0.9642
[2] Accuracy: 0.9708

![ex2](README.assets/ex2.png)

#### Experiment 3

- epoch num: 3
- learning rate: 0.1
- batch size = 32

[0] Accuracy: 0.9646
[1] Accuracy: 0.9738
[2] Accuracy: 0.9754

![ex3](README.assets/ex3.png)

