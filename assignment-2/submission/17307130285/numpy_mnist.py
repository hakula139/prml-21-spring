import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot

def mini_batch(dataset, batch_size=32):
    x_train = np.array([np.array(d[0]) for d in dataset])
    y_train = np.array([d[1] for d in dataset])
    # 打乱数据集
    size = x_train.shape[0]
    idx = np.arange(size)
    np.random.shuffle(idx)
    # 防止batch_size不整除
    num = int(np.ceil(size / batch_size)) * batch_size
    batches = [(x_train[idx[i:i + batch_size]], y_train[idx[i:i + batch_size]]) for i in range(0, num, batch_size)]
    return batches



def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
