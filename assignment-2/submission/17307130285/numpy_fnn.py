import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #
        ####################
        grad_W = np.matmul(self.memory['x'].T, grad_y)
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        #若x大于0， grad_x的结果就是传入的grad_y。否则结果为0
        x = self.memory['x']
        grad_x = np.where(x > 0, grad_y, np.zeros_like(grad_y))
        
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        grad_x = np.multiply(1./(self.memory['x'] + self.epsilon), grad_y)
        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        ####################
        #      code 4      #
        ####################
        out = []
        for i in range(x.shape[0]):
            t = x[i]
            t = t - max(t)
            t = np.exp(t)
            out.append(t/sum(t))
        out = np.array(out)
        self.memory['out'] = out
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 5      #
        ####################
        out = self.memory['out']
        j = np.array([np.diag(i) - np.outer(i, i) for i in out])

        grad_y = grad_y[:, np.newaxis, :]
        grad_x = np.matmul(grad_y, j).squeeze(axis=1)
        
        return grad_x


class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))
        
        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()
        
        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        x = x.reshape(-1, 28 * 28)
        x = self.relu_1.forward(self.matmul_1.forward(x, self.W1))
        x = self.relu_2.forward(self.matmul_2.forward(x, self.W2))
        x = self.matmul_3.forward(x, self.W3)
        x = self.log.forward(self.softmax.forward(x))

        
        return x
    
    def backward(self, y):
        
        ####################
        #      code 7      #
        ####################
        self.log_grad = self.log.backward(y)
        grad_y = self.log_grad
        self.softmax_grad = self.softmax.backward(grad_y)
        grad_y = self.softmax_grad
        self.x3_grad, self.W3_grad = self.matmul_3.backward(grad_y)
        grad_y = self.x3_grad
        self.relu_2_grad =self.relu_2.backward(grad_y)
        grad_y = self.relu_2_grad
        self.x2_grad, self.W2_grad =self.matmul_2.backward(grad_y)
        grad_y = self.x2_grad
        self.relu_1_grad = self.relu_1.backward(grad_y)
        grad_y = self.relu_1_grad
        self.x1_grad, self.W1_grad = self.matmul_1.backward(grad_y)
        
        pass
    
    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad
