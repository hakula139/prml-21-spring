# Assignment 2

## 1.FNN 模型结构

本次实验中的FNN为三层隐藏层的模型，在这一层之后输出结果h(x)，每一层的结果如下：

​	**输入层**：(784*M)的输入矩阵

​	**W1**：（784*256）的全连接参数，ReLU激活函数

​	**隐藏层1**: （256*M）的隐藏层

​	**W2**：（256*64）的全连接参数，ReLU激活函数

​	**隐藏层2**：（64*M）的隐藏层

​	**W3**：（64*10）的全连接参数，Softmax激活函数

​	**隐藏层3**：（10*M）的隐藏层

​	**输出层**：Log的激活函数

### 前向与后向传播

对此我们有如下前向传播与后向传播模型

FNN 前向传播：
$$
input = X=z^{(0)}=a^{(0)}
$$

$$
z^{(1)} = a^{(0)}W_1,a^{(1)} = Relu(z^{(1)})
$$

$$
z^{(2)} = a^{(1)}W_2,a^{(2)} = Relu(z^{(2)})
$$

$$
z^{(3)} = a^{(2)}W_3,a^{(3)} = Softmax(z^{(3)})
$$

$$
z^{(4)} = a^{(3)},a^{(4)}=Log(z^{(4)})
$$

对应的反向传播路径以此为：
$$
Y→a^{(4)}→z^{(4)}→a^{(3)}→z^{(3)}→a^{(2)}→z^{(2)}→a^{(1)}→z^{(1)}→a^{(0)}→z^{(0)}
$$


优化的步骤为：
$$
Y→Log→Softmax→W_3→Relu→	W_2→Relu→W_1→X
$$

## 2.前向传播算子

### Matmul

$$
Y=XW
$$

### Relu

$$
Y_{ij} = max(0,X_{ij})
$$

### Softmax

$$
Y_{ij} = \frac{expX_{ij}}{\sum_{n=1}^{M} X_{in}}
$$

### Log

$$
Y_{ij} = Log(X_{ij}+\epsilon)
$$

## 3.反向传播算子

### Log

$$
\frac{\partial L}{\partial X_{ij}} = \frac{\partial L}{\partial Y_{ij}} \frac{\partial Y_{ij}}{\partial X_{ij}}=\frac{\partial L}{\partial Y_{ij}} \frac{1}{\partial X_{ij}+\epsilon}
$$

$$
 \nabla X_{ij} = \nabla Y_{ij} \frac{1}{\partial X_{ij}+\epsilon}
$$

### Softmax

<img src="https://gitee.com/fnlp/prml-21-spring/raw/master/assignment-2/submission/17307130133/img/softmax.png" alt="img" style="zoom: 67%;" />


$$
J_{i} = diag(Y_i) - Y_iY_i^T
$$

$$
\nabla X = \nabla Y J
$$

### Relu

$$
ReLU^{\prime}(x) = \begin {cases} 1, x>0 \\ 0,x<=0 \end{cases}
$$


$$
\frac{\partial L}{\partial X} =\frac{\partial L}{\partial Y}\frac{\partial Y}{\partial X}
$$

$$
\nabla X = \nabla Y*ReLU^{\prime}(x)
$$



### Matmul

#### grad_X

对于Y = XW形式的计算，X为横向量,将X看作若干个列向量的组合：
$$
X = (x_1,x_2,...,x_M)
$$


因此Y=XW，右乘矩阵W，得到Y，对于每个Z的列向量有
$$
Y_i =\sum_{k=1}^{M} X_kW_{ki}
$$

$$
\therefore (\frac {\partial y}{\partial x})_{ij} =  \frac{\partial y_i}{\partial x_j} = W_{ij}
$$

$$
\therefore \frac{\partial Y}{\partial X} = W^T
$$

$$
\frac{\partial L}{\partial X} = \frac{\partial L}{\partial Y} \frac{\partial Y}{\partial X} = \frac{\partial L}{\partial Y}W^T
$$

$$
\therefore\nabla X = \nabla YW^T
$$

#### grad_W

对于Y = XW形式的计算，同样将横向两X看作多个列向量的横向组合,因此对于每个Z的列向量有
$$
y_k = \sum_{l=1}^{M} x_lw_{lk}
$$

$$
\therefore\frac {\partial y_k}{\partial w_{ij}} = \frac {\partial }{\partial w_{ij}} (\sum_{l=1}^{M} x_lw_{lk})
$$



故当且仅当l=i，k=j时改导数非零
$$
\therefore \frac {\partial Y}{\partial w_{ij}} = [0,0...,x_i,...,0,0]^T (x_i是j^{th}号元素) = \delta
$$
得到的导数结果为列向量，X为横向量，因此有
$$
\therefore \frac{\partial L}{\partial W_{ij}} = \frac{\partial L}{\partial Y} \frac{\partial Y}{\partial W_{ij}}=\sum_{k=1}^{M} \delta_k\frac {\partial z_k}{\partial w_{ij}}= \delta_jx_i = X^T\delta
$$

$$
\therefore \nabla W = X^T\nabla Y
$$

## 4.mini_batch实现

mini_batch的目的为将训练集切割为多份，依次选取进行梯度下降的优化。mini_batch函数可以分两三步：1、将数据集进行随机打乱；2、切割为多份

```python
def mini_batch(dataset,batch_size=128):
    data, label =[], []
    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])
    data = np.array(data)
    label = np.array(label)
    #2、随机打乱
    random_index = np.random.permutation(data.shape[0])
    data = data[random_index]
    label = label[random_index]
    result = []
    #3、划分
    for i in range(data.shape[0] // batch_size):
        result.append((data[i*batch_size:(i+1)*batch_size] , label[i*batch_size:(i+1)*batch_size]))
    return result
```

## 5.FNN实验结果

采用learning_rate = 0.1时,epoch_number = 3时，运行代码得到如下结果：

![Figure_1](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-2/submission/17307100038/img/Figure_1.png)

准确率结果如下

```python
[0]Accuracy: 0.9435
[1]Accuracy: 0.9654  
[2]Accuracy: 0.9706
```

## 6.模型的优化

在采用随机梯度下降方法（SGD，stochastic gradient descent）训练神经网络时，虽然收敛速度快，但是容易陷入局部最优，困在马鞍处。而采batch的训练结果进行对W参数的优化时（BGD, batch gradient descent），如果采用convex的loss function，往往容易找到全局最优解，但是梯度下降慢，训练占用较多内存。再者，采用Mini_BDG(mini-batch gradient descent)的优化虽然计算高校收敛稳定，为现在的主流，但是仍然容易陷入局部最优解。

因此提出了两种优化的方法对这些问题进行优化。

Momentum是模拟物理中动量的优化方法，在更新方向的时候保留之前的方向，从而增加稳定性还有摆脱局部最优的情况。当本次优化时的梯度方向与历史梯度一样，会增强这个梯度，反之则减弱这个梯度。对应的公式为
$$
\Delta w=\alpha \Delta w - \eta \nabla L(w)
$$

$$
w += \Delta w
$$

RMSProp方法全程Root Mean Square Prop，在Momentum的基础上进一步优化损失函数在更新中摆幅过大的问题，并进一步加快函数的收敛速度。
$$
v(w,t) = \gamma v(w,t-1) + (1-\gamma)(\nabla L(w_i))^2, \gamma为遗忘因子
$$

$$
w = w - \frac{\eta}{\sqrt{v(w,t)}}\nabla L(w_i)
$$

Adam的思路是将momentum和RMSProp结合起来，利用矩阵的一阶估计和二阶估计来动态调整每个参数的学习率
$$
v_{dw} = \beta_1v_{dw} + (1-\beta_1)dW
$$

$$
s_{dw} = \beta_2s_{dw} + (1-\beta_2)dW^2
$$

$$
v_{db} = \beta_1v_{db} + (1-\beta_1)db， (考虑偏置b时)
$$

$$
s_{db} = \beta_2s_{db} + (1-\beta_2)db^2,(考虑偏置b时)
$$

因此有：
$$
W = W - \eta \frac {v_{dw}} {\sqrt s_{dw}}
$$

$$
b = b - \eta \frac {v_{db}} {\sqrt s_{db}},(考虑偏置b时)
$$

对此采用了Adam的优化结果如下

learning_rate = 0.1时

![Figure_A_0.1](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-2/submission/17307100038/img/Figure_A_0.1.png)

```python
[0]Accuracy: 0.0919
[1]Accuracy: 0.0919  
[2]Accuracy: 0.0919
```



learning_rate = 0.05时

![Figure_A_0.05](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-2/submission/17307100038/img/Figure_A_0.05.png)

```python
[0]Accuracy: 0.8856
[1]Accuracy: 0.9221  
[2]Accuracy: 0.9300
```



learning_rate = 0.01时

![Figure_A_0.01](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-2/submission/17307100038/img/Figure_A_0.01.png)

```python
[0]Accuracy: 0.9072
[1]Accuracy: 0.9204  
[2]Accuracy: 0.9276
```



从结果可见，如果采用设定初始的学习率过大(η=0.1)时，由于步长过大，导致优化结果越过了梯度下降的区间，这导致误差快速提升并且模型准确率极低（0.0919），并且adam在1400步内未见到结果收敛；降低学习率η至0.05，可见训练开始时由于步长过大，误差提升，但随着训练次数达到30左右时开始快速下降，最后区域收敛。当eta设置未0.01时，收敛速度更快。

横向对比mini_BGD（η=0.1）和Adam(η=0.01)时，可见Adam模型虽然步长更小，但是收敛远远快于mini_BGD的结果。
