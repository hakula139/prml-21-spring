# 实验报告
### 1.模型设计
神经网络设计如下图所示
### 2.算子补充
**Matmul**

**Forward**
$$
h=X*W
$$

**Backward**
$$
\frac{\partial Y}{\partial X} = W^{T}
$$
$$
\frac{\partial Y}{\partial W} = X^{T}
$$
维度变化以及Python实现如下所示：
```
        """
        grad_y: shape(N, d')
        w.T: shape(d', d)
        """
        grad_x=np.matmul(grad_y, W.T)
        """
        grad_y: shape(N, d')
        x.T: shape(d, N)
        """
        grad_W=np.matmul(x.T, grad_y)
```
**Relu**

**Forward**
$$
Y=\begin{cases}
X&X\ge0\\\\
0&\text{otherwise}
\end{cases}
$$
**Backward**
$$
\frac{\partial Y}{\partial X}=\begin{cases}1&X\ge0\\\\
0&\text{otherwise}
\end{cases}
$$
Python实现如下所示：
```
 def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x=self.memory['x']
        grad_x=grad_y*np.where(x>0,1,0)
        return grad_x
```
**Log**

Forward
$$
Y=Log(x+epsilon)
$$
**Backward**
$$
Y=1/(x+epsilon)
$$
Python实现如下所示：
```
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        
        """
        grad_y: same shape as x
        """
        x=self.memory['x']
        grad_x=grad_y*(1./(x+self.epsilon))
        return grad_x
```
Softmax:

**Forward**
$$
Y_i = \frac{e^{X_i}}{\sum_{k=1}^n e^{X_k}}
$$

**Backward**
$$
\frac{\partial Y_i}{\partial X_j} =
	\begin{cases}
    Y_i \times (1 - Y_i) & i = j\\\\
    -Y_i \times Y_j & i \neq j
    \end{cases}
$$
Python实现如下所示：

softmax的反向传播通过逐个元素判断求导进行实现
```
    def forward(self, x):
        """
        x: shape(N, c)
        """
        ex = np.exp(x)
        rowsum = np.sum(ex,axis=1)
        rowsum = rowsum[:,np.newaxis]
        softmax = ex / rowsum
        self.memory['softmax'] = softmax
        return softmax
 
    def backward(self, grad_y):
        softmax = self.memory['softmax']
        # print(sumx.shape)
        [ROWS, COLUMNS] = softmax.shape
        grad_x = []
        grad_x=[[0 for i in range(COLUMNS)] for j in range(ROWS)]
        for i in range(len(grad_x)):
            for j in range(len(grad_x[0])):
                for k in range(len(grad_x[0])):
                    if j == k:
                        grad_x[i][j] += (1 - softmax[i][k]) * softmax[i][k] * grad_y[i][k]
                    else:
                        grad_x[i][j] += -softmax[i][j] * softmax[i][k] * grad_y[i][k]
        grad_x = np.array(grad_x)
```
### 3.mini_batch函数优化
原有mini_batch方法是将元素打乱重排进行训练 分别将数据和标签储存进对应list 

将index进行打乱

根据batch_size 从乱序index中一次取出相应大小的数据及标签进行训练

Python实现如下所示：
```
def mini_batch(dataset,batch_size=128):
    data = []
    label = []
    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])
    data = np.array(data)
    label = np.array(label)
    index=data.shape[0]
    index = list(np.random.permutation(index))
    return [(data[index[i:i + batch_size]], label[index[i:i + batch_size]]) for i in range(0, len(data), batch_size)]
```
### 4.实验结果
**准确率如下**

使用**更新后的mini_batch函数**

[0] Accuracy: 0.9367

[1] Accuracy: 0.9607

[2] Accuracy: 0.9687

![](./img/loss_value%20mini%20batch.png)

使用**util.py中的mini_batch函数**

[0] Accuracy: 0.9441

[1] Accuracy: 0.9635

[2] Accuracy: 0.9721

![](./img/mini_batch_orig.png)

经过比对，两者准确性基本相同

使用**更新后的mini_batch函数**，选取更小**batch**

[0] Accuracy: 0.9594

[1] Accuracy: 0.9702

[2] Accuracy: 0.9771

![](./img/sma_bat.png)