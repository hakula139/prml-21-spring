import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        
        ####################
        #      code 1      #
        ####################
        x=self.memory['x']
        W=self.memory['W']
        """
        grad_y: shape(N, d')
        w.T: shape(d', d)
        """
        grad_x=np.matmul(grad_y, W.T)
        """
        grad_y: shape(N, d')
        x.T: shape(d, N)
        """
        grad_W=np.matmul(x.T, grad_y)

        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        ####################
        #      code 2      #
        ####################
        x=self.memory['x']
        grad_x=grad_y*np.where(x>0,1,0)
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        
        """
        grad_y: same shape as x
        """
        ####################
        #      code 3      #
        ####################
        x=self.memory['x']
        grad_x=grad_y*(1./(x+self.epsilon))
        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        ####################
       #      code 4      #
       ####################
#        self.memory['x'] = x
#        exp_x=np.exp(x)
#        softmax=np.exp(x)/np.sum(exp_x,axis=1)
#        self.memory['softmax']=softmax
        ex = np.exp(x)
        rowsum = np.sum(ex,axis=1)
        rowsum = rowsum[:,np.newaxis]
        softmax = ex / rowsum
        self.memory['softmax'] = softmax
        return softmax
 
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        ####################
        #      code 5      #
        ####################
#        softmax = self.memory['softmax']

        softmax = self.memory['softmax']
        # print(sumx.shape)
        [ROWS, COLUMNS] = softmax.shape
        grad_x = []
        # print(grad_y)
#        grad_x=[[] for i in range(ROWS)]
        grad_x=[[0 for i in range(COLUMNS)] for j in range(ROWS)]
        for i in range(len(grad_x)):
            for j in range(len(grad_x[0])):
#            for j in range(m):
#                out[i].append(0)
                for k in range(len(grad_x[0])):
                    if j == k:
                        
                        grad_x[i][j] += (1 - softmax[i][k]) * softmax[i][k] * grad_y[i][k]
                    else:
                        grad_x[i][j] += -softmax[i][j] * softmax[i][k] * grad_y[i][k]
        grad_x = np.array(grad_x)

        return grad_x
class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]

class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))


        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None
        
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        
        A1 = self.matmul_1.forward(x, self.W1) # shape(5, 4)
        z1 = self.relu_1.forward(A1)
        A2 = self.matmul_2.forward(z1, self.W2)
        z2=self.relu_2.forward(A2)
        A3=self.matmul_3.forward(z2,self.W3)
        z3 = self.softmax.forward(A3)
        R = self.log.forward(z3)
        return R
    
    def backward(self, y):
        ####################
        #      code 7      #
        ####################
        self.log_grad=self.log.backward(y)
        self.soft_grad=self.softmax.backward(self.log_grad)
        self.x3_grad,self.W3_grad=self.matmul_3.backward(self.soft_grad)
        self.relu_2_grad=self.relu_2.backward(self.x3_grad)
        self.x2_grad,self.W2_grad=self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad=self.relu_1.backward(self.x2_grad)
        self.x1_grad,self.W1_grad=self.matmul_1.backward(self.relu_1_grad)
        pass
        
    
    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad
