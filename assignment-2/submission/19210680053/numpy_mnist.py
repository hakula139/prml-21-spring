# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 22:11:32 2021

@author: hyt
"""

import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot
def mini_batch(dataset,batch_size=128):
    data = []
    label = []
    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])
    data = np.array(data)
    label = np.array(label)
    index=data.shape[0]
    index = list(np.random.permutation(index))
    return [(data[index[i:i + batch_size]], label[index[i:i + batch_size]]) for i in range(0, len(data), batch_size)]
    
        
def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
#            y_pred = model.forward(x.numpy())
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()