from typing import Tuple
import numpy as np


class NumpyOp:
    '''
    The base class for Numpy operations.
    '''

    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    '''
    Matrix multiplication unit.
    '''

    def forward(self, x: np.ndarray, w: np.ndarray) -> np.ndarray:
        '''
        Args:
            x: shape(N, d)
            w: shape(d, d')

        Returns:
            shape(N, d')
        '''

        self.memory['x'] = x
        self.memory['w'] = w
        return np.matmul(x, w)

    def backward(self, grad_y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        '''
        Args:
            grad_y: shape(N, d')

        Returns:
            grad_x: shape(N, d)
            grad_w: shape(d, d')
        '''

        x: np.ndarray = self.memory['x']
        w: np.ndarray = self.memory['w']
        grad_x: np.ndarray = np.matmul(grad_y, w.T)
        grad_w: np.ndarray = np.matmul(x.T, grad_y)
        return grad_x, grad_w


class Relu(NumpyOp):
    '''
    Rectified Linear Unit.
    '''

    def forward(self, x: np.ndarray) -> np.ndarray:
        '''
        Args:
            x: shape(N, d)

        Returns:
            shape(N, d)
        '''

        self.memory['x'] = x
        return np.where(x > 0, x, 0)

    def backward(self, grad_y: np.ndarray) -> np.ndarray:
        '''
        Args:
            grad_y: shape(N, d)

        Returns:
            shape(N, d)
        '''

        x: np.ndarray = self.memory['x']
        return np.where(x > 0, grad_y, 0)


class Log(NumpyOp):
    '''
    Natural logarithm unit.
    '''

    def forward(self, x: np.ndarray) -> np.ndarray:
        '''
        Args:
            x: shape(N, d)

        Returns:
            shape(N, d)
        '''

        self.memory['x'] = x
        return np.log(x + self.epsilon)

    def backward(self, grad_y: np.ndarray) -> np.ndarray:
        '''
        Args:
            grad_y: shape(N, d)

        Returns:
            shape(N, d)
        '''

        x: np.ndarray = self.memory['x']
        return grad_y / x


class Softmax(NumpyOp):
    '''
    Softmax over last dimension.
    '''

    def forward(self, x: np.ndarray) -> np.ndarray:
        '''
        Args:
            x: shape(N, d)

        Returns:
            shape(N, d)
        '''

        y: np.ndarray = np.exp(x) / np.exp(x).sum(axis=1)[:, None]
        self.memory['y'] = y
        return y

    def backward(self, grad_y: np.ndarray) -> np.ndarray:
        '''
        Args:
            grad_y: shape(N, d)

        Returns:
            shape(N, d)
        '''

        y: np.ndarray = self.memory['y']
        return y * (grad_y - (grad_y * y).sum(axis=1)[:, None])


class NumpyLoss:
    '''
    Loss function.
    '''

    def __init__(self):
        self.target: np.ndarray = None

    def get_loss(self, pred: np.ndarray, target: np.ndarray) -> float:
        self.target = target
        return (-pred * target).sum(axis=1).mean()

    def backward(self) -> np.ndarray:
        return -self.target / self.target.shape[0]


class NumpyModel:
    '''
    An FNN model implemented in NumPy.
    '''

    def __init__(self):
        self.W1: np.ndarray = np.random.normal(size=(28 * 28, 256))
        self.W2: np.ndarray = np.random.normal(size=(256, 64))
        self.W3: np.ndarray = np.random.normal(size=(64, 10))

        self.matmul_1 = Matmul()
        self.relu_1 = Relu()

        self.matmul_2 = Matmul()
        self.relu_2 = Relu()

        self.matmul_3 = Matmul()
        self.softmax_3 = Softmax()
        self.log_3 = Log()

    def forward(self, x: np.ndarray) -> np.ndarray:
        '''
        Args:
            x: shape(N, d)

        Returns:
            shape(N, d)
        '''

        x = x.reshape(-1, 28 * 28)
        x = self.relu_1.forward(self.matmul_1.forward(x, self.W1))
        x = self.relu_2.forward(self.matmul_2.forward(x, self.W2))
        x = self.softmax_3.forward(self.matmul_3.forward(x, self.W3))
        x = self.log_3.forward(x)
        return x

    def backward(self, y: np.ndarray) -> None:
        '''
        Args:
            y: shape(N, d)
        '''

        self.log_grad = self.log_3.backward(y)
        self.softmax_grad = self.softmax_3.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)

        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)

        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

    def optimize(self, learning_rate: float):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad
