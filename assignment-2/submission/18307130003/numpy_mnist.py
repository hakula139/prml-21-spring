from typing import Any, List, Tuple
import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import (
    download_mnist,
    batch,
    # mini_batch,
    get_torch_initialization,
    plot_curve,
    one_hot,
)


def mini_batch(dataset: List[Tuple[Any, int]], batch_size=128) -> np.ndarray:
    '''
    Split the data and labels from the given dataset into batches.

    Args:
        dataset: the given dataset
        batch_size: the size of retrieved data

    Returns:
        Batches of [data, labels] pair.
    '''

    data: np.ndarray = np.array([np.array(pair[0]) for pair in dataset])
    labels: np.ndarray = np.array([pair[1] for pair in dataset])

    # Shuffle the dataset
    size: int = len(dataset)
    indices: np.ndarray = np.arange(size)
    np.random.shuffle(indices)

    batches: List[Tuple[np.ndarray, np.ndarray]] = []
    for i in range(0, size, batch_size):
        chunk: np.ndarray = indices[i:i+batch_size]
        batches.append((data[chunk], labels[chunk]))
    return batches


def numpy_run():
    '''
    Train the FNN network on MNIST.
    '''

    train_dataset, test_dataset = download_mnist()

    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()

    train_loss: List[float] = []

    epoch_number = 10
    batch_size = 256
    learning_rate = 0.5

    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset, batch_size=batch_size):
            x: np.ndarray
            y: np.ndarray = one_hot(y)

            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)

            train_loss.append(loss.item())

        x, y = batch(test_dataset)[0]
        accuracy: float = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))

    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
