from numpy_fnn import NumpyModel, NumpyLoss

import numpy as np
from matplotlib import pyplot as plt

def get_torch_initialization(numpy = True,a=0):


    def Kaiming_uniform(fan_in,fan_out,a):
        bound = 6.0 / (1 + a * a) / fan_in
        bound = bound ** 0.5
        W = np.random.uniform(low=-bound, high=bound, size=(fan_in,fan_out))
        return W

    W1 = Kaiming_uniform(28 * 28, 256, a)
    W2 = Kaiming_uniform(256, 64, a)
    W3 = Kaiming_uniform(64, 10, a)
    return W1,W2,W3
    
def plot_curve(data):
    plt.plot(range(len(data)), data, color='blue')
    plt.legend(['loss_value'], loc='upper right')
    plt.xlabel('step')
    plt.ylabel('value')
    plt.show()

def mini_batch(dataset, batch_size=128, numpy=False):
    data = []
    label = []
    for x in dataset:
        data.append(np.array(x[0]))
        label.append(x[1])
    data = np.array(data)
    label = np.array(label)

    #索引随机打乱
    siz = data.shape[0]
    ind = np.arange(siz)
    np.random.shuffle(ind)

    #划分batch
    res = []
    con = 0
    while con + batch_size <= siz:
        data_batch = data[ind[con:con + batch_size]]
        label_batch = label[ind[con:con + batch_size]]
        res.append((data_batch,label_batch))
        con += batch_size

    return res

def batch(dataset, numpy=True):
    data = []
    label = []
    for x in dataset:
        data.append(np.array(x[0]))
        label.append(x[1])
    data = np.array(data)
    label = np.array(label)
    return [(data, label)]

def one_hot(y, numpy=True):
    y_ = np.zeros((y.shape[0], 10))
    y_[np.arange(y.shape[0], dtype=np.int32), y] = 1
    return y_

def download_mnist():
    from torchvision import datasets, transforms
    
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.1307,), std=(0.3081,))
    ])
    
    train_dataset = datasets.MNIST(root="./data/", transform=transform, train=True, download=True)
    test_dataset = datasets.MNIST(root="./data/", transform=transform, train=False, download=True)
    
    return train_dataset, test_dataset

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
