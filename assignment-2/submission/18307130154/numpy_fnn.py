import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #
        ####################
        x = self.memory['x']
        W = self.memory['W']

        grad_W = np.matmul(x.T,grad_y)
        grad_x = np.matmul(grad_y,W.T)
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        x = self.memory['x']
        x1 = np.where(x > 0, 1, 0)
        grad_x = x1 * grad_y
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        x = self.memory['x']
        grad_x = 1/(x + self.epsilon)
        grad_x = grad_x * grad_y
        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """

        ####################
        #      code 4      #
        ####################
        self.memory['x'] = x
        ex = np.exp(x)
        rowsum = np.sum(ex,axis=1)
        rowsum = rowsum[:,np.newaxis]
        softmax = ex / rowsum
        self.memory['softmax'] = softmax
        return softmax

    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 5      #
        ####################
        sm = self.memory['softmax']
        Jacobs = []
        for i in range(sm.shape[0]):
            r = sm[i]
            #对每一行求雅各比矩阵（因为导数只与本行有关）
            J = np.diag(r) - np.outer(r, r)
            Jacobs.append(J)
        Jacobs = np.array(Jacobs)

        grad_y = grad_y[:,np.newaxis,:]
        grad_x = np.matmul(grad_y,Jacobs)
        grad_x = np.squeeze(grad_x,axis=1)

        return grad_x

class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))


        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None
        
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        x = self.matmul_1.forward(x, self.W1)
        x = self.relu_1.forward(x)
        x = self.matmul_2.forward(x, self.W2)
        x = self.relu_2.forward(x)
        x = self.matmul_3.forward(x, self.W3)
        x = self.softmax.forward(x)
        x = self.log.forward(x)

        return x
    
    def backward(self, y):
        

            
        ####################
        #      code 7      #
        ####################
        
        self.log_grad = self.log.backward(y)
        self.softmax_grad = self.softmax.backward(self.log_grad)

        mulgrade3,self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(mulgrade3)

        mulgrade2,self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(mulgrade2)

        self.x1_grad,self.W1_grad = self.matmul_1.backward(self.relu_1_grad)



        pass
    
    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad



    