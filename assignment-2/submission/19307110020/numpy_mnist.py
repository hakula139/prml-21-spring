import numpy as np
import torch
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, plot_curve, one_hot

def mini_batch(dataset, batch_size=128, numpy=False, shuffle=True):
    if not numpy:
        return torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle)
    import random
    datas = np.array([np.array(i[0].numpy(), i[1]) for i in dataset])
    if shuffle:
        random.shuffle(datas)
    data = [np.array(datas[0, i: i + batch_size]) for i in range(0, len(dataset), batch_size)]
    label = [np.array(datas[1, i: i + batch_size]) for i in range(0, len(dataset), batch_size)]
    return data, label


def get_torch_initialization(numpy=True):
    if numpy:
        ran = lambda x: (6 / x) ** 0.5
        Kaiming = lambda x, y: np.random.uniform(low=-ran(x), high=ran(x), size=(x,y))
        W1 = Kaiming(28 * 28, 256)
        W2 = Kaiming(256, 64)
        W3 = Kaiming(64, 10)
    else:
        fc1 = torch.nn.Linear(28 * 28, 256)
        fc2 = torch.nn.Linear(256, 64)
        fc3 = torch.nn.Linear(64, 10)
        W1 = fc1.weight.T.detach().clone().data
        W2 = fc2.weight.T.detach().clone().data
        W3 = fc3.weight.T.detach().clone().data
    return W1, W2, W3

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()

    train_loss = []

    epoch_number = 10
    learning_rate = 0.0003

    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            y_pred = model.forward(x.numpy())
            loss = numpy_loss.get_loss(y_pred, y)
            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            train_loss.append(loss.item())
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))

    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()