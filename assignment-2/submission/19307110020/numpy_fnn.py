import numpy as np


class NumpyOp:
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):

    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h

    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """

        ####################
        #      code 1      #
        ####################
        grad_W = np.matmul(self.memory['x'].T, grad_y)
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        return grad_x, grad_W


class Relu(NumpyOp):

    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """

        ####################
        #      code 2      #
        ####################
        x = self.memory['x']
        return grad_y * np.where(x > 0, 1, 0)


class Log(NumpyOp):

    def forward(self, x):
        """
        x: shape(N, c)
        """
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """

        ####################
        #      code 3      #
        ####################
        x = self.memory['x']
        return (1 / (x + self.epsilon)) * grad_y


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """

    def forward(self, x):
        """
        x: shape(N, c)
        """

        ####################
        #      code 4      #
        ####################
        self.memory['x'] = x
        x = np.array(x)
        N = x.shape[0]
        x = x - np.array([max(i) for i in x]).reshape(N, 1)  # A plus C trick
        x = np.exp(x)
        out = x / np.sum(x, axis=1).reshape(N, 1)
        self.memory['softmax'] = out
        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """

        ####################
        #      code 5      #
        ####################
        c = grad_y.shape[1]
        softmax = self.memory['softmax']
        tem = []
        for i in softmax:
            tem.append(np.diag(i) - np.outer(i, i))
        return np.matmul(grad_y.reshape(-1, 1, c), np.array(tem)).reshape(-1, c)


class NumpyLoss:
    def __init__(self):
        self.target = None

    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()

    def backward(self):
        return - self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))

        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        self.adam = {'lr': 0.001, 't': 0, 'beta1': 0.9, 'beta2': 0.999, 'epsilon': 1e-8,
                     'm1': 0, 'm2': 0, 'm3': 0, 'v1': 0, 'v2': 0, 'v3': 0}
        self.pre_grad1, self.pre_grad2, self.pre_grad3 = 0.0, 0.0, 0.0  # for Momentum

    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        ####################
        #      code 6      #
        ####################
        x = self.matmul_1.forward(x, self.W1)  # 256
        x = self.relu_1.forward(x)
        x = self.matmul_2.forward(x, self.W2)  # 64
        x = self.relu_2.forward(x)
        x = self.matmul_3.forward(x, self.W3)  # 10
        x = self.softmax.forward(x)
        x = self.log.forward(x)
        return x

    def backward(self, y):
        ####################
        #      code 7      #
        ####################

        self.log_grad = self.log.backward(y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

        pass

    def optimize(self, learning_rate, optimizer='Adam'):
        if optimizer == 'Adam':
            self.adam['lr'] = learning_rate
            self.adam['t'] += 1
            lr = self.adam['lr'] * (1 - self.adam['beta2'] ** self.adam['t']) ** 0.5 / (
                    1 - self.adam['beta1'] ** self.adam['t'])
            self.adam['m1'] = self.adam['beta1'] * self.adam['m1'] + (1 - self.adam['beta1']) * self.W1_grad
            self.adam['v1'] = self.adam['beta2'] * self.adam['v1'] + (1 - self.adam['beta2']) * (
                    self.W1_grad * self.W1_grad)
            self.W1 -= lr * self.adam['m1'] / (self.adam['v1'] ** 0.5 + self.adam['epsilon'])
            self.adam['m2'] = self.adam['beta1'] * self.adam['m2'] + (1 - self.adam['beta1']) * self.W2_grad
            self.adam['v2'] = self.adam['beta2'] * self.adam['v2'] + (1 - self.adam['beta2']) * (
                    self.W2_grad * self.W2_grad)
            self.W2 -= lr * self.adam['m2'] / (self.adam['v2'] ** 0.5 + self.adam['epsilon'])
            self.adam['m3'] = self.adam['beta1'] * self.adam['m3'] + (1 - self.adam['beta1']) * self.W3_grad
            self.adam['v3'] = self.adam['beta2'] * self.adam['v3'] + (1 - self.adam['beta2']) * (
                    self.W3_grad * self.W3_grad)
            self.W3 -= lr * self.adam['m3'] / (self.adam['v3'] ** 0.5 + self.adam['epsilon'])
        else:  # momentum optimization
            step, discount = 0.2, 0.7
            self.pre_grad1 = self.pre_grad1 * discount + self.W1_grad * step
            self.W1 -= self.pre_grad1 * step
            self.pre_grad2 = self.pre_grad2 * discount + self.W2_grad * step
            self.W2 -= self.pre_grad2 * step
            self.pre_grad3 = self.pre_grad3 * discount + self.W3_grad * step
            self.W3 -= self.pre_grad3 * step