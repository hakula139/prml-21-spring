# 实验报告2

16307130040

### 1，实验结果

在完成了对mini_batch函数的替换后，模型得以顺利进行：

![](./img/Figure_1.png)

```shell
[0] Accuracy: 0.9453
[1] Accuracy: 0.9656
[2] Accuracy: 0.9689
```

### 2,mini_batch的替换

```python
def mini_batch(dataset, batch_size=128, numpy=True):
    data = []
    label = []
    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])
    data = np.array(data)
    label = np.array(label)

    m = data.shape[0]
    permutation = list(np.random.permutation(m))
    data =data[permutation]
    label=label[permutation]

    n=m//batch_size
    mini_batches=[]
    for i in range(n):
        mini_batches.append([data[i*batch_size:(i+1)*batch_size],label[i*batch_size:(i+1)*batch_size]])

    return mini_batches
```

整体上参考了utils.py的batch函数。前半段和batch一样，将dataset中的数据分别放到data和label之中。之后，让data和label的元素顺序随机变化，再将每一个小batch的数据和标记放入对应的列表中，依次放入一个大的列表，并最终输出。

### 3，反向传播公式的推导

matmul：

#### ![](./img/matmul.jpg)

设输出y为l维的向量。

**dx：**对于每个x中的元素xi，它对y1，y2，y3,.....,yl的偏导为wi1,wi2......,wil.

dL/dxi=（dL/dy1）·wi1+（dL/dy2）·wi2+……+（dL/dyl）·wil

所以，dx=dy·WT.

**dW:** 对于W的元素wij,它对yj的偏导为xi。

dL/dwij=(dL/dyj)·xi

所以，dW=xT*dy。对于多个样本，需要求平均值。

```python
def backward(self, grad_y):
    """
    grad_y: shape(N, d')
    """
    N=grad_y.shape[0]

    grad_x=np.matmul(grad_y,self.memory['W'].T)
    grad_W=np.matmul(self.memory['x'].T,grad_y)

    
    return grad_x, grad_W
```



2，Relu函数

```python
def backward(self, grad_y):
    """
    grad_y: same shape as x
    """
    x=self.memory['x']
    grad_x = grad_y.copy()
    grad_x[x<=0]=0
    return grad_x
    
```

在xi<=0时，dyi/dxi=0，dL/dxi=0

在xi>0时，dyi/dxi=1,dL/dxi=dL/dyi

所以如上所示，如果xi大于0，则dx相应的位置照搬dy；如果xi小于等于0，则dx响应的位置设置为0.

3，log函数

```python
def backward(self, grad_y):
    """
    grad_y: same shape as x
    """
    
    x=self.memory['x']
    x[x<=self.epsilon]=self.epsilon
    grad_x=grad_y*(1/x)
    
    return grad_x
```

dyi/dxi=1/xi,dL/dxi=(dL/dyi)·1/xi

所以，dx=dy·（1/x）.

4，softmax函数

![](./img/softmax.jpg)

设x和y均为l维的向量。

则对于dyj/dxi,如果i=j，则dyj/dxi=yj-(yj)^2.如果i！=j，则有dyj/dxi=-yi·yj.

设D=diag（y）-yT·y，有dyj/dxi=Dij

这样的话，有dL/dxi=（dL/dy1）·Di1+（dL/dy2）·Di2+……+（dL/dyl）·Dil

所以，dx=dy·D

```python
def backward(self, grad_y):
    """
    grad_y: same shape as x
    """
    y=self.memory['y']
    l=y.shape[0]
    grad_x=[]
    for grad_y1,y1 in zip(grad_y,y):
        D= np.diag(y1) - np.outer(y1,y1)
        grad_x1=np.dot(grad_y1, D)
        grad_x.append(grad_x1)
    grad_x=np.array(grad_x)
    return grad_x
```

不过，在实际的实现中，要考虑到一批中不只有一个数据，要一个一个数据地逐个生成dx。

