import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, mini_batch, get_torch_initialization, plot_curve, one_hot

def mini_batch(dataset, batch_size=128, numpy=True):
    data = []
    label = []
    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])
    data = np.array(data)
    label = np.array(label)

    m = data.shape[0]
    permutation = list(np.random.permutation(m))
    data =data[permutation]
    label=label[permutation]

    n=m//batch_size
    mini_batches=[]
    for i in range(n):
        mini_batches.append([data[i*batch_size:(i+1)*batch_size],label[i*batch_size:(i+1)*batch_size]])

    return mini_batches

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)

            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)



if __name__ == "__main__":
    numpy_run()
