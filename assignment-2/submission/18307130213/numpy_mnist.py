import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, plot_curve, one_hot


def get_torch_initialization(numpy=True):

    def kaiming_uniform(fan_in, fan_out, a = 0.0):
        # a: the negative slope of the rectifier used after this layer, specially 0 for relu
        bound = (6.0 / ((1.0 + a**2) * fan_in))**0.5
        return np.random.uniform(low = -bound, high = bound, size = (fan_in, fan_out))

    return kaiming_uniform(28 * 28, 256), kaiming_uniform(256, 64), kaiming_uniform(64, 10)

def mini_batch(dataset, batch_size=128, numpy=False):
    data = []
    label = []
    for x in dataset:
        data.append(np.array(x[0]))
        label.append(x[1])
    data = np.array(data)
    label = np.array(label)

    size = data.shape[0]
    index = np.arange(size)
    np.random.shuffle(index)

    batches = []
    i = 0
    while i + batch_size <= size:
        batches.append((data[index[i:i + batch_size]], label[index[i:i + batch_size]]))
        i += batch_size

    return batches

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
