# 课程报告

18307130213

## NumpyModel 类实现

  `NumpyModel` 类的实现位于 [numpy_fnn.py](./numpy_fnn.py) 。

具体内容包括：

1. 实现 `Matmul, Relu, Log, Softmax​` 等支持前向传播和反向传播的基础算子类。
2. 完善 `NumpyModel` 的前向传播函数 `forward` 和反向传播函数 `backward` 。



## 模型训练与测试

此模型应用了新的初始化方法即非 `PyTorch​` 版初始化，在第一个 `epoch` 能达到更好的效果。

单次实验的三个 `epoch` ​中，模型的准确率分别在 95.7%, 96.6%, 97.2% 附近波动，以下为某次实验的结果：

```
[0] Accuracy: 0.9550
[1] Accuracy: 0.9651
[2] Accuracy: 0.9723
```

对应的图像为：

![](./img/numpy_minist_result.jpg)

可以看到，随着模型训练过程 `Loss​` 逐渐收敛于某个较小值。



## 数据处理和参数初始化

在 `NumPy​` 库基础上实现了 `mini_batch` 函数和 `get_torch_initialization` 函数，位于[numpy_mnist.py](./numpy_mnist.py) 。

其中 `get_torch_initialization`​ 函数使用了**何恺明**提出的 `Kaiming` 初始化方法。这也是 `PyTorch` 线性层默认的初始化方法。

究其原因可能有以下两方面的考量：

1. 若权重初始绝对值过小，导致信号逐层衰减，激活函数趋于线性。
2. 若权重初始绝对值过大，导致信号逐层放大，激活函数饱和，可能造成梯度消失等后果。

使用 `Kaiming` 初始化可以得到一个适中的随机分布值，有效地加强训练效果。

### Kaiming初始化公式

 `Kaiming​` 初始化方法相较于其他方法可以在使用 `relu` 或 `leaky_relu` 时取得更好的效果。

令 `a​` 为 `leaky_relu` 的负区域所对应的的斜率且尽量保证 $a<1$，显然对于 `relu​` 有 $a = 0$。

 `Kaiming​` 初始化即使用某个均匀分布 `U(-bound, bound)` 对参数矩阵进行初始化。

其中 `bound​` 的计算公式为
$$
bound = \sqrt[]{\frac{6}{(1 + a ^2) \times fan\_in}}
$$
 `fan_in` 为扇入部分的参数个数。

此方法的具体实现见 `get_torch_initialization` 函数。



## 反向传播算子公式推导

在本实验中，大部分算子要求进行矩阵对矩阵求导，正确的求导方式应先将矩阵向量化，进行向量对向量的求导。

![](./img/formula_1.jpg)


### Matmul算子

![](./img/formula_2.jpg)

### Relu算子

![](./img/formula_3.jpg)

### log算子

![](./img/formula_4.jpg)

### softmax算子

![](./img/formula_5.jpg)

## 总结

已完成：自动测试 `60%`

已完成：模仿 `torch_mnist.py` 的代码，在 `numpy_mnist.py` 中进行模型的训练和测试，并在报告中介绍你的实验过程与结果 `20%`

 已完成：在 `numpy_mnist.py` 中只用 `NumPy​` 实现 `mini_batch` 函数，替换 `utils.py` 中使用 `PyTorch` 实现的 `mini_batch` 函数 `10%`

已完成：在报告中推导 `numpy_fnn.py` 中实现算子的反向传播计算公式 `10%`

已完成：调研 `PyTorch​` 中权重初始化的方法，并实现代码替换 `get_torch_initialization` 函数 `10%`

已完成：相关 `bug​` 查杀工作