import numpy as np
import torch
from numpy_fnn import NumpyModel, NumpyLoss
from matplotlib import pyplot as plt


def plot_curve(data):
    plt.plot(range(len(data)), data, color='blue')
    plt.legend(['loss_value'], loc='upper right')
    plt.xlabel('step')
    plt.ylabel('value')
    plt.show()


def download_mnist():
    from torchvision import datasets, transforms

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.1307,), std=(0.3081,))
    ])

    train_dataset = datasets.MNIST(root="./data/", transform=transform, train=True, download=True)
    test_dataset = datasets.MNIST(root="./data/", transform=transform, train=False, download=True)

    return train_dataset, test_dataset


def one_hot(y, numpy=True):
    if numpy:
        y_ = np.zeros((y.shape[0], 10))
        y_[np.arange(y.shape[0], dtype=np.int32), y] = 1
        return y_
    else:
        y_ = torch.zeros((y.shape[0], 10))
        y_[torch.arange(y.shape[0], dtype=torch.long), y] = 1
    return y_


def batch(dataset, numpy=True):
    data = []
    label = []
    for each in dataset:
        data.append(each[0])
        label.append(each[1])
    data = torch.stack(data)
    label = torch.LongTensor(label)
    if numpy:
        return [(data.numpy(), label.numpy())]
    else:
        return [(data, label)]


def mini_batch(dataset, batch_size=128):
    size = dataset.train_data.shape[0]

    # fetch data and label from dataset MNIST
    data = []
    label = []
    for one_obs in dataset:
        data.append(np.array(one_obs[0]))
        label.append(np.array(one_obs[1]))

    data = np.array(data)
    label = np.array(label)

    # shuffle
    idx = np.arange(size)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]

    # split to batch, discard the last batch if remaining size < batch_size
    data_batches = []
    label_batches = []
    batched = 0
    while batched+batch_size <= size:
        data_batches.append(data[batched:batched + batch_size])
        label_batches.append(label[batched:batched + batch_size])
        batched += batch_size

    data_batches = np.array(data_batches)
    label_batches = np.array(label_batches)

    return zip(data_batches, label_batches)


def get_torch_initialization(numpy=True):
    fc1 = torch.nn.Linear(28 * 28, 256)
    fc2 = torch.nn.Linear(256, 64)
    fc3 = torch.nn.Linear(64, 10)

    if numpy:
        W1 = fc1.weight.T.detach().clone().numpy()
        W2 = fc2.weight.T.detach().clone().numpy()
        W3 = fc3.weight.T.detach().clone().numpy()
    else:
        W1 = fc1.weight.T.detach().clone().data
        W2 = fc2.weight.T.detach().clone().data
        W3 = fc3.weight.T.detach().clone().data

    return W1, W2, W3


def numpy_run(epoch_number=3, learning_rate=0.1, batch_size=128):
    train_dataset, test_dataset = download_mnist()

    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()

    train_loss = []

    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset, batch_size):
            y = one_hot(y)

            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)

            train_loss.append(loss.item())

        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))

    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()

#    for lr in [2, 4, 6]:
#        l_rate = lr/10
#        numpy_run(epoch_number=3, learning_rate=l_rate, batch_size=128)

#    for bs in [20, 40, 60]:
#        numpy_run(epoch_number=3, learning_rate=0.1, batch_size=bs)



