# FNN

## 1. Activation Function

<img src="img/md0.png"/>

### 1.1 Matmul

<img src="img/md1.png"/>

<img src="img/md2.png"/>

```python
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        x = self.memory['x']
        W = self.memory['W']

        grad_x = np.matmul(grad_y, W.T)
        grad_W = np.matmul(x.T, grad_y)
        return grad_x, grad_W
```

### 1.2 Relu:

<img src="img/md3.png"/>

```python
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = np.where(x <= 0, 0, grad_y)

        return grad_x
```

### 1.3 Log

<img src="img/md4.png"/>

```python
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = (x + self.epsilon)**(-1) * grad_y

        return grad_x
```

### 1.4 Softmax:

<img src="img/md5.png"/>

```python
    def forward(self, x):
        """
        x: shape(N, c)
        """
        exp = np.exp(x - self.epsilon)
        out = exp/exp.sum(axis=1, keepdims=True)
        self.memory['x'] = x
        self.memory['s'] = out

        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        s = self.memory['s']
        n = s.shape[1]

        grad_x = np.zeros(s.shape)
        # calculate Jacobian for ith row
        for i in range(s.shape[0]):
            jacob = np.zeros((n, n))

            for j in range(n):
                for k in range(n):
                    if j == k:
                        jacob[k, j] = s[i, k] * (1-s[i, j])
                    else:
                        jacob[k, j] = -1 * s[i, k] * s[i, j]
            # apply Jacobian to the ith row of grad_y
            grad_x[i, :] = np.matmul(jacob, grad_y[i, :])

        return grad_x
```

## 2. Model Training

### 2.1 Change mini_batch:

From documentation, we can see that mini_batch is a function that shuffles data and create small batches with user-specified batch size. The return is a batch such that the code: **for x, y in mini_batch(train_dataset):** in function **numpy_run()** can retrive appropriate training data and training label in the batch. The suffling method implemented by the code below uses ordered sampling without replacement. If the remaining data is insufficient to form a batch of the specified size, we'll drop the remaining ones.

```python
def mini_batch(dataset, batch_size=128):
    
    # get total number of observations to batch from
    size = dataset.train_data.shape[0]

    # fetch data and label
    data = []
    label = []
    for one_obs in dataset:
        data.append(np.array(one_obs[0]))
        label.append(np.array(one_obs[1]))
    data = np.array(data)
    label = np.array(label)

    # random shuffle
    idx = np.arange(size)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]

    # split to batch
    # discard the last batch if remaining size < batch_size
    data_batches = []
    label_batches = []
    batched = 0
    while batched+batch_size <= size:
        data_batches.append(data[batched:batched + batch_size])
        label_batches.append(label[batched:batched + batch_size])
        batched += batch_size

    data_batches = np.array(data_batches)
    label_batches = np.array(label_batches)

    return zip(data_batches, label_batches)
```

### 2.2 Training Result:

 - Hyper-parameters:
     * epoch number = 3
     * learning rate = 0.1


 - Model structure: 
     * 3 layers
     * dimension reduced in each layer using a weight matrix
     * softmax as the final activation function
     * loss calculated as $-y^{T}\log{\hat{y}}$

```python
def forward(self, x):
    x = x.reshape(-1, 28 * 28)
    a0 = x
    
    #First Layer
    z1 = self.matmul_1.forward(a0, self.W1)
    a1 = self.relu_1.forward(z1)
    
    #Second Layer
    z2 = self.matmul_2.forward(a1, self.W2)
    a2 = self.relu_2.forward(z2)
    
    #Third Layer
    z3 = self.matmul_3.forward(a2, self.W3)
    a3 = self.softmax.forward(z3)
    x = self.log.forward(a3)

    return x
```

```python
def backward(self, y):
    
    #Third Layer
    self.log_grad = self.log.backward(y)
    self.softmax_grad = self.softmax.backward(self.log_grad)
    self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
    
    #Second Layer
    self.relu_2_grad = self.relu_2.backward(self.x3_grad)
    self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
    
    #First Layer
    self.relu_1_grad = self.relu_1.backward(self.x2_grad)
    self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

    pass
```

Result:
```python
[0] Accuracy: 0.9482
[1] Accuracy: 0.9643
[2] Accuracy: 0.9735
```

<img src="img/figure 1.png" width=450 height=300/>

## 3. Change of Parameters

**Learning Rate [0.2, 0.4, 0.6]**

<img src="img/lr1.png" width=450 height=300/>
<img src="img/lr2.png" width=450 height=300/>
<img src="img/lr3.png" width=450 height=300/>

**Batch Size [20, 40, 60]**

<img src="img/bs1.png" width=450 height=300/>
<img src="img/bs2.png" width=450 height=300/>
<img src="img/bs3.png" width=450 height=300/>

## 4. Optimization

**Momentum:**

<img src="img/md6.png"/>

```python
#Extends NumpyModel
#Save step size
self.delta1 = 0
self.delta2 = 0
self.delta3 = 0
    
#Extends the optimize function defined in numpy_fnn.py
if method == "Momentum":
    self.delta1 = momentum * self.delta1 - learning_rate * self.W1_grad
    self.W1 += self.delta1

    self.delta2 = momentum * self.delta2 - learning_rate * self.W2_grad
    self.W2 += self.delta2

    self.delta3 = momentum * self.delta3 - learning_rate * self.W3_grad
    self.W3 += self.delta3
```

Result:
```python
[0] Accuracy: 0.9654
[1] Accuracy: 0.9712
[2] Accuracy: 0.9726
```

<img src="img/figure 2.png" width=450 height=300/>

Discussion:

Momentum method differs from gradient descent in that it adds a short term memory for step size. Momentum, a number between 0 and 1, is the weight given to the previous step size. Momentum smoothes the path (visual demonstration below, [ref](https://dominikschmidt.xyz/nesterov-momentum/)). A greater momentum results in a more direct path with less changes in direction. Or, from another perspective, accelarates towards the optimum. If momentum = 0, the method reduces to Gradient Descent.

<img src="img/gd1.png" width=450 height=300 left/> <img src="img/m1.png" width=450 height=300 right/>

Momentum $\beta$ can be thought of the weight of the previous force, while learning rate $\alpha$ can be thought of the weight of the current force. Intuitively, the optimal training model needs an optimal combination of $\alpha$ and $\beta$ depending on the eigenvalues of a convex function. Inappropriate values could lead to divergence.


```python

```
