import numpy as np


class NumpyOp:

    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):

    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)

        return h

    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        x = self.memory['x']
        W = self.memory['W']

        grad_x = np.matmul(grad_y, W.T)
        grad_W = np.matmul(x.T, grad_y)
        return grad_x, grad_W


class Relu(NumpyOp):

    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = np.where(x <= 0, 0, grad_y)

        return grad_x


class Log(NumpyOp):

    def forward(self, x):
        """
        x: shape(N, c)
        """
        out = np.log(x + self.epsilon)
        self.memory['x'] = x

        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = (x + self.epsilon)**(-1) * grad_y

        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """

    def forward(self, x):
        """
        x: shape(N, c)
        """
        exp = np.exp(x + self.epsilon)
        out = exp/exp.sum(axis=1, keepdims=True)
        self.memory['x'] = x
        self.memory['s'] = out

        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        s = self.memory['s']
        n = s.shape[1]

        grad_x = np.zeros(s.shape)
        # calculate Jacobian for ith row
        for i in range(s.shape[0]):
            jacob = np.zeros((n, n))

            for j in range(n):
                for k in range(n):
                    if j == k:
                        jacob[k, j] = s[i, k] * (1-s[i, j])
                    else:
                        jacob[k, j] = -1 * s[i, k] * s[i, j]
            # apply Jacobian to the ith row of grad_y
            grad_x[i, :] = np.matmul(jacob, grad_y[i, :])

        return grad_x


class NumpyLoss:

    def __init__(self):
        self.target = None

    def get_loss(self, pred, target):
        self.target = target
        return (-1 * pred * target).sum(axis=1).mean()

    def backward(self):
        return -1 * self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.epsilon = 1e-12

        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))

        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        # 如需要，以下变量在optimize中更新
        self.delta1 = 0
        self.delta2 = 0
        self.delta3 = 0

    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        a0 = x
        z1 = self.matmul_1.forward(a0, self.W1)
        a1 = self.relu_1.forward(z1)
        z2 = self.matmul_2.forward(a1, self.W2)
        a2 = self.relu_2.forward(z2)
        z3 = self.matmul_3.forward(a2, self.W3)
        a3 = self.softmax.forward(z3)
        x = self.log.forward(a3)

        return x

    def backward(self, y):
        self.log_grad = self.log.backward(y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

        pass

    def optimize(self, learning_rate, method="GD", momentum=0.9, gamma=0.9):
        if method == "GD":
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad

        # idea: want different learning rate on different dimensions
        if method == "Momentum":
            self.delta1 = momentum * self.delta1 - learning_rate * self.W1_grad
            self.W1 += self.delta1

            self.delta2 = momentum * self.delta2 - learning_rate * self.W2_grad
            self.W2 += self.delta2

            self.delta3 = momentum * self.delta3 - learning_rate * self.W3_grad
            self.W3 += self.delta3


