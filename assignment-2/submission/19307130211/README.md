# Assignment2

姓名：陈洋

学号：19307130211



### 1.算子推算：

首先先推算得到FNN网络需要的算子，并编写相应代码。

##### Matmul:

对于Matmul的计算公式为：
$$
Y_{ij}=\sum_{1\leq k\leq d} W_{ik}\times X{kj}
$$
又因为[神经网络与深度学习-邱锡鹏](https://nndl.github.io/nndl-book.pdf)书中公式B.21![20210429002059041](img/image-20210429002059041.png)

得到
$$
\frac{\partial Y}{\partial X}=\frac{\partial(W\times X)}{\partial X}=W^T\\\\
\frac{\partial Y}{\partial X}=\frac{\partial(W\times X)}{\partial W}=X^T
$$
又因为链式法则，grad_y已知，所以:

$grad\_x=grad\_y\times W^T $   即`grad_x=np.matmul(grad_y,W.T)    `

$grad\_w=X^T\times grad\_y$   即`grad_W=np.matmul(x.T,grad_y)`

##### ReLU:

对于ReLU的计算公式：
$$
Y_{ij}= \begin{cases} X{ij}, X{ij}\geq 0 \\\\ 0, X{ij}<0 \end{cases}
$$
所以
$$
\frac{\partial Y_{ij}}{\partial X{ij}}= \begin{cases} 1, X{ij}\geq 0 \\\\ 0, X{ij}<0 \end{cases}
$$
又因为grad_y已知，所以得到:

![image-20210430110418797](img/image-20210430110418797.png)

##### Log:

Log 的计算公式为
$$
Y_{ij}=\ln(X_{ij}+\epsilon)
$$
所以
$$
\frac {\partial Y_{ij}}{\partial X_{ij}}= \frac {1}{(X_{ij}+\epsilon)}
$$
已知grad_y,所以:

![image-20210430110239276](img/image-20210430110239276.png)

即对应于代码：

~~~python
mask=1/(x+self.epsilon) #求1/(X+e)
grad_x=mask*grad_y      
~~~

##### Softmax：

softmax的计算公式为
$$
Y_{ij}=\frac{\exp\{X_{ij}\}}{\sum_{k=1}^d\exp\{X_{ik} \}}
$$
所以对应的代码为

~~~python
x_exp=np.exp(x) #先对X中每个元素求e^Xij,
out=x_exp / np.sum(x_exp,axis=1,keepdims=True)#axis=1,压缩列，将矩阵的每一行相加，并保持x的维度
~~~

其导数的推导：

对于每一行的softmax是单独的，所以这里一行为例，证明过程参考了[神经网络与深度学习-邱锡鹏](https://nndl.github.io/nndl-book.pdf)书中推导：

![image-20210429183140858](img/image-20210429183140858.png)

所以对于每一行的softmax的Jacobs矩阵为：
$$
J=diag(Y_i)-Y_i\times Y_i^T
$$
所以对应的代码为

~~~Python
J=np.diag(temp)-np.outer(temp,temp)
#每一行的导数为grad_Yi*J,所以grad_Xi计算代码如下：
t=np.dot(grad_y[i],Jacobs[i])
~~~

### 2.网络的完善与实现

通过对torch_mnist.py的阅读得到网络的结构：

`input->全连接层->激活函数ReLU->全连接层->激活函数ReLU->全连接层->softmax->Log`

所以其实只要按照同样的顺序调用之前实现好的算子，就可以完成实验的要求。

##### 1.实验

填写完代码后，初次运行numpy_mnist.py,结果如下：

其中 epoch=3,learning_rate=0.1。

~~~shell
[0] Accuracy: 0.9487
[1] Accuracy: 0.9647
[2] Accuracy: 0.9690
~~~

![figure_1](img/figure_1.png)

##### 2.mini_batch 的实现

mini_batch()函数源代码就是使用pytorch包里面的dataloader实现了对数据分组和打乱，所以我们实现的mini_batch()需要实现这两个功能。

首先将data和label分别取出放在list中，通过numpy.random.choice()函数得到一个用来随机打乱的index。

~~~python
idx=np.random.choice(Num,Num,replace=False)
~~~

因为需要使用index作为索引，所以原先为list的数据需要使用np.array()进行转换，所以在训练过程中需要修改代码，如下。

~~~python
#y_pred = model.forward(x.numpy())
y_pred = model.forward(x)
~~~

##### 3.对模型的进一步讨论

增大epoch，使epoch=10，15，20：

<center class="half">
    <img src="img/Figure_4.png" width="450"/>
    <img src="img/Figure_7.png" width="450"/>
    <img src="img/Figure_8.png" width="450"/>
    <center style="color:#C0C0C0;text-decoration:underline">epoch依次为10,15,20</center>
</center>


可以看到epoch越大，最后曲线越平滑。在图像前面部分一直有较大的抖动，猜测为learning_rate过大导致，于是下面对learning_rate讨论。

使用不同的学习率，使learning_rate=0.05,0.01（epoch=30）：

| epoch | learning_rate=0.1 | learning_rate=0.05 | learning_rate=0.01 |
| ----- | ----------------- | ------------------ | ------------------ |
| 0     | 0.9526            | 0.9283             | 0.8705             |
| 5     | 0.9784            | 0.9725             | 0.9377             |
| 10    | 0.9786            | 0.9783             | 0.9530             |
| 15    | 0.9807            | 0.9799             | 0.9623             |
| 20    | 0.9819            | 0.9810             | 0.9688             |
| 25    | 0.9816            | 0.9803             | 0.9714             |
| 30    | 0.9819            | 0.9802             | 0.9728             |

在测试了epoch=20情况下，未得到明显收敛的图像，于是将epoch增大为30。

<center class="half">
    <img src="img/Figure_9.png" width="450"/>
    <img src="img/Figure_5.png" width="450"/>
    <img src="img/Figure_6.png" width="450"/>
    <center style="color:#C0C0C0;text-decoration:underline">learning_rate依次为0.1,0.05,0.01</center>
</center>


在改变学习率后，收敛的速度显著变慢，在学习率为0.1时反而因为快速到达收敛，其曲线更加平滑，与猜测相反。

### 3.扩展探究

##### 1.momentum

Momentum在梯度下降中加入了惯性这一概念，使得梯度在方向不变的维度上收敛速度加快，梯度方向有所改变的维度上更新速度变慢，这样就可以加快收敛并减小震荡:
$$
m_t=\beta_1\cdot m_{t-1}+(1-\beta_2)\cdot g_t\\\\
w_{t+1}=w_t-\eta\cdot m_t
$$
其中mt是重新计算得到的下降梯度。

##### 2.Adam

Adam = Adaptive + Momentum，Adam相比起momentum，多了一个Adaptive部分：我们希望能够根据参数的重要性而对不同参数进行不同程度的更新：即对于经常更新的参数，我们积累了大量关于它的知识，不希望被单个个体影响，希望学习速度慢一点；而对于偶尔更新的参数则相反，希望学习速率更大一些。

而度量历史更新频率的方法为，使用一个二阶动量——该维度上迄今为止的所有梯度值的平方和:
$$
V_t=\sum_{\tau=1}^t(g_\tau^2)
$$
但是这一方法也存在一些问题，因为下降梯度为
$$
\eta_t=\alpha\cdot m_t/\sqrt{V_t}
$$
而Vt单点递增就会造成学习率单调递减到0，使得训练过程提前结束，所以修改为：
$$
V_t=\beta_2\cdot V_{t-1}+(1-\beta_2)\cdot g_t^2
$$
加上Momentum中的公式算得mt，然后有上面的公式一起算得：
$$
w_{t+1}=w_t-\eta_t
$$
(以上内容参考网站：https://zhuanlan.zhihu.com/p/32230623)

##### 3.代码实现：

具体参见numpy_fnn.py中新定义的两个函数optimizeM和optimizeA分别代表Momentum和Adam两种优化算法的代码。

同时为了方便函数的实现，在模型初始化中新添加了以下代码：

~~~python
	self.W1_mt=0
    self.W2_mt=0
    self.W3_mt=0

    self.W1_vt=0
    self.W2_vt=0
    self.W3_vt=0
	
    self.epsilon = 1e-8
~~~

##### 4.实验

实验结果：其中epoch为20，beta_1=0.9,beta_2=0.999，Adam的learning_rate=0.001。

| epoch | Accuracy(normal) | Accuracy(M) | Accuracy(A) |
| ----- | ---------------- | ----------- | ----------- |
| 0     | 0.9470           | 0.9420      | 0.9678      |
| 5     | 0.9780           | 0.9770      | 0.9791      |
| 10    | 0.9819           | 0.9795      | 0.9795      |
| 15    | 0.9816           | 0.9822      | 0.9804      |
| 20    | 0.9827           | 0.9828      | 0.9795      |

<center class="half">
    <img src="img/Figure_4.png" width="450"/>
    <img src="img/Figure_7.png" width="450"/>
    <img src="img/Figure_8.png" width="450"/>
    <center style="color:#C0C0C0;text-decoration:underline">依次为原始方法，Momentum，Adam</center>
</center>


实验结果显示，Momentum方法和原有的方法相比，无明显优势，但是Adam方法可以看到，在训练阶段初，其抖动较小，且收敛的速度较快，在完整的数据中很快就达到了0.9827这样几乎顶点的水平，但是在后续中，抖动比起前两种方法都要剧烈，且最后结果也更差。

论文([https://arxiV.org/pdf/1711.05101.pdf](https://arxiv.org/pdf/1711.05101.pdf))提到一个重要原因是因为Adam中L2正则化项并不像在SGD中那么有效，有两个原因：

* L2正则和Weight Decay在Adam这种自适应学习率算法中并不等价

* 使用Adam优化带L2正则的损失并不有效

具体内容参见论文。

