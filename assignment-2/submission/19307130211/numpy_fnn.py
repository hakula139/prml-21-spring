import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12
        


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #
        ####################
        x=self.memory['x']
        W=self.memory['W']
        grad_x=np.matmul(grad_y,W.T)
        grad_W=np.matmul(x.T,grad_y)
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        x=self.memory['x']
        #通过判断是否大于1，得到不同的导数

        mask=np.where(x>0,np.ones_like(x),np.zeros_like(x))
        grad_x=mask*grad_y
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        # grad_x = grad_y * np.reciprocal(self.memory['x'] + self.epsilon)
        x=self.memory['x']
        mask=1/(x+self.epsilon)
        grad_x=mask*grad_y
        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        ####################
        #      code 4      #
        ####################
        self.memory['x']=x
        x_exp=np.exp(x)
        out=x_exp / np.sum(x_exp,axis=1,keepdims=True)
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        ####################
        #      code 5      #
        ####################
        x=self.memory['x']
        softmax=self.forward(x)
        N=x.shape[0]
        Jacobs=[]
    
        for i in range(N):
            temp=softmax[i]
            J=np.diag(temp)-np.outer(temp,temp)#计算每一层的Jacobs矩阵    
            Jacobs.append(J)
        Jacobs=np.array(Jacobs)

        grad_x=[]
        for i in range(N):
            t=np.dot(grad_y[i],Jacobs[i])#通过每一层的矩阵
            grad_x.append(t)
        grad_x=np.array(grad_x)    
        return grad_x


class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))

        #为了拓展optimize设置的初始化
        self.W1_mt=0
        self.W2_mt=0
        self.W3_mt=0

        self.W1_vt=0
        self.W2_vt=0
        self.W3_vt=0

        self.epsilon = 1e-8
        
        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()
        
        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        #模型架构：输入X->全连接层->激活函数ReLU->全连接层->激活函数ReLu->全连接层->softmax层->log->loss
        x=self.matmul_1.forward(x, self.W1)
        x=self.relu_1.forward(x)
        x=self.matmul_2.forward(x, self.W2)
        x=self.relu_2.forward(x)
        x=self.matmul_3.forward(x, self.W3)
        x=self.softmax.forward(x)
        x=self.log.forward(x)
        return x
    
    def backward(self, y):
        
        ####################
        #      code 7      #
        ####################
        self.log_grad=self.log.backward(y)
        self.softmax_grad=self.softmax.backward(self.log_grad)
        self.x3_grad,self.W3_grad=self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad=self.relu_2.backward(self.x3_grad)
        self.x2_grad,self.W2_grad=self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad=self.relu_1.backward(self.x2_grad)
        self.x1_grad,self.W1_grad=self.matmul_1.backward(self.relu_1_grad)
        pass
    
    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad

    def optimizeM(self,learning_rate,beta_1=0.9):
        self.W1_mt=beta_1*self.W1_mt+(1-beta_1)*self.W1_grad
        self.W2_mt=beta_1*self.W2_mt+(1-beta_1)*self.W2_grad
        self.W3_mt=beta_1*self.W3_mt+(1-beta_1)*self.W3_grad

        self.W1 -= learning_rate * self.W1_mt
        self.W2 -= learning_rate * self.W2_mt
        self.W3 -= learning_rate * self.W3_mt


    def optimizeA(self,learning_rate=0.001,beta_1=0.9,beta_2=0.999):
        self.W1_mt=beta_1*self.W1_mt+(1-beta_1)*self.W1_grad
        self.W2_mt=beta_1*self.W2_mt+(1-beta_1)*self.W2_grad
        self.W3_mt=beta_1*self.W3_mt+(1-beta_1)*self.W3_grad

        self.W1_vt=beta_2*self.W1_vt+(1-beta_2)*self.W1_grad*self.W1_grad
        self.W2_vt=beta_2*self.W2_vt+(1-beta_2)*self.W2_grad*self.W2_grad
        self.W3_vt=beta_2*self.W3_vt+(1-beta_2)*self.W3_grad*self.W3_grad

        self.W1-=learning_rate*self.W1_mt/(self.W1_vt**0.5+self.epsilon)
        self.W2-=learning_rate*self.W2_mt/(self.W2_vt**0.5+self.epsilon)
        self.W3-=learning_rate*self.W3_mt/(self.W3_vt**0.5+self.epsilon)

        