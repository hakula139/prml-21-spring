import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch,  get_torch_initialization, plot_curve, one_hot


def mini_batch(dataset,batch_size=128,numpy=False):
    

    #得到data和label
    data=[]
    label=[]
    for each in dataset:
        data.append(np.array(each[0]))
        label.append(np.array(each[1]))
    
    #得到打乱的索引
    Num=dataset.__len__()
    idx=np.random.choice(Num,Num,replace=False)
    #打乱数据
    data=np.array(data)[idx,]
    label=np.array(label)[idx,]

    #对数据进行分割
    result=[]
    i=0
    while i*batch_size <= Num:
        start=batch_size*i
        if (i+1)*batch_size<=Num:
            end=(i+1)*batch_size
        else:
            end=Num
        result.append((data[start:end],label[start:end]))
        i=i+1
    return result

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 30
    learning_rate = 0.1

    opt_m=False
    opt_v=False 

    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            if opt_m:
                model.optimizeM(learning_rate)
            elif opt_v:
                model.optimizeA()
            else:    
                model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
