import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss, Matmul, Relu, Log, Softmax
from utils import download_mnist, batch, mini_batch, get_torch_initialization, plot_curve, one_hot
from matplotlib import pyplot as plt

np.random.seed(1)


def gaussian_noise(img, mu=0.1307, std=0.3081):
    """
    产生随机噪声，噪声的均值与方差与图像采样数据一致
    :param img: 待处理的数据
    :param mu: 噪声均值
    :param std: 噪声方差
    :return: 经过噪声处理后的图像
    """
    epsilon = 1
    sigma = std
    noise = np.random.normal(mu, sigma, img.shape) * epsilon
    # 设置阈值
    out = np.clip(img + noise, -1, 1)
    return out


def get_numpy_initialization(mode='kaiming', test=False, neuron_shape=(256, 64)):
    """
    使用numpy模拟torch初始化
    :param neuron_shape: 控制隐藏层神经元数量
    :param mode: 初始化模式:normal Xavier kaiming_uniform
    :param test: 当用来进行探究初始化实验时，设为True，此时每层均为4096个神经元，否则为False，与fnn模型结构一致
    :return: 参数W1 W2 W3
    """
    # 设置神经元数量
    if test:
        d0 = 28 * 28
        d1 = 4096
        d2 = 4096
        d3 = 4096
    else:
        d0 = 28 * 28
        d1, d2 = neuron_shape
        d3 = 10
    # 设置初始化方式
    if mode == 'normal':
        factor = 0.01  # 缩放因子，用来控制参数初始化的范围
        W1 = np.random.normal(size=(d0, d1)) * factor
        W2 = np.random.normal(size=(d1, d2)) * factor
        W3 = np.random.normal(size=(d2, d3)) * factor

        return W1, W2, W3

    if mode == 'Xavier':
        W1 = np.random.normal(size=(d0, d1)) * (2 / (np.sqrt(d0) + np.sqrt(d1)))
        W2 = np.random.normal(size=(d1, d2)) * (2 / (np.sqrt(d1) + np.sqrt(d2)))
        W3 = np.random.normal(size=(d2, d3)) * (2 / (np.sqrt(d2) + np.sqrt(d3)))
        return W1, W2, W3

    elif mode == 'kaiming':
        bound1 = np.sqrt(6 / d0)
        bound2 = np.sqrt(6 / d1)
        bound3 = np.sqrt(6 / d2)

        W1 = (np.random.rand(d0, d1) - .5) * 2 * bound1
        W2 = (np.random.rand(d1, d2) - .5) * 2 * bound2
        W3 = (np.random.rand(d2, d3) - .5) * 2 * bound3

        return W1, W2, W3


def visualize_weight(mode='kaiming', act='ReLU', test=True):
    """
    用来可视化每层的参数分布情况
    :param mode: 使用的初始化方式
    :param act: 激活函数类型：tanh ReLU
    :param test: 当用来进行探究初始化实验时，设为True，此时每层均为4096个神经元，否则为False，与fnn模型结构一致
    :return: None
    """
    print(mode, act)
    x = np.random.rand(16, 28 * 28)  # 随机初始化输入
    W1, W2, W3 = get_numpy_initialization(mode, test)  # 获取初始化参数
    W = [0, W1, W2, W3]

    # 下面这个循环模拟了一个具有三个隐藏层的神经网络
    for i in range(1, 4):
        if act == 'tanh':
            x = np.tanh(x.dot(W[i]))
        elif act == 'ReLU':
            x = np.maximum(0, x.dot(W[i]))  # 过滤掉小于0的值，模拟ReLU
        else:
            raise ValueError("WRONG ACTIVATION")
        # 获取每一层经过激活后的输出
        mean = np.mean(x)
        std = np.std(x)
        # 绘制分布直方图
        plt.subplot(1, 3, i)
        plt.hist(x.flatten())
        if act == 'ReLU':
            lim = 5
        else:
            lim = 1

        plt.xlim(-lim, lim)
        plt.title(f'layer{i}\nmean={mean:.2f}\nstd={std:.2f}')

    plt.show()


def mini_batch_numpy(dataset, batch_size=128, noise=False):
    """
    使用numpy实现minibatch
    :param dataset: torch获取的MNIST数据集
    :param batch_size: 批大小
    :return: 一个list，其中每个元素是一个batch(x, y)
    """
    # 对数据进行标准化处理 mean=(0.1307,), std=(0.3081,)
    X = dataset.data.numpy() / 255
    mean = 0.1307
    std = 0.3081
    X = (X - mean) / std
    y = dataset.targets.numpy()

    # 添加高斯噪声
    if noise:
        X = gaussian_noise(X)

    # 打乱样本和标签
    n = X.shape[0]
    idx = np.arange(n)
    np.random.shuffle(idx)
    X = X[idx]
    y = y[idx]

    # 用于切分数据生成batches
    iter_num = int(np.ceil(n / batch_size))
    dataloader = \
        [(X[i * batch_size: (i + 1) * batch_size], y[i * batch_size: (i + 1) * batch_size])
         # 处理当最后的部分不足batch size时
         if (i + 1) * batch_size <= n
         else (X[i * batch_size:], y[i * batch_size:])
         for i in range(iter_num)]
    return dataloader


class NumpyModel_neuron:
    def __init__(self, neuron_shape=(256, 64)):
        h2, h3 = neuron_shape
        self.W1 = np.random.normal(size=(28 * 28, h2))
        self.W2 = np.random.normal(size=(h2, h3))
        self.W3 = np.random.normal(size=(h3, 10))

        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新.softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        self.memory = {}

    def forward(self, x):
        x = x.reshape(-1, 28 * 28)

        x = self.relu_1(self.matmul_1(x, self.W1))
        x = self.relu_2(self.matmul_2(x, self.W2))
        x = self.matmul_3(x, self.W3)
        x = self.softmax(x)
        x = self.log(x)

        return x

    def backward(self, y):
        grad_y = y

        self.log_grad = self.log.backward(grad_y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

    def optimize(self, learning_rate, mode='SGD', h_params=None):
        """
        优化器，用于更新参数
        :param learning_rate: 学习率
        :param mode: 优化器类型，包括SGD AdaGrad RMSProp Adam
        :param h_params: 优化器所需的超参数
        :return: None
        """
        if mode == 'SGD':
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
        elif mode == 'momentum':
            # beta 一般取0.9
            beta = h_params[0]
            # 获取上一时刻的动量，初始值为0
            V1, V2, V3 = self.memory.get('V1', 0), self.memory.get('V2', 0), self.memory.get('V3', 0)
            # 更新动量
            V1 = beta * V1 + (1 - beta) * self.W1_grad
            V2 = beta * V2 + (1 - beta) * self.W2_grad
            V3 = beta * V3 + (1 - beta) * self.W3_grad
            # 存储当前动量
            self.memory['V1'] = V1
            self.memory['V2'] = V2
            self.memory['V3'] = V3
            # 更新参数
            self.W1 -= learning_rate * V1
            self.W2 -= learning_rate * V2
            self.W3 -= learning_rate * V3

        elif mode == 'AdaGrad':
            # 学习率大于1e-2以后梯度会消失
            epsilon = 1e-7
            # 读取历史梯度值平方和
            r1, r2, r3 = self.memory.get('r1', 0), self.memory.get('r2', 0), self.memory.get('r3', 0)
            # 更新历史梯度值平方和
            r1 += np.square(self.W1_grad)
            r2 += np.square(self.W2_grad)
            r3 += np.square(self.W3_grad)
            # 存储历史梯度值平方和
            self.memory['r1'] = r1
            self.memory['r2'] = r2
            self.memory['r3'] = r3
            # 更新参数
            self.W1 -= learning_rate / (epsilon + np.sqrt(r1)) * self.W1_grad
            self.W2 -= learning_rate / (epsilon + np.sqrt(r2)) * self.W2_grad
            self.W3 -= learning_rate / (epsilon + np.sqrt(r3)) * self.W3_grad

        elif mode == 'RMSProp':
            # lr 1e-3, rho 0.999
            epsilon = 1e-6
            rho = h_params[0]
            # 读取历史梯度值平方和
            r1, r2, r3 = self.memory.get('r1', 0), self.memory.get('r2', 0), self.memory.get('r3', 0)
            # 更新历史梯度值平方和
            r1 = rho * r1 + (1 - rho) * np.square(self.W1_grad)
            r2 = rho * r2 + (1 - rho) * np.square(self.W2_grad)
            r3 = rho * r3 + (1 - rho) * np.square(self.W3_grad)
            # 存储历史梯度值平方和
            self.memory['r1'] = r1
            self.memory['r2'] = r2
            self.memory['r3'] = r3
            # 更新参数
            self.W1 -= learning_rate / (epsilon + np.sqrt(r1)) * self.W1_grad
            self.W2 -= learning_rate / (epsilon + np.sqrt(r2)) * self.W2_grad
            self.W3 -= learning_rate / (epsilon + np.sqrt(r3)) * self.W3_grad

        elif mode == 'Adam':
            # lr=1e-3, rho1=0.9, rho2=0.999
            epsilon = 1e-8
            rho1, rho2 = h_params[0], h_params[1]
            # 确定当前时刻值
            t = self.memory.get('t', 0)
            t += 1
            # 读取历史梯度值平方和以及历史动量，初始值均为0
            s1, s2, s3 = self.memory.get('s1', 0), self.memory.get('s2', 0), self.memory.get('s3', 0)
            r1, r2, r3 = self.memory.get('r1', 0), self.memory.get('r2', 0), self.memory.get('r3', 0)
            # 更新动量
            s1 = rho1 * s1 + (1 - rho1) * self.W1_grad
            s2 = rho1 * s2 + (1 - rho1) * self.W2_grad
            s3 = rho1 * s3 + (1 - rho1) * self.W3_grad
            # 存储动量
            self.memory['s1'] = s1
            self.memory['s2'] = s2
            self.memory['s3'] = s3
            # 更新梯度值平方和
            r1 = rho2 * r1 + (1 - rho2) * np.square(self.W1_grad)
            r2 = rho2 * r2 + (1 - rho2) * np.square(self.W2_grad)
            r3 = rho2 * r3 + (1 - rho2) * np.square(self.W3_grad)
            # 存储梯度值平方和
            self.memory['r1'] = r1
            self.memory['r2'] = r2
            self.memory['r3'] = r3
            # 修正当前动量
            s1_hat = s1 / (1 - np.power(rho1, t))
            s2_hat = s2 / (1 - np.power(rho1, t))
            s3_hat = s3 / (1 - np.power(rho1, t))
            # 修正当前梯度平方和
            r1_hat = r1 / (1 - np.power(rho2, t))
            r2_hat = r2 / (1 - np.power(rho2, t))
            r3_hat = r3 / (1 - np.power(rho2, t))
            # 更新梯度
            self.W1 -= learning_rate / (epsilon + np.sqrt(r1_hat)) * s1_hat
            self.W2 -= learning_rate / (epsilon + np.sqrt(r2_hat)) * s2_hat
            self.W3 -= learning_rate / (epsilon + np.sqrt(r3_hat)) * s3_hat


def numpy_run(arg_list, neuron_shape=(256 * 1, 64 * 1), modify=None, noise=None):
    """
    训练开始
    :param modify: 进行调整神经元实验的参数
    :param neuron_shape: 用来控制隐藏层神经元数量
    :param arg_list: 优化器类型与超参数列表，[1e-3, 'Adam', [0.9, 0.999]]
    :return: None
    """
    # 获取超参数
    if noise is None:
        noise = [False, False]
    lr = arg_list[0]
    mode = arg_list[1]
    print(mode)
    h_params = arg_list[-1]

    train_dataset, test_dataset = download_mnist()
    model = NumpyModel()
    if modify == 'neuron':
        model = NumpyModel_neuron(neuron_shape=neuron_shape)
    elif modify == 'layer':
        model = NumpyModel_layer(neuron_shape=neuron_shape)
    numpy_loss = NumpyLoss()
    # 初始化选择
    # model.W1, model.W2, model.W3 = get_torch_initialization()
    # model.W1, model.W2, model.W3 = get_numpy_initialization()
    model.W1, model.W2, model.W3 = get_numpy_initialization(neuron_shape=neuron_shape)

    train_loss = []
    test_acc = []
    test_acc_noise = []
    test_loss = []

    epoch_number = 3
    learning_rate = lr

    for epoch in range(epoch_number):
        # 选择minibatch方法
        # for x, y in mini_batch(train_dataset):
        for x, y in mini_batch_numpy(train_dataset, noise = noise[0]):
            # print(x.numpy().shape)
            y = one_hot(y)

            y_pred = model.forward(x)  # 已经在mini_batch_numpy中改为numpy类型
            # y_pred = model.forward(x.numpy()) #如果使用torch_mini_batch启用此行
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate, mode, h_params)

            train_loss.append(loss.item())

        x, y = batch(test_dataset)[0]
        # x = gaussian_noise(x)
        y_pred = model.forward(x)
        accuracy = np.mean((y_pred.argmax(axis=1) == y))
        test_acc.append(accuracy)

        if noise[1]:
            x = gaussian_noise(x)
            y_pred = model.forward(x)
            accuracy = np.mean((y_pred.argmax(axis=1) == y))
            test_acc_noise.append(accuracy)

        y = one_hot(y)
        loss = (-y_pred * y).sum(axis=1).mean()
        test_loss.append(loss)
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))

    plot_curve(train_loss, str(neuron_shape))
    if modify is not None:
        return test_acc
    elif noise[1]:
        return test_acc, test_acc_noise
    else:
        return train_loss


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1 and sys.argv[1] == 'v':
        # 可视化权重
        visualize_weight('normal', 'tanh')
        visualize_weight('Xavier', 'tanh')
        visualize_weight('Xavier', 'ReLU')
        visualize_weight('kaiming', 'ReLU')
    elif len(sys.argv) > 1 and sys.argv[1] == 'o':
        # 比较优化器
        cases = [
            [0.1, 'SGD', None],
            [0.1, 'momentum', [0.9]],
            [1e-2, 'AdaGrad', None],
            [1e-4, 'RMSProp', [0.999]],
            [1e-3, 'Adam', [0.9, 0.999]]
        ]

        loss = []
        for case in cases:
            loss.append(numpy_run(case))
        step = 1
        # loss变化图
        color = ['k', 'y', 'c', 'b', 'r']
        line = ['-', '-', '--', '-.', ':']
        optim = [case[1] for case in cases]
        for i in range(5):
            plt.subplot(3, 2, i + 1)
            plt.plot(range(len(loss[i]))[::step], loss[i][::step], color=color[i], linestyle=line[i], label=optim[i])
            plt.xlabel('step')
            plt.ylabel('value')
            plt.legend()
        plt.show()

        # 取对数绘图
        for i in range(5):
            plt.plot(range(len(loss[i]))[::step], np.log(loss[i][::step]) / np.log(100), color=color[i], linestyle=line[i],
                     label=optim[i])
        plt.legend()
        plt.xlabel('step')
        plt.ylabel('log_value')
        plt.show()
        
    elif len(sys.argv) > 1 and (sys.argv[1] == 'm' or sys.argv[1] == 'd' or sys.argv[1] == 'f'):
        # 神经元数量实验
        if sys.argv[1] == 'm':
            ns = (256 * 1, 64 * 1)
        elif sys.argv[1] == 'n':
            ns = (256 * 2, 64 * 2)
        elif sys.argv[1] == 'k':
            ns = (256 * 5, 64 * 5)
        else:
            raise ValueError("WRONG ARGV")

        acc = numpy_run([0.1, 'SGD', None], neuron_shape=ns, modify='neuron')
        plt.plot(range(len(acc)), acc)
        plt.title(f"test acc {ns}")
        plt.ylabel("acc")
        plt.xlabel("epoch")
        plt.show()
    elif len(sys.argv) > 1 and (sys.argv[1] == 'g' or sys.argv[1] == 'n'):
        # 带噪声实验
        if sys.argv[1] == 'g':
            test_acc, test_acc_noise =  numpy_run([0.1, 'SGD', None], noise=[True, True])   # noise=[训练带噪，测试带噪]
            plt.plot(range(len(test_acc)), test_acc, linestyle='--', marker='+', color='k', label='test without noise')
            plt.plot(range(len(test_acc_noise)), test_acc_noise, marker='o', color='y', label='test with noise')
            plt.xlabel("epoch")
            plt.ylabel("acc")
            plt.title("train_with_noise")
            plt.legend()
            plt.show()
        elif sys.argv[1] == 'n':
            test_acc, test_acc_noise =  numpy_run([0.1, 'SGD', None], noise=[False, True])
            plt.plot(range(len(test_acc)), test_acc, linestyle='--', marker='+', color='k', label='test without noise')
            plt.plot(range(len(test_acc_noise)), test_acc_noise, marker='o', color='y', label='test with noise')
            plt.xlabel("epoch")
            plt.ylabel("acc")
            plt.title("train_without_noise")
            plt.legend()
            plt.show()
    else:
        numpy_run([0.1, 'SGD', None])
