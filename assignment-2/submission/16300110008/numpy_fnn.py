import numpy as np


class NumpyOp:

    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):

    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h

    def __call__(self, x, W):
        return self.forward(x, W)

    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        x = self.memory['x']
        W = self.memory['W']
        grad_W = np.matmul(x.T, grad_y)
        grad_x = np.matmul(grad_y, W.T)

        return grad_x, grad_W


class Relu(NumpyOp):

    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))

    def __call__(self, x):
        return self.forward(x)

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = np.where(x > 0, grad_y, np.zeros_like(grad_y))

        return grad_x


class Log(NumpyOp):

    def forward(self, x):
        """
        x: shape(N, c)
        """

        out = np.log(x + self.epsilon)
        self.memory['x'] = x

        return out

    def __call__(self, x):
        return self.forward(x)

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = 1 / (x + self.epsilon) * grad_y

        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """

    def forward(self, x):
        """
        x: shape(N, c)
        """
        max = np.max(x, axis=1, keepdims=True)
        out = np.exp(x - max) / np.sum(np.exp(x - max), axis=1).reshape(-1, 1)
        self.memory['A'] = out

        return out

    def __call__(self, x):
        return self.forward(x)

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        N, C = grad_y.shape
        A = self.memory['A']
        # 扩充激活值的维度，计算softmax导数
        temp = A[:, np.newaxis, :] * np.eye(C) - np.matmul(A[:, np.newaxis, :].transpose(0, 2, 1), A[:, np.newaxis, :])
        # 扩充误差项与dX进行乘法
        grad_x = np.matmul(grad_y[:, np.newaxis, :], temp).squeeze(1)

        return grad_x


class NumpyLoss:

    def __init__(self):
        self.target = None

    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()

    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))

        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新.softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        self.memory = {}

    def forward(self, x):
        x = x.reshape(-1, 28 * 28)

        x = self.relu_1(self.matmul_1(x, self.W1))
        x = self.relu_2(self.matmul_2(x, self.W2))
        x = self.matmul_3(x, self.W3)
        x = self.softmax(x)
        x = self.log(x)

        return x

    def backward(self, y):
        grad_y = y

        self.log_grad = self.log.backward(grad_y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

    def optimize(self, learning_rate, mode='SGD', h_params=None):
        """
        优化器，用于更新参数
        :param learning_rate: 学习率
        :param mode: 优化器类型，包括SGD AdaGrad RMSProp Adam
        :param h_params: 优化器所需的超参数
        :return: None
        """
        if mode == 'SGD':
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
        elif mode == 'momentum':
            # beta 一般取0.9
            beta = h_params[0]
            # 获取上一时刻的动量，初始值为0
            V1, V2, V3 = self.memory.get('V1', 0), self.memory.get('V2', 0), self.memory.get('V3', 0)
            # 更新动量
            V1 = beta * V1 + (1 - beta) * self.W1_grad
            V2 = beta * V2 + (1 - beta) * self.W2_grad
            V3 = beta * V3 + (1 - beta) * self.W3_grad
            # 存储当前动量
            self.memory['V1'] = V1
            self.memory['V2'] = V2
            self.memory['V3'] = V3
            # 更新参数
            self.W1 -= learning_rate * V1
            self.W2 -= learning_rate * V2
            self.W3 -= learning_rate * V3

        elif mode == 'AdaGrad':
            # 学习率大于1e-2以后梯度会消失
            epsilon = 1e-7
            # 读取历史梯度值平方和
            r1, r2, r3 = self.memory.get('r1', 0), self.memory.get('r2', 0), self.memory.get('r3', 0)
            # 更新历史梯度值平方和
            r1 += np.square(self.W1_grad)
            r2 += np.square(self.W2_grad)
            r3 += np.square(self.W3_grad)
            # 存储历史梯度值平方和
            self.memory['r1'] = r1
            self.memory['r2'] = r2
            self.memory['r3'] = r3
            # 更新参数
            self.W1 -= learning_rate / (epsilon + np.sqrt(r1)) * self.W1_grad
            self.W2 -= learning_rate / (epsilon + np.sqrt(r2)) * self.W2_grad
            self.W3 -= learning_rate / (epsilon + np.sqrt(r3)) * self.W3_grad

        elif mode == 'RMSProp':
            # lr 1e-3, rho 0.999
            epsilon = 1e-6
            rho = h_params[0]
            # 读取历史梯度值平方和
            r1, r2, r3 = self.memory.get('r1', 0), self.memory.get('r2', 0), self.memory.get('r3', 0)
            # 更新历史梯度值平方和
            r1 = rho * r1 + (1 - rho) * np.square(self.W1_grad)
            r2 = rho * r2 + (1 - rho) * np.square(self.W2_grad)
            r3 = rho * r3 + (1 - rho) * np.square(self.W3_grad)
            # 存储历史梯度值平方和
            self.memory['r1'] = r1
            self.memory['r2'] = r2
            self.memory['r3'] = r3
            # 更新参数
            self.W1 -= learning_rate / (epsilon + np.sqrt(r1)) * self.W1_grad
            self.W2 -= learning_rate / (epsilon + np.sqrt(r2)) * self.W2_grad
            self.W3 -= learning_rate / (epsilon + np.sqrt(r3)) * self.W3_grad

        elif mode == 'Adam':
            # lr=1e-3, rho1=0.9, rho2=0.999
            epsilon = 1e-8
            rho1, rho2 = h_params[0], h_params[1]
            # 确定当前时刻值
            t = self.memory.get('t', 0)
            t += 1
            # 读取历史梯度值平方和以及历史动量，初始值均为0
            s1, s2, s3 = self.memory.get('s1', 0), self.memory.get('s2', 0), self.memory.get('s3', 0)
            r1, r2, r3 = self.memory.get('r1', 0), self.memory.get('r2', 0), self.memory.get('r3', 0)
            # 更新动量
            s1 = rho1 * s1 + (1 - rho1) * self.W1_grad
            s2 = rho1 * s2 + (1 - rho1) * self.W2_grad
            s3 = rho1 * s3 + (1 - rho1) * self.W3_grad
            # 存储动量
            self.memory['s1'] = s1
            self.memory['s2'] = s2
            self.memory['s3'] = s3
            # 更新梯度值平方和
            r1 = rho2 * r1 + (1 - rho2) * np.square(self.W1_grad)
            r2 = rho2 * r2 + (1 - rho2) * np.square(self.W2_grad)
            r3 = rho2 * r3 + (1 - rho2) * np.square(self.W3_grad)
            # 存储梯度值平方和
            self.memory['r1'] = r1
            self.memory['r2'] = r2
            self.memory['r3'] = r3
            # 修正当前动量
            s1_hat = s1 / (1 - np.power(rho1, t))
            s2_hat = s2 / (1 - np.power(rho1, t))
            s3_hat = s3 / (1 - np.power(rho1, t))
            # 修正当前梯度平方和
            r1_hat = r1 / (1 - np.power(rho2, t))
            r2_hat = r2 / (1 - np.power(rho2, t))
            r3_hat = r3 / (1 - np.power(rho2, t))
            # 更新梯度
            self.W1 -= learning_rate / (epsilon + np.sqrt(r1_hat)) * s1_hat
            self.W2 -= learning_rate / (epsilon + np.sqrt(r2_hat)) * s2_hat
            self.W3 -= learning_rate / (epsilon + np.sqrt(r3_hat)) * s3_hat
