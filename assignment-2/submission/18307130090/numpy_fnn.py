import numpy as np


class NumpyOp:

    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):

    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h

    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        x, W = self.memory['x'], self.memory['W']
        grad_x = np.matmul(grad_y, W.T)
        grad_W = np.matmul(x.T, grad_y)

        return grad_x, grad_W


class Relu(NumpyOp):

    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = grad_y * np.where(x > 0, np.ones_like(x), np.zeros_like(x))

        return grad_x


class Log(NumpyOp):

    def forward(self, x):
        """
        x: shape(N, c)
        """

        out = np.log(x + self.epsilon)
        self.memory['x'] = x

        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = grad_y * np.reciprocal(x + self.epsilon)

        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """

    def forward(self, x):
        """
        x: shape(N, c)
        """
        exp_x = np.exp(x - x.max())
        exp_sum = np.sum(exp_x, axis=1, keepdims=True)
        out = exp_x / exp_sum
        self.memory['x'] = x
        self.memory['out'] = out

        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        sm = self.memory['out']
        Jacobs = np.array([np.diag(r) - np.outer(r, r) for r in sm])

        grad_y = grad_y[:, np.newaxis, :]
        grad_x = np.matmul(grad_y, Jacobs).squeeze(axis=1)

        return grad_x


class NumpyLoss:

    def __init__(self):
        self.target = None

    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()

    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))

        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        self.beta_1 = 0.9
        self.beta_2 = 0.999
        self.epsilon = 1e-8
        self.is_first = True

        self.W1_grad_mean = None
        self.W2_grad_mean = None
        self.W3_grad_mean = None

        self.W1_grad_square_mean = None
        self.W2_grad_square_mean = None
        self.W3_grad_square_mean = None

    def forward(self, x):
        x = x.reshape(-1, 28 * 28)

        x = self.relu_1.forward(self.matmul_1.forward(x, self.W1))
        x = self.relu_2.forward(self.matmul_2.forward(x, self.W2))
        x = self.matmul_3.forward(x, self.W3)

        x = self.log.forward(self.softmax.forward(x))

        return x

    def backward(self, y):
        self.log_grad = self.log.backward(y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

        return self.x1_grad

    def optimize(self, learning_rate):
        def SGD():
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad

        def SGDM():
            if self.is_first:
                self.is_first = False

                self.W1_grad_mean = self.W1_grad
                self.W2_grad_mean = self.W2_grad
                self.W3_grad_mean = self.W3_grad
            else:
                self.W1_grad_mean = self.beta_1 * self.W1_grad_mean + (1 - self.beta_1) * self.W1_grad
                self.W2_grad_mean = self.beta_1 * self.W2_grad_mean + (1 - self.beta_1) * self.W2_grad
                self.W3_grad_mean = self.beta_1 * self.W3_grad_mean + (1 - self.beta_1) * self.W3_grad

            delta_1 = learning_rate * self.W1_grad_mean
            delta_2 = learning_rate * self.W2_grad_mean
            delta_3 = learning_rate * self.W3_grad_mean

            self.W1 -= delta_1
            self.W2 -= delta_2
            self.W3 -= delta_3

        def Adam(learning_rate=0.001):
            if self.is_first:
                self.is_first = False
                self.W1_grad_mean = self.W1_grad
                self.W2_grad_mean = self.W2_grad
                self.W3_grad_mean = self.W3_grad

                self.W1_grad_square_mean = np.square(self.W1_grad)
                self.W2_grad_square_mean = np.square(self.W2_grad)
                self.W3_grad_square_mean = np.square(self.W3_grad)

                self.W1 -= learning_rate * self.W1_grad_mean
                self.W2 -= learning_rate * self.W2_grad_mean
                self.W3 -= learning_rate * self.W3_grad_mean
            else:
                self.W1_grad_mean = self.beta_1 * self.W1_grad_mean + (1 - self.beta_1) * self.W1_grad
                self.W2_grad_mean = self.beta_1 * self.W2_grad_mean + (1 - self.beta_1) * self.W2_grad
                self.W3_grad_mean = self.beta_1 * self.W3_grad_mean + (1 - self.beta_1) * self.W3_grad

                self.W1_grad_square_mean = self.beta_2 * self.W1_grad_square_mean + (1 - self.beta_2) * np.square(
                    self.W1_grad)
                self.W2_grad_square_mean = self.beta_2 * self.W2_grad_square_mean + (1 - self.beta_2) * np.square(
                    self.W2_grad)
                self.W3_grad_square_mean = self.beta_2 * self.W3_grad_square_mean + (1 - self.beta_2) * np.square(
                    self.W3_grad)

                delta_1 = learning_rate * self.W1_grad_mean * np.reciprocal(
                    np.sqrt(self.W1_grad_square_mean) + np.full_like(self.W1_grad_square_mean, self.epsilon))
                delta_2 = learning_rate * self.W2_grad_mean * np.reciprocal(
                    np.sqrt(self.W2_grad_square_mean) + np.full_like(self.W2_grad_square_mean, self.epsilon))
                delta_3 = learning_rate * self.W3_grad_mean * np.reciprocal(
                    np.sqrt(self.W3_grad_square_mean) + np.full_like(self.W3_grad_square_mean, self.epsilon))

                self.W1 -= delta_1
                self.W2 -= delta_2
                self.W3 -= delta_3

        # SGD()
        # SGDM()
        Adam()
