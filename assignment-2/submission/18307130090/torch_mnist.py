import torch
from utils import mini_batch, batch, download_mnist, get_torch_initialization, one_hot, plot_curve


class TorchModel:
    
    def __init__(self):
        self.W1 = torch.randn((28 * 28, 256), requires_grad=True)
        self.W2 = torch.randn((256, 64), requires_grad=True)
        self.W3 = torch.randn((64, 10), requires_grad=True)
        self.softmax_input = None
        self.log_input = None
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        x = torch.relu(torch.matmul(x, self.W1))
        x = torch.relu(torch.matmul(x, self.W2))
        x = torch.matmul(x, self.W3)
        
        self.softmax_input = x
        self.softmax_input.retain_grad()
        
        x = torch.softmax(x, 1)
        
        self.log_input = x
        self.log_input.retain_grad()
        
        x = torch.log(x)
        
        return x
    
    def optimize(self, learning_rate):
        with torch.no_grad():
            self.W1 -= learning_rate * self.W1.grad
            self.W2 -= learning_rate * self.W2.grad
            self.W3 -= learning_rate * self.W3.grad
            
            self.W1.grad = None
            self.W2.grad = None
            self.W3.grad = None


def torch_run():
    train_dataset, test_dataset = download_mnist()
    
    model = TorchModel()
    model.W1.data, model.W2.data, model.W3.data = get_torch_initialization(numpy=False)
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset, numpy=False):
            y = one_hot(y, numpy=False)
            
            y_pred = model.forward(x)
            loss = (-y_pred * y).sum(dim=1).mean()
            loss.backward()
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset, numpy=False)[0]
        accuracy = model.forward(x).argmax(dim=1).eq(y).float().mean().item()
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    torch_run()
