# PRML-2021 Assignment2

姓名：夏海淞

学号：18307130090

## 简述

在本次实验中，我通过`NumPy`实现了一个简单的前馈神经网络，其中包括`numpy_fnn.py`中算子的反向传播以及前馈神经网络模型的构建。为了验证模型效果，我在MNIST数据集上进行了训练和测试。此外，我还实现了`Momentum`和`Adam`优化算法，并比较了它们的性能。

## 算子的反向传播

### `Matmul`

`Matmul`的计算公式为：
$$
Y=X\times W
$$
其中$Y,X,W$分别为$n\times d',n\times d,d\times d'$的矩阵。

由[神经网络与深度学习-邱锡鹏](https://nndl.github.io/nndl-book.pdf)中公式(B.20)和(B.21)，有
$$
\frac{\partial Y}{\partial W}=\frac{\partial(X\times W)}{\partial W}=X^T\\\\
\frac{\partial Y}{\partial X}=\frac{\partial(X\times W)}{\partial X}=W^T
$$
结合链式法则和矩阵运算法则，可得
$$
\nabla_X=\nabla_Y\times W^T\\\\
\nabla_W=X^T\times \nabla_Y
$$

### `Relu`

`Relu`的计算公式为：
$$
Y_{ij}=\begin{cases}
X_{ij}&X_{ij}\ge0\\\\
0&\text{otherwise}
\end{cases}
$$
因此有
$$
\frac{\partial Y_{ij}}{\partial X_{ij}}=\begin{cases}
1&X_{ij}>0\\\\
0&\text{otherwise}
\end{cases}
$$
结合链式法则，得到反向传播的计算公式：$\nabla_{Xij}=\nabla_{Yij}\cdot\frac{\partial Y_{ij}}{\partial X_{ij}}$

### `Log`

`Log`的计算公式为
$$
Y_{ij}=\ln(X_{ij}+\epsilon),\epsilon=10^{-12}
$$
因此有
$$
\frac{\partial Y_{ij}}{\partial X_{ij}}=\frac1{X_{ij}+\epsilon}
$$
结合链式法则，得到反向传播的计算公式：$\nabla_{Xij}=\nabla_{Yij}\cdot\frac{\partial Y_{ij}}{\partial {X_{ij}}}$

### `Softmax`

`Softmax`的计算公式为
$$
Y_{ij}=\frac{\exp\{X_{ij} \}}{\sum_{k=1}^c\exp\{X_{ik} \}}
$$
其中$Y,X$均为$N\times c$的矩阵。容易发现`Softmax`以$X$的每行作为单位进行运算。因此对于$X,Y$的行分量$X_k,Y_k$，有
$$
\frac{\partial Y_{ki}}{\partial X_{kj}}=\begin{cases}
\frac{\exp\{X_{kj} \}(\sum_t\exp\{X_{kt}\})-\exp\{2X_{ki}\}}{(\sum_t\exp\{X_{kt}\})^2}=Y_{ki}(1-Y_{ki})&i=j\\\\
-\frac{\exp\{X_{ki} \}\exp\{X_{kj} \}}{(\sum_t\exp\{X_{kt}\})^2}=-Y_{ki}Y_{kj}&i\not=j
\end{cases}
$$
因此可计算得到$X_k,Y_k$的Jacob矩阵，满足$J_{ij}=\frac{\partial Y_{ki}}{\partial X_{kj}}$。结合链式法则，可得
$$
\nabla_X=\nabla_Y\times J
$$
将行分量组合起来，就得到了反向传播的最终结果。

## 模型构建与训练

### 模型构建

#### `forward`

参考`torch_mnist.py`中`TorchModel`方法的模型，使用如下代码构建：

```python
def forward(self, x):
    x = x.reshape(-1, 28 * 28)

    x = self.relu_1.forward(self.matmul_1.forward(x, self.W1))
    x = self.relu_2.forward(self.matmul_2.forward(x, self.W2))
    x = self.matmul_3.forward(x, self.W3)

    x = self.log.forward(self.softmax.forward(x))
    
    return x
```

模型的计算图如下：

![](./img/fnn_model.png)

#### `backward`

根据模型的计算图，按照反向的计算顺序依次调用对应算子的反向传播算法即可。

```python
def backward(self, y):
    self.log_grad = self.log.backward(y)
    self.softmax_grad = self.softmax.backward(self.log_grad)
    self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
    self.relu_2_grad = self.relu_2.backward(self.x3_grad)
    self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
    self.relu_1_grad = self.relu_1.backward(self.x2_grad)
    self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

    return self.x1_grad
```

#### `mini_batch`

`mini_batch`的作用是提高模型的训练速度，同时得到较好的优化效果。传统的批处理方法对整个数据集计算平均的损失函数值，随后计算相应梯度进行反向传播。当训练数据集容量较大时，对训练速度造成严重影响；而随机方法则对数据集的每个样本计算损失函数值，随后计算相应梯度进行反向传播。此时数据集容量不对训练速度产生影响，然而由于样本的随机性，可能导致参数无法收敛到最优值，在最优值附近震荡。因此一个折中的方法是将数据集划分为若干批次，在提高训练速度的同时保证了较好的收敛效果。

在本次实验中，我参照`utils.py`中的`mini_batch`，在`numpy_mnist.py`中重新实现了`mini_batch`方法：

```python
def mini_batch(dataset, batch_size=128):
    data = np.array([np.array(each[0]) for each in dataset])
    label = np.array([each[1] for each in dataset])

    size = data.shape[0]
    index = np.arange(size)
    np.random.shuffle(index)

    return [(data[index[i:i + batch_size]], label[index[i:i + batch_size]]) for i in range(0, size, batch_size)]
```

### 模型训练

设定`learning_rate=0.1`，`batch_size=128`，`epoch_number=10`。训练结果如下：

```
[0] Accuracy: 0.9486
[1] Accuracy: 0.9643
[2] Accuracy: 0.9724
[3] Accuracy: 0.9738
[4] Accuracy: 0.9781
[5] Accuracy: 0.9768
[6] Accuracy: 0.9796
[7] Accuracy: 0.9802
[8] Accuracy: 0.9800
[9] Accuracy: 0.9796
```

<img src="./img/SGD_normal.png" style="zoom: 80%;" />

尝试缩减`batch_size`的大小，设定`batch_size=64`。训练结果如下：

```
[0] Accuracy: 0.9597
[1] Accuracy: 0.9715
[2] Accuracy: 0.9739
[3] Accuracy: 0.9771
[4] Accuracy: 0.9775
[5] Accuracy: 0.9803
[6] Accuracy: 0.9808
[7] Accuracy: 0.9805
[8] Accuracy: 0.9805
[9] Accuracy: 0.9716
```

<img src="./img/SGD_batch_size.png" style="zoom:80%;" />

尝试降低`learning_rate`，设定`learning_rate=0.01`。训练结果如下：

```
[0] Accuracy: 0.8758
[1] Accuracy: 0.9028
[2] Accuracy: 0.9143
[3] Accuracy: 0.9234
[4] Accuracy: 0.9298
[5] Accuracy: 0.9350
[6] Accuracy: 0.9397
[7] Accuracy: 0.9434
[8] Accuracy: 0.9459
[9] Accuracy: 0.9501
```

<img src="./img/SGD_learning_rate.png" style="zoom:80%;" />

根据实验结果，可以得出以下结论：

当学习率和批处理容量合适时，参数的收敛速度随着学习率的减小而减小，而参数的震荡幅度随着批处理容量的减小而增大。

## 梯度下降算法的改进

传统的梯度下降算法可以表述为：
$$
w_{t+1}=w_t-\eta\cdot\nabla f(w_t)
$$
尽管梯度下降作为优化算法被广泛使用，它依然存在一些缺点，主要表现为：

- 参数修正方向完全由当前梯度决定，导致当学习率过高时参数可能在最优点附近震荡；
- 学习率无法随着训练进度改变，导致训练前期收敛速度较慢，后期可能无法收敛。

针对上述缺陷，产生了许多梯度下降算法的改进算法。其中较为典型的是`Momentum`算法和`Adam`算法。

### `Momentum`

针对“参数修正方向完全由当前梯度决定”的问题，`Momentum`引入了“动量”的概念。

类比现实世界，当小球从高处向低处滚动时，其运动方向不仅与当前位置的“陡峭程度”相关，也和当前的速度，即先前位置的“陡峭程度”相关。因此在`Momentum`算法中，参数的修正值不是取决于当前梯度，而是取决于梯度的各时刻的指数移动平均值：
$$
m_t=\beta\cdot m_{t-1}+(1-\beta)\cdot\nabla f(w_t)\\\\
w_{t+1}=w_t-\eta\cdot m_t
$$
指数移动平均值反映了参数调整时的“惯性”。当参数调整方向正确时，`Momentum`有助于加快训练速度，减少震荡的幅度；然而当参数调整方向错误时，`Momentum`会因为无法及时调整方向造成性能上的部分损失。

使用`Momentum`算法的训练结果如下：

```
[0] Accuracy: 0.9444
[1] Accuracy: 0.9627
[2] Accuracy: 0.9681
[3] Accuracy: 0.9731
[4] Accuracy: 0.9765
[5] Accuracy: 0.9755
[6] Accuracy: 0.9768
[7] Accuracy: 0.9790
[8] Accuracy: 0.9794
[9] Accuracy: 0.9819
```

<img src="./img/SGDM.png" style="zoom:80%;" />

可以看出相较传统的梯度下降算法并无明显优势。

### `Adam`

针对“学习率无法随着训练进度改变”的问题，`Adam`在`Momentum`的基础上引入了“二阶动量”的概念。

`Adam`的改进思路为：由于神经网络中存在大量参数，不同参数的调整频率存在差别。对于频繁更新的参数，我们希望适当降低其学习率，提高收敛概率；而对于其他参数，我们希望适当增大其学习率，加快收敛速度。同时，参数的调整频率可能发生动态改变，我们也希望学习率能够随之动态调整。

因为参数的调整值与当前梯度直接相关，因此取历史梯度的平方和作为衡量参数调整频率的标准。如果历史梯度平方和较大，表明参数被频繁更新，需要降低其学习率。因此梯度下降算法改写为：
$$
m_t=\beta\cdot m_{t-1}+(1-\beta)\cdot\nabla f(w_t)\\\\
V_t=V_{t-1}+\nabla^2f(w_t)\\\\
w_{t+1}=w_t-\frac\eta{\sqrt{V_t}}\cdot m_t
$$
然而，由于$V_t$关于$t$单调递增，可能导致训练后期学习率过低，参数无法收敛至最优。因此将$V_t$也改为指数移动平均值，避免了上述缺陷：
$$
m_t=\beta_1\cdot m_{t-1}+(1-\beta_1)\cdot\nabla f(w_t)\\\\
V_t=\beta_2\cdot V_{t-1}+(1-\beta_2)\cdot\nabla^2f(w_t)\\\\
w_{t+1}=w_t-\frac\eta{\sqrt{V_t}}\cdot m_t
$$
使用`Adam`算法的训练结果如下：

```
[0] Accuracy: 0.9657
[1] Accuracy: 0.9724
[2] Accuracy: 0.9759
[3] Accuracy: 0.9769
[4] Accuracy: 0.9788
[5] Accuracy: 0.9778
[6] Accuracy: 0.9775
[7] Accuracy: 0.9759
[8] Accuracy: 0.9786
[9] Accuracy: 0.9779
```

<img src="./img/Adam.png" style="zoom:80%;" />

可以看出相较传统的梯度下降算法，损失函数值的震荡幅度有所减小，而收敛速度与传统方法相当。
