import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #    
        ####################
        xT = np.transpose(self.memory['x'])
        WT = np.transpose(self.memory['W'])

        grad_x = np.matmul(grad_y, WT)
        grad_W = np.matmul(xT, grad_y)
        
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        x = self.memory['x']
        grad_x = grad_y * np.where(x > 0, 1, 0)
        
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        x = self.memory['x']
        grad_x = grad_y / (x + self.epsilon)
        
        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        ####################
        #      code 4      #
        ####################
        sum = np.exp(x).sum(axis = 1)
        sum = sum.reshape(x.shape[0], 1)
        out = np.exp(x) / sum

        self.memory['y'] = out

        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 5      #
        ####################
        y = self.memory['y']
        
        grad_x = y * (grad_y -  (y * grad_y).sum(axis = 1).reshape(len(y),1))

        return grad_x


class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))
        
        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()
        
        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        # Momentum优化
        self.v_W1_grad = 0
        self.v_W2_grad = 0
        self.v_W3_grad = 0

        # Adam优化
        self.s_W1_grad = 0
        self.s_W2_grad = 0
        self.s_W3_grad = 0
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        x = self.matmul_1.forward(x, self.W1) 
        x = self.relu_1.forward(x)

        x = self.matmul_2.forward(x, self.W2)
        x = self.relu_2.forward(x)

        x = self.matmul_3.forward(x, self.W3)
        x = self.softmax.forward(x)

        x = self.log.forward(x)

        return x
    
    def backward(self, y):
        
        ####################
        #      code 7      #
        ###################

        self.log_grad = self.log.backward(y)

        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)

        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)

        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
    
    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad
    
    def optimize_Momentum(self, learning_rate, belta):
        self.v_W1_grad = belta * self.v_W1_grad + (1 - belta) * self.W1_grad
        self.v_W2_grad = belta * self.v_W2_grad + (1 - belta) * self.W2_grad
        self.v_W3_grad = belta * self.v_W3_grad + (1 - belta) * self.W3_grad

        self.W1 -= learning_rate * self.v_W1_grad
        self.W2 -= learning_rate * self.v_W2_grad
        self.W3 -= learning_rate * self.v_W3_grad

    def optimize_Adam(self, learning_rate, beta1, beta2, beta1_t, beta2_t, eps):

        self.v_W1_grad = beta1 * self.v_W1_grad + (1 - beta1) * self.W1_grad
        self.v_W2_grad = beta1 * self.v_W2_grad + (1 - beta1) * self.W2_grad
        self.v_W3_grad = beta1 * self.v_W3_grad + (1 - beta1) * self.W3_grad

        v_W1_corr = self.v_W1_grad / (1 - beta1_t)
        v_W2_corr = self.v_W2_grad / (1 - beta1_t)
        v_W3_corr = self.v_W3_grad / (1 - beta1_t)

        self.s_W1_grad = beta2 * self.s_W1_grad + (1 - beta2) * (self.W1_grad ** 2)
        self.s_W2_grad = beta2 * self.s_W2_grad + (1 - beta2) * (self.W2_grad ** 2)
        self.s_W3_grad = beta2 * self.s_W3_grad + (1 - beta2) * (self.W3_grad ** 2)

        s_W1_corr = self.s_W1_grad / (1 - beta2_t)
        s_W2_corr = self.s_W2_grad / (1 - beta2_t)
        s_W3_corr = self.s_W3_grad / (1 - beta2_t)

        self.W1 -= learning_rate * v_W1_corr / (np.sqrt(s_W1_corr) + eps)
        self.W2 -= learning_rate * v_W2_corr / (np.sqrt(s_W2_corr) + eps)
        self.W3 -= learning_rate * v_W3_corr / (np.sqrt(s_W3_corr) + eps)

