import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, mini_batch, batch, get_torch_initialization, plot_curve, one_hot

def mini_batch_numpy(dataset, batch_size=128):
    data = []
    label = []

    for x in dataset:
        data.append(np.array(x[0]))
        label.append(x[1])

    data = np.array(data)
    label = np.array(label)

    idx = np.random.permutation(len(dataset))
    data = data[idx]
    label = label[idx]

    split_num = len(dataset) // batch_size
    split_pos = split_num * batch_size

    ret_data = np.split(data[:split_pos], split_num)
    ret_data.append(data[split_pos+1:])

    ret_label = np.split(label[:split_pos], split_num)
    ret_label.append(label[split_pos+1:])
    
    ret = list(zip(ret_data, ret_label))
    return ret

def numpy_run():
    
    import time
    start = time.time()

    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1

    #Adam 优化
    beta1 = 0.9
    beta2 = 0.999
    beta1_t = 1
    beta2_t = 1
    
    for epoch in range(epoch_number):
        #Adam 优化
        beta1_t *= beta1
        beta2_t *= beta2
        
        # for x, y in mini_batch_numpy(train_dataset): # mini_batch_numpy
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            # y_pred = model.forward(x) # mini_batch_numpy
            y_pred = model.forward(x.numpy())
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())

            #原始optimize
            model.optimize(learning_rate)

            #Momentum 优化
            # model.optimize_Momentum(learning_rate, 0.9)
            
            #Adam 优化
            # model.optimize_Adam(learning_rate, beta1, beta2, beta1_t, beta2_t, 1e-8)

            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    end = time.time()
    print("time = %.2f s"%(end-start))

    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
