# 实验报告

陈疏桐   17307130331

本次实验，我用numpy实现了Matmul、log、softmax和relu四个算子的前向计算与后向计算，用四个算子构建分类模型，通过了自动测试，并实现了mini_batch函数，在mnist数据集上用不同的学习率与Batch大小进行训练和测试，讨论学习率与Batch大小对模型训练效果的影响。最后，我还实现Momentum、RMSProp与Adam三种优化方法，与传统梯度下降进行比较。

## 算子的反向传播与实现
### Matmul

Matmul是矩阵的乘法，在模型中的作用相当于pytorch的一个线性层，前向传播的公式是：

$$ \mathrm{Y} = \mathrm{X}\mathrm{W} $$

其中，$\mathrm{X}$是形状为 $N \times d$的输入矩阵，$\mathrm{W}$是形状为$d \times d'$的矩阵， $\mathrm{Y}$是形状为$N\times d'$的输出矩阵。Matmul算子相当于输入维度为$d$、输出$d'$维的线性全连接层。

Matmul分别对输入求偏导，有

$$ \frac{\partial \mathrm{Y}}{\partial \mathrm{X}} = \frac{\partial \mathrm{X}\mathrm{W}}{\partial \mathrm{X}} = \mathrm{W}^T$$

$$ \frac{\partial \mathrm{Y}}{\partial \mathrm{W}} = \frac{\partial \mathrm{X}\mathrm{W}}{\partial \mathrm{W}} = \mathrm{X}^T $$

则根据链式法则，反向传播的计算公式为：

$$ \triangledown{\mathrm{X}} = \triangledown{\mathrm{Y}} \times \mathrm{W}^T $$
$$ \triangledown{\mathrm{W}} = \mathrm{X}^T \times \triangledown{\mathrm{Y}} $$

### Relu 

Relu函数对输入每一个元素的公式是：

$$ \mathrm{Y}_{ij}=
\begin{cases}
\mathrm{X}_{ij} & \mathrm{X}_{ij} \ge 0 \\\\
0 & \text{otherwise}
\end{cases} 
$$


每一个输出 $\mathrm{Y}_{ij}$都只与输入$\mathrm{X}_{ij}$有关。则$\mathrm{X}$每一个元素的导数也只和对应的输出有关，为：

$$ \frac{\partial \mathrm{Y}_{ij}}{\partial \mathrm{X}_{ij}} = 
\begin{cases}
1 & \mathrm{X}_{ij} \ge 0 \\\\
0 & \text{otherwise}
\end{cases}$$ 

因此，根据链式法则，输入的梯度为：

$$ \triangledown{\mathrm{X}_{ij}} = \triangledown{\mathrm{Y}_{ij}} \times \frac{\partial \mathrm{Y}_{ij}}{\partial \mathrm{X}_{ij}}$$

### Log

Log 函数公式：

$$ \mathrm{Y}_{ij} = \log(\mathrm{X}_{ij} + \epsilon)$$

$$ \frac{\partial \mathrm{Y}_{ij}}{\partial \mathrm{X}_{ij}} = \frac{1}{(\mathrm{X}_{ij} + \epsilon)} $$

类似地，反向传播的计算公式为：

$$ \triangledown{\mathrm{X}_{ij}} = \triangledown{\mathrm{Y}_{ij}} \times \frac{\partial \mathrm{Y}_{ij}}{\partial \mathrm{X}_{ij}}$$

### Softmax

Softmax对输入$\mathrm{X}$的最后一个维度进行计算。前向传播的计算公式为：

$$ \mathrm{Y}_{ij} = \frac{\exp^{\mathrm{X}_{ij}}}{\sum_{k} \exp ^ {\mathrm{X}_{ik}}}$$

从公式可知，Softmax的每一行输出都是独立计算的，与其它行的输入无关。而对于同一行，每一个输出都与每一个输入元素有关。以行$k$为例，可推得输出元素对输入元素求导的计算公式是：

$$\frac{\partial Y_{ki}}{\partial X_{kj}} = \begin{cases}
\frac{\exp ^ {X_{kj}} \times (\sum_{t \ne j}{\exp ^ {X_{kt}}}) }{(\sum_{t}{\exp ^ {X_{kt}}})^2} = Y_{kj}(1-Y_{kj}) & i = j \\\\
-\frac{\exp^{X_{ki} }\exp^{X_{kj} }}{(\sum_t\exp^{X_{kt}})^2}=-Y_{ki} \times Y_{kj} & i\ne j
\end{cases}$$

可得每行输出$\mathrm{Y}_{k}$与每行输入$\mathrm{X}_{k}$的Jacob矩阵$\mathrm{J}_{k}$， $\mathrm{J_{k}}_{ij} = \frac{\partial \mathrm{Y}_{ki}}{\partial \mathrm{X}_{kj}}$.

输出的一行对于输入$\mathrm{X}_{kj}$的导数，是输出每一行所有元素对其导数相加，即$\sum_{i} {\frac{\partial \mathrm{Y}_{ki}}{\partial \mathrm{X}_{kj}}}$ 的结果。

因此，根据链式法则，可得到反向传播的计算公式为：
$$ \triangledown \mathrm{X}_{kj} = \sum_{i} {\frac{\partial \mathrm{Y}_{ki} \times \triangledown \mathrm{Y}_{ki}}{\partial \mathrm{X}_{kj}}}$$

相当于：

$$ \triangledown \mathrm{X}_{k} = \mathrm{J}_{k} \times \triangledown \mathrm{Y}_{k} $$

在实现时，可以用`numpy`的`matmul`操作实现对最后两个维度的矩阵相乘，得到的矩阵堆叠起来，得到最后的结果。


## 模型构建与训练
### 模型构建

参照`torch_mnist.py`中的`torch_model`，`numpy`模型的构建只需要将其中的算子换成我们实现的算子：
```
def forward(self, x):
    x = x.reshape(-1, 28 * 28)

    x = self.relu_1.forward(self.matmul_1.forward(x, self.W1))
    x = self.relu_2.forward(self.matmul_2.forward(x, self.W2))

    x = self.matmul_3.forward(x, self.W3)

    x = self.softmax.forward(x)
    x = self.log.forward(x)

    return x
```

模型的computation graph是：
![compu_graph](img/compu_graph.png)

根据计算图，可以应用链式法则，推导出各个叶子变量（$\mathrm{W}_{1}, \mathrm{W}_{2}, \mathrm{W}_{3}, \mathrm{X}$）以及中间变量的计算方法。

反向传播的计算图为：
![backpropagration](img/backgraph.png)

可根据计算图完成梯度的计算：
```
def backward(self, y):
    self.log_grad = self.log.backward(y)
    self.softmax_grad = self.softmax.backward(self.log_grad)
    self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
    self.relu_2_grad = self.relu_2.backward(self.x3_grad)
    self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
    self.relu_1_grad = self.relu_1.backward(self.x2_grad)
    self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
```

### MiniBatch

在`utils`中的`mini_batch`方法，直接调用了`pytorch`的`DataLoader`。 `DataLoader`是一个负责从数据集中读取样本、组合成批次输出的方法。简单地使用`DataLoader`， 可以方便地多线程并行化预取数据，加快训练速度，且节省代码。`DataLoader`还可以自定义`Sampler`，以不同的方式从数据集中进行采样，以及`BatchSampler`以自定的方式将采集的样本组合成批，这样就可以实现在同一Batch内将数据补0、自定义Batch正负样本混合比例等操作。

在这里，我们模仿`DataLoader`的默认行为实现`mini_batch`方法。
```
def mini_batch(dataset, batch_size=128):
    data = np.array([each[0].numpy() for each in dataset]) # 需要先处理数据
    label = np.array([each[1] for each in dataset])
    
    data_size = data.shape[0]
    idx = np.array([i for i in range(data_size)])
    np.random.shuffle(idx)   # 打乱顺序
    
    return [(data[idx[i: i+batch_size]], label[idx[i:i+batch_size]])  for i in range(0, data_size, batch_size)]  # 这里相当于DataLoader 的BatchSampler，但一次性调用
```

### 模型训练

构建模型，设置`epoch=10`, `learning_rate=0.1`, `batch_size=128`后，开始训练。训练时每次fit一个batch的数据，前向传播计算输出，然后根据输出计算loss，再调用`loss.backward`计算loss对输出的求导，即模型输出的梯度，之后就可以调用模型的`backward`进行后向计算。 最后调用模型的`optimize`更新参数。

训练过程：
![train10](img/train10.png)
 
各个epoch的测试准确率为：
```
[0] Test Accuracy: 0.9437
[1] Test Accuracy: 0.9651
[2] Test Accuracy: 0.9684
[3] Test Accuracy: 0.9730
[4] Test Accuracy: 0.9755
[5] Test Accuracy: 0.9775
[6] Test Accuracy: 0.9778
[7] Test Accuracy: 0.9766
[8] Test Accuracy: 0.9768
[9] Test Accuracy: 0.9781
```

将`learning_rate` 调整到0.2，重新训练：
![train02](img/train02.png)

各个epoch的测试准确率为：
```
[0] Test Accuracy: 0.9621
[1] Test Accuracy: 0.9703
[2] Test Accuracy: 0.9753
[3] Test Accuracy: 0.9740
[4] Test Accuracy: 0.9787
[5] Test Accuracy: 0.9756
[6] Test Accuracy: 0.9807
[7] Test Accuracy: 0.9795
[8] Test Accuracy: 0.9814
[9] Test Accuracy: 0.9825
```

可见，稍微提高学习率之后，训练前期参数更新的幅度更大，损失下降得更快，能够更早收敛。训练相同迭代数，现在的模型测试准确率更高。

将`learning_rate` 提高到0.3，重新训练：
![train03](img/train03.png)

```
[0] Test Accuracy: 0.9554
[1] Test Accuracy: 0.9715
[2] Test Accuracy: 0.9744
[3] Test Accuracy: 0.9756
[4] Test Accuracy: 0.9782
[5] Test Accuracy: 0.9795
[6] Test Accuracy: 0.9801
[7] Test Accuracy: 0.9816
[8] Test Accuracy: 0.9828
[9] Test Accuracy: 0.9778
```

增大学习率到0.3之后，训练前期损失下降速度与上一次训练差不多，但是到了训练后期，过大的学习率导致权重在局部最小值的附近以过大的幅度移动，难以进入最低点，模型loss表现为振荡，难以收敛。本次训练的测试准确率先提高到0.9828，后反而下降。

因此，可认为对于大小为128的batch，0.2是较为合适的学习率。

之后，维持学习率为0.2， 修改batch_size 为256， 重新训练：
![train256](img/train256.png)
```
[0] Test Accuracy: 0.9453
[1] Test Accuracy: 0.9621
[2] Test Accuracy: 0.9657
[3] Test Accuracy: 0.9629
[4] Test Accuracy: 0.9733
[5] Test Accuracy: 0.9766
[6] Test Accuracy: 0.9721
[7] Test Accuracy: 0.9768
[8] Test Accuracy: 0.9724
[9] Test Accuracy: 0.9775
```

batch_size增大后，每个batch更新一次参数，参数更新的频率更低，从而收敛速度有所降低；但是对比本次实验与前几次实验loss的曲线图，可发现振荡幅度更小。

将batch_size减小到64， 重新实验：
![train64](img/train64.png)
```
[0] Test Accuracy: 0.9526
[1] Test Accuracy: 0.9674
[2] Test Accuracy: 0.9719
[3] Test Accuracy: 0.9759
[4] Test Accuracy: 0.9750
[5] Test Accuracy: 0.9748
[6] Test Accuracy: 0.9772
[7] Test Accuracy: 0.9791
[8] Test Accuracy: 0.9820
[9] Test Accuracy: 0.9823
```

loss的下降速度增加，但是振荡幅度变大了。

总结：在一定范围之内，随着学习率的增大，模型收敛速度增加；随着batch_size的减小，模型收敛速度也会有一定增加，但是振荡幅度增大。 学习率过大会导致后期loss振荡、难以收敛；学习率过小则会导致loss下降速度过慢，甚至可能陷入局部最小值而错过更好的最低点。

## 其他优化方式实现

### momentum

普通梯度下降每次更新参数仅仅取决于当前batch的梯度，这可能会让梯度方向受到某些特殊的输入影响。Momentum引入了动量，让当前更新不仅取决于当前的梯度，还考虑到先前的梯度，能够在一定程度上保持一段时间的趋势。momentum的计算方式为：

$$
\begin{align}
& v = \alpha v - \gamma \frac{\partial L}{\partial W} \\\\
& W = W + v
\end{align}
$$

我们在`numpy_fnn.py`的模型中实现了Momentum的优化方法。 设置学习率为0.02，batch_size为128， 继续实验：
![momentum](img/momentum.png)
```
[0] Test Accuracy: 0.9586
[1] Test Accuracy: 0.9717
[2] Test Accuracy: 0.9743
[3] Test Accuracy: 0.9769
[4] Test Accuracy: 0.9778
[5] Test Accuracy: 0.9786
[6] Test Accuracy: 0.9782
[7] Test Accuracy: 0.9809
[8] Test Accuracy: 0.9790
[9] Test Accuracy: 0.9818
```

momentum 相比传统梯度下降，不一定最后会得到更好的效果。当加入动量，当前梯度方向与动量方向相同时，参数就会得到更大幅度的调整，因此loss下降速度更快，并且前期动量基本上会积累起来，如果使用过大的学习率，很容易会溢出。所以momentum适合的学习率比普通梯度下降要小一个数量级。 而当梯度方向错误的时候，加入动量会使得参数来不及更新，从而错过最小值。

### RMSProp


RMSProp引入了自适应的学习率调节。 在训练前期，学习率应该较高，使得loss能快速下降；但随着训练迭代增加，学习率应该不断减小，使得模型能够更好地收敛。 自适应调整学习率的基本思路是根据梯度来调节，梯度越大，学习率就衰减得越快；后期梯度减小，学习率衰减就更加缓慢。

而为了避免前期学习率衰减得过快，RMSProp还用了指数平均的方法，来缓慢丢弃原来的梯度历史。计算方法为：

$$
\begin{align}
& h = \rho h + (1-\rho) \frac{\partial L}{\partial W} \odot \frac{\partial L}{\partial W} \\\\
& W = W - \gamma \frac{1}{\sqrt{\delta + h}} \frac{\partial L}{\partial W}
\end{align}$$

设置梯度为0.001， weight_decay 为0.01， 进行训练和测试：
![rmsprop](img/rmsprop.png)

```
[0] Test Accuracy: 0.9663
[1] Test Accuracy: 0.9701
[2] Test Accuracy: 0.9758
[3] Test Accuracy: 0.9701
[4] Test Accuracy: 0.9748
[5] Test Accuracy: 0.9813
[6] Test Accuracy: 0.9813
[7] Test Accuracy: 0.9819
[8] Test Accuracy: 0.9822
[9] Test Accuracy: 0.9808
```

可见，在训练的中间部分，loss振荡幅度比普通梯度下降更小。训练前期，模型的收敛速度更快，但到后期比起普通梯度下降并无明显优势。

### Adam

Adam 同时结合了动量与自适应的学习率调节。Adam首先要计算梯度的一阶和二阶矩估计，分别代表了动量与自适应的部分：

$$
\begin{align}
& \mathrm{m} = \beta_1 \mathrm{m} + (1-\beta_1) \frac{\partial L}{\partial W} \\\\
& \mathrm{v} = \beta_2 \mathrm{v} + (1-\beta_2) \frac{\partial L}{\partial W} \odot \frac{\partial L}{\partial W}
\end{align}
$$

然后进行修正：

$$
\begin{align}
& \mathrm{\hat{m}} = \frac{\mathrm{m}}{1-\beta_1 ^ t }\\\\
& \mathrm{\hat{v}} = \frac{\mathrm{v}}{1-\beta_2 ^ t}
\end{align}
$$

最后，参数的更新为：
$$ W = W - \gamma \frac{\mathrm{\hat m}}{\sqrt{\mathrm{\hat v}+ \delta}}$$


设置学习率为0.001， batch_size为128， 开始训练：
![adam](img/train_adam.png)
```
[0] Test Accuracy: 0.9611
[1] Test Accuracy: 0.9701
[2] Test Accuracy: 0.9735
[3] Test Accuracy: 0.9752
[4] Test Accuracy: 0.9787
[5] Test Accuracy: 0.9788
[6] Test Accuracy: 0.9763
[7] Test Accuracy: 0.9790
[8] Test Accuracy: 0.9752
[9] Test Accuracy: 0.9806

```

相比传统梯度下降，loss振荡略微有所减小，前期loss下降速度略微更快，但是最后收敛的速度相当。