import numpy as np
import torch
from torch import matmul as torch_matmul, relu as torch_relu, softmax as torch_softmax, log as torch_log

from numpy_fnn import Matmul, Relu, Softmax, Log, NumpyModel, NumpyLoss
from torch_mnist import TorchModel
from utils import get_torch_initialization, one_hot

err_epsilon = 1e-6
err_p = 0.4


def check_result(numpy_result, torch_result=None):
    if isinstance(numpy_result, list) and torch_result is None:
        flag = True
        for (n, t) in numpy_result:
            flag = flag and check_result(n, t)
        return flag
    # print((torch.from_numpy(numpy_result) - torch_result).abs().mean().item())
    T = (torch_result * torch.from_numpy(numpy_result) < 0).sum().item()
    direction = T / torch_result.numel() < err_p
    return direction and ((torch.from_numpy(numpy_result) - torch_result).abs().mean() < err_epsilon).item()


def case_1():
    x = np.random.normal(size=[5, 6])
    W = np.random.normal(size=[6, 4])
    
    numpy_matmul = Matmul()
    numpy_out = numpy_matmul.forward(x, W)
    numpy_x_grad, numpy_W_grad = numpy_matmul.backward(np.ones_like(numpy_out))
    
    torch_x = torch.from_numpy(x).clone().requires_grad_()
    torch_W = torch.from_numpy(W).clone().requires_grad_()
    
    torch_out = torch_matmul(torch_x, torch_W)
    torch_out.sum().backward()
    
    return check_result([
        (numpy_out, torch_out),
        (numpy_x_grad, torch_x.grad),
        (numpy_W_grad, torch_W.grad)
    ])


def case_2():
    x = np.random.normal(size=[5, 6])
    
    numpy_relu = Relu()
    numpy_out = numpy_relu.forward(x)
    numpy_x_grad = numpy_relu.backward(np.ones_like(numpy_out))
    
    torch_x = torch.from_numpy(x).clone().requires_grad_()
    
    torch_out = torch_relu(torch_x)
    torch_out.sum().backward()
    
    return check_result([
        (numpy_out, torch_out),
        (numpy_x_grad, torch_x.grad),
    ])


def case_3():
    x = np.random.uniform(low=0.0, high=1.0, size=[3, 4])
    
    numpy_log = Log()
    numpy_out = numpy_log.forward(x)
    numpy_x_grad = numpy_log.backward(np.ones_like(numpy_out))
    
    torch_x = torch.from_numpy(x).clone().requires_grad_()
    
    torch_out = torch_log(torch_x)
    torch_out.sum().backward()
    
    return check_result([
        (numpy_out, torch_out),
        
        (numpy_x_grad, torch_x.grad),
    ])


def case_4():
    x = np.random.normal(size=[4, 5])
    
    numpy_softmax = Softmax()
    numpy_out = numpy_softmax.forward(x)
    
    torch_x = torch.from_numpy(x).clone().requires_grad_()
    
    torch_out = torch_softmax(torch_x, 1)
    
    return check_result(numpy_out, torch_out)


def case_5():
    x = np.random.normal(size=[20, 25])
    
    numpy_softmax = Softmax()
    numpy_out = numpy_softmax.forward(x)
    numpy_x_grad = numpy_softmax.backward(np.ones_like(numpy_out))
    
    torch_x = torch.from_numpy(x).clone().requires_grad_()

    torch_out = torch_softmax(torch_x, 1)
    torch_out.sum().backward()

    return check_result([
        (numpy_out, torch_out),
        (numpy_x_grad, torch_x.grad),
    ])


def test_model():
    try:
        numpy_loss = NumpyLoss()
        numpy_model = NumpyModel()
        torch_model = TorchModel()
        torch_model.W1.data, torch_model.W2.data, torch_model.W3.data = get_torch_initialization(numpy=False)
        numpy_model.W1 = torch_model.W1.detach().clone().numpy()
        numpy_model.W2 = torch_model.W2.detach().clone().numpy()
        numpy_model.W3 = torch_model.W3.detach().clone().numpy()
        
        x = torch.randn((10000, 28, 28))
        y = torch.tensor([1, 2, 3, 4, 5, 6, 7, 8, 9, 0] * 1000)
        
        y = one_hot(y, numpy=False)
        x2 = x.numpy()
        y_pred = torch_model.forward(x)
        loss = (-y_pred * y).sum(dim=1).mean()
        loss.backward()
        
        y_pred_numpy = numpy_model.forward(x2)
        numpy_loss.get_loss(y_pred_numpy, y.numpy())
        
        check_flag_1 = check_result(y_pred_numpy, y_pred)
        print("+ {:12} {}/{}".format("forward", 10 * check_flag_1, 10))
    except:
        print("[Runtime Error in forward]")
        print("+ {:12} {}/{}".format("forward", 0, 10))
        return 0
    
    try:
        
        numpy_model.backward(numpy_loss.backward())
        
        check_flag_2 = [
            check_result(numpy_model.log_grad, torch_model.log_input.grad),
            check_result(numpy_model.softmax_grad, torch_model.softmax_input.grad),
            check_result(numpy_model.W3_grad, torch_model.W3.grad),
            check_result(numpy_model.W2_grad, torch_model.W2.grad),
            check_result(numpy_model.W1_grad, torch_model.W1.grad)
        ]
        check_flag_2 = sum(check_flag_2) >= 4
        print("+ {:12} {}/{}".format("backward", 20 * check_flag_2, 20))
    except:
        print("[Runtime Error in backward]")
        print("+ {:12} {}/{}".format("backward", 0, 20))
        check_flag_2 = False
    
    return 10 * check_flag_1 + 20 * check_flag_2


if __name__ == "__main__":
    testcases = [
        ["matmul", case_1, 5],
        ["relu", case_2, 5],
        ["log", case_3, 5],
        ["softmax_1", case_4, 5],
        ["softmax_2", case_5, 10],
    ]
    score = 0
    for case in testcases:
        try:
            res = case[2] if case[1]() else 0
        except:
            print("[Runtime Error in {}]".format(case[0]))
            res = 0
        score += res
        print("+ {:12} {}/{}".format(case[0], res, case[2]))
    score += test_model()
    print("{:14} {}/60".format("FINAL SCORE", score))
