import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot

def mini_batch(dataset, batch_size=128):
    data = np.array([each[0].numpy() for each in dataset])
    label = np.array([each[1] for each in dataset])

    data_size = data.shape[0]
    idx = np.array([i for i in range(data_size)])
    np.random.shuffle(idx)
    
    return [(data[idx[i: i+batch_size]], label[idx[i:i+batch_size]])  for i in range(0, data_size, batch_size)]

class Adam():
    def __init__(self, param, learning_rate=0.001, beta_1=0.9, beta_2=0.999):
        self.param = param
        self.iter = 0
        self.m = 0
        self.v = 0
        self.beta1 = beta_1
        self.beta2 = beta_2
        self.lr = learning_rate
    def optimize(self, grad):
        self.iter+=1
        self.m = self.beta1 * self.m + (1 - self.beta1) * grad
        self.v = self.beta2 * self.v + (1 - self.beta2) * grad * grad
        m_hat = self.m / (1 - self.beta1 ** self.iter)
        v_hat = self.v / (1 - self.beta2 ** self.iter)
        self.param -= self.lr * m_hat / (v_hat ** 0.5 + 1e-8)
        return self.param
        
def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    W1_opt, W2_opt, W3_opt = Adam(model.W1), Adam(model.W2), Adam(model.W3)
    
    train_loss = []
    
    epoch_number = 10
    learning_rate = 0.0015
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset, batch_size=128):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            #model.Adam(learning_rate)
            W1_opt.optimize(model.W1_grad)
            W2_opt.optimize(model.W2_grad)
            W3_opt.optimize(model.W3_grad)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Test Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)
            

if __name__ == "__main__":
    numpy_run()
