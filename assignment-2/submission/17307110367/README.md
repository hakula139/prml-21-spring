# 课程报告

##   模型的训练与测试

直接运行numpy_minist.py，设定batch_size =128, learning_rate = 0.1, epoch_number = 3得到如下的结果:

| 轮次 | Accuracy |
| ---- | -------- |
| [0]  | 0.9474   |
| [1]  | 0.9654   |
| [2]  | 0.9704   |

得到的损失函数的变化如下图所示：

<img src="./img/loss_value1.png" alt="loss" style="zoom:50%;" />

**减小batch_size的值为 64**，保持其他的参数不变进行实验，得到结果如下：

| 轮次 | Accuracy |
| ---- | -------- |
| [0]  | 0.9548   |
| [1]  | 0.9682   |
| [2]  | 0.9679   |

可见当batch_size 减小时，模型准确率在每一轮中进步的幅度会变小。



## mini_batch 的替换

```
def mini_batch(dataset, batch_size=128,seed=0):
    np.random.seed(seed)
    x_train = dataset.train_data
    y_train = dataset.train_labels
    m = y_train.shape[0]
    def mini_batch(dataset, batch_size=128,seed=0):
    np.random.seed(seed)
    x_train = dataset.train_data
    y_train = dataset.train_labels
    m = y_train.shape[0]
    # m为所有样本的数量
    mini_batchs =[]
    permutation = list(np.random.permutation(m))
    # 打乱样本顺序
    shuffle_X = x_train[permutation, :, :]
    shuffle_Y = y_train[permutation]
    num_mini_batch = int(m//batch_size)
    # num_mini_batch为mini_batch的块数
    for i in range(num_mini_batch):
        mini_batch_x = shuffle_X[i*batch_size:(i+1)*batch_size, :, :]
        mini_batch_y = shuffle_Y[i*batch_size:(i+1)*batch_size]
        mini_batch = (mini_batch_x, mini_batch_y)
        mini_batchs.append(mini_batch)
    if m % batch_size != 0:
        # 如果样本数不能被整除，取余下的部分
        mini_batch_X = shuffle_X[num_mini_batch * batch_size:m, :, :]
        mini_batch_Y = shuffle_Y[num_mini_batch * batch_size:m]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batchs.append(mini_batch)
    return mini_batchs
    mini_batchs =[]
    permutation = list(np.random.permutation(m))
    shuffle_X = x_train[permutation, :, :]
    shuffle_Y = y_train[permutation]
    num_mini_batch = int(m//batch_size)
    for i in range(num_mini_batch):
        mini_batch_x = shuffle_X[i*batch_size:(i+1)*batch_size, :, :]
        mini_batch_y = shuffle_Y[i*batch_size:(i+1)*batch_size]
        mini_batch = (mini_batch_x, mini_batch_y)
        mini_batchs.append(mini_batch)
    if m % batch_size != 0:
        # 如果样本数不能被整除，取余下的部分
        mini_batch_X = shuffle_X[num_mini_batch * batch_size:m, :, :]
        mini_batch_Y = shuffle_Y[num_mini_batch * batch_size:m]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batchs.append(mini_batch)
    return mini_batchs
```

整体思路是打乱样本的顺序，然后依次取出batch_size个样本的数据，作为一个mini_batch组。最后mini_batch函数返回所有的mini_batch组的集合

用numpy实现mini_batch之后，运行numpy_minist.py， 设定batch_size =128, learning_rate = 0.1, epoch_number = 3， 得到的实验结果如下：

| 轮次 | Accuracy |
| ---- | -------- |
| [0]  | 0.8780   |
| [1]  | 0.8994   |
| [2]  | 0.9099   |



## 反向传播公式的推导

**Matmul**

Matmul的计算式为
$$
Y = X*W
$$
根据矩阵的求导法则，可以推出
$$
\frac{\partial Y}{\partial X} = W^{T}
$$

$$
\frac{\partial Y}{\partial W} = X^{T}
$$

 对应代码段如下：

```
grad_W = np.matmul(self.memory['x'].T, grad_y)
grad_x = np.matmul(grad_y, self.memory['W'].T)
```



**Relu**

Relu的计算公式为
$$
Y=\begin{cases}
X&X\ge0\\\\
0&\text{otherwise}
\end{cases}
$$
所以有
$$
\frac{\partial Y}{\partial X}=\begin{cases}
1&X>0\\\\
0&\text{otherwise}
\end{cases}
$$
对应代码块如下：

```
grad_x = np.where(self.memory['x'] > 0, grad_y, np.zeros_like(self.memory['x']))
```

意思是说若x大于0， grad_x的结果就是传入的grad_y。否则结果为0



**Log**

log的计算公式为
$$
Y=\ln(X+\epsilon)
$$
因此有
$$
\frac{\partial Y}{\partial X} = \frac1{X+\epsilon}
$$
对应的代码块如下：

```
grad_x = np.multiply(1./(self.memory['x'] + self.epsilon), grad_y)
```



**Softmax**

Softmax的计算公式为
$$
Y=\frac{\exp\{X\}}{\sum_{k=1}^c\exp\{X\}}
$$
根据邱老师书第411~412页对该公式的导数的推导（在这里直接引用结果），有


$$
\frac{\partial Y}{\partial X} = diag(softmax(x))-softmax(x)softmax(x)^{T}
$$
对应的代码块如下

```
out = self.memory['out']
grad_x = []
for idx in range(out.shape[0]):
	dout = np.diag(out[idx]) - np.outer(out[idx], out[idx])
    grad = np.matmul(dout, grad_y[idx])
    grad_x.append(grad)
grad_x = np.array(grad_x)
```

