import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot

def mini_batch(dataset, batch_size=128,seed=0):
    np.random.seed(seed)
    x_train = dataset.train_data
    y_train = dataset.train_labels
    m = y_train.shape[0]
    # m为所有样本的数量
    mini_batchs =[]
    permutation = list(np.random.permutation(m))
    # 打乱样本顺序
    shuffle_X = x_train[permutation, :, :]
    shuffle_Y = y_train[permutation]
    num_mini_batch = int(m//batch_size)
    # num_mini_batch为mini_batch的块数
    for i in range(num_mini_batch):
        mini_batch_x = shuffle_X[i*batch_size:(i+1)*batch_size, :, :]
        mini_batch_y = shuffle_Y[i*batch_size:(i+1)*batch_size]
        mini_batch = (mini_batch_x, mini_batch_y)
        mini_batchs.append(mini_batch)
    if m % batch_size != 0:
        # 如果样本数不能被整除，取余下的部分
        mini_batch_X = shuffle_X[num_mini_batch * batch_size:m, :, :]
        mini_batch_Y = shuffle_Y[num_mini_batch * batch_size:m]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batchs.append(mini_batch)
    return mini_batchs

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()

    train_loss = []

    epoch_number = 3
    learning_rate = 0.1

    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            y_pred = model.forward(x.numpy()/255)
            #y_pred = model.forward(x.numpy())
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)

            train_loss.append(loss.item())

        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))

    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
