import sys, time
import numpy as np
import matplotlib.pyplot as plt
#from mpl_toolkits import mplot3d

class MatMul:
    def __init__(self, W):
        self.params = [W]
        self.grads = [np.zeros_like(W)]
        self.x = None

    def forward(self, x):
        W, = self.params
        out = np.dot(x, W)
        self.x = x
        return out

    def backward(self, dout):
        W, = self.params
        dx = np.dot(dout, W.T)
        dW = np.dot(self.x.T, dout)
        self.grads[0][...] = dW
        return dx

class Affine:
    def __init__(self, W, b):
        self.params = [W, b]
        self.grads = [np.zeros_like(W), np.zeros_like(b)]
        self.x = None

    def forward(self, x):
        W, b = self.params
        out = np.dot(x, W) + b
        self.x = x
        return out 

    def backward(self, dout):
        W, b = self.params
        dx = np.dot(dout, W.T)
        dW = np.dot(self.x.T, dout)
        db = np.sum(dout, axis=0)
        self.grads[0][...] = dW
        self.grads[1][...] = db
        return dx

class ReLU:
    def __init__(self):
        self.params, self.grads = [], []
        self.x = None

    def forward(self, x):
        self.mask = (x <= 0)
        out = x.copy()
        out[self.mask] = 0
        return out

    def backward(self, dout):
        dout[self.mask] = 0
        dx = dout
        return dx 

class IdentityWithMSELoss:
    '''
    identity neuron node with mean square error(MSE) loss 
    '''
    def __init__(self):
        self.params, self.grads = [], []

    def forward(self, x, t):
        self.y = x
        self.t = t
        loss = 0.5 * np.mean(np.sum(np.square(x - t), axis=1))
        return loss

    def backward(self, dout=1):
        batch_size = self.t.shape[0]
        dx = (self.y - self.t) * dout / batch_size
        return dx

class IdentityWithHuberLoss:
    '''
    identity neuron node with Huber error loss 
    '''
    def __init__(self, beta=1.0):
        self.params, self.grads = [], []
        self.beta = beta

    def forward(self, x, t):
        self.y = x
        self.t = t
        l1 = np.sum(self.beta * (np.abs(x - t) - 0.5 * self.beta), axis=1)
        l2 = 0.5 * np.sum(np.square(x - t), axis=1)
        l1_mask = (x - t >= self.beta).astype(int)
        l2_mask = 1 - l1_mask
        loss = np.mean(l1 * l1_mask + l2 * l2_mask)
        return loss

    def backward(self, dout=1):
        batch_size = self.t.shape[0]
        d_l1 = self.beta * np.abs(self.y - self.t) * dout / batch_size
        d_l2 = (self.y - self.t) * dout / batch_size
        l1_mask = (self.y - self.t >= self.beta).astype(int)
        l2_mask = 1 - l1_mask
        dx = d_l1 * l1_mask + d_l2 * l2_mask
        return dx

class SGD:
    '''
    stochastic gradient descent(SGD) optimization 
    '''
    def __init__(self, lr=0.001):
        self.lr = lr

    def update(self, params, grads):
        for i in range(len(params)):
            params[i] -= self.lr * grads[i]

class Adam:
    '''
    adaptive moment(Adam) optimization
    '''
    def __init__(self, lr=0.001, beta1=0.9, beta2=0.999):
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.iter = 0
        self.m = None
        self.v = None
        
    def update(self, params, grads):
        if self.m is None:
            self.m, self.v = [], []
            for param in params:
                self.m.append(np.zeros_like(param))
                self.v.append(np.zeros_like(param))
        self.iter += 1
        lr_t = self.lr * np.sqrt(1.0 - self.beta2**self.iter) / (1.0 - self.beta1**self.iter)
        for i in range(len(params)):
            self.m[i] += (1 - self.beta1) * (grads[i] - self.m[i])
            self.v[i] += (1 - self.beta2) * (grads[i]**2 - self.v[i])
            params[i] -= lr_t * self.m[i] / (np.sqrt(self.v[i]) + 1e-7)        

class TwoReLULayerNet:
    def __init__(self, input_size=1, hidden_size=10, output_size=1, 
                weight_scale='he', loss_func='huber'):
        '''
        weight_scale:   scale value used to initialize weight. 
                        there are 3 possible value 'he', 'xavier' and 'std'
                        'he': set sqrt(2/n) as weight init scale, proposed by Kaiming He
                        'xavier': set sqrt(1/n) as weight init scale, proposed by Xavier Glorot
                        'std': set 0.01 as weight init scale
        loss_func:      loss function type. there are 2 possible value here, 'huber' and 'mse'
                        'huber': Huber error loss
                        'mse': MSE loss 
        '''
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.weight_scale = weight_scale
        self.loss_func = loss_func
        # init w and b 
        scale = self.get_weight_scale(input_size) 
        W1 = scale * np.random.randn(input_size, hidden_size)
        b1 = np.random.randn(hidden_size)
        scale = self.get_weight_scale(hidden_size)
        W2 = scale * np.random.randn(hidden_size, output_size)
        # init layers
        self.layers = [Affine(W1, b1), ReLU(), MatMul(W2)]
        if loss_func == 'huber':
            self.loss_layer = IdentityWithHuberLoss()
        else: # mse loss function
            self.loss_layer = IdentityWithMSELoss()
        # init parameters and gradients
        self.params, self.grads = [], []
        for layer in self.layers:
            self.params += layer.params
            self.grads += layer.grads
    
    def get_weight_scale(self, node_size):
        scale = 0.01
        if self.weight_scale == 'he':
            scale = np.sqrt(2.0 / node_size)
        elif self.weight_scale == 'xavier':
            scale = np.sqrt(2.0 / node_size)
        return scale

    def predict(self, x):
        for layer in self.layers:
            x = layer.forward(x)
        return x

    def forward(self, x, t):
        y = self.predict(x)
        loss = self.loss_layer.forward(y, t)
        return loss

    def backward(self, dout=1):
        dout = self.loss_layer.backward(dout)
        for layer in reversed(self.layers):
            dout = layer.backward(dout)
        return dout

class Trainer:
    def __init__(self, model, optimizer):
        self.model = model
        self.optimizer = optimizer
        self.loss_list = []
        self.eval_interval = None

    def fit(self, x, t, max_epoch=10, batch_size=36, eval_interval=20):
        data_size = len(x)
        max_iters = data_size // batch_size
        self.eval_interval = eval_interval
        model, optimizer = self.model, self.optimizer
        epoch_loss, epoch_loss_count = 0, 0
        start_time = time.time()
        for epoch in range(max_epoch):
            idx = np.random.permutation(np.arange(data_size))
            x = x[idx]
            t = t[idx]
            iter_loss, iter_loss_count = 0, 0
            for iter in range(max_iters):
                batch_x = x[iter*batch_size:(iter+1)*batch_size]
                batch_t = t[iter*batch_size:(iter+1)*batch_size]
                loss = model.forward(batch_x, batch_t)
                model.backward()
                optimizer.update(model.params, model.grads)
                epoch_loss += loss
                epoch_loss_count += 1
                iter_loss += loss
                iter_loss_count += 1
                if (eval_interval is not None) and (iter % eval_interval) == 0:
                    iter_avg_loss = iter_loss / iter_loss_count
                    self.loss_list.append(float(iter_avg_loss))
                    iter_loss, iter_loss_count = 0, 0
            epoch_avg_loss = epoch_loss / epoch_loss_count
            elapsed_time = time.time() - start_time
            print('| epoch %d | time %d[s] | loss %.2f' % (epoch + 1, elapsed_time, epoch_avg_loss))
            epoch_loss, epoch_loss_count = 0, 0

    def plot(self, ylim=None):
        x = np.arange(len(self.loss_list))
        if ylim is not None:
            plt.ylim(*ylim)
        plt.plot(x, self.loss_list, label='train')
        plt.xlabel('iterations (x' + str(self.eval_interval) + ')')
        plt.ylabel('loss')
        plt.show()

def grid_search_cv(x, t, param_grid, max_epoch=5):
    input_size = x.shape[1]
    output_size = t.shape[1]
    hidden_size, weight_scale, loss_func, batch_size, learning_rate = np.meshgrid(
            param_grid['hidden_size'], 
            param_grid['weight_scale'],
            param_grid['loss_func'],
            param_grid['batch_size'], 
            param_grid['learning_rate'])
    params = [p for p in zip(hidden_size.flat, weight_scale.flat, 
            loss_func.flat, batch_size.flat, learning_rate.flat)]
    min_score = 99999999
    best_param = {}
    for param in params:
        print(param)
        # split train data and validation data 
        (x_train, t_train), (x_val, t_val) = split_dataset(x, t, 0.2)
        # train model with train data
        model = TwoReLULayerNet(input_size, param[0], output_size, param[1], param[2])
        optimizer = Adam(param[4])
        trainer = Trainer(model, optimizer)
        trainer.fit(x_train, t_train, max_epoch, param[3], None)
        # evaluate model with validation data
        score = model.forward(x_val, t_val)
        print('score: ', score)
        if score < min_score:
            min_score = score
            best_param['hidden_size'] = param[0]
            best_param['weight_scale'] = param[1]
            best_param['loss_func'] = param[2]
            best_param['batch_size'] = param[3]
            best_param['learning_rate'] = param[4]
    return best_param

def split_dataset(x, t, rate=0.2):
    N = x.shape[0]
    idx = np.random.permutation(np.arange(N))
    x = x[idx]
    t = t[idx]
    split_idx = int(N * rate)
    x_train = x[split_idx:]
    t_train = t[split_idx:]
    x_test = x[:split_idx]
    t_test = t[:split_idx]
    return (x_train, t_train), (x_test, t_test)

def load_data(func, params):
    # parse func parameters
    x = params.get('x', np.array([]))
    if len(x) == 1:
        x = x[0]
    else:
        d_grid = np.meshgrid(*x)
        x = np.array([p for p in zip(*[d.flat for d in d_grid])])
    kwargs = params.get('kwargs', np.array([]))
    # execute function to get 't'
    t = eval(func)
    # reshape x and t
    N = x.shape[0]
    x = x.reshape(N, -1)
    N = t.shape[0]
    t = t.reshape(N, -1)
    return x, t

def relu_func(x):
    y = np.zeros_like(x)
    y[...] = x
    y[x < 0] = 0
    return y

def bump_func_1d(x, x1, x2, h):
    y = np.ones_like(x) * h
    y[x < x1] = 0
    y[x > x2] = 0
    return y

def approx_bump_1d(x, x1, x2, h, d):
    y1 = d * h * relu_func(x - x1) / (x2 - x1)
    y2 = -d * h * relu_func(x - (x1  + (x2 - x1) / d)) / (x2 - x1)
    y3 = -d * h * relu_func(x - (x2 - (x2 - x1) / d)) / (x2 - x1)
    y4 = d * h * relu_func(x - x2) / (x2 - x1)
    return y1 + y2 + y3 + y4

def rsqare(x, t):
    mse = np.mean(np.sum(np.square(x - t), axis=1))
    loss = 1 - mse / np.var(t)
    return loss

def approx_func3(x):
    sum = np.zeros_like(x)
    for i in range(10):
        t = np.sin((2 * i + 1) * x) / (2 * i + 1)
        sum += t
    return 0.5 - 2 * sum / np.pi

def approx_func4(x):
    sum = np.zeros_like(x)
    for i in range(10):
        t = np.cos((np.power(17,i) * np.pi * x) / np.power(2,i))
        sum += t
    return sum

def approx_func5(x):
    t = np.exp(-1 / (1 - np.square(x)))
    t[np.abs(x) >= 1] = 0
    return t

def approx_func6(x):
    t = np.exp(-1 / x)
    t[x <= 0] = 0
    return t

max_epoch = 50
eval_interval = 100
data_size = 10000
x_begin, x_end = -2, 2
param_grid = {
    'hidden_size': [10, 100, 1000],
    'batch_size': [30, 60, 90],
    'learning_rate': [1e-2, 1e-4, 1e-6],
    'weight_scale': ['he'], #['he', 'xavier', 'std'],
    'loss_func': ['huber'] #['huber', 'mse']
}
l_config = [
    { # 1
        'func':     'relu_func(x + 0.5) - relu_func(x - 0.5)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*100)], 'kwargs': {'x1': -1, 'x2': 1, 'h': 5, 'd': 1000}},
        'approx':   'draw',
        'title':    'A squashing function constructed with ReLU'
    },
    { # 2
        'func':     'approx_bump_1d(x, **kwargs)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*100)], 'kwargs': {'x1': -1, 'x2': 1, 'h': 5, 'd': 1000}},
        'approx':   'draw',
        'title':    ''
    },
    { # 3
        'func':     '1 - x + np.power(x,2) - np.power(x,3) + np.power(x,4) - np.power(x,5)/5 + np.power(x,6)/6 - np.power(x,7)/7',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*100)]},
        'approx':   'split',
        'split_num': 30,
        'title':    ''
    },
    { # 4
        'func':     'x - np.power(x,3)/3 + np.power(x,5)/5 - np.power(x,7)/7 + np.power(x,9)/9 - np.power(x,11)/11',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*100)]},
        'approx':   'split',
        'split_num': 30,
        'title':    ''
    },
    { # 5
        'func':     'approx_func3(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*100)]},
        'approx':   'split',
        'split_num': 30,
        'title':    ''
    },
    { # 6 
        'func':     'approx_func4(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*10000)]},
        'approx':   'split',
        'split_num': 300,
        'title':    ''
    },
    { # 7 
        'func':     'approx_func5(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*100)]},
        'approx':   'split',
        'split_num': 30,
        'title':    ''
    },
    { # 8
        'func':     'approx_func6(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size*100)]},
        'approx':   'split',
        'split_num': 30,
        'title':    ''
    },
    { # 9
        'func':     '1 - x + np.power(x,2) - np.power(x,3) + np.power(x,4) - np.power(x,5)/5 + np.power(x,6)/6 - np.power(x,7)/7',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size)]},
        'approx':   'dnn',
        'title':    ''
    },
    { # 10
        'func':     'x - np.power(x,3)/3 + np.power(x,5)/5 - np.power(x,7)/7 + np.power(x,9)/9 - np.power(x,11)/11',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size)]},
        'approx':   'dnn',
        'title':    ''
    },
    { # 11
        'func':     'approx_func3(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size)]},
        'approx':   'dnn',
        'title':    ''
    },
    { # 12
        'func':     'approx_func4(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size)]},
        'approx':   'dnn',
        'title':    ''
    },
    { # 13
        'func':     'approx_func5(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size)]},
        'approx':   'dnn',
        'title':    ''
    },
    { # 14
        'func':     'approx_func6(x)',
        'params':   {'x': [np.linspace(x_begin, x_end, data_size)]},
        'approx':   'dnn',
        'title':    ''
    },
]

if __name__ == '__main__':
    max_lab = len(l_config)
    
    # parse user input
    if len(sys.argv) > 1 and sys.argv[1].isnumeric():
        lab_num = int(sys.argv[1])
    else:
        print('Usage: python source lab_num (1 ~ {0})'.format(max_lab))
        sys.exit(-1) 

    if lab_num > max_lab or lab_num < 1:
        print('Usage: python source lab_num (1 ~ {0})'.format(max_lab))
        sys.exit(-1)     

    # prepare dataset
    func = l_config[lab_num-1]['func']
    params = l_config[lab_num-1]['params']
    approx = l_config[lab_num-1]['approx']
    title = l_config[lab_num-1]['title']
    x_data, t_data = load_data(func, params)

    dim = x_data.shape[1]
    if dim > 1:
        #print('Cannot draw diagram with dimension higher tha 3.')
        print('Only implement senario with dimension 1')
        exit(-1)

    # approximate function 
    if approx == 'dnn':
        (x_train, t_train), (x_test, t_test) = split_dataset(x_data, t_data)
        # grid search best hyperparameters
        best_param = grid_search_cv(x_train, t_train, param_grid, max_epoch=6)
        # create neuron network to approximate target function
        model = TwoReLULayerNet(x_train.shape[1],
                                best_param['hidden_size'],
                                t_train.shape[1],
                                best_param['weight_scale'],
                                best_param['loss_func'])
        optimizer = Adam(best_param['learning_rate'])
        trainer = Trainer(model, optimizer)
        trainer.fit(x_train, t_train, max_epoch, best_param['batch_size'], eval_interval)
        # evaluate model with test data
        print('best param: ', best_param)
        t_predict = model.predict(x_test)
        test_loss = rsqare(t_predict, t_test)
        print('| final test | loss %.8f' % (test_loss))
        # draw diagram
        trainer.plot()
        plt.plot(x_data, t_data)
        plt.plot(x_data, model.predict(x_data), color='coral')
    elif approx == 'split':
        # split x for each dimention
        plt.plot(x_data, t_data)
        split_num = l_config[lab_num-1]['split_num']
        x_split = np.linspace(x_begin, x_end, split_num+1)
        x_sample = np.array([(x_split[i] + x_split[i+1]) / 2 for i in range(len(x_split)-1)])
        xs = np.array_split(x_data, split_num)
        t_bumps = np.array([])
        for i in range(split_num):
            # get h
            params['x'] = [x_sample[i:i+1]]
            _, h = load_data(func, params)
            # get x1, x2
            x1 = x_split[i]
            x2 = x_split[i+1]
            # load bump function data
            bump_params = {}
            bump_params['x'] = [xs[i]]
            bump_params['kwargs'] = {'x1': x1, 'x2': x2, 'h': h[0], 'd': 1000}
            bump_func = 'approx_bump_1d(x, **kwargs)'
            x_bump, t_bump = load_data(bump_func, bump_params)
            t_bumps = np.concatenate((t_bumps, t_bump.flatten()))
            plt.plot(x_bump, t_bump, color='coral')
    else:
        plt.plot(x_data, t_data, color='coral')
    plt.title(title)
    plt.grid()
    plt.show()