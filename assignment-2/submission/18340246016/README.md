**1. Mini-batch 函数的实现** 

```
def mini_batch(dataset, batch_size=128, numpy=False):

# 仿照batch函数分离data与label 
    data = []
    label = []
    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])

    data = np.array(data)
    label = np.array(label)


    res = []

# 以batch_size为单位，从[0,len(data)]分割mini-batch
    for start_idx in range(0, data.shape[0], batch_size):
        end_idx = min(start_idx + batch_size, len(data))
        res.append((data[start_idx:end_idx],label[start_idx:end_idx]))

    return res
```

 **2. 模型的训练和测试** 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0430/113123_f204953a_8823823.png "processon.png")

```
    # 前向传播过程
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #

        #x = torch.relu(torch.matmul(x, self.W1))
        # 模仿torch的relu(Matmul.(x,w1))操作，下同
        x = self.relu_1.forward(self.matmul_1.forward(x,self.W1))
        
        #x = torch.relu(torch.matmul(x, self.W2))
        x = self.relu_2.forward(self.matmul_2.forward(x,self.W2))
        
        #x = torch.matmul(x, self.W3)
        #x = torch.softmax(x, 1)
        x =  self.softmax.forward(self.matmul_3.forward(x,self.W3))
        
        #x = torch.log(x)
        x = self.log.forward(x)

        ####################
        
        return x
    
    # 反向传播过程
    def backward(self, y):
        
        ####################
        #      code 7      #
        
        self.log_grad = self.log.backward(y)
        
        self.softmax_grad = self.softmax.backward(self.log_grad)
        
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
        
        
        
        ####################
        
        pass
```





| Epoch                | Batch_size  |
| ---------------- | ------ |
| 3 | 16 | 
| 10 | 128 |


* Epoch = 3 Batch_size=128

[0] Accuracy: 0.9373
<br>[1] Accuracy: 0.9583
<br>[2] Accuracy: 0.9683

![输入图片说明](https://images.gitee.com/uploads/images/2021/0430/102618_2f8a1661_8823823.png "3-128.png")



* Epoch = 3 Batch_size=16

[0] Accuracy: 0.9640
<br>[1] Accuracy: 0.9676
<br>[2] Accuracy: 0.9707

![输入图片说明](https://images.gitee.com/uploads/images/2021/0430/103502_131ca59f_8823823.png "3-16.png")

* Epoch = 10 Batch_size=16

[0] Accuracy: 0.9602
<br>[1] Accuracy: 0.9657
<br>[2] Accuracy: 0.9741
<br>[3] Accuracy: 0.9747
<br>[4] Accuracy: 0.9701
<br>[5] Accuracy: 0.9731
<br>[6] Accuracy: 0.9760
<br>[7] Accuracy: 0.9763
<br>[8] Accuracy: 0.9760
<br>[9] Accuracy: 0.9780

![输入图片说明](https://images.gitee.com/uploads/images/2021/0430/105531_82cafce0_8823823.png "10-16.png")


* Epoch = 10 Batch_size=128

[0] Accuracy: 0.9389
<br>[1] Accuracy: 0.9585
<br>[2] Accuracy: 0.9679
<br>[3] Accuracy: 0.9706
<br>[4] Accuracy: 0.9746
<br>[5] Accuracy: 0.9760
<br>[6] Accuracy: 0.9769
<br>[7] Accuracy: 0.9777
<br>[8] Accuracy: 0.9781
<br>[9] Accuracy: 0.9781

![输入图片说明](https://images.gitee.com/uploads/images/2021/0430/111052_ddb4ecda_8823823.png "10-128.png")



*探究发现*
1. mini-batch size太小的情况下，模型的波动率非常高，并不是对batch_size越细分模型的准确率就越高
2. 从epoch的数量上来看，epoch的数量与模型accuracy有正相关性


 **3.Momentum 优化函数下的对比试验** 


```
    # 利用momentum优化下的optimize函数
    def optimize(self, learning_rate):
        
        # 初始化V值
        v1=0
        v2=0
        v3=0
        mu=0.9
        
        # momentum下的更新算法
        v1 = mu * v1 - learning_rate * self.W1_grad
        self.W1 = self.W1 + v1
        
        v2 = mu * v2 - learning_rate * self.W2_grad
        self.W2 = self.W2 + v2
        
        v3 = mu * v3 - learning_rate * self.W3_grad
        self.W3 = self.W3 + v3
        
        
        #self.W1 -= learning_rate * self.W1_grad
       # self.W2 -= learning_rate * self.W2_grad
        #self.W3 -= learning_rate * self.W3_grad
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0430/115301_1fa39f2b_8823823.png "屏幕截图.png")

1. 在我们的测试数据下，momentum与梯度下降模型准确率差异不大
2. momentum模型主要用来解决全局最优与局部最优之间的差异，但在本测试集下不存在全局与局部的差异
3. 理论上来说，当 momentum 动量越大时，其转换为势能的能量也就越大，就越有可能摆脱局部凹域的束缚，进入全局凹域