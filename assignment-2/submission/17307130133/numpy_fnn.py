import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #
        ####################
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        grad_W = np.matmul(self.memory['x'].T, grad_y)
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        x = self.memory['x']
        grad_x = grad_y * np.where(x > 0, np.ones_like(x), np.zeros_like(x))
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        grad_x = grad_y * np.reciprocal(self.memory['x'] + self.epsilon)

        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        ####################
        #      code 4      #
        ####################
        out = np.exp(x) / np.sum(np.exp(x), axis=1, keepdims=True)
        self.memory['x'] = x
        self.memory['out'] = out
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 5      #
        ####################
        out = self.memory['out']
        Jacobs = np.array([np.diag(r) - np.outer(r, r) for r in out])
        grad_y = grad_y[:, np.newaxis, :]
        grad_x = np.matmul(grad_y, Jacobs).squeeze(axis=1)
        return grad_x


class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))
        
        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()
        
        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        # for momentum and adam
        self.W1_m = 0
        self.W2_m = 0
        self.W3_m = 0
        self.W1_v = 0
        self.W2_v = 0
        self.W3_v = 0

    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        x = self.relu_1.forward(self.matmul_1.forward(x, self.W1))
        x = self.relu_2.forward(self.matmul_2.forward(x, self.W2))
        x = self.matmul_3.forward(x, self.W3)
        x = self.softmax.forward(x)
        x = self.log.forward(x)
        return x
    
    def backward(self, y):
        
        ####################
        #      code 7      #
        ####################
        self.log_grad = self.log.backward(y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
    
    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad

    def optimizeM(self, learning_rate, beta_1=0.9):
        self.W1_m = beta_1 * self.W1_m + (1-beta_1)*self.W1_grad
        self.W2_m = beta_1 * self.W2_m + (1-beta_1)*self.W2_grad
        self.W3_m = beta_1 * self.W3_m + (1-beta_1)*self.W3_grad

        self.W1 -= learning_rate * self.W1_m
        self.W2 -= learning_rate * self.W2_m
        self.W3 -= learning_rate * self.W3_m

    def optimizeAM(self, learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-8):
        self.W1_m = beta_1 * self.W1_m + (1-beta_1) * self.W1_grad
        self.W2_m = beta_1 * self.W2_m + (1-beta_1) * self.W2_grad
        self.W3_m = beta_1 * self.W3_m + (1-beta_1) * self.W3_grad

        self.W1_v = beta_2 * self.W1_v + (1-beta_2) * self.W1_grad * self.W1_grad
        self.W2_v = beta_2 * self.W2_v + (1-beta_2) * self.W2_grad * self.W2_grad
        self.W3_v = beta_2 * self.W3_v + (1-beta_2) * self.W3_grad * self.W3_grad

        self.W1 -= learning_rate * self.W1_m / (self.W1_v**0.5 + epsilon)
        self.W2 -= learning_rate * self.W2_m / (self.W2_v**0.5 + epsilon)
        self.W3 -= learning_rate * self.W3_m / (self.W3_v**0.5 + epsilon)
