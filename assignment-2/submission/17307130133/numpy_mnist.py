import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot


def mini_batch(dataset, shuffle=True, batch_size=128):
    data = np.array([each[0].numpy() for each in dataset])
    label = np.array([each[1] for each in dataset])
    data_size = data.shape[0]

    index = np.arange(data_size)
    if shuffle:
        np.random.shuffle(index)

    mini_batches = []

    for i in range(0, data_size, batch_size):
        mini_batches.append([data[index[i:i+batch_size]], label[index[i:i+batch_size]]])
    return mini_batches


def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 15
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            # model.optimize(learning_rate)
            model.optimizeAM()
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
