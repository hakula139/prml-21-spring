# 选题1

## 前馈神经网络的实现

### Matmul

forward时
$$
Y=XW
$$
backward时
$$
\frac {\partial L}{\partial X}=\frac {\partial L}{\partial Y}\frac {\partial Y}{\partial X}=\frac {\partial L}{\partial Y}\frac {\partial XW}{\partial X}=\frac {\partial L}{\partial Y} W^T\\\\
\frac {\partial L}{\partial W}=\frac {\partial L}{\partial Y}\frac {\partial Y}{\partial W}=\frac {\partial L}{\partial Y}\frac {\partial XW}{\partial W}=X^T\frac {\partial L}{\partial Y}
$$

### Relu

forward时
$$
Y_{ij}= \begin{cases} X_{ij}& X_{ij}\geq 0 \\\\
0& otherwise \end{cases}
$$
backward时
$$
\frac{\partial L}{\partial X_{ij}}=\frac{\partial L}{\partial Y_{ij}} \frac{\partial Y_{ij}}{\partial X_{ij}}
\\\\
\frac{\partial Y_{ij}}{\partial X_{ij}}= \begin{cases} 1 & X_{ij}\geq 0 \\\\
0& otherwise \end{cases}\\\\
\therefore \frac{\partial L}{\partial X_{ij}}=\begin{cases} \frac{\partial L}{\partial Y_{ij}} & X_{ij}\geq 0 \\\\
0& otherwise \end{cases}
$$

### Log

forward时
$$
Y_{ij}=ln(X_{ij}+\epsilon)
$$
backward时
$$
\frac{\partial L}{\partial X_{ij}}=\frac{\partial L}{\partial Y_{ij}} \frac{\partial Y_{ij}}{\partial X_{ij}}=\frac{\partial L}{\partial Y_{ij}}\frac {1}{X_{ij}+\epsilon}
$$

### Softmax

forward时
$$
Y_{ij} = \frac {expX_{ij}}{\sum_k expX_{ik}}
$$
backward时，参考课本

![softmax](./img/softmax.png)

Softmax是对每一行进行的操作，由此可以得到每一行的Jacob矩阵为
$$
Jacob = diag(Y_r) - Y_rY_r^T
$$

### 补全NumpyModel

forward部分根据torch_mnisy.py补全，得到网络结构为

input -> matmul_1 -> relu_1 -> matmul_2 -> relu_2 -> matmul_3 -> softmax -> log

根据此结构，依次反向调用之前实现的backward部分即可

## 实验

### 实现mini_batch

有时候训练集数据过多，如果对整个数据集计算平均的损失函数值然后计算梯度进而进行反向传播，训练速度会比较慢；使用随机梯度下降则可能会导致参数无法收敛到最优值。mini_batch方法是这两种方法的折中，将数据集划分为若干批次，在提高训练速度的同时保证了较好的收敛效果。

参考utils.py中mini_batch，在numpy_mnist.py中实现了mini_batch，可调参数有dataset，shuffle，batch_size。下面实验均在我实现的mini_batch上进行。

### 在numpy_mnist.py中进行模型的训练和测试

通过改变batch_size、学习率、是否shuffle这三个变量进行了实验。

![batch_size](./img/batch_size.jpg)

上图左边一列是batch_size为64时的情况，右边两列是batch_size为128时的情况；上面一行学习率为0.1，下面一行学习率为0.05。本次实验都进行了shuffle。可以看出，batch_size越小，在同样的step里收敛得越慢；在训练前期震荡比较大；在训练后期损失函数的震荡反而较小。

![lr](./img/lr.jpg)

上图左边一列是学习率为0.05时的情况，右边两列是学习率为0.1时的情况；同一行的数据其他实验参数相同。可以看出，学习率越小，收敛得就越慢。出乎我意料的是，学习率越小，在收敛后的实验后期，损失函数震荡比学习率大的更严重。

![shuffle](./img/shuffle.jpg)

上图左边未对训练数据进行shuffle，右边进行了shuffle，其他条件一样。可以看出未进行shuffle的模型收敛更慢，损失函数的震荡更大。这是因为在我的mini_batch的实现中，不进行shuffle的话，每次梯度的更新都是固定的total_size/batch_size组数据，相当于数据量减少了。

## momentum、Adam等优化

SGD会造成损失函数的震荡，可能会使最后的结果停留在一个局部最优点。为了抑制SGD的震荡，提出了SGD with Momentum的概念。SGDM在梯度下降过程中加入了一阶动量
$$
m_t = \beta _1m_{t-1}+(1-\beta_1)g_t
$$
也就是说，在梯度下降方向不变时，梯度下降的速度变快，梯度下降方向有所变化的维度上更新速度变慢。这里$\beta _1$的经验值为0.9。也就是说，梯度下降的方向主要是之前累积的下降方向，并略微偏向当前时刻的下降方向。

除了一阶动量，二阶动量也被广泛使用。对于经常更新的参数，我们已经积累了大量关于它的知识，不希望被单个样本影响太大，希望学习速率慢一些；对于偶尔更新的参数，我们了解的信息太少，希望能从每个偶然出现的样本身上多学一些，即学习速率大一些。二阶动量的一种比较好的计算方法是：不累积全部历史梯度，只关注过去一段时间窗口的下降梯度，这种方法解决了学习率急剧下降的问题。这就是AdaDelta/RMSProp。
$$
V_t = \beta _ 2 V_{t-1}+(1-\beta_2)g_t^2
$$
由此，Adam自然而然地出现了，Adam把一阶动量和二阶动量都用起来，下降梯度为
$$
\Delta = \alpha\ m_t/\sqrt V_t
$$
一般为了避免分母为0，会在分母上加一个小的平滑项。$\beta _2$的经验值为0.999，Adam学习率为0.001。

参考：https://zhuanlan.zhihu.com/p/32230623

![op](./img/op.jpg)

上图从左往右第一张图为不使用优化器，第二张图为使用momentum优化，第三张图为使用Adam优化，其中，前两张图学习率为0.1，最后一张图学习率为0.001。可以看出，使用momentum之后前期收敛速度加快，并且在后期损失函数震荡减少。使用Adam优化器，虽然学习率低，但是收敛速度更快，并且损失函数的震荡从训练器前期到后期一直比不使用要小。

## 总结

完成了自动测试																			 60%

![tester](./img/tester.png)

在numpy_minist.py中对模型进行训练和测试							20%

只用NumPy实现了mini_batch函数											 10%

推导了numpy_fnn.py中算子的反向传播公式							 10%

实现了momentum、Adam等优化方法并进行了对比实验   	10%