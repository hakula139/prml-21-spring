# FNN实现

18307130116

## 模型实现

各算子实现参考[算子导数推导部分](##算子导数推导)，网络结构如下图所示

![model](img/model.png)

根据上图对应的模型，建立顺序将算子拼接在一起，并在反向传播时从loss开始逐层回传，基本没什么难点，最终模型构建了函数

$log(softmax(W_3\sigma(W_2\sigma(W_1X)))$

## 模型训练

在运行实现给出的`numpy_minst.py`，共运行了三个epoch，对应的准确率和loss变化情况如下

| epoch | Accuracy |
| ----- | -------- |
| 0     | 94.49%   |
| 1     | 96.47%   |
| 2     | 96.58%   |

![Figure_1](img/Figure_1.png)

### 学习率和epoch的影响

观察发现，loss下降到一定范围后开始上下抖动，推测其原因为接近极值点时学习率过大，为达到更优的性能，我调小的学习率并增大了epoch数量，得到结果如下，并做了不更改学习率仅调整epoch数量的对比实验其中i为[(i-1)*5, i\*5)中位数，20为最终结果

| epoch | Accuracy(learning_rate = 0.1) | Accuracy(learning_rate = 0.05) | Accuracy(learning_rate = 0.1+0.05) |
| ----- | ----------------------------- | ------------------------------ | ---------------------------------- |
| 0     | 97.27%                        | 95.85%                         | 96.59%                             |
| 5     | 97.93%                        | 97.85%                         | 97.91%                             |
| 10    | 98.03%                        | 98.03%                         | 98.18%                             |
| 15    | 98.12%                        | 98.09%                         | 98.18%                             |
| 20    | 98.12%                        | 98.19%                         | 98.18%                             |

<center class="half">
    <img src="img/Figure_2.png" width="450"/>
    <img src="img/Figure_3.png" width="450"/>
    <img src="img/Figure_4.png" width="450"/>
    <center style="color:#C0C0C0;text-decoration:underline">依次为lr=0.1, lr=0.05, lr=0.1+0.05</center>
</center>

可以看到，当学习率调低时，整个收敛过程变慢，在0-5个epoch，0.1的学习率已经达到了97.27%，而0.05仍在95.85%，这个结果符合预期，从最终的结果上看，lr调小收敛较慢，虽然在epoch=20时偶然达到了较高水平，但是在15-20的中位数仍然低于lr = 0.1，推测可能原因为lr过小导致epoch=20时模型收敛程度不好

进一步的，观察发现，该模型在epoch=10时基本已经趋向于收敛，综合考量lr=0.1收敛较快和lr=0.05步长小，最终更可能收敛到最优的极值点两个因素，我做了一个简单的trade-off，前10个epoch采用0.1的学习率，后10个epoch采用0.05，加快收敛的同时，减少在极值点附近的震荡，最终效果符合预期，epoch在15-20区间提升了0.06个百分点，从图上也能看出，在step = 6000附近震荡减小符合预期

在实际的训练过程中，有一系列调度方法根据梯度动态调整学习率，这个实验只是实际训练的简化版，但也应证了学习率调整的重要性

另一方面，epoch增多也显著提升了模型的最终表现，使得其收敛效果更好，符合预期

## `mini_batch`实现

原先的mini_batch主要是套用了PyTorch的dataloader，本质上完成的工作是给定一个batch_size，返回指定batch_size大小的数据，为了事先指定的逻辑，用numpy复现的dataloader首先将数据集中所有的内容存在一个list中，原先函数参数中的shuffle，利用numpy随机打乱。

由于dataloader原先的`drop_last`参数默认为False，在`mini_batch`实现中，如果dataset的总数不为batchsize的整数倍且drop_last的值为False，最后一个部分数据也会被加入进去，batch将会小一点

最后返回的data将会是一个[num, batch_size, 1, 28,28]的numpy数组，而label则是[num, batch_size]的numpy数组，其中num为数据集batch数量

## 算子导数推导

在这部分推导过程中将广泛采用矩阵论的做法推导对应的导数，我将该问题的本质看成了标量对矩阵复合求导的问题，采用微分性质和迹方法变换得到最终结果，即
$$
已有dl = tr（\frac{\partial l}{\partial Y}^T dY）,Y=\sigma(X)，将其化简为dl = tr（\frac{\partial l}{\partial X}^T dX）
$$
`softmax`部分的推导最为复杂，将重点对该部分运算方法与原理细致介绍，其他算子采用的运算性质大多被`softmax`包涵，推导过程中将会省略

### Softmax

（由于gitee的公式支持问题，以下为推导过程截图）

![softmax1](img/softmax1.png)

![softmax2](img/softmax2.png)

### Log

$dl = tr（\frac{\partial l}{\partial Y}^T dY）,Y=log(X+\epsilon)$

$dY = dlog(X +\epsilon) = log'(X+\epsilon)\odot dx = \frac{1}{x+\epsilon}\odot dx$

$dl = tr(D_y^T*(\frac{1}{x+\epsilon}\odot dx)) = tr((D_Y\odot \frac{1}{x+\epsilon})^T*dx)$

$D_X = (D_Y\odot \frac{1}{x+\epsilon})$

### Relu

$dl = tr（\frac{\partial l}{\partial Y}^T dY）,Y=h(X)$

$D_{x_{ij}} = 1, x_{ij} \geq0$ 

$D_{x_{ij}} = 1, x_{ij} < 0$

$其余推导同log，D_X = D_Y\odot h'(x)$

### Matmul

(因为gitee公式问题，这里为推导过程截图)

<img src="img/matmul.png" alt="matmul" style="zoom: 80%;" />



## 优化器

### Adam原理

类似于实验部分做的对学习率的调整，Adam优化器作为一种很多情况下常常使用到的优化器，在自动调整学习率这个点较为出彩，基本已经成为了很多模型优化问题的默认优化器，另一方面初始的学习率选择也影响到了优化过程。

Adam优化器的基本公式为$\theta_t = \theta_{t-1}-\alpha*\hat m_t/(\sqrt{\hat v_t}+\epsilon)$，其中$\hat m_t$以指数移动平均的方式估计样本的一阶矩，并通过超参$\beta_1$的t次方削减初始化为0导致偏差的影响，其基本公式如下，$g_t$为梯度值

$\hat m_t = m_t/(1-\beta_1^t)$,$m_t = \beta_1m_{t-1}+(1-\beta_1)g_t$

类似的计算$\hat v = v_t/(1-\beta_2^t),v_t = \beta_2v_{t-1}+(1-\beta_2)g_t^2$

$\epsilon$目的是为了防止除数变成0

### Momentum原理

Momentum优化器的思路和Adam类似，但是并不考虑标准差对学习率的影响，同样利用滑动窗口机制，指数加权动量，赋给当前梯度一个较小的权重，从而平滑梯度在极值点附近的摆动，更能够接近极值点

其公式如下

$v_t = \beta v_{t-1}+(1-\beta)dW$

$W = W - \alpha v_t$

### 实现

有了如上公式，我在`numpy_mnist.py`中设计了Adam类和Momentum类，由于并不能对`numpy_fnn.py`进行修改，对这两个优化器的实现大体思路变成了，针对每一个变量生成一个优化器，并通过内部变量记录上一轮迭代时参数信息，并计算后返回新的参数，例如Moment的使用呈如下格式：

`model.W1 = W1_opt.optimize(model.W1, model.W1_grad)`

即计算新的权值后，赋给模型

### 实验比较

我们将两个优化器我们同之前获得的最优结果，`lr` = 0.1+0.05方式作比较，loss和Accuracy变化如下

| epoch | Accuracy(learning_rate = 0.1+0.05) | Accuracy（Adam, $\alpha = 0.001$） | Accuracy(Momentum,$\alpha = 0.1$) |
| ----- | ---------------------------------- | ---------------------------------- | --------------------------------- |
| 0     | 96.59%                             | 97.46%                             | 97.01%                            |
| 5     | 97.91%                             | 97.69%                             | 97.95%                            |
| 10    | 98.18%                             | 97.80%                             | 98.07%                            |
| 15    | 98.18%                             | 97.98%                             | 98.22%                            |
| 20    | 98.18%                             | 98.04%                             | 98.36%                            |

<img src="img/Adam.png" alt="Adam" style="zoom: 67%;" /><img src="img/momentum.png" alt="momentum" style="zoom: 67%;" />

### 分析

从表格和loss变化情况来看，Momentum的效果明显优于手动学习率调整，而Adam的效果甚至不如恒定学习率，查看论文中的算法后，我排除了实现错误的可能性，查找了相关资料，发现了这样的一段话：

[简单认识Adam]: https://www.jianshu.com/p/aebcaf8af76e	"Adam的缺陷与改进"

虽然Adam算法目前成为主流的优化算法，不过在很多领域里（如计算机视觉的对象识别、NLP中的机器翻译）的最佳成果仍然是使用带动量（Momentum）的SGD来获取到的。Wilson 等人的论文结果显示，在对象识别、字符级别建模、语法成分分析等方面，自适应学习率方法（包括AdaGrad、AdaDelta、RMSProp、Adam等）通常比Momentum算法效果更差。

根据该资料的说法，本次实验手写数字识别应划归为对象识别，自适应学习率方法确为效果更差，Adam的好处在于，对于不稳定目标函数，效果很好，因此，从这里可以看到，优化器选择应该针对实际问题类型综合考量