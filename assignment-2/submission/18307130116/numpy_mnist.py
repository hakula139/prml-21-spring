import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot

def mini_batch(dataset, batch_size=128, numpy=False, drop_last=False):
    data = []
    label = []
    dataset_num = dataset.__len__()
    idx = np.arange(dataset_num)
    np.random.shuffle(idx)
    for each in dataset:
        data.append(each[0].numpy())
        label.append(each[1])
    label_numpy = np.array(label)[idx]
    data_numpy = np.array(data)[idx]

    result = []
    for iter in range(dataset_num // batch_size):
        result.append((data_numpy[iter*batch_size:(iter+1)*batch_size], label_numpy[iter*batch_size:(iter+1)*batch_size]))
    if drop_last == False:
        result.append((data_numpy[(iter+1)*batch_size:dataset_num], label_numpy[(iter+1)*batch_size:dataset_num]))
    return result

class Adam:
    def __init__(self, weight, lr=0.0015, beta1=0.9, beta2=0.999, epsilon=1e-8):
        self.theta = weight
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.epislon = epsilon
        self.m = 0
        self.v = 0
        self.t = 0

    def optimize(self, grad):
        self.t += 1
        self.m = self.beta1 * self.m + (1 - self.beta1) * grad
        self.v = self.beta2 * self.v + (1 - self.beta2) * grad * grad
        self.m_hat = self.m / (1 - self.beta1 ** self.t)
        self.v_hat = self.v / (1 - self.beta2 ** self.t)
        self.theta -= self.lr * self.m_hat / (self.v_hat ** 0.5 + self.epislon)
        return self.theta

class Momentum:
    def __init__(self, lr=0.1, beta=0.9):
        self.lr = lr
        self.beta = beta
        self.v = 0
    
    def optimize(self, weight, grad):
        self.v = self.beta*self.v + (1-self.beta)*grad
        weight -= self.lr * self.v
        return weight

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    W1_opt = Momentum()
    W2_opt = Momentum()
    W3_opt = Momentum()


    train_loss = []
    
    epoch_number = 20
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            # if epoch >= 10:
            #     learning_rate = 0.05
            # else:
            #     learning_rate = 0.1
            # model.optimize(learning_rate)
            model.W1 = W1_opt.optimize(model.W1, model.W1_grad)
            model.W2 = W2_opt.optimize(model.W2, model.W2_grad)
            model.W3 = W3_opt.optimize(model.W3, model.W3_grad)

            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
