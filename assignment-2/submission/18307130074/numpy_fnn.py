import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        grad_W = np.matmul(self.memory['x'].T, grad_y)
        
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        grad_x = np.where(self.memory['x'] > 0, 1, 0) * grad_y
        
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        grad_x = np.reciprocal(self.memory['x'] + self.epsilon) * grad_y
        
        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        r = np.exp(x)
        s = np.sum(r, axis=1).reshape(-1, 1)
        out = (r / s).astype('float64')
        self.memory['out'] = out

        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        out = self.memory['out']
        Matrix = []
        for i in range(out.shape[0]):
            row = out[i]
            Jacob = np.diag(row) - np.outer(row, row)
            Matrix.append(Jacob)
        Matrix = np.array(Matrix)
        grad_x = np.squeeze(np.matmul(grad_y[:,np.newaxis,:], Matrix), axis=1)

        return grad_x


class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))
        
        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()
        
        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        z1 = self.matmul_1.forward(x, self.W1)
        x2 = self.relu_1.forward(z1)
        z2 = self.matmul_2.forward(x2, self.W2)
        x3 = self.relu_2.forward(z2)
        z3 = self.matmul_3.forward(x3, self.W3)
        out = self.softmax.forward(z3)
        x = self.log.forward(out)
        
        return x
    
    def backward(self, y):
        
        self.log_grad = self.log.backward(y)   
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
        
        pass
    
    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad
