import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss

import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"

# from utils import download_mnist, batch, mini_batch, get_torch_initialization, plot_curve, one_hot
from utils import download_mnist, batch, plot_curve, one_hot

def mini_batch(dataset, batch_size=128):
    
    data = []
    label = []

    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])
    
    label = np.array(label)
    data = np.array(data)

    num = data.shape[0]
    i = np.arange(num)
    np.random.shuffle(i)

    label_ = label[i]
    data_ = data[i]

    res = []
    for id in range(num // batch_size):
        batch_data = data_[id * batch_size: (id + 1) * batch_size]
        batch_label = label_[id * batch_size: (id + 1) * batch_size]
        res.append((batch_data, batch_label))
    
    return res

    
def get_torch_initialization():
    
    def parameters(in_features, out_features, param = 5**0.5):
        bound = (6 / (1 + param * param) / in_features ) ** 0.5
        return np.random.uniform(-bound, bound, (in_features, out_features))
    
    W1 = parameters(28 * 28, 256)
    W2 = parameters(256, 64)
    W3 = parameters(64, 10)
    return W1, W2, W3


def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):

        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
