# Assignment2:FNN

18307130074

## 1. numpy_fnn.py算子反向传播推导

### 1. Matmul

首先正向计算时有矩阵 x(N * d') * W(d' * d) = h(N * d)

假设反向传播中输入的梯度为y(N * d)

则有
$$
y_{ij} = \frac{\partial Loss}{\partial h_{ij}}
$$
计算W的梯度
$$
\begin{aligned}
	\frac{\partial Loss}{\partial W_{pq}}
		&=\sum_{i \leqslant N\\ j\leqslant d} \frac{\partial Loss}{\partial h_{ij}} \times \frac{\partial h_{ij}}{\partial W_{pq}}\\\\
		&=\sum_{i \leqslant N} \ y_{iq} \times \frac{\partial h_{iq}}{\partial W_{pq}}\\\\
		&=\sum_{i \leqslant N} \ y_{iq} \times \ x_{ip}\\\\
		&=\sum_{i \leqslant N} \ x_{pi}^T \times y_{iq}
\end{aligned}
$$
所以有
$$
\frac{\partial Loss}{\partial W} = x^T \times y
$$
同理可得P的梯度
$$
\frac{\partial Loss}{\partial x} = y \times W^T
$$

### 2. RELU

正向计算有RELU(x(N * d)) = out(N * d)
$$
out = \begin{cases}
	  0& where\ x \leq 0 \\\\
	  x& where\ x > 0
	  \end{cases}
$$
计算x的梯度
$$
\begin{aligned}
	\frac{\partial Loss}{\partial x_{ij}}
		&=\frac{\partial Loss}{\partial out_{ij}} \times \frac{d_{out_{ij}}}{d_{x_{ij}}}\\\\
		&=y \times \frac{d_{out_{ij}}}{d_{x_{ij}}}
\end{aligned}
$$
其中
$$
\frac{d_{out_{ij}}}{d_{x_{ij}}} = \begin{cases}
	  0& x_{ij} \leq 0 \\\\
	  1& x_{ij} > 0
	  \end{cases}
$$
所以
$$
\frac{\partial Loss}{\partial x} = y \times x'\\\
其中x' = \begin{cases}
	  0& where\ x \leq 0 \\\\
	  1& where\ x > 0
	  \end{cases}
$$


### 3. Log

正向计算有Log(x(N * d)) = out(N * d)
$$
out_{ij} = log\ x_{ij}
$$
计算x的梯度
$$
\begin{aligned}
	\frac{\partial Loss}{\partial x_{ij}}
		&=\frac{\partial Loss}{\partial out_{ij}} \times \frac{d_{out_{ij}}}{d_{x_{ij}}}\\\\
		&=y \times \frac{d_{out_{ij}}}{d_{x_{ij}}}
\end{aligned}
$$
其中
$$
\frac{d_{out_{ij}}}{d_{x_{ij}}} = \frac {1}{x_{ij}}
$$
所以
$$
\frac{\partial Loss}{\partial x} = y \times x'\\\
其中x' = \frac{1}{x}
$$


### 4. Softmax

正向计算有Softmax(x(N * d)) = out(N * d)
$$
out_{ij} = \frac{e^{ij}}{\sum_{k=1}^d e^{x_{ik}}}
$$
可见每一行的out只与所在行有关，所以我们不妨先只考虑一行，然后推广到整个矩阵。
$$
\begin{aligned}
	\frac{\partial Loss}{\partial x_i}
		&=\sum_{j=1}^d \frac{\partial Loss}{\partial out_j} \times \frac{\partial out_j}{\partial x_i}\\\\
		&=y \times \frac{\partial out_i}{\partial x_i}
\end{aligned}
$$
并且有
$$
\frac{\partial out_i}{\partial x_j} =
	\begin{cases}
    out_i \times (1 - out_i) & i = j\\\\
    -out_i \times out_j & i \neq j
    \end{cases}
$$



$$
softmax([x_1, x_2, ...,x_d]) = [out_1, out_2, ..., out_d]\\\\
$$



$$
\frac{\partial Loss}{\partial x} = [\frac{\partial Loss}{\partial out_1}, \frac{\partial Loss}{\partial out_2},...,\frac{\partial Loss}{\partial out_d}] \times 
\begin{matrix}
\frac{\partial out_1}{\partial x_1} & \frac{\partial out_1}{\partial x_2} & ... & \frac{\partial out_1}{\partial x_d}\\\\
\frac{\partial out_2}{\partial x_1} & \frac{\partial out_2}{\partial x_2} & ... & \frac{\partial out_2}{\partial x_d}\\\\
... & ... & ... & ...\\\\
\frac{\partial out_d}{\partial x_1} & \frac{\partial out_d}{\partial x_2} & ... & \frac{\partial out_d}{\partial x_d}\\\\
\end{matrix}
$$
可以看到[1 * d]维的向量需要乘一个[d * d]的矩阵才可得到梯度

那么对于[N * d]维的矩阵则可以通过添加维看成一个[N * 1 * d]的多维矩阵，需要乘一个[N * d * d]的矩阵才能得到梯度，然而得到的矩阵也为[N * 1 * d]，需要通过numpy压缩成[N * d]维的矩阵作为结果返回

代码如下：

```python
out = self.memory['out']
Matrix = []
for i in range(out.shape[0]):
	row = out[i]
	Jacob = np.diag(row) - np.outer(row, row)
	Matrix.append(Jacob)
Matrix = np.array(Matrix)
grad_x = np.squeeze(np.matmul(grad_y[:,np.newaxis,:], Matrix), axis=1)

return grad_x
```



## 2. 模型训练与测试

### 1. 利用utils.py中已经实现的函数来做测试

基础测试

| epoch | accuracy |
| ----- | -------- |
| 0     | 0.9409   |
| 1     | 0.9653   |
| 2     | 0.9694   |

![basic_test](img/basic_test.png)

**考虑更改epoch数量和learning_rate做对比实验**

| epoch | accuracy with learning_rate = 0.05 | accuracy with learning_rate = 0.1 | accuracy with learning_rate = 0.2 |
| ----- | ---------------------------------- | --------------------------------- | --------------------------------- |
| 0     | 0.9266                             | 0.9468                            | 0.9602                            |
| 1     | 0.9482                             | 0.9638                            | 0.9708                            |
| 2     | 0.9571                             | 0.9701                            | 0.9752                            |
| 4     | 0.9692                             | 0.9755                            | 0.9785                            |
| 9     | 0.9787                             | 0.9792                            | 0.9811                            |
| 14    | 0.9793                             | 0.9811                            | 0.9835                            |
| 19    | 0.9811                             | 0.9811                            | 0.9835                            |
| 29    | 0.9820                             | 0.9806                            | 0.9838                            |
| 39    | 0.9823                             | 0.9809                            | 0.9832                            |
| 49    | 0.9822                             | 0.9809                            | 0.9835                            |
| 69    | 0.9826                             | 0.9810                            | 0.9836                            |
| 99    | 0.9826                             | 0.9811                            | 0.9836                            |

由于数据量较大，这里只展示部分数据。实际上不同的learning rate以及其对应的最高accuracy和accuracy所对应的epoch如下图所示。

| learning_rate | epoch | accuracy |
| ------------- | ----- | -------- |
| 0.05          | 44    | 0.9827   |
| 0.1           | 23    | 0.9815   |
| 0.2           | 16    | 0.9839   |

可以看到随着learning_rate的提升，第一次得到最大accuracy的epoch数逐渐降低，前几次的accuracy也较高。此外，可以发现无论learning_rate为何值，accuracy都会存在持续的抖动现象（甚至epoch数还未超过10就已经开始抖动）。

**下面对epoch、learning_rate的选取进行探究**

如果epoch选取过大会导致浪费gpu时间且会导致过拟合，如果选取过小又有可能使得结果并非最优。所以在实验开始之前，给epoch和learning_rate选取合适的值是一件非常有意义的事情。查阅资料后发现，learning_rate最优的选取方式并非是一个定值，而是一个随着epoch变化的函数，所以初始learning_rate可以设置较大，随后每隔几个epoch减半是一种比较好的实现方式。而epoch的选取则没有较为固定的方法，一般是观察loss的变化，选取loss最小时的epoch值，一般选取10左右。下面是一组epoch=10，每隔2个epoch则learning_rate减半，learning_rate初始设为0.2的一组实验数据。

| epoch | accuracy |
| ----- | -------- |
| 0     | 0.9573   |
| 1     | 0.9694   |
| 2     | 0.9759   |
| 3     | 0.9791   |
| 4     | 0.9789   |
| 5     | 0.9789   |
| 6     | 0.9809   |
| 7     | 0.9812   |
| 8     | 0.9807   |
| 9     | 0.9812   |

![research](img/research.png)

与上文所做的固定learning_rate的结果相比之下，效果有稍微的提升，不过不是很明显。

### 2. mini_batch的复现

可以看到mini_batch函数中只有简单的一行

```python
def mini_batch(dataset, batch_size=128, numpy=False):
    return torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True)
```

那么去查阅torch.utils.data的源码中DataLoader类可以看到该函数的作用是给定batch_size和dataset，将数据集打乱并分割成batch_size大小的一个个小数据集。知道了原理之后就可以很简单的利用numpy.shuffle来进行打乱，并进行很简单的数据处理实现该函数。

```python
def mini_batch(dataset, batch_size=128):
    
    data = []
    label = []

    for each in dataset:
        data.append(np.array(each[0]))
        label.append(each[1])
    
    label = np.array(label)
    data = np.array(data)

    num = data.shape[0]
    i = np.arange(num)
    np.random.shuffle(i)

    label_ = label[i]
    data_ = data[i]

    res = []
    for id in range(num // batch_size):
        batch_data = data_[id * batch_size: (id + 1) * batch_size]
        batch_label = label_[id * batch_size: (id + 1) * batch_size]
        res.append((batch_data, batch_label))
    
    return res
```

### 3.利用新版本的mini_batch进行基础测试

| epoch | accuracy |
| ----- | -------- |
| 0     | 0.9476   |
| 1     | 0.9640   |
| 2     | 0.9715   |

可以看到和utils.py中的mini_batch效果相近

![mini_batch](img/mini_batch.png)

## 3. Pytorch权重初始化

首先去查阅torch.nn.linear的源码，如下所示

```python
def __init__(self, in_features: int, out_features: int, bias: bool = True) -> None:
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

def reset_parameters(self) -> None:
        init.kaiming_uniform_(self.weight, a=math.sqrt(5))
        if self.bias is not None:
            fan_in, _ = init._calculate_fan_in_and_fan_out(self.weight)
            bound = 1 / math.sqrt(fan_in)
            init.uniform_(self.bias, -bound, bound)
```

可以看到在定义self.bias和self.weight之后进行了reset_parameters的操作，而在这个函数中关键的一步为init.kaiming_uniform_

去查看该函数的源码，如下所示

```python
def kaiming_uniform_(tensor, a=0, mode='fan_in', nonlinearity='leaky_relu'):
    fan = _calculate_correct_fan(tensor, mode)
    gain = calculate_gain(nonlinearity, a)
    std = gain / math.sqrt(fan)
    bound = math.sqrt(3.0) * std
    with torch.no_grad():
        return tensor.uniform_(-bound, bound)
```

calculate_gain的源码，如下所示

```python
def calculate_gain(nonlinearity, param=None):
    linear_fns = ['linear', 'conv1d', 'conv2d', 'conv3d', 'conv_transpose1d', 'conv_transpose2d', 'conv_transpose3d']
    if nonlinearity in linear_fns or nonlinearity == 'sigmoid':
        return 1
    elif nonlinearity == 'tanh':
        return 5.0 / 3
    elif nonlinearity == 'relu':
        return math.sqrt(2.0)
    elif nonlinearity == 'leaky_relu':
        if param is None:
            negative_slope = 0.01
        elif not isinstance(param, bool) and isinstance(param, int) or isinstance(param, float):
            negative_slope = param
        else:
            raise ValueError("negative_slope {} not a valid number".format(param))
        return math.sqrt(2.0 / (1 + negative_slope ** 2))
    elif nonlinearity == 'selu':
        return 3.0 / 4  
    else:
        raise ValueError("Unsupported nonlinearity {}".format(nonlinearity))
```

| 函数       | gain效果                   |
| ---------- | -------------------------- |
| ReLU       | sqrt(2)                    |
| Leaky_ReLU | sqrt(2 / (1 + param ** 2)) |

由于默认为Leaky_ReLU，所以
$$
bound = \sqrt[]{\frac{2}{1 + param^2}} \times \sqrt[]{\frac{3}{fan\_in}}
$$
而且可以看到默认的param = math.sqrt(5)，所以很容易就可以写出代码，如下所示

```python
def get_torch_initialization():
    
    def parameters(in_features, out_features, param = 5**0.5):
        bound = (6 / (1 + param * param) / in_features ) ** 0.5
        return np.random.uniform(-bound, bound, (in_features, out_features))
    
    W1 = parameters(28 * 28, 256)
    W2 = parameters(256, 64)
    W3 = parameters(64, 10)
    return W1, W2, W3
```

如上为kaiming分布中的均匀分布，也是pytorch初始化参数的做法

其实kaiming分布中还有一种正态分布，和均匀分布稍有不同，将值的求法稍稍改动，如下所示
$$
std = \sqrt[]{\frac{2}{fan\_in \times (1 + param^2)}}
$$
采用复现的get_torch_initialization进行基础测试，结果如下

| epoch | accuracy |
| ----- | -------- |
| 0     | 0.9421   |
| 1     | 0.9658   |
| 2     | 0.9742   |

![get_torch_initialization](img/get_torch_initialization.png)