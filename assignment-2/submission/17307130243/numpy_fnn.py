import numpy as np


class NumpyOp:

    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):

    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h

    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """

        ####################
        #      code 1      #
        ####################

        grad_x = np.matmul(grad_y, self.memory['W'].T)
        grad_W = np.matmul(self.memory['x'].T, grad_y)

        return grad_x, grad_W


class Relu(NumpyOp):

    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """

        ####################
        #      code 2      #
        ####################

        x = self.memory['x']
        grad_x = np.where(x > 0, grad_y, np.zeros_like(grad_y))

        return grad_x


class Log(NumpyOp):

    def forward(self, x):
        """
        x: shape(N, c)
        """

        out = np.log(x + self.epsilon)
        self.memory['x'] = x

        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """

        ####################
        #      code 3      #
        ####################

        x = self.memory['x']
        grad_x = (1 / (x + self.epsilon)) * grad_y

        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """

    def forward(self, x):
        """
        x: shape(N, c)
        """

        ####################
        #      code 4      #
        ####################

        self.memory['x'] = x

        exp = np.exp(x - np.max(x, axis=1, keepdims=True))
        out = exp / np.sum(exp, axis=1, keepdims=True)
        self.memory['out'] = out

        return out

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """

        ####################
        #      code 5      #
        ####################

        y = self.memory['out']
        temp = np.matmul(grad_y[:, np.newaxis], np.matmul(y[:, :, np.newaxis], y[:, np.newaxis, :])).squeeze(1)
        grad_x = -temp + grad_y * y

        return grad_x


class NumpyLoss:

    def __init__(self):
        self.target = None

    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()

    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))

        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        # 以下变量在 momentum 中使用
        self.v1 = np.zeros_like(self.W1)
        self.v2 = np.zeros_like(self.W2)
        self.v3 = np.zeros_like(self.W3)

        # 以下变量在 adam 中使用
        self.m1 = np.zeros_like(self.W1)
        self.m2 = np.zeros_like(self.W2)
        self.m3 = np.zeros_like(self.W3)

        # 迭代次数
        self.t = 0

    def forward(self, x):
        x = x.reshape(-1, 28 * 28)

        ####################
        #      code 6      #
        ####################

        x = self.matmul_1.forward(x, self.W1)
        x = self.relu_1.forward(x)

        x = self.matmul_2.forward(x, self.W2)
        x = self.relu_2.forward(x)

        x = self.matmul_3.forward(x, self.W3)
        x = self.softmax.forward(x)

        x = self.log.forward(x)

        return x

    def backward(self, y):
        ####################
        #      code 7      #
        ####################

        self.log_grad = self.log.backward(y)

        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)

        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)

        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

    def optimize(self, learning_rate):
        self.W1 -= learning_rate * self.W1_grad
        self.W2 -= learning_rate * self.W2_grad
        self.W3 -= learning_rate * self.W3_grad

    def momentum(self, learning_rate, gamma=0.95):
        self.v1 = gamma * self.v1 + (1 - gamma) * self.W1_grad
        self.v2 = gamma * self.v2 + (1 - gamma) * self.W2_grad
        self.v3 = gamma * self.v3 + (1 - gamma) * self.W3_grad

        self.W1 -= learning_rate * self.v1
        self.W2 -= learning_rate * self.v2
        self.W3 -= learning_rate * self.v3

    def AdaGrad(self, learning_rate):
        eps = 1e-7

        self.v1 += self.W1_grad ** 2
        self.v2 += self.W2_grad ** 2
        self.v3 += self.W3_grad ** 2

        self.W1 -= learning_rate * self.W1_grad / (self.v1 ** 0.5 + eps)
        self.W2 -= learning_rate * self.W2_grad / (self.v2 ** 0.5 + eps)
        self.W3 -= learning_rate * self.W3_grad / (self.v3 ** 0.5 + eps)

    def RMSProp(self, learning_rate, decay_rate=0.999):
        eps = 1e-7
        self.v1 = decay_rate * self.v1 + (1 - decay_rate) * np.square(self.W1_grad)
        self.v2 = decay_rate * self.v2 + (1 - decay_rate) * np.square(self.W2_grad)
        self.v3 = decay_rate * self.v3 + (1 - decay_rate) * np.square(self.W3_grad)

        self.W1 -= learning_rate * self.W1_grad / (np.sqrt(self.v1) + eps)
        self.W2 -= learning_rate * self.W2_grad / (np.sqrt(self.v2) + eps)
        self.W3 -= learning_rate * self.W3_grad / (np.sqrt(self.v3) + eps)

    def Adam(self, learning_rate, beta1=0.9, beta2=0.999):
        self.t += 1
        eps = 1e-8
        self.m1 = beta1 * self.m1 + (1 - beta1) * self.W1_grad
        self.m2 = beta1 * self.m2 + (1 - beta1) * self.W2_grad
        self.m3 = beta1 * self.m3 + (1 - beta1) * self.W3_grad

        self.v1 = beta2 * self.v1 + (1 - beta2) * self.W1_grad ** 2
        self.v2 = beta2 * self.v2 + (1 - beta2) * self.W2_grad ** 2
        self.v3 = beta2 * self.v3 + (1 - beta2) * self.W3_grad ** 2

        # 修正
        m1 = self.m1 / (1 - beta1 ** self.t)
        m2 = self.m2 / (1 - beta1 ** self.t)
        m3 = self.m3 / (1 - beta1 ** self.t)

        v1 = self.v1 / (1 - beta2 ** self.t)
        v2 = self.v2 / (1 - beta2 ** self.t)
        v3 = self.v3 / (1 - beta2 ** self.t)

        self.W1 -= learning_rate * m1 / (np.sqrt(v1) + eps)
        self.W2 -= learning_rate * m2 / (np.sqrt(v2) + eps)
        self.W3 -= learning_rate * m3 / (np.sqrt(v3) + eps)
