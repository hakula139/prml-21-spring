import matplotlib.pyplot as plt
import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot
import time

np.random.seed(16)

colors = ['lightskyblue', 'sandybrown', 'mediumpurple', 'olivedrab',
          'gold', 'hotpink']


def normalization(dataset):
    """
    对数据进行标准化
    :param dataset: shape (N, d)
    :return: (dataset - mean) / std
    """
    # 避免溢出
    eps = 1e-8
    temp = dataset - dataset.mean(axis=0)
    return temp / (dataset.var(axis=0) + eps) ** 0.5


def mini_batch(dataset, batch_size=128, shuffle=True, normal=False):
    data = np.array([datum[0].numpy() for datum in dataset])
    label = np.array([datum[1] for datum in dataset])

    num = data.shape[0]
    idx = np.arange(num)

    # shuffle
    if shuffle:
        np.random.shuffle(idx)

    batches = []
    for i in range(0, num, batch_size):
        batch_data = data[idx[i: i + batch_size]]
        batch_label = label[idx[i: i + batch_size]]

        # batch normalization
        if normal:
            batch_data = normalization(batch_data)

        batches.append((batch_data, batch_label))

    # return [(data[idx[i: i + batch_size]], label[idx[i: i + batch_size]]) for i in range(0, num, batch_size)]
    return batches


def get_torch_initialization_numpy():
    bound1 = np.sqrt(6 / (28 * 28))
    bound2 = np.sqrt(6 / 256)
    bound3 = np.sqrt(6 / 64)

    W1 = np.random.uniform(-bound1, bound1, (28 * 28, 256))
    W2 = np.random.uniform(-bound2, bound2, (256, 64))
    W3 = np.random.uniform(-bound3, bound3, (64, 10))

    return W1, W2, W3


def numpy_run(learning_rate=0.1, epoch_number=3, batch_size=128, optimizer='SGD', max_iter=None):
    train_dataset, test_dataset = download_mnist()

    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization_numpy()

    train_loss = []

    epoch_number = epoch_number
    learning_rate = learning_rate

    begin = time.time()  # 开始时间

    acc = []

    iter = 0
    flag = False

    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset, batch_size=batch_size):
            y = one_hot(y)

            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            # model.optimize(learning_rate)
            # model.momentum(learning_rate)
            # model.AdaGrad(learning_rate)
            # model.RMSProp(learning_rate)
            # model.Adam(learning_rate)
            if optimizer == 'SGD':
                model.optimize(learning_rate)
            elif optimizer == 'momentum':
                model.momentum(learning_rate)
            elif optimizer == 'AdaGrad':
                model.momentum(learning_rate)
            elif optimizer == 'RMSProp':
                model.RMSProp(learning_rate)
            elif optimizer == 'Adam':
                model.Adam(learning_rate)

            train_loss.append(loss.item())
            iter += 1
            if max_iter and iter > max_iter:
                flag = True
                break

        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        acc.append(accuracy)
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
        if flag:
            break

    # 结束时间
    end = time.time()
    print('learning rate:', learning_rate)
    print("time:{:.4f}".format(end - begin))

    plot_curve(train_loss)

    #   idx = np.arange(0, len(train_loss), 50)
    # 　plot_curve(train_loss[idx])
    simple_train_loss = []
    for i in range(len(train_loss)):
        if i % 50 == 0:
            simple_train_loss.append(train_loss[i])
    plot_curve(simple_train_loss)
    return acc, simple_train_loss


""" 学习率的模型效果的影响 """
alpha = [0.0001, 0.001, 0.01, 0.1, 1, 2, 5]
alpha2 = [0.0001, 0.001, 0.01, 0.1]


def learning_rate_expr():
    x = np.arange(1, 16)
    plt.grid()
    accs = []
    for i in range(len(alpha)):
        acc, _ = numpy_run(learning_rate=alpha[i], epoch_number=15)

        # plt.plot(x, acc, label='alpha={}'.format(alpha[i]))
        accs.append(acc)

    # figure
    for i in range(len(alpha)):
        plt.plot(x, accs[i], label='alpha={}'.format(alpha[i]))

    plt.legend()
    plt.xlabel('epoch')
    plt.ylabel('accuracy')

    plt.savefig('./img/learning_rate.png', dpi=300, bbox_inches='tight')


def learning_rate_expr2():
    x = np.arange(1, 51)
    plt.grid()
    accs = []
    for i in range(len(alpha)):
        acc, _ = numpy_run(learning_rate=alpha[i], epoch_number=50)

        # plt.plot(x, acc, label='alpha={}'.format(alpha[i]))
        accs.append(acc)

    # figure
    for i in range(len(alpha2)):
        plt.plot(x, accs[i], label='alpha={}'.format(alpha[i]))

    plt.legend()
    plt.xlabel('epoch')
    plt.ylabel('accuracy')

    plt.savefig('./img/learning_rate2.png', dpi=300, bbox_inches='tight')
    plt.show()


"""batch size 对模型效果的影响"""

batch_sizes = [1, 16, 32, 64, 128, 256, 512, 1024]


def batch_size_expr():
    # x = np.arange(1, 16)
    plt.grid()
    accs = []
    for size in batch_sizes:
        acc, _ = numpy_run(learning_rate=0.01, epoch_number=5, batch_size=size, optimizer='SGD', max_iter=5000)

        # figure
        # plt.plot(x, acc, label='size={}'.format(size))
        accs.append(acc)

    for i in range(len(batch_sizes)):
        plt.plot(accs[i], label='size={}'.format(batch_sizes[i]))

    plt.legend()
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.savefig('./img/batch_size.png', dpi=300, bbox_inches='tight')


""" optimizer对模型效果的影响 """
opts = ['SGD', 'momentum', 'AdaGrad', 'RMSProp', 'Adam']


def opt_expr():
    x = np.arange(1, 6)
    accs = []
    losses = []
    for opt in opts:
        acc, loss = numpy_run(learning_rate=0.002, epoch_number=5, batch_size=128, optimizer=opt)
        accs.append(acc)
        losses.append(loss)

    # figure accuracy
    for i in range(5):
        plt.plot(accs[i], label=opts[i])

    plt.grid()
    plt.legend()
    plt.xlabel("step/50")
    plt.ylabel("Accuracy")
    plt.savefig("./img/opt_1.png", dpi=300, bbox_inches='tight')
    plt.show()

    # figure loss
    for i in range(5):
        plt.plot(losses[i], label=opts[i])

    plt.grid()
    plt.legend()
    plt.xlabel("epoch")
    plt.ylabel("Loss Value")
    plt.savefig("./img/opt_2.png", dpi=300, bbox_inches='tight')
    plt.show()


if __name__ == "__main__":
    # numpy_run()
    # learning_rate_expr()
    # learning_rate_expr2()
    # batch_size_expr()
    # opt_expr()
    numpy_run()
