# Assignment2——前馈神经网络

------

## 算子反向传播推导与实现

------

以下记损失函数为$L$, 各算子前向输出为$Y$, $L$ 关于 $Y$ 的梯度为$\frac{\partial L}{\partial Y}$.

### Matmul
对输入$X$的每一行$x$，记$Y$的对应行为$y$，则$xW=y$.  
由向量值函数求导公式$\frac{\partial y}{\partial x}=W^T$以及链式法则可知$\frac{\partial L}{\partial x}=\frac{\partial L}{\partial y}\frac{\partial y}{\partial x}=\frac{\partial L}{\partial y}W^T$，于是$\frac{\partial L}{\partial X}=\frac{\partial L}{\partial Y}W^T$.  
类似可得$\frac{\partial L}{\partial w}=X^T\frac{\partial L}{\partial Y}$.  

代码如下  


```{python}
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        grad_W = np.matmul(self.memory['x'].T, grad_y)

        return grad_x, grad_W
```

### ReLU 

$\frac{\partial y}{\partial x}=\begin{cases}0 \quad x < 0 \\\\ 1 \quad x\geq 0\end{cases}$  
故由链式法则，每一行，$\frac{\partial L}{\partial x}=\frac{\partial L}{\partial y}\odot\frac{\partial y}{\partial x}=\begin{cases}0 \quad x < 0 \\\\  \frac{\partial L}{\partial y}\quad x\geq 0\end{cases}$

```{python}
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        # max(x, 0)
        x = self.memory['x']
        grad_x = np.where(x > 0, grad_y, np.zeros_like(grad_y))

        return grad_x
```

### Log  

$\frac{\partial y}{\partial x}=\frac{1}{x+\epsilon}$

故由链式法则，每一行，$\frac{\partial L}{\partial x}=\frac{\partial L}{\partial y}\odot \frac{1}{x+\epsilon}$
```{python}

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        x = self.memory['x']
        grad_x = (1 / (x + self.epsilon)) * grad_y

        return grad_x
```

### Softmax  

- forward  
代码实现时，为了防止数据上溢，对原始输入中的每一行减去其最大值后再进行指数运算。 

```{python}
    def forward(self, x):
        """
        x: shape(N, c)
        """
        self.memory['x'] = x
        exp = np.exp(x - np.max(x, axis=1, keepdims=True))
        out = exp / np.sum(exp, axis=1, keepdims=True)
        self.memory['out'] = out

        return out
```

- backward  
课本附录中，对每一行：

![softmax](./img/softmax.png)

代码实现如下：  

```{python}

    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        out = self.memory['out']
        J = np.array([np.diag(i) - np.outer(i, i) for i in out])

        grad_y = grad_y[:, np.newaxis, :]
        grad_x = np.matmul(grad_y, J).squeeze(axis=1)
        
        return grad_x

```

## 实验  

------
实验中主要修改了`numpy_mnist.py`中的`numpy_run`函数，使其可以接收参数`learning_rate`，`epoch_number`，`batch_size`，`optimizer`与`max_iter`五个参数，运用在`numpy_fnn`中定义的网络结构进行训练并对结果进行可视化。后续实验中通过对`numpy_run`传入不同的参数来分析与比较各参数对模型学习性能的影响。 

### 网络搭建

阅读文件`torch_mnist.py`中的`TorchModel`类代码，可知其中定义了如下所示的前馈神经网络结构：
> 网络结构图  
![网络模型结构图](./img/net_structure.png)

> 计算图  
![计算图](./img/net_structure-1.png)

于是，根据网络结构，模仿`TorchModel`代码，利用上面用`numpy`实现的算子搭建该网络，具体代码如下：

- foward

```{python}
    x = self.matmul_1.forward(x, self.W1)
    x = self.relu_1.forward(x)

    x = self.matmul_2.forward(x, self.W2)
    x = self.relu_2.forward(x)

    x = self.matmul_3.forward(x, self.W3)
    x = self.softmax.forward(x)

    x = self.log.forward(x)
```

- backward


```{python}
    self.log_grad = self.log.backward(y)

    self.softmax_grad = self.softmax.backward(self.log_grad)
    self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)

    self.relu_2_grad = self.relu_2.backward(self.x3_grad)
    self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)

    self.relu_1_grad = self.relu_1.backward(self.x2_grad)
    self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
```

### mini-batch的numpy实现

用`numpy`实现的 mini-batch 方法位于文件`numpy_mnist` 的函数 `mini_batch`，其将数据集随机打乱后划分为指定大小(batch-size)的多个批次，后续优化中每步梯度下降轮流使用其中的单个批次来计算梯度。以下是原框架代码中调用了`PyTorch`中的`Dataloader`实现的 `mini-batch` 与实验中用`numpy`实现的`mini-batch`的分类效果对比实验的可视化结果。实验中，采用`MNIST`数据集，运用上面搭建的网络结构，令学习率为0.01，batch-size为128，epoch数量为5进行训练。可以看到，两者loss曲线相似，训练集准确率相近，说明`mini-batch`的numpy实现大致正确。

![mini_batch对比](./img/mini_batch.png)

> 左：使用原代码中mini-batch的实验结果

> 右：使用自行实现的mini-batch的实验结果  
> 
|Accuracy|epoch 1|epoch 2|epoch 3|epoch 4|epoch 5|
|--|--|--|--|--|--|
|torch mini-batch|0.9510|0.9640|0.9729|0.9773|0.9773|
|numpy mini-batch|0.9337|0.9636|0.9716|0.9748|0.9756|  


### learning-rate实验   

这部分实验对模型设定多种不同的学习率大小后来进行训练，并根据实验结果对学习率大小对模型性能的影响进行分析。   

实验中所使用的学习率大小 ： 

|learning_rate|0.0001|0.001|0.01|0.1|1|2|5|  
|--|--|--|--|--|--|--|--|

实验中所使用的其他参数(固定不变):  

|epoch number|batch size|optimizer|  
|--|--|--|  
|15|128|SGD|

#### 实验结果  

> 不同学习率下 Accuracy与Epoch的关系图  
![lr](./img/learning_rate.png)   

从图中可以看到，当学习率过大(实验中为大于等于1的情况)时，模型的准确率维持在0.1左右保持不变，模型无法学习。由于模型中使用了ReLU作为激活函数，可以猜测这种情况的发生是由于学习率过大导致了梯度消失、死亡ReLU的现象。而学习率取其他待选值时，模型的训练集准确率都随训练步数而提高，且学习率越大，模型性能提升得越快。学习率取0.1时，模型在第一个epoch后就达到了很高的训练集准确率，并且在随后的训练中也总是胜过其他取值。学习率取0.0001时，模型准确率虽然在训练过程中不断上升，但其起点很低，提升速度也慢，说明这不是一个好的取值。  

> 模型在不同学习率下，每50次迭代后的训练集损失 ：
> 
|learning rate = 0.0001|learning rate = 0.001|
|--|--|  
|![lr=0.0001](./img/lr_1.png)|![lr=0.001](./img/lr_2.png)|  
|**learning rate = 0.01**|**learning rate = 0.1**|
|![lr=0.01](./img/lr_3.png)|![lr=0.1](./img/lr_4.png)|  

从图中可以看出，除了学习率取0.1的情况，其余取值下模型的训练集损失虽然以不同速度降低，但都有较大程度的波动，并未接近收敛。

|Accuracy|epoch 1|epoch 3|epoch 5| epoch 7| epoch 9|epoch 11|epoch 13| epoch 15|   
|--|--|--|--|--|--|--|--|--|  
lr = 0.0001|0.1043|0.1578|0.2507|0.3498|0.4308|0.5035|0.5736|0.6268|
|lr=0.001|0.4540|0.7319|0.8226|0.8503|0.8675|0.8798|0.8883|0.8944|  
|lr=0.01|0.8692|0.9148|0.9287|0.9379|0.9447|0.9509|0.9566|0.9602|  
|lr=0.1|0.9417|0.9637|0.9746|0.9774|0.9787|0.9705|0.9781|0.9801|  

**总结**：为追求模型训练的速度与性能，学习率的选取不应过小。而当学习率过大时，可能会产生数据异常、梯度消失或者模型不稳定等现象，因此过大的学习率也应当规避。  

### batch-size 实验 

这部分实验对模型设定不同的batch-size并进行训练，分析batch-size对模型性能的影响。  

实验中所选用的batch-size： 
|batch-size|1|8|16|32|64|128|256|512|1024|
|--|--|--|--|--|--|--|--|--|--|

实验中使用的其他参数：
|learning rate| epoch number| optimizer| max_iter|
|--|--|--|--|
|0.01|5|SGD|5000| 

由于数据量一定，batch size的大小影响着梯度下降的迭代次数，为了研究batch size对模型性能的影响，需要消减迭代次数所带来的效果提升在其中的作用，所以代码中设置了`max_iter`为最大迭代次数，并将其设置为5000，迭代次数大于5000或epoch达到5时停止训练。  

#### 实验结果

![batch size](./img/batch_size.png)
|batch size=1|batch size=16|batch size=32|batch size=64|  
|--|--|--|--| 
|![b=1](./img/b1_1.png)|![b=16](./img/b16_1.png)|![b=32](./img/b32_1.png)|![b=64](./img/b64_1.png)|
|**batch size=128**|**batch size=256**|**batch size=512**|**batch size=1024**|  
|![b=1](./img/b128_1.png)|![b=16](./img/b256_1.png)|![b=32](./img/b512_1.png)|![b=1024](./img/b1024_1.png)|  

可以看到，随着 batch size 增加，由于批量数据与整体数据更相似，模型的表现更加稳定，但是loss下降变慢，训练速度变慢，因此batch size的选取也应当适中。


### 优化方法实验

------
本部分参考了[CS231n](https://cs231n.github.io/neural-networks-3/#sgd).   

#### SGD  

SGD为上面实验中所采用的优化方法，其在每次迭代中计算一个batch的梯度，并根据固定的学习率与梯度的乘积来更新参数。如上面实验所见，在SGD方法下，选择适当的学习率是不容易的，过小则收敛缓慢，过大则可能造成异常，而且学习率是固定的这一点也不尽合理。以下为几种对梯度下降算法的改进，各有优势。  

#### SGD with momentum
Momentum方法借鉴了物理学中的动量概念，其保留一定的原始梯度，在更新梯度的时候利用它对当前梯度做微调，得到最终的更新方向，这可以增加模型的稳定性。  

示意代码： 
```{python}
# Momentum update
v = mu * v - learning_rate * dx # integrate velocity
x += v # integrate position
```

#### AdaGrad  
AdaGrad算法在训练过程中能对学习率根据梯度大小进行自动调整，使梯度大的参数对应的学习率更快地衰减，其示意代码如下：

```{python}
# Assume the gradient dx and parameter vector x
cache += dx**2
x += - learning_rate * dx / (np.sqrt(cache) + eps)
```

#### RMSprop 

AdaGrad一大弊病是学习率会单调下降且下降速度较快，RMSProp试图避免这种情况，其利用梯度平方的均值来更新改变学习率，而非累加梯度的平方。  

示意代码： 
```{python}  
cache = decay_rate * cache + (1 - decay_rate) * dx**2
x += - learning_rate * dx / (np.sqrt(cache) + eps)
```
其中 decay_rate 一般取 0.9/0.99/0.999

#### Adam  
Adam是一种结合了上述多种方法优点的算法，其示意代码如下： 

```{python}
# t is your iteration counter going from 1 to infinity
m = beta1*m + (1-beta1)*dx
mt = m / (1-beta1**t)
v = beta2*v + (1-beta2)*(dx**2)
vt = v / (1-beta2**t)
x += - learning_rate * mt / (np.sqrt(vt) + eps)
```
一般地，beta1取0.9，beta2取0.999， eps取1e-8  


#### 实验

代码中运用`numpy`对上述方法均予以实现，位于`numpy_fnn.py`文件中的类`Numpy_Model`中。

实验中分别对模型设定不同的优化器进行训练，实验使用的其它参数如下：
|learning rate|epoch number|batch size|  
|--|--|--|  
|0.002|10|128|  


#### 实验结果

![opt1](./img/opt_1.png)

从图中可以看出，Adam算法使模型快速收敛，并有很高的准确率，优势很明显；RMSProp算法可能由于参数不匹配而出现了数值异常；Momentum算法与AdaGrad算法均比SGD稳定一些，但在实验所用模型上效果相近。


### 权重初始化方法  

在神经网络训练中，参数的初始化对迭代是否收敛，以及是否收敛到最优解等重要的问题有很大的影响。在本次实验所使用的前馈神经网络中，如果权重初始化不当，有可能出现梯度消失或梯度爆炸问题。实验的这一部分探究了原始代码中权重的初始化方式。  

首先，通过阅读`utils.py`中的`get_torch_initialization`函数可以得知，代码中借用一个同样大小`torch.nn.Linear`线性层的初始化权重来对权重进行初始化。

然后，阅读Pytorch的官方文档，可知`torch.nn.Linear`的权重初始化方式是`init.kaiming_uniform_(self.weight, a=math.sqrt(5))`，即kaiming均匀分布初始化，其规定权重矩阵的元素服从$(-bound, bound)$上均匀分布，这里$bound=\sqrt{\frac{6}{(1+a^2)d}}$， $d$为输入层的大小，$a$为非线性激活函数在负半轴上的导数值(leaky-ReLU，实验中为ReLU, 故$a$取0)。 

于是，在`numpy_mnist.py`中运用`numpy`实现权重初始化如下： 

```{python}
def get_torch_initialization_numpy():
    bound1 = np.sqrt(6 / (28 * 28))
    bound2 = np.sqrt(6 / 256)
    bound3 = np.sqrt(6 / 64)

    W1 = np.random.uniform(-bound1, bound1, (28 * 28, 256))
    W2 = np.random.uniform(-bound2, bound2, (256, 64))
    W3 = np.random.uniform(-bound3, bound3, (64, 10))

    return W1, W2, W3
``` 

#### 实验  
调用默认`numpy_run()`函数进行实验，结果如下： 
![init](./img/init.png) 
```
[0] Accuracy: 0.9553
[1] Accuracy: 0.9595
[2] Accuracy: 0.9694
```
结果与原始初始化的结果相近。