import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
import torch
from utils import download_mnist, batch, plot_curve, one_hot
import argparse
from matplotlib import pyplot as plt

def mini_batch(dataset, batch_size=128, numpy=False):
    # modify dataset into LIST type and shuffle it
    dataset = list(dataset)
    np.random.shuffle(dataset)
    
    # divide the dataset into different batches
    res = []
    dataset_size = len(dataset)
    for start_index in range(0, dataset_size, batch_size):
        end_index = min(start_index + batch_size, dataset_size)
        # retrieve the data and label of the same batch separately
        data = np.array([np.array(x[0]) for x in dataset[start_index:end_index]])
        label = np.array([x[1] for x in dataset[start_index:end_index]])
        res.append((data, label))
    
    return res

def kaiming_uniform(in_features, out_features, a = 0):
    bound = 6.0 / (1 + a * a) / in_features
    bound = bound ** 0.5
    W = np.random.uniform(low = -bound, high = bound, size = (in_features, out_features))
    return W

def get_torch_initialization(numpy=True):    
    W1 = kaiming_uniform(28 * 28, 256)
    W2 = kaiming_uniform(256, 64)
    W3 = kaiming_uniform(64, 10)
    
    return W1, W2, W3


def numpy_run(parameter_list = [(128, 0.1, "None")], epoch_number = 3):
    train_dataset, test_dataset = download_mnist()
    
    step_loss = []
    epoch_acc = []

    for batch_size, learning_rate, method in parameter_list:
        model = NumpyModel()
        numpy_loss = NumpyLoss()
        model.W1, model.W2, model.W3 = get_torch_initialization()
        
        train_loss = []
        train_acc = []

        for epoch in range(epoch_number):
            for x, y in mini_batch(train_dataset, batch_size=batch_size):
                y = one_hot(y)
                
                y_pred = model.forward(x)
                loss = numpy_loss.get_loss(y_pred, y)

                model.backward(numpy_loss.backward())
                model.optimize(learning_rate, method=method)
                
                train_loss.append(loss.item())
            
            x, y = batch(test_dataset)[0]
            accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
            train_acc.append(accuracy)
            print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
        
        step_loss.append(train_loss)
        epoch_acc.append(train_acc)

    return step_loss, epoch_acc

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--mode', type=str, default="None", help='mode')
    
    args = parser.parse_args()

    print("----- mode: " + args.mode + " -----")

    if args.mode == "None":
        step_loss, epoch_acc = numpy_run()
        plot_curve(step_loss[0])
    
    elif args.mode == "lr":
        # batch_size, learning_rate, optimizer
        parameter_list = [
            (128, 0.1, "None"),
            (128, 0.01, "None"),
            (128, 0.001, "None")
        ]

        step_loss, epoch_acc = numpy_run(
            parameter_list = parameter_list,
            epoch_number = 30
        )

        plt.xlabel('step')
        plt.ylabel('loss')
        plt.plot(step_loss[0], color = 'r', label="lr=0.1")
        plt.plot(step_loss[1], color = 'g', label="lr=0.01")
        plt.plot(step_loss[2], color = 'b', label="lr=0.001")
        plt.legend()
        plt.savefig("exp_lr_step_loss.jpg")
        plt.close()

        plt.xlabel('epoch')
        plt.ylabel('acc')
        plt.plot(epoch_acc[0], color = 'r', label="lr=0.1")
        plt.plot(epoch_acc[1], color = 'g', label="lr=0.01")
        plt.plot(epoch_acc[2], color = 'b', label="lr=0.001")
        plt.legend()
        plt.savefig("exp_lr_epoch_acc.jpg")
        plt.close()
    
    elif args.mode == "batchsize":
        # batch_size, learning_rate, optimizer
        parameter_list = [
            (32, 0.1, "None"),
            (64, 0.1, "None"),
            (128, 0.1, "None")
        ]

        step_loss, epoch_acc = numpy_run(
            parameter_list = parameter_list,
            epoch_number = 30
        )

        plt.xlabel('step')
        plt.ylabel('loss')
        plt.plot(step_loss[0], color = 'r', label="batch_size=32")
        plt.plot(step_loss[1], color = 'g', label="batch_size=64")
        plt.plot(step_loss[2], color = 'b', label="batch_size=128")
        plt.legend()
        plt.savefig("exp_batchsize_step_loss.jpg")
        plt.close()

        plt.xlabel('epoch')
        plt.ylabel('acc')
        plt.plot(epoch_acc[0], color = 'r', label="batch_size=32")
        plt.plot(epoch_acc[1], color = 'g', label="batch_size=64")
        plt.plot(epoch_acc[2], color = 'b', label="batch_size=128")
        plt.legend()
        plt.savefig("exp_batchsize_epoch_acc.jpg")
        plt.close()
    
    elif args.mode == "optimizer":
        # batch_size, learning_rate, optimizer
        parameter_list = [
            (128, 0.001, "adam"),
            (128, 0.1, "momentum"),
            (128, 0.1, "None")
        ]

        step_loss, epoch_acc = numpy_run(
            parameter_list = parameter_list,
            epoch_number = 30
        )

        plt.xlabel('step')
        plt.ylabel('loss')
        plt.plot(step_loss[2], color = 'r', label="origin")
        plt.plot(step_loss[1], color = 'g', label="momentum")
        plt.plot(step_loss[0], color = 'b', label="adam")
        plt.legend()
        plt.savefig("exp_optimizer_step_loss.jpg")
        plt.close()

        plt.xlabel('epoch')
        plt.ylabel('acc')
        plt.plot(epoch_acc[2], color = 'r', label="origin")
        plt.plot(epoch_acc[1], color = 'g', label="momentum")
        plt.plot(epoch_acc[0], color = 'b', label="adam")
        plt.legend()
        plt.savefig("exp_optimizer_epoch_acc.jpg")
        plt.close()

        # batch_size, learning_rate, optimizer
        # parameter_list = [
        #     (128, 0.1, "adam"),
        #     (128, 0.01, "adam"),
        #     (128, 0.001, "adam")
        # ]

        # step_loss, epoch_acc = numpy_run(
        #     parameter_list = parameter_list,
        #     epoch_number = 30
        # )

        # plt.xlabel('step')
        # plt.ylabel('loss')
        # plt.plot(step_loss[0], color = 'r', label="lr=0.1")
        # plt.plot(step_loss[1], color = 'g', label="lr=0.01")
        # plt.plot(step_loss[2], color = 'b', label="lr=0.001")
        # plt.legend()
        # plt.savefig("exp_optimizer_adam_step_loss.jpg")
        # plt.close()

        # plt.xlabel('epoch')
        # plt.ylabel('acc')
        # plt.plot(epoch_acc[0], color = 'r', label="lr=0.1")
        # plt.plot(epoch_acc[1], color = 'g', label="lr=0.01")
        # plt.plot(epoch_acc[2], color = 'b', label="lr=0.001")
        # plt.legend()
        # plt.savefig("exp_optimizer_adam_epoch_acc.jpg")
        # plt.close()

        # parameter_list = [
        #     (128, 0.1, "adam"),
        #     (128, 0.01, "adam"),
        #     (128, 0.001, "adam")
        # ]

        # step_loss, epoch_acc = numpy_run(
        #     parameter_list = parameter_list,
        #     epoch_number = 30
        # )

        # plt.xlabel('step')
        # plt.ylabel('loss')
        # plt.plot(step_loss[0], color = 'r', label="lr=0.1")
        # plt.plot(step_loss[1], color = 'g', label="lr=0.01")
        # plt.plot(step_loss[2], color = 'b', label="lr=0.001")
        # plt.legend()
        # plt.savefig("exp_optimizer_momentum_step_loss.jpg")
        # plt.close()

        # plt.xlabel('epoch')
        # plt.ylabel('acc')
        # plt.plot(epoch_acc[0], color = 'r', label="lr=0.1")
        # plt.plot(epoch_acc[1], color = 'g', label="lr=0.01")
        # plt.plot(epoch_acc[2], color = 'b', label="lr=0.001")
        # plt.legend()
        # plt.savefig("exp_optimizer_momentum_epoch_acc.jpg")
        # plt.close()