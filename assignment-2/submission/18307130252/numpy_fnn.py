import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #
        ####################
        grad_x = np.matmul(grad_y, self.memory['W'].transpose())
        grad_W = np.matmul(self.memory['x'].transpose(), grad_y)
        
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        grad_x = np.where(self.memory['x'] > 0, np.ones_like(self.memory['x']), np.zeros_like(self.memory['x']))
        grad_x *= grad_y

        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        grad_x = np.reciprocal(self.memory['x'] + self.epsilon)
        grad_x *= grad_y

        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        ####################
        #      code 4      #
        ####################
        
        x -= x.max(axis = 1).reshape(x.shape[0], -1) # 防止溢出
        out = np.exp(x) / np.sum(np.exp(x), axis = 1).reshape(-1, 1)
        self.memory['x'] = x

        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 5      #
        ####################
        out = np.exp(self.memory['x']) / np.sum(np.exp(self.memory['x']), axis = 1).reshape(-1, 1)
        grad_x = grad_y * out - np.sum(grad_y * out, axis = 1).reshape(-1, 1) * out
        
        return grad_x


class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))
        
        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()
        
        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        # 以下变量在 Momentum 优化算法中使用并更新
        self.momentum_V1 = 0
        self.momentum_V2 = 0
        self.momentum_V3 = 0
        self.momentum_Beta = 0.9 # 一般取值：[0.5, 0.9, 0.99]，Andrew Ng推荐：0.9

        # 以下变量在 Adam 优化算法中使用并更新
        self.adam_V1 = 0
        self.adam_S1 = 0
        self.adam_V2 = 0
        self.adam_S2 = 0
        self.adam_V3 = 0
        self.adam_S3 = 0
        self.adam_t = 0
        self.adam_epsilon = 1e-8
        self.adam_Beta1 = 0.9
        self.adam_Beta2 = 0.999 # 一般取值：[0.5, 0.9, 0.99]，Andrew Ng推荐：0.9
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        layer1 = self.matmul_1.forward(x, self.W1)
        out1 = self.relu_1.forward(layer1)
        layer2 = self.matmul_2.forward(out1, self.W2)
        out2 = self.relu_2.forward(layer2)
        layer3 = self.matmul_3.forward(out2, self.W3)
        out3 = self.softmax.forward(layer3)
        out = self.log.forward(out3)
        
        return out
    
    def backward(self, y):
        
        ####################
        #      code 7      #
        ####################
        self.log_grad = self.log.backward(y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)

        pass
    
    def optimize(self, learning_rate, method = "None"):
        if method == "None":
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad

        elif method == "momentum":
            self.momentum_V1 = self.momentum_Beta * self.momentum_V1 + (1 - self.momentum_Beta) * self.W1_grad
            self.W1 -= learning_rate * self.momentum_V1
            self.momentum_V2 = self.momentum_Beta * self.momentum_V2 + (1 - self.momentum_Beta) * self.W2_grad
            self.W2 -= learning_rate * self.momentum_V2
            self.momentum_V3 = self.momentum_Beta * self.momentum_V3 + (1 - self.momentum_Beta) * self.W3_grad
            self.W3 -= learning_rate * self.momentum_V3

        elif method == "adam":
            self.adam_t += 1
            self.adam_V1 = self.adam_Beta1 * self.adam_V1 + (1 - self.adam_Beta1) * self.W1_grad
            self.adam_S1 = self.adam_Beta2 * self.adam_S1 + (1 - self.adam_Beta2) * np.square(self.W1_grad)
            v_corrected = self.adam_V1 / (1 - np.power(self.adam_Beta1, self.adam_t))
            s_corrected = self.adam_S1 / (1 - np.power(self.adam_Beta2, self.adam_t))
            self.W1 -= learning_rate * v_corrected / (np.sqrt(s_corrected) + self.adam_epsilon)

            self.adam_V2 = self.adam_Beta1 * self.adam_V2 + (1 - self.adam_Beta1) * self.W2_grad
            self.adam_S2 = self.adam_Beta2 * self.adam_S2 + (1 - self.adam_Beta2) * np.square(self.W2_grad)
            v_corrected = self.adam_V2 / (1 - np.power(self.adam_Beta1, self.adam_t))
            s_corrected = self.adam_S2 / (1 - np.power(self.adam_Beta2, self.adam_t))
            self.W2 -= learning_rate * v_corrected / (np.sqrt(s_corrected) + self.adam_epsilon)

            self.adam_V3 = self.adam_Beta1 * self.adam_V3 + (1 - self.adam_Beta1) * self.W3_grad
            self.adam_S3 = self.adam_Beta2 * self.adam_S3 + (1 - self.adam_Beta2) * np.square(self.W3_grad)
            v_corrected = self.adam_V3 / (1 - np.power(self.adam_Beta1, self.adam_t))
            s_corrected = self.adam_S3 / (1 - np.power(self.adam_Beta2, self.adam_t))
            self.W3 -= learning_rate * v_corrected / (np.sqrt(s_corrected) + self.adam_epsilon)

        else:
            print("The optimize method has not been implemented yet.")