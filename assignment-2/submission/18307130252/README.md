# Assignment-2 FNN

- [Assignment-2 FNN](#assignment-2-fnn)
  - [1. FNN 算子反向传播公式推导](#1-fnn-算子反向传播公式推导)
    - [1.1 Matmul](#11-matmul)
    - [1.2 Relu](#12-relu)
    - [1.3 Log](#13-log)
    - [1.4 Softmax](#14-softmax)
  - [2. 模型的训练与测试](#2-模型的训练与测试)
    - [2.1 模型的前向计算](#21-模型的前向计算)
    - [2.2 模型的反向传播](#22-模型的反向传播)
    - [2.3 小批量梯度下降法 Mini-Batch](#23-小批量梯度下降法-mini-batch)
    - [2.4 实验](#24-实验)
  - [3. 优化方法](#3-优化方法)
    - [3.1 Momentum](#31-momentum)
    - [3.2 Adam](#32-adam)
    - [3.3 实验](#33-实验)
  - [4. 参数初始化](#4-参数初始化)
    - [4.1 Xavier 初始化](#41-xavier-初始化)
    - [4.2 Kaiming/He 初始化](#42-kaiminghe-初始化)
    - [4.3 实验](#43-实验)
  - [5. 参考文献](#5-参考文献)

## 1. FNN 算子反向传播公式推导

### 1.1 Matmul

$$
Y_{ij} = \Sigma_k X_{ik}·W_{kj}
$$

$$
\begin{split}
    \frac{\partial L}{\partial X_{ij}} &= \Sigma_k\frac{\partial L}{\partial Y_{ik}} \frac{\partial Y_{ik}}{\partial X_{ij}}\\\\
    &= \Sigma_k gradY_{ik} \cdot W_{jk}\\\\
    &=\Sigma_k gradY_{ik}\cdot W^T_{kj}\\\\
    \\\\
    \frac{\partial L}{\partial W_{ij}} &= \Sigma_k\frac{\partial L}{\partial Y_{kj}} \frac{\partial Y_{kj}}{\partial W_{ij}}\\\\
    &=\Sigma_k gradY_{kj}\cdot X_{ki}\\\\
    &=\Sigma_k gradY_{kj}\cdot X^T_{ik}\\\\
\end{split}
$$

即 $ \frac{\partial L}{\partial X} = gradY \cdot W^T $,  $ \frac{\partial L}{\partial W} = X^T \cdot gradY$


### 1.2 Relu

$$
Y_{ij} = ReLu(X_{ij}) = 
    \begin{cases}
    X_{ij} & X_{ij} \geq 0 \\\\
    0 & X_{ij} \lt 0
    \end{cases}
= max(0, X_{ij})
$$

$$
\begin{split}
    \frac{\partial L}{\partial X_{ij}} &= \frac{\partial L}{\partial Y_{ij}} \frac{\partial Y_{ij}}{\partial X_{ij}} \\\\
    &= gradY_{ij} \frac{\partial Relu(X_{ij})}{\partial X_{ij}} \\\\
    &= 
    \begin{cases}
        gradY_{ij} \cdot \frac{\partial X_{ij}}{\partial X_{ij}} & X_{ij} \geq 0 \\\\
        gradY_{ij} \cdot \frac{\partial \space 0}{\partial X_{ij}} & X_{ij} \lt 0
    \end{cases} \\\\
    &= 
    \begin{cases}
        gradY_{ij} & X_{ij} \geq 0 \\\\
        0 & X_{ij} \lt 0
    \end{cases}
\end{split}
$$

令 $$M_{ij} = \begin{cases} 1 &  X_{ij} \geq 0\\\\ 0 & X_{ij} \lt 0\end{cases}$$，则 $ \frac{\partial L}{\partial X} = gradY \cdot M$



### 1.3 Log

$$
Y_{ij} = log(X_{ij} + \epsilon)
$$

$$
\begin{split}
\frac{\partial L}{\partial X_{ij}} &= \frac{\partial L}{\partial Y_{ij}} \frac{\partial Y_{ij}}{\partial X_{ij}}\\
&= gradY_{ij} \cdot \frac {\partial log(X_{ij} + \epsilon)}{\partial X_{ij}} \\
&= gradY_{ij} \cdot \frac {1}{X_{ij} + \epsilon}
\end{split}
$$

令 $$M_{ij} = \frac{1}{X_{ij} + \epsilon}$$，则 $ \frac{\partial L}{\partial X} = gradY \cdot M$



### 1.4 Softmax


$$
Y_{ij} = softmax(X_{ij}) = \frac {exp(X_{ij})}{\Sigma_k exp(X_{ik})}
$$

考虑到 $softmax$ 的特殊性质： $softmax((X+c)_i)=\frac{e^{x_i+c}}{\Sigma_k e^{x_k + c}} = \frac{e^{x_i}}{\Sigma_k e^{x_k}} = softmax(X_i)$，实际应用时，通常实现将 $x$ 减去最大值，从而防止溢出。

$$
\begin{split}
    \frac{\partial Y_{ij}}{\partial X_{ik}} &= \frac{\partial }{\partial X_{ik}} \frac{e^{X_{ij}}}{\Sigma_p e^{X_{ip}}}\\\\
    \\\\
    当\space  j = k \space 时， 
    \frac{\partial Y_{ij}}{\partial X_{ik}} &= \frac{\partial Y_{ij}}{\partial X_{ij}} \\\\
    &= \frac{\partial}{\partial X_{ij}}\frac{e^{X_{ij}}}{\Sigma_p e^{X_{ip}}} \\\\
    &= \frac{(e^{X_{ij}})'\cdot\Sigma_p e^{X_{ip}}- e^{X_{ij}}\cdot(\Sigma_p e^{X_{ip}})'}{(\Sigma_p e^{X_{ip}})^2} \\\\
    &= \frac{e^{X_{ij}}\cdot(\Sigma_p e^{X_{ip}}- e^{X_{ij}})}{(\Sigma_p e^{X_{ip}})^2} \\\\
    &=Y_{ij} - Y_{ij}^2 = Y_{ik} - Y_{ij}Y_{ik}  \\\\
    \\\\
    当\space  j \neq k \space 时， 
    \frac{\partial Y_{ij}}{\partial X_{ik}} &= \frac{\partial Y_{ij}}{\partial X_{ik}} \\\\
    &= \frac{\partial}{\partial X_{ik}}\frac{e^{X_{ij}}}{\Sigma_p e^{X_{ip}}} \\\\
    &= \frac{(e^{X_{ij}})'\cdot\Sigma_p e^{X_{ip}}- e^{X_{ij}}\cdot(\Sigma_p e^{X_{ip}})'}{(\Sigma_p e^{X_{ip}})^2} \\\\
    &= \frac{e^{X_{ij}}\cdot e^{X_{ik}}}{(\Sigma_p e^{X_{ip}})^2} \\\\
    &=Y_{ij}Y_{ik}\\\\
\end{split}
$$

$$
\begin{split}
    \frac{\partial L}{\partial X_{ij}} &= \Sigma_k \frac{\partial L}{\partial Y_{ik}}\frac{\partial Y_{ik}}{\partial X_{ij}}\\\\
    &=\Sigma_k gradY_{ik} \cdot 
    \begin{cases}
    Y_{ik} - Y_{ij}Y_{ik} & & j = k\\\\
    Y_{ij}Y_{ik} & & j \neq k
    \end{cases}\\\\
    &=gradY_{ik} \cdot Y_{ik} - \Sigma_k \space gradY_{ik} \cdot Y_{ij} \cdot Y_{ik}\\\\
    &=gradY_{ik} \cdot Y_{ik} - Y_{ji}^T \cdot \Sigma_k \space gradY_{ik} \cdot Y_{ik}
\end{split}
$$

记 $C = A*B$ 为 $C_{ij} = A_{ij} \cdot B_{ij}$, $S_i = \Sigma_k gradY_{ik} \cdot Y_{ik}$，则 $\frac{\partial L}{\partial X} = gradY * Y - Y^T \cdot S$


## 2. 模型的训练与测试

### 2.1 模型的前向计算

![image-20210501201313443](img/neural-network-model.png)
$$
\begin{split}
layer1 &= W1 \cdot X \\\\
out1 &= ReLu(layer1)\\\\
layer2 &= W2 \cdot out1\\\\
out2 &= ReLu(layer2)\\\\
layer3 &= W3 \cdot out2\\\\
out3 &= softmax(layer3)\\\\
out &= log(out3)
\end{split}
$$


### 2.2 模型的反向传播

从后向前，根据之前推导的算子公式依次计算梯度即可：

```python
self.log_grad = self.log.backward(y)
self.softmax_grad = self.softmax.backward(self.log_grad)
self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
self.relu_2_grad = self.relu_2.backward(self.x3_grad)
self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
self.relu_1_grad = self.relu_1.backward(self.x2_grad)
self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
```



### 2.3 小批量梯度下降法 Mini-Batch

由于深度神经网络的训练数据通常较大，如果在整个训练数据集上进行迭代计算梯度，会耗费大量计算资源。同时，大规模的训练数据集往往包含很多冗余信息，因此，在训练深度神经网络时，经常会使用**Mini-Batch**这一优化方法。

在 Mini-Batch 算法中，每次只训练数据集的一小部分，而不训练整个数据集。从而训练模型所需的内存减小，使得原本无法训练的模型可训练。

在每次训练前，对训练数据进行随机 shuffle ，根据预设的 batch_size 将原本的数据集划分成若干个大小相等的 mini-batch （除最后一个 mini-batch 大小可能不一致外），对每个 mini-batch 做梯度下降。

此部分的实现代码，在 `numpy_mnist.py/mini_batch()` 中。



### 2.4 实验

此实验可通过运行 `python numpy_mnisit.py --mode=lr` 进行复现

通过调整学习率分别为 0.1, 0.01, 0.001，从 epoch 和迭代两个角度来讨论 mini-batch 中学习率这一超参的性质。

<img src='img/exp_lr_epoch_acc.png'/>

<img src='img/exp_lr_step_loss.png'/>

通过以上实验，发现对于 MNIST 数据集并采用 mini-batch 随机梯度下降时，学习率越大，模型收敛越快。

因此，取学习率 $lr = 0.1$，调整 mini-batch 的 batch-size ，观察小批量算法中，批量大小对模型训练结果的影响（ `python numpy_mnist.py --mode=batchsize` ) ：

<img src='img/exp_batchsize_epoch_acc.png'/>

<img src='img/exp_batchsize_step_loss.png'/>

从迭代角度看， batchsize 越大，下降效果越明显，模型能越快收敛。

然而从 epoch 角度来看，batchsize 越小，通过同样的回合，模型的拟合准确度往往越高。



## 3. 优化方法

### 3.1 Momentum

SGD 训练参数时，有时下降速度会非常慢，并且有可能会陷入到局部最优解。Momentum 算法能够加快模型的学习过程，并且对于高曲率、小旦一致的梯度，或者噪声比较大的梯度有很好的效果。

其思想主要是通过引入一个新的变量 $v$ 来积累之前的梯度的指数级衰减移动平均，从而加速学习过程。数学表示如下：
$$
\begin{split}
V_{dW} &= \beta V_{dW} &+ \space (1 - \beta)dW \\\\
V_{db} &= \beta V_{db} &+ \space (1 - \beta)db \\\\
W &= W &- \space \alpha V_{dW} \\\\
b &= b &- \space \alpha V_{db}
\end{split}
$$
$\beta$ 越大，则之前积累的梯度对现在的方向影响越大。

$\beta$ 的常见取值为：0.5， 0.9， 0.99。在本次实验中，$\beta$ 取值0.9。



### 3.2 Adam

Adam 算法本质可以看作是 Momentum 算法和 RMSprop 算法的结合，不但使用动量作为参数更新的方向，而且可以自适应地调整学习率。

其数学表示如下：
$$
\begin{split}
M_t &= \beta_1 M_{t-1} + (1 - \beta_1) g_t \\\\
G_t &= \beta_2 G_{t-1} + (1 - \beta_2) g_t^2 \\\\
\end{split}
$$
每次迭代时，$t$ 都加 1 。初始时，$M_0 = 0, G_0 = 0$ , 但由于迭代初期 $M_t$ 和 $G_t$ 的值会比真实的均值和方差小，$\beta_1$ 和 $\beta_2$ 接近 1 时，会产生很大的偏差，需要进行修正：
$$
\begin{split}
\hat M_t &= \frac{M_t}{1-\beta_1^t} \\\\
\hat G_t &= \frac{G_t}{1-\beta_2^t} \\\\
\Delta\theta_t &= -\frac{\alpha}{\sqrt{\hat G_t + \epsilon}} \hat M_t
\end{split}
$$
通常而言，学习率 $\alpha$ 设为 0.001，$\beta_1$ 设为 0.9 ，$\beta_2$ 设为 0.999。

### 3.3 实验

设置 epoch = 20 ， batch_size = 128。对比原始方法、 momentum 算法、 adam 算法在常用超参下的训练情况如下图所示。

其中，momentum 和 adam 算法额外的超参如下：

| **origin**   | $learnint\\_rate = 0.1$                                       |
| ------------ | ------------------------------------------------------------ |
| **momentum** | $learnint\\_rate = 0.001, \space \beta = 0.9$                 |
| **adam**     | $learnint\\_rate = 0.001, \space \beta_1 = 0.9 , \beta_2 = 0.999$ |

<img src='img/exp_optimizer_epoch_acc.png'>

<img src='img/exp_optimizer_step_loss.png'/>

可以看到，在常用超参下，三种算法的收敛速度几乎没有任何差别，模型拟合的准确率也较为相近。

adam 相比前 momentum 和 原始算法，在准确率上虽没有那么稳定，但考虑到其学习率的不同，结合之前原始模型各学习率的收敛速度，可以发现，应用 adam 优化算法后，模型可以在更小的学习率上做到更快地收敛。并且，经过足够多的回合后，adam的准确率是最高的



## 4. 参数初始化

训练神经网络时，权重参数的初始化非常重要。

如果权重过小，那么输入信号通过每一层网络后，其方差就会逐渐减小，最终降低到一个非常低的值，影响训练效果。

而如果权重过大，那么输入信号通过每一层网络后，其方差就会逐渐增大，最终会造成梯度爆炸会消失。

因此，在初始化深度网络时，应尽可能保持每一层输入输出的方差京可能一致，自适应地调整初始化方差。

### 4.1 Xavier 初始化

设第 $l$ 层的一个神经元 $a^{(l)}$，其接受前一层的 $M_{l-1}$ 个神经元的输出 $a_i^{(l-1)}$，$1 \leq i \leq M_{l-1}$，则 $a^{(l)} = f(\Sigma_{i=1}^{M_{l-1}}w_i^{(l)}a_i^{(l-1)})$。

根据教材，为了使得信号在前向和反向传播中都保持不被放大或缩小，可以设置 $var(w_i^{l}) = \frac{2}{M_{l-1} + M_l}$。

在得到参数的理想方差后，就可以通过高斯分布和均匀分布来随机地初始化参数，从而，可以得到 Xavier 初始化公式如下：

均匀分布 ~ $U(-a, a)$：
$$
a = \sqrt{\frac{6}{in\\_feature + out\\_feature}}
$$
正态分布 ~ $N(0, std)$：
$$
std = \sqrt{\frac{2}{in\\_feature + out\\_feature}}
$$


### 4.2 Kaiming/He 初始化

由于 Xavier 在 ReLu 激活函数中表现很差，因此，通常基于 Kaiming 初始化进行优化，其思想为：假定每一层一半的神经元被激活，另一半为0。因此，要保持方差不变，只需在 Xavier 初始化的基础上再除以 2。

因此， Kaiming 初始化公式如下：

均匀分布 ~ $U(-bound, bound)$：
$$
bound = \sqrt{\frac{6}{(1 + a^2) \cdot in\\_feature}}
$$
正态分布 ~ $N(0, std)$：
$$
std = \sqrt{\frac{2}{(1 + a^2) \cdot in\\_feature}}
$$
$a$ 为后一层的激活函数中负的斜率，对于 ReLu而言，$a = 0$。



### 4.3 实验

将原本的 get_torch_initilization 重新实现，运行 `python numpy_mnist.py` 查看 epoch = 3，learning rate = 0.1 时的结果。因为只是用 numpy 复现了 pytorch 的版本，所以和之前使用 pytorch 时基本一致。

<img src='img/kaiming.png'/>

```
[0] Accuracy: 0.9566
[1] Accuracy: 0.9621
[2] Accuracy: 0.9689
```



## 5. 参考文献

[1] [Understanding Xavier Initialization In Deep Neural Networks](https://prateekvjoshi.com/2016/03/29/understanding-xavier-initialization-in-deep-neural-networks/)

[2] [pytorch系列 -- 9 pytorch nn.init 中实现的初始化函数 uniform, normal, const, Xavier, He initialization](https://blog.csdn.net/dss_dssssd/article/details/83959474)

[3] ["深度学习中优化方法——momentum、Nesterov Momentum、AdaGrad、Adadelta、RMSprop、Adam"](https://blog.csdn.net/u012328159/article/details/80311892)

[4] [《神经网络与深度学习》第7章](https://nndl.github.io/nndl-book.pdf) 
