# 课程报告

18307130104

这是 prml 的 assignment-2 课程报告，我的代码可以查看 numpy_fnn.py 中 code 1 ~ code 7 部分，以及 util.py 中 mini_batch 函数 numpy == True 的部分。

在 assignment-2 中，完成了 numpy_fnn.py 中各种算子的反向传播，以及一个简单的前馈神经网络构建（包括正向传播和反向传播）。修改了 mini_batch，在 numpy == True 的情况下，不使用 torch 中的 dataloader 函数完成测试集的打乱和分批。

## 模型实现

为了区别矩阵乘法（np.matmul）和矩阵元素逐一做乘法（\*），下面用$\times$表示矩阵乘法，\*表示元素逐一相乘。

### Matmul 算子的反向传播

Matmul 算子输入一个 X 和权重 W，输出 $$[Y] = [X] \times [W]$$

对于 Y 中的元素 $$Y_{ij}$$ 有$$Y_{ij}=\sum_{k}X_{ik} * W_{kj}$$

在计算 grad_x 的时候，已知 grad_y，根据链式法则，可以得到 $gradx_{ij}=\sum_{k}\frac{\partial Y_{ik}}{\partial X_{ij}} * grady_{ik}$

由 $Y_{ij}$的计算公式可以得到，$\frac{\partial Y_{ik}}{\partial X_{ij}}=W_{jk}$

故 $gradx_{ij}=\sum_k W_{jk} *grady_{ik}$

所以 $[gradx] = [grady] \times [W^T]$

同理，可以得到$[gradW]=[x^T]\times [grady]$

经过验证，矩阵的大小符合矩阵乘法规则。

### Relu 算子的反向传播

relu 函数的计算规则如下：

$relu(x) = \begin{cases}0 & x < 0 \\\\ x & otherwise \end{cases}$

求导可以得到

$relu^{'}(x) = \begin{cases}0 & x < 0 \\\\ 1 & otherwise \end{cases}$

故

$[relugrad]=[grady]* [relu^{'}]$

### Log 算子的反向传播

$log(x) = \ln x$

可以得到

$log^{'}(x)=\frac 1 x$

故

$[loggrad]=[grady]* [log^{'}]$

### softmax 算子的反向传播

$softmax(x_i) = \frac {e^{x_i}}{\sum_j e^{x_j}}$

在实现过程中，因为每一行代表一个测试数据点，所以以每一行为整体对每个元素进行 softmax 操作，从而达成对每个测试数据点进行分类的目的。

采用 softmax 算子和交叉熵损失函数可以让损失函数的形式比较简单，但是遗憾的是实现的神经网络要求将两个算子的反向传播操作分开，因此没有办法投机取巧，只能分步进行计算。

为了表达方便，不妨令 $a_i = softmax(x_i)$

下面考虑$a_i$对$x_j$的反向传播。

$a_i = \frac{e^{x_i}}{\sum_k e^{x_k}}$

$\frac {\partial a_i}{\partial x_j}=\frac{\partial}{\partial x_j}(\frac{e^{x_i}}{\sum_k e^{x_k}})$

接下来根据 i 和 j 是否相等分情况进行讨论。

若 i == j，则 $\frac{\partial}{\partial x_j}(\frac{e^{x_i}}{\sum_k e^{x_k}})=\frac{e^{x_i}(\sum_j e^{x_j})-e^{z_i}e^{z_i}}{(\sum_k e^{x_k})^2}=a_i(1-a_i)$

若 i != j，则$\frac{\partial}{\partial x_j}(\frac{e^{x_i}}{\sum_k e^{x_k}})=-\frac{e^{x_i}e^{x_j}}{(\sum_k e^{x_k})^2}=-a_ia_j$

结合 grady，可以得到

$gradx_{ij}=\sum_k \frac{\partial}{\partial x_j}(\frac{e^{x_k}}{\sum_w e^{x_w}}) grady_{ik}$

由于这个梯度的计算需要进行分类讨论，我没有想到可以直接用 numpy 中函数进行计算的方法，所以首先计算出一个 list 再转换成 ndarray 进行返回。

### 模型正向传播

模型每一层的输出作为下一层的输入，最后得到的是经过 Log 计算的 softmax 结果，这样就能很方便的进行交叉熵损失函数的计算。同时经过“模型反向传播”中的分析可以知道，这样设计使反向传播时的输入也非常简便。

### 模型反向传播

模型进行反向传播的时候会输入一个每行为一个独热向量的矩阵，表示每个数据集的类别，初始代码中会将矩阵中所有元素都除以矩阵的大小，但是经过的尝试，需要将所有元素除以训练数据的组数才能保证结果正确。~~同时，虽然通过了测试，但是 softmax 层的输出也和 torch 中的结果有不同，而后面层的输出是正确的。我认定我理解的 softmax 层和 torch 实现的 softmax 层有一定区别。~~

在更改了测试代码之后，输出和 torch 层比较接近，可以认定是正确的。

接下来推导反向传播时 Log 层的输入。

交叉熵损失函数的形式为

$Loss = -\sum_k t_k*\ln a_k$

其中 $t_k$表示是否属于第 k 个类别，$a_k$为 softmax 层的输出，Log 层的输出为$\ln a_k$，则$\frac{\partial Loss}{\partial \ln a_k}=-t_k$

因此，将输入到反向传播的矩阵 T 取反作为 Log 层的反向传播输入，然后将结果作为前一层的输入逐一反向传播。

## 模型训练

随着训练轮数增长，训练的正确率如下

learning_rate = 0.1    mini_batch = 128

> [0] Accuracy: 0.9403<br>
> [1] Accuracy: 0.9641<br>
> [2] Accuracy: 0.9716<br>
> [3] Accuracy: 0.9751<br>
> [4] Accuracy: 0.9772<br>
> [5] Accuracy: 0.9782<br>
> [6] Accuracy: 0.9745<br>
> [7] Accuracy: 0.9807<br>
> [8] Accuracy: 0.9790<br>
> [9] Accuracy: 0.9811

损失随训练轮数变化如下图所示

<img src="./img/result-1.png" alt="loss" style="zoom:50%;" />

可以看到，正确率随着训练稳步上升，在 6 轮之后，数字基本稳定，仅仅有略微的上下波动。

learning_rate = 0.1    mini_batch = 32

> [0] Accuracy: 0.9646<br>
> [1] Accuracy: 0.9726<br>
> [2] Accuracy: 0.9768<br>
> [3] Accuracy: 0.9788<br>
> [4] Accuracy: 0.9792<br>
> [5] Accuracy: 0.9770<br>
> [6] Accuracy: 0.9820<br>
> [7] Accuracy: 0.9808<br>
> [8] Accuracy: 0.9822<br>
> [9] Accuracy: 0.9835

<img src="./img/result-2.png" alt="loss" style="zoom:50%;" />

可以看到，由于 mini_batch 从 128 变成 32，损失随着轮数的变化会有比较大的起伏。

learning_rate = 0.2    mini_batch = 128

> [0] Accuracy: 0.9295<br>
> [1] Accuracy: 0.9688<br>
> [2] Accuracy: 0.9753<br>
> [3] Accuracy: 0.9734<br>
> [4] Accuracy: 0.9793<br>
> [5] Accuracy: 0.9777<br>
> [6] Accuracy: 0.9792<br>
> [7] Accuracy: 0.9807<br>
> [8] Accuracy: 0.9821<br>
> [9] Accuracy: 0.9815

<img src="./img/result-3.png" alt="loss" style="zoom:50%;" />

虽然调高了学习率，但是损失并没有因此产生比较大的起伏，仍然表现出非常好的效果。

learning_rate = 0.05 mini_batch = 128

> [0] Accuracy: 0.9310<br>
> [1] Accuracy: 0.9504<br>
> [2] Accuracy: 0.9601<br>
> [3] Accuracy: 0.9661<br>
> [4] Accuracy: 0.9691<br>
> [5] Accuracy: 0.9728<br>
> [6] Accuracy: 0.9749<br>
> [7] Accuracy: 0.9761<br>
> [8] Accuracy: 0.9768<br>
> [9] Accuracy: 0.9752

<img src="./img/result-5.png" alt="loss" style="zoom:50%;" />

降低了学习率之后，可以看到正确率的增长比较缓慢，但是经过几轮训练之后的结果和高学习率的时候差不多。

综合来看，影响最终正确率的主要还是模型本身的学习能力，一定范围内修改学习率和 mini_batch 对结果的影响不大。采用 mini_batch 的方式训练有助于降低训练过程中损失的波动。