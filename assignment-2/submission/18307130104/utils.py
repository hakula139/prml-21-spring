import torch
import numpy as np
from matplotlib import pyplot as plt

def plot_curve(data):
    plt.plot(range(len(data)), data, color='blue')
    plt.legend(['loss_value'], loc='upper right')
    plt.xlabel('step')
    plt.ylabel('value')
    plt.xlim(-100,5000)
    plt.savefig('./img/result.png')
    plt.close()
    plt.show()


def download_mnist():
    from torchvision import datasets, transforms
    
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.1307,), std=(0.3081,))
    ])
    
    train_dataset = datasets.MNIST(root="./data/", transform=transform, train=True, download=True)
    test_dataset = datasets.MNIST(root="./data/", transform=transform, train=False, download=True)
    
    return train_dataset, test_dataset


def one_hot(y, numpy=True):
    if numpy:
        y_ = np.zeros((y.shape[0], 10))
        y_[np.arange(y.shape[0], dtype=np.int32), y] = 1
        return y_
    else:
        y_ = torch.zeros((y.shape[0], 10))
        y_[torch.arange(y.shape[0], dtype=torch.long), y] = 1
    return y_


def batch(dataset, numpy=True):
    data = []
    label = []
    for each in dataset:
        data.append(each[0])
        label.append(each[1])
    data = torch.stack(data)
    label = torch.LongTensor(label)
    if numpy:
        return [(data.numpy(), label.numpy())]
    else:
        return [(data, label)]


def mini_batch(dataset, batch_size=128, numpy=False):
    if numpy:
        import random
        datas = [(each[0].numpy(), each[1]) for each in dataset]
        random.shuffle(datas)
        datat = [each[0] for each in datas]
        labelt = [each[1] for each in datas]
        data = [np.array(datat[i: i + batch_size]) for i in range(0, len(datat), batch_size)]
        label = [np.array(labelt[i: i + batch_size]) for i in range(0, len(datat), batch_size)]
        return zip(data, label)
    else:
        return torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True)


def get_torch_initialization(numpy=True):
    fc1 = torch.nn.Linear(28 * 28, 256)
    fc2 = torch.nn.Linear(256, 64)
    fc3 = torch.nn.Linear(64, 10)
    
    if numpy:
        W1 = fc1.weight.T.detach().clone().numpy()
        W2 = fc2.weight.T.detach().clone().numpy()
        W3 = fc3.weight.T.detach().clone().numpy()
    else:
        W1 = fc1.weight.T.detach().clone().data
        W2 = fc2.weight.T.detach().clone().data
        W3 = fc3.weight.T.detach().clone().data
    
    return W1, W2, W3
