# Assignment 2. 前馈神经网络

- **姓名：高庆麾**
- **学号：19307130062**



## 第一部分：梯度计算公式推导

### Matmul

考虑 $Y = XW$，其中 $Y \in \mathbb R^{n\times d\_2},\ X \in \mathbb R^{n \times d\_1},\ W \in \mathbb R^{d\_1 \times d\_2}$ 

设损失函数为 $\mathcal L(\boldsymbol y,\ \boldsymbol {\hat y})$ ，且 $\Delta\_Y$ （即 $Y$ 相对于损失函数的梯度）已知，希望得到 $\Delta\_X,\ \Delta\_W$ 

推导如下：

#### $\Delta\_X$ 的推导

我们考虑 $Y$ 的每一位对 $X$ 贡献的偏导，即 $\frac{\partial Y\_{ij}}{\partial X}$

由于 $Y\_{ij} = \sum\_{k = 1}^{d\_1}X\_{ik}W\_{kj}$ ，$X$ 各位独立，且
$$
\frac{\partial Y\_{ij}}{\partial X} =
\begin{bmatrix}
\frac{\partial Y\_{ij}}{\partial X\_{11}}      & \frac{\partial Y\_{ij}}{\partial X\_{12}} & \cdots & \frac{\partial Y\_{ij}}{\partial X\_{1d\_1}}      \\\\
\frac{\partial Y\_{ij}}{\partial X\_{21}}      & \frac{\partial Y\_{ij}}{\partial X\_{22}} & \cdots & \frac{\partial Y\_{ij}}{\partial X\_{2d\_1}}      \\\\
\vdots & \vdots & \ddots & \vdots \\\\
\frac{\partial Y\_{ij}}{\partial X\_{n1}}      & \frac{\partial Y\_{ij}}{\partial X\_{n2}} & \cdots & \frac{\partial Y\_{ij}}{\partial X\_{nd\_1}}      \\\\
\end{bmatrix}
$$
故 $\left[\frac{\partial Y\_{ij}}{\partial X}\right]\_{ik} = W\_{kj},\ k \in [1,\ d\_1] \cap\mathbb Z$ ，其余项为 $0$ 

由于 $\Delta\_Y$ 已知，即 $\frac{\partial{\mathcal L}}{\partial Y\_{ij}}$ 已知，则有
$$
\frac{\partial{\mathcal L}}{\partial X\_{ij}} = \sum\_{s = 1}^n\sum\_{t = 1}^{d\_2} \frac{\partial{\mathcal L}}{\partial Y\_{st}}\frac{\partial{Y\_{st}}}{\partial X\_{ij}} = \sum\_{s = 1}^n\sum\_{t = 1}^{d\_2} \frac{\partial{\mathcal L}}{\partial Y\_{st}}\left[\frac{\partial{Y\_{st}}}{\partial X}\right]\_{ij} = \sum\_{t = 1}^{d\_2} \frac{\partial{\mathcal L}}{\partial Y\_{it}}\left[\frac{\partial{Y\_{it}}}{\partial X}\right]\_{ij} = \sum\_{t = 1}^{d\_2} \frac{\partial{\mathcal L}}{\partial Y\_{it}}W\_{jt} = \sum\_{t = 1}^{d\_2} \frac{\partial{\mathcal L}}{\partial Y\_{it}}W^T\_{tj}
$$
即
$$
\frac{\partial{\mathcal L}}{\partial X} = \frac{\partial{\mathcal L}}{\partial Y}W^T
$$
可知
$$
\Delta\_X = \Delta\_YW^T
$$

#### $\Delta\_W$ 的推导

其次，对于 $\Delta\_W$ ，我们用类似的方法进行计算，有 $\left[\frac{\partial Y\_{ij}}{\partial W}\right]\_{kj} = X\_{ik},\ k \in [1,\ d\_1] \cap\mathbb Z$ ，其余项为 $0$ ，则有
$$
\frac{\partial{\mathcal L}}{\partial W\_{ij}} = \sum\_{s = 1}^{n}\sum\_{t = 1}^{d\_2} \frac{\partial{\mathcal L}}{\partial Y\_{st}}\left[\frac{\partial{Y\_{st}}}{\partial W}\right]\_{ij} = \sum\_{s = 1}^{n} \frac{\partial{\mathcal L}}{\partial Y\_{sj}}\left[\frac{\partial{Y\_{sj}}}{\partial W}\right]\_{ij} = \sum\_{s = 1}^{n} \frac{\partial{\mathcal L}}{\partial Y\_{sj}}X\_{si} = \sum\_{s = 1}^{n} X\_{is}^T\frac{\partial{\mathcal L}}{\partial Y\_{sj}}
$$
即 
$$
\frac{\partial{\mathcal L}}{\partial W} = X^T\frac{\partial{\mathcal L}}{\partial Y}
$$
可知
$$
\Delta\_W = X^T\Delta\_Y
$$
$\square$ 



### ReLU

考虑 $Y = \mathrm{ReLU}(X)$ ，其中 $Y,\ X \in \mathbb R^{n\times m}$ 

设损失函数为 $\mathcal L(\boldsymbol y,\ \boldsymbol {\hat y})$ ，且 $\Delta\_Y$ （即 $Y$ 相对于损失函数的梯度）已知，希望得到 $\Delta\_X$ 

推导如下：

#### $\Delta\_X$ 的推导

方法类似于上文，只要注意这里的 $\mathrm{ReLU}$ 是一个逐元素函数

考虑 $Y$ 的每一位对 $X$ 贡献的导数，即 $\frac{\mathrm{d}Y\_{ij}}{\mathrm{d} X}$

由于 $Y\_{ij} = \mathrm{ReLU}(X\_{ij})$ ，故 $\left[\frac{\mathrm{d}Y\_{ij}}{\mathrm{d}X}\right]\_{ij} = \mathrm{ReLU}'(X\_{ij})$ ，其余项为 $0$ 

显然 
$$
\mathrm{ReLU}'(x) = \begin{cases}
0, & n < 0 \\\\
1, & n > 0 \\\\
\mathrm{Undefined}, & n = 0
\end{cases}
$$
当然，在 $x = 0$ 处，由于截断误差的存在，可以认为导数为 $0$ 或 $1$ 

则有
$$
\frac{\partial{\mathcal L}}{\partial X\_{ij}} = \sum\_{s = 1}^n\sum\_{t = 1}^{m} \frac{\partial{\mathcal L}}{\partial Y\_{st}}\frac{\mathrm{d}{Y\_{st}}}{\mathrm{d} X\_{ij}} = \frac{\partial{\mathcal L}}{\partial Y\_{ij}}\left[\frac{\partial{Y\_{ij}}}{\partial X}\right]\_{ij} =\frac{\partial{\mathcal L}}{\partial Y\_{ij}}\mathrm{ReLU}'(X\_{ij})
$$
即（此处 $\odot$ 表示矩阵的哈达玛积，即对应位乘积）
$$
\frac{\partial{\mathcal L}}{\partial X} = \frac{\partial{\mathcal L}}{\partial Y}\odot\mathrm{ReLU}'(X)
$$
可知
$$
\Delta\_X = \Delta\_Y\odot\mathrm{ReLU}'(X)
$$
$\square$ 



### Log

考虑 $Y = \mathrm{Log}(X)$ ，其中 $Y,\ X \in \mathbb R^{n\times m}$ 

设损失函数为 $\mathcal L(\boldsymbol y,\ \boldsymbol {\hat y})$ ，且 $\Delta\_Y$ （即 $Y$ 相对于损失函数的梯度）已知，希望得到 $\Delta\_X$ 

推导如下：

#### $\Delta\_X$ 的推导

方法类似于上文，只要注意这里的 $\mathrm{Log}$ 是一个逐元素函数

考虑 $Y$ 的每一位对 $X$ 贡献的导数，即 $\frac{\mathrm{d}Y\_{ij}}{\mathrm{d} X}$

由于 $Y\_{ij} = \mathrm{Log}(X\_{ij})$ ，故 $\left[\frac{\mathrm{d}Y\_{ij}}{\mathrm{d}X}\right]\_{ij} = \mathrm{Log}'(X\_{ij}) = \frac{1}{X\_{ij}}$ ，其余项为 $0$ 

则有
$$
\frac{\partial{\mathcal L}}{\partial X\_{ij}} = \sum\_{s = 1}^n\sum\_{t = 1}^{m} \frac{\partial{\mathcal L}}{\partial Y\_{st}}\frac{\mathrm{d}{Y\_{st}}}{\mathrm{d} X\_{ij}} = \frac{\partial{\mathcal L}}{\partial Y\_{ij}}\left[\frac{\partial{Y\_{ij}}}{\partial X}\right]\_{ij} =\frac{\partial{\mathcal L}}{\partial Y\_{ij}}\frac{1}{X\_{ij}}
$$
即（其中 $\frac{1}{X}$ 表示 $X$ 的每一位取倒数后的结果）
$$
\frac{\partial{\mathcal L}}{\partial X} = \frac{\partial{\mathcal L}}{\partial Y}\odot\frac{1}{X}
$$
可知
$$
\Delta\_X = \Delta\_Y\odot\frac{1}{X}
$$
$\square$ 



### Softmax

考虑 $\boldsymbol y = \mathrm{Softmax}(\boldsymbol x)$ ，其中 $\boldsymbol y,\ \boldsymbol x \in \mathbb R^{1 \times c}$ 

设损失函数为 $\mathcal L(\boldsymbol y,\ \boldsymbol {\hat y})$ ，且 $\Delta\_{\boldsymbol y}$ （即 $Y$ 相对于损失函数的梯度）已知，希望得到 $\boldsymbol y$ 的表达（前向计算）及 $\Delta\_{\boldsymbol x}$ 

推导如下：

#### $\boldsymbol y$ 的推导（前向计算）

根据 $\mathrm{Softmax}$ 的定义，可以得到
$$
\boldsymbol y\_i = \frac{e^{\boldsymbol x\_i}}{\sum\_{j = 1}^ce^{\boldsymbol x\_j}}
$$

#### $\Delta\_{\boldsymbol x}$ 的推导

由于
$$
\boldsymbol y\_i = \frac{e^{\boldsymbol x\_i}}{\sum\_{j = 1}^ce^{\boldsymbol x\_j}}
$$
且
$$
\frac{\partial \boldsymbol y}{\partial\boldsymbol x} =
\begin{bmatrix}
\frac{\partial \boldsymbol y\_1}{\partial \boldsymbol x\_1}      & \frac{\partial \boldsymbol y\_1}{\partial \boldsymbol x\_2} & \cdots & \frac{\partial \boldsymbol y\_1}{\partial \boldsymbol x\_c}      \\\\
\frac{\partial \boldsymbol y\_2}{\partial \boldsymbol x\_1}      & \frac{\partial \boldsymbol y\_2}{\partial \boldsymbol x\_2} & \cdots & \frac{\partial \boldsymbol y\_2}{\partial \boldsymbol x\_c}      \\\\
\vdots & \vdots & \ddots & \vdots \\\\
\frac{\partial \boldsymbol y\_c}{\partial \boldsymbol x\_1}      & \frac{\partial \boldsymbol y\_c}{\partial \boldsymbol x\_2} & \cdots & \frac{\partial \boldsymbol y\_c}{\partial \boldsymbol x\_c}      \\\\
\end{bmatrix}
$$
故当 $i = j$ 时，有
$$
\left[\frac{\partial \boldsymbol y}{\partial\boldsymbol x}\right]\_{ii} = \frac{\partial \boldsymbol y\_i}{\partial \boldsymbol x\_i} = \frac{\partial\left( \frac{e^{\boldsymbol x\_i}}{\sum\_{j = 1}^ce^{\boldsymbol x\_j}}\right)}{\partial \boldsymbol x\_i} = \frac{e^{\boldsymbol x\_i}(\sum\_{j = 1}^ce^{\boldsymbol x\_j}) - e^{\boldsymbol x\_i}e^{\boldsymbol x\_i}}{\left(\sum\_{j = 1}^ce^{\boldsymbol x\_j}\right)^2} = \frac{e^{\boldsymbol x\_i}}{\sum\_{j = 1}^ce^{\boldsymbol x\_j}}\frac{\left(\sum\_{j = 1}^ce^{\boldsymbol x\_j}\right) - e^{\boldsymbol x\_i}}{\sum\_{j = 1}^ce^{\boldsymbol x\_j}} = \boldsymbol y\_i(1 - \boldsymbol y\_i)
$$
当 $i \neq j$ 时，有
$$
\left[\frac{\partial \boldsymbol y}{\partial\boldsymbol x}\right]\_{ij} = \frac{\partial \boldsymbol y\_i}{\partial \boldsymbol x\_j} = \frac{\partial\left( \frac{e^{\boldsymbol x\_i}}{\sum\_{j = 1}^ce^{\boldsymbol x\_j}}\right)}{\partial \boldsymbol x\_j} = \frac{-e^{\boldsymbol x\_i}e^{\boldsymbol x\_j}}{\left(\sum\_{j = 1}^ce^{\boldsymbol x\_j}\right)^2} = -\boldsymbol y\_i\boldsymbol y\_j
$$
则有
$$
\frac{\partial{\mathcal L}}{\partial\boldsymbol x\_{j}} = \sum\_{i = 1}^c\frac{\partial{\mathcal L}}{\partial\boldsymbol y\_{i}}\frac{\partial\boldsymbol y\_i}{\partial \boldsymbol x\_j} = \sum\_{i = 1}^c\frac{\partial{\mathcal L}}{\partial\boldsymbol y\_{i}}\left[\frac{\partial\boldsymbol y}{\partial \boldsymbol x}\right]\_{ij}
$$
即
$$
\frac{\partial{\mathcal L}}{\partial\boldsymbol x} = \frac{\partial{\mathcal L}}{\partial \boldsymbol y}\left[\frac{\partial\boldsymbol y}{\partial \boldsymbol x}\right]
$$
可知（其中 $\left[\frac{\partial\boldsymbol y}{\partial \boldsymbol x}\right]$ 已经在上文中求出）
$$
\Delta\_{\boldsymbol x} = \Delta\_{\boldsymbol y}\left[\frac{\partial\boldsymbol y}{\partial \boldsymbol x}\right]
$$
由于原代码中给出的是一组行向量构成的矩阵，因此，我们可以对每一行分别进行如上操作，且行与行之间互不干扰，由此完成 $\mathrm{Softmax}$ 的反向传播

$\square$ 

*注意此处对应代码的写法，对于 numpy 里的 array A，A[i] 只会被认为具有一个维度（被看做是一列数而非一个行向量），因此如果希望使用其对应的行向量，需要将 A[i] 转换为 matrix 类型，此时就会带上表示行的一维



### FNN

由于整个 FNN 由上面推导过的许多基本层次构成，因此在 FNN 中进行前向计算，只需要类似函数嵌套的方法将模型结构的各个层次结合在一起即可

设 FNN 对应的函数为 $F$ ，则

#### $F$ 的推导（前向计算）

$F(X) = \mathrm{Log}(\mathrm{Softmax}(\mathrm{ReLU}(\mathrm{ReLU}(X\cdot W\_1)\cdot W\_2)\cdot W\_3))$ 

其中 $X \in \mathbb R^{n \times 784},\ W\_1 \in \mathbb R^{784\times 256},\ W\_2 \in \mathbb R^{256\times 64},\ W\_3 \in \mathbb R^{64\times 10},\ F(X) \in \mathbb R^{n \times 10}$ ，$n$ 为数据条数

#### FNN 后向传播的推导

根据代码，我们可以得到模型定义的损失函数为 $\mathcal L(\boldsymbol y,\ \boldsymbol {\hat y}) = -\boldsymbol {\hat y}\boldsymbol y^T$ （$\boldsymbol {\hat y}$ 表示预测向量），而在整个数据集上的定义为

$\mathcal L(Y,\ \hat Y) = -\frac{1}{n}\sum\_{i = 1}^n{\hat Y\_{i:}}Y\_{i:}^T = -\frac{1}{n}\sum\_{i = 1}^n\sum\_{j = 1}^d{\hat Y\_{ij}}Y\_{ij}$

由此我们需要计算 $\Delta\_{\hat Y}$ （即 $\hat Y$ 相对于该损失函数的梯度，$\hat Y  = F(X)$ ）

推导如下：

#### $\Delta\_{\hat Y}$ 的推导

根据上文对 $\mathcal L$ 的定义，我们很容易得到
$$
\frac{\partial\mathcal L}{\partial\hat Y\_{ij}} = -\frac{1}{n}Y\_{ij}
$$
由此即可得到最初进入反向传播过程的梯度矩阵 $\Delta\_{\hat Y}$ ，其它层上的梯度可以通过逐层反向传播得到



### 总结

**这里希望就笔者对梯度和导数的理解，以及根据自己的理解和方法简明地推导反向传播公式的感悟，做一点总结。**

对于后向传播的梯度计算，有许许多多的方法，比如迹运算技术[^1]和向量化技术[^2]即为比较通用的高维求导方法。但简单尝试后可以发现，这些方法运用起来实在过于复杂。实际上，为了正确而又简单地计算梯度，我们只需要明确所谓梯度（或者说导数）究竟有怎样的实际含义。

*下面对梯度和导数的概念可能有一些混用，在本文中这两者表达的含义基本相同。

对于一个非常一般的关系 $A = f(B)$ ，此处 $A,\ B$ 可视为任意维度和大小的张量。如果对线性代数比较熟悉，可以知道，其实这只是对许多多元函数关系的一种简化表达。如果我们拆开来看，若 $a$ 是 $A$ 中某一项，那么 $a$ 实际上就是一个**关于 $B$ 中所有项的多元函数**。

其次，为什么要在神经网络中计算梯度？无非就是希望通过梯度下降来迭代地优化神经网络的效果（而梯度下降的方向也是损失函数值下降最快的方向，且一般而言这个损失函数的结果都是一个标量，否则难以比较大小）。既然如此，我们就需要计算出网络中每个参数的梯度，并通过将每个参数都向梯度下降的方向做一些移动，使得整个网络的损失函数在特定数据集上的表现有所优化。

所以，我们可以认为，每个参数的移动都对损失函数的最终结果有一定的贡献。而链式法则告诉我们，这种贡献的正确计算方法，是考虑该参数到达损失函数的**每一条作用路径**[^3]。反向传播算法 (Back Propagation, BP) 即是利用了这种思想。如果对动态规划 (Dynamic Programming, DP) 算法比较熟悉（比如对运筹学比较熟悉或有程序设计竞赛方面的背景），可以知道 BP 实际是一个**非常显然而简单**的有向无环图 (Directed Acyclic Graph, DAG)  上 做 DP 的算法（当然这也很大程度上归功于链式法则的证明），因为神经网络中的计算图本身一定是一个 DAG，即从各个结点出发，沿着计算边行进，最终必定会到达损失函数。

这样，我们在计算 $B$ 到 $\mathcal L$ 的梯度时，不再需要枚举所有作用路径，计算并累加所有的贡献，而只是需要计算从 $B$ 到以它为自变量（的一部分）的关系的象 $A$ 中所有可能的作用路径，再和先前计算好的从 $A$ 到 $\mathcal L$ 的梯度做一个“路径拼接”，实际也就是矩阵（或张量）的乘法，即可得到 $B$ 所有的贡献。关于 $A,\ B$ 的顺序，由于计算图是一个 DAG ，因此可以通过拓扑排序 (Topological Sort) 确定二者（乃至全部参数）之间合法的先后关系。

回到上文的一般关系 $A = f(B)$ 中，从 $B$ 到 $A$ 的所有合法路径是怎样的？这就回到了一开始从 $A$ 中抽出的一项 $a$ （它也代表一个关于 $B$ 中所有项的多元函数） ，所谓所有的合法路径，实际也就是要考虑所有这样的 $A$ 中的项（或多元函数）对于 $B$ 中每一项的梯度，然后 $B$ 中每一项的贡献，也就是以 $\mathcal L$ 到 $A$ 中每一项的梯度作为权，乘这一 $A$ 中的项对 $B$ 中该项刚刚计算出的梯度（或称偏导）的和，这也就是先前提到的**“路径拼接”**（也是一种加权和，如此理解的意义也是非常显然的）。

这样，对于日常遇到的简单情形，我们其实并不需要套用一些非常通用而繁琐的方法，只需要如上文一般，对 $A$ 中每一项求出其对 $B$ 中每一项的偏导，由此将问题转化为一个多元函数（结果是标量）对其中每个自变量的偏导问题，这显然是非常简单的（甚至不需要大学的知识）。而关于“简单情形”的定义，其实也就是指这种多元函数的形式比较一致，这样我们只需要对其中几个多元函数求出偏导，就能得到对全部情况的理解。最后模仿“路径拼接”或链式法则，将表达简化为矩阵乘法的形式，就可以非常容易地得到梯度。本文对相关常见的神经网络层反向传播的推导，全部基于这种认识和由此而来的方法，在笔者个人看来，是足够简明和容易理解的。





[^1]: 来源于该式 $\mathrm{d}f = \mathrm{tr}\left(\frac{\partial f}{\partial X}^T \mathrm{d}X\right)$，用于标量对矩阵的求导。从需要求导的标量出发，套上迹运算，再结合一些迹内运算的恒等式推导得到类似形式，则迹运算内 $\mathrm{d}X$ 左侧部分的转置即为所求导数
[^2]: 来源于该式 $\mathrm{vec}(\mathrm{d}F) = \frac{\partial F}{\partial X}^T \mathrm{vec}(\mathrm{d}X)$，用于矩阵对矩阵的求导。类似地，从需要求导的矩阵出发，套上向量化运算，再结合一些向量化内运算的恒等式推导得到类似形式，则 $\mathrm{vec}(\mathrm{d}X)$ 左侧部分的转置即为所求导数
[^3]: 对于这一点，可以举例考虑函数 $f = (x,\ y,\ z),\ x = g(u),\ y = h(u),\ z = t(u)$ 。如果可导相关的条件上没有任何障碍，那么想要求出 $\frac{\partial f}{\partial u}$ ，我们就必须计算 $\frac{\partial f}{\partial x}\frac{\partial x}{\partial u} + \frac{\partial f}{\partial y}\frac{\partial y}{\partial u} + \frac{\partial f}{\partial z}\frac{\partial z}{\partial u}$ ，也就是考虑 $u$ 到 $f$ 的每一条作用路径。



<!---- 以下是未对 `numpy_fnn.py` 作修改之前的结果 ------>

## 第二部分：模型的训练和测试（先前版本）

在从损失函数进行梯度回传时，发现并没有为损失函数提供 `backward` 接口，因此就去看了一下损失函数的具体形式，然后进行了一下推导，发现只需要在原来取平均的基础上加一个负号就可以，然后就通过了所有的测试。但不知道为什么初始准确率不是很高，只有 $80\%$ 多。多训练几个 epoch 后才提升得比较好。

### 调整学习率

#### 保守尝试

根据上文的推导结果，在 `numpy_fnn.py` 中填写好有关代码，然后运行 `numpy_mnist.py` 进行训练，并尝试不同的学习率，得到结果如下：

（实际顺序是先测试了原配置 $\alpha = 0.1$，后来发现损失函数后期波动较大，以为是学习率过大导致的不稳定，就尝试了更小的学习率 $\alpha = 0.05$ ，结果发现效果明显不如从前，就开始逐渐增大学习率进行试验，观察效果。最后决定将数据和损失函数图象放在一起，这样对比更加明显，且节约篇幅）

| Epoch | Accuracy ($\alpha = 0.05$) | Accuracy ($\alpha = 0.1$) | Accuracy $(\alpha = 0.15$) | Accuracy $(\alpha = 0.3$) |
| :---: | :------------------------: | :-----------------------: | :------------------------: | :-----------------------: |
|  $0$  |         $80.18\%$          |         $87.50\%$         |         $89.20\%$          |         $91.06\%$         |
|  $1$  |         $87.25\%$          |         $90.46\%$         |         $91.22\%$          |         $93.54\%$         |
|  $2$  |         $89.23\%$          |         $91.65\%$         |         $92.46\%$          |         $94.64\%$         |
|  $3$  |         $90.40\%$          |         $92.21\%$         |         $93.19\%$          |         $95.42\%$         |
|  $4$  |         $91.03\%$          |         $92.98\%$         |         $93.67\%$          |         $96.02\%$         |
|  $5$  |         $91.61\%$          |         $93.23\%$         |         $94.45\%$          |         $96.38\%$         |

<img src="img/2.png" alt="2" style="zoom:3.5%;" /><img src="img/8.png" alt="8" style="zoom:3.5%;" /><img src="img/3.png" alt="3" style="zoom:3.5%;" /><img src="img/4.png" alt="4" style="zoom:3.5%;" />

（上四图从左到右分别是 $\alpha = 0.05,\ 0.1,\ 0.15,\ 0.3$ 的情况，可以看到损失函数下降的速度明显不同）

设置这几个 $\alpha$ 也是为了比较在 $\mathrm{epoch}\times \alpha$ 相同时的模型训练效果，结果如下：

|              condition               | accuracy  |
| :----------------------------------: | :-------: |
| $\alpha = 0.05,\ \mathrm{epoch} = 6$ | $91.61\%$ |
| $\alpha = 0.1,\ \mathrm{epoch} = 3$  | $91.65\%$ |
| $\alpha = 0.15,\ \mathrm{epoch} = 2$ | $91.22\%$ |
| $\alpha = 0.3,\ \mathrm{epoch} = 1$  | $91.06\%$ |

可以发现，在保持模型训练效果大体不变的情况下，可以**适当增加**学习率，减少训练的 $\mathrm{epoch}$ 次数，从而提高训练效率

那么，既然提到了适当，那何时是不太适当的呢？这将在下一节中进行探索。

#### 过大的 $\alpha$

显然一切都不能太极端，我们也不能一昧地提升学习率而不计后果。过大的学习率容易造成模型训练的不稳定，以及数值计算上的严重问题，于是希望在这一节探索怎样的 $\alpha$ 是过大的

| Epoch | Accuracy ($\alpha = 1.0$) | Accuracy ($\alpha = 5.0$) | Accuracy ($\alpha = 8.0$) |
| :---: | :-----------------------: | :-----------------------: | :-----------------------: |
|  $0$  |         $94.64\%$         |         $96.36\%$         |         $9.80\%$          |
|  $1$  |         $96.28\%$         |         $96.99\%$         |         $9.80\%$          |
|  $2$  |         $97.08\%$         |         $97.14\%$         |         $9.80\%$          |
|  $3$  |         $97.39\%$         |         $97.73\%$         |         $9.80\%$          |
|  $4$  |         $97.57\%$         |         $97.52\%$         |         $9.80\%$          |
|  $5$  |         $97.82\%$         |         $97.84\%$         |         $9.80\%$          |

<img src="img/5.png" alt="5" style="zoom: 3.5%;" /><img src="img/6.png" alt="5" style="zoom: 3.5%;" /><img src="img/7.png" alt="5" style="zoom: 3.5%;" />

（上三图分别对应 $\alpha = 1.0,\ 5.0,\ 8.0$ 情况）

可以看到 $\alpha = 1.0$ 的效果还非常好，$\alpha = 5.0$ 时损失函数已经出现相对明显大幅度的波动，而 $\alpha = 8.0$ 时在训练过程中出现了数值问题，模型优化失效

#### 总结

- 可以发现，在 $\alpha$ 一定的范围内（合理范围内），epoch $\times\ \alpha$ 相同时，训练模型的精度也大体相近。总体看来在这个模型中，$0.1$ 的学习率确实还不太合适，迭代速度太慢
- 增大 $\alpha$ 时，损失函数下降的速度也有非常明显的提升，可以看到损失函数近于直线下降的部分，随着 $\alpha$ 增大其倾斜程度也有非常明显的增大
- 合理增大 $\alpha$ 对模型精度的提升非常明显，如 $\alpha = 1.0$ 在第 $6$ 个 epoch 时的精度甚至达到了近 $98\%$（但和 torch 下的模型训练效率相比，差距仍然很明显）



<!---- 以下是 `numpy_fnn.py` 修改后的结果 ------>

## 第二部分：模型的训练和测试（修改后版本）

然后听说对 `numpy_fnn.py` 作了不少修改，然后就 pull 了一下，发现给损失函数加上了 `backward`，并在模型 `backward` 时传入了，这样就不需要再手动处理什么，直接从 `ReLU` 开始反向传播就好。然而，这次在 MNIST 上的测试准确率变得很好，初始就是 $90\%$  以上，训练几个 epoch 以后也显著地变得更好。所以这一部分就是把之前的结果重新做一遍，然后更新一下实验数据。

### 调整学习率

#### 保守尝试

根据上文的推导结果，在 `numpy_fnn.py` 中填写好有关代码，然后运行 `numpy_mnist.py` 进行训练，并尝试不同的学习率，得到结果如下：

| Epoch | Accuracy ($\alpha = 0.05$) | Accuracy ($\alpha = 0.1$) | Accuracy $(\alpha = 0.15$) | Accuracy $(\alpha = 0.3$) |
| :---: | :------------------------: | :-----------------------: | :------------------------: | :-----------------------: |
|  $0$  |         $92.51\%$          |         $94.56\%$         |         $95.47\%$          |         $96.09\%$         |
|  $1$  |         $94.96\%$          |         $96.12\%$         |         $95.89\%$          |         $95.97\%$         |
|  $2$  |         $96.15\%$          |         $96.96\%$         |         $97.27\%$          |         $96.94\%$         |
|  $3$  |         $96.28\%$          |         $97.28\%$         |         $97.52\%$          |         $97.78\%$         |
|  $4$  |         $96.93\%$          |         $97.23\%$         |         $97.54\%$          |         $98.05\%$         |
|  $5$  |         $97.14\%$          |         $97.55\%$         |         $98.08\%$          |         $97.91\%$         |

<img src="img/2.2.png" alt="2" style="zoom:3.5%;" /><img src="img/2.8.png" alt="8" style="zoom:3.5%;" /><img src="img/2.3.png" alt="3" style="zoom:3.5%;" /><img src="img/2.4.png" alt="4" style="zoom:3.5%;" />

（上四图从左到右分别是 $\alpha = 0.05,\ 0.1,\ 0.15,\ 0.3$ 的情况，可以看到损失函数下降的速度明显不同）

设置这几个 $\alpha$ 也是为了比较在 $\mathrm{epoch}\times \alpha$ 相同时的模型训练效果，结果如下：

|              condition               | accuracy  |
| :----------------------------------: | :-------: |
| $\alpha = 0.05,\ \mathrm{epoch} = 6$ | $97.1\%$  |
| $\alpha = 0.1,\ \mathrm{epoch} = 3$  | $96.96\%$ |
| $\alpha = 0.15,\ \mathrm{epoch} = 2$ | $95.89\%$ |
| $\alpha = 0.3,\ \mathrm{epoch} = 1$  | $96.09\%$ |

#### 过大的 $\alpha$

上一节中，我们就看到，在采用较大学习率时，模型学习效果会出现明显的波动，且最终学习效果会有所下降。那么当采用更大学习率时，会出现什么结果呢？

| Epoch | Accuracy ($\alpha = 1.0$) |
| :---: | :-----------------------: |
|  $0$  |         $9.80\%$          |
|  $1$  |         $9.80\%$          |
|  $2$  |         $9.80\%$          |
|  $3$  |         $9.80\%$          |
|  $4$  |         $9.80\%$          |
|  $5$  |         $9.80\%$          |

<img src="img/2.5.png" alt="5" style="zoom: 3.5%;" />

（上三图分别对应 $\alpha = 1.0,\ 5.0,\ 8.0$ 情况）

和先前不同， $\alpha = 1.0$ 时模型就出现了数值问题，导致优化失效



## 第三部分：自定义 mini_batch

用 numpy 手写 mini_batch 函数，其实原理很简单，算法大概分为如下步骤（写在代码中）：

```python
def mini_batch(dataset, batch_size = 128, numpy = False):
	if batch_size <= 0 or not isinstance(batch_size, int): 
		return None
    # 1. 判断传入的 batch_size 是否合法，需要为正整数，不合法返回空
		
	data, label = batch(dataset)[0]
    # 2. 用 batch 方法将 torchvision 下的 MNIST 数据集转换为 numpy 的 array
    
	datanum = len(data)
	idx = np.arange(datanum)
	np.random.shuffle(idx) 
	data, label = data[idx], label[idx]
    # 3. 对 data 和 label 进行 random shuffle，具体来说，可以先对一个指示下标的数组做 random shuffle，然后用这个下标数组配合 slice 机制对 data 和 label 进行对应的 random shuffle，从而防止 data 和 label 错误匹配
	
	batchnum = (datanum - 1) // batch_size + 1 # datanum 对 batch_size 下取整
	batches = []
    # 4. 计算 batch 数量，初始化 batches 列表
    
	for i in range(batchnum): 
		batches.append((data[i * batch_size: min(datanum, (i + 1) * batch_size)], label[i * batch_size: min(datanum, (i + 1) * batch_size)]))
    # 5. 通过 slice 机制选出第 i 个 batch 对应的 data 和 label 子集，放入 batches 列表中
	
	return batches
```



## 第四部分：额外探究

### 其他基于梯度的优化方法对比实验

在代码中 ( numpy_fnn.py 和 numpy_mnist.py ) 实现了十种优化方法，有

- Momentum, 动量法
- Nesterov Accelerated Gradient, NAG, Nesterov 加速梯度 或称 Nesterov 动量法
- Adaptive Moment Estimation Algorithm, Adam 算法
- Inverse Time Decay, 逆时衰减
- Exponential Decay, 指数衰减
- Natural Exponential Decay, 自然指数衰减
- Cosine Decay, 余弦衰减
- Adaptive Gradient Algorithm, AdaGrad 算法
- RMSprop 算法
- AdaDelta 算法

包括无优化的版本，共有十一种不同的对比实验

#### $\mathrm{epoch} = 3,\ \alpha = 0.1$

这里设定 $\mathrm{epoch} = 3,\ \alpha = 0.1$ ，当然，对于调整学习率的算法，$\alpha$ 是不固定的，提供的只是一个初始值

| Epoch |   None    | Momentum  | Nesterov  |   Adam    |
| :---: | :-------: | :-------: | :-------: | :-------: |
|  $0$  | $92.15\%$ | $96.72\%$ | $96.42\%$ | $95.43\%$ |
|  $1$  | $96.21\%$ | $97.00\%$ | $96.88\%$ | $96.87\%$ |
|  $2$  | $96.81\%$ | $97.54\%$ | $97.09\%$ | $97.55\%$ |

| Epoch | Inverse Time Decay | Exponential Decay | Natural Exponential Decay | Cosine Decay |
| :---: | :----------------: | :---------------: | :-----------------------: | :----------: |
|  $0$  |     $86.71\%$      |     $81.10\%$     |         $81.05\%$         |  $94.17\%$   |
|  $1$  |     $87.93\%$      |     $81.10\%$     |         $81.05\%$         |  $95.91\%$   |
|  $2$  |     $88.49\%$      |     $81.10\%$     |         $81.05\%$         |  $96.08\%$   |

| Epoch |  AdaGrad  |  RMSprop  | AdaDelta  |
| :---: | :-------: | :-------: | :-------: |
|  $0$  | $95.04\%$ | $95.47\%$ | $76.42\%$ |
|  $1$  | $96.28\%$ | $96.45\%$ | $86.86\%$ |
|  $2$  | $97.24\%$ | $97.43\%$ | $89.22\%$ |

<img src="img/5.2.png" alt="5" />

*其实这时候忘了把 RMSprop 加进去了，但是对比也是很丰富的...



#### $\mathrm{epoch} = 3,\ \alpha = 0.05$

为了更好地突出各个算法的不同，将学习率 $\alpha$ 降低为 $0.05$

| Epoch |   None    | Momentum  | Nesterov  |   Adam    |
| :---: | :-------: | :-------: | :-------: | :-------: |
|  $0$  | $92.47\%$ | $96.20\%$ | $96.65\%$ | $93.67\%$ |
|  $1$  | $94.43\%$ | $97.38\%$ | $97.13\%$ | $96.11\%$ |
|  $2$  | $95.67\%$ | $97.70\%$ | $97.70\%$ | $96.69\%$ |

| Epoch | Inverse Time Decay | Exponential Decay | Natural Exponential Decay | Cosine Decay |
| :---: | :----------------: | :---------------: | :-----------------------: | :----------: |
|  $0$  |     $77.18\%$      |     $67.31\%$     |         $64.09\%$         |  $92.47\%$   |
|  $1$  |     $80.42\%$      |     $67.31\%$     |         $64.09\%$         |  $94.07\%$   |
|  $2$  |     $81.90\%$      |     $67.31\%$     |         $64.09\%$         |  $94.35\%$   |

| Epoch |  AdaGrad  |  RMSprop  | AdaDelta  |
| :---: | :-------: | :-------: | :-------: |
|  $0$  | $93.29\%$ | $93.74\%$ | $75.09\%$ |
|  $1$  | $95.47\%$ | $95.17\%$ | $87.09\%$ |
|  $2$  | $96.49\%$ | $96.71\%$ | $89.76\%$ |

<img src="img/5.3.png" alt="5" />

*这时候仍然忘了把 RMSprop 加进去了...



#### $\mathrm{epoch} = 3,\ \alpha = 0.01$

为了更好地突出各个算法的不同，将学习率 $\alpha$ 进一步降低为 $0.01$，同时加上 RMSprop 算法

| Epoch |   None    | Momentum  | Nesterov  |   Adam    |
| :---: | :-------: | :-------: | :-------: | :-------: |
|  $0$  | $87.89\%$ | $94.37\%$ | $94.51\%$ | $89.81\%$ |
|  $1$  | $90.47\%$ | $96.32\%$ | $95.82\%$ | $91.76\%$ |
|  $2$  | $91.98\%$ | $97.12\%$ | $96.85\%$ | $92.96\%$ |

| Epoch | Inverse Time Decay | Exponential Decay | Natural Exponential Decay | Cosine Decay |
| :---: | :----------------: | :---------------: | :-----------------------: | :----------: |
|  $0$  |     $36.83\%$      |     $21.94\%$     |         $29.31\%$         |  $87.22\%$   |
|  $1$  |     $45.39\%$      |     $21.94\%$     |         $29.31\%$         |  $89.67\%$   |
|  $2$  |     $50.02\%$      |     $21.94\%$     |         $29.31\%$         |  $89.85\%$   |

| Epoch |  AdaGrad  |  RMSprop  | AdaDelta  |
| :---: | :-------: | :-------: | :-------: |
|  $0$  | $89.44\%$ | $89.56\%$ | $77.58\%$ |
|  $1$  | $91.83\%$ | $91.85\%$ | $86.84\%$ |
|  $2$  | $92.76\%$ | $92.90\%$ | $89.26\%$ |

<img src="img/5.4.png" alt="5" />

#### 总结

- 可以发现 Momentum 和 Nesterov 效果一直不错，且表现接近，这可能是因为本作业的模型比较简单，而 Momentum 和 Nesterov 总体来看做的事情比较相似，所以没有体现出 Nesterov 对梯度更新的修正。Adam 在 $\alpha$ 较大时表现也很好
- 对于以某种方式衰减 $\alpha$ 的优化，初始 $\alpha$ 必须足够大才能体现出较好的效果，毕竟 $\alpha$ 太小的话，衰减到最后已经不足以对参数产生实质性的更新从而导致停滞（如 Exponential Decay 和 Natural Exponential Decay）
- AdaGrad 和 RMSprop 表现比较接近，这可能也是因为本作业的模型比较简单，所以没有体现出 RMSprop 中指数衰减移动平均的优势，且这两者也会受到学习率减小的影响



### 权重初始化

#### torch.nn.Linear 的初始化方法

首先是原代码中 `torch.nn.Linear` 的初始化方法调查，这个可以去查一下文档，然后就可以发现

<img src="img/Pytorch.png" alt="5" />

可以看到，这里 $k$ 表示上一层神经元个数（in_features)  的倒数，而 old_features 即为当前层神经元个数，然后用 $\mathcal U(-\sqrt{k},\ \sqrt{k})$ 的均匀分布进行初始化，这和 He 初始化有一些相似，但是差了常数



#### 其他初始化方法的探索

测试了几种方法，有

- 均匀分布采样的 Xavier 初始化
- 均匀分布采样的 Hekaiming 初始化
- 高斯分布采样的 Xavier 初始化
- 高斯分布采样的 Hekaiming 初始化

#### $\mathrm{epoch} = 3,\ \alpha = 0.1$

<img src="img/6.3.png" alt="5" />

| Epoch |   Torch   | Xavier_Uniform | HeKaiming_Uniform | Xavier_Normal | HeKaiming_Normal |
| :---: | :-------: | :------------: | :---------------: | :-----------: | :--------------: |
|  $0$  | $94.38\%$ |   $95.59\%$    |     $95.66\%$     |   $95.55\%$   |    $95.58\%$     |
|  $1$  | $95.47\%$ |   $96.19\%$    |     $96.72\%$     |   $96.68\%$   |    $96.83\%$     |
|  $2$  | $96.91\%$ |   $97.20\%$    |     $96.81\%$     |   $97.12\%$   |    $96.97\%$     |



#### 总结

- 可以看到 Xavier 和 He 初始化相对于 Pytorch 默认的方法来说还是有一定优势的，但实际测试中 He 初始化有时会不够稳定，且参数设定上存在一些困难