import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, mini_batch, get_torch_initialization, plot_curve, one_hot

def mini_batch(dataset, batch_size = 128, numpy = False):
	if batch_size <= 0 or not isinstance(batch_size, int): 
		return None
    # 1. 判断传入的 batch_size 是否合法，需要为正整数，不合法返回空
		
	data, label = batch(dataset)[0]
    # 2. 用 batch 方法将 torchvision 下的 MNIST 数据集转换为 numpy 的 array
    
	datanum = len(data)
	idx = np.arange(datanum)
	np.random.shuffle(idx) 
	data, label = data[idx], label[idx]
    # 3. 对 data 和 label 进行 random shuffle，具体来说，可以先对一个指示下标的数组做 random shuffle，然后用这个下标数组配合 slice 机制对 data 和 label 进行对应的 random shuffle，从而防止 data 和 label 错误匹配
	
	batchnum = (datanum - 1) // batch_size + 1 # datanum 对 batch_size 下取整
	batches = []
    # 4. 计算 batch 数量，初始化 batches 列表
    
	for i in range(batchnum): 
		batches.append((data[i * batch_size: min(datanum, (i + 1) * batch_size)], label[i * batch_size: min(datanum, (i + 1) * batch_size)]))
    # 5. 通过 slice 机制选出第 i 个 batch 对应的 data 和 label 子集，放入 batches 列表中
	return batches

def numpy_run():
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            # y_pred = model.forward(x.numpy()) # minibatch from pytorch
            
            y_pred = model.forward(x) # now x is a numpy array, so x.numpy() is not needed
            
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)
    
def get_torch_initialization(numpy = True, init_type = 'Torch'):
    import torch 
    if init_type == 'Torch': 
        fc1 = torch.nn.Linear(28 * 28, 256)
        fc2 = torch.nn.Linear(256, 64)
        fc3 = torch.nn.Linear(64, 10)
        
        if numpy:
            W1 = fc1.weight.T.detach().clone().numpy()
            W2 = fc2.weight.T.detach().clone().numpy()
            W3 = fc3.weight.T.detach().clone().numpy()
        else:
            W1 = fc1.weight.T.detach().clone().data
            W2 = fc2.weight.T.detach().clone().data
            W3 = fc3.weight.T.detach().clone().data
            
    elif init_type == 'Xavier_Uniform':
        print('Xavier_Uniform')
        r = np.sqrt(6.0 / (28 * 28 + 256))
        W1 = np.random.uniform(-r, r, (28 * 28, 256))
        r = np.sqrt(6.0 / (256 + 64))
        W2 = np.random.uniform(-r, r, (256, 64))
        r = np.sqrt(6.0 / (64 + 10))
        W3 = np.random.uniform(-r, r, (64, 10))
        
    elif init_type == 'HeKaiming_Uniform':
        print('HeKaiming_Uniform')
        r = np.sqrt(6.0 / (28 * 28))
        W1 = np.random.uniform(-r, r, (28 * 28, 256))
        r = np.sqrt(6.0 / 256)
        W2 = np.random.uniform(-r, r, (256, 64))
        r = 4 * np.sqrt(6.0 / 64)
        W3 = np.random.uniform(-r, r, (64, 10))
        
    elif init_type == 'Xavier_Normal':
        print('Xavier_Normal')
        sigma = 2.0 / (28 * 28 + 256)
        W1 = np.random.normal(0., np.sqrt(sigma), (28 * 28, 256))
        sigma = 2.0 / (256 + 64)
        W2 = np.random.normal(0., np.sqrt(sigma), (256, 64))
        sigma = 2.0 / (64 + 10)
        W3 = np.random.normal(0., np.sqrt(sigma), (64, 10))
        
    elif init_type == 'HeKaiming_Normal':
        print('HeKaiming_Normal')
        sigma = 2.0 / (28 * 28)
        W1 = np.random.normal(0., np.sqrt(sigma), (28 * 28, 256))
        sigma = 2.0 / 256
        W2 = np.random.normal(0., np.sqrt(sigma), (256, 64))
        sigma = 16 * 2.0 / 64
        W3 = np.random.normal(0., np.sqrt(sigma), (64, 10)) 
        
    return W1, W2, W3

def my_numpy_run(learning_rate = 0.1, epoch_number = 3, update_type = None, init_type = 'Torch'):
    print('learning rate = ' + str(learning_rate))
    train_dataset, test_dataset = download_mnist()
    
    model = NumpyModel(learning_rate, update_type, iter_times = 1407)
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization(init_type = init_type)
    
    train_loss = []
    
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset):
            y = one_hot(y)
            
            # y_pred = model.forward(x.numpy()) # minibatch from pytorch
            
            y_pred = model.forward(x) # now x is a numpy array, so x.numpy() is not needed
            
            loss = numpy_loss.get_loss(y_pred, y)

            model.backward(numpy_loss.backward())
            model.optimize(learning_rate) 
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    return train_loss
    # plot_curve(train_loss)

def multi_test():
    from matplotlib import pyplot as plt
    cases = [ None,
             'Momentum',
             'Nesterov',
             'Adam',
             'Inverse Time Decay',
             'Exponential Decay',
             'Natural Exponential Decay',
             'Cosine Decay',
             'AdaGrad',
             'RMSprop',
             'AdaDelta',
                    ]

    colors = ['#1f77b4',
              '#ff7f0e',
              '#2ca02c',
              '#d62728',
              '#9467bd',
              '#8c564b',
              '#e377c2',
              '#7f7f7f',
              '#bcbd22',
              '#17becf',
              '#1a55FF']

    # Configure rcParams axes.prop_cycle to simultaneously cycle cases and colors.
    # mpl.rcParams['axes.prop_cycle'] = cycler(markevery=cases, color=colors) 
    # Set the plot curve with markers and a title
    plt.rcParams['figure.figsize'] = (10.0, 4.0) # 设置figure_size尺寸
    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
    plt.xlabel('step')
    plt.ylabel('loss value')
    for i in range(len(cases)):
        print('Test ' + str(cases[i]) + ' :')
        data = my_numpy_run(update_type = cases[i])
        print('-------------\n')
        ax.plot(range(len(data)), data, linewidth = 0.5, label = str(cases[i]))
        ax.legend(bbox_to_anchor = (1.05, 1), loc = 'upper left', borderaxespad = 0.)
        
    plt.savefig("5.4.png", format = 'png', dpi = 1000) 
    
def multi_test_2():
    from matplotlib import pyplot as plt
    cases = [ 'HeKaiming_Normal',
              'HeKaiming_Uniform',
              'Torch',
              'Xavier_Uniform', 
              'Xavier_Normal', 
                    ]

    colors = ['#1f77b4',
              '#ff7f0e',
              '#2ca02c',
              '#d62728',
              '#9467bd',
              '#8c564b',
              '#e377c2',
              '#7f7f7f',
              '#bcbd22',
              '#17becf',
              '#1a55FF']

    # Configure rcParams axes.prop_cycle to simultaneously cycle cases and colors.
    # mpl.rcParams['axes.prop_cycle'] = cycler(markevery=cases, color=colors) 
    # Set the plot curve with markers and a title
    plt.rcParams['figure.figsize'] = (10.0, 4.0) # 设置figure_size尺寸
    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
    plt.xlabel('step')
    plt.ylabel('loss value')
    for i in range(len(cases)):
        print('Test ' + str(cases[i]) + ' :')
        data = my_numpy_run(init_type = cases[i])
        print('-------------\n')
        ax.plot(range(len(data)), data, linewidth = 0.5, label = str(cases[i]))
        ax.legend(bbox_to_anchor = (1.05, 1), loc = 'upper left', borderaxespad = 0.)
        
    plt.savefig("5.4.png", format = 'png', dpi = 1000) 
    
    
if __name__ == "__main__":
    numpy_run()
    # my_numpy_run(learning_rate = 0.05, update_type = 'RMSprop')
    # multi_test()
    # multi_test_2()
