import numpy as np

class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        # code1 
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        grad_W = np.matmul(self.memory['x'].T, grad_y)
        
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        # code2
        grad_x = grad_y * (self.memory['x'] >= 0)
        
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        # code3
        grad_x = grad_y * (1.0 / (self.memory['x'] + self.epsilon))
        
        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        # code4
        mx  = x.max(axis = 1).reshape(x.shape[0], -1) # 防止上溢和下溢
        ex  = np.exp(x - mx)
        out = (ex.T / (ex.sum(axis = 1))).T 
        self.memory['x'] = x
        self.memory['y'] = out
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        # code5 
        x = self.memory['x']
        y = self.memory['y']
        grad_x = np.zeros(x.shape)
        for i in range(x.shape[0]): 
            grad_x[i] = np.matmul(grad_y[i], -np.matmul(np.matrix(y[i]).T, np.matrix(y[i])) + np.diag(np.array(y[i])))
        return grad_x
        
class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]

class NumpyModel:
    def __init__(self, learning_rate = 0.1, update_type = None, iter_times = 1407):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))


        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()

        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None
        
        # 以下变量指定了梯度回传所用的优化方法，并完成了有关的初始化
        self.update_type   = update_type 
        self.learning_rate = learning_rate
        self.iter_times    = iter_times
        if update_type == 'Momentum':
            self.rho = 0.9
            self.W1_delta = np.zeros(self.W1.shape)
            self.W2_delta = np.zeros(self.W2.shape)
            self.W3_delta = np.zeros(self.W3.shape) 
            
        elif update_type == 'Nesterov':
            self.rho = 0.9 
            self.W1_delta = np.zeros(self.W1.shape)
            self.W2_delta = np.zeros(self.W2.shape)
            self.W3_delta = np.zeros(self.W3.shape) 
            
        elif update_type == 'Adam':
            self.epsilon = 1e-7
            self.beta1, self.beta2 = 0.9, 0.99
            self.M1 = np.zeros(self.W1.shape)
            self.M2 = np.zeros(self.W2.shape)
            self.M3 = np.zeros(self.W3.shape) 
            self.G1, self.G2, self.G3 = .0, .0, .0
            
        elif update_type == 'Inverse Time Decay':
            self.beta = 0.1
            self.t    = 0
            
        elif update_type == 'Exponential Decay':
            self.beta = 0.96
            self.t    = 0
            
        elif update_type == 'Natural Exponential Decay':
            self.beta = 0.04
            self.t    = 0
            
        elif update_type == 'Cosine Decay':
            self.t    = 0
            
        elif update_type == 'AdaGrad':
            self.epsilon = 1e-7
            
        elif update_type == 'RMSprop':
            self.beta = 0.9
            self.epsilon = 1e-7
            self.G1, self.G2, self.G3 = .0, .0, .0
            
        elif update_type == 'AdaDelta':
            self.beta = 0.9
            self.epsilon = 1e-7
            self.W1_delta = np.zeros(self.W1.shape)
            self.W2_delta = np.zeros(self.W2.shape)
            self.W3_delta = np.zeros(self.W3.shape) 
            self.X1, self.X2, self.X3 = .0, .0, .0
            self.G1, self.G2, self.G3 = .0, .0, .0
            
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        # code6
        if self.update_type == 'Nesterov':
            # 在前向传播之前进行 Nesterov 算法的第一阶段
            self.W1_delta = self.rho * self.W1_delta
            self.W2_delta = self.rho * self.W2_delta
            self.W3_delta = self.rho * self.W3_delta
            self.W1 += self.W1_delta
            self.W2 += self.W2_delta
            self.W3 += self.W3_delta
            
        x = self.matmul_1.forward(x, self.W1)
        x = self.relu_1.forward(x)
        x = self.matmul_2.forward(x, self.W2)
        x = self.relu_2.forward(x)
        x = self.matmul_3.forward(x, self.W3)
        x = self.softmax.forward(x)
        x = self.log.forward(x) 
        
        return x
    
    def backward(self, y):
        # for size in y.shape:
        #    y /= size
            
        
        # code7
        #self.log_grad                 = self.log.backward(-y) 
        self.log_grad                 = self.log.backward(y)
        self.softmax_grad             = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad     = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad             = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad    = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad             = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad  = self.matmul_1.backward(self.relu_1_grad)
        
        
    
    def optimize(self, learning_rate):
        if not self.update_type:
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
            
        elif self.update_type == 'Momentum':
            self.W1_delta = self.rho * self.W1_delta - learning_rate * self.W1_grad
            self.W2_delta = self.rho * self.W2_delta - learning_rate * self.W2_grad
            self.W3_delta = self.rho * self.W3_delta - learning_rate * self.W3_grad
            self.W1 += self.W1_delta
            self.W2 += self.W2_delta
            self.W3 += self.W3_delta
            
        elif self.update_type == 'Nesterov':
            # 在参数更新时进行 Nesterov 第二阶段
            self.W1_delta -= learning_rate * self.W1_grad
            self.W2_delta -= learning_rate * self.W2_grad
            self.W3_delta -= learning_rate * self.W3_grad
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
            
        elif self.update_type == 'Adam':
            self.M1 = self.beta1 * self.M1 + (1 - self.beta1) * self.W1_grad
            self.G1 = self.beta2 * self.G1 + (1 - self.beta2) * (self.W1_grad * self.W1_grad).sum() 
            _M1 = self.M1 / (1 - self.beta1)
            _G1 = self.G1 / (1 - self.beta2)
            self.W1 -= learning_rate / np.sqrt(_G1 + self.epsilon) * _M1
            
            self.M2 = self.beta1 * self.M2 + (1 - self.beta1) * self.W2_grad
            self.G2 = self.beta2 * self.G2 + (1 - self.beta2) * (self.W2_grad * self.W2_grad).sum() 
            _M2 = self.M2 / (1 - self.beta1)
            _G2 = self.G2 / (1 - self.beta2)
            self.W2 -= learning_rate / np.sqrt(_G2 + self.epsilon) * _M2
            
            self.M3 = self.beta1 * self.M3 + (1 - self.beta1) * self.W3_grad
            self.G3 = self.beta2 * self.G3 + (1 - self.beta2) * (self.W3_grad * self.W3_grad).sum() 
            _M3 = self.M3 / (1 - self.beta1)
            _G3 = self.G3 / (1 - self.beta2)
            self.W3 -= learning_rate / np.sqrt(_G3 + self.epsilon) * _M3
            
        elif self.update_type == 'Inverse Time Decay': 
            learning_rate = self.learning_rate / (1.0 + self.beta * self.t)
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad 
            self.t  += 1
            
        elif self.update_type == 'Exponential Decay': 
            learning_rate = self.learning_rate * pow(self.beta, self.t)
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
            self.t  += 1
            
        elif self.update_type == 'Natural Exponential Decay': 
            learning_rate = self.learning_rate * np.exp(-self.beta * self.t)
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
            self.t  += 1
            
        elif self.update_type == 'Cosine Decay': 
            learning_rate = self.learning_rate / 2.0 * (1.0 + np.cos(self.t * np.pi / self.iter_times))
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
            self.t  += 1
            
        elif self.update_type == 'AdaGrad': 
            G = (self.W1_grad * self.W1_grad).sum() 
            self.W1 -= learning_rate / np.sqrt(G + self.epsilon) * self.W1_grad
            G = (self.W2_grad * self.W2_grad).sum()
            self.W2 -= learning_rate / np.sqrt(G + self.epsilon) * self.W2_grad
            G = (self.W3_grad * self.W3_grad).sum()
            self.W3 -= learning_rate / np.sqrt(G + self.epsilon) * self.W3_grad
         
        elif self.update_type == 'RMSprop': 
            self.G1 = self.beta * self.G1 + (1 - self.beta) * (self.W1_grad * self.W1_grad).sum() 
            self.W1 -= learning_rate / np.sqrt(self.G1 + self.epsilon) * self.W1_grad
            self.G2 = self.beta * self.G2 + (1 - self.beta) * (self.W2_grad * self.W2_grad).sum() 
            self.W2 -= learning_rate / np.sqrt(self.G2 + self.epsilon) * self.W2_grad
            self.G3 = self.beta * self.G3 + (1 - self.beta) * (self.W3_grad * self.W3_grad).sum() 
            self.W3 -= learning_rate / np.sqrt(self.G3 + self.epsilon) * self.W3_grad  
             
        elif self.update_type == 'AdaDelta':
            self.X1 = self.beta * self.X1 + (1 - self.beta) * (self.W1_delta * self.W1_delta).sum()
            self.G1 = self.beta * self.G1 + (1 - self.beta) * (self.W1_grad * self.W1_grad).sum() 
            self.W1_delta = -np.sqrt(self.X1 + self.epsilon) / np.sqrt(self.G1 + self.epsilon) * self.W1_grad
            self.W1 += self.W1_delta
            
            self.X2 = self.beta * self.X2 + (1 - self.beta) * (self.W2_delta * self.W2_delta).sum()
            self.G2 = self.beta * self.G2 + (1 - self.beta) * (self.W2_grad * self.W2_grad).sum() 
            self.W2_delta = -np.sqrt(self.X2 + self.epsilon) / np.sqrt(self.G2 + self.epsilon) * self.W2_grad
            self.W2 += self.W2_delta
            
            self.X3 = self.beta * self.X3 + (1 - self.beta) * (self.W3_delta * self.W3_delta).sum()
            self.G3 = self.beta * self.G3 + (1 - self.beta) * (self.W3_grad * self.W3_grad).sum() 
            self.W3_delta = -np.sqrt(self.X3 + self.epsilon) / np.sqrt(self.G3 + self.epsilon) * self.W3_grad
            self.W3 += self.W3_delta
    
