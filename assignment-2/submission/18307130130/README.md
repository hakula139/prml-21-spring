# Assignment-2 Report

<p align="right">------李睿琛 18307130130</p>
## 算子实现

`Matmul`

实现两个矩阵相乘。对于：
$$
Y = X \times W
$$
存在函数`Loss = f(Y)`，根据链式法则，求导得：


$$
\frac{\partial L}{\partial W_{ij}}={\sum_{k,l}}\frac{\partial L}{\partial Y_{kl}}\times\frac{\partial Y_{kl}}{\partial W_{ij}} \\\\
$$

$$
\frac{\partial Y_{kl}}{\partial X_{ij}}=\frac{\partial {\sum_{s}(X_{ks}\times W_{sl})}}{W_{ij}}=\frac{\partial X_{ki}W_{il}}{W_{ij}}=A_{ki}{\delta_{lj}}
$$

$$
\frac{\partial L}{\partial W}=X^T\frac{\partial L}{\partial Y}
$$

于是有：
$$
grad\_w = X^T \times grad\_y\\\\
同理有： grad\_x = grad\_y \times W^T
$$
`Relu`

作为激活函数，具有单侧抑制、宽兴奋边界等生物学合理性。
$$
Y_{ij}=\begin{cases}
X_{ij}&X_{ij}\ge0\\\\
0&\text{otherwise}
\end{cases}
$$

求导得：
$$
\frac{\partial Y_{ij}}{\partial X_{mn}}=\begin{cases}1&X_{ij}>0,i=m,j=n\\\\0&\text{otherwise}\end{cases}
$$

`Log`

计算公式为：
$$
Y_{ij}=\ln(X_{ij}+\epsilon),\epsilon=10^{-12} \\\\
$$

求导得：
$$
\frac{\partial Y_{ij}}{\partial X_{ij}}=\frac1{X_{ij}+\epsilon}
$$


`Softmax`

称为多项的Logistic回归，表达式为：
$$
Y_{ij}=\frac{\exp\{X_{ij} \}}{\sum_{k=1}^c\exp\{X_{ik} \}}\\\\
$$
向量`Y_i`对向量`X_j`求导得：
$$
\frac{\partial Y_{i}}{\partial X_{j}}=\begin{cases}
Y_{i}(1-Y_{j})&i=j\\\\
-Y_{i}Y_{j}&\text otherwise
\end{cases}
$$


## 模型搭建

根据`torch_mnist.py`中`TorchModel`方法搭建模型：、

![model](./img/model.png)

根据模型计算图，即可得到如下**前馈过程：**

```python
x = self.matmul_1.forward(x, self.W1)
x = self.relu_1.forward(x)
x = self.matmul_2.forward(x, self.W2)
x = self.relu_2.forward(x)
x = self.matmul_3.forward(x, self.W3)
x = self.softmax.forward(x)
x = self.log.forward(x)
```

**反向传播：**

首先是标量Loss对向量pred的求导：
$$
\frac{\partial L}{\partial X^T} =[\frac{\partial L}{\partial x_1},...,\frac{\partial L}{\partial x_n}]^T
$$
于是有：

```python
class NumpyLoss:
def backward(self):
    return -self.target / self.target.shape[0]
```

根据计算图，继续反向传播：

```python
self.log_grad = self.log.backward(y)
self.softmax_grad = self.softmax.backward(self.log_grad)
self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
self.relu_2_grad = self.relu_2.backward(self.x3_grad)
self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
self.relu_1_grad = self.relu_1.backward(self.x2_grad)
self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
```

## mini_batch实现

由于数据集巨大，深度学习往往训练速度很慢，导致难以发挥最大效果。相比batch梯度下降法，mini_batch进一步分割数据集，能在一次遍历训练集的过程中做多次梯度下降，收敛到一个合适的精度。获得mini_batch过程为：

```python
# 将数据集打乱；
sz = data.shape[0]
index = np.arange(sz)
np.random.shuffle(index)

# 按照batch_size分割数据集；
for start in range(0, sz, batch_size):
    ret.append([data[index[start: start+ batch_size]], label[index[start: start+ batch_size]]])
```

## 模型参数影响

探究了学习率、batch_size大小对收敛速度的影响。

* learning_rate = 0.1，batch_size=128

```
[0] Accuracy: 0.9436
[1] Accuracy: 0.9610
[2] Accuracy: 0.9710
```

![SGN](./img/SGD_train_128_01.png)

* learning_rate = 0.01，batch_size=128

```
[0] Accuracy: 0.8730
[1] Accuracy: 0.9047
[2] Accuracy: 0.9142
```

![SGN](./img/SGD_train_128_001.png)

* learning_rate = 0.1，batch_size=512

```
[0] Accuracy: 0.8233
[1] Accuracy: 0.9244
[2] Accuracy: 0.9296
```

![SGN](./img/SGD_train_512_01.png)

在一定范围内：

随着学习率的减小，参数收敛速度减小，在相同迭代次数下准确率更低。

随着批处理容量的增大，迭代次数减少，震荡幅度减小，随着容量继续增大，可能达到时间上的最优。

## 梯度下降算法优化

* SGD算法。learning_rate = 0.1，batch_size=512

即优化前的算法。

* Momentum算法。learning_rate = 0.1，batch_size=512

SGN的损失在一个方向上快速变化而在另一个方向慢慢变化。Momentum算法将一段时间内的梯度向量进行了加权平均 ，有利于帮助SGN加速，冲破沟壑，加速收敛。引入动量能够使得在遇到局部最优的时候在动量的基础上**冲出局部最优**；另外也可使**震荡减弱**，更快运动到最优解。

**算法实现：**

![](./img/Momentum.png)

```python
# 算法实现
self.beta1 = self.momentum * self.beta1 + (1 - self.momentum) * self.W1_grad
self.W1 -= learning_rate * self.beta1

​``` 结果
[0] Accuracy: 0.9058
[1] Accuracy: 0.9293
[2] Accuracy: 0.9391
​```
```

![](./img/Momentum_train_512_01.png)

* Adam算法。learning_rate = 0.1，batch_size=512

相比随机梯度下降中**不变**的学习率，Adam算法通过计算梯度的一阶矩估计和二阶矩估计而为不同的参数设计独立的**自适应性学习率**。 

**算法实现：**

![](./img/Adam.png)

```python
# 算法实现
self.m1 = self.theta1 * self.m1 + (1 - self.theta1) * self.W1_grad
self.v1 = self.theta2 * self.v1 + (1 - self.theta1) * np.square(self.W1_grad)
m_ = self.m1 / (1 - np.power(self.theta1, self.n))
v_ = self.v1 / (1 - np.power(self.theta2, self.n))
self.W1 -= learning_rate * m_ / (np.sqrt(v_) + self.eps)

​``` 结果
[0] Accuracy: 0.9639
[1] Accuracy: 0.9661
[2] Accuracy: 0.9690
​```
```

![Adam](./img/Adam_train_512_01.png)

比较三次迭代后的准确率，收敛速度：Adam > Momentum > SGD 。