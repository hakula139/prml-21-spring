import numpy as np
from numpy_fnn import NumpyModel, NumpyLoss
from utils import download_mnist, batch, get_torch_initialization, plot_curve, one_hot

def mini_batch(train_dataset, batch_size=128):
    data = np.array([np.array(x[0]) for x in train_dataset])
    label = np.array([np.array(x[1]) for x in train_dataset])

    sz = data.shape[0]
    index = np.arange(sz)
    np.random.shuffle(index)

    ret = []
    for start in range(0, sz, batch_size):
        ret.append([data[index[start: start+ batch_size]], label[index[start: start+ batch_size]]])
    return ret


def numpy_run():
    train_dataset, test_dataset = download_mnist()

    model = NumpyModel()
    numpy_loss = NumpyLoss()
    model.W1, model.W2, model.W3 = get_torch_initialization()
    
    train_loss = []
    
    epoch_number = 3
    learning_rate = 0.1
    
    for epoch in range(epoch_number):
        for x, y in mini_batch(train_dataset, batch_size=512):
            y = one_hot(y)
            y_pred = model.forward(x)
            loss = numpy_loss.get_loss(y_pred, y)

            # numpy_loss.backward().shape: batch_size * 10
            model.backward(numpy_loss.backward())
            model.optimize(learning_rate)
            
            train_loss.append(loss.item())
        
        x, y = batch(test_dataset)[0]
        accuracy = np.mean((model.forward(x).argmax(axis=1) == y))
        print('[{}] Accuracy: {:.4f}'.format(epoch, accuracy))
    
    plot_curve(train_loss)


if __name__ == "__main__":
    numpy_run()
