import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"
import numpy as np


class NumpyOp:
    
    def __init__(self):
        self.memory = {}
        self.epsilon = 1e-12


class Matmul(NumpyOp):
    
    def forward(self, x, W):
        """
        x: shape(N, d)
        w: shape(d, d')
        """
        self.memory['x'] = x
        self.memory['W'] = W
        h = np.matmul(x, W)
        return h
    
    def backward(self, grad_y):
        """
        grad_y: shape(N, d')
        """
        
        ####################
        #      code 1      #
        ####################
        
        grad_x = np.matmul(grad_y, self.memory['W'].T)
        grad_W = np.matmul(self.memory['x'].T, grad_y)
        
        return grad_x, grad_W


class Relu(NumpyOp):
    
    def forward(self, x):
        self.memory['x'] = x
        return np.where(x > 0, x, np.zeros_like(x))
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 2      #
        ####################
        
        x = self.memory['x']
        grad_x = grad_y * np.where( x > 0, np.ones_like(x), np.zeros_like(x) )
        
        return grad_x


class Log(NumpyOp):
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        out = np.log(x + self.epsilon)
        self.memory['x'] = x
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 3      #
        ####################
        
        x = self.memory['x']
        grad_x = grad_y / (x + self.epsilon)

        return grad_x


class Softmax(NumpyOp):
    """
    softmax over last dimension
    """
    
    def forward(self, x):
        """
        x: shape(N, c)
        """
        
        ####################
        #      code 4      #
        ####################

        # 得到矩阵每行的最大值，避免溢出
        row_max = np.max(x, axis=1).reshape(-1, 1)
        x -= row_max
        x_exp = np.exp(x)
        # 每行求和
        sum_exp = np.sum(x_exp, axis=1, keepdims=True)
        # out: N * c
        out = x_exp / sum_exp
        self.memory['out'] = out
        
        return out
    
    def backward(self, grad_y):
        """
        grad_y: same shape as x
        """
        
        ####################
        #      code 5      #
        ####################

        out = self.memory['out']
        # Jacobs: N * c * c
        Jacobs = np.array([np.diag(x) - np.outer(x, x) for x in out])

        # (B, n, m) * (B, m, d) = (B, n, d)
        grad_y = grad_y[:, np.newaxis, :]
        grad_x = np.matmul(grad_y, Jacobs).squeeze(axis=1)
        
        return grad_x


class NumpyLoss:
    
    def __init__(self):
        self.target = None
    
    def get_loss(self, pred, target):
        self.target = target
        return (-pred * target).sum(axis=1).mean()
    
    def backward(self):
        return -self.target / self.target.shape[0]


class NumpyModel:
    def __init__(self):
        self.W1 = np.random.normal(size=(28 * 28, 256))
        self.W2 = np.random.normal(size=(256, 64))
        self.W3 = np.random.normal(size=(64, 10))
        
        # 以下算子会在 forward 和 backward 中使用
        self.matmul_1 = Matmul()
        self.relu_1 = Relu()
        self.matmul_2 = Matmul()
        self.relu_2 = Relu()
        self.matmul_3 = Matmul()
        self.softmax = Softmax()
        self.log = Log()
        
        # 以下变量需要在 backward 中更新。 softmax_grad, log_grad 等为算子反向传播的梯度（ loss 关于算子输入的偏导）
        self.x1_grad, self.W1_grad = None, None
        self.relu_1_grad = None
        self.x2_grad, self.W2_grad = None, None
        self.relu_2_grad = None
        self.x3_grad, self.W3_grad = None, None
        self.softmax_grad = None
        self.log_grad = None

        # Momentum parameters
        self.beta1 = np.zeros_like(self.W1) 
        self.beta2 = np.zeros_like(self.W2) 
        self.beta3 = np.zeros_like(self.W3) 
        self.momentum = 0.9

        # Adam parameters
        self.theta1 = 0.9
        self.theta2 = 0.999
        self.eps = 1e-8
        self.m1 = np.zeros_like(self.W1)
        self.v1 = np.zeros_like(self.W1)
        self.m2 = np.zeros_like(self.W2)
        self.v2 = np.zeros_like(self.W2)
        self.m3 = np.zeros_like(self.W3)
        self.v3 = np.zeros_like(self.W3)
        self.n = 0
    
    def forward(self, x):
        x = x.reshape(-1, 28 * 28)
        
        ####################
        #      code 6      #
        ####################
        
        x = self.matmul_1.forward(x, self.W1)
        x = self.relu_1.forward(x)
        x = self.matmul_2.forward(x, self.W2)
        x = self.relu_2.forward(x)
        x = self.matmul_3.forward(x, self.W3)
        x = self.softmax.forward(x)
        x = self.log.forward(x)
    
        return x
    
    def backward(self, y):
        
        ####################
        #      code 7      #
        ####################

        self.log_grad = self.log.backward(y)
        self.softmax_grad = self.softmax.backward(self.log_grad)
        self.x3_grad, self.W3_grad = self.matmul_3.backward(self.softmax_grad)
        self.relu_2_grad = self.relu_2.backward(self.x3_grad)
        self.x2_grad, self.W2_grad = self.matmul_2.backward(self.relu_2_grad)
        self.relu_1_grad = self.relu_1.backward(self.x2_grad)
        self.x1_grad, self.W1_grad = self.matmul_1.backward(self.relu_1_grad)
        
        return self.x1_grad
    
    def optimize(self, learning_rate):
        def SGD():
            self.W1 -= learning_rate * self.W1_grad
            self.W2 -= learning_rate * self.W2_grad
            self.W3 -= learning_rate * self.W3_grad
        
        def Momentum():

            self.beta1 = self.momentum * self.beta1 + (1 - self.momentum) * self.W1_grad
            self.beta2 = self.momentum * self.beta2 + (1 - self.momentum) * self.W2_grad
            self.beta3 = self.momentum * self.beta3 + (1 - self.momentum) * self.W3_grad
            self.W1 -= learning_rate * self.beta1
            self.W2 -= learning_rate * self.beta2
            self.W3 -= learning_rate * self.beta3

        def Adam():
            
            self.n += 1

            self.m1 = self.theta1 * self.m1 + (1 - self.theta1) * self.W1_grad
            self.v1 = self.theta2 * self.v1 + (1 - self.theta1) * np.square(self.W1_grad)
            m_ = self.m1 / (1 - np.power(self.theta1, self.n))
            v_ = self.v1 / (1 - np.power(self.theta2, self.n))
            self.W1 -= learning_rate * m_ / (np.sqrt(v_) + self.eps)
            
            self.m2 = self.theta1 * self.m2 + (1 - self.theta1) * self.W2_grad
            self.v2 = self.theta2 * self.v2 + (1 - self.theta1) * np.square(self.W2_grad)
            m_ = self.m2 / (1 - np.power(self.theta1, self.n))
            v_ = self.v2 / (1 - np.power(self.theta2, self.n))
            self.W2 -= learning_rate * m_ / (np.sqrt(v_) + self.eps)
            
            self.m3 = self.theta1 * self.m3 + (1 - self.theta1) * self.W3_grad
            self.v3 = self.theta2 * self.v3 + (1 - self.theta1) * np.square(self.W3_grad)
            m_ = self.m3 / (1 - np.power(self.theta1, self.n))
            v_ = self.v3 / (1 - np.power(self.theta2, self.n))
            self.W3 -= learning_rate * m_ / (np.sqrt(v_) + self.eps)

        SGD()
        #Momentum()
        #Adam()

