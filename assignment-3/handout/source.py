import numpy as np

class KMeans:

    def __init__(self, n_clusters):
        pass
    
    def fit(self, train_data):
        pass
    
    def predict(self, test_data):
        pass

class GaussianMixture:

    def __init__(self, n_clusters):
        pass
    
    def fit(self, train_data):
        pass
    
    def predict(self, test_data):
        pass

class ClusteringAlgorithm:

    def __init__(self):
        pass
    
    def fit(self, train_data):
        pass
    
    def predict(self, test_data):
        pass
