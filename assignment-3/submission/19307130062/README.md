# Assignment 3. 聚类算法

- **姓名：高庆麾**
- **学号：19307130062**



## 第〇部分 算法实现介绍

*对 K-Means 和 GMM 算法在代码中已经包含比较详细的注释，对大部分实现过程和细节都在相应位置做了标注和说明，请参看代码。

实验实现了支持多维数据（大于两维）的高维 GMM 算法。详细介绍也请参看代码。



## 第一部分 自动聚类算法实验

生成数据组数 $n = 500$，维数 $dim = 2$ ，生成采用的高斯分布标准差为 $1$ ，中心从 $[0,\ 1]$ 中随机生成，但做了乘 $10$ 倍放大的处理，以增大间距。

聚类数依次为 $3,\ 5,\ 7,\ 9,\ 11,\ 13$ 时的结果如下：

<img src="img/xhynyscjpi Elbow Method ALL15 500, 2, 3.png" alt="xhynyscjpi Elbow Method ALL15 500, 2, 3" style="zoom:3.5%;" /><img src="img/hkmxtjeiaj Elbow Method ALL15 500, 2, 5.png" alt="xhynyscjpi Elbow Method ALL15 500, 2, 3" style="zoom:3.5%;" /><img src="img/ndrdqqpmea Elbow Method ALL15 500, 2, 7.png" alt="xhynyscjpi Elbow Method ALL15 500, 2, 3" style="zoom:3.5%;" /><img src="img/boqdtamgmq Elbow Method ALL15 500, 2, 9.png" alt="xhynyscjpi Elbow Method ALL15 500, 2, 3" style="zoom:3.5%;" /><img src="img/heyhertslr Elbow Method ALL15 500, 2, 11.png" alt="xhynyscjpi Elbow Method ALL15 500, 2, 3" style="zoom:3.5%;" /><img src="img/maddmqffwl Elbow Method ALL15 500, 2, 13.png" alt="xhynyscjpi Elbow Method ALL15 500, 2, 3" style="zoom:3.5%;" />

如果仅通过肉眼观察，可以发现 elbow method 配合自行定义的一套选择拐点的方法（详细介绍请见代码对应部分注释）是非常符合人工观察的预期的，但是数据生成的聚类确实比较不规则，所在的位置在距离和上难以体现出来。



## 第二部分 对聚类结果的可视化实验

生成数据组数 $n = 500$，维数 $dim = 2$ ，生成采用的高斯分布标准差为 $1$ ，中心从 $[0,\ 1]$ 中随机生成，但做了乘 $10$ 倍放大的处理，以增大间距。

KMeans 算法在迭代次数 $20$， 聚类数依次为 $5,\ 9,\ 13$ 时的结果如下：
<img src="img/vhklkqwtkz 500, 2, 5.png" alt="vhklkqwtkz 500, 2, 5" style="zoom:3.5%;" /><img src="img/uvqnndbvpu 500, 2, 9.png" alt="vhklkqwtkz 500, 2, 5" style="zoom:3.5%;" /><img src="img/wuxvkrvwya 500, 2, 13.png" alt="vhklkqwtkz 500, 2, 5" style="zoom:3.5%;" />

可以发现，在聚类数增多时，由于数据生成过于混杂，KMeans 给出了从肉眼观察上看更优的结果，尽管这可能和原始数据的生成方式有所区别。



如果我们适当增大各个聚类的间距，GMM 算法在迭代次数 $20$， 聚类数依次为 $5,\ 9,\ 13$ 时的结果如下：
<img src="img/uxcngpxjhl 500, 2, 5.png" alt="vhklkqwtkz 500, 2, 5" style="zoom:3.5%;" /><img src="img/wishwtfbzz 500, 2, 9.png" alt="vhklkqwtkz 500, 2, 5" style="zoom:3.5%;" /><img src="img/onaixzshct 500, 2, 13.png" alt="vhklkqwtkz 500, 2, 5" style="zoom:3.5%;" />

可以看到很多时候，各个高斯分布的均值容易聚到一起，通过对算法的逻辑进行思考和分析，算法应该确实存在这方面的问题。



