import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn')


# 计算两个点的欧几里得距离
def Euclidistance(pointa, pointb):
    return np.sqrt(np.sum(np.power(pointa - pointb, 2)))


# 从数据集中随机生成k个质心
def CentroidGenerator(K, data):
    t_data = list(data)
    np.random.shuffle(t_data)
    centroids = t_data[:K]
    return np.mat(centroids)


# 从数据集中利用KMeans++初始化质心
def Centroid_Generator(K, data):
    
    centroids = []
    centroids.append(data[np.random.randint(data.shape[0]), :])  # 随机选择一个点作为第一个质心
    
    # 迭代 k-1 次
    for cnt in range(K-1):
        
        dis = []        # 存储距离
        
        for i in range(data.shape[0]):  
            
            point = data[i, :]
            minn = float('inf')
            
            for j in range(len(centroids)):  # 扫描所有质心，选出该样本点与最近的类中心的距离
                
                mid = Euclidistance(point, centroids[j])
                minn = min(minn, mid)
            
            dis.append(minn)
        
        dis = np.array(dis)
        next_centroid = data[np.argmax(dis), :]  # 返回最大dis的下标，即距离当前所有质心最远的一个点
        centroids.append(next_centroid)  
    
    centroids = np.mat(centroids)
    return centroids   


class KMeans:
    """
        1.从数据集中随机选择k个作为质心，
        2.将每个样本分配给其最近的质心
        3.创建新的质心
        4.重复2、3两步直到结束
    """

    def __init__(self, n_clusters):
        """
            K:          簇的数量
            num:        数据集大小
            centroids:  当前状态下K个质心
        """
        
        self.K = n_clusters 
        self.num = 0
        self.c = 0
        self.centroids = None
    
    def fit(self, train_data):
        self.num = train_data.shape[0]

        # 构建一个信息矩阵以存储每一个数据点所属的类以及其距离质心的距离
        self.InfoMatrix = np.mat(np.zeros((self.num, 2)))

        # self.centroids = CentroidGenerator(self.K, train_data)
        self.centroids = Centroid_Generator(self.K, train_data)
              
        # 判定是否继续聚类的指示变量
        continueC = True
        
        while continueC:
            
            continueC = False
            self.c += 1
            
            # 将每个样本分配给其最近的质心
            for i in range(self.num):
                
                minn = float('inf') # 该样本数据点距离质心的最小距离
                clusterNo = -1      # 该样本点所属的类别编号
                
                # 寻找最近的质心
                for j in range(self.K):
                    dist = Euclidistance(train_data[i], self.centroids[j])
                    if dist < minn:
                        minn = dist
                        clusterNo = j
                # 如果某个点所属的类发生了变化则需要继续进行聚类 
                if self.InfoMatrix[i, 0] != clusterNo:
                    continueC = True
                # 更新信息矩阵
                self.InfoMatrix[i, :] = clusterNo, minn
               
            # 创建新的质心
            for i in range(self.K):
                MidMatrix = train_data[np.nonzero(self.InfoMatrix[:, 0].A == i)[0]]
                self.centroids[i, :] = np.mean(MidMatrix, axis=0)
        
        self.InfoMatrix = np.array(self.InfoMatrix)

    def predict(self, test_data):
        
        size = test_data.shape[0]   # 获取测试集的大小
        ans = np.zeros((size, 1)) # 构建答案矩阵以存储每一个测试集数据所属的簇
        for i in range(size):
                
            minn = float('inf') # 该样本数据点距离质心的最小距离
            clusterNo = -1      # 该样本点所属的类别编号

            # 寻找最近的质心
            for j in range(self.K):
                dist = Euclidistance(test_data[i], self.centroids[j])
                if dist < minn:
                    minn = dist
                    clusterNo = j
            
            ans[i] = clusterNo
        
        ans = ans.squeeze()
        return ans
            

class GaussianMixture:

    def __init__(self, n_clusters):
        """
            K:          簇的数量
            p:          任一点属于每个簇的概率
            means:      参数均值
            cov:        参数协方差
            centroids:  为了统一画图而定义的变量，在高斯混合模型中不适用
            times:      迭代次数
            maxtimes:   最大迭代次数
        """
        self.K = n_clusters
        self.centroids = None
        self.p = None
        self.means = None
        self.cov = []
        self.times = 0
        self.maxtimes = 30
    
    def fit(self, train_data):
        
        num, dimension = train_data.shape
        r = np.zeros((num, self.K))   #概率矩阵

        # 初始化并归一化概率
        self.p = np.random.rand(self.K)
        self.p /= np.sum(self.p)
        
        # 通过KMeans初始化协方差和均值
        tpmodel = KMeans(self.K)
        tpmodel.fit(train_data)
        res = tpmodel.predict(train_data)
        
        self.means = tpmodel.centroids  # 质心作为均值
        
        for i in range(self.K):

            mid = train_data[np.where(res == i)] - self.means[i]
            tri = np.eye(self.means[0].shape[1])
            covt = np.array(np.power(np.sum(np.multiply(mid, mid), axis=0), 0.5))
            for j in range(len(covt[0])):
                tri[j][j] = covt[0][j]
            self.cov.append(tri)    # 构造协方差矩阵
        

        def Expectation(x, means, cov):
            
            size = np.shape(cov)[0]
            cov += np.eye(size) * 0.001
            det = np.linalg.det(cov)
            inv = np.linalg.inv(cov)
            X = (x - means).reshape((1, size))
            return 1.0 / np.power(np.power(2 * np.pi, size) * np.abs(det), 0.5) * np.exp(-0.5 * X.dot(inv).dot(X.T))[0][0]

        while self.times < self.maxtimes:

            self.times += 1
            
            # EM算法中的E步,计算期望
            for i in range(num):
                
                mid = [self.p[j] * Expectation(train_data[i], self.means[j], self.cov[j]) for j in range(self.K)]
                sum = np.sum(np.array(mid))
                if sum == 0:
                    sum = self.K
                r[i] = (mid / sum).reshape(r[i].shape)
            
            # EM算法中的M步,更新参数
            for i in range(self.K):
                s = np.sum([r[j][i] for j in range(num)])
                self.p[i] = 1.0 * s / num
                self.means[i] =  1.0 / s * (np.sum([r[j][i] * train_data[j] for j in range(num)], axis=0))
                X = train_data - self.means[i]
                self.cov[i] = 1.0 / s * (np.sum([r[j][i] * X[j].reshape((dimension, 1)).dot(X[j].reshape((1, dimension))) for j in range(num)], axis=0))
    
    def predict(self, test_data):

        size = test_data.shape[0]
        r = np.zeros((size, self.K))   #概率矩阵
        
        def Expectation(x, means, cov):
            
            size = np.shape(cov)[0]
            cov += np.eye(size) * 0.0001
            det = np.linalg.det(cov)
            inv = np.linalg.inv(cov)
            X = (x - means).reshape((1, size))
            return 1.0 / np.power(np.power(2 * np.pi, size) * np.abs(det), 0.5) * np.exp(-0.5 * X.dot(inv).dot(X.T))[0][0]
        
        for i in range(size):
            mid = [self.p[j] * Expectation(test_data[i], self.means[j], self.cov[j]) for j in range(self.K)]
            sum = np.sum(np.array(mid))
            r[i] = (mid / sum).reshape(r[i].shape)
        return np.argmax(r, axis=1)


def CA(model, data):
    """
        AIC = 2 * k - 2 * ln(L)
    """
    k = model.K
    labels = np.array(model.InfoMatrix[:, 0].reshape(1,-1))[0].astype("int")
    centroid = model.centroids
    n, dimension = data.shape
    S = sum([Euclidistance(data[np.where(labels == i)], centroid[i]) ** 2 for i in range(k)])   # 每个数据点和质心的距离平方和
    var = S * 1.0 / (n - k) / dimension
    count = np.bincount(labels)     # 每一类中数据的数量
    LN = np.sum([count[i] * (np.log(count[i]) - np.log(n)) - (count[i] * dimension / 2) * (np.log(2 * np.pi * var) + 1) + (dimension / 2) for i in range(k)])
    AIC = 2 * k - 2 * LN
    return AIC
    


def CB(model, data):
    """
        BIC = k * ln(n) - 2 * ln(L)
    """
    k = model.K
    labels = np.array(model.InfoMatrix[:, 0].reshape(1,-1))[0].astype("int")
    centroid = model.centroids
    n, dimension = data.shape
    S = sum([Euclidistance(data[np.where(labels == i)], centroid[i]) ** 2 for i in range(k)])   # 每个数据点和质心的距离平方和
    var = S * 1.0 / (n - k) / dimension
    count = np.bincount(labels)     # 每一类中数据的数量
    LN = np.sum([count[i] * (np.log(count[i]) - np.log(n)) - (count[i] * dimension / 2) * (np.log(2 * np.pi * var) + 1) + (dimension / 2) for i in range(k)])
    BIC = k * np.log(n) - 2 * LN
    return BIC


class ClusteringAlgorithm:

    def __init__(self):
        self.Kmax = 12
        self.score = float('inf')
        self.centroids = None
        self.K = 0
    
    def fit(self, train_data):
        for i in range(2, self.Kmax):
            model = KMeans(i)
            model.fit(train_data)
            AIC = CA(model, train_data)
            BIC = CB(model, train_data)
            Score = 2 * AIC * BIC / (AIC + BIC)
            print(Score)
            if Score < self.score:
                self.K = i
                self.score = Score

    
    def predict(self, test_data):
        model = KMeans(self.K)
        model.fit(test_data)
        self.centroids = model.centroids
        return model.predict(test_data)


# 数据随机化函数
def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


# 簇质心可视化函数
def centroid_visualize(centroids):
    midlist = np.transpose(centroids).tolist()
    plt.scatter(midlist[0], midlist[1], marker="*", color='yellow', s=100)


# 簇可视化函数
def cluster_visualize(clusters, centroids, cnt):
    K = len(clusters)
    color = plt.cm.get_cmap("plasma", K+1)
    for i in range(K):
        midlist = np.transpose(clusters[i]).tolist()
        plt.scatter(midlist[0], midlist[1], color=color(i), s=10)
    # centroid_visualize(centroids)
    plt.savefig('./img/%d.png'%cnt, dpi=400)
    plt.show()


# 下面是数据生成函数
def data_1():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (400,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (400,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    mean = (10, -10)
    cov = np.array([[10, 0], [0, 10]])
    w = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z, w)
    return data, 4


def data_2():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (400,))

    mean = (2, -1)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (400,))

    mean = (-1, 2)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    mean = (0, -0)
    cov = np.array([[10, 0], [0, 10]])
    w = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z, w)
    return data, 4


def data_3():
    mean = (0, 0, 0)
    cov = np.array([[10, 0, 0], [0, 10, 0], [0, 0, 10]])
    x = np.random.multivariate_normal(mean, cov, (500,))
    
    mean = (0, 8, 12)
    cov = np.array([[5, 0, 0], [0, 5, 0], [0, 0, 5]])
    y = np.random.multivariate_normal(mean, cov, (500,))
    
    mean = (0, 20, 1)
    cov = np.array([[7, 0, 0], [0, 7, 0], [0, 0, 7]])
    z = np.random.multivariate_normal(mean, cov, (500,))

    data, _ = shuffle(x, y, z)
    return data, 3


def data_4():
    mean = (0, 0, 0)
    cov = np.array([[10, 0, 0], [0, 10, 0], [0, 0, 10]])
    x = np.random.multivariate_normal(mean, cov, (500,))
    
    mean = (1, 1, 1)
    cov = np.array([[5, 0, 0], [0, 5, 0], [0, 0, 5]])
    y = np.random.multivariate_normal(mean, cov, (500,))
    
    mean = (-2, 5, 1)
    cov = np.array([[7, 0, 0], [0, 7, 0], [0, 0, 7]])
    z = np.random.multivariate_normal(mean, cov, (500,))

    data, _ = shuffle(x, y, z)
    return data, 3


# 分析作图函数
def Analysis(data_function, algorithm_class):
    data, n_clusters = data_function()
    model = algorithm_class(n_clusters)
    model.fit(data)
    res = model.predict(data)
    clusters = [data[np.where(res==i)] for i in range(model.K)]
    cluster_visualize(clusters, model.centroids, 12)


def Analysis_algorithm(data_function, algorithm_class):
    data, n_clusters = data_function()
    model = algorithm_class()
    model.fit(data)
    res = model.predict(data)
    print(model.K)
    clusters = [data[np.where(res==i)] for i in range(model.K)]
    cluster_visualize(clusters, model.centroids, 9)
    

if __name__ == "__main__":
    Analysis(data_1, GaussianMixture)
