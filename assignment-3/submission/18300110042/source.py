from os import error
import sys
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.einsumfunc import _optimal_path
from numpy.core.fromnumeric import mean, std
from numpy.core.numeric import identity
from numpy.random import random_sample

class KMeans:
    ''' Implementing K-Means clustering algorithm.  '''

    def __init__(self, n_clusters=1, init='random', max_iter=100, tol=1e-6, random_state=825):
        '''
        Initializ KMeans class.
        Args:
            n_clusters: int, default=1
                Number of clusters to form as well as the number of centroids to generate. 
            init: 'random' or 'kmeans++', default='random'
                Method used to initialize centorids. Must be 'random' or 'kmeans++' or 'input',
                - 'random': choose initial centroids randomly 
                - 'kmeans++' : choose initial centroids in a smart way to speed up convergence. 
            max_iter: int, default=100 
                Maximum number of iterations to run.
            tol: float, default=1e-6
                The convergence threshold. Iterations will stop when the difference between the 
                centroids of two consecutive iterations is below this threshold.
            random_state: int, default=825
                RandomState used to determine random number generation for centroids initialization.
        '''
        assert init == 'random' or init == 'kmeans++', 'Unknow initializing method.'
        self.n_clusters = n_clusters
        self.init = init
        self.max_iter = max_iter
        self.tol = tol
        self.random_state = random_state
        self.labels = None # data labels
        self.centroids = None # center of clusters
        self.errors_log = None # sum of squares error


    def _select_centroids(self, X, K):
        '''
        Select K centroids from dataset X.
        Args:
            X: numpy array (n_samples, n_featuers)
                The dataset among which the centroids are selected.
            K: int
                The number of centroids to be selected.
        Return:
            centroids: numpy array (K, n_features)
                The selected K centroids.
        '''
        # check the number of train dataset
        n_samples, n_features = X.shape
        assert n_samples >= K, "The number of data must be great than clusters."

        rng = np.random.RandomState(self.random_state)
        
        centroids  = np.zeros((K, n_features))
        if self.init == 'random': # select centroids ramdomly 
            rand_idx = rng.permutation(n_samples)
            centroids = X[rand_idx[:K]]
        elif self.init == 'kmeans++': # select centroids using kmeans++ algorithm
            # randomly select the first centroid
            rand_idx = rng.choice(n_samples)
            centroids[0] = X[rand_idx]

            # loop to select the rest K-1 centroids
            distance_square = calculate_distance_square(X, centroids[0:1]).flatten()
            for i in range(1, K): 
                # calculate the distance between all points and the current centroid
                new_distance_square = calculate_distance_square(X, centroids[i-1:i]).flatten()
                # compare and keep the shortest distance for all points
                distance_square = np.min(np.vstack((distance_square, new_distance_square)), axis=0)
                # calculate the probability and select the next centroid accordingly
                best_idx = rng.choice(n_samples, size=1, p=distance_square/np.sum(distance_square))
                centroids[i] = X[best_idx]
        return centroids

    
    def _recalculate_model(self, X, K, labels, distance):
        '''
        Recalculate and update model based on the labels of all data in dataset X.
        Notes there may be empty cluster sometimes, and it will cause error. My solution
        is to move the furthest point in the biggest cluster to the empty cluster.
        Args:
            X: numpy array (n_samples, n_featuers)
                The dataset.
            K: int
                The number of cluster to be formed.
            labels: numpy array (n_samples, )
                The labels identifying the cluster that each sample belongs to. 
                label[i] is the index of the cluster for the i-th observation. The clustering
                includes n_clusters clusters.
            distance: numpy array (n_samples, K)
                Distance between points and centroids.
                distance[i, j] is the distance from point(i) to centroid(j)
        '''
        # check wheter there is an empty cluster. move a point to it if empty cluster exist.
        for label in range(K):
            l_keys, l_counts = np.unique(labels, return_counts=True)
            if not label in l_keys: # empty cluster
                # find the biggest cluster
                l_key = l_keys[l_counts.argmax()]
                # find the furthest point in the cluster
                d_copy = np.copy(distance[:, l_key])
                d_copy[labels!=l_key] = 0
                p_idx = np.argmax(d_copy)
                # move the point to the empty cluster
                labels[p_idx] = label
        
        # recalculate centroids
        centroids = np.zeros((K, X.shape[1]))
        for i in range(K):
            centroids[i, :] = np.mean(X[labels==i, :], axis=0)

        return labels, centroids
        

    def _statistic_params(self, X):
        ''' 
        Calculate the means, covariances and mixing probabilities for each clusters.
        Args:
            X: numpy array (n_samples, n_featuers)
                The dataset.
        Returns:
            means: numpy array (K, n_features)
                Means of the clusters.
            covariances: numpy array (K, n_feature, n_feature)
                Covariances of the clusters.
            mixing_probs: numpy array (K, )
                Mixing probabilities of mixture model.
        '''
        # set means to self.centroids
        means = self.centroids
        K, n_features = means.shape
        covariances = np.zeros((K, n_features, n_features))
        mixing_probs = np.zeros(K)

        # calculate covariances and mixing probabilities
        distances_square = calculate_distance_square(X, means)
        labels = np.argmin(distances_square, axis=1)
        for i in range(K):
            Xi = X[labels==i]
            if len(Xi) == 1: # if there is only one point in the cluster
                # duplicate the point and calculate the covariances
                covariances[i] = np.cov(np.vstack((Xi, Xi)).T)
            else: 
                covariances[i] = np.cov(Xi.T)
            mixing_probs[i] = 1.0 * len(Xi) / len(X) 

        return means, covariances, mixing_probs
        

    def fit(self, train_data, K=None, centroids=None, ic=False):
        '''
        Fit the KMeans model in training data.
        Args:
            train_data: nump array (n_samples, n_featuers)
                The input training dataset.
            K: int, default=None
                The input K. Enable change n_clusters when fit.
            centroids: numpy array (K, n_features), default=None
                The input centroids. Enable assign centroids when fit.
            ic: bool, default=False
                Whether to calculate and return means, covariances, mixing probabilities.
        '''
        if K is not None: self.n_clusters = K

        if centroids is not None:
            self.centroids = centroids
            self.n_clusters = centroids.shape[0]
        else:
            self.centroids = self._select_centroids(train_data, self.n_clusters)
        
        # loop to fit kmeans model, break if iteration limits reached or the difference 
        # between the centroids of two consecutive iterations is lower than threshold
        errors = []
        for i in range(self.max_iter):
            # calculate distances between all points and all centroids
            all_distance_square = calculate_distance_square(train_data, self.centroids)

            # calculate and update labels, centroids
            labels = np.argmin(all_distance_square, axis=1)
            old_centroids = self.centroids
            self.labels, self.centroids = self._recalculate_model(train_data, 
                                                                  self.n_clusters, 
                                                                  labels, 
                                                                  all_distance_square)

            # calculate errors, update errors_log
            inner_distance_square = np.zeros(train_data.shape[0])
            for i in range(self.n_clusters):
                inner_distance_square[labels==i] = calculate_distance_square(
                                                            train_data[labels==i], 
                                                            self.centroids[i:i+1]).flatten()
            errors.append(np.sum(inner_distance_square))

            # check convergence, break if centroids' diffirence is below threshold
            if np.linalg.norm(self.centroids - old_centroids) < self.tol:
                break
        self.errors_log = np.array(errors)

        # if fit for information criterion, then calculate and return statistic parameters
        if ic:
            centroids, covariances, mixing_probs = self._statistic_params(train_data)
            return centroids, self.labels, covariances, mixing_probs
        else:
            return self.n_clusters, self.centroids, self.labels, self.errors_log
    

    def predict(self, test_data):
        '''
        Predict to identify the labels for test dataset.
        Args:
            test_data: numpy array (n_samples, n_featuers)
                The test dataset.
        Returns:
            The labels of test data.
        '''
        distance_square = calculate_distance_square(test_data, self.centroids)
        return np.argmin(distance_square, axis=1)


    def optimize_k(self, X, optimizer):
        '''
        Optimize K for the model.
        Args:
            X: numpy array (n_samples, n_featuers)
                The dataset.
            algorithm: must be GapsOptimizer or XICOptimizer
                The optimizer implementing Gap Statistic or AIC/BIC algorithm.
        Returns:
            Optimized model.
        '''
        K = optimizer.get_optimal_k(X)
        self.__init__(n_clusters=K)
        return self 

    def get_sse(self, X):
        '''
        Return sum of square error.
        '''
        inner_distance_square = np.zeros(X.shape[0])
        for i in range(self.n_clusters):
            inner_distance_square[labels==i] = calculate_distance_square(
                                                X[labels==i], self.centroids[i:i+1]).flatten()
        return np.sum(inner_distance_square)


class GaussianMixture:
    ''' Implementing Gaussian Mixture clustering algorithm.  '''

    def __init__(self, n_clusters=1, init='random', max_iter=100, tol=1e-4, random_state=825):
        '''
        Initializing GaussianMixture class.
        Args:
            n_clusters: int, default=1
                Number of clusters to form as well as the number of centroids to generate. 
            init: 'random' or 'kmeans++', default='random'
                Method used to initialize the parameters.
                -'random': initialize parameters randomly.
                - 'kmeans': initialize parameters using kmeans algorithm.
            max_iter: int, default=100 
                Maximum number of iterations to run.
            tol: float, default=1e-6
                The convergence threshold. EM iterations will stop when the lower bound average 
                gain is below this threshold.
            random_state: int, default=825
                RandomState used to determine random number generation for parameters initialization.
        '''
        assert init == 'random' or init == 'kmeans', 'Unknow initializing method.'
        self.n_clusters = n_clusters
        self.init = init
        self.max_iter = max_iter
        self.tol = tol
        self.random_state = random_state
        self.labels = None
        self.centroids = None
        self.covariances = None
        self.mixing_probs = None
        self.errors_log = None

    def _initialize_parameters(self, X, K, centroids):
        '''
        Initialize parameters including means, covariances and mixing probabilities
        based on dataset X.
        Args:
            X: numpy array (n_samples, n_featuers)
                The dataset.
            K: int
                The number of clusters.
            centroids: numpy array (K, n_features)
                The centroids used to initialize the model.
        Returns:
            means: numpy array (K, n_features)
                The initial K centroids.
            covariances: numpy array (K, n_features, n_features)
                The initial K covariaces.
            mixing_probs: numpy array (K, )
                The mixing probabilities.
        '''
        n_samples, n_features = X.shape
        assert n_samples >= K, "The number of data must be great than clusters."

        rng = np.random.RandomState(self.random_state)

        if centroids is not None: 
            means = centroids
            self.n_clusters = centroids.shape[0]
            K = centroids.shape[0]
        else:
            # initialize means randomly
            if self.init == 'random':
                rand_idx = rng.permutation(n_samples)
                X_shuffled = X[rand_idx]
                means = X_shuffled[:K]
            # initialize means using KMeans algorithm
            elif self.init == 'kmeans':
                kmeans = KMeans(K)
                kmeans.fit(X)
                means = kmeans.centroids

        # calculate covariances and mixing probabilities
        covariances = np.zeros((K, n_features, n_features))
        mixing_probs = np.zeros(K)
        distances_square = calculate_distance_square(X, means)
        labels = np.argmin(distances_square, axis=1)
        for i in range(K):
            Xi = X[labels==i]
            if len(Xi) == 0:
                covariances[i] = np.zeros((n_features, n_features))
            elif len(Xi) == 1: # if there is only one point in the cluster
                # duplicate the point and calculate the covariances
                covariances[i] = np.cov(np.vstack((Xi, Xi)).T)
            else: 
                covariances[i] = np.cov(Xi.T)
            mixing_probs[i] = 1.0 * len(Xi) / n_samples 
        return means, covariances, mixing_probs


    def _statistic_params(self, X):
        return self.centroids, self.covariances, self.mixing_probs


    def fit(self, train_data, K=None, centroids=None, ic=False):
        '''
        Fit the model in the training dataset.
        Args:
            train_data: np.array (n_samples, n_features)
                The input training dataset.
            K: int, default=None
                The input K. Enable change n_clusters when fit.
            centroids: numpy array (K, n_features), default=None
                The input centroids. Enable assign centroids when fit.
            ic: bool, default=False
                Whether to calculate and return means, covariances, mixing probabilities.
        '''
        if K is not None: self.n_clusters = K

        self.centroids, self.covariances, self.mixing_probs = \
                    self._initialize_parameters(train_data, self.n_clusters, centroids)

        n_samples = train_data.shape[0]
        log_likelihood = 0
        errors = []
        for iter in range(self.max_iter):
            # 1) calculate normal distribution probabilities density matrix
            norm_densities = normal_pdf(train_data, self.centroids, self.covariances)

            # 2) check log likelihood for convergence, break if threshold reached
            pxs = [np.dot(self.mixing_probs.T, norm_densities[i]) for i in range(train_data.shape[0])]
            new_log_likelihood = np.sum(np.log(np.array(pxs)))
            errors.append(-new_log_likelihood)
            if np.abs(new_log_likelihood - log_likelihood) < self.tol:
                break
            log_likelihood = new_log_likelihood

            # 3) calculate posterior probability of each point for each cluster
            responsibilities = np.empty((n_samples, self.n_clusters), np.float64)
            for i in range(n_samples):
                denominator = np.dot(self.mixing_probs.T, norm_densities[i])
                for j in range(self.n_clusters):
                    responsibilities[i][j] = self.mixing_probs[j] * norm_densities[i][j] / denominator

            # 4) update parameters means, covariances and mixing probabilities 
            for i in range(self.n_clusters):
                responsibility = responsibilities[:, i]
                denominator = np.sum(responsibility)
                # re-calculate means
                self.centroids[i] = np.dot(responsibility.T, train_data) / denominator
                # re-calculate covariances
                difference = train_data - np.tile(self.centroids[i], (n_samples, 1))
                _r = responsibility.reshape(n_samples, 1)
                self.covariances[i] = np.dot(np.multiply(_r, difference).T, difference) / denominator
                # re-calculate mixing probabilities
                self.mixing_probs[i] = denominator / n_samples
        
        # calcuate and update model
        self.labels = self.predict(train_data)
        self.errors_log = np.array(errors)

        # if fit for information criterion, then calculate and return statistic parameters
        if ic:
            centroids, covariances, mixing_probs = self._statistic_params(train_data)
            return centroids, self.labels, covariances, mixing_probs
        else:
            return self.n_clusters, self.centroids, self.labels, self.errors_log


    def predict(self, test_data):
        '''
        Predict the labels of test_data.
        Args:
            test_data: numpy array (n_samples, n_featuers)
                The test dataset.
        Returns:
            labels: numpy array (n_samples, )
                The labels of test data.
        '''
        n_row, _ = test_data.shape
        labels = np.empty(n_row, dtype=np.int32)
        probs = normal_pdf(test_data, self.centroids, self.covariances)
        for i in range(n_row):
            labels[i] = np.argmax(probs[i])
        return labels


    def optimize_k(self, X, optimizer):
        '''
        Optimize K for the model.
        Args:
            X: numpy array (n_samples, n_featuers)
                The dataset.
            algorithm: must be GapsOptimizer or XICOptimizer
                The optimizer implementing Gap Statistic or AIC/BIC algorithm.
        Returns:
            Optimized model.
        '''
        K = optimizer.get_optimal_k(X)
        self.__init__(n_clusters=K)
        return self 


    def get_sse(self, X):
        '''
        Return sum of square error.
        '''
        inner_distance_square = np.zeros(X.shape[0])
        for i in range(self.n_clusters):
            inner_distance_square[labels==i] = calculate_distance_square(
                                                X[labels==i], self.centroids[i:i+1]).flatten()
        return np.sum(inner_distance_square)


class GapsOptimizer:
    '''
    An optimizer that uses the gap statistic to estimate the optimal number of clusters.
    '''
    def __init__(self, base_clusterer=None, B=10, min_k=1, max_k=10, random_state=825):
        '''
        Initialize the GapStatistic optimizer.
        Args:
            base_clusterer: object or None, default=None
                The base clusterer to use to cluster the data. If None, then the base 
                clusterer is K-Means.
            B: int, default=10
                The number of reference data sets that are generated and clustered in 
                order to estimate the optimal number of clusters for the data set.
            min_k: int, default=1
                The minimal number of clusters to consider when estimating the optimal 
                number of clusters for the data set.
            max_k: int, default=10
                The maximum number of clusters to consider when estimating the optimal 
                number of clusters for the data set.
        '''
        self.base_clusterer = self._check_clusterer(base_clusterer)
        self.B = B
        self.min_k = min_k
        self.max_k = max_k
        self.random_state = random_state


    def _check_clusterer(self, clusterer=None):
        '''
        Check that the clusterer is a valid clusterer with required methods.  If no cluster
        is provided, create a default KMeans clusterer.
        Args: 
            clusterer: object or None, default=None
                The clusterer to use to cluster the data sets. If None, then the clusterer 
                is K-Means.
        Returns:
            clusterer: object
                The supplied clusterer or a default clusterer if none was provided.
        '''
        if (clusterer is None): 
            clusterer = KMeans()
        else:
            # make sure base clusterer implements fit()
            getattr(clusterer, 'fit')
            # make sure base clusterer has n_clusters attribute
            getattr(clusterer, 'n_clusters')
        return clusterer


    def _gap_statistic(self, X):
        '''
        Gap statistic clustering algorithm. Uses the gap statistic method to estimate the 
        optimal number of clusters.
        Args:
            X: numpy array (n_samples, n_features)
                The dataset to cluster.
        Returns:
            K: int
                The estimate of the optimal number of clusters.
            gaps: numpy array, (K, ) 
                The gap statistic calculated as log(W*) - log(W).
                - W: The mean pooled within-cluter sum of squares around the cluster means 
                    for the input data set.
                - log(W): the logarithm of W.
                - log(W*): The expectation of log(W) under an reference distribution of the data. 
            std_errs: numpy array, (K, )
                The standard error for the means of the B sample reference datasets
        '''
        # randomly generate the B reference datasets based on the input dataset X
        rdg = np.random.RandomState(self.random_state)
        n_row, n_col = X.shape
        maxs, mins = X.max(axis=0), X.min(axis=0)
        ranges = np.diag(maxs - mins)
        X_ref = rdg.random_sample(size=(self.B, n_row, n_col))
        for i in range(self.B):
            X_ref[i, :, :] = X_ref[i, :, :] @ ranges + mins
        # range of K from min_k to max_k
        K = range(self.min_k, self.max_k + 1)
        # calculate W and W*
        W = np.zeros(len(K))
        W_star = np.zeros((len(K), self.B))
        for idx, k in enumerate(K):
            # calculate sum of square error using self.clusterer
            self.base_clusterer.fit(X, k)
            W[idx] = self.base_clusterer.get_sse(X)
            for i in range(self.B):
                self.base_clusterer.fit(X_ref[i], k)
                W_star[idx, i] = self.base_clusterer.get_sse(X)
        # calculate gaps, and standard deviation
        gaps = np.mean(np.log(W_star), axis=1) - np.log(W)
        sd_ks = np.std(np.log(W_star), axis=1)
        std_errs = sd_ks * np.sqrt(1 + 1.0 / self.B)
        return gaps, std_errs 


    def get_optimal_k(self, X):
        '''
        Estimate and return the optimal number of clusters.
        Args:
            X: numpy array (n_samples, n_features)
                The dataset to cluster.
        Returns:
            n_clusters: int
                The estimate of the optimal number of clusters.
        '''
        # ensure the max_k is not greater than the samples of input dataset.
        self.max_k = X.shape[0] if self.max_k > X.shape[0] else self.max_k

        # perform gap statistic to get the gaps and std_errs
        gaps, std_errs = self._gap_statistic(X)

        # estimate optimal K
        optimal_k = self.max_k
        for k in range(self.max_k - self.min_k):
            if std_errs[k+1] - (gaps[k+1] - gaps[k]) > 0:
                optimal_k = self.min_k + k
                break

        return optimal_k


class XICOptimizer:
    '''
    An optimizer that uses AIC (Akaike information criterion) or Bayesian Information Criterion (BIC)
    to approximate the correct number of clusters.
    '''
    def __init__(self, base_clusterer=None, ic='BIC', min_k=1, max_k=10, random_state=825):
        '''
        Initialize the BIC optimizer.
        Args:
            base_clusterer: object or None, default=None
                The base clusterer to use to cluster the data. If None, then the base 
                clusterer is K-Means.
            min_k: int, default=1
                The minimal number of clusters to consider when estimating the optimal 
                number of clusters for the data set.
            max_k: int, default=10
                The maximum number of clusters to consider when estimating the optimal 
                number of clusters for the data set.
        '''
        self.base_clusterer = self._check_clusterer(base_clusterer)
        self.min_k = min_k
        self.max_k = max_k
        self.random_state = random_state


    def _check_clusterer(self, clusterer=None):
        '''
        Check that the clusterer is a valid clusterer with required methods.  If no cluster
        is provided, create a default KMeans clusterer.
        Args: 
            clusterer: object or None, default=None
                The clusterer to use to cluster the data sets. If None, then the clusterer 
                is K-Means.
        Returns:
            clusterer: object
                The supplied clusterer or a default clusterer if none was provided.
        '''
        if clusterer is None: 
            clusterer = KMeans()
        else:
            # make sure base clusterer implements fit()
            getattr(clusterer, 'fit')
            # make sure base clusterer has n_clusters attribute
            getattr(clusterer, 'n_clusters')
        return clusterer
    

    def _xic(self, X, means, covariances, mixing_probs, type='BIC'):
        '''
        Calculate the AIC (Akaike information criterion) or BIC (Bayesian Information Criterion),  
        The AIC is defined as: AIC = 2 * k - 2 * ln(L)
        The BIC is defined as: BIC = k * ln(n) - 2 * ln(L) 
        where:
            * ln(L) is the log-likelihood of the estimated model
            * k is the number of parameters
            * n is the number of samples
        Args:
            X: numpy array (n_samples, n_features)
                The dataset to cluster.
            type: must be 'AIC' or 'BIC'
                The information criterion type.
        Returns:
            ic: float
                AIC or BIC value.
        '''
        assert type == 'AIC' or type == 'BIC', 'Unknow information criterion type.'
        # calculate likelihood
        means, covariances, mixing_probs = self.base_clusterer._statistic_params(X)
        ll = calculate_log_likelihood(X, means, covariances, mixing_probs)
        # calculate number of parameters
        # K - 1 (mixing probabilities) 
        # K * n_features (means)
        # K * n_features * (n_features + 1) / 2 (covariances)
        n_features = X.shape[1]
        k = means.shape[0] * (0.5*n_features**2 + 1.5*n_features +1) - 1
        # calculate number of samples
        n = X.shape[0]
        if type == 'AIC':
            return 2 * k - 2 * ll
        elif type == 'BIC':
            return k * np.log(n) - 2 * ll


    def get_optimal_k(self, X):
        '''
        Estimate and return the optimal number of clusters.
        Args:
            X: numpy array (n_samples, n_features)
                The dataset to cluster.
        Returns:
            n_clusters: int
                The estimate of the optimal number of clusters.
        '''
        # ensure the max_k is not greater than the samples of input dataset.
        self.max_k = X.shape[0] if self.max_k > X.shape[0] else self.max_k
        # estimate optimal K
        k = self.min_k 
        _, centroids, labels, _ = self.base_clusterer.fit(X, k)
        while k <= self.max_k:
            new_centroids = []
            for i, centroid in enumerate(centroids):
                Xi = X[labels==i]
                if len(Xi) == 1:
                    new_centroids.append(centroid)
                else:
                    means_1, _, covs_1, probs_1 = self.base_clusterer.fit(Xi, 1, np.array([centroid]), ic=True)
                    ic_1 = self._xic(Xi, means_1, covs_1, probs_1)
                    means_2, _, covs_2, probs_2 = self.base_clusterer.fit(Xi, 2, ic=True)
                    ic_2 = self._xic(Xi, means_2, covs_2, probs_2)
                    if ic_1 <= ic_2: # keep parent
                        new_centroids.append(means_1[0])
                    else: # keep children
                        new_centroids.append(means_2[0])
                        new_centroids.append(means_2[1])
            if k == len(new_centroids):
                break
            k = len(new_centroids)
            new_centroids = np.array(new_centroids)
            _, centroids, labels, _ = self.base_clusterer.fit(X, k, new_centroids)
        return k


class ClusteringAlgorithm:
    def __init__(self, clusterer='KMeans', optimizer='GapsOptimizer'):
        assert clusterer == 'KMeans' or clusterer == 'GaussianMixture', 'Unknow clustering model.'
        assert optimizer == 'GapsOptimizer' or optimizer == 'XICOptimizer', 'Unknow clustering optimizer.'

        if clusterer == 'KMeans':
            self.clusterer = KMeans()
        elif clusterer == 'GaussianMixture':
            self.clusterer = GaussianMixture()

        if optimizer == 'GapsOptimizer':
            self.optimizer = GapsOptimizer(self.clusterer)
        elif optimizer == 'XICOptimizer':
            self.optimizer = XICOptimizer(self.clusterer)

    def fit(self, train_data):
        self.clusterer = self.clusterer.optimize_k(train_data, self.optimizer)
        K, centroids, labels, errors_log = self.clusterer.fit(train_data)
        return K, centroids, labels, errors_log
    
    def predict(self, test_data):
        labels = self.model.predict(test_data)


def calculate_distance_square(X, centroids):
    '''
    Help function to calculate distance^2 from each points in dataset X to eahc centroids.
    Args:
        X: numpy array (n_samples, n_featuers)
            The dataset.
        centroids: numpy array (n, n_features)
            The centroids.
    Returns:
        distance: numpy array (n_samples, n)
            Distance between points and centroids.
            distance[i, j] is the distance from point(i) to centroid(j)
    '''
    epsilon = 1e-8
    n_row = centroids.shape[0]
    distance_square = np.zeros((X.shape[0], n_row))
    for i in range(n_row):
        # distance between each point in X and the centroid[i]
        row_norm = np.linalg.norm(X - centroids[i, :], axis=1)
        # duplicate point will get 0 distance, this will cause some weird error.
        # add a very small to avoid these error.
        row_norm[row_norm - epsilon < 0] = epsilon
        distance_square[:, i] = np.square(row_norm)
    return distance_square


def normal_pdf(X, means, covariances):
    '''
    Help functions to calculate the normal distribution probability density.
    Arg:
        X: numpy array (n_samples, n_features)
            The dataset.
        means: numpy array (K, n_features)
            Means of the clusters.
        covariances: numpy array (K, n_feature, n_feature)
            Covariances of the clusters.
    Returns:
        densities: numpy array (n_samples, K)
        The normal distribution probabilities density matrix.
    '''
    # add perturbation to covariance matrix to avoid singular matrix error
    epsilon = 1e-6
    n_samples = X.shape[0]
    K = means.shape[0]
    norm_densities = np.empty((n_samples, K), np.float64)
    for i in range(n_samples):
        x = X[i]
        for j in range(K):
            if len(x) == 1: # if 1D, covariance is the square of variance 
                cov = covariances[j] + epsilon * epsilon
                # calculate the inverse of covariance
                cov_inv = 1 / cov
                # calculate the determinant of covariance
                cov_det = cov
            else: # if 2D or above
                cov = covariances[j] + epsilon * np.identity(covariances[j].shape[0])
                # calculate the inverse of covariance
                cov_inv = np.linalg.inv(cov)
                # calculate the determinant of covariance
                cov_det = np.linalg.det(cov)
            # calculate the exponent portion of the formula
            exponent = -0.5 * np.dot(np.dot((x - means[j]).T, cov_inv), (x - means[j])) 
            # calculate the denominator portion of the formula
            denominator = np.power(2 * np.pi, K / 2) * np.sqrt(cov_det)
            norm_densities[i][j] = np.exp(exponent) / denominator
    return norm_densities


def calculate_log_likelihood(X, means, covariances, mixing_probs):
    ''' 
    Help function to calculate the log likelihood of the model given the dataset and 
    parameters means, covariances, mixing probabilities
    Arg:
        X: numpy array (n_samples, n_features)
            The dataset.
        means: numpy array (K, n_features)
            Means of the clusters.
        covariances: numpy array (K, n_feature, n_feature)
            Covariances of the clusters.
        mixing_probs: numpy array (K, )
            Mixing probabilities of mixture model.
    Returns:
        log_likelihood: float
            The log likelihood.
    '''
    norm_densities = normal_pdf(X, means, covariances)
    pxs = [np.dot(mixing_probs.T, norm_densities[i]) for i in range(X.shape[0])]
    log_likelihood = np.sum(np.log(np.array(pxs)))
    return log_likelihood

#def plot_fit(C, K, X, labels, means, errors_log, labels_p, means_p):
def plot_fit(C, K, X, labels, errors_log, labels_p, means_p):
    colours=['mediumpurple', 'gold', 'dodgerblue', 'royalblue', 'orange', 
             'mediumaquamarine', 'coral', 'lightskyblue', 'cornflowerblue', 'lightcoral', 
             'mediumturquoise', 'mediumslateblue', 'lightseagreen']
    fig, axs = plt.subplots(1, 2, sharex='col', sharey='row', figsize=(11, 5))
    title = 'Algorithm: {0}, K: {1}'.format(C, K)
    x_min, x_max = np.min(X[:, 0]) - 1 , np.max(X[:, 0]) * 1.05
    y_min, y_max = np.min(X[:, 1]) - 1, np.max(X[:, 1]) * 1.05
    # axs[0], original data
    axs[0].grid()
    axs[0].set_title('Original Data ({0})'.format(title))
    axs[0].set_xlim(x_min, x_max)
    axs[0].set_ylim(y_min, y_max)
    for i in range(len(np.unique(labels))):
        axs[0].scatter(X[labels==i, 0], X[labels==i, 1], s=10, c=colours[i])
        #axs[0].scatter(means[i, 0], means[i, 1], c='k', s=60, marker='*', label='centroid')
    # axs[1], clustering result
    axs[1].grid()
    axs[1].set_title('Clustering Result ({0})'.format(title))
    axs[1].set_xlim(x_min, x_max)
    axs[1].set_ylim(y_min, y_max)
    for i in range(means_p.shape[0]):
        axs[1].scatter(X[labels_p==i, 0], X[labels_p==i, 1], s=10, c=colours[i])
        axs[1].scatter(means_p[i, 0], means_p[i, 1], c='k', s=60, marker='*', label='centroid')
    plt.show()
    # axs[2], loss
    plt.grid()
    plt.title('Loss ({0})'.format(title))
    k = list(range(len(errors_log)))
    plt.plot(k, errors_log, '-o')
    plt.show()


l_config = [
    { # 1
        'means':    [ [1, 2], [16, -5], [10, 22] ],
        'covs':     [ [ [70,  0], [ 0, 22] ],
                      [ [21,  0], [ 0, 32] ],
                      [ [10,  5], [ 5, 10] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'random',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 2
        'means':    [ [1, 2], [16, -5], [10, 22] ],
        'covs':     [ [ [70,  0], [ 0, 22] ],
                      [ [21,  0], [ 0, 32] ],
                      [ [10,  5], [ 5, 10] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 3
        'means':    [ [1, 2], [16, -5], [10, 22] ],
        'covs':     [ [ [70,  0], [ 0, 22] ],
                      [ [21,  0], [ 0, 32] ],
                      [ [10,  5], [ 5, 10] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'random',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 4
        'means':    [ [1, 2], [16, -5], [10, 22] ],
        'covs':     [ [ [70,  0], [ 0, 22] ],
                      [ [21,  0], [ 0, 32] ],
                      [ [10,  5], [ 5, 10] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 5
        'means':    [ [1, 5], [15, 10], [10, 20] ],
        'covs':     [ [ [70,  0], [ 0, 70] ],
                      [ [30,  0], [ 0, 30] ],
                      [ [50,  0], [ 0, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 6
        'means':    [ [1, 5], [15, 10], [10, 20] ],
        'covs':     [ [ [70,  0], [ 0, 70] ],
                      [ [30,  0], [ 0, 30] ],
                      [ [50,  0], [ 0, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 7
        'means':    [ [-10, 50], [30, 10], [10, 20] ],
        'covs':     [ [ [50, 25], [25, 40] ],
                      [ [30, 20], [20, 60] ],
                      [ [20,  0], [ 0, 50] ] ],
        'n_data':   [ 1000, 400, 100 ],
        'K':        3,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 8
        'means':    [ [-10, 50], [30, 10], [10, 20] ],
        'covs':     [ [ [50, 25], [25, 40] ],
                      [ [30, 20], [20, 60] ],
                      [ [20,  0], [ 0, 50] ] ],
        'n_data':   [ 1000, 400, 100 ],
        'K':        3,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 9 
        'means':    [ [ 1, 30], [15, 10], [10, 20] ],
        'covs':     [ [ [50,  0], [ 0, 50] ],
                      [ [30,  0], [ 0, 30] ],
                      [ [50,  0], [ 0, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 10 
        'means':    [ [ 1, 30], [15, 10], [10, 20] ],
        'covs':     [ [ [50,  0], [ 0, 50] ],
                      [ [30,  0], [ 0, 30] ],
                      [ [50,  0], [ 0, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 11 
        'means':    [ [ 1, 30], [15, 10], [10, 20] ],
        'covs':     [ [ [30,  0], [ 0, 20] ],
                      [ [60,  0], [ 0, 50] ],
                      [ [70,  0], [ 0, 30] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 12 
        'means':    [ [ 1, 30], [15, 10], [10, 20] ],
        'covs':     [ [ [30,  0], [ 0, 20] ],
                      [ [60,  0], [ 0, 50] ],
                      [ [70,  0], [ 0, 30] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 13 
        'means':    [ [ 1, 30], [15, 10], [10, 20] ],
        'covs':     [ [ [50, 25], [25, 40] ],
                      [ [30,-20], [-20,60] ],
                      [ [20,-15], [-15, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 14 
        'means':    [ [ 1, 30], [15, 10], [10, 20] ],
        'covs':     [ [ [50, 25], [25, 40] ],
                      [ [30,-20], [-20,60] ],
                      [ [20,-15], [-15, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 15
        'x_func':   [ '20 * np.random.random((500,))', 
                      '25 * np.random.random((500,)) + 3'], 
        'y_func':   [ '5 * np.ones(500)',
                      '2 * np.ones(500)'],
        'K':        2,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 16
        'x_func':   [ '20 * np.random.random((500,))', 
                      '25 * np.random.random((500,)) + 3'], 
        'y_func':   [ '5 * np.ones(500)',
                      '2 * np.ones(500)'],
        'K':        2,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 17
        'x_func':   [ 'np.pi * np.random.random((500,))', 
                      'np.pi * np.random.random((500,))'], 
        'y_func':   [ 'np.sin(x) + 3',
                      '3 * np.sin(x + np.pi / 2) - 7'],
        'K':        2,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'None'
    },
    { # 18
        'x_func':   [ 'np.pi * np.random.random((500,))', 
                      'np.pi * np.random.random((500,))'], 
        'y_func':   [ 'np.sin(x) + 3',
                      '3 * np.sin(x + np.pi / 2) - 7'],
        'K':        2,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'None'
    },
    { # 19 
        'means':    [ [-10, 50], [30, 10], [10, 20] ],
        'covs':     [ [ [50, 25], [25, 40] ],
                      [ [30, 20], [20, 60] ],
                      [ [20,  0], [ 0, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans++',
        'cluster':  'KMeans',
        'optimizer':'GapsOptimizer'
    },
    { # 20 
        'means':    [ [-10, 50], [30, 10], [10, 20] ],
        'covs':     [ [ [50, 25], [25, 40] ],
                      [ [30, 20], [20, 60] ],
                      [ [20,  0], [ 0, 50] ] ],
        'n_data':   [ 500, 500, 500 ],
        'K':        3,
        'init':     'kmeans',
        'cluster':  'GMM',
        'optimizer':'GapsOptimizer'
    },
]

def generate_data(means, covs, n_data):
    ds, ls = [], []
    for i in range(len(means)):
        d = np.random.multivariate_normal(means[i], covs[i], n_data[i])
        ds.append(d)
        ls.append(np.ones((n_data[i],), dtype=int) * i)
    data = np.concatenate(ds)
    lable = np.concatenate(ls)
    return data, lable

def generate_data_by_func(x_func, y_func):
    ds, ls = [], []
    for i in range(len(x_func)):
        x = eval(x_func[i])
        y = eval(y_func[i])
        ds.append(list(zip(x, y)))
        ls.append(np.ones(len(x)) * i)
    data = np.concatenate(ds)
    lable = np.concatenate(ls)
    return data, lable
        

if __name__ == '__main__':
    max_lab = len(l_config)

    # parse user input
    if len(sys.argv) > 1 and sys.argv[1].isnumeric():
        lab_num = int(sys.argv[1])
    else:
        print('Usage: python source.py lab_num (1 ~ {0})'.format(max_lab))
        sys.exit(-1) 

    if lab_num > max_lab or lab_num < 1:
        print('Usage: python source.py lab_num (1 ~ {0})'.format(max_lab))
        sys.exit(-1)     

    # prepare dataset
    if lab_num < 15 or lab_num > 18:
        n_data = np.array(l_config[lab_num - 1]['n_data'])
        means = np.array(l_config[lab_num - 1]['means'])
        covs = np.array(l_config[lab_num - 1]['covs'])
        K = l_config[lab_num - 1]['K']
        init = l_config[lab_num - 1]['init']
        cluster = l_config[lab_num - 1]['cluster']
        optimizer = l_config[lab_num - 1]['optimizer']
        data, labels = generate_data(means, covs, n_data)
    else:
        x_func = l_config[lab_num-1]['x_func']
        y_func = l_config[lab_num-1]['y_func']
        K = l_config[lab_num - 1]['K']
        init = l_config[lab_num - 1]['init']
        cluster = l_config[lab_num - 1]['cluster']
        optimizer = l_config[lab_num - 1]['optimizer']
        data, labels = generate_data_by_func(x_func, y_func)

    if optimizer == 'None':
        model = KMeans(init=init) if cluster == 'KMeans' else GaussianMixture(init=init)
        K, centroids_p, labels_p, errors_log = model.fit(data, K)
    else:
        if cluster == 'KMeans':
            model = ClusteringAlgorithm('KMeans', 'GapsOptimizer')
        else:
            model = ClusteringAlgorithm('GaussianMixture', 'XICOptimizer') 
        K, centroids_p, labels_p, errors_log = model.fit(data)

    plot_fit(cluster, K, data, 
             labels, #means,
             errors_log,
             labels_p, centroids_p, 
             )