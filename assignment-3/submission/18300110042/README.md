# 课程报告
这是`prml-21-spring/assignment-3` 的课程报告，我的代码在 [source.py](source.py) 中。

这次报告的主要内容如下：

1. 聚类算法
    - k-means
    - GMM (Gaussian Mixture Model)

2. 基础实验（给定聚簇数量）
    - 实验设计
    - 实验结果

3. 自动选择聚簇数量的实验
    - 算法
    - 实验设计
    - 实验结果


## 聚类算法
本次报告主要围绕两种聚类算法展开： `k-means` 和 `GMM (Gaussian Mixture Model)` 。

以下分别介绍实验使用的算法。

### k-means
#### 算法简介
`k-means` 是一种无监督的聚类算法，每一类（簇，cluster）以中心点为典型成员，通过迭代的方法，寻找 `cost function` 的局部最小值并收敛（后文 `自动选择聚簇数量-算法-Gap Statistic` 处有进一步说明）。一般使用欧氏距离（`Euclidean Distance`）作为衡量差异的标准，即不同的点到簇中心的距离。


<img src="img/k_means_diagram.png" alt="k_means_diagram" width="270"/>

如图为 `k-means` 算法的流程图。

记样本数据量为 $N$ ， `k-means` 算法的步骤为：
1. 给定 $K$ 个聚簇 $\mathcal{C_k}$，随机初始化对应的簇中心 $c_k, \space k = 1, \cdots, K$ ；

2. 分别计算每个簇中心和每个样本点 $x_i, \space i = 1, \cdots, N$ 的欧式距离的平方，即 $$D = d^2 =  ||x_i - c_j||^2;$$ 

3. 把每个样本 $x_i$ 分配到距离最近的中心点 $c_k$ 所属的簇 $\mathcal{C_k}$ ；

4. 通过以下公式更新簇中心（记 $N_k = |\mathcal{C_k}|$ 为 $\mathcal{C_k}$ 中元素个数）： $$c_k = \frac{\sum_{x_i \in\mathcal{C_k}} x_i}{N_k} ;$$ 

5. 重复以上 `3` 、 `4` 步，直到满足收敛准则，即经过一次迭代后，聚簇中心的变化小于特定的 `threshold` （一个很小的正数 $\epsilon$ ）。

记 $$t = [\sum_{j, j+1} abs(c_{j, j+1})^2]^{\frac{1}{2}} ,$$

其中 $j$ 指前一次迭代的次数，当 $t < \epsilon$ 时，判断模型收敛。

`k-means` 算法相对比较简单，复杂度较低；但执行的是硬分类算法（hard clustering algorithm），灵活度较低，在实行过程中由于数据分布、数据量等原因，可能出现空簇（empty cluster）的情况，需要解决；对随机初始化的簇中心敏感，不同的初始值会导致最终收敛的结果有较大差异；适合分类的数据分布有限；对噪声、离群点敏感。

#### 优化
为了部分解决以上 `k-means` 的一些问题，我们对算法进行了一定的优化：

- 首先，对于第 `1` 步，初始化簇中心，除了 `random` 的方法外，增加了 `k-means++` 的方法，改善簇中心的初始化（参考 [ [Arthur & Vassilvitskii, 2007](https://courses.cs.duke.edu/cps296.2/spring07/papers/kMeansPlusPlus.pdf "k-means++: The Advantages of Careful Seeding") ] ）。

`k-means++` 的步骤为：给定聚簇数量 $K$ ，
1）随机选取一个簇中心 $c_1$ ；

2）根据样本点到已确定的簇中心的距离（欧式距离的平方）计算概率分布，而后根据概率分布抽取下一个簇中心，直到抽取完 $K$ 个簇中心；

具体的，记已确定的簇中心为 $k$ 个，

当 $k=1$ 时，分别计算所有样本点 $x_i$ 到 $c_k$ 的欧式距离的平方（$k=1$），即 $d_{i}^2 = ||x_i - c_1||^2, \space i = 1, \cdots, N$ ，得到不同样本点成为下一个簇中心的概率，即 $p(c_{k+1} = x_i) = \frac{d_{i}^2}{\sum_{i=1}^{N}{d_{i}^2}}$ ，根据这个概率分布抽取下一个簇中心；

当 $k > 1$ 时，分别计算所有样本到已确定的 $k$ 个簇中心的欧式距离的平方，而后令 $d_{i}^2 = \min\{d_{ik}^2\} = \min\{||x_i - c_k||^2\}, \space k = 1,2, \cdots$ ，得到不同样本点成为下一个簇中心的概率，即 $p(c_{k+1} = x_i) = \frac{d_{i}^2}{\sum_{i=1}^{N}{d_{i}^2}}$ ，根据该概率分布抽取下一个簇中心。

当 $k = K$ 时，簇中心的初始化完成。


- 其次，对于 `k-means` 运算过程中出现空簇的问题，在样本量最大的簇中挑选离中心点距离最大的点，作为新的簇中心。


### GMM (Gaussian Mixture Model)
#### 算法简介
相比 `k-means` 算法， `GMM` 是一种软分类算法（soft clustering algorithm），混合模型的每一个子模型可以看作一个有两个参数（均值 `mean` 和（协）方差 `(co)variance`）的高斯生成模型。

`GMM` 模型通过分别估计概率分布参数（均值、（协）方差）进行分类。模型学习参数的方法是 `EM算法` 。

<img src="img/GMM_diagram.png" alt="GMM_diagram" width="270"/>

如图为 `GMM` 算法的流程图。

关于 `GMM` 的算法步骤，首先进行如下定义：

记样本数据量为 $N$ ， $x_i, \space i = 1, \cdots, N$ 为不同的样本点；

$K$ 为混合模型中子高斯模型的数量，不同的子模型分别记为 $\mathcal{C_k}, \space k = 1, \cdots, K$ ；

对于整个模型，其参数可以记为 $\theta = (\boldsymbol{\mu}, \boldsymbol{\Sigma}, \boldsymbol{\alpha})$ ，其中 $\boldsymbol{\mu}, \boldsymbol{\Sigma}$ 包含了每个子模型的两个参数（均值和协方差）； $\boldsymbol{\alpha}$ 指每个子模型在混合模型中发生的概率。

对于子模型 $\mathcal{C_k}$ ，其参数 $\theta_k = (\mu_k, \Sigma_k)$ ，

其中， $\mu_k, \Sigma_k$ 即对应高斯分布 $\mathcal{N}(\mu_k, \Sigma_k)$ 的两个参数，当分类数据为一维时， $\Sigma_k$ 即 $\sigma_k^2$ ；

记 $\alpha_k$ 为子模型 $\mathcal{C_k}$ 发生的概率，即样本数据属于子模型 $\mathcal{C_k}$ 的（先验）概率， $\alpha_k \ge 0, \space \sum_{k=1}^{K} {\alpha_k} = 1$ ；


记 $\phi(x_i|\theta_k)$ 为 $\mathcal{C_k}$ 的高斯概率密度函数（Probability Density Function, `PDF` ），

当样本数据为一维数据时，
$$\phi(x_i|\theta_k) = \frac{1}{\sqrt{2\pi} \sigma} \exp{ \{- \frac{(x_i - \mu)^2}{2 \sigma^2} \} }.$$

当样本数据为多维数据时，记数据维度为 $D$ ，

$$\phi(x_i|\theta_k) = \frac{1}{{(2\pi)}^{\frac{D}{2}} |\Sigma|^{\frac{1}{2}}} \exp{\{- \frac{(x_i - \mu)^T \Sigma^{-1} (x_i - \mu)}{2} \}} ;$$


`GMM` 算法的步骤为：
1. 初始化 $K$ 个子模型 $\mathcal{C_k}$ 的参数， $k = 1, \cdots, K$ ，即均值 $\mu_k$ ，方差 $\sigma_k^2$ （协方差矩阵 $\Sigma_k$ ），以及先验概率密度函数 $\alpha_k$ ；

其中，

1）对 $\mu_k$ 的初始化方法分为两种，（a）随机取 $K$ 个样本作为均值，具体的，将样本数据顺序随机 `shuffle` ，然后取前 $K$ 个；（b）通过 `k-means` 计算得出 $K$ 个簇中心分别作为 $\mu_k, \space k = 1, \cdots, K$ ；

2）对 $\Sigma_k$ 的初始化，由初始化的 $\mu_k$ ，按 `k-means` 相同的方法，将样本分入距离最近的 $\mu_k$ 所在的簇 $\mathcal{C_k}$ ，而后根据该簇的样本估计 $\Sigma_k$ ；

3）对 $\alpha_k$ 的初始化， $\alpha_k = \frac{N_k}{N}$ ，其中 $N_k$ 即属于 $\mathcal{C_k}$ 的样本数量。


2. 对于每个样本 $x_i$ 以及每个子模型 $\mathcal{C_k}$，计算后验分布 $\gamma_{ik} = p(\mathcal{C_k}|x_i)$ ，即 $x_i \in \mathcal{C_k}$ 的概率 （`E-step`）:

$$\begin{align} \gamma_{ik}  &= p(\mathcal{C_k}|x_i) \\\\ &= \frac{p(x_i|\mathcal{C_k}) \cdot p(\mathcal{C_k})}{p(x_i)} \\\\ &= \frac{\phi(x_i|\theta_k) \cdot  \alpha_k}{\sum_{k=1}^{K}{\phi(x_i|\theta_k) \cdot \alpha_k }} ; \end{align}$$

3. 通过最大似然估计更新模型参数 $\theta_k, \space k = 1, \cdots, K$ （`M-step`）:
$$\begin{align} \alpha_k &= \frac{\sum_{i=1}^{N}{\gamma_{ik}}}{N}; \\\\ \mu_k &= \frac{\sum_{i=1}^{N}{\gamma_{ik}} \cdot x_i}{\sum_{i=1}^{N}{\gamma_{ik}}}; \\\\\Sigma_k &= \frac{\sum_{i=1}^{N}{\gamma_{ik}(x_i - \mu_k)(x_i - \mu_k)^T}}{\sum_{i=1}^{N}{\gamma_{ik}}}; \end{align}$$

4. 重复以上 `3` 、 `4` （即 `E-step` 和 `M-step` ），直到满足收敛条件，即定义一个很小的正数 $\epsilon$ 作为 `threshold` ，当 $||\theta_{j+1} - \theta_{j}|| < \epsilon$ 时（其中 $j$ 指前一次迭代的次数），即经过一次迭代时参数变化非常小，判断模型收敛。

`GMM` 是一种软分类方法，相比 `k-means` 更加灵活，能够适应更多不同的数据分布，进行分类；受噪声、离群点影响较小；对初始值相对不敏感；但算法复杂度较高，分类较慢。

#### 优化
`GMM` 算法相对稳定，没有进行很多调整。但当（某一个簇的）数据量不足时，迭代过程中可能出现协方差矩阵不满秩，无法求逆的情况，实际上，这也和模型过拟合有关。针对这一点需要有进一步的处理。

由 $\Sigma_k = \frac{\sum_{i=1}^{N}{\gamma_{ik}(x_i - \mu_k)(x_i - \mu_k)^T}}{\sum_{i=1}^{N}{\gamma_{ik}}}$ 的公式可以看出， $\Sigma_k$ 的秩不会超过 $N_k$ ，即属于第 $k$ 个簇的样本数据量。记 $\Sigma_k$ 的维度，即数据维度为 $D$ ，当 $N_k < D$ ，即数据维度大于数据量时， $\Sigma_k$ 就一定会变成奇异矩阵；当然，只要 $N_k$ 相对较小， $D$ 相对较大时，就有可能出现 $\Sigma_k$ 不满秩的情况。

从抽象的角度看，对于对称阵 $\Sigma_k$ ，需要估计的参数有 $\frac{D(D+1)}{2}$ 个，当 $D$ 比较大的时候，需要估计的参数量也会很大，也就是说，模型复杂度很高。当训练数据不足时，无法确定最优的模型，即出现了过拟合现象。在没有更多数据的情况下，解决过拟合的一个常见方法就是通过正则化降低模型复杂度，或者说，加入某种先验分布。

我们实现的算法中，用 $\hat{\Sigma}_k = \Sigma_k + \lambda I$ 代替了最大似然估计的 $\Sigma_k$ 。

因为 $\Sigma_k$ 一定是半正定矩阵，对于 $\forall v \ne 0$ ，
$$v^T \hat{\Sigma}_k v =v^T (\Sigma_k + \lambda I) v = v^T \Sigma_k v + \lambda v^T I v \ge 0 + \lambda ||v||^2 > 0,$$

即 $\hat{\Sigma}_k$ 一定是正定（满秩）的。

这个方法的合理性可以从贝叶斯推断的角度得出，通过最大后验（ `MAP` , Maximum a posteriori) 的方法估计协方差矩阵 $\Sigma$ ，即对 $\Sigma$ 加入先验的影响（参考 [Regularized Gaussian Covariance Estimation](https://freemind.pluskid.org/machine-learning/regularized-gaussian-covariance-estimation/ "Regularized Gaussian Covariance Estimation") ）。

另外， `GMM` 算法初始化 $\Sigma_k$ 时，可能会出现某一个子模型的样本只有一个的情况，此时无法计算 $\Sigma_k$ （都为 `Nan` ），对此，我们先复制该样本，而后根据以上方法对 $\Sigma_k$ 进行修正。

最后，`k-means` 和 `GMM` 算法都无法避免维度灾难（Curse of Dimensionality）；都需要手动设置聚簇（子模型）的数量，除非和别的算法结合。以及， `EM算法` 虽然保证收敛，但得到的依然是局部最小值。

## 基础实验

### 实验设计

之前提到 `k-means` 虽然保证收敛，但有一些缺陷，一是算法本身依赖初始簇中心设置，不同的簇中心会导致最终收敛的结果不同，缺乏一致性；二是关于适用的数据方面，对大小不同/密度不同；不同分布的数据适应情况不同；对离群点敏感；以及维度灾难问题。

在实验中，我们希望探究 `k-means` 的这些缺点，可能的解决方案，如 `k-means++` ，以及 `GMM` 相对 `k-means` 的优势。

- 对于初始簇中心的设置，探究随机设置的初始值和 `k-means++` 最终得到的分类结果有什么不同；以及不同的初始参数对 `GMM` 算法的影响（主要是随机设置的均值 $\boldsymbol{\mu}$ 和通过 `k-means` 设置的均值 $\boldsymbol{\mu}$ ）。

- 关于数据大小/密度的影响，探究 `k-means` 和 `GMM` 算法在类别之间平衡/不平衡的数据集上的表现。

- 关于数据分布的影响，设置不同的数据分布，探究 `k-means` 和 `GMM` 算法对于不同分布的数据的分类能力。包括不同组数据服从相同/不同的分布；协方差矩阵为对角阵（对角阵的值相同/不同）；协方差矩阵为一般的（半）正定矩阵的高斯分布；均匀分布等。

- 关于离群点敏感，由于如何判断某个样本是离群点还是新的类别本身就依赖领域知识的判断；对离群点的处理方式一般包括去除（`remove`）或裁剪（`clip`），也需要领域知识，在本次实验中不进行处理。

- 关于维度灾难问题，一般采用 `PCA` 等降维方式或通过别的方法提高分类算法效率，实验中不进行处理。

关于聚类的评价，

- 对于 `k-means` 算法，采用 
$$W_K = \sum_{k=1}^{K} {\sum_{x_i \in \mathcal{C_k}} ||x_i - \mu_k||^2}$$
作为 `loss function` ，评价结果。

- 对于 `GMM` 算法，采用 `negative log likelihood` ，即 $- \log \hat{L}$ 作为 `loss function` ，评价结果。

### 实验结果

#### 关于初始簇中心（均值 $\boldsymbol{\mu}$ ）的设置

为了避免其他因素的影响，选择样本量平衡的数据集，通过高斯分布生成数据进行实验。

a. 根据以下高斯分布生成三组数据：

|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples      |   
|   :----:  | :------------:  |  :------------:   |  :-------:   |
| $1$ | $\begin{bmatrix} 1 & 2 \end{bmatrix}$ | $\begin{bmatrix} 70 & 0 \\\\ 0 & 22 \end{bmatrix}$ | $500$ |
| $2$ | $\begin{bmatrix} 16 & -5 \end{bmatrix}$ | $\begin{bmatrix} 21 & 0 \\\\ 0 & 32 \end{bmatrix}$ | $500$ |
| $3$ | $\begin{bmatrix} 10 & 22 \end{bmatrix}$ | $\begin{bmatrix} 10 & 5 \\\\ 5 & 10 \end{bmatrix}$ | $500$ |

以下为随机设置初始簇中心的 `k-means` 算法的结果：

<img src="img/lab1_1.png" alt="lab1_1" width="600"/>

<img src="img/lab1_2.png" alt="lab1_2" width="300"/>

> lab_number = 1


以下为使用 `k-means++` 设置初始簇中心的 `k-means` 算法结果：

<img src="img/lab2_1.png" alt="lab2_1" width="600"/>

<img src="img/lab2_2.png" alt="lab2_2" width="300"/>

> lab_number = 2



以下为随机设置初始均值的 `GMM` 算法的结果：

<img src="img/lab3_1.png" alt="lab3_1" width="600"/>

<img src="img/lab3_2.png" alt="lab3_2" width="300"/>

> lab_number = 3


以下为根据 `k-means` 算法设置初始均值的 `GMM` 算法的结果：

<img src="img/lab4_1.png" alt="lab4_1" width="600"/>

<img src="img/lab4_2.png" alt="lab4_2" width="300"/>

> lab_number = 4


对于实验中的高斯分布， `k-means` 和 `GMM` 算法最终聚类结果受初始值的影响并不是很大，但更好的初始值，即 `k-means` 中的 `k-means++` ，以及 `GMM` 中利用 `k-means` 设置初始均值 $\boldsymbol{\mu}$ 可以加快算法的收敛。

以后的实验中，对于 `k-means` 使用 `k-means++` 进行初始化； `GMM` 利用 `k-means` 算法初始化。

#### 关于数据大小/密度的影响

对平衡和不平衡的数据集的分类结果进行比较。

a. 根据以下高斯分布生成三组数据，控制类别数量。

|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples      |   
|   :----:  | :------------:  |  :------------:   |  :-------:   | 
| $1$ | $\begin{bmatrix} 1 & 5 \end{bmatrix}$ | $\begin{bmatrix} 70 & 0 \\\\ 0 & 70 \end{bmatrix}$ | $500$ |
| $2$ | $\begin{bmatrix} 15 & 10\end{bmatrix}$ | $\begin{bmatrix} 30 & 0 \\\\ 0 & 30 \end{bmatrix}$ | $500$ | 
| $3$ | $\begin{bmatrix} 10 & 20 \end{bmatrix}$ | $\begin{bmatrix} 50 & 0 \\\\ 0 & 50 \end{bmatrix}$ | $500$ | 

以下为样本数据平衡时 `k-means` 算法的结果：

<img src="img/lab5_1.png" alt="lab5_1" width="600"/>

<img src="img/lab5_2.png" alt="lab5_2" width="300"/>

> lab_number = 5

以下为样本数据平衡时 `GMM` 算法的结果：

<img src="img/lab6_1.png" alt="lab6_1" width="600"/>

<img src="img/lab6_2.png" alt="lab6_2" width="300"/>

> lab_number = 6

|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples     |    
|   :----:  | :------------:  |  :------------:   |  :-------:   | 
| $1$ | $\begin{bmatrix} -10 & 50 \end{bmatrix}$ | $\begin{bmatrix} 50 & 25 \\\\ 25 & 40 \end{bmatrix}$ | $1000$ |
| $2$ | $\begin{bmatrix} 30 & 10\end{bmatrix}$ | $\begin{bmatrix} 30 & 20 \\\\ 20 & 60 \end{bmatrix}$ |$400$ |
| $3$ | $\begin{bmatrix} 10 & 20 \end{bmatrix}$ | $\begin{bmatrix} 20 & 0 \\\\ 0 & 50 \end{bmatrix}$ | $100$ |

以下为样本数据不平衡时 `k-means` 算法的结果：

<img src="img/lab7_1.png" alt="lab7_1" width="600"/>

<img src="img/lab7_2.png" alt="lab7_2" width="300"/>

> lab_number = 7

以下为样本数据不平衡时 `GMM` 算法的结果：

<img src="img/lab8_1.png" alt="lab8_1" width="600"/>

<img src="img/lab8_2.png" alt="lab8_2" width="300"/>

> lab_number = 8

可以看出，`k-means` 算法对于不平衡的数据集聚类效果较差，而 `GMM` 可以更好的解决这个问题。

#### 关于数据分布的影响

##### 高斯分布

首先，探究两种算法对于高斯分布的聚类效果。

高斯分布产生的数据按协方差矩阵可以分为三类： `spherical` ， `diagonal` ， `full` 。

a. `spherical` ，即不同簇类有自己的方差。

根据以下高斯分布生成三组数据，每组 $500$ 个样本，共 $1500$ 个：

|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples      |   
|   :----:  | :------------:  |  :------------:   |  :-------:  |
| $1$ | $\begin{bmatrix} 1 & 30 \end{bmatrix}$ | $\begin{bmatrix} 50 & 0 \\\\ 0 & 50 \end{bmatrix}$ | $500$ |
| $2$ | $\begin{bmatrix} 15 & 10 \end{bmatrix}$ | $\begin{bmatrix} 30 & 0 \\\\ 0 & 30 \end{bmatrix}$ | $500$ |
| $3$ | $\begin{bmatrix} 10 & 20 \end{bmatrix}$ | $\begin{bmatrix} 50 & 0 \\\\ 0 & 50 \end{bmatrix}$ | $500$ |


<img src="img/lab9_1.png" alt="lab9_1" width="600"/>

<img src="img/lab9_2.png" alt="lab9_2" width="300"/>

> lab_number = 9


<img src="img/lab10_1.png" alt="lab10_1" width="600"/>

<img src="img/lab10_2.png" alt="lab10_2" width="300"/>

> lab_number = 10


b. `diagonal` ，即不同簇类的协方差均为对角阵。

根据以下高斯分布生成三组数据：

|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples      |   
|   :----:  | :------------:  |  :------------:   |  :-------:  |
| $1$ | $\begin{bmatrix} 1 & 30 \end{bmatrix}$ | $\begin{bmatrix} 30 & 0 \\\\ 0 & 20 \end{bmatrix}$ | $500$ |
| $2$ | $\begin{bmatrix} 15 & 10 \end{bmatrix}$ | $\begin{bmatrix} 60 & 0 \\\\ 0 & 50 \end{bmatrix}$ | $500$ |
| $3$ | $\begin{bmatrix} 10 & 20 \end{bmatrix}$ | $\begin{bmatrix} 70 & 0 \\\\ 0 & 30 \end{bmatrix}$ | $500$ |


<img src="img/lab11_1.png" alt="lab11_1" width="600"/>

<img src="img/lab11_2.png" alt="lab11_2" width="300"/>

> lab_number = 11

<img src="img/lab12_1.png" alt="lab12_1" width="600"/>

<img src="img/lab12_2.png" alt="lab12_2" width="300"/>

> lab_number = 12



c. `full` ，即不同簇类的协方差矩阵中的（ $\frac{D(D+1)}{2}$ 个）参数都为自由设定的（ $D$ 为样本数据维度）。

根据以下高斯分布生成三组数据：

|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples    |  
|   :----:  | :------------:  |  :------------:   |  :-------:   | 
| $1$ | $\begin{bmatrix} 1 & 30 \end{bmatrix}$ | $\begin{bmatrix} 50 & 25 \\\\ 25 & 40 \end{bmatrix}$ | $500$ |
| $2$ | $\begin{bmatrix} 15 & 10\end{bmatrix}$ | $\begin{bmatrix} 30 & -20 \\\\ -20 & 60 \end{bmatrix}$ | $500$ |
| $3$ | $\begin{bmatrix} 10 & 20 \end{bmatrix}$ | $\begin{bmatrix} 20 & -15 \\\\ -15 & 50 \end{bmatrix}$ | $500$ | 

<img src="img/lab13_1.png" alt="lab13_1" width="600"/>

<img src="img/lab13_2.png" alt="lab13_2" width="300"/>

> lab_number = 13

<img src="img/lab14_1.png" alt="lab14_1" width="600"/>

<img src="img/lab14_2.png" alt="lab14_2" width="300"/>

> lab_number = 14


##### 其他分布

均匀分布

|   Cluster No.        |    Distribution    |  Number of samples  |    
|   :------:  | :---------------------------------:  |  :-------:  |
| $1$  | $f(x,y) = \begin{cases} \frac{1}{20}, && 0 \le x \le 20, \space y = 5 \\\\ 0, && \mathrm{else} \end{cases}$  |  $500$  |
| $2$  | $f(x,y) = \begin{cases} \frac{1}{25}, && 3 \le x \le 28, \space y = 2 \\\\ 0, && \mathrm{else} \end{cases}$  |  $500$  |


<img src="img/lab15_1.png" alt="lab15_1" width="600"/>

<img src="img/lab15_2.png" alt="lab15_2" width="300"/>

> lab_number = 15


<img src="img/lab16_1.png" alt="lab16_1" width="600"/>

<img src="img/lab16_2.png" alt="lab16_2" width="300"/>

> lab_number = 16



|   Cluster No.        |    Distribution    |  Number of samples  |    
|   :------:  | :---------------------------------:  |  :-------:  |
| $1$  | $X \sim U(0, \pi) \\\\ Y = \sin(X) + 3$  |  $500$  |
| $2$  | $X \sim U(0, \pi) \\\\ Y = 3 \sin(X) - 2$  |  $500$  |



<img src="img/lab17_1.png" alt="lab17_1" width="600"/>

<img src="img/lab17_2.png" alt="lab17_2" width="300"/>

> lab_number = 17


<img src="img/lab18_1.png" alt="lab18_1" width="600"/>

<img src="img/lab18_2.png" alt="lab18_2" width="300"/>

> lab_number = 18


对于以上不服从高斯分布的，不同数据类别之间的“距离”对于聚类的准确性影响更大，当该“距离”较大时， `k-means` 算法用 `k-means++` 初始化后都不需要进一步迭代； `GMM` 算法的迭代次数也很少。

### 小结
- `k-means` 算法的优点是算法复杂度较低，效率较高；缺点是难以处理不平衡数据集；适应的数据分布没有 `GMM` 算法多；以及初始值的设置对 `k-means` 比较重要。

- `GMM` 算法复杂度较高；解决了部分 `k-means` 的问题，包括处理不平衡数据集、适应更多不同的数据分布；但是在实验中发现，（相比原始类别数据，） `GMM` 的分类结果并不一定比 `k-means` 好。

- `k-means` 聚类边界明显（硬分类）；`GMM` 的聚类边界较为平滑（软分类）。

- 对于重合度较高的分布，`k-means` 和 `GMM` 基本都不会有很好的聚类效果，在实际使用中，需要根据具体情况，考虑增大不同组之间的距离（如优化特征提取）。

- 在实验中设置的数据分布没有体现出不同的初始值对聚类算法结果的很大影响，但更好的初始值 （如 `k-means++` 和 `GMM` 中利用 `k-means` 设置初始均值）能够提高收敛的效率。


## 自动选择聚簇数量的实验
以上部分的实验都事先给定了聚簇数量，而实际情况中可能不能手动确定聚簇数量，需要自动选择聚簇数量的方法。这可以看作对以上聚类算法的进一步优化。

### 算法

#### Gap statistic
直觉上，聚类算法的目标是最大化类内的相似度，同时使得类间的相似度最低（最大化类间的 dissimilarity ）。因而最优的聚簇是有一定主观性的，依赖于衡量“相似度”的方法，以及需要的分类的精细度。一种常用的衡量“相似度”的方法就是欧式距离。

根据欧氏距离，可以通过 

$$D_k = \sum_{x_i \in \mathcal{C_k}} \sum_{x_j \in \mathcal{C_k}} ||x_i - x_j||^2 = 2 N_k \sum_{x_i \in \mathcal{C_k}} ||x_i - \mu_k||^2$$

衡量某一聚簇 $\mathcal{C_k}$ 内部的总距离，其中， $N_k = |\mathcal{C_k}|$ 为 $\mathcal{C_k}$ 内部元素总个数， $\mu_k$ 为簇均值， $x_i, \space x_j$ 为簇内样本。

令 
$$W_K = \sum_{k=1}^{K} {\frac{1}{2 N_k} D_k} ,$$

通过加入标准项 $\frac{1}{2 N_k}$ ，而后计算所有聚簇内部总距离之和，可以用 $W_K$ 衡量聚类的 `compactness` ， $W_K$ 越小，聚簇结果越好。（ `k-means` 算法中，给定聚簇数量 $K$ ，可以把 $W_K$ 看作最小化的目标，即 `cost function`。）

由于
$$\begin{align} W_K &= \sum_{k=1}^{K} {\frac{1}{2 N_k} D_k} \\\\ &= \sum_{k=1}^{K} {\frac{1}{2 N_k} \cdot (2 N_k \sum_{x_i \in \mathcal{C_k}} ||x_i - \mu_k||^2)} \\\\ &= \sum_{k=1}^{K} {\sum_{x_i \in \mathcal{C_k}} ||x_i - \mu_k||^2} , \end{align}$$
具体实现中，直接通过 $W_K = \sum_{k=1}^{K} {\sum_{x_i \in \mathcal{C_k}} ||x_i - \mu_k||^2}$ 计算 $W_K$ 。

`Gap statistic` 方法通过将一个合适的零分布作为参考（`an appropriate reference null distribution`），比较聚类算法得出的结果，选择最优的聚簇数量 `K` ，即相对的 $W_K$ 最小。

具体的，构造 `gap statstic` ，选择使之最大的 `K` 作为聚簇数量。

`gap statistic` 的定义如下：

$$\text{Gap}_{N}(K) = E\_{N}^{*}\\{\log{(W_K)}\\} - \log{(W_K)};$$

其中 $E_{N}^{*}$ 是指零分布下的，样本数为 $N$ 的期望。（可以看出，论文中提到的方法并没有对聚类算法以及零分布的选择有任何限制。）

记最佳聚簇数量为 $K_b$ ，当选择的聚簇数量 $K \le K_b$ 时， $\log{(W_K)}$ 的下降速率会大于其期望的下降速率；当 $K > K_b$ 时，实际上是增加了一个不必要的簇中心， $\log{(W_K)}$ 的下降速率会小于其期望，因而，当 $K = K_b$ 时， `gap statistic` 会达到最大值。

具体实现的过程中，

- 关于作为参考的零分布（`reference distribution`），选择了论文中较为简单的方法，即根据样本数据每个特征维度范围内的均匀分布产生数据，即 $f\_{d}^* \sim U(\min(f\_{d}), \max(f\_d))$ ，其中 $f\_d$ 指 $d$ 维度上的特征值； $f\_{d}^{*}$ 为根据参考分布产生的模拟数据。

- 关于对 $E\_{N}^{*} \\{\log(W\_K)\\}$ 的估计，采用 `Monte Carlo` 算法，根据参考分布产生 $N$ 个样本数据 $X\_{1}^{\*}, \cdots, X_{N}^{\*}$ ，计算 $\log(W\_{K}^{\*})$ ，重复 $B$ 次，取平均值（具体的，在实验中令 $B = 10$ ）。

- 另外，还要考虑模拟产生的误差，记 $B$ 次 `Mote Carlo` 模拟产生的 $\log(W\_{K}^{*})$ 的标准差为 $\text{sd}(K)$ ，最后考虑的误差则为 
$$s_K = \text{sd}(K) \sqrt{1 + \frac{1}{B}} .$$


算法步骤：

1. 根据给定算法以及聚簇数量 $K$ 分别对样本数据进行聚类（$K = 1, 2, \cdots, K_{max}$），并且计算 $W_K$ ；

2. 根据参考分布生成 $B$ 组数据，根据每组数据计算 $W_{Kb}^{*}, \space b = 1,2,\cdots, B, \space K = 1,2,\cdots,K_{max}.$ 而后计算 `gap statistic` ：

$$\text{Gap}(K) = \frac{1}{B} \sum_{b=1}^{B} {\log{(W_{Kb}^{*})}} - \log{(W_K)} .$$

3. 令 $\bar{l} = \frac{1}{B} \sum_{b=1}^{B} {\log{(W_{Kb}^{*})}}$ ，计算标准差 

$$\text{sd}_K = [\frac{1}{B} \sum\_{b=1}^{B} \\{\log(W\_{Kb}^{*}) - \bar{l}\\}^2]^{\frac{1}{2}} ,$$

令 $s_K = \text{sd}(K) \sqrt{1 + \frac{1}{B}}$ ，选择

$$\hat{K} = \text{smallest } K \text{ such that } \text{Gap}(K) \ge \text{Gap}(K+1) - s_{K+1}.$$
作为聚簇数量。

（参考 [ [Tibshirani et al., 2001](https://statweb.stanford.edu/~gwalther/gap "Estimating the number of clusters in a data set via the gap statistic") ] ）

由于 `GMM` 算法本身复杂度较高，而 `gap statistic` 的方法又需要多次模拟、计算聚类结果，因而这两者的结合会导致算法运行很慢。后文将 `k-means` 和 `gap statistic` 结合进行实验。

#### Information Criterions (ICs)
`ICs` 是一种选择模型的准则，可以很容易的和基于最大似然的模型结合起来。

一般，随着模型复杂度的提升，模型对数据的拟合能力（ `maximum likelihood` ）也会提升，但会出现过拟合的问题。 `ICs` 通过引入对模型复杂度的惩罚项，希望得到最合适的模型，即在模型对数据的拟合能力与较小的模型复杂度之间达到平衡。

##### Akaike Information Criterion (AIC)
`AIC` 的定义是

$$\text{AIC} = 2K - 2 \log(\hat{L}) ,$$

其中， $K$ 为模型的自由参数量（`number of free parameters`）； $\hat{L}$ 是模型估计的最大化的似然函数值。

一般选择 `AIC` 值更小的模型。

`AIC` 通过引入对模型复杂度（模型参数量）的惩罚项，平衡模型的拟合能力与（较小的）模型复杂度。

一般 `AIC` 在聚类算法中的效果不是很好 [ [Nylund et al., 2007](https://www.semanticscholar.org/paper/Deciding-on-the-Number-of-Classes-in-Latent-Class-A-Nylund-Asparouhov/439672067967d3501b0d201bdb50a60886459b00 "Deciding on the Number of Classes in Latent Class Analysis and Growth Mixture Modeling: A Monte Carlo Simulation Study") ] ，由于惩罚力度不够，倾向选择过拟合的模型。

##### Bayesian Information Criterion (BIC)
`BIC` 选择的模型是使得

$$\log(\hat{L}) - \frac{1}{2} K \log{(N)} $$

最大的模型。其中， $K$ 为模型的自由参数量（`number of free parameters`）； $N$ 为样本数据量； $\hat{L}$ 是模型估计的最大化的似然函数值。 [ [Schwarz, 1978](http://www.jstor.org/stable/2958889 "Estimating the Dimension of a Model") ]


相比 `Akaike Information Criterion (AIC)` ， `BIC` 对模型复杂度（模型参数量）的惩罚力度更大，且考虑了样本数据量，当数据量较大时对模型参数惩罚更多，倾向于参数较少的简单模型。

在实际计算中，采用 
$$\text{BIC} = K \ln (N)-2 \ln (\hat{L}) ,$$
选择 `BIC` 值更小的模型。

在后文中将 `BIC` 与 `GMM` 结合进行实验。

##### 算法实现
在具体的实现过程中，

记样本数据量为 $N$ ，样本数据维度为 $D$ ，聚簇（子模型）数量为 $K$ ，

模型的自由参数量即为各聚簇中心（子模型的均值） $\boldsymbol{\mu}$ 、协方差矩阵 $\boldsymbol{\Sigma}$ 以及各子模型概率 $\boldsymbol{\alpha}$ 中的自由参数，

- 聚簇中心的自由参数量为 $K \cdot D$ ；

- 协方差矩阵的自由参数量为 $K \cdot \frac{D(D+1)}{2}$ ；

- 子模型概率的自由参数量为 $K - 1$ （ $\sum_{k=1}^{K} \alpha_k = 1$ ）。

对数最大似然 $\log (\hat{L})$ 为

$$\log (\hat{L}) = \sum_{i=1}^{N} \log P(x_i|\theta) = \sum_{i=1}^{N} \log[\sum_{k=1}^{K} \alpha_k \phi (x_i|\theta_k)] .$$

在分类过程中参考 [ [Pelleg & Moore, 2000](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.19.3377 "X-means: Extending K-means with Efficient Estimation of the Number of Clusters") ] ，

通过循环的方式不断进行分类：先将所有样本看作一个聚簇，而后根据 `k-means` 设置两个中心点，计算分成两簇后的 `IC` 值是否比一簇的小，如果 `IC` 值没有下降，则不分为两簇；若 `IC` 值下降，则分成两簇，而后对每一簇内部继续进行循环。直到 $K$ 达到设定的 $K_{max}$ 或 `IC` 值不再下降，结束循环。


### 实验

实验探究以上两种选择聚簇数量的方法： 

1. `k-means` + `gap statistic` ；
2. `GMM` + `BIC` ；


|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples       |    
|   :----:  | :------------:  |  :------------:   |  :-------:   | 
| $1$ | $\begin{bmatrix} -10 & 50 \end{bmatrix}$ | $\begin{bmatrix} 50 & 25 \\\\ 25 & 40 \end{bmatrix}$ | $500$ |
| $2$ | $\begin{bmatrix} 30 & 10\end{bmatrix}$ | $\begin{bmatrix} 30 & 20 \\\\ 20 & 60 \end{bmatrix}$ | $500$ | 
| $3$ | $\begin{bmatrix} 10 & 20 \end{bmatrix}$ | $\begin{bmatrix} 20 & 0 \\\\ 0 & 50 \end{bmatrix}$ | $500$ | 

以下为 `k-means + gap statistic` 的表现：

<img src="img/lab19_1.png" alt="lab19_1" width="600"/>

<img src="img/lab19_2.png" alt="lab19_2" width="300"/>


> lab_number = 19

以下为 `GMM + BIC` 的表现：

<img src="img/lab20_1.png" alt="lab20_1" width="600"/>

<img src="img/lab20_2.png" alt="lab20_2" width="300"/>


> lab_number = 20


|   Cluster No.        |  $\boldsymbol{\mu}$  |   $\boldsymbol{\Sigma}$    |    Number of samples       |    
|   :----:  | :------------:  |  :------------:   |  :-------:   | 
| $1$ | $\begin{bmatrix} -100 & 50 \end{bmatrix}$ | $\begin{bmatrix} 50 & 25 \\\\ 25 & 40 \end{bmatrix}$ | $500$ |
| $2$ | $\begin{bmatrix} 30 & 10\end{bmatrix}$ | $\begin{bmatrix} 30 & 20 \\\\ 20 & 60 \end{bmatrix}$ | $500$ | 
| $3$ | $\begin{bmatrix} 10 & 20 \end{bmatrix}$ | $\begin{bmatrix} 20 & 0 \\\\ 0 & 50 \end{bmatrix}$ | $500$ | 

修改第 `1` 组均值 $\boldsymbol{\mu}$ 后， `GMM + BIC` 的表现有所提升：

<img src="img/lab21_1.png" alt="lab21_1" width="600"/>

<img src="img/lab21_2.png" alt="lab21_2" width="300"/>

## 总结
- `GMM` 算法理论上聚类效果优于 `k-means` 算法，但效率较低；面对重合度较高的数据，以及非高斯分布的其他数据，按照给定的类别划分，效果没有特别好。不同数据类别之间的距离起到的影响更大。在实际应用中可以考虑更好的提取特征，拉大不同数据分布之间的“距离”。

- 更好的初始参数设置能够提高算法收敛的速度；其对模型聚类结果的影响有待进一步的探究。

- 自动寻找聚簇数量的算法表现与数据分布的关联较大，当数据分布较远时效果较好。具体情况可以进一步探究。可能直接采用 `x-means` [ [Pelleg & Moore, 2000](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.19.3377 "X-means: Extending K-means with Efficient Estimation of the Number of Clusters") ] 的算法会达到较好的结果。


## Reference

[1]. [Arthur, D., & Vassilvitskii, S. (2007). k-means++: The advantages of careful seeding. SODA ’07.](https://courses.cs.duke.edu/cps296.2/spring07/papers/kMeansPlusPlus.pdf "k-means++: The Advantages of Careful Seeding")

[2]. [Nylund, K. L., Asparouhov, T., & Muthén, B. O. (2007). Deciding on the Number of Classes in Latent Class Analysis and Growth Mixture Modeling: A Monte Carlo Simulation Study. Structural Equation Modeling: A Multidisciplinary Journal, 14(4), 535–569.](https://doi.org/10.1080/10705510701575396 "Deciding on the Number of Classes in Latent Class Analysis and Growth Mixture Modeling: A Monte Carlo Simulation Study") 

[3]. [Pelleg, D., & Moore, A. (2000). X-means: Extending K-means with Efficient Estimation of the Number of Clusters. In Proceedings of the 17th International Conf. on Machine Learning, 727–734.](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.19.3377 "X-means: Extending K-means with Efficient Estimation of the Number of Clusters")

[4]. [Schwarz, G. (1978). Estimating the Dimension of a Model. The Annals of Statistics, 6(2), 461-464.](http://www.jstor.org/stable/2958889 "Estimating the Dimension of a Model")

[5]. [Tibshirani, R., Walther, G., & Hastie, T. (2001). Estimating the Number of Clusters in a Data Set via the Gap Statistic. Journal of the Royal Statistical Society. Series B (Statistical Methodology), 63(2), 411–423.](https://statweb.stanford.edu/~gwalther/gap "Estimating the number of clusters in a data set via the gap statistic")

[6]. [Regularized Gaussian Covariance Estimation](https://freemind.pluskid.org/machine-learning/regularized-gaussian-covariance-estimation/ "Regularized Gaussian Covariance Estimation")


## 代码使用方法
```
python source.py lab_number #lab_number = 1, 2, ..., 20
    e.g., python source.py 1
```