import numpy as np
import matplotlib.pyplot as plt


class KMeans:

    def __init__(self, n_clusters, init_type="kmeans++", max_iter=200):
        '''
        模型初始化，得到聚类的数目k
        '''
        self.k = n_clusters
        self.cluster_center = None
        self.init_type = init_type
        self.max_iter = max_iter

    def init_centers(self, train_data):
        K = self.k
        N, D = train_data.shape
        temp = train_data.tolist()

        if self.init_type == 'random':
            # 初始化聚类中心：随机初始化
            random_list = np.random.choice(N, K, replace=False).tolist()
            self.cluster_center = train_data[random_list, :]

        else:
            # 初始化聚类中心：使用K-means++，让中心点尽可能的远
            cluster_centers = []

            # 将train_data中随机一个点选取为中心点
            cluster_centers.append(temp[np.random.randint(N)])
            cluster_centers = np.array(cluster_centers)

            for i in range(1, K):
                distances = []
                for row in train_data:
                    distances.append(np.min(np.linalg.norm(
                        row - cluster_centers, axis=1)))  # 与最近一个聚类中心的距离

                # 轮盘法选出下一个聚类中心；
                total = sum(distances)*np.random.rand()
                for j in range(N):
                    total -= distances[j]
                    if total < 0:
                        cluster_centers = np.append(
                            cluster_centers, [train_data[j]], axis=0)
                        break

            self.cluster_center = cluster_centers

    def fit(self, train_data):
        '''
        通过训练集拟合模型
        '''
        K = self.k
        N = train_data.shape[0]
        self.init_centers(train_data)
        cluster_centers = self.cluster_center

        labels = np.ones(N)

        for i in range(self.max_iter):
            distances = []
            get_new_label = []
            #分配步：
            for i in range(N):
                distances = (np.linalg.norm(
                    train_data[i]-cluster_centers, axis=1))
                label = np.argsort(distances)[0]
                get_new_label.append(label)

            labels_new = np.array(get_new_label)

            if (sum(np.abs(labels-labels_new))) == 0:
                break
            else:
            #更新步：
                labels = labels_new
                for i in range(K):
                    cluster_centers[i] = np.mean(
                        train_data[labels == i], axis=0)

        self.cluster_center = cluster_centers
        return labels_new

    def predict(self, test_data):
        return self.fit(test_data)


def multi_norm(x, mu, sigma):
    '''
    返回多维高斯分布的结果
    x: 输入数据 [d1,d2,d3,.....,dn]
    mu: 高斯分布的均值
    sigma: 协方差矩阵
    '''
    det = np.linalg.det(sigma)
    inv = np.matrix(np.linalg.inv(sigma))
    x_mu = np.matrix(x - mu).T

    const_item = 1 / (((2 * np.pi) ** (len(x) / 2))
                      * (det ** (0.5)))             # 高斯公式中的常数项
    exp_item = -0.5 * x_mu.T * inv * x_mu           # 高斯公式中的指数项

    return float(const_item * (np.exp(exp_item)+1e-8))


class GaussianMixture:

    def __init__(self, n_clusters, max_iter=200, init_params="kmeans"):
        '''
        模型初始化，得到聚类的数目k
        '''
        self.k = n_clusters
        self.max_iter = max_iter
        self.init_method = init_params

        self.pi = None
        self.means = None
        self.covs = None

    def init_params(self, train_data):
        k = self.k
        N, d = train_data.shape
        self.covs = np.empty((k, d, d))
        for i in range(k):
            self.covs[i] = np.eye(d)*np.random.rand(1)*k

        if self.init_method == "random":
            # 随机初始化GMM的参数

            self.pi = np.random.rand(k)
            self.pi = self.pi/self.pi.sum()
            self.means = np.random.rand(k, d) * np.max(train_data, axis=0)

        else:
            # 通过Kmeans初始化GMM的参数

            kmeans_model = KMeans(k)
            kmeans_model.fit(train_data)
            KM_label = kmeans_model.predict(train_data)

            self.means = kmeans_model.cluster_center

            self.pi = []
            for i in range(k):
                temp = np.zeros(KM_label.shape)
                temp[KM_label == i] = 1
                self.pi.append(np.sum(temp)/N)

    def fit(self, train_data):
        '''
        通过训练集对模型进行拟合
        '''
        k = self.k
        N, d = train_data.shape

        # self.means = np.random.rand(k, d) * np.max(train_data, axis=0)
        # self.covs = np.empty((k, d, d))
        # for i in range(k):
        #     self.covs[i] = np.eye(d)*np.random.rand(1)*k
        self.init_params(train_data)

        for _ in range(self.max_iter):

            # E步：固定参数，计算x_n属于第k个高斯分布的后验概率γ_nk
            posterior = []
            for i in range(N):
                q_i = []
                for j in range(k):
                    postprob = multi_norm(
                        train_data[i], self.means[j], self.covs[j])
                    q_i.append(self.pi[j]*postprob)

                posterior.append(q_i)
            posterior = np.array(posterior)

            posterior = posterior / posterior.sum(axis=1, keepdims=True)

            # M步：固定γ_nk，计算更新后的π_K,means,covs
            pi_hat = posterior.sum(axis=0)
            means_hat = np.tensordot(posterior, train_data, axes=[0, 0])

            covs_hat = np.empty(self.covs.shape)
            for i in range(k):
                tmp = train_data - self.means[i]
                covs_hat[i] = np.dot(tmp.T*posterior[:, i], tmp) / pi_hat[i]

            self.covs = covs_hat
            self.means = means_hat/pi_hat.reshape(-1, 1)
            self.pi = pi_hat/N

    def predict(self, test_data):
        posterior = []
        for i in range(test_data.shape[0]):
            q_i = []
            for j in range(self.k):
                postprob = multi_norm(
                    test_data[i], self.means[j], self.covs[j])
                q_i.append(self.pi[j]*postprob)
            posterior.append(q_i)

        posterior = np.array(posterior)
        labels = np.argmax(posterior, axis=1)

        return labels


class ClusteringAlgorithm:

    def __init__(self, Max_k=10):
        self.Max_K = Max_k
        self.k = 2
        self.Model = None
        self.SSE = None

    def fit(self, train_data):
        #计算每个K值对应的SSE
        SSE_list = []
        for i in range(1, self.Max_K):
            KMeans_model = KMeans(i)
            KMeans_model.fit(train_data)
            cents = KMeans_model.cluster_center
            distances = []
            for row in train_data:
                distances.append(np.min(np.linalg.norm(
                    row - cents, axis=1)))  # 与最近一个聚类中心的距离
            SSE_list.append(sum(distances))
        
        #计算相邻K值间的SSE之差
        self.SSE = SSE_list
        diff_SSE = []
        for i in range(self.Max_K-2):
            diff_SSE.append(abs(SSE_list[i]-SSE_list[i+1]))
        #获得best_K
        Max_change_rate = 0
        best_k = 2
        for i in range(self.Max_K-3):
            if float((diff_SSE[i] - diff_SSE[i+1])/diff_SSE[i+1]) > Max_change_rate:
                Max_change_rate = float(
                    (diff_SSE[i] - diff_SSE[i+1])/diff_SSE[i+1])
                best_k = i+2

        self.k = best_k
        self.Model = KMeans(self.k)

        #画出SSE图
        X = range(1, 10)
        plt.xlabel('k')
        plt.ylabel('SSE')
        plt.plot(X, SSE_list, 'o-')
        plt.show()

    def predict(self, test_data):
        return self.Model.predict(test_data)

########################
#  以下为实验部分的代码 #
########################


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_1():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data, label = shuffle(x, y, z)
    return (data, data), label, 3


def data_2():
    mean = (0, 0)
    cov = np.array([[40, 0], [0, 40]])
    x = np.random.multivariate_normal(mean, cov, (750,))

    mean = (0, 15)
    cov = np.array([[10, 0], [0, 10]])
    y = np.random.multivariate_normal(mean, cov, (150,))

    mean = (20, 0)
    cov = np.array([[10, 0], [0, 10]])
    z = np.random.multivariate_normal(mean, cov, (150,))

    mean = (0, -20)
    cov = np.array([[10, 0], [0, 10]])
    w = np.random.multivariate_normal(mean, cov, (150,))

    mean = (-20, 0)
    cov = np.array([[10, 0], [0, 10]])
    t = np.random.multivariate_normal(mean, cov, (150,))
    data, labels = shuffle(x, y, z, w, t)
    return (data, data), labels, 5

#绘图函数
def display(data, label, cluster):
    datas = []
    for k in range(cluster):
        data_k = []
        for i in range(len(data)):
            if label[i] == k:
                data_k .append(data[i])
        datas.append(data_k)

    for each in datas:
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1])
    plt.show()


def print_params(model):
    print("pi:\n", model.pi, "\n")
    print("means:\n", model.means, "\n")
    print("covs:\n", model.covs, "\n")


if __name__ == '__main__':
    (train_data, test_data), labels, cluster = data_1()

    model = ClusteringAlgorithm(Max_k=10)
    model.fit(train_data)
    res1 = model.predict(test_data)
    print(model.k)
    display(test_data, labels, cluster=cluster)
    display(test_data, res1, cluster=model.k)

    # model1 = KMeans(cluster,init_type="kmeans++")
    # model1.fit(train_data)
    # res1 = model1.predict(test_data)

    # model2 = GaussianMixture(cluster,init_params='random')
    # model2.fit(train_data)
    # res2 = model2.predict(test_data)

    # display(train_data, res1, cluster=cluster)
    # display(train_data, res2, cluster=cluster)
