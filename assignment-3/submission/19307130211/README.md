#  Assignment-3

陈洋 19307130211

## 模型实现

### KMeans

Kmeans算法在开始为每个聚类都选定一个聚类中心，在之后的迭代中进行以下两步操作，直到收敛位置：

* 分配步：通过计算每个点到聚类中心的距离，将它分配到与之距离最小的聚类中心去。

  ![image-20210614150634927](img/image-20210614150634927.png)

* 更新步：计算每个聚类的均值，将之作为新的聚类中心。

  ![image-20210614150906892](img/image-20210614150906892.png)

收敛条件为每个点的标签不再发生变化。

普通的Kmeans算法在最开始随机选取中心点，其收敛速度和收敛结果都有可能会受到影响，所以在实现上使用了Kmeans++这一方法。

其实现方法为开始在样本中随机选取一个点为第一个聚类中心，之后算出训练样本中每个点离与其最近的聚类中心距离，然后通过轮盘法选出下一个聚类中心，这样能够保证在随机选择下一个聚类中心时，距离当前聚类中心越远的点被选中的概率越大。

在实验部分会比较不同方法初始化聚类中心的效果。

### GMM

高斯混合模型通过EM算法进行参数估计，所以算法主要分为E步和M步进行迭代。

* E步：先固定参数 *μ,σ*，然后计算后验分布$p(z^{(n)}|x^{(n)})$,即：

  ![image-20210614153344247](img/image-20210614153344247.png)

* M步：固定$γ_{nk}$，将参数问题转化为优化问题，通过令偏导数为0得到参数$π,μ,σ$的更新公式：

  ![image-20210614153814100](img/image-20210614153814100.png)

  

  其中![image-20210614153834743](img/image-20210614153834743.png)

在实现中GMM中使用了两种选取初始高斯分布平均值的方法，第一种是在训练样本的数据范围中随机选取，第二种是使用之前实现的Kmeans算法得到的聚类中心作为高斯分布的均值，在实验部分会进行两种方法的比较。

## 基本实验

### 测试结果：

![image-20210614193106909](img/image-20210614193106909.png)

### 1.对于Kmeans聚类中心不同初始化的比较：

以下三张图分别是原数据，Kmeans随机初始化聚类中心和使用Kmeans++（迭代100次）的结果，可以看到kmeans++明显更符合原数据的分类。

<center class="half">
    <img src="img/data_1.png" width="300"/>
    <img src="img/kmeans_random.png" width="300"/>
    <img src="img/kmeans++.png" width="300"/>
</center>

### 2.对于GMM高斯分布均值的不同初始化的比较：

以下分别是原数据，GMM随机获得高斯分布均值和使用Kmeans获取高斯分布均值的结果。（分别迭代50次和10次）

<center class="half">
    <img src="img/data_1.png" width="300"/>
    <img src="img/GMM_random.png" width="300"/>
    <img src="img/GMM_kmeans.png" width="300"/>
</center>

<center class="half">
    <img src="img/data_1.png" width="300"/>
    <img src="img/GMM_random1.png" width="300"/>
    <img src="img/GMM_kmeans1.png" width="300"/>
</center>

可以看到使用kmeans初始化的GMM其绿色部分的点数更加符合原来的数据，原因应该为Kmeans能在一定程度上优化GMM，特别是当迭代次数较小时，使用kmeans进行初始化的方法仍然可以取得较为理想的结果。

### 3.Kmeans和GMM的比较：

为了较为公平的比较，GMM选用的是随机初始化均值的初始化方法。

以下分别是原数据，Kmeans++和随机初始化GMM的结果：

<center class="half">
    <img src="img/data_1.png" width="300"/>
    <img src="img/kmeans++_1.png" width="300"/>
    <img src="img/GMM_random2.png" width="300"/>
</center>

可以清楚的看到Kmeans因为是基于距离的划分，所以照成第二幅图中下半部分均分的形势，而GMM所做出的聚类更加符合原始数据，可以看出GMM更优一点。

<center class="half">
    <img src="img/data_2.png" width="300"/>
    <img src="img/kmeans++_2.png" width="300"/>
    <img src="img/GMM_random3.png" width="300"/>
</center>

在自己创造的数据上这一点更加明显，因为数据的建立比较有规律，Kmeans的结果类似于将点均匀分为了5块，而GMM的每一个聚类的点数规模都更接近真实数据。

综上，GMM的效果要优于Kmeans，但是在训练中也发现GMM的显著缺点：就是花费时间远大于Kmeans。

##  自动选择聚簇数量的实验

按照助教的提醒，使用Elbow method配合Kmeans，在了解Elbow Method的基本思路后，我需要做的就是在找出这个肘部。

方法简述：设定一个最大的K值，然后从k=1开始调用之前已经写好的kmeans，计算对应k值的簇内误差平方和(SSE)，再计算处相邻两个K值对应的SSE之差，计算出相邻差值间的变化率，变化率最大的点就是我们需要的拐点。

~~~Python
'''计算每个K值对应的SSE'''
SSE_list = []
for i in range(1, self.Max_K):
	KMeans_model = KMeans(i)
	KMeans_model.fit(train_data)
	cents = KMeans_model.cluster_center
	distances = []
	for row in train_data:
		distances.append(np.min(np.linalg.norm(row - cents, axis=1)))  # 与最近一个聚类中心的距离
	SSE_list.append(sum(distances))

'''计算相邻K值间的SSE之差'''
self.SSE = SSE_list
diff_SSE = []
for i in range(self.Max_K-2):
diff_SSE.append(abs(SSE_list[i]-SSE_list[i+1]))

'''获得K'''
Max_change_rate = 0
best_k = 2
for i in range(self.Max_K-3):
	if float((diff_SSE[i] - diff_SSE[i+1])/diff_SSE[i+1]) > Max_change_rate:
 		Max_change_rate = float(
 			(diff_SSE[i] - diff_SSE[i+1])/diff_SSE[i+1])
		best_k = i+2
~~~

最后结果展示：

<center class="half">
    <img src="img/data_2.png" width="300"/>
    <img src="img/SSE.png" width="300"/>
    <img src="img/Auto_k.png" width="300"/>
</center>

可以看到这一方法判断拐点的方法在我自己创造的数据集上表现较为良好，能够得到理想的结果

<center class="half">
    <img src="img/data_1.png" width="300"/>
    <img src="img/SSE1.png" width="300"/>
    <img src="img/Auto_k1.png" width="300"/>
</center>

而在助教提供的data1中，方法却没有得到较为良好的效果，推测原因为：数据集1(助教提供的数据集)下方两个点集的重合区域过高，使用SSE这一指标，容易将之认为是同一种数据。