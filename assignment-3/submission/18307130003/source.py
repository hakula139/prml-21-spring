from typing import Any, Callable, List, NamedTuple, Tuple, Type
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod
import math
import numpy as np


# Utilities

class NormalParameters(NamedTuple):
    '''
    Attributes:
        `size`: the number of data points in the dataset
        `mean`: the mean of the distribution
        `cov`: the coefficient of variation of the distribution (dimension > 1)
        `scale`: the standard deviation of the distribution (dimension = 1)
    '''

    size: int
    mean: Tuple[float, ...]
    cov: List[List[float]] = None
    scale: float = None


class UniformParameters(NamedTuple):
    '''
    Attributes:
        `size`: the number of data points in the dataset
        `intervals`: the range of each dimension, Tuple[shape(N), shape(N)]
    '''

    size: int
    intervals: Tuple[np.ndarray, np.ndarray]


def distance(point_1: np.ndarray, point_2: np.ndarray) -> float:
    '''
    Args:
        `point_1`: shape(d)
        `point_2`: shape(d)

    Return:
        The Euclidean distance between two points.
    '''

    return np.linalg.norm(point_1 - point_2)


def multinormal_pdf(x: np.ndarray, mean: np.ndarray, cov: np.ndarray) -> float:
    '''
    The probability density function of a multivariate Gaussian distribution
    with given parameters.

    Args:
        `x`: an observation, shape(d)
        `mean`: the mean of the distribution, shape(d)
        `cov`: the coefficient of variation of the distribution, shape(d, d)

    Return:
        f(x | mean, cov)
    '''

    cov_det: float = np.linalg.det(cov)
    dim: int = mean.shape[0]
    const: float = (((2 * math.pi) ** dim) * cov_det) ** (-1/2)
    x_m: np.ndarray = x - mean
    exp: float = -np.dot(x_m, np.linalg.solve(cov, x_m)) / 2
    return const * math.exp(exp)


def normal_pdf(x: float, mean: float, scale: float) -> float:
    '''
    The probability density function of a Gaussian distribution with given
    parameters.

    Args:
        `x`: an observation
        `mean`: the mean of the distribution
        `scale`: the standard deviation of the distribution

    Return:
        f(x | mean, scale)
    '''

    const = (2 * math.pi) ** (-1/2) / scale
    exp = -((x - mean) / scale) ** 2 / 2
    return const * math.exp(exp)


def assert_(var_name: str, got: Any, expected: Any) -> None:
    '''
    Args:
        `var_name`: variable name for logging
        `got`: actual value
        `expected`: expected value
    '''

    message = f'Assertion failed for {var_name}: expected {expected}, got {got}'
    assert got == expected, message


# Models

class Model(ABC):
    '''
    The abstract class (ABC) of a model.
    '''

    @abstractmethod
    def __init__(self, n_clusters: int, n_epochs: int) -> None:
        '''
        Args:
            `n_clusters`: the number of clusters to partition into
            `n_epochs`: the number of epochs to run in total
        '''

        # Dataset properties
        self.train_data: np.ndarray = None
        self.data_size: int = 0
        self.dimension: int = 0

        # Model parameters
        self.k: int = n_clusters
        self.n_epochs: int = n_epochs

        # Others
        self.rng: np.random.Generator = np.random.default_rng()
        self.FLOAT_MAX: float = np.finfo(float).max
        self.EPS: float = np.finfo(float).eps

    @abstractmethod
    def fit(self, train_data: np.ndarray) -> float:
        '''
        Train the model using training data.

        Args:
            `train_data`: shape(N, d)

        Return:
            The training loss.
        '''

        pass

    @abstractmethod
    def predict(self, test_data: np.ndarray) -> np.ndarray:
        '''
        Predict the labels of testing data using the pre-trained model.

        Args:
            `test_data`: shape(N, d)

        Return:
            The predicted labels of testing data, shape(N).
        '''

        pass


class KMeans(Model):
    '''
    k-means clustering algorithm, which aims to partition n observations into k
    clusters in which each observation belongs to the cluster with the nearest
    mean, serving as a prototype of the cluster.
    '''

    def __init__(self, n_clusters: int, n_epochs: int = 10) -> None:

        super().__init__(n_clusters, n_epochs)

        # Model parameters
        self.centroids: np.ndarray = None

    def label(self, point: np.ndarray, centroids: np.ndarray) -> int:
        '''
        Args:
            `point`: shape(d)
            `centroids`: shape(k, d)

        Return:
            The index of the closest centroid to the point.
        '''

        return np.argmin([distance(point, c) for c in centroids])

    def fit(self, train_data: np.ndarray) -> float:

        self.data_size: int = train_data.shape[0]
        self.dimension: int = (
            train_data.shape[1] if len(train_data.shape) > 1
            else 1
        )
        self.train_data = (
            train_data if self.dimension > 1
            else train_data.reshape(self.data_size,)
        )
        min_loss = self.FLOAT_MAX

        # Run n_epochs times to find the best model.
        for i in range(self.n_epochs):
            # Select k initial centroids randomly from training data.
            centroids: np.ndarray = train_data[
                np.random.choice(self.data_size, size=self.k, replace=False)
            ]
            labels: np.ndarray = np.zeros(self.data_size, dtype=int)

            while True:
                # Assign labels to points based on their closest centroids.
                new_labels = np.array([
                    self.label(point, centroids) for point in train_data
                ])

                # Check for convergence.
                if np.array_equal(new_labels, labels):
                    break
                labels = new_labels

                # Find new centroids from the means of points in each cluster.
                centroids = np.array([
                    np.mean(train_data[labels == i], axis=0) for i in range(self.k)
                ])

            loss: float = np.sum((train_data - centroids[labels]) ** 2)
            if loss < min_loss:
                min_loss = loss
                self.centroids = centroids

            print('Training progress: {}/{}, \tloss: {}'.format(
                i + 1, self.n_epochs, loss,
            ), end='\r')

        print()
        return min_loss

    def predict(self, test_data: np.ndarray) -> np.ndarray:

        return np.array([
            self.label(point, self.centroids) for point in test_data
        ])


class GaussianMixture(Model):
    '''
    Gaussian Mixture Model (GaussianMixture) is a probabilistic model that assumes there
    are a certain number of Gaussian distributions, and each of these
    distributions represent a cluster. Hence, a GaussianMixture tends to group the data
    points belonging to a single distribution together.
    '''

    def __init__(self, n_clusters: int, n_epochs: int = 5) -> None:

        super().__init__(n_clusters, n_epochs)

        # Dataset properties
        self.train_data: np.ndarray = None
        self.data_size: int = 0
        self.dimension: int = 0

        # Model parameters

        # means: shape(k, d)
        self.means: np.ndarray = None
        # covs: shape(k, d, d)
        self.covs: np.ndarray = None
        # scales: shape(k)
        self.scales: np.ndarray = None
        # weights: shape(k)
        self.weights: np.ndarray = None
        # likelihoods: shape(N, k)
        self.likelihoods: np.ndarray = None

        self.best_means: np.ndarray = None
        self.best_covs: np.ndarray = None
        self.best_scales: np.ndarray = None
        self.best_weights: np.ndarray = None
        self.best_likelihoods: np.ndarray = None

    def _init_params(self) -> None:
        '''
        Randomly initialize the starting parameters.
        '''

        # Select k initial centroids randomly from training data.
        self.means = self.train_data[
            np.random.choice(self.data_size, size=self.k, replace=False)
        ]

        if self.dimension > 1:
            # Initialize the coefficient of variation of each Gaussian
            # distribution to be an identity matrix.
            self.covs = np.array([
                np.identity(self.dimension) for _i in range(self.k)
            ])
        else:
            # Initialize the standard deviation of each Gaussian distribution
            # to be 1.
            self.scales = np.ones(self.k)

        # Initialize the likelihood that an observation belongs to a cluster
        # to be 1/k for all clusters.
        self.weights = np.ones(self.k) / self.k

    def _e_step(self, dataset: np.ndarray = None, test_mode: bool = False) -> None:
        '''
        In the E(xpectation) step, we calculate the likelihood of each
        observation x_i belonging to each cluster using the current estimated
        parameters.

        Args:
            `dataset`: shape(N, d)
            `test_mode`: whether test data is being used
        '''

        if dataset is None:
            dataset = self.train_data
        data_size: int = dataset.shape[0]

        if test_mode:
            self.means = self.best_means
            self.covs = self.best_covs
            self.scales = self.best_scales
            self.weights = self.best_weights
            self.likelihoods = self.best_likelihoods

        # Calculate the posterior probability that an observation belongs to
        # each Gaussian distribution, using Bayes' Theorem.
        self.likelihoods = np.zeros((data_size, self.k))
        for i, x_i in enumerate(dataset):
            # Calculate the likelihood of each observation x_i.
            if self.dimension > 1:
                f_i: np.ndarray = np.array([
                    self.weights[k] * multinormal_pdf(
                        x_i, self.means[k], self.covs[k] + self.EPS
                    ) for k in range(self.k)
                ])
            else:
                f_i: np.ndarray = np.array([
                    self.weights[k] * normal_pdf(
                        x_i, self.means[k], self.scales[k] + self.EPS
                    ) for k in range(self.k)
                ])
            f_i_sum: float = np.sum(f_i) + self.EPS
            self.likelihoods[i] = f_i / f_i_sum

    def _m_step(self) -> None:
        '''
        In the M(aximization) step, we re-estimate our learning parameters for
        each cluster.
        '''

        # shape(n, k).sum(axis=0) = shape(k)
        cluster_likelihoods: np.ndarray = np.sum(
            self.likelihoods, axis=0
        ) + self.EPS
        assert_(
            'cluster_likelihoods.shape',
            cluster_likelihoods.shape,
            (self.k,),
        )

        # shape(k) / scalar = shape(k)
        self.weights = cluster_likelihoods / self.data_size
        assert_('weights.shape', self.weights.shape, (self.k,))

        if self.dimension > 1:
            # shape(k, n) * shape(n, d) = shape(k, d)
            self.means = np.matmul(
                self.likelihoods.T, self.train_data
            ) / cluster_likelihoods[:, None]
            assert_('means.shape', self.means.shape, (self.k, self.dimension))

            # shape(k, d, d)
            self.covs = np.array([
                # shape(d, d) / scalar = shape(d, d)
                np.sum([
                    # scalar * shape(d, 1) * shape(1, d) = shape(d, d)
                    self.likelihoods[i, k] * np.matmul(
                        x_m.reshape((self.dimension, 1)),
                        x_m.reshape((1, self.dimension)),
                    )
                    # shape(n, d) - shape(d) = shape(n, d)
                    for i, x_m in enumerate(self.train_data - self.means[k])
                ], axis=0) / cluster_likelihoods[k]
                for k in range(self.k)
            ])
            assert_(
                'covs.shape',
                self.covs.shape,
                (self.k, self.dimension, self.dimension),
            )
        else:
            # shape(k, n) * shape(n) = shape(k)
            # shape(k) / shape(k) = shape(k)
            self.means = np.dot(
                self.likelihoods.T, self.train_data
            ) / cluster_likelihoods
            assert_('means.shape', self.means.shape, (self.k,))

            # shape(k)
            self.scales = np.array([
                np.sqrt(np.sum([
                    self.likelihoods[i, k] * (x_m ** 2)
                    for i, x_m in enumerate(self.train_data - self.means[k])
                ], axis=0) / cluster_likelihoods[k])
                for k in range(self.k)
            ])
            assert_('scales.shape', self.scales.shape, (self.k,))

    def fit(self, train_data: np.ndarray) -> float:

        self.data_size: int = train_data.shape[0]
        self.dimension: int = (
            train_data.shape[1] if len(train_data.shape) > 1
            else 1
        )
        self.train_data = (
            train_data if self.dimension > 1
            else train_data.reshape(self.data_size,)
        )
        min_loss = 0.0

        # Run n_epochs times to find the best model.
        for i in range(self.n_epochs):
            self._init_params()
            labels: np.ndarray = np.zeros(self.data_size, dtype=int)

            while True:
                self._e_step()
                self._m_step()

                # Assign labels to points based on the most possible cluster
                # according to the likelihood matrix.
                new_labels = np.argmax(self.likelihoods, axis=1)

                # Check for convergence.
                if np.array_equal(new_labels, labels):
                    break
                labels = new_labels

            loss: float = -np.mean(np.max(self.likelihoods, axis=1))
            if loss < min_loss:
                min_loss = loss
                self.best_means = self.means
                self.best_covs = self.covs
                self.best_scales = self.scales
                self.best_weights = self.weights
                self.best_likelihoods = self.likelihoods

            print('Training progress: {}/{}, \tloss: {}'.format(
                i + 1, self.n_epochs, loss,
            ), end='\r')

        print()
        return min_loss

    def predict(self, test_data: np.ndarray) -> np.ndarray:

        self._e_step(dataset=test_data, test_mode=True)
        return np.argmax(self.likelihoods, axis=1)


class ClusteringAlgorithm(Model):
    '''
    Based on k-means clustering algorithm, using Gap Statistic to estimate the
    number of clusters in the dataset automatically.
    '''

    def __init__(
        self, max_n_clusters: int, n_epochs: int = 10, n_ref: int = 5
    ) -> None:
        '''
        Args:
            `max_n_clusters`: the maximum number of clusters to test
            `n_epochs`: the number of epochs to run in total
            `n_ref`: the number of reference datasets in Gap Statistic
        '''

        # Not setting the number of clusters at the beginning.
        super().__init__(0, n_epochs)

        # Model parameters
        self.max_k = max_n_clusters
        self.n_ref = n_ref
        self.gaps: np.ndarray = None
        self.BREAK_THRESHOLD = 3

        self.best_model = None

    def generate_uniform(self, param: UniformParameters) -> np.ndarray:
        '''
        Generate a dataset from a uniform distribution with given parameters.

        Args:
            `param`: parameters used to generate a dataset

        Return:
            shape(N, d)
        '''

        size, (low, high) = param
        dimension: int
        if np.isscalar(low):
            assert_('np.isscalar(high)', np.isscalar(high), True)
            dimension = 1
            return self.rng.uniform(low, high, size)
        else:
            assert_('low.shape[0]', low.shape[0], high.shape[0])
            dimension = low.shape[0]
            return self.rng.uniform(low, high, (size, dimension))

    def fit(self, train_data: np.ndarray) -> float:

        gaps, max_gap, prev_gap = [0.0], 0.0, 0.0
        min_loss = self.FLOAT_MAX
        # Break the loop if the gap statistics is not likely to ascend anymore.
        break_counter = 0

        for k in range(1, self.max_k + 1):
            model = KMeans(k, self.n_epochs)
            loss = model.fit(train_data)

            low: np.ndarray = np.amin(train_data, axis=0)
            high: np.ndarray = np.amax(train_data, axis=0)

            # Generate n_ref reference datasets, cluster each one using the
            # current model, and calculate the gap statistic.
            log_random_loss: float = np.mean([
                # Since the dataset is generated from a uniform distribution,
                # there's no need to run the k-means clustering algorithm for
                # multiple times.
                math.log(KMeans(k, 1).fit(self.generate_uniform(
                    UniformParameters(
                        size=model.data_size,
                        intervals=(low, high),
                    )
                ))) for _i in range(self.n_ref)
            ])
            gap = log_random_loss - math.log(loss)
            gaps.append(gap)
            print('Tried k: {}/{}, \tgap statistics: {}'.format(
                k, self.max_k, gap
            ))

            if gap > max_gap:
                max_gap = gap
                min_loss = loss
                self.best_model = model

            if gap > prev_gap:
                break_counter = 0
            else:
                break_counter += 1
                if break_counter >= self.BREAK_THRESHOLD:
                    print('Gap statistics is not ascending, loop terminated.')
                    break
            prev_gap = gap

        self.gaps = np.array(gaps)
        print('Estimated k: {}'.format(self.best_model.k))
        return min_loss

    def predict(self, test_data: np.ndarray) -> np.ndarray:

        return self.best_model.predict(test_data)


# Tester

class TestSuite:
    '''
    Multiple testing data for models.
    '''

    def __init__(self) -> None:
        self.rng: np.random.Generator = np.random.default_rng()

    def generate_normal(self, param: NormalParameters) -> np.ndarray:
        '''
        Generate a dataset from a Gaussian distribution with given parameters.

        Args:
            `param`: parameters used to generate a dataset

        Return:
            shape(N, d)
        '''

        size, mean, cov, scale = param
        if len(mean) > 1:
            return self.rng.multivariate_normal(mean, cov, size)
        else:
            return self.rng.normal(mean[0], scale, size)

    def combine(self, *datasets: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        '''
        Combine several datasets into a single dataset.

        Args:
            `*datasets`: a tuple of datasets needed to combine

        Return:
            `dataset`: shape(N, d), where N is the total size of all datasets
            `labels`: shape(N), the labels for all points in the dataset
        '''

        dataset: np.ndarray = np.concatenate(datasets)
        labels: np.ndarray = np.concatenate([
            np.ones(d.shape[0], dtype=int) * i
            for (i, d) in enumerate(datasets)
        ])
        indices = np.arange(dataset.shape[0])
        np.random.shuffle(indices)
        dataset = dataset[indices]
        labels = labels[indices]
        return dataset, labels

    def generate_data(self, *params: NormalParameters) -> Tuple[np.ndarray, int]:
        '''
        Generate a dataset for tests.

        Args:
            `params`: a tuple of parameters to generate datasets

        Return:
            `dataset`: shape(N, d)
            `n_clusters`: the number of clusters to partition into
        '''

        dataset, _labels = self.combine(*tuple(
            self.generate_normal(p) for p in params
        ))
        n_clusters: int = len(params)
        return dataset, n_clusters

    def train(
        self, train_data: np.ndarray, model: Model
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        '''
        Train a model with training data.

        Args:
            `train_data`: shape(N, d)
            `model`: the model that we need to train
            `n_clusters`: the number of clusters to partition into

        Return:
            `train_labels`: the predicted labels of training data, shape(N)
            `centroids`: the centroids calculated from training data, shape(k, d)
            `gaps`: the gap statistics of each k, shape(k + 1)
        '''

        model.fit(train_data)
        train_labels = model.predict(train_data)
        centroids: np.ndarray = None
        gaps: np.ndarray = None

        if isinstance(model, KMeans):
            centroids = model.centroids
        elif isinstance(model, GaussianMixture):
            centroids = model.means
        elif isinstance(model, ClusteringAlgorithm):
            centroids = model.best_model.centroids
            gaps = model.gaps
        return train_labels, centroids, gaps

    def evaluate(self, test_data: np.ndarray, model: Model) -> Tuple[np.ndarray]:
        '''
        Evaluate a model with testing data.

        Args:
            `test_data`: shape(N, d)
            `model`: the model that we need to evaluate

        Return:
            `test_labels`: The predicted labels of testing data, shape(N)
        '''

        test_labels = model.predict(test_data)
        return test_labels

    def test_data_1(self) -> Tuple[np.ndarray, int]:

        return self.generate_data(
            NormalParameters(
                size=800,
                mean=(1, 2),
                cov=[[73, 0], [0, 22]],
            ),
            NormalParameters(
                size=200,
                mean=(16, -5),
                cov=[[21.2, 0], [0, 32.1]],
            ),
            NormalParameters(
                size=1000,
                mean=(10, 22),
                cov=[[10, 5], [5, 10]],
            ),
        )

    def test_data_2(self) -> Tuple[np.ndarray, int]:

        return self.generate_data(
            NormalParameters(
                size=800,
                mean=(1, 0),
                cov=[[73, 0], [0, 22]],
            ),
            NormalParameters(
                size=400,
                mean=(20, 15),
                cov=[[21.2, 0], [0, 32.1]],
            ),
            NormalParameters(
                size=1000,
                mean=(10, -22),
                cov=[[10, 5], [5, 10]],
            ),
            NormalParameters(
                size=500,
                mean=(-12, -6),
                cov=[[7, 3], [3, 16]],
            ),
            NormalParameters(
                size=600,
                mean=(-15, 17),
                cov=[[15, 0], [0, 12]],
            ),
        )

    def test_data_3(self) -> Tuple[np.ndarray, int]:

        return self.generate_data(
            NormalParameters(
                size=800,
                mean=(-6, 3, 5),
                cov=[[73, 0, 0], [0, 50, 0], [0, 0, 22]],
            ),
            NormalParameters(
                size=500,
                mean=(12, 0, -10),
                cov=[[20, 5, 0], [5, 20, 0], [0, 0, 20]],
            ),
            NormalParameters(
                size=800,
                mean=(10, -20, 0),
                cov=[[10, 1, 3], [1, 10, 0], [3, 0, 10]],
            ),
        )

    def test_data_4(self) -> Tuple[np.ndarray, int]:

        return self.generate_data(
            NormalParameters(
                size=100,
                mean=(-20,),
                scale=2,
            ),
            NormalParameters(
                size=150,
                mean=(0,),
                scale=1,
            ),
            NormalParameters(
                size=100,
                mean=(15,),
                scale=2,
            ),
        )

    def test_data_5(self) -> Tuple[np.ndarray, int]:

        return self.generate_data(
            NormalParameters(
                size=800,
                mean=(0, -5),
                cov=[[73, 0], [0, 2]],
            ),
            NormalParameters(
                size=500,
                mean=(-3, 0),
                cov=[[100, 0], [0, 2]],
            ),
            NormalParameters(
                size=500,
                mean=(2, 5),
                cov=[[70, 1], [1, 3]],
            ),
        )

    def run(self) -> None:
        '''
        Run all the tests.
        '''

        testcases: List[Tuple[
            str, Callable[[], Tuple[np.ndarray, int]], Type[Model], int
        ]] = [
            ('k-means_1', self.test_data_1, KMeans, 0),
            # ('k-means_2', self.test_data_2, KMeans, 0),
            # ('k-means_3', self.test_data_3, KMeans, 0),
            # ('k-means_4', self.test_data_4, KMeans, 0),
            # ('k-means_5', self.test_data_5, KMeans, 0),
            # ('GaussianMixture_1', self.test_data_1, GaussianMixture, 0),
            # ('GaussianMixture_2', self.test_data_2, GaussianMixture, 0),
            # ('GaussianMixture_3', self.test_data_3, GaussianMixture, 0),
            # ('GaussianMixture_4', self.test_data_4, GaussianMixture, 0),
            # ('GaussianMixture_5', self.test_data_5, GaussianMixture, 0),
            # ('auto-k-means_1', self.test_data_1, ClusteringAlgorithm, 10),
            # ('auto-k-means_2', self.test_data_2, ClusteringAlgorithm, 10),
            # ('auto-k-means_3', self.test_data_3, ClusteringAlgorithm, 10),
            # ('auto-k-means_4', self.test_data_4, ClusteringAlgorithm, 10),
            # ('auto-k-means_5', self.test_data_5, ClusteringAlgorithm, 10),
        ]

        for testcase in testcases:
            name, get_dataset, model_class, n_clusters = testcase

            # Obtain training data and testing data
            dataset, real_n_clusters = get_dataset()
            train_size: int = math.floor(dataset.shape[0] * 0.8)
            train_data: np.ndarray = dataset[:train_size]
            test_data: np.ndarray = dataset[train_size:]

            # Train the model with training data
            model = model_class(n_clusters or real_n_clusters)
            train_labels, centroids, gaps = self.train(train_data, model)

            # Evaluate the model with testing data
            test_labels = self.evaluate(test_data, model)

            # Visualize the datasets with labels
            visualize(name + '_train', train_data, train_labels, centroids)
            visualize(name + '_test', test_data, test_labels, centroids)

            # Visualize the gap statistics for
            if gaps is not None:
                visualize_gaps(name + '_gaps', gaps)

            print(f'{name}: Done.')


def visualize(
    name: str,
    dataset: np.ndarray,
    labels: np.ndarray,
    centroids: np.ndarray = None,
) -> None:
    '''
    Visualize a dataset with labels.

    Args:
        `name`: the output filename when saving the figure
        `dataset`: shape(N, d)
        `labels`: shape(N)
        `centroids`: shape(k, d)
    '''

    assert_('dataset.shape[0]', dataset.shape[0], labels.shape[0])

    # Plot the data points and the centroids.
    if len(dataset.shape) > 1:
        plt.scatter(dataset[:, 0], dataset[:, 1], c=labels, s=30)
        if centroids is not None:
            plt.scatter(
                centroids[:, 0], centroids[:, 1], c='black', s=100, alpha=0.5,
            )
    else:
        plt.scatter(dataset, np.zeros(dataset.shape[0]), c=labels, s=30)
        if centroids is not None:
            plt.yticks([])
            plt.scatter(
                centroids, np.zeros(centroids.shape[0]),
                c='black', s=100, alpha=0.5,
            )

    # Save the figure to a local file.
    plt.savefig(f'img/{name}')
    plt.clf()


def visualize_gaps(name: str, gaps: np.ndarray) -> None:
    '''
    Visualize the gap statistics.

    Args:
        `name`: the output filename when saving the figure
        `gaps`: the gap statistics of each k, shape(k + 1)
    '''

    # Plot the gap statistics.
    indices = np.arange(1, gaps.shape[0], dtype=int)
    plt.xticks(indices)
    plt.plot(indices, gaps[indices], '-bo')

    # Save the figure to a local file.
    plt.savefig(f'img/{name}')
    plt.clf()


if __name__ == '__main__':
    TestSuite().run()
