import numpy as np

import matplotlib.pyplot as plt
from scipy.spatial import voronoi_plot_2d  # 作图
from scipy.spatial import Voronoi


class KMeans:

    def __init__(self, n_clusters, init='k-means++',
                 max_iter=300, tol=1e-4, normal=False):
        """

        :param n_clusters:int, the number of clusters to form
        :param init:{'k-means++', 'random'}, callable or array-like of shape(n_clusters, n_features)
        :param max_iter:int, default=300, maximum number of iterations for a single run
        :param tol:float, default=1e-4

        Attributes:
            centers:NdArray of shape(n_clusters, n_features)
            labels:NdArray of shape(n_samples,), labels of training data
            n_iter:int, the number of iterations
            sse: list of float,sum of square error


        """
        # parameters
        self.n_clusters = n_clusters
        self.init = init
        self.max_iter = max_iter
        self.tol = tol
        self.normal = normal

        # attributes
        self.centers = None
        self.labels = None
        self.n_iter = 0
        self.sse = []

    def init_centers(self, train_data, ):
        """
        初始化聚类中心.
        :param train_data: NdArray of shape(n_samples, n_features),
                     the input samples
        :return: centers: NdArray of shape (n_clusters, n_features)
        """
        assert train_data.shape[0] >= self.n_clusters
        if hasattr(self.init, '__array__'):
            # 直接赋值
            self.centers = self.init

        elif self.init == 'random':
            # 从训练数据中随机选取n_clusters个样本作为聚类中心初始值
            idx = np.random.permutation(train_data.shape[0])[: self.n_clusters]
            self.centers = train_data[idx]

        elif self.init == 'k-means++':
            # 采用k-means++算法初始化
            self.centers = k_means_plusplus(train_data, self.n_clusters)

        return self.centers

    def update_centers(self, train_data, labels):
        """单步计算各簇中心."""
        # dimension of data
        n_features = train_data.shape[1]
        centers = np.zeros((self.n_clusters, n_features))
        for i in range(self.n_clusters):
            centers[i, :] = np.mean(train_data[labels == i], axis=0)

        self.centers = centers
        return centers

    def cal_sse(self, train_data, labels):
        """计算SSE, SSE的大小用以评估聚类效果."""
        distances = np.zeros(train_data.shape[0])
        for i in range(self.n_clusters):
            distances[labels == i] = np.linalg.norm(train_data[labels == i] - self.centers[i], axis=1)
        return np.sum(distances ** 2)

    def predict(self, data):
        """
        判断数据所属类别.
        :param data: NdArray of shape(n_samples, n_features)
        :return: labels: NdArray of shape(n_samples,)
        """
        if len(data.shape) == 1:
            data = data.reshape((-1, 1))

        distances = np.zeros((data.shape[0], self.n_clusters))
        for j in range(self.n_clusters):
            distances[:, j] = np.linalg.norm(data - self.centers[j], axis=1)

        labels = np.argmin(distances, axis=1)
        return labels

    def update_labels(self, train_data):
        """单步中对训练数据进行类别划分."""
        self.labels = self.predict(train_data)
        return self.labels

    def fit(self, train_data):
        """
        模型训练过程.
        :param train_data: NdArray of shape(n_samples, n_features)
        :return: init_centers: NdArray of shape(n_clusters, n_features)
        """
        if len(train_data.shape) == 1:
            train_data = train_data.reshape((-1, 1))

        # 归一化
        if self.normal:
            data_ = normalize(train_data)
        else:
            data_ = train_data

        # 参数初始化
        self.sse = []
        self.n_iter = 0
        # 聚类中心初始化
        init_centers = self.init_centers(data_)
        curr_centers = init_centers

        # 迭代
        for _ in range(self.max_iter):
            self.n_iter += 1
            labels = self.update_labels(data_)
            self.sse.append(self.cal_sse(data_, labels))
            self.update_centers(data_, labels)
            if np.linalg.norm(self.centers - curr_centers, ord='fro') <= self.tol:
                break
            curr_centers = self.centers

        return init_centers


class GaussianMixture:

    def __init__(self, n_clusters, max_iter=30, tol=1e-4, init='k-means++', normal=True):
        """

        :param n_clusters:int, the number of clusters to form
        :param max_iter:int, default=300, maximum number of iterations for a single run
        :param tol:float, default=1e-4
        :param init:{'k-means++', 'random'}, callable or array-like of shape(n_clusters, n_features)
        """
        # parameters
        self.n_clusters = n_clusters
        self.max_iter = max_iter
        self.tol = tol
        self.init = init
        self.normal = normal

        # attributes
        self.weights = None  # (n_clusters,)
        self.means = None  # (n_clusters, n_features)
        self.covariances = None  # (n_clusters, n_features, n_features)
        self.gamma = None  # (n_samples, n_clusters)
        self.n_iter = 0
        self.Q_funcs = []
        self.labels = None

    def init_params(self, train_data):
        """
        初始化混合高斯分布的各项参数以及隐变量. 具体地：
        means: 按照 self.init(random 或 k-means++) 的值来初始化
        weights & gamma: 均匀初始化
        covariances: 初始化为单位矩阵

        :param train_data: NdArray of shape(n_samples, n_features)
        :return: None
        """
        n_samples, n_features = train_data.shape
        # uniform
        self.weights = np.ones(self.n_clusters) / self.n_clusters
        self.covariances = np.array([np.identity(n_features)] * self.n_clusters)
        self.gamma = np.ones((n_samples, self.n_clusters)) / self.n_clusters

        # means
        if hasattr(self.init, '__array__'):
            assert self.init.shape == (self.n_clusters, n_features)
            self.means = self.init
        elif self.init == 'random':
            # 从训练数据中随机选取n_clusters个样本作为聚类中心初始值
            idx = np.random.permutation(train_data)[: self.n_clusters]
            self.means = train_data[idx]

        elif self.init == 'k-means++':
            # 采用k-means++算法初始化
            self.means = k_means_plusplus(train_data, self.n_clusters)

    def cal_q(self, train_data):
        """
        计算当前数据及参数下 Q函数的取值，
        Q函数即对数完全似然函数在数据及参数已知时的条件期望.
        :param train_data: NdArray of shape(n_samples, n_features)
        :return: q:float, the value of q function
        """
        n_samples, dim = train_data.shape
        q = 0
        for j in range(self.n_clusters):
            # calculate
            cov = self.covariances[j, :, :]
            cov_inv = np.linalg.inv(cov)
            det = np.abs(np.linalg.det(cov))

            # update
            q += (np.log(self.weights[j]) - 0.5 * np.log(det)
                  - 0.5 * dim * np.log(2 * np.pi)) * np.einsum('i->', self.gamma[:, j])
            for i in range(n_samples):
                v = train_data[i] - self.means[j]
                q -= self.gamma[i, j] * 0.5 * np.matmul(v, np.matmul(v, cov_inv))

        return q

    def e_step(self, train_data):
        """
        E step of EM Algorithm for GMM.
        :return: None
        """

        # update gamma
        n_samples = train_data.shape[0]
        for j in range(n_samples):
            new_gamma_j = []
            single_data = train_data[j]
            for k in range(self.n_clusters):
                new_gamma_j.append(self.weights[k] *
                                   multi_normal_pdf(single_data,
                                                    self.means[k],
                                                    self.covariances[k]))
            new_gamma_j = np.array(new_gamma_j)
            if sum(new_gamma_j) == 0:
                self.gamma[j, :] = np.ones(self.n_clusters) / self.n_clusters
            else:
                self.gamma[j, :] = new_gamma_j / sum(new_gamma_j)

        # calculate q function
        q = self.cal_q(train_data)
        self.Q_funcs.append(q)

    def m_step(self, train_data):
        """
        M Step of EM Algorithm for GMM.
        :return: None
        """
        # update weights, not completed, to be normalized
        self.weights = np.einsum('ij->j', self.gamma)

        # update means and covariances
        for k in range(self.n_clusters):
            # update means
            self.means[k] = np.matmul(self.gamma[:, k], train_data) / self.weights[k]

            # update covariances
            s = 0
            for j in range(train_data.shape[0]):
                v = train_data[j] - self.means[k]
                s += self.gamma[j, k] * np.matmul(v.reshape((-1, 1)), v.reshape((1, -1)))
            self.covariances[k] = s / self.weights[k]

            # to avoid singular cov matrix
            self.covariances[k] += 1e-3 * np.eye(train_data.shape[1])

        # update weight finished
        self.weights = self.weights / np.einsum('i->', self.weights)

    def fit(self, train_data):
        """
        EM Algorithm process.
        :return: None
        """
        if len(train_data.shape) == 1:
            train_data = train_data.reshape((-1, 1))

        # 参数初始化
        self.n_iter = 0
        self.Q_funcs = []

        # normalization
        if self.normal:
            data = normalize(train_data)
        else:
            data = train_data

        self.init_params(data)
        for _ in range(self.max_iter):
            self.e_step(data)
            self.n_iter += 1

            if (self.n_iter > 1 and
                    np.abs(self.Q_funcs[-1] - self.Q_funcs[-2]) < self.tol):
                break
            self.m_step(data)

        self.labels = np.argmax(self.gamma, axis=1)

    def predict(self, test_data):
        """
        给定数据集，给出其最有可能所属的子高斯分布.
        :param test_data: NdArray of shape(n, n_features)
        :return: labels: NdArray of shape(n,)
        """
        if len(test_data.shape) == 1:
            test_data = test_data.reshape((-1, 1))

        if self.normal:
            test_data = normalize(test_data)

        labels = np.zeros(test_data.shape[0])
        for i in range(test_data.shape[0]):
            prob = []
            for k in range(self.n_clusters):
                prob.append(self.weights[k] *
                            multi_normal_pdf(test_data[i],
                                             self.means[k],
                                             self.covariances[k]))
            labels[i] = np.argmax(prob)

        return labels

    def pdf(self, x):
        """
        混合高斯分布的密度函数.
        :param x: NdArray of shape(n_features,)
        :return: pdf:float
        """
        density = 0
        for n in range(self.n_clusters):
            density += self.weights[n] * multi_normal_pdf(x, self.means[n], self.covariances[n])

        return density


class ClusteringAlgorithm:

    def __init__(self):
        self.centers = None
        self.model = None
        self.n_clusters = 1

    def elbow(self, train_data):
        k_candidates = np.arange(1, 11)
        k_sse = np.zeros(10)
        for i in range(10):
            model = KMeans(n_clusters=k_candidates[i])
            model.fit(train_data)
            k_sse[i] = model.sse[-1]
        plt.plot(k_candidates, k_sse, marker='.')
        plt.title("SSE vs K")
        plt.xlabel("K")
        plt.ylabel("SSE")
        plt.savefig("elbow.png", dpi=300)
        plt.show()

        k_optimal = input("optimal k:")
        self.model = KMeans(k_optimal)

    def gap_statistic(self, train_data, figure=False):
        n_samples, n_features = train_data.shape
        k_max = max(10, int((n_samples / 2) ** 0.5))
        data_min = train_data.min(axis=0)
        data_max = train_data.max(axis=0)
        k_candidates = np.arange(1, 1 + k_max)
        k_sse = np.zeros(k_max)
        s = np.zeros(k_max)
        expect = np.zeros(k_max)

        # MC次数
        n_simulate = 20
        scalar = ((n_simulate + 1) / n_simulate) ** 0.5
        k_optimal = 0

        for k in k_candidates:
            model = KMeans(n_clusters=k)
            model.fit(train_data)
            k_sse[k - 1] = model.sse[-1]
            simulate_sse = np.zeros(n_simulate)
            for i in range(n_simulate):
                simulate_data = np.random.uniform(data_min, data_max, [n_samples, n_features])
                model.fit(simulate_data)
                simulate_sse[i] = model.sse[-1]

            expect[k - 1] = np.mean(np.log(simulate_sse))
            s[k - 1] = scalar * np.std(np.log(simulate_sse))

        gap = expect - np.log(k_sse)
        for k in range(k_max):
            if k_optimal == 0 and gap[k] >= (gap[k + 1] - s[k + 1]):
                k_optimal = k + 1
                break

        if figure:
            plt.figure()
            plt.plot(k_candidates, expect, marker='.', label='EW_k')
            plt.plot(k_candidates, np.log(k_sse), marker='.', label='logW_k')
            plt.plot(k_candidates, gap, marker='.', label='gap')
            plt.legend()
            plt.grid()
            plt.xlabel("the number of clusters K")
            plt.savefig("gap_statistic.png", dpi=300)
            plt.show()

        self.model = KMeans(k_optimal)

    def fit(self, train_data):
        if len(train_data.shape) == 1:
            train_data = train_data.reshape((-1, 1))
        self.model.fit(train_data)

    def predict(self, test_data):
        pass


colors = ['lightskyblue', 'sandybrown', 'mediumpurple', 'darkseagreen',
          'salmon', 'darkturquoise', 'hotpink', 'slategrey']


class plot:
    def __init__(self):
        pass

    @staticmethod
    def contour(f, scale):
        """
        等高线图.
        :param f: 密度函数
        :param scale: 图像范围
        :return:None
        """
        x_l, x_u, y_l, y_u = scale
        X = np.linspace(int(x_l), int(x_u), 50)
        Y = np.linspace(int(y_l), int(y_u), 50)

        X, Y = np.meshgrid(X, Y)
        Z = np.zeros_like(X)
        for i in range(X.shape[0]):
            for j in range(X.shape[1]):
                Z[i, j] = f(X[i, j], Y[i, j])

        plt.contour(X, Y, Z, cmap='Greys')

    @staticmethod
    def scatter(data_, labels, centers, voronoi=True):
        """
        聚类结果的可视化，适用于2维数据
        :param data_: NdArray of shape(n_samples, n_features)
        :param labels: NdArray of shape(n_samples,)
        :param centers: NdArray of shape(n_clusters, n_features)
        :param voronoi: bool
        :return: None
        """
        # 作voronoi图
        if voronoi:
            vor = Voronoi(points=centers)
            voronoi_plot_2d(vor)

        for i in range(data_.shape[0]):
            plt.scatter(data_[i, 0], data_[i, 1], color=colors[int(labels[i])], alpha=0.85)

        # data_min = data_.min(axis=0)
        # data_max = data_.max(axis=0)
        # plt.xlim(int(data_min[0]), int(data_max[0]) + 1)
        # plt.ylim(int(data_min[1]), int(data_max[1]) + 1)


"""
数据生成以及聚类实验
"""


class GenData(object):
    def __init__(self):
        pass

    @staticmethod
    def square():
        """
        生成两类二维长方形数据.
        :return:data, n_cluster
        """
        data = np.zeros((800, 2))
        for i in range(800):
            new_data = np.random.uniform([-1, -1], [1, 1])
            while -0.05 <= new_data[0] <= 0.05:
                new_data = np.random.uniform([-1, -1], [1, 1])
            data[i] = new_data
        return data, 2

    @staticmethod
    def circle(num):
        """
        生成单位圆内均匀分布的数据.
        :param num: int, number of data
        :return: data
        """
        data = []
        for i in range(num):
            new_data = np.random.uniform([-1, -1], [1, 1])
            while np.linalg.norm(new_data) > 1:
                new_data = np.random.uniform([-1, -1], [1, 1])
            data.append(new_data)

        return np.array(data)

    def three_circles(self):
        """
        三个相切的圆.
        :return: data, n_clusters
        """
        circle1 = self.circle(200) * 0.5 + [0.5, 0]
        circle2 = self.circle(200) * 0.5 + [-0.5, 0]
        circle3 = self.circle(200) * 0.5 + [0, 0.5 * 3 ** 0.5]
        circle = np.concatenate([circle1, circle2, circle3])
        np.random.shuffle(circle)
        return circle, 3

    @staticmethod
    def ring():
        ring1 = []  # 半径1的圆周
        ring2 = []  # 半径2的圆周

        for i in range(200):
            alpha = np.random.uniform(0, 2 * np.pi)
            error = np.random.normal(0, 0.09)
            r = 1 + error
            ring1.append(r * np.array([np.cos(alpha), np.sin(alpha)]))

        for i in range(400):
            alpha = np.random.uniform(0, 2 * np.pi)
            error = np.random.normal(0, 0.09)
            r = 2 + error
            ring2.append(r * np.array([np.cos(alpha), np.sin(alpha)]))

        return np.concatenate([ring1, ring2]), 2

    @staticmethod
    def mixed_gaussian(rho, sizes=(200, 200, 200)):
        """
        生成含有三个子分布的混合高斯分布.
        :param rho: float in (0, 1), 相关系数
        :param sizes: tuple, 每个子分布的规模
        :return:
        """
        cov = np.array([[1, rho], [rho, 1]])
        data1 = np.random.multivariate_normal([-2, 0], cov, size=sizes[0])
        plt.scatter(data1[:, 0], data1[:, 1], color=colors[0], alpha=0.8, edgecolors='white')
        data2 = np.random.multivariate_normal([2, 0], cov * 0.7, size=sizes[1])
        plt.scatter(data2[:, 0], data2[:, 1], color=colors[1], alpha=0.8, edgecolors='white')
        data3 = np.random.multivariate_normal([1.5, 2], cov * 1.5, size=sizes[2])
        plt.scatter(data3[:, 0], data3[:, 1], color=colors[2], alpha=0.8, edgecolors='white')
        plt.title("rho={}".format(rho))
        plt.savefig("gmm_init_{}.png".format(rho), dpi=300)
        plt.show()
        return np.concatenate([data1, data2, data3]), 3


def init_expr():
    """
    初始化方法对K-means的影响
    :return:None
    """
    data_gen = GenData()
    train_data, _ = data_gen.mixed_gaussian(0.1, (100, 200, 300))
    means = np.array([[-2, 0], [2, 0], [1.5, 2]])

    # 全部数据
    plt.scatter(train_data[:, 0], train_data[:, 1], color='grey', alpha=0.6)
    plt.savefig("total_init_expr.png", dpi=300)
    plt.show()
    sse = []
    labels = []

    methods = ['random', 'k-means++']

    # 随机初始化
    for j in range(2):
        for i in range(3):
            model = KMeans(3, init=methods[j])
            init_centers = model.fit(train_data)

            plot.scatter(train_data, model.labels, model.centers)
            plt.xlim(-5, 5)
            plt.ylim(-3, 6)

            # 作聚类中心
            plt.scatter(init_centers[:, 0], init_centers[:, 1], marker='x', color='black', label='initial centers')
            plt.scatter(means[:, 0], means[:, 1], marker='x', color='gold', label='actual centers')
            plt.scatter(model.centers[:, 0], model.centers[:, 1], marker='x', color='red', label='final centers')
            plt.legend()
            plt.savefig("init_expr_{}{}.png".format(methods[j], i), dpi=300)
            plt.show()
            sse.append(model.sse)

    legends = ['random1', 'random2', 'random3', 'k-means++1', 'k-means++2', 'k-means++3']
    for i in range(len(sse)):
        mk = '--' if i < 3 else '-'
        plt.plot(sse[i], linestyle=mk, color=colors[i], label=legends[i])

    plt.xlabel("iteration")
    plt.ylabel("SSE")
    plt.legend()
    plt.grid()
    plt.title("model under different initializing methods".capitalize())
    plt.savefig("init_expr_line.png", dpi=300)
    plt.show()


methods = ['k-means', 'GMM']


def general(train_data, n_cluster, filename, method='k-means'):
    """通用实验"""
    assert method in methods
    model = KMeans(n_cluster, normal=True) if method == 'k-means' else GaussianMixture(n_cluster, max_iter=50)
    model.fit(train_data)
    loss = model.sse if method == 'k-means' else model.Q_funcs
    name = 'loss' if method == 'k-means' else 'q_function'
    # loss curve
    plt.plot(loss)
    plt.xlabel("iteration")
    plt.ylabel(name)
    plt.grid()
    plt.savefig(name + filename, dpi=300)
    plt.show()

    # voronoi = True if method == 'k-means' else False
    centers = model.centers if method == 'k-means' else model.means
    plot.scatter(train_data, model.labels, centers, False)
    plt.scatter(centers[:, 0], centers[:, 1], marker='x', color='red')
    plt.savefig(filename, dpi=300)
    plt.show()


def general_compare(train_data, n_clusters, data_name):
    plt.scatter(train_data[:, 0], train_data[:, 1], color='grey', alpha=0.5)
    plt.savefig("{}.png".format(data_name), dpi=300)
    plt.show()
    general(train_data, n_clusters, data_name + "_kmeans_outcome.png")
    general(train_data, n_clusters, data_name + "_gmm_outcome.png", "GMM")


def square_expr():
    """方形数据聚类."""
    gen = GenData()
    train_data, n_clusters = gen.square()
    general_compare(train_data, n_clusters, "square")


def circle_expr():
    """圆形数据聚类."""
    gen = GenData()
    train_data, n_clusters = gen.three_circles()
    general_compare(train_data, n_clusters, "circle")


def ring_expr():
    """环形数据聚类."""
    gen = GenData()
    train_data, n_clusters = gen.ring()
    general_compare(train_data, n_clusters, "ring")


def mixed_gaussian_expr():
    np.random.seed(16)
    gen = GenData()
    train_data, n_clusters = gen.mixed_gaussian(0.1)
    general_compare(train_data, n_clusters, "gmm_01_balance")

    train_data, n_clusters = gen.mixed_gaussian(0.5)
    general_compare(train_data, n_clusters, "gmm_05_balance")

    train_data, n_clusters = gen.mixed_gaussian(0.9)
    general_compare(train_data, n_clusters, "gmm_09_balance")

    train_data, n_clusters = gen.mixed_gaussian(0.99)
    general_compare(train_data, n_clusters, "gmm_099_balance")

    train_data, n_clusters = gen.mixed_gaussian(0.5, (50, 50, 50))
    general_compare(train_data, n_clusters, "gmm_05_balance_s")

    train_data, n_clusters = gen.mixed_gaussian(0.5, (500, 500, 500))
    general_compare(train_data, n_clusters, "gmm_05_balance_l")

    train_data, n_clusters = gen.mixed_gaussian(0.5, (80, 200, 600))
    general_compare(train_data, n_clusters, "gmm_05_imbalance")


"""utils"""


def k_means_plusplus(dataset, n_clusters):
    """
    利用 k-means++ 算法选取初始聚类中心.
    :param dataset: NdArray of shape(n_samples, n_features)
    :param n_clusters: int, number of clusters and centroids to form
    :return: centers: NdArray of shape(n_clusters, n_features)
    """
    n_samples = dataset.shape[0]
    centers = [dataset[np.random.randint(0, n_samples)]]

    # 寻找其余 n_clusters - 1 个 聚类中心
    for _ in range(1, n_clusters):
        max_dist = 0
        next_center = dataset[0]
        for data in dataset:
            # data到各聚类中心的距离
            dist_centers = np.linalg.norm(data - centers, axis=1)
            min_dist = np.min(dist_centers)

            # update
            if min_dist > max_dist:
                max_dist = min_dist
                next_center = data

        centers.append(next_center)
    return np.array(centers)


def multi_normal_pdf(x, mean, cov):
    """多元正态分布密度"""
    if isinstance(x, float) or isinstance(x, int):
        return np.exp(-0.5 * (x - mean) ** 2 / cov ** 2) / ((2 * np.pi) ** 0.5 * cov)

    # assert len(x.shape) == 1
    # assert cov.shape[0] == cov.shape[1], 'wrong covariance.'
    # assert x.shape[0] == mean.shape[0] == cov.shape[0], 'dimension error.'

    d = x.shape[0]
    inv = np.linalg.pinv(cov)
    det = 1 / np.linalg.det(inv)
    v = x - mean
    exp_part = -0.5 * np.matmul(v, np.matmul(v, inv))
    return np.exp(exp_part) / ((2 * np.pi) ** d * det) ** 0.5


def normalize(data):
    """归一化."""
    mean = np.mean(data, axis=0)
    std = np.std(data, axis=0)
    return (data - mean) / std


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int) * i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


if __name__ == '__main__':
    """
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (320,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (80,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (400,))

    data, _ = shuffle(x, y, z)
    means = np.array([[1, 2], [16, -5], [10, 22]])

    gen = GenData()
    data1, n_clusters = gen.mixed_gaussian(0.9)

    """

    # 初始化方法 实验
    init_expr()

    square_expr()

    circle_expr()

    ring_expr()

    mixed_gaussian_expr()
