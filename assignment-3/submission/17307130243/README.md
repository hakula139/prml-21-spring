# 聚类算法

## 0. 概述
本次实验利用`numpy`包，完成了对**K-Means**算法的实现以及对**GMM**模型的构建，同时生成多种类型的数据并在其上运用以上两种算法进行聚类。此外，在**K-means**算法的基础上实现了可以自动选择聚类个数的功能。实验中的数据聚类实验均利用`matplotlib`包对结果进行可视化展示，并对结果作了一定的分析。

## 1. 算法实现

### 1.1 K-Means算法

#### 1.1.1 算法概要

K-means 算法是一种迭代地对无标签数量型数据进行聚类的方法。作为一种聚类算法，在指定类别个数 $K$ 后，它需要找到一个数据指标集到$\{0,...,k-1\}$的映射$C$，即将所有数据划分为$K$类，使得反映各类别中的数据的类内差异的函数：
$$W(C)=\frac{1}{2}\sum_{1\leq k\leq K}\sum_{C(i)=C(i')=k}d(x_i,x_{i'})$$

尽可能小，其中$d$为衡量两数据的差异或不相似程度的函数。 
具体地，K-means算法用样本点之间的欧氏距离的平方来衡量它们之间的差异/不相似程度，即： 
$$d(x_i,x_{i'})=\|x_i-x_{i'}\|^2=\sum_{p}(x_{ip}-x_{i'p})^2$$
从而，若令$N_k$为第$k$类样本个数，容易得到：$$W(C)=\frac{1}{2}\sum_{1\leq k\leq K}\sum_{C(i)=C(i')=k}\|x_i-x_{i'}\|^2=\sum\limits_{k=1}^KN_k\sum\limits_{C(i)=k}\Vert x_i-\bar x_k\Vert^2$$

K-means算法不直接求解以上问题，而是以一种迭代的方式来使上式逐渐减小。首先，算法以某种方式获得 $K$ 个初始的聚类中心，随后开始迭代，每次迭代完成以下两步： 

- **Assignment:** 对每一个样本，计算其与各聚类中心的欧氏距离，将其划分为距离最近的聚类中心所在的类别
- **Update:** 对于每个聚类，将其中数据的平均值作为新的聚类中心

当满足迭代停止条件时，算法停止。
注意：K-means算法可能收敛至局部最优解。



#### 1.1.2 代码实现
K-means算法的代码实现位于文件[source.py](./source.py) 中的类 `KMeans` 

- **初始化方法**

K-means 算法是初始值敏感的算法，K 个初始聚类中心的选取可能会影响算法的收敛速度，在数据集本身聚类性质不佳或者K 的选取不合适时，不同初始聚类中心的选取甚至会得到完全不同的聚类结果。代码实现时（位于类`KMeans`中的方法`init_centers`）提供了两种初始化的方法以供选择：**随机初始化**和**k-means++初始化**，默认方法为**k-means++初始化**。

**随机初始化**: 从训练数据中随机抽取 K 个样本作为初始的聚类中心。 

**k-means++**: 由于随机抽取数据有一定盲目性，可能发生多个初始聚类中心比较相似，位于同一个聚簇中，从而导致算法收敛缓慢或者更倾向于收敛到局部最优解而非全局最优解无法收敛，甚至可能无法收敛。针对这一问题，k-means++算法做出改进，其主要思想就是：初始聚类中心间的距离尽可能地远。具体做法如下：

1. 在训练数据中随机选取一个点，将其加入初始聚类中心集。
2. 对于其余的每个数据，计算其与已选中心间最近距离$D(x)$。
3. 对每个数据$x$,以概率$\frac{{D(x)}}{\sum_{x\in\mathcal{X}}D(x)}$选为下一个聚类中心。即距离已选的聚类中心越远的点与有可能被选为下一个聚类中心。
4. 重复以上2、3，直至聚类中心个数达到 K。

在代码具体实现时，为方便，直接将第2步中$D(x)$最大的$x$选为新的聚类中心。

- **聚类效果评估指标**

算法实现时将训练数据的误差平方和（Sum of Squared Errors, SSE）作为评估算法聚类效果的指标：
$$SSE=\sum_{k=1}^K\sum_{C(n)=k}\|x_n-m_k\|^2$$
每次迭代计算一次$SSE$并存储，当前后两次迭代所得模型的$SSE$之差足够小或者迭代次数到达所设置的最大值时，算法停止，最后一次计算得到的$SSE$作为模型聚类效果的度量。

- **预测方法**
位于类`KMeans`的方法`predict`，对于给定的数据，将其划分为距离最近的聚类中心所在类。




### 1.2 高斯混合模型(GMM)

#### 1.2.1 模型概要
高斯混合模型是具有以下形式的概率分布模型：
$$p(x|\mu,\sigma^2)=\sum_{k=1}^K\pi_kf(x|\mu_k,\sigma_k^2),\sum_k^K\pi_k=1, f(x|\mu_k,\sigma_k^2)\sim\mathcal{N}(\mu_k,\sigma_k^2),\theta=(\mu,\sigma^2)$$
高斯混合模型由于其复杂性，常常被运用于估计其他连续的随机变量或随机向量。
高斯混合分布的概率密度函数中存在多个复合指数函数的求和，通常的（对数）极大似然估计（MLE）难以计算极值，因此一般利用EM算法配合极大似然估计（MLE）或者极大后验估计来对高斯混合模型的参数进行估计，具体做法如下：
- 首先，引入隐变量$\gamma_{ik}, 1\leq i\leq N, 1\leq k \leq K$，若第$i$个观测数据来自第$k$个子高斯分布，则$\gamma_{ik}=1$, 否则$\gamma_{ik}=0$。
  此时，完全数据的似然函数为：
  $$p(x,\gamma|\mu,\sigma^2)=\prod_{k=1}^K\prod_{i=1}^N[\pi_kf(x|\mu_k,\sigma_k^2)]^{\gamma_{ik}}$$
  对数似然函数：
  $$\sum_{k=1}^K\sum_{i=1}^N\gamma_{ik}[\log\pi_k-\frac{1}{2}\log(2\pi)-\log\sigma_k-\frac{1}{2\sigma_k^2}(x_i-\mu_k)^2]$$
- 对各项参数初始化，开始迭代
- E步：
  1. 建立$Q$函数：
  设$\theta_{(t)}$为上一步迭代所得出的参数
  $$Q(\theta,\theta^{(t)})=\mathbb{E}[\log P(x,\gamma|\theta)|x, \theta^{(t)}]=\\\sum_{k=1}^K\sum_{i=1}^N\mathbb{{E}}\gamma_{ik}[\log\pi_k-\frac{1}{2}\log(2\pi)-\log\sigma_k-\frac{1}{2\sigma_k^2}(x_i-\mu_k)^2]$$
  即在似然函数中用隐变量的条件期望来代替其自身，从而可以进行类似极大似然估计的步骤 
  2. 更新$\gamma$以其期望，可以得到：
  $$\hat{\gamma_{ik}}=\mathbb{{E}}\gamma_{ik}=\frac{\pi_kf(x_i|\theta_k^{(t)})}{\sum_{k=1}^K\pi_kf(x_i|\theta_{k}^{(t)})}, \forall i,k$$
- M步：
  求$\theta^{(t+1)}=\argmax_{\theta}Q(\theta,\theta^{(t)})$ 
  可以得到如下参数更新公式:
  $$\mu_k=\frac{\sum_{i}\hat{\gamma_{ik}x_i}}{\sum_{i}\hat{\gamma_{ik}}}$$
  $$\sigma_k^2=\frac{\sum_{i}\hat{\gamma_{ik}}(x_i-\mu_k)^2}{\sum_{i}\hat{\gamma_{ik}}}$$
  $$\hat{\pi_k}=\frac{\sum_{i}\hat{\gamma_{ik}}}{N}$$

交替重复以上E步与M步，直至Q函数不发生明显变化。
以上过程与公式针对一维随机变量，多维随机变量的情形与之类似，仅有个别地方为了合理性需要做小的改动，这里不再赘述。
#### 1.2.2 代码实现

高斯混合模型的代码位于于文件[source.py](./source.py) 中的类 `GaussianMixture`。
- 参数初始化
  模型的参数有：
  > weights:各子高斯分布的权重  
  > means: 各子高斯分布的均值  
  > covariances: 各子高斯分布的协方差矩阵  
  > gamma：隐变量  

  `gamme`与`weights`进行均匀初始化，`covariances`初始化为单位矩阵。
  `means`的初始化同样提供了**随机初始化**与**k-means++**两种方式，默认为**k-means++**，与KMean模型中的实现相同。

- 迭代停止条件
  
  每次迭代计算一次当前$Q$函数的值并储存，当$Q$函数的值前后两次之差小于一个给定值或迭代次数达到最大值时迭代停止。
- E步：`e_step()`，计算$Q$函数，计算隐变量的期望，对其进行更新。
- M步：`m_step()`，对模型各参数进行更新。
- 数值考虑：
  1. 可能存在离集群很远的数据，计算得出其来自任何一个子分布的概率都接近于零，由于数值精度有限，导致$\gamma$矩阵存在全0行，归一化时出现错误，遇到这种情况时将$\gamma$矩阵对应行设置为每个元素都相等且和为1的向量。
  2. 迭代过程中得到的协方差矩阵可能不是正定的，甚至可能不是可逆的，为了避免这种情况，在每个协方差阵上加单位矩阵乘以一个很小的正数。这一做法的合理性参考[Regularized Gaussian Covariance Estimation](https://freemind.pluskid.org/machine-learning/regularized-gaussian-covariance-estimation/).

## 2. 实验

### 2.1 基础实验

#### 2.1.1 初始化方式对K-means算法的影响
实验代码位于[source.py](./source.py)中的`init_expr()`
这部分实验中，根据以下参数生成来自三个高斯分布的样本，每类200个样本。然后分别调用以k-means++/random为初始化方法的KMeans模型来对数据进行聚类，为消减偶然性带来的大方差，每组实验进行了三次。
$$\mu: [-2, 0],[2,0],[1.5,2]$$
$$\Sigma:I, 0.7I, 1.5I$$
下图展示了两种初始化方法下的聚类效果以及各阶段的聚类中心。
|**K-means++**|**random**|
|--|--|
|![kmeans++](./img/init_expr_k-means  0.png)|![random](./img/init_expr_random1.png) 

从图中可以看出，采用k-means++算法进行初始化，得到的初始聚类中心十分极端，一般位于集群边缘，而右图中随机初始化得到的聚类中心仅分布在一块小区域中。两者最终的聚类效果相差不大。 

下图是六次实验中SSE与迭代次数的关系图：
<div align="center">
<img src=./img/init_expr_line.png width=80% />
</div>
可以看到，k-means++初始化由于初始聚类中心均在边缘且距离很远，所以最初的SSE值普遍很高，但是经过一次迭代后迅速下降，并且快速收敛，普遍比随机初始化的情形更快收敛。这体现出k-means++算法的强大能力

#### 2.1.2 特殊形状数据
这部分实验生成了一些特殊形状的数据集来进行聚类，代码位于[source.py](./source.py)文件中的类 `GenData`.

- **方形块状数据**
数据生成代码位于`GenData`类中的`square`方法，聚类实验代码位于`square_expr`。
数据生成方式：对$[-1, 1]^2$上的均匀分布进行采样，并去除横坐标介于-0.05与0.05之间的样本，形成两个块状矩阵类。

|原始数据|K-means 聚类结果|GMM聚类结果|
|--|--|--|
|![square1](./img/square.png)|![square2](./img/square_kmeans_outcome.png)|![square3](./img/square_gmm_outcome.png)|  


可以看到，k-means算法很好地完成了聚类，聚类中心在各矩形中心。GMM算法难以收敛，这里选取了某一步迭代后的结果，可见GMM对该数据集聚类效果不好。

- **圆形数据**
  数据生成代码位于`GenData`类中的`three_circles`方法，聚类实验代码位于`circle_expr`。
  数据生成方式：对单位圆上的均匀分布进行采样，并将其平移。采样多次，形成三个相切的圆。

|原始数据|K-means 聚类结果|GMM聚类结果|
|--|--|--|
|![square1](./img/circle.png)|![square2](./img/circle_kmeans_outcome.png)|![square3](./img/circle_gmm_outcome.png)| 

从图中可以看出，k-means与GMM都取得了不错的聚类效果。k-means算法得到的聚类中心几乎就是三个圆的圆心，但是GMM算法的聚类中心均远远偏离三个圆心，训练数据只能模拟高斯分布靠近边缘的分布。这一结果反映了k-means算法对于均匀分布的、类间有明显边界的、集群形状凸的数据的聚类能力，同时反映了GMM模型对与自身不相似的分布的强大的模拟能力。

- **环形数据**
数据生成代码位于`GenData`类中的`ring`方法，聚类实验代码位于`ring_expr`。
数据生成方式:首先分别生成服从两个圆周上的均匀分布的样本，再对各样本加上服从小方差、零均值正态分布的噪声，得到两个环形数据集。

|原始数据|K-means 聚类结果|GMM聚类结果|
|--|--|--|
|![square1](./img/ring.png)|![square2](./img/ring_kmeans_outcome.png)|![square3](./img/ring_gmm_outcome.png)|


两种聚类算法在该数据集上的聚类表现都不好。
#### 2.1.3 混合高斯分布

这部分实验的数据主要为基于以下均值和协方差阵生成的混合高斯分布：
$$\mu: [-2, 0],[2,0],[1.5,2]$$
$$\Sigma:E, 0.7E, 1.5E， E=[[1, \rho],[\rho,1]]$$

- **不同$\rho$下的混合高斯分布**

|$\rho$|原始类别|K-means聚类结果|GMM聚类结果|
|--|--|--|--|
|0.1|![](./img/gmm_init_0.1.png)|![](./img/gmm_01_balance_kmeans_outcome.png)|![](./img/gmm_01_balance_gmm_outcome.png)|
|0.5|![](./img/gmm_init_0.5.png)|![](./img/gmm_05_balance_kmeans_outcome.png)|![](./img/gmm_05_balance_gmm_outcome.png)|
|0.9|![](./img/gmm_init_0.9.png)|![](./img/gmm_09_balance_kmeans_outcome.png)|![](./img/gmm_09_balance_gmm_outcome.png)|
|0.99|![](./img/gmm_init_0.99.png)|![](./img/gmm_099_balance_kmeans_outcome.png)|![](./img/gmm_099_balance_gmm_outcome.png)|

- **等比例不同规模**

|每类样本数|K-means聚类结果|GMM聚类结果|
|--|--|--|
|50|![](./img/gmm_05_balance_s_kmeans_outcome.png)|![](./img/gmm_05_balance_s_gmm_outcome.png)|
|200|![](./img/gmm_05_balance_kmeans_outcome.png)|![](./img/gmm_05_balance_gmm_outcome.png)
|500|![](./img/gmm_05_balance_l_kmeans_outcome.png)|![](./img/gmm_05_balance_l_gmm_outcome.png)

- **不均衡数据**

|各类样本个数|K-means聚类结果|GMM聚类结果|
|--|--|--|
|(80,200,600)|![](./img/gmm_05_imbalance_kmeans_outcome.png)|![](./img/gmm_05_imbalance_gmm_outcome.png)|

K-means算法倾向于把数据划分成等规模的类别，在不均衡数据集上可能会表现不好。
### 2.2 自动选择聚簇数量
这部分实验实现了基于KMeans的算法的自动选择聚簇数量的聚类算法。代码位于类`ClusteringAlgorithm`。
#### 2.2.1 Elbow算法

Elbow 是一种用于确定聚类数量的启发式方法，需要作出损失函数(SSE)与聚类个数K 的关系图，并寻找曲线的拐点作为聚类个数，满足：当K小于该点时，SSE快速降低，K大于该点时，SSE降低的速度放缓，即SSE前后两点的差值在该点处有显著降低。

代码实现位于`ClusteringAlgorithm.elbow`
实验中，使用了`tester_demo.py`中的`data_1`数据集，其为含有三个子高斯分布的高斯混合分布。
以下为SSE与K 的关系曲线：
可以看到 3 是满足要求的拐点，恰恰也是真实的聚簇个数。

<div align="center">
<img src=./img/elbow.png width=80% />
</div>

#### 2.2.2 Gap Statistic
elbow方法需要人工根据曲线来选择最合适的K，不能算作完全自动的聚簇个数选择方法。而且，有时SSE与K的关系曲线不那么清晰，难以用肉眼找出最好的K。Gap Statistic 方法可以视作一种自动化的elbow方法，主要步骤如下：
定义 **$Gap(k)=\mathbb{E}\log(SSE) - \log SSE$**
其中$\mathbb{E}\log(SSE)$通过MC模拟产生，具体生成方式是：在样本所在的矩形区域中均匀采样得到与训练数据数量相同的样本，对该组样本作KMeans算法聚类，得到一个SSE。重复多次后（default=20), 计算$\log SSE$的均值，得到$\mathbb{E}\log(SSE）$的估计$w$。
为了修正MC采样带来的误差，计算标准差来修正Gap：
$$sd(k)=\sqrt{\frac{1}{B}\sum_b(\log(SSE_{kb})-w)^2}$$

$$s_k=\sqrt{\frac{1 + B}{B}}sd(k)$$
最后，选择满足$Gap(k+1)-s_{k+1}\leq Gap(k)$的最小的K作为最优的聚类个数。

以下实验沿用上面的数据集，运用Gap Statistic方法求得最优的聚类个数为3，符合事实，可见Gap Statistic实现正确且有效。


<div align="center">
<img src=./img/gap_statistic.png width=80% />
</div>

## 3.总结
**K-means**算法原理简单，收敛较快，在一些数据集上可以有很好的效果。由算法使用的度量：欧氏距离以及其聚类方法可以知道，K-means算法用数个超平面（二维情形为直线）划分全空间，划分所得每块区域是凸多边形。因此，K-means对集群形状是凸的、连通的数据集会更有效，否则可能需要对原始数据集做一些变换来达到更好的聚类效果，其本身并不太灵活。K-means算法是基于数据本身来聚类，只会考虑存在的数据，一些没有数据的小区域的存在不会对聚类产生太大影响，但是“没有数据”也是一种信息。比如上面带状的混合高斯分布，K-means算法作的划分几乎与分布的走向垂直，这是只考虑数据间距离及其均值的短处。  

**GMM**作为一种聚类手段时，属于软聚类，它提供数据属于某个类别的概率而非直接划分，因此可以处理那些处于重叠区域的数据，适用的集群形状也更加多样，总体来说相比于K-means算法，GMM更加灵活。但是每一步迭代的计算开销很大，与某些数据集，可能需要自动选择聚类个数才能较好地体现GMM的优势，计算量将会很大。
## 代码运行方式
> python source.py