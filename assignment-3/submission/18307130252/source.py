import numpy as np
import math
from depict import scatter_graph, curve_graph, generate

def normalize(data):
    """
    parameter:
        data: 需要标准化的数据
    return
        data' : 标准化之后的数据
    """
    stats = (data.mean(axis=0), data.std(axis=0))
    return (data - stats[0]) / stats[1]

class KMeans:

    def __init__(self, n_clusters):
        """
        parameter:
            n_clusters: 聚类数目
        """
        self.n_clusters = n_clusters
        self.means = None
        pass
    
    def distance(self, X, Y):
        """
        parameter:
            X, Y: 两个 n 维向量
        return:
            X 和 Y 的欧几里得距离
        """
        return np.sum((X - Y) ** 2) ** 0.5

    def initialize(self, train_data, initializer):
        """
        初始化 n_clusters 个中心点
        parameter:
            train_data: 训练数据
            initializer: 初始化方法
        """
        n, dim = train_data.shape
        if initializer == "furthest":    # 先随机选一个点，之后迭代地选取与已选点集合最远的点作为初始样本中心
            self.means = np.zeros((self.n_clusters, dim))
            idx = np.random.randint(n)
            self.means[0] = train_data[idx]
            for i in range(1, self.n_clusters):
                min_dis = []    # min_dis 中存放每个点到距离它最近的已选点的距离
                for p in train_data:
                    min_dis.append(min([self.distance(p, self.means[k]) for k in range(i)]))
                self.means[i] = train_data[np.argmax(min_dis)]    # 选取距离最远的点作为新的中心点加入
        if initializer == "KMeansPlusPlus":    # K-Means++ 初始化，和上面的很类似，也是迭代地加入点。区别在于新点加入原则为离已选择初始点集合越远的点被选中加入的概率更大。即在第 2 种方法的基础上加入了概率选择。
            self.means = np.zeros((self.n_clusters, dim))
            idx = np.random.randint(n)
            self.means[0] = train_data[idx]
            for i in range(1, self.n_clusters):
                prob = []    # prob 中存放每个点被选中成为中心点的概率
                for p in train_data:    # 先计算每个点到距离它最近的已选点的距离
                    prob.append(min([self.distance(p, self.means[k]) for k in range(i)]))
                prob = np.array(prob)
                prob /= sum(prob)    # 归一化处理，将距离映射成被选中的概率
                self.means[i] = train_data[np.random.choice(range(n), p = prob.ravel())]    # 根据概率分布选择一个点成为新的中心点
        if initializer == "random":    # 随机地选取 n_cluster 个不重复的样本点作为初始样本中心
            idx = np.random.permutation(n)
            self.means = np.array(train_data[idx[:self.n_clusters]])

    def fit(self, train_data, max_iter = 100, initializer = "furthest"):
        """
        parameter:
            train_data: 训练数据
            max_iter: 最大迭代次数，默认为 100. 同时，如果在 100 轮迭代前已收敛，则提前结束.
            initializer： 初始化方法，可选择的初始化方法如下
                            "random": 随机化方法
                            "furthest": 最远点对方法 (默认)
                            "KMeansPlusPlus": KMeans++ 方法
        """
        # 获取训练集样本数和样本维数
        n, dim = train_data.shape

        # 标准化预处理
        train_data = normalize(train_data)

        # 先把训练数据随机打乱
        idx = np.random.permutation(n)
        train_data = train_data[idx]

        # 初始化 n_clusters 个随机聚类中心
        self.initialize(train_data, initializer)
        label = np.zeros(n)
        sse = []
        while True:
            isChange = False
            points = [[] for i in range(self.n_clusters)]    # points[i] 为第 i 个聚簇的样本点集合
            for i in range(n):
                minDis = self.distance(self.means[0], train_data[i])
                minIndex = 0
                for j in range(1, self.n_clusters):    # 对给定的样本点，找到距离其最近的聚簇中心点
                    nowDis = self.distance(self.means[j], train_data[i])
                    if nowDis < minDis:
                        minIndex = j
                        minDis = nowDis
                if label[i] != minIndex:    # 更新当前样本点所属的聚簇分类
                    isChange = True
                    label[i] = minIndex
                points[minIndex].append(train_data[i])
            ans = 0
            for i in range(self.n_clusters):    # 根据当前所有样本点的聚簇分类情况，更新聚簇中心
                self.means[i] = np.sum(points[i], axis = 0) / len(points[i])
                for j in range(len(points[i])):
                    ans += self.distance(self.means[i], points[i][j])
            sse.append(ans)
            if (isChange == False):
                break
            max_iter -= 1
            if (max_iter <= 0):
                break
        pass
    
    def predict(self, test_data):
        """
        parameter:
            test_data: 训练数据
        return:
            test_data 对应的 label 数组(numpy格式)
        """
        n, dim = test_data.shape
        test_data = normalize(test_data)    # 标准化预处理
        label = np.zeros(n)
        for i in range(n):
            minDis = self.distance(self.means[0], test_data[i])
            minIndex = 0
            for j in range(1, self.n_clusters):
                nowDis = self.distance(self.means[j], test_data[i])
                if nowDis < minDis:
                    minIndex = j
                    minDis = nowDis
            if label[i] != minIndex:
                label[i] = minIndex
        return label

def bisecting_kmeans(train_data, n_clusters):
    """
    通过 二分-Kmeans 做法确定聚簇中心点
    parameter:
        train_data: 训练数据
    return:
        train_data 对应的 label 数组(numpy格式)
    """
    # 获取训练集样本数和样本维数
    n, dim = train_data.shape

    # 标准化预处理
    train_data = normalize(train_data)
    origin_data = train_data

    # 先把训练数据随机打乱
    idx = np.random.permutation(n)
    train_data = train_data[idx]

    # 将整个数据集看成一个聚簇，并求得第一个聚簇中心
    center = [train_data.mean(axis = 0)[0]]
    cluster_sse = [sum([sum((x - center) ** 2) for x in train_data])]
    label = np.zeros(n)
    cnt = 0
    
    while len(center) < n_clusters:
        min_sse = 1e40
        chosen_cluster = 0
        new_center = np.zeros((2, dim))
        new_sse = []
        for i in range(len(center)):    # 枚举每个现有的聚簇，并尝试将其分裂
            cur = [k for k, x in enumerate(label) if x == i]
            model = KMeans(2)
            model.fit(train_data[cur])
            res = model.predict(train_data[cur])    # 通过 KMeans 模型将当前聚簇一分为二
            
            # 计算新的 SSE
            split_group = [[], []]
            for j in range(len(cur)):
                tag = int(res[j])
                split_group[tag].append(np.sum((train_data[cur[j]] - model.means[tag]) ** 2))
            split_sse = [sum(split_group[i]) / len(split_group[i]) for i in range(2)]

            sse_other = sum(cluster_sse) - cluster_sse[i]

            if sse_other + sum(split_sse) < min_sse:    # 取最小值，找到最优分裂方案
                min_sse = sse_other + sum(split_sse) 
                chosen_cluster = i
                new_center = model.means
                new_sse = split_sse
                new_cluster = [cur[k] for k, x in enumerate(res) if x == 1]
        
        # 根据当前的最优分裂方案，更新各聚簇中的点以及聚簇标签
        center[chosen_cluster] = new_center[0]
        center.append(new_center[1])
        cluster_sse[chosen_cluster] = new_sse[0]
        cluster_sse.append(new_sse[1])
        cnt += 1
        for idx in new_cluster:
            label[idx] = cnt
    
    label = np.zeros(n)
    for i in range(n):
        minDis = np.sum((center[0] - origin_data[i]) ** 2)
        minIndex = 0
        for j in range(1, n_clusters):
            nowDis = np.sum((center[j] - origin_data[i]) ** 2)
            if nowDis < minDis:
                minIndex = j
                minDis = nowDis
        if label[i] != minIndex:
            label[i] = minIndex
    return label

class GaussianMixture:

    def __init__(self, n_clusters, eps = 1e-30):
        """
        parameter:
            n_clusters: 聚类数目
            eps: 精度误差参数，默认为 1e-30
        """
        self.means = None
        self.pi = None
        self.sigma = None
        self.n_clusters = n_clusters
        self.eps = eps    # 精度误差参数
        self.train_data = None
        pass
    
    def gaussian(self, x, mean, cov):
        """
        parameter:
            x, mean, cov: 高斯函数的参数
        return：
            N(x|mean, cov)
        """
        cov = np.asmatrix(cov)
        delta = np.asmatrix(x - mean)
        num = - (0.5 * delta * cov.I * delta.T + self.eps)
        res = (1.0 / (2.0 * np.pi * (np.linalg.det(cov) ** 0.5)) * math.exp(num))
        return res
    
    def distance(self, X, Y):
        """
        parameter:
            X, Y: 两个 n 维向量
        return:
            X 和 Y 的欧几里得距离
        """
        return np.sum((X - Y) ** 2) ** 0.5
    
    def initialize(self, train_data, initializer):
        """
        初始化 n_clusters 个分布的相关参数
        parameter:
            train_data: 训练数据
            initializer: 初始化方法
        """
        n, dim = train_data.shape
        if initializer == "furthest":    # 先随机选一个点，之后迭代地选取与已选点集合最远的点作为初始样本钟信
            self.means = np.zeros((self.n_clusters, dim))
            idx = np.random.randint(n)
            self.means[0] = train_data[idx]
            for i in range(1, self.n_clusters):
                min_dis = []
                for p in train_data:
                    min_dis.append(min([self.distance(p, self.means[k]) for k in range(i)]))
                self.means[i] = train_data[np.argmax(min_dis)]
            self.pi = np.array([1 / self.n_clusters for i in range(self.n_clusters)])
            self.sigma = np.array([np.identity(dim) for i in range(self.n_clusters)])
        if initializer == "random":    # 随机选择样本点中的 n_clusters 个作为初始的分布中心
            idx = np.random.permutation(n)
            self.means = np.array(train_data[idx[:self.n_clusters]])
            self.pi = np.array([1 / self.n_clusters for i in range(self.n_clusters)])
            self.sigma = np.array([np.identity(dim) for i in range(self.n_clusters)])
        if initializer == "KMeans":    # 在 KMeans 聚类的基础上进行 GMM 分类
            # 通过 KMeans 得到每个样本点的分类情况
            model = KMeans(self.n_clusters)
            model.fit(train_data)
            label = model.predict(train_data)

            # 模仿 GMM 算法中的 M 步来初始化参数

            # 每个分布的中心就是 KMeans 方法得到的 n_clusters 个聚簇中心
            self.means = model.means
            # 每个样本点由第 k 个分布生成的概率 pi_k =【每个聚簇中的样本数/总样本】
            N = np.zeros(self.n_clusters)
            for i in range(n):
                N[int(label[i])] += 1
            sum_N = np.sum(N)
            self.pi = N / sum_N
            # 根据 M 步公式计算每个分布的协方差
            self.sigma = np.zeros((self.n_clusters, dim, dim))
            for j in range(n):
                for k in range(self.n_clusters):
                    delta = np.asmatrix(train_data[j] - self.means[k])
                    self.sigma[k] += delta.T * delta
            self.sigma = np.asarray([self.sigma[k] / N[k] for k in range(self.n_clusters)])
    
    def fit(self, train_data, max_iter = 30, initializer = "KMeans"):
        """
        parameter:
            train_data: 训练数据
            max_iter: 迭代轮次，默认为 30. 同时，如果在 100 轮迭代前已收敛，则提前结束.
            initializer： 初始化方法，可选择的初始化方法如下
                            "random" -- 随机化方法
                            "furthest" -- 最远点对方法 
                            "KMeans" -- KMeans初始化 (默认)
        """
        # 获取训练集样本数和样本维数
        n, dim = train_data.shape
        
        # 标准化预处理
        train_data = normalize(train_data)

        # 先把训练数据随机打乱
        idx = np.random.permutation(n)
        train_data = train_data[idx]
        
        # 根据指定的方法进行参数初始化
        self.initialize(train_data, initializer)

        last_func = -1
        
        for i in range(max_iter):
            # E 步，固定参数 means 和 sigma, 计算后验分布 p(z|x)
            gamma = np.zeros((n, self.n_clusters))
            p = np.zeros((n, self.n_clusters))

            for j in range(n):
                p[j] = [self.eps + self.pi[k] * self.gaussian(train_data[j], self.means[k], self.sigma[k]) for k in range(self.n_clusters)]
            
            for j in range(n):
                gamma[j] = p[j] / sum(p[j])

            # M 步，固定后验分布，更新参数 means 和 sigma

            # 根据公式更新 N_k = sum(gamma[n, k])   n = 1..N
            N = np.zeros(self.n_clusters)
            for j in range(n):
                N = np.asarray([N[k] + gamma[j, k] for k in range(self.n_clusters)])
            sum_N = np.sum(N)
            
            # 根据公式更新 pi_k = N_k / N
            self.pi = N / sum_N

             # 根据公式更新 mean_k = sum(gamma[n, k] * x[n]) / N_k   n = 1..N
            self.means = np.zeros((self.n_clusters, dim))
            for j in range(n):
                self.means = np.asarray([self.means[k] + gamma[j][k] * train_data[j] for k in range(self.n_clusters)])
            self.means = np.asarray([self.means[k] / N[k] for k in range(self.n_clusters)])

            # 根据公式更新 sigma_k^2 = sum(gamma[n, k] * (x[n] - mean_k)^2) / N_k   n = 1..N
            self.sigma = np.zeros((self.n_clusters, dim, dim))
            for j in range(n):
                for k in range(self.n_clusters):
                    delta = np.asmatrix(train_data[j] - self.means[k])
                    self.sigma[k] += gamma[j][k] * (delta.T * delta)
            self.sigma = np.asarray([self.sigma[k] / N[k] for k in range(self.n_clusters)])
            
            # 计算对数边际分布是否收敛，收敛则退出
            now_func = np.mean(np.log(p))
            if abs(now_func - last_func) / abs(last_func) < self.eps:
                break
            last_func = now_func
        pass
    
    def predict(self, test_data):
        """
        parameter:
            test_data: 训练数据
        return:
            test_data 对应的 label 数组(numpy格式)
        """
        n = test_data.shape[0]
        test_data = normalize(test_data)    # 标准化预处理
        label = np.zeros(n)
        for i in range(n):
            # 计算样本点属于每个分布的概率
            p = [self.pi[k] * self.gaussian(test_data[i], self.means[k], self.sigma[k]) for k in range(self.n_clusters)]
            # 取最大可能的分布作为当前样本的聚簇分类
            label[i] = np.argmax(np.array(p))
        return label

class ClusteringAlgorithm:

    def __init__(self, max_n_clusters = 20, model_method = KMeans, fit_method = "elbow"):
        """
        parameter:
            max_n_clusters: 最大聚类数目, 默认为 20
            model_method: 使用的聚类模型：KMeans(默认), GaussianMixture
            fit_method: 确定 K 值的方法："elbow"(默认), "silhouette"
            best_n_clusters: 拟合后得到的最佳聚簇数量，初始时为 None
        """
        self.max_n_clusters = max_n_clusters
        self.model_method = model_method
        self.fit_method = fit_method
        self.best_n_clusters = None
        pass

    def distance(self, X, Y):
        """
        parameter:
            X, Y: 两个 n 维向量
        return:
            X 和 Y 的欧几里得距离
        """
        return np.sum((X - Y) ** 2) ** 0.5

    def elbow_method(self, train_data):
        """
        根据肘方法进行拟合
        parameter:
            train_data: 训练数据
        return:
            data, (target_X, target_Y)] 用于可视化
            data: list 格式, 其中每一项对应给定 K 值下的聚类结果指标
            (target_X, target_Y)： tuple格式, target_X 为最优 K 值, target_Y 为取最优 K 值时对应的聚类结果指标
        """
        n, dim = train_data.shape
        r = np.zeros(self.max_n_clusters)
        for i in range(1, self.max_n_clusters):
            print(i)
            model = self.model_method(i)
            model.fit(train_data)
            label = model.predict(train_data)
            r[i] = 0
            for j in range(n):
                x = int(label[j])
                r[i] += self.distance(model.means[x], train_data[j])    # 计算每个聚簇的 SSE
        show_data = r
        r = r[1:self.max_n_clusters-1] - r[2:] 
        self.best_n_clusters = np.argmax(r[:self.max_n_clusters-3] / r[1:]) + 2    # 找到拐点
        return show_data[1:], (self.best_n_clusters, show_data[self.best_n_clusters])    # 返回可视化需要的数据

    def silhouette_score(self, train_data):
        """
        根据轮廓系数进行拟合
        parameter:
            train_data: 训练数据
        return:
            data, (target_X, target_Y)] 用于可视化
            data: list 格式, 其中每一项对应给定 K 值下的聚类结果指标
            (target_X, target_Y)： tuple格式, target_X 为最优 K 值, target_Y 为取最优 K 值时对应的聚类结果指标
        """
        n, dim = train_data.shape
        r = np.zeros(self.max_n_clusters)
        score = []
        for i in range(2, self.max_n_clusters):
            print(i)
            model = self.model_method(i)
            model.fit(train_data)
            label = model.predict(train_data)
            s = []
            for j in range(n):    # 计算第 j 个样本点的轮廓系数 s_j
                dis = np.zeros(i)
                N = np.zeros(i)
                for k in range(n):
                    if j == k:
                        continue
                    x = int(label[k])
                    dis[x] += self.distance(train_data[j], train_data[k])    # 计算当前样本点 j 和 聚簇标签为 x 的所有样本点 k 的距离总和
                    N[x] += 1    # 计算每一个标签下的样本数目（除去样本点 j)
                for k in range(i):
                    N[k] += 1
                dis /= N
                a = np.partition(dis, 0)[0]    # 与同一聚簇下其他样本的平均距离
                b = np.partition(dis, 1)[1]    # 与最近簇中所有样本的平均距离
                s.append((b - a) / max(a, b))    # 计算轮廓系数
            score.append(sum(s) / len(s))    # 计算平均轮廓系数
        self.best_n_clusters = np.argmax(score) + 2
        return score, (self.best_n_clusters, score[self.best_n_clusters - 2])    # 返回可视化需要的数据

    def fit(self, train_data):
        """
        根据已经选定的方法进行拟合
        parameter:
            train_data: 训练数据
        return:
            data, (target_X, target_Y)] 用于可视化
            data: list 格式, 其中每一项对应给定 K 值下的聚类结果指标
            (target_X, target_Y)： tuple格式, target_X 为最优 K 值, target_Y 为取最优 K 值时对应的聚类结果指标
        """
        # 标准化预处理
        train_data = normalize(train_data)

        if self.fit_method == "elbow":
            return self.elbow_method(train_data)
        if self.fit_method == "silhouette":
            return self.silhouette_score(train_data)

    def predict(self, test_data):
        """
        parameter:
            test_data: 测试数据
        return:
            test_data 对应的 label 数组(numpy格式)
        """
        # 标准化预处理
        test_data = normalize(test_data)

        model = self.model_method(self.best_n_clusters)
        model.fit(test_data)
        return model.predict(test_data)

def cal_acc(pred, res, K):
    """
    parameter:
        pred: 预测标签
        res: 数据集原始标签
        K: 数据集标签总数
    return:
        聚类预测的准确性
    """
    n = pred.shape[0]
    
    label_matrix = np.zeros((K, len(set(pred))))
    label_map = np.zeros(K)

    for i in range(n):    # 统计样本同时被贴上预测标签x和原始标签y的数量
        label_matrix[int(res[i])][int(pred[i])] += 1
    for i in range(K):    # 如果一个标签x频繁和另一个标签y一起出现，可以认为预测标签x和原始标签y是等价的
        label_map[i] = np.argmax(label_matrix[i])

    acc = 0
    for i in range(n):   # 根据得到的标签映射关系计算 acc
        if int(pred[i]) == int(label_map[res[i]]):
            acc += 1
    return acc / n

# 以下部分为模型实验和数据可视化
# if __name__ == "__main__":    
#     mean = np.array([
#         (6,4), (31, 44), (22, 6), (10, 29), (43, 25), (3, 17), (27, 15)
#     ])
#     cov = np.array([
#         [[11, 0], [0, 35]],
#         [[21, 0], [0, 24]],
#         [[25, 0], [0, 10]],
#         [[13, 0], [0, 10]],
#         [[3, 0], [0, 23]],
#         [[4, 0], [0, 10]],
#         [[30, 0], [0, 11]],
#     ])
#     num = [200 for i in range(mean.shape[0])]
#     X, Y = generate(mean, cov, num)

    # scatter_graph("./img1/KMeans_origin.png", X, Y)
    # acc_two = []
    # for i in range(10):
    #     res = bisecting_kmeans(X, mean.shape[0])
    #     acc_two.append(cal_acc(res, Y, mean.shape[0]))
    #     scatter_graph("./img1/KMeans{}_bisecting.png".format(i), X, res.tolist())
    # print(acc_two)
    # print(sum(acc_two) / len(acc_two))
    # curve_graph("./img1/KMeans_Bisecting.png", range(1, 11), [acc_two])

    # acc_random = []
    # for i in range(10):
    #     model = KMeans(mean.shape[0])
    #     model.fit(X, initializer = "random")
    #     res = model.predict(X)
    #     acc_random.append(cal_acc(res, Y, mean.shape[0]))
    #     scatter_graph("./img1/KMeans{}_random_initialization.png".format(i), X, res.tolist())
    # print(acc_random)
    # print(sum(acc_random) / len(acc_random))

    # acc_furthest = []
    # for i in range(10):
    #     model = KMeans(mean.shape[0])
    #     model.fit(X, initializer = "furthest")
    #     res = model.predict(X)
    #     acc_furthest.append(cal_acc(res, Y, mean.shape[0]))
    #     scatter_graph("./img1/KMeans{}_furthest_initialization.png".format(i), X, res.tolist())
    # print(acc_furthest)
    # print(sum(acc_furthest) / len(acc_furthest))

    # acc_kmeansplusplus = []
    # for i in range(10):
    #     model = KMeans(mean.shape[0])
    #     model.fit(X, initializer = "KMeansPlusPlus")
    #     res = model.predict(X)
    #     acc_kmeansplusplus.append(cal_acc(res, Y, mean.shape[0]))
    #     scatter_graph("./img1/KMeans{}_KMeansPlusPlus_initialization.png".format(i), X, res.tolist())
    # print(acc_kmeansplusplus)
    # print(sum(acc_kmeansplusplus) / len(acc_kmeansplusplus))
    
    # curve_graph("./img1/KMeans.png", range(1, 11), [acc_random, acc_furthest, acc_kmeansplusplus], label=["random", "furthest", "KMeans++"])

    # print("round:", test)
    # acc_KMeans= []
    # acc_random = []
    # acc_furthest = []
    # from tqdm import tqdm
    # for i in tqdm(range(10)):
    #     model = GaussianMixture(mean.shape[0])
    #     model.fit(X, initializer = "KMeans")
    #     res = model.predict(X)
    #     acc_KMeans.append(cal_acc(res, Y, mean.shape[0]))
        
    #     model = GaussianMixture(mean.shape[0])
    #     model.fit(X, initializer = "random")
    #     res = model.predict(X)
    #     acc_random.append(cal_acc(res, Y, mean.shape[0]))
        
    #     model = GaussianMixture(mean.shape[0])
    #     model.fit(X, initializer = "furthest")
    #     res = model.predict(X)
    #     acc_furthest.append(cal_acc(res, Y, mean.shape[0]))
    # print(acc_KMeans)
    # print(sum(acc_KMeans) / len(acc_KMeans))
    # scatter_graph("./img/GaussianMixture_KMeans_initialization{}.png".format(test), X, res.tolist())
    # print(acc_random)
    # print(sum(acc_random) / len(acc_random))
    # scatter_graph("./img/GaussianMixture_random_initialization{}.png".format(test), X, res.tolist())
    # print(acc_furthest)
    # print(sum(acc_furthest) / len(acc_furthest))
    # scatter_graph("./img/GaussianMixture_furthest_initialization{}.png".format(test), X, res.tolist())

    
    # curve_graph("./img/GaussianMixture{}.png".format(test), [acc_random, acc_furthest, acc_KMeans], label=["random", "furthest", "KMeans"])

    # model = GaussianMixture(mean.shape[0])
    # model.fit(X)
    # res = model.predict(X)
    # scatter_graph("GMM_result.png", X, res.tolist())

    
    # scatter_graph("./img/cluster.png", X, Y)
    
    # model = ClusteringAlgorithm(fit_method="silhouette")
    # a, b = model.fit(X)
    # res = model.predict(X)
    # print(cal_acc(res, Y, 7))
    # scatter_graph("./img/silhouette_result.png", X, res.tolist())
    # # curve_graph("./img/silhouette_single_curve.png", range(2, 20), [a], target = [b])

    
    # model = ClusteringAlgorithm(fit_method="elbow")
    # a, b = model.fit(X)
    # res = model.predict(X)
    # print(cal_acc(res, Y, 7))
    # scatter_graph("./img/elbow_result.png", X, res.tolist())
    # # curve_graph("./img/elbow_single_curve.png", range(1, 20), [a], target = [b])

    # elbow_acc = []
    # silhouette_acc = []
    # elbow_Y = []
    # silhouette_Y = []
    # elbow_target = []
    # silhouette_target = []
    # for i in range(10):
    #     model = ClusteringAlgorithm(fit_method="silhouette")
    #     a, b = model.fit(X)
    #     res = model.predict(X)
    #     elbow_acc.append(cal_acc(res, Y, 7))
    #     elbow_Y.append(a)
    #     elbow_target.append(b)
        
    #     model = ClusteringAlgorithm(fit_method="elbow")
    #     a, b = model.fit(X)
    #     res = model.predict(X)
    #     silhouette_acc.append(cal_acc(res, Y, 7))
    #     silhouette_Y.append(a)
    #     silhouette_target.append(b)
    
    # curve_graph("./img/silhouette_curve.png", range(2, 20), silhouette_Y, silhouette_target)
    # curve_graph("./img/elbow_curve.png", range(1, 20), elbow_Y, elbow_target)
    