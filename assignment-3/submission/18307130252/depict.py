import numpy as np
import matplotlib.pyplot as plt

color = [
    "red",
    "blue",
    "lightsteelblue",
    "mediumpurple",
    "gold",
    "lightgreen",
    "olive",
    "sandybrown",
    "green",
    "lightskyblue"
]
def test_color():
    
    n = len(color)
    print(len(color))
    for i in range(n):
        for j in range(20):
            plt.scatter(j, i, c = 'black', alpha = 0.3)
    plt.show()

def scatter_graph(filename, data, label, target = None):
    n = len(label)
    print(len(label))
    for i in range(n):
        plt.scatter(data[i][0], data[i][1], c = color[int(label[i])], alpha=0.3)
    if target is not None:
        n = target.shape[0]
        for i in range(n):
            plt.scatter(target[i][0], target[i][1], marker = 'x', c = 'black')
    plt.savefig(filename)
    plt.close()

def scatter_graph_3D(filename, data, label, target = None):
    n = len(label)
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = Axes3D(fig)
    for i in range(n):
        if label[i] >= 10:
            ax.scatter(data[i][0], data[i][1], data[i][2], c = label[i], alpha=0.3)
        else:
            ax.scatter(data[i][0], data[i][1], data[i][2], c = color[int(label[i])], alpha=0.3)
    if target is not None:
        n = target.shape[0]
        for i in range(n):
            ax.scatter(target[i][0], target[i][1], target[i][2], marker = 'x', c = 'black')
    plt.savefig(filename)
    plt.close()

def curve_graph(filename, x, data, x_label = None, y_label = None, target = None, label = None):
    if x_label is not None:
        plt.xlabel(x_label)
    if y_label is not None:
        plt.ylabel(y_label)
    sz = len(data)
    for i in range(sz):
        if label is None:
            plt.plot(x, data[i], color = color[i], marker='o',)
        else:
            plt.plot(x, data[i], color = color[i], marker='o', label=label[i])
    if target is not None:
        for i in range(sz):
            plt.plot(target[i][0], target[i][1], color = 'black', marker='o')
    plt.xticks(x,x[::1])
    # plt.show()
    if label is not None:
        plt.legend()
    plt.savefig(filename)
    plt.close()

def generate(mean, cov, num):
    n, dim = mean.shape
    data = []

    for i in range(n):
        data.append(np.random.multivariate_normal(mean[i], cov[i], num[i]))
        
    X = np.concatenate([x for x in data], axis = 0)
    Y = np.concatenate([[i] * num[i] for i in range(n)], axis = 0)

    idx = np.random.permutation(sum(num))
    X = X[idx]
    Y = Y[idx]

    return X, Y
