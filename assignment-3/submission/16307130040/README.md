# 实验报告3



## 1，KMeans 和 GaussianMixture 聚类算法的实现



#### 1，KMeans的实现



首先，是KMeans的实现。在fit之前要初始化，我的做法是随机选择数据中的n_clusters个点作为初始的聚簇中心。

训练的实现较为简单，在limit变量规定的次数中，反复进行以下两步：



1，为数据的每一个点寻找最近的聚簇中心，并将它归于聚簇中。最终，输出一个表示每个点所属的聚簇的数组idx。这一步由方法FindClosestCenters(self,train_data)实现。



2，将每个聚簇中所有点的坐标进行平均，最终得出新的聚簇中心。这一步由方法ComputeNewCenters(self,idx,train_data)实现。

其中，聚簇中心存储在类的私有变量centers中。



预测的时候，使用的逻辑与FindClosestCenters相似。不同的是，输入的数据又训练数据变成了测试数据。



#### 2，GaussianMixture的实现



之后，是 GaussianMixture 的实现。初始的方法与Kmeans相同。



在训练中，GaussianMixture类使用了以下数据结构：

*self.mean*：每个聚簇的平均值，格式为k*n。
*self.var*:每个聚簇的方差，格式为k*n。在计算概率的时候，会将方差展开为对角矩阵。
*self.W*：每个数据属于不同的聚簇的可能性,格式为m*k。W[i][j]为第i个数据属于第j个聚簇的概率。
*self.Pi*：对随机的一个数据，它属于各个聚簇的可能性够成的数组。可以由W计算得出，但是为了计算方便，使用了独立的Pi变量。



在训练中，GaussianMixture类会在limit变量规定的次数中，反复进行以下两步：



##### 1，E步

在E步中，我们会更新聚簇的分布。使用贝叶斯公式来重新计算每个数据属于各个聚簇的概率：

![](.\img\1.png)

之后，再更新pi变量：

![](.\img\2.png)

代码如下：

```python
    def step_E(self,train_data):
        #update W matrix
        m, n = train_data.shape
        new_W=np.zeros((m,self.k))

        for i in range(self.k):
            for j in range(m):
                new_W[j,i]=self.Pi[i]*self.gauss_prob(train_data[j],self.mean[i],np.diag(self.var[i]))

        for j in range(m):
            sum=new_W[j].sum()
            if sum != 0:
                self.W[j]=new_W[j]/new_W[j].sum()
        self.Pi = self.W.sum(axis=0) / self.W.sum()
```

这里用到了高斯分布的概率公式。这里使用了numpy进行实现，并在计算前提前考虑方差为0的情况：

```python
    def gauss_prob(self,x,mean,var):
        #calculate the probability of gauss distribution by np
        if not np.any(var):
            n=var.shape[0]
            var=np.diag(np.ones(n)/100)

        y = np.exp((-1 / 2) * (x - mean).T.dot(np.linalg.inv(var)).dot(x - mean)) / np.sqrt(
            np.power(2 * np.pi, len(x)) * np.linalg.det(var)
        )
        return y
```



##### 2，M步

在M步中，更新每个聚簇的均值和方差。具体说来，实现方法是基于W矩阵，计算数据坐标的均值和方差的均值:

![](.\img\3.png)

![](.\img\4.png)

以下是代码：

```python
    def step_M(self,train_data):
        m, n = train_data.shape

        #update the mean and var of each cluster
        new_mean=np.zeros((self.k,n))
        new_var=np.zeros((self.k,n))
        for i in range(self.k):
            new_mean[i]=np.average(train_data,axis=0,weights=np.array(self.W[:,i]))
            new_var[i]=np.average((train_data-new_mean[i])**2, axis=0, weights=np.array(self.W[:, i]))
        self.mean=new_mean
        self.var=new_var
```



预测的时候，将测试数据代入每一个聚簇的高斯概率公式中进行计算，得出该数据点属于各类的概率。最终，选取概率最大的聚簇。



## 2，基础实验

基础实验中，我生成了如下2700个数据，属于三个二维高斯分布模型：

```python
    mean = (1, 2)
    cov = np.array([[80, 0], [0, 22]])
    data_x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[40, 0], [0, 32.1]])
    data_y = np.random.multivariate_normal(mean, cov, (900,))

    mean = (-6, 10)
    cov = np.array([[20, 10], [10, 20]])
    data_z = np.random.multivariate_normal(mean, cov, (1000,))
```

之后，混合，贴上标签，并洗匀之后，将其中80%作为训练数据，20%作为测试数据。

这是训练数据：

![](.\img\test1_1.png)

这是测试数据：

![](.\img\test1_2.png)

然后，通过KMeans模型进行聚类操作：

![](.\img\test1_3.png)

之后，用GaussianMixture模型进行聚类：

![](.\img\test1_4.png)

通过上方的准确率可以看出，高斯混合模型比KMeans的表现要好。

因为聚类模型采用的标签和数据原来的标签很有可能不一致，所以如果简单地进行一一比对的话，是不会得出准确的准确率的。所以，使用这样的方法来计算准确率：将聚类模型采用的标签不断地进行轮换，再一一比较，计算出每一个轮换中的准确率，并将最高的准确率作为这个模型的准确率。



```python
def calculate_accu(label_predict,label_test):
    #when calculate the accu ,try all probables of label combination
    rotates=np.array([[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]])
    label_predict_rotated=[]
    for rotate in rotates:
        label_predict_temp=np.array(label_predict)
        label_predict_temp=np.where(label_predict_temp != 0, label_predict_temp, -1)
        label_predict_temp=np.where(label_predict_temp != 1, label_predict_temp, -2)
        label_predict_temp=np.where(label_predict_temp != 2, label_predict_temp, -3)

        label_predict_temp=np.where(label_predict_temp != -1, label_predict_temp, rotate[0])
        label_predict_temp=np.where(label_predict_temp != -2, label_predict_temp, rotate[1])
        label_predict_temp=np.where(label_predict_temp != -3, label_predict_temp, rotate[2])
        label_predict_rotated.append(label_predict_temp)

    correct_max = np.count_nonzero((label_predict == label_test))
    for label_predict_temp in label_predict_rotated:
        correct = np.count_nonzero((label_predict_temp == label_test))
        if correct>correct_max:
            correct_max=correct

    accurate= correct_max/len(label_test)
    return  accurate
```

该实验所有的代码在test1.py中。

## 3， 自动选择聚簇数量的实验



首先说明选择聚簇数的思路。每当选择聚簇数，并训练数据之后，计算出每个聚簇的直径的平均数。一般来说，随着聚簇的数量增加，平均直径会减小。但是，在达到某一个值之后，直径的下降会明显缓慢，有时反而会微量地增加。这个临界值就是最佳的聚簇数。

该实验中，生成了如下2700个数据，属于三个二维高斯分布模型：

```python
    mean = (-10, 2)
    cov = np.array([[6, 0], [0, 12]])
    data_x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (-10, -5)
    cov = np.array([[14, 0], [0, 14]])
    data_y = np.random.multivariate_normal(mean, cov, (900,))

    mean = (0, 0)
    cov = np.array([[15, 14], [14, 15]])
    data_z = np.random.multivariate_normal(mean, cov, (1000,))

```

之后，混合，贴上标签，并洗匀之后，将其中80%作为训练数据，20%作为测试数据。

这是训练数据：

![](.\img\test2_1.png)

这是测试数据：

![](.\img\test2_2.png)

之后，用自动选择聚簇数的KMeans模型进行聚类：

![](.\img\test2_3.png)

这里模型自动认为这组数据可以分为三类。



不过，当三个类靠得更近一些的时候，会发生一些不同的情况：

```python
    mean = (2, 2)
    cov = np.array([[6, 0], [0, 12]])
    data_x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (-5, -5)
    cov = np.array([[7, 2], [2, 7]])
    data_y = np.random.multivariate_normal(mean, cov, (900,))

    mean = (0, 0)
    cov = np.array([[5, 0], [0, 5]])
    data_z = np.random.multivariate_normal(mean, cov, (1000,))
```

![](.\img\test2_4.png)

![](.\img\test2_5.png)

此时，模型将数据分为了四个聚簇：

![](.\img\test2_7.png)



在观察后，发现这种分类方式的确更为合适。



该实验所有的代码在test2.py中。