import numpy as np
import matplotlib.pyplot as plt
from source import KMeans, GaussianMixture


def train_test_split(data, label):
    offset = int(len(data) * 0.8)
    data_train = data[:offset]
    data_test = data[offset:]
    label_train = label[:offset]
    label_test = label[offset:]


    return np.array(data_train), np.array(data_test), np.array(label_train), np.array(label_test)

def display(data, label,name,accu):
    datas =[[],[],[]]
    colors=['b','r','y']
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    for i,each in enumerate(datas):
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1],c=colors[i])
    if not accu==0:
        plt.text(-10, 20, 'accurate=%f'%accu)
    plt.title(name)
    plt.show()

def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label

def calculate_accu(label_predict,label_test):
    #when calculate the accu ,try all probables of label combination
    rotates=np.array([[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]])
    label_predict_rotated=[]
    for rotate in rotates:
        label_predict_temp=np.array(label_predict)
        label_predict_temp=np.where(label_predict_temp != 0, label_predict_temp, -1)
        label_predict_temp=np.where(label_predict_temp != 1, label_predict_temp, -2)
        label_predict_temp=np.where(label_predict_temp != 2, label_predict_temp, -3)

        label_predict_temp=np.where(label_predict_temp != -1, label_predict_temp, rotate[0])
        label_predict_temp=np.where(label_predict_temp != -2, label_predict_temp, rotate[1])
        label_predict_temp=np.where(label_predict_temp != -3, label_predict_temp, rotate[2])
        label_predict_rotated.append(label_predict_temp)

    correct_max = np.count_nonzero((label_predict == label_test))
    for label_predict_temp in label_predict_rotated:
        correct = np.count_nonzero((label_predict_temp == label_test))
        if correct>correct_max:
            correct_max=correct

    accurate= correct_max/len(label_test)
    return  accurate


def test1():
    mean = (1, 2)
    cov = np.array([[80, 0], [0, 22]])
    data_x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[40, 0], [0, 32.1]])
    data_y = np.random.multivariate_normal(mean, cov, (900,))

    mean = (-6, 10)
    cov = np.array([[20, 10], [10, 20]])
    data_z = np.random.multivariate_normal(mean, cov, (1000,))

    data, label = shuffle(data_x,data_y,data_z)
    data_train,data_test,label_train,label_test=train_test_split(data,label)

    display(data_train,label_train,'train',0)
    display(data_test,label_test,'test',0)

    model_KMeans=KMeans(3)
    model_KMeans.fit(data_train)
    label_predict_KMeans=np.array(model_KMeans.predict(data_test),dtype=np.int32)
    accurate_Kmeans=calculate_accu(label_predict_KMeans,label_test)
    display(data_test,label_predict_KMeans,'predict_Kmeans',accurate_Kmeans)


    model_GaussianMixture=GaussianMixture(3)
    model_GaussianMixture.fit(data_train)
    label_predict_GaussianMixture=np.array(model_GaussianMixture.predict(data_test),dtype=np.int32)
    accurate_GaussianMixture=calculate_accu(label_predict_GaussianMixture,label_test)
    display(data_test,label_predict_GaussianMixture,'predict_GaussianMixture',accurate_GaussianMixture)






if __name__ == "__main__":
    test1()