import numpy as np

class KMeans:

    def __init__(self, n_clusters):
        self.limit=50
        self.labels=[]
        self.centers=[]
        self.k=n_clusters



    def FindClosestCenters(self,train_data):

        m=train_data.shape[0]
        idx=np.zeros(m)
        # for each point,find the closest center
        for i in range(m):
            min_dist=np.sum((train_data[i] - self.centers[0]) ** 2)
            idx[i]=0
            for j in range(self.k):
                dist=np.sum((train_data[i] - self.centers[j]) ** 2)
                if dist < min_dist:
                    min_dist=dist
                    idx[i]=j
        return idx


    def ComputeNewCenters(self,idx,train_data):
        m, n = train_data.shape
        for i in range(self.k):
            indices = np.where(idx == i)
            # for each center ,find the points select it

            if len(indices[0]) != 0:
                self.centers[i]=np.sum(train_data[indices], axis=0) / len(indices[0])



    def ComputeTheDiameter(self,train_data):
        # calculate the average of each clusters after fit the train data
        diameters=[]
        idx=self.FindClosestCenters(train_data)
        for i in range(self.k):
            indices = np.where(idx == i)
            data_temp=train_data[indices]
            if len(indices[0]) != 0:
                    diameter_max=0
                    for data1 in data_temp:
                        for data2 in data_temp:
                            diameter=sum((data1 - data2) ** 2)
                            if diameter>diameter_max:
                                diameter_max=diameter

                    diameters.append(diameter_max)
        return np.average(diameters)




    def fit(self, train_data):
        # initialize the centers, which are  just same to k random points of  data at first
        m, n = train_data.shape
        self.centers = np.zeros((self.k, n))
        select_idx = np.random.randint(0, m, self.k)
        for i in range(self.k):
            self.centers[i] = train_data[select_idx[i]]

        for i in range(self.limit):
            idx=self.FindClosestCenters(train_data)
            self.ComputeNewCenters(idx,train_data)




    
    def predict(self, test_data):
        m,n=test_data.shape
        predict_idx=np.zeros(m)
        # similar to function above
        for i in range(m):
            min_dist=np.sum((test_data[i] - self.centers[0]) ** 2)
            predict_idx[i]=0
            for j in range(self.k):
                dist=np.sum((test_data[i] - self.centers[j]) ** 2)
                if dist < min_dist:
                    min_dist=dist
                    predict_idx[i]=j
        return  predict_idx

class GaussianMixture:

    def __init__(self, n_clusters):
        # k is the number of clusters , each one set of mean and var is related to one cluster
        # W is for hidden variable ,W（i，j) is the probability for Xi belongs to cluster j
        self.k=n_clusters
        self.mean=[]
        self.var=[]
        self.W=[]
        self.Pi=[]
        self.limit=5

    def gauss_prob(self,x,mean,var):
        #calculate the probability of gauss distribution by np
        if not np.any(var):
            n=var.shape[0]
            var=np.diag(np.ones(n)/100)

        y = np.exp((-1 / 2) * (x - mean).T.dot(np.linalg.inv(var)).dot(x - mean)) / np.sqrt(
            np.power(2 * np.pi, len(x)) * np.linalg.det(var)
        )
        return y

    def step_E(self,train_data):
        #update W matrix
        m, n = train_data.shape
        new_W=np.zeros((m,self.k))

        for i in range(self.k):
            for j in range(m):
                new_W[j,i]=self.Pi[i]*self.gauss_prob(train_data[j],self.mean[i],np.diag(self.var[i]))

        for j in range(m):
            sum=new_W[j].sum()
            if sum != 0:
                self.W[j]=new_W[j]/new_W[j].sum()
        self.Pi = self.W.sum(axis=0) / self.W.sum()
        #print(self.W)



    def step_M(self,train_data):
        m, n = train_data.shape

        #update the mean and var of each cluster
        new_mean=np.zeros((self.k,n))
        new_var=np.zeros((self.k,n))
        for i in range(self.k):
            new_mean[i]=np.average(train_data,axis=0,weights=np.array(self.W[:,i]))
            new_var[i]=np.average((train_data-new_mean[i])**2, axis=0, weights=np.array(self.W[:, i]))
        self.mean=new_mean
        self.var=new_var
        #print(self.mean)

    def ComputeTheDiameter(self,train_data):
        diameters=[]
        idx=self.predict(train_data)
        for i in range(self.k):
            indices = np.where(idx == i)
            data_temp=train_data[indices]
            if len(indices[0]) != 0:
                    diameter_max=0
                    for data1 in data_temp:
                        for data2 in data_temp:
                            diameter=sum((data1 - data2) ** 2)
                            if diameter>diameter_max:
                                diameter_max=diameter
                    #print(diameter_max)
                    diameters.append(diameter_max)
        return np.average(diameters)


    def fit(self, train_data):
        # initialize the variables above
        m,n=train_data.shape
        self.mean=np.zeros((self.k,n))
        select_idx = np.random.randint(0, m, self.k)
        for i in range(self.k):
            self.mean[i] = train_data[select_idx[i]]
        self.var=np.ones((self.k,n))
        self.W=np.ones((m,self.k))/self.k
        self.Pi = self.W.sum(axis=0) / self.W.sum()

        for i in range(self.limit):
            self.step_E(train_data)
        #    if(m==3):
        #        print('W: ',self.W)
            self.step_M(train_data)
        #    if (m == 3):
        #        print('mean：',self.mean)
        #        print('var: ',self.var)




    
    def predict(self, test_data):
        m,n=test_data.shape
        predict_idx=np.zeros(m)
        # similar to function above
        for i in range(m):
            max_prob=self.gauss_prob(test_data[i],self.mean[0],np.diag(self.var[0]))
            predict_idx[i]=0
            for j in range(self.k):
                prob=self.gauss_prob(test_data[i],self.mean[j],np.diag(self.var[j]))
                if max_prob < prob:
                    max_prob=prob
                    predict_idx[i]=j
        #print(predict_idx)
        return  predict_idx

class ClusteringAlgorithm:


    def __init__(self):
        self.k=2
        self.model_Kmeans=KMeans(2)

    def fit(self, train_data):
        last_diameter=0
        last_model = KMeans(2)
        selected_k=2
        selected_model=KMeans(2)
        # try the differnet n_clusters in order
        for i in range(2,31):
            #print(i)
            model_Kmeans= KMeans(i)
            model_Kmeans.fit(train_data)
            if i == 2:
                last_diameter=model_Kmeans.ComputeTheDiameter(train_data)
                last_model=model_Kmeans

            else:
                diameter=model_Kmeans.ComputeTheDiameter(train_data)
                # when the average diameter of clusters is not decreased sharply , then select the previous n_cluster and Kmeans model
                if diameter>last_diameter*0.80:
                    selected_k=i-1
                    selected_model=last_model
                    break
                else:
                    last_diameter=diameter
                    last_model=model_Kmeans
        self.k=selected_k
        self.model_Kmeans=selected_model
    
    def predict(self, test_data):
        return self.k,self.model_Kmeans.predict(test_data)


