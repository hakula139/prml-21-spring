import numpy as np
import matplotlib.pyplot as plt
from source import KMeans, GaussianMixture,ClusteringAlgorithm


def train_test_split(data, label):
    offset = int(len(data) * 0.8)
    data_train = data[:offset]
    data_test = data[offset:]
    label_train = label[:offset]
    label_test = label[offset:]


    return np.array(data_train), np.array(data_test), np.array(label_train), np.array(label_test)

def display(data, label,name,n_clusters):
    # the display function of test2,which is vaild in different numbers of clusters
    datas =[[] for i in range(n_clusters)]

    colors=['b','r','y','xkcd:neon purple','xkcd:deep green','xkcd:reddish pink','aliceblue','xkcd:electric green','xkcd:royal']
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    for i,each in enumerate(datas):
        each = np.array(each)
        plt.scatter(each[:, 0], each[:, 1],c=colors[i])
    #if not accu==0:
    #    plt.text(-10, 20, 'accurate=%f'%accu)
    plt.title(name)
    plt.show()


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label




def test2():
    mean = (2, 2)
    cov = np.array([[6, 0], [0, 12]])
    data_x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (-5, -5)
    cov = np.array([[7, 2], [2, 7]])
    data_y = np.random.multivariate_normal(mean, cov, (900,))

    mean = (0, 0)
    cov = np.array([[5, 0], [0, 5]])
    data_z = np.random.multivariate_normal(mean, cov, (1000,))

    data, label = shuffle(data_x,data_y,data_z)
    data_train,data_test,label_train,label_test=train_test_split(data,label)

    display(data_train,label_train,'train',3)
    display(data_test,label_test,'test',3)

    model_KMeans=ClusteringAlgorithm()
    model_KMeans.fit(data_train)
    n_clusters, label_predict_KMeans = model_KMeans.predict(data_test)
    label_predict_KMeans=np.array(label_predict_KMeans,dtype=np.int32)
    display(data_test,label_predict_KMeans,'predict_Kmeans',n_clusters)













if __name__ == "__main__":
    test2()