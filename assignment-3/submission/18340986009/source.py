import numpy as np
import matplotlib.pyplot as plt

import sklearn.metrics



def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int) * i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_2d(ns, means, covs, train_pct=.8):
    # Generate data sets and labels
    d_list = []
    l_list = []
    for i in range(len(ns)):
        d = np.random.multivariate_normal(means[i], covs[i], (ns[i],))
        d_list.append(d)
        l_list.append(np.repeat(i, ns[i]))
    data = np.concatenate(d_list)
    label = np.concatenate(l_list)

    # Shuffle
    N = np.sum(ns)
    cut = int(np.ceil(N * train_pct))
    idx = np.arange(N)
    np.random.shuffle(idx)

    data = data[idx]
    label = label[idx]

    return (data, data), len(ns), label


def find_dist(x, y):
    return np.sqrt(np.matmul(x - y, x - y))


def plot_cluster(test_data, result, assigned_means, true_means):
    # Plot model result
    plt.scatter(test_data[:, 0], test_data[:, 1],
                c=result, alpha=.6)
    # Plot model assigned means
    plt.scatter(assigned_means[:, 0], assigned_means[:, 1],
                c='red', marker='^', s=70)
    # Plot true means
    plt.scatter(true_means[:, 0], true_means[:, 1],
                c='black', marker='^', s=70)
    plt.show()


class KMeans:

    def __init__(self, n_clusters, n_iter=10):
        self.k = n_clusters
        self.iter = n_iter
        self.centroids = None

    def fit(self, train_data):
        # INITIALIZE centroids
        idx = np.random.choice(len(train_data), self.k, replace=False)
        self.centroids = train_data[idx]

        # ITERATE for a specified number of times to update the centroids
        centroids_record = [self.centroids]
        for _ in range(self.iter):
            # ASSIGN the nearest centroid to every point
            groups = []
            for point in train_data:
                dists = []
                for centroid in self.centroids:
                    dist = find_dist(point, centroid)
                    dists.append(dist)
                group = np.argmin(dists)
                groups.append(group)

            # UPDATE centroids based on assigned points
            new_centroids = []
            for group in np.unique(groups):
                idx = [v == group for v in groups]
                new_centroid = np.mean(train_data[idx], axis=0)
                new_centroids.append(new_centroid)
            self.centroids = new_centroids
            centroids_record.append(np.array(new_centroids))

    def predict(self, test_data):
        # ASSIGN test data points to their nearest centroid
        groups = []
        for point in test_data:
            dists = []
            for centroid in self.centroids:
                dist = find_dist(point, centroid)
                dists.append(dist)
            group = np.argmin(dists)
            groups.append(group)

        return np.array(groups)


class GaussianMixture:

    def __init__(self, n_clusters, n_iter=5):
        self.k = n_clusters
        self.centroids = None
        self.covs = None
        self.pis = None
        self.iter = n_iter
        self.epsilon = 10 ** (-5)

    def __Normal__(self, x, miu, cov):
        # If data is 1-D
        if cov.shape == (1, 1):
            c = 1 / (np.sqrt(2 * np.pi) * np.sqrt(cov))
            md = ((x - miu) / np.sqrt(cov)) ** 2 / (-2)

        # If data is > 1-D
        else:
            c = 1 / np.sqrt((2 * np.pi) ** cov.shape[0] * np.linalg.det(cov))
            md = np.dot(np.dot((x - miu).T, np.linalg.inv(cov)), (x - miu)) / (-2)

        return np.max([c * np.exp(md), self.epsilon])

    def fit(self, train_data):
        # SPLIT train_data into k sub-data sets
        N = len(train_data)
        sub_ds = np.array_split(train_data, self.k)

        # Randomly add another point if there exists only one number in an array
        for i in range(len(sub_ds)):
            if len(sub_ds[i]) == 1:
                idx = np.random.choice(N - 1, 1)
                dum = sub_ds[i].tolist()
                dum.append(train_data[idx].tolist()[0])
                sub_ds[i] = np.array(dum)

        # INITIALIZE means, covariances, and pis for sub-data sets
        self.pis = [1 / self.k for _ in range(self.k)]
        self.centroids = [np.mean(v, axis=0) for v in sub_ds]
        self.covs = []
        for i in range(self.k):
            # If data is 1-D
            if sub_ds[i].shape[0] != 1 and sub_ds[i].shape[1] == 1:
                cov = np.array([[np.var(sub_ds[i])]])
            # If data is > 1-D
            else:
                cov = np.cov(sub_ds[i].T)
            self.covs.append(cov)

        # ITERATE for a specified number of times to update parameters
        for _ in range(self.iter):
            # E STEP
            # CALCULATE r matrix
            r_mat = np.zeros((N, self.k))
            for n in range(N):
                for k in range(self.k):
                    num = self.pis[k] * self.__Normal__(train_data[n], self.centroids[k], self.covs[k])
                    dum = [self.pis[k] * self.__Normal__(train_data[n], self.centroids[k], self.covs[k])
                           for k in range(self.k)]
                    denom = np.sum(dum)
                    r_mat[n][k] = num / denom
            Nks = np.sum(r_mat, axis=0)

            # M STEP
            # UPDATE pis
            self.pis = [Nks[k] / N for k in range(self.k)]

            # UPDATE mean parameter
            self.centroids = np.zeros((self.k, len(train_data[0])))
            for k in range(self.k):
                for n in range(N):
                    self.centroids[k] += r_mat[n][k] * train_data[n]
            self.centroids = [self.centroids[k] / Nks[k] for k in range(self.k)]

            # UPDATE covariance parameter
            self.covs = [np.zeros((len(train_data[0]), len(train_data[0]))) for _ in range(self.k)]
            for k in range(self.k):
                for n in range(N):
                    v = (train_data[n] - self.centroids[k])
                    self.covs[k] += r_mat[n][k] * np.outer(v, v)
            self.covs = [self.covs[k] / Nks[k] for k in range(self.k)]

    def predict(self, test_data):
        groups = []
        # ASSIGN test data points to centroids with their maximum conditional probability
        for n in range(len(test_data)):
            ps = [self.__Normal__(test_data[n], self.centroids[k], self.covs[k]) for k in range(self.k)]
            group = np.argmax(ps)
            groups.append(group)

        return np.array(groups)


class ClusteringAlgorithm:

    def __init__(self, Ks, method='KMeans'):
        self.Ks = Ks
        self.method = method
        self.optimum_K = None
        self.centroids = None

    def __plot__(self, K, S):
        plt.figure(figsize=(16, 8))
        plt.plot(K, S, 'bx-')
        plt.axvline(x=self.optimum_K, linestyle='--', color='r')
        plt.xlabel('K')
        plt.title('Elbow Method')
        plt.show()

    def fit(self, train_data):
        Scores_of_Ks = []

        # ITERATE over all K candidates
        for j in range(len(self.Ks)):
            # Train Model & Obtain result
            if self.method == 'KMeans':
                model = KMeans(self.Ks[j])
            if self.method == 'GMM':
                model = GaussianMixture(self.Ks[j])
            model.fit(train_data)
            result = model.predict(train_data)

            # CALCULATE cumulative sum for each K
            cumsum = 0
            for i in range(len(train_data)):
                centroid = model.centroids[result[i]]
                cumsum += find_dist(centroid, train_data[i])
            Scores_of_Ks.append(cumsum)

        # FIND Optimum K
        d = np.zeros((len(self.Ks), 1))
        for i in range(len(self.Ks)):
            y = (Scores_of_Ks[-1] - Scores_of_Ks[0]) / (self.Ks[-1] - self.Ks[0]) * \
                (self.Ks[i] - self.Ks[0]) + Scores_of_Ks[0]
            d[i] = y - Scores_of_Ks[i]

        self.optimum_K = self.Ks[np.argmax(d)]

        # VISUALIZE
        self.__plot__(self.Ks, Scores_of_Ks)

    def predict(self, test_data):

        if self.method == 'KMeans':
            model = KMeans(self.optimum_K)
        if self.method == 'GMM':
            model = GaussianMixture(self.optimum_K)

        model.fit(test_data)
        self.centroids = model.centroids
        result = model.predict(test_data)

        return result

    def eval(self, test_data, true_label, true_means, assigned_label):
        N = len(self.centroids)

        # FIND the corresponding groups
        corresponds = dict.fromkeys(np.unique(true_label))
        for g in range(N):
            flt = true_label[assigned_label == g]
            score = [len(flt[flt == m]) for m in range(N)]
            corresponds[g] = np.argmax(score)
        pred_label = np.array([corresponds[v] for v in assigned_label])

        # Plot model result
        wrong_pts = test_data[true_label != pred_label]
        plt.scatter(test_data[:, 0], test_data[:, 1],
                    c='grey', alpha=.6)
        # Plot wrong points
        plt.scatter(wrong_pts[:, 0], wrong_pts[:, 1],
                    c='red', alpha=.8)
        # Plot model assigned means
        plt.scatter([v[0] for v in self.centroids], [v[1] for v in self.centroids],
                    c='purple', marker='^', s=70)
        # Plot true means
        plt.scatter(true_means[:, 0], true_means[:, 1],
                    c='black', marker='^', s=70)
        plt.show()

        return assigned_label


if __name__ == '__main__':
    # 基础实验====================================================
    ns = np.array([300, 300, 300])

    means = np.array([
        [20, 20],
        [35, 25],
        [25, 35]])

    covs = np.array([
        [[20, 5], [5, 35]],
        [[45, 0], [0, 20]],
        [[20, 0], [0, 15]]])

    (train_data, test_data), n_clusters, test_label = data_2d(ns, means, covs)

    # K-Means
    model = KMeans(n_clusters, n_iter=10)
    model.fit(train_data)
    res = model.predict(test_data)
    plot_cluster(test_data, res,
                 np.array([v.tolist() for v in model.centroids]),
                 means)

    # GMM
    model = GaussianMixture(n_clusters, n_iter=10)
    model.fit(train_data)
    res = model.predict(test_data)
    plot_cluster(test_data, res,
                 np.array([v.tolist() for v in model.centroids]),
                 means)

    # 自动选择聚簇数量实验===========================================
    Ks = [1, 2, 3, 4, 5, 6, 7, 8]

    table = np.zeros((len(Ks), 2))
    for k in range(len(Ks)):
        print(k)
        cut = int(800 / (1 + k))

        ns = np.array([cut, 800 - cut])

        means = np.array([
            [20, 20],
            [40, 20]])

        covs = np.array([
            [[20, 5], [5, 35]],
            [[45, 0], [0, 20]]])

        (train_data, test_data), n_clusters, test_label = data_2d(ns, means, covs)

        model = ClusteringAlgorithm(Ks, method='KMeans')
        model.fit(train_data)
        print('optimum K')
        print(model.optimum_K)
        res = model.predict(test_data)
        # plot_cluster(test_data, res,
        #              np.array([v.tolist() for v in model.centroids]),
        #              means)

        assigned_label = model.eval(test_data, test_label, means, res)
        rand_s = sklearn.metrics.adjusted_rand_score(test_label, assigned_label)
        silhouette_s = sklearn.metrics.silhouette_score(test_data, assigned_label)
        table[k, 0] = rand_s
        table[k, 1] = silhouette_s
