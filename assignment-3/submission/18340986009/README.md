# Clustering Algorithm (K-Means & GMM)

Below is true for all the following analysis:
 - Data used: 2-D, Gaussian distributions with given means and variance-covariance matrix.
 - Default number of iteration is set to 10.
 - Centroids are initialized by randomly selecting k points from the train dataset.

## 1. Algorithm Realization

Main purpose of this section is to validify the models used for clustering. Dataset below is used for model training.

$
  N_0 = 300 \hspace{1cm}
  C_0 \sim \mathcal{N}(\mu = \begin{bmatrix}20\\\\20\end{bmatrix}\,\sigma^{2} = \begin{bmatrix}20 & 5\\\\5 & 35\end{bmatrix})\, 
$

$
  N_1 = 300 \hspace{1cm}
  C_1 \sim \mathcal{N}(\mu = \begin{bmatrix}35\\\\25\end{bmatrix}\,\sigma^{2} = \begin{bmatrix}45 & 0\\\\0 & 20\end{bmatrix})\, 
$

$
  N_2 = 300 \hspace{1cm}
  C_2 \sim \mathcal{N}(\mu = \begin{bmatrix}25\\\\35\end{bmatrix}\,\sigma^{2} = \begin{bmatrix}20 & 0\\\\0 & 15\end{bmatrix})\, 
$

<img src="img/Fig1.png" width=400 height=300/>

### 1.1 K-Means

Steps：

     1. Initialize centroids by random selection
     For n_iter times:
         2. Assign every point to their nearest centroid
         3. Recalculate Means based on the assigned points 

Result:

<img src="img/Fig2.png" width=400 height=300/>

Red points indicate centroids assigned by our model. They are very close the the actual mean in this example.

### 1.2 GMM

Steps:

    1. Initialize distribution parameters by spliting data into k sub-datasets and perform calculation
    For n_iter times:
        2. E-Step, calculate matrix
        3. M-Step, update parameters (means, variance-covariance, pis)

<img src="img/Fig3.png" width=400 height=300/>

Red points indicate centroids assigned by our model. They are further away from the actual means compared with that of K-Means.

## 2. K-optimizating Experiment

Elbow method:
Choose K by evaluating the cost (measured by the sum of squared distance from the assigned centroid for every point) of adding every new centroid.

### 2.1 Elbow Method Demonstration

Dataset below is used in this section:

$
  N_0 = 400 \hspace{1cm}
  C_0 \sim \mathcal{N}(\mu = \begin{bmatrix}20\\\\20\end{bmatrix},\,\sigma^{2} = \begin{bmatrix}20 & 5\\\\5 & 35\end{bmatrix})\, 
$

$
  N_1 = 400 \hspace{1cm}
  C_1 \sim \mathcal{N}(\mu = \begin{bmatrix}40\\\\20\end{bmatrix},\,\sigma^{2} = \begin{bmatrix}45 & 0\\\\0 & 20\end{bmatrix})\, 
$

Candicates of K = [1, 2, 3, 4, 5, 6].

For each candidate, run a K-Means model, calculate the sum of squared distance from its centroid for every cluster, and plot the sum of squares to help determine the optimum K.

The optimum K is found automatically by finding the maximum distance between the line of sum-of-squares and the straight line passing through the starting and ending point of the sum-of-squares.

**Elbow Line Plot**

<img src="img/Fig4.png" width=450 height=300/>

**Result From Model**

<img src="img/Fig5.png" width=400 height=300/>

**Misclassified Points**

<img src="img/Fig6.png" width=400 height=300/>

*A limitation of elbow method is that the optimum K will never be the first and last candidate of K, so make sure to be less restricting on choosing the range of K.

### 2.2 Effect of data balancing on Clustering Algorithm

**The effect of data-balancing on Elbow mthod**

Using the same means and variance-covariance matrix in 2.1, we'll investigate if changing the ratios of the number of points between two clusters has any effect on our algorithm.

ratios tested = [1/1, 1/2, 1/3, 1/4, 1/5, 1/6, 1/7]

**Metrics for model evaluation:**

Adjusted Rand Index:

    Let A be a clustering result with clusters {a1, a2, ..., ar}
    Let B be a clustering result with clusters {b1, b2, ..., bs}
    Then given a (r,n) contingency table where the values of each cell in the ith, jth position is nij, 
    the Adjusted Rand Index (ARI) is calculated as:

<img src="img/Fig7.png" width=250 height=40/>

Silhouette Coefficient:
    
    Let a be The mean distance between a sample and all other points in the same class
    Let b be the mean distance between a sample and all other points in the next nearest cluster
    Then for every point: (Silhouette Coefficient for the whole dataset is taking the mean of all s)

<img src="img/Fig8.png" width=300 height=50/>

**Result**:

|       \      | 1:1 | 1:2 | 1:3 | 1:4 | 1:5 | 1:6 | 1:7 |
| ------------ |:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| opt K        | 2 | 2 | 3 | 3 | 3 | 3 | 3 | 3 |
| Adjusted Rand Index| .796|.845|.386|.346|.305|.287|.277|
| Silhouette Index| .558| .557| .390| .400| .385| .356| .358|

The optimum K found using Elbow Method is 3 for the last 5 cases, which is not the actual number of cluster. Thus, for data 1 ratio less than 1/2, ARI, a measure similar to accuracy drops significantly. Clustering result became less similar to the actual clustering pattern. Silhouette index is roughly equal for 1:1 and 1:2 and equal for the rest of the cases. This difference can also be related to the difference on the choice of K. As two centroids are assigned to a cluster of points which actually shares the same centroid, the separateness between the two clusters becomes small.

<img src="img/Fig9-1.png" width=200 height=150/>

<img src="img/Fig9-2.png" width=200 height=150/>

<img src="img/Fig9-3.png" width=200 height=150/>

<img src="img/Fig9-4.png" width=200 height=150/>

<img src="img/Fig9-5.png" width=200 height=150/>

<img src="img/Fig9-6.png" width=200 height=150/>

<img src="img/Fig9-7.png" width=200 height=150/>


```python

```
