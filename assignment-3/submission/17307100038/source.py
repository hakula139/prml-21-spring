import numpy as np

class KMeans:

    def __init__(self, n_clusters, max_iter = 500):
        self.n_clusters = n_clusters
        self.max_iter = max_iter
        self.labels = None
        self.centers = None


    def initcenters(self, k, dataSet):
        # 初始化每个k个中心
        m = dataSet.shape[0]
        shape  = list(dataSet.shape)
        shape[0] = k
        centers = np.zeros(shape)
        xrange = np.r_[0:m]
        for i in range(0,k):
            index = int(np.random.choice(xrange, 1))
            xrange = np.delete(xrange, index)  # 不放回抽样，防止两个点初始化在同一个位置
            centers[i,:] = dataSet[index, :]
        return centers

    def euc(self, vec1, vec2):
        distance = np.sum(pow(vec1 - vec2, 2))
        return distance

    def fit(self, train_data):
        self.centers = self.initcenters(self.n_clusters, train_data)  # 初始化点
        m = train_data.shape[0]
        dismatrix = np.mat(np.zeros((m, 2)))  # 生成一分矩阵记录label与distance

        for iteration in range(self.max_iter):
            for i in range(0,m):
                # 初始化一个距离
                n = train_data[i, :].reshape(-1)
                minDistance = pow(train_data[i, :].max(), 2) *n.shape[0]
                minIndex = 0

                # 循环获得最近的center
                for j in range(0,self.n_clusters):
                    distance = self.euc(self.centers[j, :], train_data[i, :])
                    if distance < minDistance:
                        minDistance = distance
                        minIndex = j
                dismatrix[i, :] = minIndex, minDistance
            # 更新中心
            for j in range(0,self.n_clusters):
                pointsInCluster = train_data[np.nonzero(dismatrix[:,0].A == j)[0]]  # 选取所有该类别的点
                self.centers[j, :] = np.mean(pointsInCluster, axis=0)  # 去这些点的中心

    def predict(self, test_data):
        m_td = test_data.shape[0]
        labels = np.zeros(m_td)
        for i in range(0,m_td):
            dist = [self.euc(self.centers[j, :], test_data[i, :]) for j in range(self.n_clusters)]  # 这个点与每个center的距离

            labels[i] = dist.index(min(dist))  # 最近点为对应的label
        return labels


class GaussianMixture:

    def __init__(self, n_components, max_iter=300):
        self.n_componets = n_components
        self.max_iter = max_iter
        self.pi = [1 / self.n_componets for each in range(self.n_componets)]  # 初始化PI
        self.tail = 1e-10

    def multivariate_normal(self, train_data, mean_vector, covariance_matrix):
        # 计算多元正态分布
        return (2 * np.pi) ** (-len(train_data) / 2) * np.linalg.det(covariance_matrix) ** (-1 / 2) * np.exp(
            -np.dot(np.dot((train_data - mean_vector).T, np.linalg.inv(covariance_matrix)),
                    (train_data - mean_vector)) / 2)

    def normal(self, train_data, mean, covariance):
        # 正态分布
        return (1/((2*np.pi) ** 0.5 * covariance + 1e-10)) * np.exp(-(train_data-mean)**2/(2*covariance**2 + 1e-10))


    def fit(self, train_data):
        # 初始化均值、方差
        new_X = np.array_split(train_data, self.n_componets)
        self.mean_vector = [np.mean(x, axis=0) for x in new_X]
        self.covariance_matrixes = [np.cov(x.T) for x in new_X]
        del new_X  # 删除节省内存

        for iteration in range(self.max_iter):
            '''E-step '''
            self.gamma = np.zeros((len(train_data), self.n_componets))  # 初始化gamma系数矩阵
            for n in range(len(train_data)):  # 计算每个gamma的值
                for k in range(self.n_componets):
                    self.gamma[n][k] = self.pi[k] * self.multivariate_normal(train_data[n], self.mean_vector[k],
                                                                             self.covariance_matrixes[k])
                    self.gamma[n][k] /= (sum([self.pi[j] * self.multivariate_normal(train_data[n], self.mean_vector[j],
                                                                                   self.covariance_matrixes[j]) for j in
                                             range(self.n_componets)]) + self.tail)
            N = np.sum(self.gamma, axis=0)

            '''M-step'''
            # 更新均值
            self.mean_vector = np.zeros((self.n_componets, len(train_data[0])))  # 初始化一个用于存放mean值
            for k in range(self.n_componets):  # 计算每个值
                for n in range(len(train_data)):
                    self.mean_vector[k] += self.gamma[n][k] * train_data[n]
            self.mean_vector = [1 / N[k] * self.mean_vector[k] for k in range(self.n_componets)]
            # 更新方差
            for k in range(self.n_componets):
                self.covariance_matrixes[k] = np.cov(train_data.T, aweights=(self.gamma[:, k]), ddof=0)
            self.covariance_matrixes = [1 / N[k] * self.covariance_matrixes[k] for k in range(self.n_componets)]
            self.pi = [N[k] / len(train_data) for k in range(self.n_componets)]

    def predict(self, test_data):
        # 计算每个点属于各类的概率
        probas = []
        for n in range(len(test_data)):
            probas.append([self.multivariate_normal(test_data[n], self.mean_vector[k], self.covariance_matrixes[k])
                           for k in range(self.n_componets)])
        labels = np.argmax(np.array(probas), axis=1)  # 计算所属的类别
        return labels


class ClusteringAlgorithm:

    def __init__(self):
        self.n_clusters = 2
        self.model = None

    def line(self, pointA, pointB):
        length = np.sqrt(np.sum(pow(pointA - pointB, 2)))
        return length

    def area(self, lineA, lineB, lineC):
        s = (lineA + lineB + lineC) / 2
        area = (s * (s - lineA) * (s - lineB) * (s - lineC)) ** 0.5
        return area

    def fit(self, train_data, high=5):
        ''' 对每个k值对应的聚类结果，计算每个点距离中心的距离之和。再以elbow规则判别最优的k值。其中k值选取距离头尾两种结果连线最远的点'''
        # 计算每个的结果与类中心距离之和
        res_list, num_list = [], []
        for i in range(2, high + 1):
            model = KMeans(i, max_iter=100)
            model.fit(train_data)
            res = model.predict(train_data)

            centers = model.centers
            inertia = 0
            for j in range(len(centers)):
                new_data = train_data[np.where(res == j)]  # 获得这个类别的所有点
                center = centers[j]
                center = np.tile(center, len(new_data)).reshape(len(new_data), 2)
                ss = np.sum(np.sqrt(pow(new_data - center, 2)))
                inertia += ss
            res_list.append(inertia)
            num_list.append(i)

        # plt.plot(num_list,res_list)
        # plt.show()

        # 计算选取拐点
        point1 = np.array([num_list[0], res_list[0]])
        point2 = np.array([num_list[-1], res_list[-1]])
        linec = self.line(point1, point2)
        area_list = []
        for i in range(3, high):
            pointx = np.array([num_list[i - 2], res_list[i - 2]])
            linea, lineb = self.line(point1, pointx), self.line(point2, pointx)
            area = self.area(linea, lineb, linec)
            area_list.append(area)

        self.n_clusters = area_list.index(max(area_list)) + 3  # 取最大值所在位置+3
        # print(self.n_clusters)
        self.model = KMeans(self.n_clusters)
        self.model.fit(train_data)

    def predict(self, test_data):
        return self.model.predict(test_data)
