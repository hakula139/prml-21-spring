import numpy as np
import sys

from source import KMeans, GaussianMixture


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_1():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z)
    return (data, data), 3


def data_2():
    train_data = np.array([
        [23, 12, 173, 2134],
        [99, -12, -126, -31],
        [55, -145, -123, -342],
    ])
    return (train_data, train_data), 2


def data_3():
    train_data = np.array([
        [23],
        [-2999],
        [-2955],
    ])
    return (train_data, train_data), 2


def test_with_n_clusters(data_fuction, algorithm_class):
    (train_data, test_data), n_clusters = data_fuction()
    model = algorithm_class(n_clusters)
    model.fit(train_data)
    res = model.predict(test_data)
    assert len(
        res.shape) == 1 and res.shape[0] == test_data.shape[0], "shape of result is wrong"
    return res


def testcase_1_1():
    test_with_n_clusters(data_1, KMeans)
    return True


def testcase_1_2():
    res = test_with_n_clusters(data_2, KMeans)
    return res[0] != res[1] and res[1] == res[2]


def testcase_2_1():
    test_with_n_clusters(data_1, GaussianMixture)
    return True


def testcase_2_2():
    res = test_with_n_clusters(data_3, GaussianMixture)
    return res[0] != res[1] and res[1] == res[2]


def test_all(err_report=False):
    testcases = [
        ["KMeans-1", testcase_1_1, 4],
        ["KMeans-2", testcase_1_2, 4],
        # ["KMeans-3", testcase_1_3, 4],
        # ["KMeans-4", testcase_1_4, 4],
        # ["KMeans-5", testcase_1_5, 4],
        ["GMM-1", testcase_2_1, 4],
        ["GMM-2", testcase_2_2, 4],
        # ["GMM-3", testcase_2_3, 4],
        # ["GMM-4", testcase_2_4, 4],
        # ["GMM-5", testcase_2_5, 4],
    ]
    sum_score = sum([case[2] for case in testcases])
    score = 0
    for case in testcases:
        try:
            res = case[2] if case[1]() else 0
        except Exception as e:
            if err_report:
                print("Error [{}] occurs in {}".format(str(e), case[0]))
            res = 0
        score += res
        print("+ {:14} {}/{}".format(case[0], res, case[2]))
    print("{:16} {}/{}".format("FINAL SCORE", score, sum_score))


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "--report":
        test_all(True)
    else:
        test_all()
