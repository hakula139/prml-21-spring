# Assignment 3

## 1.KMeans

KMeans，首先随机化得到的k个中心。再每个点按照距离各中心远近划分类别，在对每个类更新新的中心。循环上述过程至收敛。

本实验中的KMean类包含以下函数：

```markdown
model.fit(train_data)  #主程序，对各中心初始化，循环训练模型

model.predict(test_data) # 返回一个数组，表示各个数据点的分类

model.initcenters(k,dataSet) #初始化得到各个中心
```

以test_demo中的data1为例，训练结果如下图：

![kmean_1](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-3/submission/17307100038/img/kmean_1.jpg)

更改数据集，对应data2的均值、方差如下：
$$
\mu =
  \left[
 \begin{matrix}
   4 &4
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -1 & 11
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -8 & 10
  \end{matrix}
  \right]
  
$$

$$
\Sigma = 
 \left[
 \begin{matrix}
   9 & 0 \\
   0 & 9 
  \end{matrix}
  \right]
 \Sigma = 
 \left[
 \begin{matrix}
   1.5 & 0 \\
   0 & 1.5 
  \end{matrix}
  \right]
  \Sigma = 
 \left[
 \begin{matrix}
   1.5 & 0 \\
   0 & 1.5 
  \end{matrix}
  \right]
$$

对应结果如图：

![kmean_2](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-3/submission/17307100038/img/kmean_2.jpg)

更改数据集，对应data3的均值、方差如下：
$$
\mu =
  \left[
 \begin{matrix}
   5&10
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   -5 & -2
  \end{matrix}
  \right]
   \mu =
  \left[
 \begin{matrix}
   10 & -5
  \end{matrix}
  \right]
$$

$$
\Sigma = 
 \left[
 \begin{matrix}
   9 & 6 \\
   8 & 5 
  \end{matrix}
  \right]
 \Sigma = 
 \left[
 \begin{matrix}
   9 & 9 \\
   12 & 5 
  \end{matrix}
  \right]
  \Sigma = 
 \left[
 \begin{matrix}
   9 & 9 \\
   16 & 25 
  \end{matrix}
  \right]
$$

对应的结果：

![kmnea_3](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-3/submission/17307100038/img/kmnea_3.png)

## 2.GMM

GMM模型用em算法。

e步优化参数gamma，m步优化均值、方差与pi。本次实验中对应的函数如下：

```markdown
model.fit(train_data)  #主程序，对各中心初始化，循环训练模型

model.predict(test_data) # 返回一个数组，表示各个数据点的分类

multivariate_normal(train_data,mean_vector, covariance_matrix)  #生成多元正态分布

normal（train_data, mean, covariance) #生成正态分布
```

### 3.ClusteringAlgorithm

本类对KMeans实现k值的选取。选取的方式如下：

1、对应取值范围内的每个K值：

​	1.1分别训练KMeans模型， 

​	1.2 对每个KMeans模型中的点，计算与中心的距离之和intertia

2.绘制k - inertia曲线图，以elbow规则选取最优值k，自动化选取方法为：

​	2.2 选取第一个点与最后一个点连线为基准线

​	2.2 计算其余每个点与基准线构成三角形面积

​	2.3 选取面积最大对应的k值，即elbow的拐点

以data1为数据集，模型迭代次数500次，k值范围在2-5对应的k-inertia值如下图

![1](https://gitee.com/xyhdsg10022/prml-21-spring/raw/master/assignment-3/submission/17307100038/img/1.jpg)

对应的模型自动选择k值为3，与人工判别相同。

