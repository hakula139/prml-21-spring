

# 课程报告

这是一个有关numpy实现K-Means以及GMM的实验报告，代码保存在<u>source.py</u>中，实验结果展示使用了matplotlib库。

## 可视化聚类分析

### 数据生成

数据生成采用了`numpy.random.multivariate_normal()`方法，可以在给定参数的情况下生成多元二维高斯分布，示例如下：

<img src="img/data_generation.png" style="zoom:72%;" />

下面实验中所用到的数据均由此方法产生。

### K-Means

#### 原理简介

K-Means算法是一类原型聚类方法，此类方法假定据类结构可以通过一组原型刻画，一般来说，算法先对原型进行初始化，之后对原型进行迭代更新求解，不同的原型聚类方法在原型表示、求解方式上有所不同。

在原型表示方式上，K-Means利用均值向量作为簇的原型；在求解方式上，给定样本集 $D=\{\pmb x_1,\pmb x_2,...,\pmb x_m\}$ ，假设算法得到的一个簇划分为 $C=\{C_1,C_2,...,C_k\}$ ，此划分应当满足平方误差最小，即使
$$
E=\sum^k_{i=1}\sum_{\pmb x \in C_i} || \pmb x - \pmb \mu_i ||^2_2
\tag{1}
$$
最小，其中 $\pmb \mu_i$ 是簇 $C_i$ 的均值向量。显然，想要找到这样的簇结构需要考察样本集的所有可能的划分，而这则是一个NP难问题，因此往往采用贪心策略，使用迭代优化来近似求解，当迭代更新后，聚类结果不变或者均值向量不变时停止迭代。

#### 数据预处理

考虑到输入数据中数据的分布可能比较分散，在计算均值向量及距离时很有可能出现因为数据太大而溢出的现象。因此在进行聚类前，笔者对数据的进行了标准化处理，代码如下：

```python
def normalization(X):
    """
    对数据进行标准化处理，使其均值为0，方差为1
    :param X: 输入数据
    :return: 标准化后的数据
    """
    mean = np.mean(X, axis=0)
    std = np.std(X, axis=0)
    X = (X - mean) / std
    return X
```

#### 初始化对聚类的影响分析

笔者使用了随机选取均值向量的方法确定各个簇的原型，这一做法在分布较为均衡的样本上表现很好，基本可以得到良好的聚类结果，但是如果样本分布十分不均衡时，随机选取的均值向量属于容量较少的类的可能就会降低，从而影响聚类结果，下图中笔者在两组实验中使用了相同的均值和协方差均值作为分布的参数，调整了生成数据时每个分布产生的样本数量，随机选取向量的代码如下：

```python
idx = np.arange(num) # num是样本的数量
mean_vec_idx = np.random.choice(idx, self.n_clusters, replace=False)  # self.n_clusters是簇的个数，replace=False表示抽样不重复
mean_vec = train_data[mean_vec_idx]	# 初始均值向量
```

参数如下：

<center>
  表1 参数取值
</center>

| 生成簇名 | 均值                   | 协方差矩阵                                                   | 组1数量 | 组2数量 | 组3数量 |
| -------- | ---------------------- | ------------------------------------------------------------ | ------- | ------- | ------- |
| C1       | $\pmb \mu_1= [50, 22]$ | $\pmb \Sigma_1=\left[\begin{array}{cc}10 & 5 \\\\5 & 10\end{array}\right]$ | 100     | 100     | 100     |
| C2       | $\pmb \mu_2= [16, -5]$ | $\pmb \Sigma_2=\left[\begin{array}{cc}21.2 & 0 \\\\0 & 32.1\end{array}\right]$ | 100     | 100     | 1000    |
| C3       | $\pmb \mu_3= [10, 20]$ | $\pmb \Sigma_3=\left[\begin{array}{cc}73 & 0 \\\\0 & 22\end{array}\right]$ | 100     | 1000    | 10000   |

<center>
  表2 聚类过程与结果
</center>

<table>
  <tr>
    <th>Generation</th>
    <th>First Iteration</th>
    <th>Result</th>
  </tr>
  <tr>
    <td><img src="img/init11.png" width="1000px" /></td>
    <td><img src="img/init12.png" width="1000px" /></td>
    <td><img src="img/init13.png" width="1000px" /></td>
  </tr>
  <tr>
    <td><img src="img/init21.png" width="1000px" /></td>
    <td><img src="img/init22.png" width="1000px" /></td>
    <td><img src="img/init23.png" width="1000px" /></td>
  </tr>
  <tr>
    <td><img src="img/init31.png" width="1000px" /></td>
    <td><img src="img/init32.png" width="1000px" /></td>
    <td><img src="img/init33.png" width="1000px" /></td>
  </tr>
</table>

实验结果表明，当原始簇的数量比较均衡时，即使初始向量分布比较集中，也能够经过逐次迭代最终返回一个比较合理的簇分化；但是当原始簇的数量分布不均衡时，初始向量对最终的聚类结果会产生很大的影响，比如导致数量较少的原始簇被并入数量较大的原始簇中，尽管这两个簇可能在分布上差距较大；当各簇之间数量悬殊过大时，即使能够生成理想的初始向量，聚类结果也难以令人满意。对于后一个问题，GMM模型可以很好地解决，这里先不予分析；对于前一个问题，则需要对初始均值向量的选取做一定的限定

一种避免初始向量分布集中的做法是迭代挑选均值向量，使得新的均值向量距离已有向量足够远。K-Means++采用了以依据距离确定选取均值向量概率的做法，使得距离簇中心越远的样本被选为新中心的概率越大，基本思路如下：

1. 随机选取第一个均值向量
2. 计算每个样本到最近的均值向量的距离
3. 以距离为概率选取新的均值向量
4. 重复2、3，直到选出k个均值向量

代码如下：

```python
def initialize(self):
    """
    初始化均值向量，KMeans++方法
    :return: 初始化后的均值向量
    """
    # n, d, k分别是样本数量，维度以及簇数
    n = self.data.shape[0]
    d = self.data.shape[1]
    k = self.n_clusters
    # 随机选取第一个均值向量
    idx = np.random.randint(0, n)
    # 初始化均值向量存储矩阵
    mean_vec = np.zeros((k, d))
    mean_vec[0] = self.data[idx].reshape(1, -1)
    # 初始化每个样本到均值向量的距离存储矩阵
    dist = np.zeros((n, k))
    # 将每个样本到第一个向量的距离填入矩阵中
    dist[:, 0] =  np.linalg.norm(self.data - mean_vec[0].reshape(1, -1), axis=1)
    # 迭代选取剩余的向量
    for i in range(1, k):
        # 保留每个样本到距离其最近的类中心的距离，距离都保存在dist矩阵的前i列中
        dist_min = np.min(dist[:, :i], axis=1)
        # 在保留的距离中选取距离最大的一个作为新的均值向量
        new_vec_idx = np.argmax(dist_min)
        # 将此步产生的向量并入
        mean_vec[i] = self.data[new_vec_idx].reshape(1, -1)
        # 计算每个样本到新向量的距离，并存入距离矩阵
        dist[:, i] = np.linalg.norm(self.data - mean_vec[i], axis=1)
    return mean_vec
```

对于上述实验中的组2，在更换了初始化方式后，聚类结果直观上有了很大的改善：

<center>
  表3 KMeans++方法初始化的组2
</center>

<table>
  <tr>
    <th>Generation</th>
    <th>First Iteration</th>
    <th>Result</th>
  </tr>
  <tr>
    <td><img src="img/init21.png" width="1000px" /></td>
    <td><img src="img/init41.png" width="1000px" /></td>
    <td><img src="img/init42.png" width="1000px" /></td>
  </tr>
</table>

### GMM

#### 原理简介

GMM也是一类原型聚类向量，但与KMeans不同的是，其采用概率模型而非原型向量来刻画聚类结构。下面对GMM模型的原理进行梳理，推导过程参考周志华《机器学习》。

GMM算法假设样本由高斯混合分布生成，高斯混合分布的定义如下：
$$
\begin {aligned}
P(\pmb x) &=
\frac 1 {(2\pi)^{\frac n 2} |\pmb \Sigma|^{\frac 1 2}}
e^{-\frac 1 2 
(\pmb x-\pmb \mu)^T |\pmb \Sigma|^{-1}(\pmb x-\pmb \mu)}
\\\\
P_{M}(\pmb x) &= \sum^k_{i=1}\alpha_i \cdot P(\pmb x|\pmb \mu_i, \pmb \Sigma_i)
\end{aligned}
\tag{2}
$$
生成的过程为根据混合系数的先验分布选择高斯混合成分，再根据被选择的混合成分的概率密度函数进行采样生成响应的样本。

根据贝叶斯定理可以计算样本属于每个混合成分的后验概率：
$$
\begin{aligned}
P_M(z_j=i|\pmb x_j) 
&= \frac{P(z_j=i) \cdot P_M(\pmb x_j|z_j=i)}{P_M(\pmb x_j)}\\\\
&=\frac {\alpha_i \cdot P(\pmb x_j|\pmb \mu_i,\pmb \Sigma_i)} {\sum^k_{l=1}\alpha_l \cdot P(\pmb x_j|\pmb \mu_l,\pmb \Sigma_l)}
\\\\
&=\gamma_{ji}
\end{aligned}
\tag{3}
$$
其中，$z_j=i$ 表示第j个样本属于第i个高斯混合成分，将此后验概率记为 $\gamma_{ji}$ 。依据此后验概率分布，可以将样本划归其对应后验概率最大的混合成分。

可以使用极大似然法对模型参数进行估计：
$$
\begin{aligned}
LL(D)&=\ln \biggl(\prod^m_{j=1} P_M(\pmb x_j) \biggl)\\\\
&=\sum^m_{j=1} \ln 
\biggl(
\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)
\biggl)
\end{aligned}
\tag{4}
$$
那么寻找一个最优的簇划分的问题则转变为最大化（4）式的问题。

对（4）式对参数 $\pmb \mu_i, \pmb \Sigma_i$ 的偏导进行推导：

对 $\pmb \mu_i$ :
$$
\begin{aligned}
&\frac{\partial LL(D)}{\partial \pmb \mu_i}=0 \\\\
\iff& 
\frac{\partial}{\partial \pmb \mu_i}
\sum^m_{j=1} \ln 
\biggl(
\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)
\biggl)
=0 \\\\
\iff&
\sum^m_{j=1} 
\frac{\partial}{\partial \pmb \mu_i}
\ln 
\biggl(
\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)
\biggl)
=0 \\\\
\iff&
\sum^m_{j=1} 
\frac {1}{\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}
\frac{\partial}{\partial \pmb \mu_i}
\biggl(
\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)
\biggl)
=0 \\\\
\iff&
\sum^m_{j=1} 
\frac {\alpha_i}{\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}
\frac{\partial}{\partial \pmb \mu_i}
\biggl(
P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)
\biggl)
=0
\end{aligned}
\tag{5}
$$

其中
$$
\begin{aligned}
\frac{\partial}{\partial \pmb \mu_i}
\biggl(
P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)
\biggl)
&=\frac{\partial}{\partial \pmb \mu_i}
\biggl(
\frac 1 {(2\pi)^{\frac n 2} |\pmb \Sigma_i|^{\frac 1 2}}
e^{-\frac 1 2 
(\pmb x_j-\pmb \mu_i)^T \pmb \Sigma_i^{-1}(\pmb x_j-\pmb \mu_i)}
\biggl)
\\\\
&=
\frac 1 {(2\pi)^{\frac n 2} |\pmb \Sigma_i|^{\frac 1 2}}
e^{-\frac 1 2 
(\pmb x-\pmb \mu_i)^T \pmb \Sigma_i^{-1}(\pmb x-\pmb \mu_i)}
\\\\
&\ \cdot\frac{\partial}{\partial \pmb \mu_i}
\biggl(
-\frac 1 2
(\pmb x_j-\pmb \mu_i)^T \pmb \Sigma_i^{-1}(\pmb x_j-\pmb \mu_i)
\biggl)
\\\\
&=
P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)\\\\
& \cdot
\biggl(
-\frac 1 2 
\biggl)
\frac{\partial}{\partial \pmb \mu_i}
\biggl(
(\pmb x_j-\pmb \mu_i)^T \pmb \Sigma_i^{-1}(\pmb x_j-\pmb \mu_i)
\biggl)
\end{aligned}
\tag{6}
$$

根据二次型矩阵求导公式
$$
\frac
{\partial x^TAx}
{\partial x}
=2Ax
\tag{7}
$$

有
$$
\frac{\partial}{\partial \pmb \mu_i}
\biggl(
(\pmb x_j-\pmb \mu_i)^T \pmb \Sigma_i^{-1}(\pmb x_j-\pmb \mu_i)
\biggl)
=-2
\pmb \Sigma_i^{-1}
(\pmb x_j-\pmb \mu_i)
\tag{8}
$$

将(6)、(8)带回(5)，有
$$
\sum^m_{j=1} 
\frac 
{\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}{\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}
\pmb \Sigma_i^{-1}
(\pmb x_j-\pmb \mu_i)
=0
\tag{9}
$$

因为 $|\pmb \Sigma_i|^{-1}$ 左边是一个标量，因此可以在两边左乘 $|\pmb \Sigma_i|$ ，则有
$$
\sum^m_{j=1} 
\frac 
{\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}{\sum^k_{i=1}\alpha_i \cdot P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}
(\pmb x_j-\pmb \mu_i)
=0
\tag{10}
$$

由式(3)有
$$
\pmb \mu_i = 
\frac
{\sum^m_{j=1} \gamma_{ji} \pmb x_j}
{\sum^m_{j=1} \gamma_{ji}}
\tag{11}
$$

也就是均值向量可以通过样本的加权平均来估计，样本权重是每个样本属于该成分的后验概率。
同理可以求出协方差矩阵（参考《机器学习》）：
$$
\pmb \Sigma_i
=\frac
{\sum^m_{j=1}\gamma_{ji}(\pmb x_j-\pmb \mu_i)(\pmb x_j-\pmb \mu_i)^T}
{\sum^m_{j=1}\gamma_{ji}}
\tag{12}
$$

又因为优化LL(D)建立在混合系数之和为1的条件下，因此使用拉格朗日乘子法：
$$
LL(D)+\lambda
\biggl(
\sum^k_{i=1}\alpha_i-1
\biggl)
\tag{13}
$$

对混合系数求导：
$$
\sum^m_{j=1} 
\frac 
{P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}{\sum^k_{l=1}\alpha_l \cdot P(\pmb x_l|\pmb \mu_i, \pmb \Sigma_l)}
+\lambda
=0
\tag{14}
$$

对此式在两边乘以αi，并且对所有成分进行求和，有
$$
\begin{aligned}
&\sum^k_{i=1}
\sum^m_{j=1} 
\frac 
{\alpha_i P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}{\sum^k_{l=1}\alpha_l \cdot P(\pmb x_l|\pmb \mu_i, \pmb \Sigma_l)}
+\sum^k_{i=1}\lambda \alpha_i
=0\\\\
\iff
&
\sum^m_{j=1} 
\sum^k_{i=1}
\frac 
{\alpha_i P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}{\sum^k_{l=1}\alpha_l \cdot P(\pmb x_l|\pmb \mu_i, \pmb \Sigma_l)}
+\sum^k_{i=1}\lambda \alpha_i
=0\\\\
\iff
&
\sum^m_{j=1} 
\sum^k_{i=1}
\gamma_{ji}
+\sum^k_{i=1}\lambda \alpha_i
=0\\\\
\iff
&
\sum^m_{j=1} 
\sum^k_{i=1}
P_M(z_j=i|\pmb x_j) 
+\sum^k_{i=1}\lambda \alpha_i
=0\\\\
\iff
&
\sum^m_{j=1} 
1
+\lambda \sum^k_{i=1} \alpha_i
=0\\\\
\iff
&
m
+\lambda 
=0\\\\
\iff
& \lambda = -m
\end{aligned}
\tag{15}
$$

所以有
$$
\begin{aligned}
&\sum^m_{j=1} 
\frac 
{\alpha_i P(\pmb x_j|\pmb \mu_i, \pmb \Sigma_i)}{\sum^k_{l=1}\alpha_l \cdot P(\pmb x_l|\pmb \mu_i, \pmb \Sigma_l)}
+\lambda \alpha_i
=0\\\\
\iff
&\sum^m_{j=1} 
\gamma_{ji}
-m \alpha_i
=0\\\\
\iff
&
\alpha_i=
\frac 1 m
\sum^m_{j=1} 
\gamma_{ji}
\end{aligned}
\tag{16}
$$

即每个高斯混合成分的混合系数由样本属于该成分的平均后验概率确定。

因为我们无法观测到样本属于哪一个混合成分，因此可以使用EM算法求解，

- E步：先根据当前参数计算每个样本属于每个高斯成分的后验概率 $\gamma_{ji}$ 
- M步：再根据(11),(12),(16)更新模型参数 $\mu,\Sigma,\alpha$

<img src="img/gmm1.png" alt="《机器学习》P210" style="zoom: 67%;" />

聚类代码参见 [source.py](source.py) 。

#### 初始化与计算稳定性

初始化方式上，笔者将设定混合系数的初始值相等，方差为样本方差，均值通过随机抽取的方产生。在计算过程中，笔者发现，如果样本中某两个特征i与j之间线性相关程度很高，会导致计算协方差矩阵逆矩阵的过程中产生奇异矩阵。为了提升计算稳定性，笔者在求取后验概率时，对于奇异矩阵使用伪逆作为替代，并且使用了平滑手段，代码如下：

```python
def compute_posterior_prob(weight, mu, cov_mat, x):
    """
    计算每个样本的后验概率
    :param weight: 混合系数
    :param mu: 均值
    :param cov_mat: 协方差矩阵
    :param x: 样本数据
    :return: 后验概率
    """
    d = mu.shape[0]
    # 用来盛放协方差矩阵的逆矩阵
    inv_cov_mat = np.zeros_like(cov_mat)
    for i in range(d):
        # 如果遇到奇异矩阵则使用伪逆替代
        try:
            inv_cov_mat[i] = np.linalg.inv(cov_mat[i])
        except np.linalg.LinAlgError:
            print("遭遇奇异矩阵，使用伪逆替代")
            inv_cov_mat[i] = np.linalg.pinv(cov_mat[i])
    # 下面开始计算正态分布概率密度
    ## 指数部分，使用了增加维度的方式提升运算速度
    exponential = - np.diagonal(
        np.matmul(np.matmul(x - mu[:, np.newaxis, :], inv_cov_mat),
                  (x - mu[:, np.newaxis, :]).transpose(0, 2, 1)), axis1=1, axis2=2
    ) / 2
    ## 求行列式可能溢出，进行平滑，满足下式：cov_det = sign * np.exp(logdet)，因为协方差矩阵的行列式不为赋值，所以就等于后半部分
    sign, logdet = np.linalg.slogdet(cov_mat)
    ## 计算没有加权的概率密度，np.exp(logdet / 2)是开了根号的行列式
    phi = np.divide(np.exp(exponential), np.power(2 * np.pi, d / 2)
                    * np.exp(logdet / 2).reshape(-1, 1) + epsilon).T
    ## 实际上，计算概率密度的过程中很容易出现上溢或下溢的现象，对其进行平滑，
    ## 分别取最小或最大值作为平滑标准，因为平滑因子在分子分母上斗湖出现，所以不会对结果产生影响
    if np.abs(np.min(phi)) < 1e-10:
        phi += epsilon
        factor = np.power(np.min(phi), -0.5)
        numer = phi * factor * weight.T
    elif np.abs(np.max(phi)) > 1e8:
        factor = np.power(np.max(phi), 0.5)
        numer = phi * factor * weight.T
    else:
        numer = phi * weight.T
    # 计算归一化操作需要的分母
    denom = np.sum(numer, axis=1).reshape(-1, 1)
    # 计算平滑操作后每一行是不是和为1
    res = numer / (denom + epsilon)
    assert np.abs(
        np.sum(
            np.sum(
                res,
                axis=1,
                keepdims=True) -
            np.ones((res.shape[0], 1)))) < 1e-3
    # 返回后验概率和分母，用作后续计算
    return res, denom
```

笔者同时设置了最大迭代次数与极大似然函数值作为度量，当达到最大迭代次数或者极大似然函数值更新幅度较小时，停止迭代。

#### 聚类过程与结果探究

GMM模型能够很好地对初始化不理想的模型进行聚类，对于1.2.3中的组2，即便初始化的均值向量比较集中，GMM可以进行很好地分类。为了让实验具有对照的作用，均值初始化方式选择了无约束的随机初始化。

<center>
  表4 GMM聚类的组2
</center>

<table>
  <tr>
    <td>
      <center><img src="img/init211.png" width="1000px" /></center>
      <center>初始分布</center>
    </td>
    <td>
      <center><img src="img/init212.png" width="1000px" /></center>
      <center>Ietration=1</center>
    </td>
  </tr>
  <tr>
    <td>
      <center><img src="img/init213.png" width="1000px" /></center>
      <center>Ietration=7</center>
    </td>
    <td>
      <center><img src="img/init214.png" width="1000px" /></center>
      <center>Ietration=13</center>
    </td>
  </tr>
    <tr>
    <td>
      <center><img src="img/init215.png" width="1000px" /></center>
      <center>Ietration=21</center>
    </td>
    <td>
      <center><img src="img/init217.png" width="1000px" /></center>
      <center>Ietration=39</center>
    </td>
  </tr>
</table>
不难看出，即使初始向量分布非常集中，经过一定次数的迭代最终也可以得到比较理想的聚类结果，而且另一方面GMM会得到一个数据分布不连续的集合，这对于一些非连续的分布也会有很好的作用。

对于分布不均衡的数据，KMeans会受到非常大的影响，这是因为KMean可以看作高斯混合聚类在混合成份方差相等，且每个样本仅指派给一个混合成分时的特例。GMM由于考虑了不同成分进行混合的比例，并且考虑了各成分的方差，因此在面对不均衡样本时有更强的适应力，GMM在组3上的表现如下：

<center>
  表5 GMM聚类的组3
</center>

<table>
  <tr>
    <td>
      <center><img src="img/init221.png" width="1000px" /></center>
      <center>初始分布</center>
    </td>
    <td>
      <center><img src="img/init222.png" width="1000px" /></center>
      <center>Ietration=1</center>
    </td>
  </tr>
  <tr>
    <td>
      <center><img src="img/init223.png" width="1000px" /></center>
      <center>Ietration=7</center>
    </td>
    <td>
      <center><img src="img/init224.png" width="1000px" /></center>
      <center>Ietration=13</center>
    </td>
  </tr>
    <tr>
    <td>
      <center><img src="img/init225.png" width="1000px" /></center>
      <center>Ietration=21</center>
    </td>
    <td>
      <center><img src="img/init227.png" width="1000px" /></center>
      <center>Ietration=39</center>
    </td>
  </tr>
</table>

从结果上看，GMM对于样本不均衡的模型也有很好的表现。

## 自动选择聚簇数量

这一节实验主要探究使用了Elbow Method配合K-Means 算法进行自动选择聚类簇数的实验，详细代码见`ClusteringAlgorithm`类。算法主要思想是从一个较小的初始值到一个较大的初始值之间选取一些簇数取值c进行聚类，并考察聚类结果在误差平方和SSE上的表现，当k等于样本个数时，SSE等于0。当c小于真实簇数时，c每增大一点都会给SSE带来比较大的降幅，而当k大于真实簇数时，k的增大对SSE带来的影响幅度较小。SSE计算公式如下：
$$
SSE = 
\sum^{|C|} _ {i=1} 
\sum_{p \in C_i}
|p-m_i|^2 
\tag{17}
$$

代码：

```python
def compute_SSE(self):
    """
    计算误差平方和SSE
    :return: 
    """
    SSE = 0
    for c in self.cluster.keys():
        member = self.cluster[c]    # 获取类内成员的下标
        if len(member) == 0:    # 空类则跳过
            continue
        # 累计SSE，考虑到直接相加在样本数量较大的情况下很容易溢出，对其进行缩放，self.const = 1e7
        SSE += np.sum(np.square(self.data[member] - self.mean_vec[c])) / self.const
    return SSE
```

初始值的选定上采用了启发式的方法，c的取值从2开始，当样本总数量大于1000时，c的最大值定位样本数的开方，否则则定为样本数。为了使得采样点更加密集，步长上统一设置为1。得到SSE散点图后，寻找转折点的方法参考了知乎文章（[求曲线转折点坐标——浮生一记](https://zhuanlan.zhihu.com/p/109348995)），做法是分析每个点左右定长窗口内的点的方差，寻找左右方差之比最大的点，笔者选择了不同大小的窗口并进行加权平均，代码见`fit()`方法。笔者使用一组数据作为说明，

<table>
  <tr>
    <td>
      <center><img src="img/Figure_1.png" width="1000px" /></center>
      <center>初始分布</center>
    </td>
    <td>
      <center><img src="img/Figure_2.png" width="1000px" /></center>
      <center>SSE散点图</center>
    </td>
  </tr>
  <tr>
    <td>
      <center><img src="img/Figure_3.png" width="1000px" /></center>
      <center>窗口方差比率</center>
    </td>
    <td>
      <center><img src="img/Figure_4.png" width="1000px" /></center>
      <center>聚类结果</center>
    </td>
</table>

从结果上来看，找到的c值就是SSE曲线的转折点，说明此方法有一定的效果。

当样本数量较多、簇数较多时，此方法效果较好，当样本点数量较少或是簇类较少时，此方法表现较差；当簇比较集中时效果较好，簇比较分散时效果较差。为了简化实验，笔者采用了随机生成协方差矩阵和均值向量的方法来产生数据，并设定每个初始簇的样本数量相同，代码如下：

```python
data_lst = []  
for i in range(15):
	mean = np.random.rand(2) * 100
	cov = generate_positive_cov(2) + np.eye(2) * 10
	data_ = np.random.multivariate_normal(mean, cov, (1000,))
	data_lst.append(data_)
```

下表中记录了一些自动选择簇数量的数据，其中(100/cluster)表示每个簇的样本数量，C1_t表示在C1(100/cluster)的基础上将均值扩大十倍的结果，这一操作能够使得簇间间隔更大。

<center>
  表6 自动聚类结果
</center>

| num of clusters | C1(100/cluster) | C1_t | C2(1000/cluster) | C2_t |
| --------------- | --------------- | ---- | ---------------- | ---- |
| 5               | 4               | 3    | 4                | 3    |
| 15              | 4               | 12   | 15               | 12   |
| 30              | 26              | 28   | 15               | 27   |

实际上，当簇内元素比较离散时，不同的簇很容易重叠到一起，这将会消弭不同簇之间的差别，从而影响自动选择出来的簇数量。

六、代码使用方法

```python
python source.py --auto_c	# Elbow Method配合K-Means
python source.py --kmeans	# KMeans展示
python source.py --gaussian	# GMM展示
```



