import numpy as np
import matplotlib.pyplot as plt


epsilon = 1e-6

plt.style.use('seaborn')

def normalization(X):
    """
    对数据进行标准化处理，使其均值为0，方差为1
    :param X: 输入数据
    :return: 标准化后的数据
    """
    mean = np.mean(X, axis=0)
    std = np.std(X, axis=0)
    X = (X - mean) / std
    return X


def compute_dis(center_vec, target_vec):
    """
    计算向量与类中心的距离
    :param center_vec: 类中心，tensor，k * d
    :param target_vec: 待计算向量，n * d
    :return: 距离，k * n，第i行第j列位置的元素表示第j个向量到第i类中心的距离
    """
    # 这里的计算方法是将center_vec扩充为n * 1 * d维 可以利用广播机制直接计算
    # target_vec到所有类中心的距离，并利用norm方法计算距离，可以充分利用numpy
    # 的并行计算优势
    return np.linalg.norm(center_vec[:, np.newaxis, :] - target_vec, axis=-1)


def scatter_show(model, iteration=None):
    """
    绘图函数
    :param iteration: 迭代次数
    :param model: 使用的模型
    :return: None
    """
    # 获取数据与簇情况
    data = model.data
    label = model.cluster
    mean_vec = model.mean_vec
    # 为每个簇绘图
    color = ['#FFB6C1', '#00BFFF', '#DA70D6']
    for c in label:
        idx = label[c]
        # plt.scatter(data[idx, 0], data[idx, 1], c=color[c], s=20)
        plt.scatter(data[idx, 0], data[idx, 1], s=20)
    # 将簇中心画出
    plt.scatter(mean_vec[:, 0], mean_vec[:, 1], marker='+', c='#000000', s=1000)

    if iteration is not None:
        plt.title(f"n clusters = {model.n_clusters}\n"
                  f"iteration = {iteration}")
    else:
        plt.title(f"n clusters = {model.n_clusters}")

    plt.show()


class KMeans:
    def __init__(self, n_clusters):
        """
        初始化
        :param n_clusters: 待聚类的簇数
        """
        self.n_clusters = n_clusters
        # 簇
        self.cluster = {
            c: None for c in range(n_clusters)
        }
        # 当簇中心变动距离的均值小于阈值时，停止迭代
        self.epsilon = 1e-1
        # 用来缓存训练数据与簇中心
        self.data = None
        self.mean_vec = None

    def reset_dict(self):
        """
        重置簇
        :return: None
        """
        self.cluster = {
            c: None for c in range(self.n_clusters)
        }
        self.mean_vec = None

    def initialize(self):
        """
        初始化均值向量，KMeans++方法
        :return: 初始化后的均值向量
        """
        # n, d, k分别是样本数量，维度以及簇数
        n = self.data.shape[0]
        d = self.data.shape[1]
        k = self.n_clusters
        # 随机选取第一个均值向量
        idx = np.random.randint(0, n)
        # 初始化均值向量存储矩阵
        mean_vec = np.zeros((k, d))
        mean_vec[0] = self.data[idx].reshape(1, -1)
        # 初始化每个样本到均值向量的距离存储矩阵
        dist = np.zeros((n, k))
        # 将每个样本到第一个向量的距离填入矩阵中
        dist[:, 0] =  np.linalg.norm(self.data - mean_vec[0].reshape(1, -1), axis=1)
        # 迭代选取剩余的向量
        for i in range(1, k):
            # 保留每个样本到距离其最近的类中心的距离，这些距离保存在dist矩阵的前i列
            dist_min = np.min(dist[:, :i], axis=1)
            # 在保留的距离中选取距离最大的一个作为新的均值向量
            new_vec_idx = np.argmax(dist_min)
            # 将此步产生的向量并入
            mean_vec[i] = self.data[new_vec_idx].reshape(1, -1)
            # 计算每个样本到新向量的距离，并存入距离矩阵
            dist[:, i] = np.linalg.norm(self.data - mean_vec[i], axis=1)
        return mean_vec


    def fit(self, train_data, is_plot=False):
        """
        聚类过程
        :param train_data: 训练数据
        :return: None
        """
        # 缓存
        print("Fit Begin")
        num = len(train_data)
        train_data = normalization(train_data)
        self.data = train_data
        # 从D中随机选择n_clusters个样本作为初始均值向量
        # idx = np.arange(num)
        # mean_vec_idx = np.random.choice(idx, self.n_clusters, replace=False)
        # mean_vec = train_data[mean_vec_idx]
        mean_vec = self.initialize()

        # 最大迭代次数
        max_iterations = 50
        # 缓存
        self.mean_vec = mean_vec
        # 迭代开始
        for i in range(max_iterations):
            print(f"Iteration: {i + 1}")
            # 保存上一步簇中心结果，用来计算更新步幅
            last_mean_vec = mean_vec.copy()
            # 计算距离
            dis = compute_dis(mean_vec, train_data)
            # 获取标签：样本x以距离其最近的簇中心的标签为自身的标签
            label = np.argmin(dis, axis=0)

            # 绘图
            if i % 2 == 0 and is_plot:
                # 计算每个簇内的样本数量
                for j in range(self.n_clusters):
                    self.cluster[j] = np.where(label == j)[0].tolist()
                print(self.mean_vec)
                scatter_show(self, i + 1)

            # 计算新的簇中心
            for j in range(self.n_clusters):
                mean_vec[j] = np.mean(train_data[np.where(label == j)], axis=0)

            # 计算更新幅度：对应簇中心变化距离的均值，小于阈值停止更新，结束聚类过程
            difference = mean_vec - last_mean_vec
            if np.mean(np.linalg.norm(difference, axis=1)) < self.epsilon:
                break
            # 缓存新的簇中心
            self.mean_vec = mean_vec.copy()

        print("Fit End")

    def predict(self, test_data):
        """
        预测测试数据的标签
        :param test_data: 测试数据，n * d
        :return: 每个样本的类别，(n,)
        """
        # 计算每个样本到每个类中心的距离
        dis = compute_dis(self.mean_vec, test_data)
        # 预测样本属于哪个簇
        return np.argmin(dis, axis=0)


def compute_posterior_prob(weight, mu, cov_mat, x):
    """
    计算每个样本的后验概率
    :param weight: 混合系数
    :param mu: 均值
    :param cov_mat: 协方差矩阵
    :param x: 样本数据
    :return: 后验概率
    """
    c = mu.shape[0]
    d = mu.shape[1]
    # 用来盛放协方差矩阵的逆矩阵
    inv_cov_mat = np.zeros_like(cov_mat)
    cov_mat += epsilon * 1e5
    for i in range(c):
        # 如果遇到奇异矩阵则使用伪逆替代
        try:
            # cov_mat_m = np.eye(cov_mat[i].shape[0]) * epsilon + cov_mat[i]
            if np.abs(np.linalg.det(cov_mat[i])) < 1e-4:
                raise np.linalg.LinAlgError
            else:
                inv_cov_mat[i] = np.linalg.inv(cov_mat[i])
        except np.linalg.LinAlgError:
            print("遭遇奇异矩阵，使用伪逆替代")
            inv_cov_mat[i] = np.linalg.pinv(cov_mat[i])
    # 下面开始计算正态分布概率密度
    ## 指数部分，使用了增加维度的方式提升运算速度
    exponential = - np.diagonal(
        np.matmul(np.matmul(x - mu[:, np.newaxis, :], inv_cov_mat),
                  (x - mu[:, np.newaxis, :]).transpose(0, 2, 1)), axis1=1, axis2=2
    ) / 2
    ## 求行列式可能溢出，进行平滑，满足下式：cov_det = sign * np.exp(logdet)
    sign, logdet = np.linalg.slogdet(cov_mat)
    ## 计算没有加权的概率密度
    phi = np.divide(np.exp(exponential) + epsilon, np.power(2 * np.pi, d / 2)
                    * np.exp(logdet / 2).reshape(-1, 1) + epsilon).T
    ## 实际上，计算概率密度的过程中很容易出现上溢或下溢的现象，对其进行平滑，
    ## 分别取最小或最大值作为平滑标准，因为平滑因子在分子分母上斗湖出现，所以不会对结果产生影响
    if np.abs(np.min(phi)) < 1e-10:
        phi += epsilon
        factor = np.power(np.min(phi), -0.5)
        numer = phi * factor * weight.T
    elif np.abs(np.max(phi)) > 1e8:
        factor = np.power(np.max(phi), 0.5)
        numer = phi * factor * weight.T
    else:
        numer = phi * weight.T
    # 计算归一化操作需要的分母
    denom = np.sum(numer, axis=1).reshape(-1, 1)
    # 计算平滑操作后每一行是不是和为1
    res = numer / (denom + epsilon)
    # assert np.abs(
    #     np.sum(
    #         np.sum(
    #             res,
    #             axis=1,
    #             keepdims=True) -
    #         np.ones((res.shape[0], 1)))) < 1e-1
    # 返回后验概率和分母，用作后续计算
    return res, denom


def compute_log_likelihood(gamma_denom):
    return np.sum(np.log(gamma_denom), axis=0)


class GaussianMixture:

    def __init__(self, n_clusters):
        """
        初始化
        :param n_clusters: 待聚类的簇数，也就是高斯混合成分的个数
        """
        self.n_clusters = n_clusters
        # 簇，value元组：(下标集合，混合权重，均值向量，协方差矩阵)
        self.cluster = {
            c: None for c in range(n_clusters)
        }
        self.data = None
        self.mean_vec = None
        self.cov_mat = None
        self.weight = None

    def reset_params(self):
        """
        重置每个簇对应的参数
        :return: None
        """
        self.cluster = {
            c: (None, None, None, None) for c in range(self.n_clusters)
        }
    def initialize(self):
        """
        初始化均值向量，KMeans++方法
        :return: 初始化后的均值向量
        """
        # n, d, k分别是样本数量，维度以及簇数
        n = self.data.shape[0]
        d = self.data.shape[1]
        k = self.n_clusters
        # 随机选取第一个均值向量
        idx = np.random.randint(0, n)
        # 初始化均值向量存储矩阵
        mean_vec = np.zeros((k, d))
        mean_vec[0] = self.data[idx].reshape(1, -1)
        # 初始化每个样本到均值向量的距离存储矩阵
        dist = np.zeros((n, k))
        # 将每个样本到第一个向量的距离填入矩阵中
        dist[:, 0] =  np.linalg.norm(self.data - mean_vec[0].reshape(1, -1), axis=1)
        # 迭代选取剩余的向量
        for i in range(1, k):
            # 保留每个样本到距离其最近的类中心的距离，这些距离保存在dist矩阵的前i列
            dist_min = np.min(dist[:, :i], axis=1)
            # 在保留的距离中选取距离最大的一个作为新的均值向量
            new_vec_idx = np.argmax(dist_min)
            # 将此步产生的向量并入
            mean_vec[i] = self.data[new_vec_idx].reshape(1, -1)
            # 计算每个样本到新向量的距离，并存入距离矩阵
            dist[:, i] = np.linalg.norm(self.data - mean_vec[i], axis=1)
        return mean_vec

    def fit(self, train_data, is_plot=False):
        """
        聚类
        :param train_data: 输入样本
        :param is_plot: 是否绘图
        :return: None
        """
        # 预处理
        train_data = normalization(train_data)
        if len(train_data) < 10:
            aug_data = train_data.copy()
            for i in range(4):
                temp = train_data + np.random.randn(*train_data.shape) * 5e-2
                aug_data = np.concatenate([aug_data, temp], axis=0)
            train_data = aug_data

        self.data = train_data

        # 获取样本的一些特征
        num = len(train_data)
        n = train_data.shape[0]
        k = self.n_clusters
        d = train_data.shape[1]

        # 下面开始初始化
        ## 初始化混合成分被选择的概率
        weight = (np.zeros(k).reshape(-1, 1) + 1) / k
        ## 初始化每个混合成分的均值向量，这里采用随机选取的方法
        # idx = np.arange(num)
        # mu_idx = np.random.choice(idx, k, replace=False)
        # mu = train_data[mu_idx]
        mu = self.initialize()
        ## 初始化协方差矩阵，假定属性间是相互独立的
        cov_mat = np.tile(
            np.diag(np.var(train_data, axis=0))[np.newaxis, :, :],
            (k, 1, 1)
        )
        # 用来存储上一步的性能度量数值
        metric_cache = 1
        # 最大迭代次数
        max_iterations = 100
        print("Fit Begin")
        for iteration in range(max_iterations):
            # 迭代开始，这里使用了增加维度的方法，能够充分利用numpy的并行计算优势，一次性计算出所有混合成分的参数
            print(f"iteration: {iteration}")
            # 计算样本xj属于每个混合成分的后验概率
            posterior_prob, gamma_denom = compute_posterior_prob(
                weight, mu, cov_mat, train_data)
            # 对后验概率求和作为归一化分母
            denom = np.sum(posterior_prob, axis=0).reshape(-1, 1)
            # assert posterior_prob.shape[0] == n, posterior_prob.shape[1] == k
            # 计算当前参数下的极大似然估计数值
            metric = compute_log_likelihood(gamma_denom)
            # 计算新均值向量，依据算法中的公式，参见README
            mu_ = np.matmul(posterior_prob.T, train_data) / denom
            # assert mu_.shape[0] == k, mu_.shape[1] == d
            # 计算新协方差矩阵
            temp = train_data[:, np.newaxis, :] - mu_
            temp = np.matmul(temp[:, :, :, np.newaxis],
                             temp[:, :, np.newaxis, :])
            # assert temp.shape == (n, k, d, d)
            temp = posterior_prob[:, :, np.newaxis, np.newaxis] * temp
            # assert temp.shape == (n, k, d, d)
            conv_mat_ = np.sum(temp, axis=0) / denom[:, :, np.newaxis]
            # assert conv_mat_.shape == (k, d, d)
            # 计算新混合系数
            weight_ = denom / n
            # assert weight_.shape == (k, 1)

            # 是否满足停止条件
            update_step = metric - metric_cache
            if iteration % 2 == 0 and is_plot:
                label = np.argmax(posterior_prob, axis=1)
                self.weight = weight
                self.mean_vec = mu
                self.cov_mat = cov_mat
                for j in range(self.n_clusters):
                    self.cluster[j] = np.where(label == j)[0].tolist()
                scatter_show(self, iteration + 1)
            # 更新步幅为负或小于阈值时停止迭代
            if iteration > 0 and (
                    update_step /
                    np.abs(metric_cache) <= 1e-3 or iteration == max_iterations -
                    1):
                label = np.argmax(posterior_prob, axis=1)
                self.weight = weight
                self.mean_vec = mu
                self.cov_mat = cov_mat
                for j in range(self.n_clusters):
                    self.cluster[j] = np.where(label == j)[0].tolist()
                if is_plot:
                    scatter_show(self)
                break
            # 更新似然函数值
            metric_cache = metric
            # 更新模型参数
            mu = mu_
            cov_mat = conv_mat_
            weight = weight_

    def predict(self, test_data):
        """
        预测测试数据的标签
        :param test_data: 测试数据，n * d
        :return: 每个样本的类别，(n,)
        """
        # 计算每个样本到每个类中心的距离
        test_data = normalization(test_data)
        posterior_prob, _ = compute_posterior_prob(
            self.weight, self.mean_vec, self.cov_mat, test_data)
        # 预测样本属于哪个簇
        return np.argmax(posterior_prob, axis=1)


class ClusteringAlgorithm:

    def __init__(self):
        """
        初始化
        """
        self.n_clusters = None
        # 簇
        self.cluster = {}
        # 当簇中心变动距离的均值小于阈值时，停止迭代
        self.epsilon = 1e-1
        # 用来缓存训练数据与簇中心
        self.data = None
        self.mean_vec = None


    def reset_dict(self):
        """
        重置簇
        :return: None
        """
        self.cluster = {
            c: None for c in range(self.n_clusters)
        }
        self.mean_vec = None

    def compute_SSE(self):
        """
        计算误差平方和SSE
        :return:
        """
        SSE = 0
        for c in self.cluster.keys():
            member = self.cluster[c]    # 获取类内成员的下标
            if len(member) == 0:    # 空类则跳过
                continue
            # 累计SSE
            SSE += np.sum(np.square(self.data[member] - self.mean_vec[c]))
        return SSE

    def initialize(self):
        """
        初始化均值向量，KMeans++方法
        :return: 初始化后的均值向量
        """
        # n, d, k分别是样本数量，维度以及簇数
        n = self.data.shape[0]
        d = self.data.shape[1]
        k = self.n_clusters
        # 随机选取第一个均值向量
        idx = np.random.randint(0, n)
        # 初始化均值向量存储矩阵
        mean_vec = np.zeros((k, d))
        mean_vec[0] = self.data[idx].reshape(1, -1)
        # 初始化每个样本到均值向量的距离存储矩阵
        dist = np.zeros((n, k))
        # 将每个样本到第一个向量的距离填入矩阵中
        dist[:, 0] =  np.linalg.norm(self.data - mean_vec[0].reshape(1, -1), axis=1)
        # 迭代选取剩余的向量
        for i in range(1, k):
            # 保留每个样本到距离其最近的类中心的距离，这些距离保存在dist矩阵的前i列
            dist_min = np.min(dist[:, :i], axis=1)
            # 在保留的距离中选取距离最大的一个作为新的均值向量
            new_vec_idx = np.argmax(dist_min)
            # 将此步产生的向量并入
            mean_vec[i] = self.data[new_vec_idx].reshape(1, -1)
            # 计算每个样本到新向量的距离，并存入距离矩阵
            dist[:, i] = np.linalg.norm(self.data - mean_vec[i], axis=1)
        return mean_vec

    def fit_step(self):
        """
        单步聚类过程
        :return: None
        """
        # 获取训练数据
        self.reset_dict()
        train_data = self.data
        # 从D中随机选择n_clusters个样本作为初始均值向量
        ## 随机初始化
        # idx = np.arange(num)
        # mean_vec_idx = np.random.choice(idx, self.n_clusters, replace=False)
        # mean_vec = train_data[mean_vec_idx]
        ## 带约束
        mean_vec = self.initialize()
        # 最大迭代次数
        max_iterations = 50
        # 缓存
        self.mean_vec = mean_vec
        # 迭代开始
        for i in range(max_iterations):
            # 保存上一步簇中心结果，用来计算更新步幅
            last_mean_vec = mean_vec.copy()
            # 计算距离
            dis = compute_dis(mean_vec, train_data)
            # 获取标签：样本x以距离其最近的簇中心的标签为自身的标签
            label = np.argmin(dis, axis=0)

            # 绘图
            if i % 2 == 0:
                # 计算每个簇内的样本数量
                for j in range(self.n_clusters):
                    self.cluster[j] = np.where(label == j)[0].tolist()
                # print(self.mean_vec)
                # scatter_show(self)

            # 计算新的簇中心
            for j in range(self.n_clusters):
                mean_vec[j] = np.nanmean(train_data[np.where(label == j)], axis=0)

            # 计算更新幅度：对应簇中心变化距离的均值，小于阈值停止更新，结束聚类过程
            difference = mean_vec - last_mean_vec
            if np.mean(np.linalg.norm(difference, axis=1)) < self.epsilon:
                break
            # 缓存新的簇中心
            self.mean_vec = mean_vec.copy()

    def fit(self, train_data):
        """
        自动挑选簇数并进行聚类
        :param train_data: 训练数据
        :return:
        """
        print("Fit Begin")
        num = len(train_data)
        # 预处理
        train_data = normalization(train_data)
        # 缓存
        self.data = train_data
        # 确定c的选取范围与迭代步长
        c_min = 1
        if num > 100:
            c_max = int(np.sqrt(num))
        else:
            c_max = num
        c_step = 1
        print(c_max)
        # 记录SSE数值
        c_SSE_pairs = []
        for c in range(c_min, c_max + 1, c_step):
            self.n_clusters = c
            self.fit_step()
            s = self.compute_SSE()
            c_SSE_pairs.append([c, s])
        pair_num = len(c_SSE_pairs)
        c_SSE_pairs = np.array(c_SSE_pairs)
        plt.scatter(c_SSE_pairs[:,0], c_SSE_pairs[:,1])
        plt.show()
        # 滑动窗口计算左右方差比
        delta_s_1 = []
        # 窗口大小的范围
        n_min = 2
        n_max = 4
        for j in range(n_max, pair_num - 1 - n_max):
            b_l = 0
            b_r = 0
            denom = 0
            for n in range(n_min, n_max + 1):
                # 计算左右断电
                left1 = j - n
                right1 = j + n
                # 计算方差
                b_l_n = np.var(c_SSE_pairs[left1: j, 1])
                b_r_n = np.var(c_SSE_pairs[j + 1: right1 + 1, 1])
                # 计算权重因子
                factor = n ** 2
                b_l += factor * b_l_n
                b_r += factor * b_r_n
                denom += factor
            # 加权平均
            b_l_bar = b_l / denom
            b_r_bar = b_r / denom
            # 计算比值
            delta_s_1.append(b_l_bar / b_r_bar)
        delta_s_1 = np.array(delta_s_1)
        plt.scatter(range(n_min + 1, len(delta_s_1) + n_min + 1), delta_s_1)
        plt.show()
        # 找到比值最大的点
        pos = np.argmax(delta_s_1)
        # 确定对应的簇数
        self.n_clusters = pos + n_min + 1
        self.fit_step()
        scatter_show(self)

        print("Fit End")

    def predict(self, test_data):
        """
        预测测试数据的标签
        :param test_data: 测试数据，n * d
        :return: 每个样本的类别，(n,)
        """
        # 计算每个样本到每个类中心的距离
        dis = compute_dis(self.mean_vec, test_data)
        # 预测样本属于哪个簇
        return np.argmin(dis, axis=0)


def generate_positive_cov(dimension):
    """
    # 用于生成随机的数据集
    :param dimension: 协方差矩阵的维度
    :return:
    """
    A = np.abs(np.random.rand(dimension, dimension))
    B = np.dot(A, A.T)
    return B


def select_c():
    np.random.seed(1)
    model = ClusteringAlgorithm()


    data_lst = []

    for i in range(30):
        mean = np.random.rand(2) * 1000
        cov = generate_positive_cov(2) + np.eye(2) * 10
        data_ = np.random.multivariate_normal(mean, cov, (100,))
        data_lst.append(data_)

    data = np.concatenate(data_lst, axis=0)
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    plt.scatter(data[:, 0], data[:, 1])
    plt.title('data generation')
    plt.show()
    model.fit(data)
    # scatter_show(model)

def show(mode):
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data = np.concatenate([x, y, z], axis=0)
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]

    plt.scatter(data[:, 0], data[:, 1])
    plt.title('data generation')
    plt.show()

    if mode == 'KMeans':
        model = KMeans(3)
        model.fit(data, True)
    elif mode == 'Gaussian':
        model = GaussianMixture(3)
        model.fit(data, True)


if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1 and sys.argv[1] == "--auto_c":
        select_c()
    elif len(sys.argv) > 1 and sys.argv[1] == "--kmeans":
        show('KMeans')
    elif len(sys.argv) > 1 and sys.argv[1] == "--gaussian":
        show('Gaussian')
