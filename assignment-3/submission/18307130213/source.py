import numpy as np
import matplotlib.pyplot as plt
import random
import sys
import math

class KMeans:

    def __init__(self, n_clusters):
        self.k = n_clusters
        self.kernels = None
        self.norms = None
    
    def fit(self, train_data):
        def normlization(data): # 对训练数据进行标准化并保留标准化参数以标准化测试数据
            minabs = np.abs(data.min(axis=0))
            maxabs = np.abs(data.max(axis=0))
            myabs = np.where(minabs > maxabs, minabs, maxabs)
            self.norm = myabs
            mydata = data/myabs
            # assert(mydata.min() >= -1.0)
            # assert(mydata.max() <= 1.0)
            return mydata
        
        def trainOnce(data):
            def genNewKernels(data, kernels): # 进行一轮迭代，生成新的聚类核
                clusters = [[] for _ in range(self.k)]
                n = data.shape[0]
                for i in range(n):
                    distances = np.sum((data[i] - kernels)**2, axis=1)
                    clusters[np.argmin(distances)].append(i)

                newKernels = []
                for i in range(self.k):
                    kern = np.average(data[clusters[i]],axis=0)
                    newKernels.append(kern)
                return np.array(newKernels)

            np.random.shuffle(data)
            kernels = data[0:self.k]
            for i in range(100000):
                newKernels = genNewKernels(data, kernels)
                if (kernels == newKernels).all():
                    break
                kernels = newKernels
                return kernels
            else:
                print('Too many iterations.')
                exit(0)

        def calcScore(data, kernels): # 计算欧氏距离来衡量模型好坏
            score = 0.0
            n = data.shape[0]
            for i in range(n):
                distances = np.sum((data[i] - kernels) ** 2,axis=1)**0.5
                score += distances.min()
            return score

        data = train_data.copy()
        data = normlization(data)
        if data.shape[0] <= self.k:
            self.kernels = data
            return
        
        bestKernels = trainOnce(data)
        bestScore = calcScore(data, bestKernels)
        for i in range(10):
            newKernels = trainOnce(data)
            newScore = calcScore(data, newKernels)
            if newScore < bestScore:
                bestKernel = newKernels
                bestScore = newScore
        
        self.kernels = bestKernels
    
    def predict(self, test_data):
        def normlization(data): # 根据训练数据的标准化参数对测试数据进行标准化处理
            mydata = data/self.norm
            return mydata

        data = normlization(test_data)
        ans = []
        for i in range(data.shape[0]):
            distances = np.sum((self.kernels - data[i]) ** 2,axis=1)
            ans.append(np.argmin(distances))
        
        return np.array(ans)

class GaussianMixture:

    def __init__(self, n_clusters):
        self.k = n_clusters
        self.mius = None
        self.sigs = None
        self.ps = None
        self.norm = None
    
    def fit(self, train_data):
        def normlization(data): # 对训练数据进行标准化并保留标准化参数以标准化测试数据
            minabs = np.abs(data.min(axis=0))
            maxabs = np.abs(data.max(axis=0))
            myabs = np.where(minabs > maxabs, minabs, maxabs)
            self.norm = myabs
            mydata = data/myabs
            # assert(mydata.min() >= -1.0)
            # assert(mydata.max() <= 1.0)
            return mydata

        def calcGaussP(x, miu, sig): # 计算高维高斯分布的概率
            dim = x.shape[0]
            sigma = np.matrix(sig)
            numerator = np.exp(-0.5 * np.matmul(np.matmul(x - miu, sigma.I), (x - miu).T))
            denominator = ((2.0*np.pi)**dim * np.linalg.det(sigma))**0.5
            return numerator / (denominator + np.exp(-213))

        def calcEachGauss(xs, mius, sigs, ps): # 计算GMM对应的概率
            n = xs.shape[0]
            res = np.zeros((n, self.k))
            for i in range(n):
                sum = 0.0
                for k in range(self.k):
                    res[i][k] = ps[k] * calcGaussP(xs[i], mius[k], sigs[k])
                    sum += res[i][k]
                for k in range(self.k):
                    res[i][k] /= sum
            return res
        
        def iteration(xs):
            mius = self.mius
            sigs = self.sigs
            ps = self.ps
            n = xs.shape[0]
            dim = xs.shape[1]
            gama = calcEachGauss(xs, mius, sigs, ps)
            nks = np.sum(gama, axis=0)
            nps = nks / n
            nmius = np.matmul(gama.T, xs) / nks[:,np.newaxis]

            nsigma = np.zeros((self.k,dim,dim))
            for k in range(self.k):
                tmp = np.zeros((dim,dim))
                for i in range(n):
                    t = np.matrix(xs[i] - mius[k])
                    tmp += nks[k]*np.matmul(t.T,t)
                nsigma[k] = tmp/nks[k]

        data = train_data.copy()
        data = normlization(data)
        n = data.shape[0]
        dim = data.shape[1]
        indexs = np.arange(n)
        np.random.shuffle(indexs)
        self.mius = data[indexs[:self.k]]
        self.sigs = np.full((self.k, dim, dim), fill_value=np.diag(np.full(dim, 0.1)))
        self.ps = np.ones(self.k) / self.k
        for iter in range(64):
            iteration(data)
    
    def predict(self, test_data):
        def normlization(data): # 根据训练数据的标准化参数对测试数据进行标准化处理
            mydata = data/self.norm
            return mydata

        def calcGaussP(x, miu, sig): # 计算高维高斯分布的概率
            dim = x.shape[0]
            sigma = np.matrix(sig)
            numerator = np.exp(-0.5 * np.matmul(np.matmul(x - miu, sigma.I), (x - miu).T))
            denominator = ((2.0*np.pi)**dim * np.linalg.det(sigma))**0.5
            return numerator / (denominator + np.exp(-213))

        def calcEachGauss(xs, mius, sigs, ps): # 计算GMM对应的概率
            n = xs.shape[0]
            res = np.zeros((n, self.k))
            for i in range(n):
                sum = 0.0
                for k in range(self.k):
                    res[i][k] = ps[k] * calcGaussP(xs[i], mius[k], sigs[k])
                    sum += res[i][k]
                for k in range(self.k):
                    res[i][k] /= sum
            return res

        data = test_data.copy()
        data = normlization(data)
        gama = calcEachGauss(data, self.mius, self.sigs, self.ps)
        pred = np.argmax(gama, axis=1)
        return pred

class ClusteringAlgorithm:

    def __init__(self):
        self.bestK = None
        self.bestModel = None
    
    def fit(self, train_data):
        def prepDist(data): # 准备距离矩阵
            n = data.shape[0]
            dist = np.zeros((n,n))
            for i in range(n):
                dist[i] = (np.sum((data[i] - data)**2, axis=1))**0.5
            return dist
        
        def calcDist(i, idx, dist): # 计算点簇距离
            if idx[0].shape[0] <=1 :
                return 0
            avg = np.sum(dist[i, idx]) / (idx[0].shape[0] - 1)
            return avg

        def calcSilhouetteCoefficient(n, labels, k, dist): #计算轮廓系数
            idxs = []
            for t in range(k):
                idxs.append(np.where(labels==t))

            S = np.zeros((n, ))
            for i in range(n):
                ai = calcDist(i, idxs[labels[i]], dist)
                bi = np.inf
                for t in range(k):
                    if t != labels[i]:
                        bi = min(bi, calcDist(i, idxs[t], dist))
                S[i] = (bi-ai)/max(bi, ai)
            return np.mean(S)

        scores = []
        
        data = train_data.copy()
        n = data.shape[0]
        dist = prepDist(data)
        bestK = 2
        model = KMeans(2)
        model.fit(data)
        labels = model.predict(data)
        bestModel = model
        bestScore = calcSilhouetteCoefficient(n, labels, 2, dist)
        scores.append(bestScore)
        cnt = 5
        maxk = int((0.5*n)**0.5)
        for i in range(3, maxk):
            model = KMeans(i)
            model.fit(data)
            labels = model.predict(data)
            score = calcSilhouetteCoefficient(n, labels, i, dist)
            scores.append(score)
            if score > bestScore:    
                bestK = i
                bestScore = score
                cnt = 5
            else:
                cnt -= 1
                if cnt == 0:
                    break

        kmeans = KMeans(bestK)
        kmeans.fit(data)
        kmeansLabels = kmeans.predict(data)
        kmeansScore = calcSilhouetteCoefficient(n, kmeansLabels, bestK, dist)

        gmm = GaussianMixture(bestK)
        gmm.fit(data)
        gmmLabels = gmm.predict(data)
        gmmScore = calcSilhouetteCoefficient(n, kmeansLabels, bestK, dist)
        if kmeansScore > gmmScore:
            self.bestK = bestK
            self.bestModel = kmeans
        else:
            self.bestK = bestK
            self.bestModel = gmm
        
        return scores

    def predict(self, test_data):
        return self.bestK, self.bestModel.predict(test_data)

if __name__ == '__main__':
    def generate(n):
        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
        if n <= 0:
            print('error: n <= 0')
            return
        r = n/max(1, math.log(n, 2))
        sizs = []
        xs = []
        for i in range(n):
            theta = i*(2*math.pi/n)
            mean = (r*math.cos(theta) , r*math.sin(theta))
            rand_mat = np.random.rand(2, 2)
            cov = rand_mat.transpose()*rand_mat
            siz = random.randint(100, 200)
            sizs.append(siz)
            x = np.random.multivariate_normal(mean, cov, (siz, ))
            xs.append(x)
        siz = sum(sizs)
        idx = np.arange(siz)
        np.random.shuffle(idx)
        data = np.concatenate(xs)
        label = np.concatenate([np.ones((sizs[j], ), dtype=int)*j for j in range(n)])
        data = data[idx]
        label = label[idx]

        t = 3
        train_data, test_data = data[:(siz//t)*(t-1),], data[(siz//t)*(t-1):,]
        train_label, test_label = label[:(siz//t)*(t-1),], label[(siz//t)*(t-1):,]

        np.save("data.npy",(
            (train_data, train_label), (test_data, test_label)
        ))

    def read():
        (train_data, train_label), (test_data, test_label) = np.load("data.npy", allow_pickle=True)
        return (train_data, train_label), (test_data, test_label)

    def genimg(n, data, label, name):
        datas =[[] for i in range(n)]
        for i in range(len(data)):
            datas[label[i]].append(data[i])
    
        for each in datas:
            each = np.array(each)
            plt.scatter(each[:, 0], each[:, 1])
        plt.savefig(f'img/{name}')
        plt.close()

    if len(sys.argv) > 1 and sys.argv[1] == 'g':
        try:
            n = int(sys.argv[2])
            generate(n)
        except:
            print('error: wrong n')
    elif len(sys.argv) > 1 and sys.argv[1] == 'd':      
        (train_data, train_label), (test_data, test_label) = read()
        try:
            n = int(sys.argv[2])
            genimg(n, train_data, train_label, 'train')
            genimg(n, test_data, test_label, 'test')
        except:
            print('somthing goes wrong!')
    elif len(sys.argv) > 2 and sys.argv[1] == 'p' and sys.argv[2] == 'KMeans':
        (train_data, train_label), (test_data, test_label) = read()
        try:
            n = int(sys.argv[3])
            model = KMeans(n)
            model.fit(train_data)
            res = model.predict(test_data)
            genimg(n, test_data, res, 'KMeans')
        except:
            print('somthing goes wrong!')
    elif len(sys.argv) > 2 and sys.argv[1] == 'p' and sys.argv[2] == 'GaussianMixture':
        (train_data, train_label), (test_data, test_label) = read()
        try:
            n = int(sys.argv[3])
            model = GaussianMixture(n)
            model.fit(train_data)
            res = model.predict(test_data)
            genimg(n, test_data, res, 'GaussianMixture')
        except:
            print('somthing goes wrong!')
    elif len(sys.argv) > 0 and sys.argv[1] == 'p':
        (train_data, train_label), (test_data, test_label) = read()
        try:
            model = ClusteringAlgorithm()
            scores = model.fit(train_data)

            nk = [x+2 for x in range(len(scores))]
            l,=plt.plot(nk, scores, label='score')
            plt.xlabel('n_clusters')
            plt.ylabel('Silhouette Coefficient by KMeans')
            plt.title('k-Silhouette Coefficient graph')
            plt.legend()
            plt.savefig('img/k-Silhouette Coefficient graph')
            plt.close()
                
            n, res = model.predict(test_data)
            genimg(n, test_data, res, 'ClusteringAlgorithm')
        except:
            print('somthing goes wrong!')
    else:
        print('Test OK')