import numpy as np
import sys
from source import GaussianMixture, KMeans, ClusteringAlgorithm
from ondGMM import GMM
from matplotlib import pyplot


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int) * i
        for (i, d) in enumerate(datas)])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_1():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z)
    return (data, data), 3


def data_test(input=3):
    mean = (1, 2)
    cov = np.array([[3, 0], [0, 2]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (26, 28)
    cov = np.array([[2, 0], [0, 2]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (-25, -23)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    mean = (11, 13)
    cov = np.array([[4, 3], [3, 4]])
    q = np.random.multivariate_normal(mean, cov, (1000,))
    if input == 3:
        data, label = shuffle(x, y, z)
    elif input == 4:
        data, label = shuffle(x, y, z, q)
    else:
        print("only 3 and 4 is accepted")
    num = len(data)
    train_data, test_data = data[:int(0.8 * num)], data[int(0.8 * num):num]
    train_label, test_label = label[:int(0.8 * num)], label[int(0.8 * num):num]
    return train_data, test_data, train_label, test_label


def data_2():
    train_data = np.array([
        [23, 12, 173, 2134],
        [99, -12, -126, -31],
        [55, -145, -123, -342],
    ])
    return (train_data, train_data), 2


def data_3():
    train_data = np.array([
        [23],
        [-2999],
        [-2955],
    ])
    
    return (train_data, train_data), 2


def test_with_n_clusters(data_fuction, algorithm_class):
    (train_data, test_data), n_clusters = data_fuction()[0], data_fuction()[1]
    model = algorithm_class(n_clusters)
    model.fit(train_data)
    res = model.predict(test_data)
    assert len(
        res.shape) == 1 and res.shape[0] == test_data.shape[0], "shape of result is wrong"
    return res


def testcase_1_1():
    test_with_n_clusters(data_1, KMeans)
    return True


def testcase_1_2():
    res = test_with_n_clusters(data_2, KMeans)
    return res[0] != res[1] and res[1] == res[2]


def testcase_2_1():
    test_with_n_clusters(data_1, GaussianMixture)
    return True


def testcase_2_2():
    res = test_with_n_clusters(data_3, GMM)
    return res[0] != res[1] and res[1] == res[2]


def test_all(err_report=False):
    testcases = [
        ["KMeans-1", testcase_1_1, 4],
        ["KMeans-2", testcase_1_2, 4],
        # ["KMeans-3", testcase_1_3, 4],
        # ["KMeans-4", testcase_1_4, 4],
        # ["KMeans-5", testcase_1_5, 4],
        ["GMM-1", testcase_2_1, 4],
        ["GMM-2", testcase_2_2, 4],
        # ["GMM-3", testcase_2_3, 4],
        # ["GMM-4", testcase_2_4, 4],
        # ["GMM-5", testcase_2_5, 4],
    ]
    sum_score = sum([case[2] for case in testcases])
    score = 0
    for case in testcases:
        try:
            res = case[2] if case[1]() else 0
        except Exception as e:
            if err_report:
                print("Error [{}] occurs in {}".format(str(e), case[0]))
            res = 0
        score += res
        print("+ {:14} {}/{}".format(case[0], res, case[2]))
    print("{:16} {}/{}".format("FINAL SCORE", score, sum_score))


def pltshow(data, res, n_clu):
    mark = [[np.random.randint(0, 256) for i in range(3)] for j in range(n_clu)]
    marklist = ["#{:02x}{:02x}{:02x}".format(mark[i][0], mark[i][1], mark[i][2]) for i in range(n_clu)]
    colorlist = [marklist[i] for i in res]
    pyplot.scatter(data[:, 0], data[:, 1], c=colorlist)
    pyplot.show()


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "--report":
        test_all(True)
    else:
        test_all()

    final = data_test(3)#input = 3 or 4
    train, test, train_label, test_label = final[0], final[1], final[2], final[3]
    cluster_num = 3
    km = KMeans(cluster_num)
    km.fit(train)
    kmeans_res = km.predict(test)
    gmm = GaussianMixture(cluster_num)
    gmm.fit(train)
    gmm_res = gmm.predict(test)
    # pltshow(train, train_label, cluster_num)
    # pltshow(test, test_label, cluster_num)
    # pltshow(test, kmeans_res, cluster_num)
    # pltshow(test, gmm_res, cluster_num)
