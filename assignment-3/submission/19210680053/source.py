import numpy as np
from scipy.stats import multivariate_normal


class KMeans(object):
    def __init__(self, n_clusters):
        self.cluNum = n_clusters
        self.distance = 0

    def fit(self, train_data):
        # first set the initial centers by random selection
        self.train_data = train_data
        numSamples, dim = self.train_data.shape
        # step1:  init the center
        self.centroids = np.zeros((self.cluNum, dim))
        for i in range(self.cluNum):
            index = int(np.random.uniform(0, numSamples))
            self.centroids[i, :] = self.train_data[index, :]
        clusterAssment = np.mat(np.zeros((numSamples, 2)))
        clusterChanged = True

        while clusterChanged:
            clusterChanged = False
            ## for each sample
            for i in range(numSamples):
                minDist = 100000.0
                minIndex = 0
                ## step 2: find the centroid who is closestfor each centroid
                for j in range(self.cluNum):
                    distance = self.euclDistance(self.centroids[j, :], self.train_data[i, :])
                    if distance < minDist:
                        minDist = distance
                        minIndex = j

                ## step 3: update its cluster
                if clusterAssment[i, 0] != minIndex:
                    clusterChanged = True
                    clusterAssment[i, :] = minIndex, minDist ** 2
            ## step 4: update centroids
            for j in range(self.cluNum):
                pointsInCluster = np.array(self.train_data[np.nonzero(clusterAssment[:, 0].A == j)[0]])
                if len(pointsInCluster) != 0:
                    self.centroids[j, :] = np.mean(pointsInCluster, axis=0)

        for i in range(len(clusterAssment)):
            self.distance += clusterAssment[i, 1]
        return self.centroids, clusterAssment, self.distance

    def predict(self, test_data):
        self.test_data = test_data
        numSamples, dim = self.test_data.shape
        m_res = []
        for i in range(numSamples):
            distance = [self.euclDistance(self.test_data[i, :], self.centroids[j, :]) for j in range(self.cluNum)]
            clus_type = distance.index(min(distance))
            m_res.append(clus_type)
        return np.array(m_res)


    def euclDistance(self, vector1, vector2):
        return np.sqrt(sum(np.power(vector2 - vector1, 2)))



class GaussianMixture:
    # covariance: ndarray
    def __init__(self, n_clusters, reg_cov: float = 1e-06):
        self.cluNum = n_clusters
        self.max_iter = 1200
        self.reg_cov = reg_cov
        self.alpha = (np.ones(n_clusters) / n_clusters)
        self.sigma2 = (np.ones(n_clusters))

    def fit(self, train_data):
        sampleN, sample_fea = train_data.shape
        '''
        gamma.shape(N, K)
        mu.shape(1, K)
        sigma2.shape(1,K)
        alpha.shape(1, K)
        '''
        if sample_fea == 1:
            train_data = self.normalize(train_data)
            print(train_data)
            self.gamma = np.ones((train_data.shape[0], self.cluNum)) / self.cluNum
            self.mu = (np.arange(self.cluNum) - self.cluNum // 2) * (train_data.max() - train_data.min()) / self.cluNum
            sigma2_ = self.sigma2
            mu_ = self.mu
            while True:
                
                self.gamma = (0.1 * self.gamma + 0.9 * self.phi(train_data, self.mu).T * self.alpha / (
                        self.phi(train_data, self.mu).T * self.alpha).sum(
                    axis=1).reshape(train_data.shape[0], 1))
               
                self.mu = (0.1 * self.mu + 0.9 * np.matmul(train_data, self.gamma) / self.gamma.sum(axis=0))
                
                self.sigma2 = (0.1 * self.sigma2 + 0.9 * (
                        self.gamma * (train_data.reshape(train_data.shape[0], 1) - self.mu) ** 2).sum(
                    axis=0) / self.gamma.sum(axis=0))
                
                self.alpha = (0.1 * self.alpha + 0.9 * self.gamma.sum(axis=0) / train_data.shape[0])
                if (np.sum((self.mu - mu_) ** 2) + np.abs(self.sigma2 - sigma2_).sum()) < 1e-3:
                    break
                mu_ = self.mu
                sigma2_ = self.sigma2
        elif sample_fea == 2:
            self.reg_cov = self.reg_cov * np.identity(sample_fea)
            self.means = np.random.randint(train_data.min() / 2, train_data.max() / 2, size=(self.cluNum, sample_fea))
            self.covariance = np.zeros((self.cluNum, sample_fea, sample_fea))
            for k in range(self.cluNum):
                np.fill_diagonal(self.covariance[k], 1)
            self.weight = np.ones(self.cluNum) / self.cluNum

            p_mat = np.zeros((sampleN, self.cluNum))
            for i in range(self.max_iter):
                for j in range(self.cluNum):
                    self.covariance += self.reg_cov
                    g = multivariate_normal(mean=self.means[j], cov=self.covariance[j])
                    p_mat[:, j] = self.weight[j] * g.pdf(train_data)
                total_n = p_mat.sum(axis=1)
                total_n[total_n == 0] = self.cluNum
                p_mat /= total_n.reshape(-1, 1)
                if sample_fea != 1:
                    for j in range(self.cluNum):
                        N_k = np.sum(p_mat[:, j], axis=0)

                        self.means[j] = (1 / N_k) * np.sum(train_data * p_mat[:, j].reshape(-1, 1), axis=0)

                        self.covariance[j] = (1 / N_k) * np.dot(
                            (p_mat[:, j].reshape(-1, 1) * (train_data - self.means[j])).T,
                            (train_data - self.means[j])) + self.reg_cov
                        self.weight[j] = N_k / sampleN
        else:
            print("sorry, the model is only designed for 1-D and 2-D data")

    def predict(self, test_data):
        sampleN, sample_fea = test_data.shape
        if sample_fea == 1:
            test_data = self.normalize(test_data)
            gamma = (self.phi(test_data, self.mu).T * self.alpha / (self.phi(test_data, self.mu).T * self.alpha).sum(
                axis=1).reshape(test_data.shape[0], 1))
            return gamma.argmax(axis=1)
        elif sample_fea == 2:
            p_mat = np.zeros((test_data.shape[0], self.cluNum))
            for j in range(self.cluNum):
                self.covariance += self.reg_cov
                g = multivariate_normal(mean=self.means[j], cov=self.covariance[j])
                p_mat[:, j] = self.weight[j] * g.pdf(test_data)

            total_n = p_mat.sum(axis=1)
            total_n[total_n == 0] = self.cluNum
            p_mat /= total_n.reshape(-1, 1)
            return np.argmax(p_mat, axis=1)
        else:
            print("one and two dimension data is accepted")

    def normalize(self, data):
        flat_data = data.flatten()
        nor_data = (flat_data - flat_data.mean()) / np.sqrt(flat_data.var())
        return nor_data

    def phi(self, data, mu):
        '''phi.shape(K, N)'''
        mu = (np.arange(self.cluNum) - self.cluNum // 2) * (data.max() - data.min()) / self.K
        phi = (1 / np.sqrt(2 * np.pi * self.sigma2.reshape(self.cluNum, 1)) * np.exp(
            - (data - mu.reshape(self.cluNum, 1)) ** 2 / (2 * self.sigma2.reshape(self.cluNum, 1))))
        return phi


class ClusteringAlgorithm:
    def __init__(self):
        self.elbow_dict = {}
        self.elbow_chg = []
        self.elbow_chg_next = []
        self.bestCluNum = 0

    def fit(self, train_data):
        for i in range(2, 6):
            mini_loss = 1e8
            '''To guarantee robustness'''
            for j in range(50):
                self.km = KMeans(i)
                if self.km.fit(train_data)[2] < mini_loss:
                    mini_loss = self.km.fit(train_data)[2]
            self.elbow_dict[i] = self.km.fit(train_data)[2]
        elbow_dist = list(self.elbow_dict.values())
        self.elbow_chg = [(elbow_dist[i] / elbow_dist[i - 1]) - 1 for i in range(1, len(elbow_dist))]
        self.elbow_chg_next = [(self.elbow_chg[i] - self.elbow_chg[i - 1]) for i in range(1, len(self.elbow_chg))]
        self.bestCluNum = self.elbow_chg.index(max(self.elbow_chg)) + 3
        self.km = KMeans(self.bestCluNum)
        self.km.fit(train_data)
        return elbow_dist, self.bestCluNum

    def predict(self, test_data):
        res = self.km.predict(test_data)
        return res

