import numpy as np
from source import KMeans, ClusteringAlgorithm
from matplotlib import pyplot


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int) * i
        for (i, d) in enumerate(datas)])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_test(input=3):
    mean = (1, 2)
    cov = np.array([[3, 0], [0, 2]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (26, 28)
    cov = np.array([[2, 0], [0, 2]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (-25, -23)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    mean = (11, 13)
    cov = np.array([[4, 3], [3, 4]])
    q = np.random.multivariate_normal(mean, cov, (1000,))
    if input == 3:
        data, label = shuffle(x, y, z)
    elif input == 4:
        data, label = shuffle(x, y, z, q)
    else:
        print("only 3 and 4 is accepted")
    num = len(data)
    train_data, test_data = data[:int(0.8 * num)], data[int(0.8 * num):num]
    train_label, test_label = label[:int(0.8 * num)], label[int(0.8 * num):num]
    return train_data, test_data, train_label, test_label


def pltshow(data, res, n_clu):
    mark = [[np.random.randint(0, 256) for i in range(3)] for j in range(n_clu)]
    marklist = ["#{:02x}{:02x}{:02x}".format(mark[i][0], mark[i][1], mark[i][2]) for i in range(n_clu)]
    # marklist = ['c', 'b', 'g', 'r', 'm', 'y', 'k', 'w']
    colorlist = [marklist[i] for i in res]
    pyplot.scatter(data[:, 0], data[:, 1], c=colorlist)
    pyplot.show()


if __name__ == "__main__":
    input_ = 4
    final = data_test(input_)
    train, test, train_label, test_label = final[0], final[1], final[2], final[3]
    # cluster_num = 3
    clu_test = ClusteringAlgorithm()
    elbow_dict, best_num = clu_test.fit(train)
    clu_res = clu_test.predict(test)

    km = KMeans(input_)
    km.fit(train)
    res = km.predict(test)

    pltshow(test, res, input_)
    pltshow(test, clu_res, best_num)
