# Assignment 3

## 1.KMeans 模型结构
本次实验首先对KMeans模型进行实现
### 类中心点的初始化
在给定类簇数目（cluNum)以及数据维度(N*dim)之后，首先随机从数据集中选取cluNum行数据，作为若干个类的中心点

得到如下初始化数据中心
$$\begin{Bmatrix}
{{{C_1,C_2,...,C_{cluNum}}}}\end{Bmatrix}
$$
### 数据到中心的距离以及分类
接着计算训练集中每个对象到各个中心的欧式距离，
$$
euclDistance_{i,j}={\sqrt {\sum_{t=1}^{dim} (X_{it}-C_{jt})^2}}
$$
对于每个对象，分别保留其相距 __最近中心点__ 下标以及对应距离平方，存储在clusterAssment中（shape: N*2)的第一列与第二列
### 中心点更新
根据clusterAssment的第一列数据，分别得到距离每个$C_k$,$1\leq k\leq cluNum$最近的一系列点坐标,因此对于k类数据点求取均值，得到其作为新的$C_k$
### 循环停止条件
设置clusterchanged作为循环标志，即对于所有点的clusterAssment中距离最近中心点$C_k$的下标k不再发生改变，则循环结束
```python
 if clusterAssment[i, 0] != minIndex:
                    clusterChanged = True
                    clusterAssment[i, :] = minIndex, minDist ** 2
```
此时输出cluNum个中心点作为模型训练结果
$$\begin{Bmatrix}
C_1,C_2,C_3,...C_{cluNum}\end{Bmatrix}
$$
### 模型预测
```python
    def predict(self, test_data):
        self.test_data = test_data
        numSamples, dim = self.test_data.shape
        m_res = []
        for i in range(numSamples):
            distance = [self.euclDistance(self.test_data[i, :], self.centroids[j, :]) for j in range(self.cluNum)]
            clus_type = distance.index(min(distance))
            m_res.append(clus_type)
        return np.array(m_res)
```
通过计算测试集每个对象距离所有中心点的欧式距离，每个点会被归类到距其最近的中心点所属类中。

## 2.GaussianMixture 模型结构
本次实验要求对1-D 与2-D 数据进行GaussianMixture的实现,所以分别对两种类型数据进行实现
### 2维数据
#### 进行每个聚类的均值、协方差矩阵以及对应权重初始化
由于初值选择会影响分类效果，因此均值采用介于训练集数据$[(Minimum \quad value)/2,(Maximum \quad value)/2]$之间生成随机数
```python
self.means = np.random.randint(train_data.min() / 2, train_data.max() / 2, size=(self.cluNum, sample_fea))

```
每个协方差矩阵令其矩阵对角线取值为1，非对角线取值为0
每个聚类具有等权重
```python
 self.weight = np.ones(self.cluNum) / self.cluNum
```
#### E step

$$\gamma_{k}^{(i)} = \frac{\pi_{k}N(x^{(i)}|\mu_{k},\Sigma_{k})}{\sum_{k=1}^{K}N(x^{(i)}|\mu_{k},\Sigma_{k})}$$
$N(x^{(i)}|\mu_{k},\Sigma_{k})$采用二维高斯分布进行拟合，再乘以$\pi_{k}$每个cluster对应weight,通过上式计算得到第i个数据样本点落在第k个聚类中的概率
#### M step
通过计算cluster k 中的样本数来进行权重更新$\pi_{k}$
$$N_{k}=\sum_{i=1}^{n}\gamma_{k}^{(i)}$$
$$\pi_{k}=\frac{N_{k}}{N}$$
并对每个cluster的均值以及协方差matrix进行更新
$$\mu_{k}=\frac{\sum_{i=1}^{cluNum}\gamma_{k}^{(i)}x^{(i)}}{N_{k}}$$
$$\Sigma_{k}=\frac{\sum_{i=1}^{cluNum}\gamma_{k}^{(i)}(x^{(i)}-\mu_{k})(x^{(i)}-\mu_{k})^{T}}{N_{k}}$$

```python
N_k = np.sum(p_mat[:, j], axis=0)
self.weight[j] = N_k / sampleN
self.means[j] = (1 / N_k) * np.sum(train_data * p_mat[:, j].reshape(-1, 1), axis=0)

self.covariance[j] = (1 / N_k) * np.dot(
    (p_mat[:, j].reshape(-1, 1) * (train_data - self.means[j])).T,
    (train_data - self.means[j])) + self.reg_cov
```
#### 迭代条件
模型可通过使得$\begin{Bmatrix}\Sigma_{k},\pi_{k},\mu_{k}\end{Bmatrix}$均收敛来作为停止条件，此处进行最大训练次数来作为模型训练的终止条件。
#### 模型预测
由train_data得到每个聚类的均值、协方差矩阵以及对应权重
$prob_{k,i}=\pi_{k}N(x^{(i)}|\mu_{k},\Sigma_{k})$
对于训练集中数据点i,寻求在所有中心点中令上式最大化的中心点k，数据会被归至该类。
### 1维数据
#### 数据正则化
对于一维数据，首先进行数据正则化处理,以及形式上转化为一维数据
```python
    def normalize(self, data):
        flat_data = data.flatten()
        nor_data = (flat_data - flat_data.mean()) / np.sqrt(flat_data.var())
        return nor_data
```
#### 进行数据的每个聚类均值、方差（而非协方差矩阵）以及对应权重初始化
对于一维数据均值初始化，采用方式是：生成关于0对称array * 训练集中均值
```python
self.mu = (np.arange(self.cluNum) - self.cluNum // 2) * (train_data.max() - train_data.min()) / self.cluNum
```
对于方差初始化，cluNum个聚类全部取值为1；
对于权重，赋予每个聚类等权重
#### E step and M step
E step与M step同2D数据思想相同
但在M step中进行每个cluster均值与方差更新时，会赋予旧均值、旧方差0.1的权重；新均值方差0.9的权重，进行迭代更新
#### 迭代停止条件
模型通过设置阈值，使得$\begin{Bmatrix}\Sigma_{k},\pi_{k},\mu_{k}\end{Bmatrix}$均收敛来作为停止条件
```python
                if (np.sum((self.mu - mu_) ** 2) + np.abs(self.sigma2 - sigma2_).sum()) < 1e-3:
                    break
```
#### 模型预测

由train_data得到每个聚类的均值、方差以及对应权重，同样进行
$argmax\frac{N(X_{k}|\mu_{k},\Sigma_{k})Weight_{k}}
{\sum_{k=1}^{cluNum}N(X_{k}|\mu_{k},\Sigma_{k})Weight_{k}}$
的计算，此时数据点i会被归至第k类
## 3.基础实验
### 3.1数据可视化
在test_demo.py中进行数据可视化实现
```python
def pltshow(test_data, res, n_clu):
    mark = [[np.random.randint(0, 256) for i in range(3)] for j in range(n_clu)]
    marklist = ["#{:02x}{:02x}{:02x}".format(mark[i][0], mark[i][1], mark[i][2]) for i in range(n_clu)]
    colorlist = [marklist[i] for i in res]
    pyplot.scatter(test[:, 0], test[:, 1], c=colorlist)
    pyplot.show()
```
### 3.2随机二维数据集生成
通过生成不同均值、方差（协方差矩阵）、以及不同数目高斯分布作为训练集与数据集的二维数据对模型效果进行检验
当数据分散时，分类效果较准确
+ 第一类数据，数量为800个，标签为0：

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
3 & 0 \\\\
0 & 2
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
1 & 2
\end{array}\right]
\end{array}
$$

+ 第二类数据，数量为200个，标签为1：

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
2 & 0 \\\\
0 & 2
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
26 & 28
\end{array}\right]
\end{array}
$$

+ 第三类数据，数量为1000个，标签为2：

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 5 \\\\
5 & 10
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
-25 & -23
\end{array}\right]
\end{array}
$$

随机打乱数据集后，前80%作为训练集，后20%作为测试集
生成训练集如下所示:

<img src="img/Train1.png" alt="Training dataset with 3 Gaussian Distribution" width="570"/>

测试集如下所示:

<img src="img/Test1.png" alt="Testing dataset with 3 Gaussian Distribution" width="570"/>

Kmeans分类结果如下所示：

<img src="img/KMeans1.png" alt="Kmeans Classification with 3 Gaussian Distribution" width="570"/>

GaussianMixture分类结果如下所示：

<img src="img/GMM1.png" alt="Kmeans Classification with 3 Gaussian Distribution" width="570"/>

此时，加大分类难度，再增加一类与已有较为接近数据集：

第四类数据集，数量为1000，标签为3：
$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
4 & 3 \\\\
3 & 4
\end{array}\right] \\\\
\mu=\left[\begin{array}{ll}
-20 & -21
\end{array}\right]
\end{array}
$$

随机打乱新数据集后，前80%作为训练集，后20%作为测试集

生成训练集如下所示：

<img src="img/Train2.png" alt="Kmeans Classification with 4 Gaussian Distribution" width="570"/>

测试集如下所示：

<img src="img/Test2.png" alt="Kmeans Classification with 4 Gaussian Distribution" width="570"/>

Kmeans分类结果如下所示：

<img src="img/KMeans2.png" alt="Kmeans Classification with 3 Gaussian Distribution" width="570"/>

GMM分类结果如下所示：

<img src="img/GMM2.png" alt="Kmeans Classification with 3 Gaussian Distribution" width="570"/>

此时，GMM分类结果优于KMeans，原因是给出数据本身服从二维高斯分布。

KMeans的迭代停止条件是计算欧氏距离，直至每一cluster中心点不再发生变化，当存在两类数据中心点较为接近时，即便迭代停止，也无法对数据点进行有效区分；

而在给定数据服从二维高斯分布，符合GMM也是假定数据服从高斯分布假设，此时分类效果更好。

## 4.类簇选择

### Elbow Method 进行优化

算法原理是在K 值相对较小的情况下，当选择的k值小于真正的n_clusters时，k每增加1，代价值就会大幅的减小；当选择的k值大于真正的n_cluster时， k每增加1，cost值的变化就不会那么明显。此时，正确的k值就会在这个转折点，类似肘部的地方。

对于代价值定义：
$$
cost = \sum_{k=1}^{(n_cluster)}\sum_{i=1}^{n}(X_{i,k}-C_{k})^2
$$
即对于训练集中每个点，计算它到所属类别中心的欧式距离平方进行加总，作为总代价值

## 最优类簇数目选择

首先进行总代价变化率：
$$
cost chg_{k} = cost_{k} / cost_{k-1}-1
$$
接着计算变化率的变动情况(即凸性）
$$
cost convex \quad chg_{k} = cost chg_{k} - cost chg_{k-1}
$$
选取$cost convex \quad chg_{k}$最大的k值作为最优分类下的类簇数目进行拟合，即分类数目为$k-1$到k下降陡峭，分类数目为k到$k+1下降平缓。

分别使用test_demo_elbow中由三个高斯分布和四个高斯分布组合的数据集进行求解：


#### 三个高斯分布

训练集的实际分布情况
<img src="img/elbowtest1.png" alt="Elbow Kmeans Classification with 3 Gaussian Distribution" width="560"/>

模型预测

<img src="img/elbowres1.png" alt="Elbow Kmeans Classification with 3 Gaussian Distribution" width="570"/>

#### 四个高斯分布

训练集的实际分布情况

<img src="img/elbowtest2.png" alt="Kmeans Classification with 3 Gaussian Distribution" width="570"/>

模型预测

<img src="img/elbowres2.png" alt="Kmeans Classification with 3 Gaussian Distribution" width="570"/>

Elbow test的局限性在于其一般适用于k值较小的情况，因此生成二维高斯数据集数目较少。
