import numpy as np

class KMeans:

    def __init__(self, n_clusters):
        self.k = n_clusters  # 聚成的类数
        self.centers = None  # 类中心（均值）
    
    def fit(self, train_data):
        train_data = train_data.copy()
        if train_data.shape[0] <= self.k:
            self.centers = train_data
            return

        def cluster(train_data, cens):            
            '''
            一轮迭代
            parameters:
                cens: 上一轮的类中心
            return：
                新的类中心
            '''  
            clusters = [[] for _ in range(self.k)]
            for i in range(train_data.shape[0]):   # 对每个点，选择最近的类中心作为这个点的类
                data = train_data[i]
                distances = np.sum((data - cens) ** 2,axis=1)
                minind = np.argmin(distances)
                clusters[minind].append(i)
            
            ncens = []
            for i in range(self.k):   # 计算每个类中点的平均值作为新的类中心
                data = train_data[clusters[i]]
                #删除异常值
                if data.shape[0] > 50:
                    distances = (np.sum((data - cens[i])** 2,axis=1)) ** 0.5
                    avgdis = np.mean(distances)
                    ind = np.where(distances < avgdis * 20)
                    data = data[ind]
                cen = np.average(data,axis=0)
                ncens.append(cen)
            return np.array(ncens)
        
        def eval(train_data):
            np.random.shuffle(train_data)
            cens = train_data[0:self.k]   # 随机选择k个数据作为初始的类中心
            iter = 0
            while 1:
                iter += 1
                ncens = cluster(train_data,cens)
                if (ncens == cens).all():   # 迭代终止条件：前后两轮类中心不变，此后簇聚结果也不再会随迭代改变
                    # print("迭代次数"+str(iter))
                    break
                cens = ncens
            self.centers = cens
            kind = self.predict(train_data)
            evaluator = ClusteringAlgorithm()
            distancs = evaluator._cal_distance(train_data)
            score = evaluator.cal_Silhouette_Coefficient(train_data,kind,self.k,distancs)
            # print(score)
            recore_cens = cens
            return recore_cens,score

        #为了克服初始类中心选取对簇聚结果的影响，重复若干次实验，选择轮廓系数最大的
        maxscore = -2
        for i in range(3):
            cens,score = eval(train_data)
            if score > maxscore:
                self.centers = cens
        
    #对测试集聚类，如果draw=true， 将绘制聚类结果图（二维可用）
    def predict(self, test_data,draw=False):  
        ans = []
        for i in range(test_data.shape[0]):
            data = test_data[i]
            # print(data)
            distances = np.sum((self.centers - data) ** 2,axis=1)
            cenid = np.argmin(distances)
            # print(cenid)
            ans.append(cenid)
        if draw:
            if test_data.shape[1] == 2:
                self._drawing_dim2(np.array(ans),test_data)
            else: 
                self._drawing_dim3(np.array(ans),test_data)
        return np.array(ans)
    
    def _drawing_dim2(self,kind,X):
        colors = ['red','green','blue','yellow']
        import matplotlib.pyplot as plt
        for i in range(self.k):
            ind = np.where(kind==i)
            plt.scatter(X[ind][:,0], X[ind][:,1],  c=colors[i%(len(colors))], alpha=0.4)
        plt.show()
    
    def _drawing_dim3(self,kind,X):
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = Axes3D(fig)
        colors = ['red','green','blue','yellow']
        for i in range(self.k):
            ind = np.where(kind==i)
            ax.scatter(X[ind][:,0], X[ind][:,1],X[ind][:,2],  c=colors[i%(len(colors))], alpha=0.4)
        
        ax.legend(loc='best')
        plt.show()
            
class GaussianMixture:

    def __init__(self, n_clusters,iteration=50):
        # pass
        self.K = n_clusters
        self.miu = self.sigma = self.P = None   # GMM中各个高斯分布的sigma和miu，以及隐变量P
        '''
        K为隐变量维度，即聚成的类数; D为维度
        miu： K * D  为均值
        sigma: K * D * D  为协方差矩阵
        P： 1 * K    
        '''
        self.itertion = iteration   # 迭代次数，默认50轮
    
    def _N(self,x, miu, sigma):
        # 计算多维高斯分布的联合概率密度函数
        '''
        x,miu,sigma是np.array  1*D
        要转化为列正态向量 np.matrix
        '''
        x = (np.matrix(x)).T
        miu = (np.matrix(miu)).T
        sigma = np.matrix(sigma)
        D = x.shape[0]
        t1 = 1/((2 * np.pi) ** (D/2) * (np.linalg.det(sigma))**0.5)
        t2 = -0.5 * np.matmul(np.matmul((x - miu).T,sigma.I),(x - miu))
        return t1 * np.exp(t2) + np.exp(-200)
    
    def _cal_r(self,X,miu,sigma,Pt):
        # 计算迭代时的中间变量 R(n * K)
        # 其中 Rik 表示第 i 个数据属于第 k 个高斯分布的概率 
        n = X.shape[0]
        ret = np.zeros((n,self.K))
        for i in range(n):
            x = X[i]
            sum = 0
            for k in range(self.K):
                t = Pt[k] * self._N(x,miu[k],sigma[k])
                sum += t
                ret[i][k] = t
            for k in range(self.K):
                ret[i][k] /= sum
        return ret

    def _iterate(self,X):
        '''
        一轮迭代
        需要更新的参数有：
            设K为隐变量维度，即聚成的类数; D为维度
            miu： K * D  为均值
            sigma: K * D * D  为协方差矩阵
            P： 1 * K    
        '''
        miu = self.miu; sigma = self.sigma; Pt = self.P
        n = X.shape[0]
        D = X.shape[1]
        R = self._cal_r(X,miu,sigma,Pt)
        m = np.sum(R,axis=0)
        nP = m / n   #新的P
        temp = np.matmul(R.T , X)
        nmiu = temp / m[:,np.newaxis] #新的miu
        
        nsigma = np.zeros((self.K,D,D))
        for k in range(self.K):
            temp = np.zeros((D,D))
            for i in range(n):
                t = X[i] - miu[k] 
                t = np.matrix(t)
                temp += R[i][k] * np.matmul(t.T,t)
            temp /= m[k]
            nsigma[k] = temp

        self.P = nP
        self.sigma = nsigma
        self.miu = nmiu

    def fit(self, train_data):
        # 对训练集进行聚类，将进行 self.itertion 次迭代
        n = train_data.shape[0]
        D = train_data.shape[1]
        train_data = train_data.copy()
        ind = np.arange(n)
        np.random.shuffle(ind)
        self.miu = train_data[ind[:self.K]]
        self.sigma = np.full((self.K,D,D), fill_value=np.diag(np.full(D,0.2)))
        self.P = np.ones(self.K) / self.K
        for iter in range(self.itertion):
            self._iterate(train_data)
        # kind = self.predict(train_data)
        # self._drawing_dim2(kind,train_data)


    def _drawing_dim2(self,kind,X):
        colors = ['red','green','blue','yellow']
        import matplotlib.pyplot as plt
        for i in range(self.K):
            ind = np.where(kind==i)
            plt.scatter(X[ind][:,0], X[ind][:,1],  c=colors[i%(len(colors))], alpha=0.4)
        plt.show()

    def _drawing_dim3(self,kind,X):
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = Axes3D(fig)
        colors = ['red','green','blue','yellow']
        for i in range(self.K):
            ind = np.where(kind==i)
            ax.scatter(X[ind][:,0], X[ind][:,1],X[ind][:,2],  c=colors[i%(len(colors))], alpha=0.4)
        
        ax.legend(loc='best')
        plt.show()

    def predict(self, test_data, draw=False):
        #预测，设置draw=true 将绘制聚类结果
        R = self._cal_r(test_data,self.miu,self.sigma,self.P)
        kind = np.argmax(R,axis=1)
        if draw:
            if test_data.shape[1] == 2:
                self._drawing_dim2(kind,test_data)
            else: 
                self._drawing_dim3(kind,test_data)
        return kind

class ClusteringAlgorithm:

    def __init__(self,type=1):
        if type==0:
            self.ModelClass = GaussianMixture
        elif type==1:
            self.ModelClass = KMeans

    def _cal_distance(self,X):
        # 计算所有点两两之间的距离，返回距离矩阵
        n = X.shape[0]
        distance = np.zeros((n,n))
        for i in range(n):
            distance[i] = (np.sum((X[i] - X)**2,axis=1) )**0.5
        return distance

    def _cal_avgdistance(self,indk,ind,distance):
        # 计算ind点到indk中所有点的平均距离
        if indk[0].shape[0] < 2:
            return 0
        indk = np.delete(indk,np.where(indk==ind))
        avg = np.mean(distance[ind,indk])
        return avg

    def cal_Silhouette_Coefficient(self,X,kind,K,distance):
        # 计算整个簇聚结果的轮廓系数
        n = X.shape[0]
        inds = []
        for k in range(K):
            inds.append(np.where(kind==k))
        S = []
        for i in range(n):  # 对每个点求 S（i）
            ai = self._cal_avgdistance(inds[kind[i]],i,distance)
            bi = np.inf
            for k in range(K):
                if k == kind[i]:
                    continue
                bi = min(bi,self._cal_avgdistance(inds[k],i,distance))
            S.append((bi-ai)/max(bi,ai))
        return np.mean(np.array(S))

    
    def fit(self, train_data):
        distance = self._cal_distance(train_data)
        print("计算距离结束")
        fl = 0
        maxscore = -10
        # 通过循环，依次尝试2至最大10000范围内的所有聚簇数量，寻找轮廓系数最高的聚簇数量。考虑效率因素，
        # 我设置这样的循环结束标准：如果连续5次尝试得到的轮廓系数比当前最大的轮廓系数小，则终止循环。
        for n_clusters in range(2,10000):
            print("K=" + str(n_clusters))
            model = self.ModelClass(n_clusters)
            model.fit(train_data)
            kind = model.predict(train_data)
            score = self.cal_Silhouette_Coefficient(train_data,kind,n_clusters,distance)
            if score <= maxscore:
                fl += 1
            else:
                self.model = model
                maxscore = score  # 选择最佳的聚类结果（轮廓系数最高）
                fl = 0
            if fl >= 5:
                break
            print("轮廓系数:" + str(score))
    
    def predict(self, test_data, draw=False):
        kind = self.model.predict(test_data,draw)
        return kind

def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label

def data_1():
    sigma = 40
    cold = 1.5
    mean = (-10,-10)
    cov = np.array([[sigma, sigma/cold], [sigma/cold, sigma]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (-10, 10)
    cov = np.array([[sigma,sigma/cold], [sigma/cold, sigma]])
    y = np.random.multivariate_normal(mean, cov, (800,))

    mean = (10, 0)
    cov = np.array([[sigma, sigma/cold], [sigma/cold, sigma]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    mean = (10, -20)
    cov = np.array([[sigma, sigma/cold], [sigma/cold, sigma]])
    w = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z,w)
    return (data, data), 4

def data_2():
    sigma = 14
    mean = (10,0,0)
    cov = np.array([[sigma, 0,0], [0, sigma,0],[0,0,sigma]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (0,10,0)
    cov = np.array([[sigma, 0,0], [0, sigma,0],[0,0,sigma]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (0,0,10)
    cov = np.array([[sigma, 0,0], [0, sigma,0],[0,0,sigma]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z)
    return (data, data), 3

if __name__=="__main__":
    # (data,_),K = data_2()
    # # print(data.shape)

    # model = KMeans(K)
    # model.fit(data)
    # model.predict(data,draw=True)

    (data,_),K = data_2()
    # print(data.shape)

    model = ClusteringAlgorithm(type=1)
    model.fit(data)
    model.predict(data,draw=True)
