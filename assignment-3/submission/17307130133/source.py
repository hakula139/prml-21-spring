import math
import sys
import numpy as np
import matplotlib.pyplot as plt


def ecul_distance(x, y):
    """
    :param x: vector
    :param y: vector
    :return: eculidean distance between x and y
    """
    return np.sqrt(np.nansum((x - y) ** 2))


def draw(n_clusters, label, points):
    """
    visualize the clustering result of data
    :param n_clusters: number of clusters
    :param label: label of data
    :param points: data
    :return: None
    """
    for i in range(n_clusters):
        cluster_i = np.where(label == i)
        plt.scatter(points[cluster_i][:, 0], points[cluster_i][:, 1])
    plt.show()


class KMeans:

    def __init__(self, n_clusters, max_iterations=100):
        self.k = n_clusters
        self.cluster_centers = []
        self.max_iterations = max_iterations

    def get_nearest_center(self, x):
        """
        get nearest center index to x
        :param x: point index in train_data
        :return: center index
        """
        center = -1
        min_dis = 1e9
        for i in range(len(self.cluster_centers)):
            dis = ecul_distance(self.cluster_centers[i], x)
            if dis < min_dis:
                min_dis = dis
                center = i
        return center

    def init_center(self):
        """
        init centers in KMeans++
        :return: None
        """
        n = len(self.train_data)
        # choose the first center randomly
        self.cluster_centers.append(self.train_data[np.random.choice(n)])
        # choose the rest k-1 centers
        for i in range(1, self.k):
            # find the point with the largest distance to the nearest center
            dis_point_center = []
            # for each sample
            for j in range(n):
                nearest_center = self.get_nearest_center(j)
                dis = ecul_distance(self.train_data[j], self.cluster_centers[nearest_center])
                dis_point_center.append(dis)
            # add this point to centers
            self.cluster_centers.append(self.train_data[np.nanargmax(dis_point_center)])

    def fit(self, train_data):
        self.train_data = train_data
        self.init_center()
        # stores which cluster this sample belongs to
        cluster_assment = np.zeros(self.train_data.shape[0])

        center_changed = True
        iteration = 0
        while center_changed and iteration < self.max_iterations:
            center_changed = False
            # for each sample
            for i in range(len(self.train_data)):
                # find neatest center
                min_idx = self.get_nearest_center(self.train_data[i])

                # update cluster
                if cluster_assment[i] != min_idx:
                    center_changed = True
                    cluster_assment[i] = min_idx

            # update center
            for j in range(self.k):
                # find all points in cluster j
                points_in_cluster = []
                not_none = False
                for k in range(train_data.shape[0]):
                    if cluster_assment[k] == j:
                        points_in_cluster.append(True)
                        not_none = True
                    else:
                        points_in_cluster.append(False)
                # update center
                if not_none:
                    self.cluster_centers[j] = np.nanmean(train_data[points_in_cluster], axis=0)

            iteration = iteration + 1

        print("iterations for k-means: %d" % iteration)

    def predict(self, test_data, show=False):
        """
        predict (and visualize result) with k-means
        :param test_data: test_data
        :param show: True to visualize result and False not to
        :return:
        """
        ret = []
        for i in range(len(test_data)):
            ret.append(self.get_nearest_center(test_data[i]))
        if show:
            draw(self.k, np.array(ret), test_data)
        return np.array(ret)


class GaussianMixture:

    def __init__(self, n_clusters, max_iterations=50):
        self.n_clusters = n_clusters
        self.pi = np.random.randint(0, 100, size=n_clusters)
        self.pi = self.pi / np.nansum(self.pi)
        self.sigma = {}
        self.mu = {}
        self.gamma = None
        self.dim = 0
        self.max_iterations = max_iterations

    def init_param(self):
        """
        init parameters by k-means++
        :return: None
        """
        k_means_model = KMeans(self.n_clusters, max_iterations=20)
        k_means_model.fit(self.train_data)
        if len(self.train_data.shape) == 1:
            self.dim = 1
        else:
            self.dim = self.train_data.shape[1]
        for i in range(self.n_clusters):
            self.mu[i] = k_means_model.cluster_centers[i]
            self.sigma[i] = np.ones(self.dim)
            self.sigma[i] = np.diag(self.sigma[i])
        self.gamma = np.empty([self.train_data.shape[0], self.n_clusters])

    def point_probability(self, point):
        """
        calculate posterior distribution used in E step
        :param point: a point in dataset
        :return: the probability distribution of point belong to different clusters
        """
        ret = []
        for i in range(self.n_clusters):
            pt = point
            pt = point.reshape(-1, 1)
            mu = self.mu[i]
            mu = mu.reshape(-1, 1)
            sigma = self.sigma[i]
            sigma = np.matrix(sigma)
            D = self.dim
            coef = 1 / ((2 * math.pi) ** (D / 2) * (np.linalg.det(sigma)) ** 0.5)
            pw = -0.5 * np.matmul(np.matmul((pt - mu).T, sigma.I), (pt - mu))
            result = float(coef * np.exp(pw) + np.exp(-200))
            ret.append(result * self.pi[i])
        return ret

    def fit(self, train_data):
        self.train_data = train_data
        self.init_param()

        # train with EM, described clearly in report
        iteration = 0
        for iteration in range(self.max_iterations):
            # E step
            for i in range(train_data.shape[0]):
                temp = np.array(self.point_probability(train_data[i]))
                self.gamma[i, :] = temp
            self.gamma = self.gamma / self.gamma.sum(axis=1).reshape(-1, 1)

            # M step
            # update pi
            self.pi = np.nansum(self.gamma, axis=0) / train_data.shape[0]
            for label in range(self.n_clusters):
                mu = np.zeros(self.dim)
                sigma = np.zeros([self.dim, self.dim])
                for i in range(train_data.shape[0]):
                    mu += self.gamma[i, label] * train_data[i]
                    point = train_data[i].reshape(-1, 1)
                    label_mu = self.mu[label].reshape(-1, 1)
                    rest = point - label_mu
                    sigma += self.gamma[i, label] * np.matmul(rest, rest.T)
                # update mu
                self.mu[label] = mu / np.nansum(self.gamma, axis=0)[label]
                # update sigma
                self.sigma[label] = sigma / np.nansum(self.gamma, axis=0)[label]
        print("iterations for GMM: %d" % iteration)

    def predict(self, test_data, show=False):
        ret = []
        for i in test_data:
            prob_dist = self.point_probability(i)
            label = prob_dist.index(max(prob_dist))
            ret.append(label)
        if show:
            draw(self.n_clusters, np.array(ret), test_data)
        return np.array(ret)


class ClusteringAlgorithm:

    def __init__(self, model="kmeans"):
        if model == "kmeans":
            self.ModelClass = KMeans
        elif model == "gm":
            self.ModelClass = GaussianMixture

    def get_dis_mtx(self):
        """
        get the distance matrix for every two points to reduce double calculation
        :return: distance matrix
        """
        n = self.train_data.shape[0]
        distance = np.zeros((n, n))
        for i in range(n):
            distance[i] = (np.nansum((self.train_data[i] - self.train_data) ** 2, axis=1)) ** 0.5
        self.distance = distance

    def get_avg_dis(self, cluster_idx, point):
        """
        get the avg distance between point and points in cluster_idx
        :param cluster_idx:
        :param point:
        :return:
        """
        if cluster_idx[0].shape[0] < 2:
            return 0
        cluster_idx = np.delete(cluster_idx, np.where(cluster_idx == point))
        avg_dis = np.nanmean(self.distance[point, cluster_idx])
        return avg_dis

    def get_silhouette_coefficient(self, label, n_clusters):
        """
        calculate the silhouette coefficient for the clustering
        :param label: label of train data
        :param n_clusters: number of clusters
        :return: silhouette coefficient for this clustering
        """
        n = self.train_data.shape[0]
        cluster_idx = []
        for k in range(n_clusters):
            cluster_idx.append(np.where(label == k))
        S = []
        epsilon = 1e-9  # in case of division by zero
        # cal silhouette coefficient for each point
        for i in range(n):
            ai = self.get_avg_dis(cluster_idx[label[i]], i)
            bi = np.inf
            for k in range(n_clusters):
                if k == label[i]:
                    continue
                bi = min(bi, self.get_avg_dis(cluster_idx[k], i))
            S.append((bi - ai) / (max(bi, ai) + epsilon))
        return np.nanmean(np.array(S))

    def fit(self, train_data):
        """
        find the best number of clusters between 2 and 20 by silhouette coefficient
        :param train_data: train_data
        :return: None
        """
        self.train_data = train_data
        self.get_dis_mtx()
        max_sc = -1
        for n_clusters in range(2, 10):
            model = self.ModelClass(n_clusters)
            model.fit(train_data)
            label = model.predict(train_data)
            sc = self.get_silhouette_coefficient(label, n_clusters)
            if sc > max_sc:
                self.model = model
                max_sc = sc
            print("num of clusters: %2d, silhouette_coefficient: %.6f" % (n_clusters, sc))

    def predict(self, test_data, show=False):
        return self.model.predict(test_data, show)


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int) * i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_1():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (800,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))


    # plt.scatter(x[:, 0], x[:, 1])
    # plt.scatter(y[:, 0], y[:, 1])
    # plt.scatter(z[:, 0], z[:, 1])
    # plt.show()
    data, _ = shuffle(x, y, z)
    return (data, data), 3


if __name__ == "__main__":
    (data, _), n_clusters = data_1()
    if len(sys.argv) > 1 and sys.argv[1] == "--kmeans":
        model = KMeans(n_clusters)
        model.fit(data)
        model.predict(data, show=True)
    elif len(sys.argv) > 1 and sys.argv[1] == "--gm":
        model = GaussianMixture(n_clusters)
        model.fit(data)
        model.predict(data, show=True)
    elif len(sys.argv) > 1 and sys.argv[1] == "--auto_kmeans":
        model = ClusteringAlgorithm(model="kmeans")
        model.fit(data)
        model.predict(data, show=True)
    elif len(sys.argv) > 1 and sys.argv[1] == "--auto_gm":
        model = ClusteringAlgorithm(model="gm")
        model.fit(data)
        model.predict(data, show=True)
