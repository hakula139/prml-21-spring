# 作业三 聚类算法

实现了K-Means++和GMM模型，基于轮廓系数实现了自动选择聚簇数量的方法。

## K-Means和GMM模型的实现

### K-Means模型

K-Means算法的基本思想是：通过迭代寻找k个聚类的中心，使得用这k个聚类的均值代表相应各类样本时误差最小。误差指的是每个样本点到相应聚类中心距离之和。

其算法流程如下

```python
init_center()
while cluster_changed:
  for each in train_data:
    for center in centers:
      cal disance between center and each point
    assign each point to nearest cluster
  for each in clusters:
    cal mean in the cluster
    change the cluster center to this mean val
```

#### K-Means++

由于基础的K-Means算法随机选取k个聚类中心点，可能会选取到距离十分靠近的数据点，不仅导致算法收敛很慢，还会容易陷入局部最小值，聚类效果不稳定。

采用K-Means++方法初始聚类中心点以解决这个问题，其算法流程如下

```python
randomly choose the first center
for i in range(1, n_clusters):
  for each in train_data:
    cal distance to chosen centers
    let D(each) be the min distance
  choose the one point with the largest D
```

### GMM

GMM模型是由多个高斯分布组成的模型，其总体密度函数为多个高斯密度函数的加权组合。从高斯混合模型中生成一个样本x的过程可以分为两步，先根据多项分布随机选择一个高斯分布，再从选中的高斯分布中选取一个样本x。

给定N个由高斯混合模型生成的训练样本，希望能学习其中的参数$\pi_k,\mu_k,\sigma_k.$

根据EM算法，参数估计可以分为两步进行迭代：

（1）E步 固定参数$\mu,\sigma$，计算后验分布
$$
\gamma_{nk} \triangleq p(z^{(n)}=k|x^{(n)})\\\\
=\frac{\pi_k N(x^{(n)};\mu_k,\sigma_k)}{\sum_{k=1}^{K}\pi_kN(x^{(n)};\mu_k,\sigma_k)}
$$
（2）M步 令$q(z=k)=\gamma_{nk}$，训练集D的证据下界为
$$
ELBO(\gamma,D;\pi,\mu,\sigma)=\sum_{n=1}^N\sum_{k=1}^K\gamma_{nk}(\frac{-(x-\mu_k)^2}{2\sigma_k^2}-log\sigma_k+log\pi_k)+C
$$
将参数估计问题转化为优化问题，约束条件为
$$
\sum_{k=1}^K\pi_k=1
$$
利用拉格朗日乘数法求解，通过求偏导使其为0的办法，可得
$$
\pi_k=\frac{N_k}{N},
\mu_k=\frac{1}{N_k}\sum_{n=1}^N\gamma_{nk}x^{(n)},
\sigma_k^2=\frac{1}{N_k}\sum_{n=1}^N\gamma_{nk}(x^{(n)}-\mu_k)^2.
\\\\N_k=\sum_{n=1}^{N}\gamma_{nk}
$$
GMM模型初始化较为重要，这里使用最大迭代次数为20的K-Means++进行初始化。

## 实验

### 对比K-Means和K-Means++

下图是进行聚类的数据

![expr1](./img/expr1.png)

下图是使用K-Means初始化，聚类该数据的结果：

<img src="./img/kmeans1.png" alt="kmeans1" style="zoom:30%;" /><img src="./img/kmeans2.png" alt="kmeans2" style="zoom:30%;" />

<img src="./img/kmeans3.png" alt="kmeans3" style="zoom:30%;" /><img src="./img/kmeans4.png" alt="kmeans4" style="zoom:30%;" />

下图是使用K-Means++初始化之后，聚类该数据的结果，该结果非常稳定。

![kmeans++](./img/kmeans++.png)

可以看出，使用K-Means++初始化的实验结果与目的相符，可以逃离局部最小值，稳定聚类结果。

与此同时，进行10次实验后，使用K-Means++初始化需要的平均轮次为4，使用K-Mean初始化需要的平均轮次为7。所以，使用K-Means++初始化可以降低聚类轮次。

### 对比K-Means++和GMM

使用同样的数据对比K-Means++和GMM。下图是GMM进行聚类的效果

![gm1](./img/gm1.png)

可以看出，在该数据集上的表现，GMM表现与K-Means++类似。

接下来生成三簇距离较近但是数量较大的的数据集来进一步比较这两种聚类算法。数据集如下：

![expr2](./img/expr2.png)

K-Means++聚类结果如下：

![kmeans++2](./img/kmeans++2.png)

GMM聚类结果如下：

![gm2](./img/gm2.png)

可以看出，此种情况时K-Means++略优于GMM，但是差别依然不大。

虽然GMM聚类效果与K-Means++相差不大，但是运行时间要比K-Means++长不少，因为需要使用K-Means++模型进行参数的初始化。

## 自动选择聚簇数量

轮廓系数（Silhouette Coefficient）是评价聚类好坏的指标，结合了内聚度和分离度两个因素。其计算方法如下：

```python
for point in dataset:
  a(point) = avg distance from point to all points in its cluster
  b(point) = min(avg distance from point to other clusters)
  silhouette_coefficient(point) = (b(point) - a(point)) / max{a(point), b(point)}
```

显然，轮廓系数取值范围为[-1, 1]，取值越接近1则说明聚类性能越好，取值越接近-1则说明聚类性能越差。进一步地，将数据集内所有点的轮廓系数求平均就是该聚类结果总的轮廓系数。可以利用该指标来自动选择聚簇数量。具体来说，分别求属于[2, 20]的聚簇数量的总轮廓系数，然后选择对应总轮廓系数最大的聚簇数量即可。

### 实验

使用如下数据集进行自动选择聚类数量的实验：

![expr2](./img/expr2.png)

最终K-Means选择的聚簇数量为3，聚类效果如下：

![kmeans++3](./img/kmeans++3.png)

GMM选择的聚簇数量不稳定，有时是2，有时是3，聚类效果如下：

![auto_gm2](./img/auto_gm2.png)

## 使用方法

```python
python source.py --kmeans # 生成数据，使用K-Means模型进行聚类并观察效果
python source.py --gm # 生成数据，使用GMM进行聚类并观察效果
python source.py --auto_kmeans # 生成数据，自动选择聚簇数量并使用K-Means模型进行聚类并观察效果
python source.py --auto_gm # 生成数据，自动选择聚簇数量并使用GMM进行聚类并观察效果
```

