 **1.基础实验部分** 

 _1.1 数据生成与可视化实现_


```
def data_generator():
    # 生成三组multivariate_normal，数据长度为2000
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z)
    return (data, data), 3
```

```
def shuffle(*datas):
    # 随机打乱算法
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label
```

```
# 展示函数
def display(x,y):
    type1_x = []; type1_y = []
    type2_x = []; type2_y = []
    type3_x = []; type3_y = []

    plt.figure(figsize=(8,6))

    for i in range(0,len(x)):
        if(y[i]==0):
            type1_x.append(x[i][0])
            type1_y.append(x[i][1])
        if(y[i]==1):
            type2_x.append(x[i][0])
            type2_y.append(x[i][1])
        if(y[i]==2):
            type3_x.append(x[i][0])
            type3_y.append(x[i][1])
        
    fig = plt.figure(figsize = (10, 6))
    ax = fig.add_subplot(111)

    type1 = ax.scatter(type1_x, type1_y, s = 30, c = 'brown')
    type2 = ax.scatter(type2_x, type2_y, s = 30, c = 'lime')
    type3 = ax.scatter(type3_x, type3_y, s = 30, c = "darkviolet")



    ax.legend((type1, type2, type3), ("A", "B", "C"), loc = 0)

    plt.show()

```

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
73 & 0 \\\\
0 & 22
\end{array}\right] 
\mu=\left[\begin{array}{ll}
1 & 2
\end{array}\right]
\end{array}
$$


$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
21.2 & 0 \\\\
0 & 32.1
\end{array}\right] 
\mu=\left[\begin{array}{ll}
16 & -5
\end{array}\right]
\end{array}
$$

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
10 & 5 \\\\
5 & 10
\end{array}\right] 
\mu=\left[\begin{array}{ll}
10 & 22
\end{array}\right]
\end{array}
$$

 _1.2 样本可视化结果_


![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/122623_858b78c2_8823823.png "屏幕截图.png")


 _1.3 K-Means分类结果_


```
(train_data, test_data), n_clusters = data_generator()
exp =KMeans(n_clusters) 
exp.fit(train_data)
result = exp.predict(test_data)
display(test_data,exp.predict(test_data))
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/122208_5966ec9c_8823823.png "屏幕截图.png")


 _1.4 GaussianMixture分类结果_


```
exp =GaussianMixture(n_clusters) 
exp.fit(train_data)
result = exp.predict(test_data)
display(test_data,exp.predict(test_data))
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/123050_f971e80a_8823823.png "屏幕截图.png")

 **2.自动选择聚簇数量(Elbow Method 配合 K-Means 算法)** 

 _2.1 fit算法实现_


```
    def fit(self, train_data):
        train_data=self.normal(train_data)
        numpoint = len(train_data)
        dist_list =[]
        
        upper_bound = min([len(train_data)+1,20])
        
        # 遍历(1,upperbound)计算distance 
        for cluster in range(1,upper_bound):
            KMean = KMeans(cluster)
            Centroids,label = KMean.fit(train_data)
            temp_length=0
            for n in range(cluster):
                temp_length = temp_length+self.length_cal(Centroids[n],train_data[label==n])
            dist_list.append(temp_length)    
        
        # 当变化率小于5%，选取最佳K
        percent_change=[]
        for i in range(len(dist_list)-1):
            percent_change.append((dist_list[i]-dist_list[i+1])/dist_list[i]*100)
        for i in range(len(percent_change)): 
            if (percent_change[i]<5):
                self.optimal = i+1
                print(i+1)
                return dist_list,percent_change
                break
```

 _2.2 数据可视化_

原始数据

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
1 & 0 \\\\
0 & 1
\end{array}\right] 
\mu=\left[\begin{array}{ll}
2 & 2
\end{array}\right]
\end{array}
$$


$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
2 & 0 \\\\
0 & 2
\end{array}\right] 
\mu=\left[\begin{array}{ll}
4 & 6
\end{array}\right]
\end{array}
$$

$$
\begin{array}{l}
\Sigma=\left[\begin{array}{cc}
2 & 1 \\\\
1 & 3
\end{array}\right] 
\mu=\left[\begin{array}{ll}
10 & 10
\end{array}\right]
\end{array}
$$



![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/130337_efd2f52a_8823823.png "屏幕截图.png")

clustering数据

![输入图片说明](https://images.gitee.com/uploads/images/2021/0614/130434_98084bb3_8823823.png "屏幕截图.png")

