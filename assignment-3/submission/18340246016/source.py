import numpy as np

class KMeans:
    
    # 初始化Centroids
    def initCentroids(self,dataSet,k):
        centers = dataSet[:k]
        return centers
    
    #dataSet分类至不同centroids下
    def cluster_classifier(self,centroids,dataSet):
        distances = np.sqrt(((dataSet - centroids[:, np.newaxis])**2).sum(axis=2))
        return np.argmin(distances, axis=0)   

    def __init__(self, n_clusters):
        self.n_clusters = n_clusters

    def fit(self, train_data):
        
        #初试化Centroids以及Centroids
        Centroids = self.initCentroids(train_data,self.n_clusters)
        temp_label = self.cluster_classifier(Centroids,train_data)
        
        #开始自适应fit，直到Centroids不再更新
        while(True):
            for i in range(self.n_clusters):
                Centroids[i] = np.mean(train_data[temp_label==i])
            updated_label = self.cluster_classifier(Centroids,train_data)   
            if (updated_label.all() == temp_label.all()):
                # 储存final Centroids
                self.Centroids=Centroids
                return Centroids,updated_label
                break
            else:
                temp_label=updated_label      
    
    def predict(self, test_data):
        #根据fit更新过的centroids分类testdata
        return self.cluster_classifier(self.Centroids,test_data)



class GaussianMixture:
    
    #正则化
    def normal(self,raw_data):
        norm = np.linalg.norm(raw_data)
        normal_array = raw_data/norm
        return normal_array
    
    #初始化参数 mu,var,Pi,W
    def ini_para(self,n_clusters,train_data):
        self.epsilon = 1e-12
        dim = train_data.shape[1]
        mu=[]
        var=[]
        ones=[1]

        for i in range(n_clusters):
            mu.append(np.random.normal(0, 1, size=(dim,)))
            var.append(ones*dim)
        Pi = [1 / n_clusters] * n_clusters
        W = np.ones((len(train_data), n_clusters)) / n_clusters 
        Pi = W.sum(axis=0) / W.sum()
        self.dim = dim

        
        return np.array(mu),np.array(var),W,Pi

    def __init__(self, n_clusters):
        self.n_clusters = n_clusters
    
    # multivariate_normal density function
    def multivariate_normal(self,x, mu, cov):
        part1 = 1 / ( ((2* np.pi)**(len(mu)/2)) * (np.linalg.det(cov)**(1/2)) )
        part2 = (-1/2) * ((x-mu).T.dot(np.linalg.inv(cov))).dot((x-mu))
        return float(part1 * np.exp(part2))
    
        
    def update_W(self,X, Mu, Var, Pi):
        n_points, n_clusters = len(X), len(Pi)
        pdfs = np.zeros(((n_points, n_clusters)))
        for m in range(n_points):
            for i in range(n_clusters):
                pdfs[m, i] = Pi[i] * self.multivariate_normal(X[m], Mu[i], (np.diag(Var[i])))
        W = pdfs / pdfs.sum(axis=1).reshape(-1, 1)
        return W


    def update_Pi(self,W):
        Pi = W.sum(axis=0) / W.sum()
        return Pi
    
    def update_Mu(self,X, W):
        n_clusters = W.shape[1]
        Mu = np.zeros((n_clusters, self.dim))
        for i in range(n_clusters):
            Mu[i] = np.average(X, axis=0, weights=W[:, i])
        return Mu

    def update_Var(self,X, Mu, W):
        n_clusters = W.shape[1]
        Var = np.zeros((n_clusters, self.dim))
        for i in range(n_clusters):
            Var[i] = np.average((X - Mu[i]) ** 2, axis=0, weights=W[:, i])+self.epsilon
        return Var
        
    
    def fit(self, train_data):
        train_data = self.normal(train_data)
        n_clusters = self.n_clusters
        
        
        Mu,Var,W,Pi = self.ini_para(n_clusters,train_data)
        n_points = len(train_data)
        X=train_data
        
        Mu_init=Mu
        Var_init=Var
        i=0
        
        # 10次loop内更新参数
        for i in range(10):
            i=i+1
            W = self.update_W(X, Mu, Var, Pi)
            Pi = self.update_Pi(W)
            Mu = self.update_Mu(X, W)
            Var = self.update_Var(X, Mu, W)
        
            if (Mu_init.all()==Mu.all)and(Var_init.all()==Var.all()):
                break
            else:
                Mu_init=Mu
                Var_init=Var
        
        self.Pi =Pi
        self.W=W
        self.Mu=Mu
        self.Var=Var

    
    def predict(self, test_data):
        test_data = self.normal(test_data)
        prob= self.update_W(test_data, self.Mu, self.Var, self.Pi)
        return np.argmax(prob,axis=1)

class ClusteringAlgorithm:
    
    def normal(self,raw_data):
        norm = np.linalg.norm(raw_data)
        normal_array = raw_data/norm
        return normal_array

    def __init__(self):
        pass
    
    def length_cal(self,Centroids,points):
        total=0
        for point in points:
            weight_array = (point-Centroids)**2
            weight_array_sum = np.sum(weight_array)
            total = total + weight_array_sum**(0.5)
            
        return total
    
    def fit(self, train_data):
        train_data=self.normal(train_data)
        numpoint = len(train_data)
        dist_list =[]
        
        upper_bound = min([len(train_data)+1,20])
        
        # 遍历(1,upperbound)计算distance 
        for cluster in range(1,upper_bound):
            KMean = KMeans(cluster)
            Centroids,label = KMean.fit(train_data)
            temp_length=0
            for n in range(cluster):
                temp_length = temp_length+self.length_cal(Centroids[n],train_data[label==n])
            dist_list.append(temp_length)    
        
        # 当变化率小于5%，选取最佳K
        percent_change=[]
        for i in range(len(dist_list)-1):
            percent_change.append((dist_list[i]-dist_list[i+1])/dist_list[i]*100)
        for i in range(len(percent_change)): 
            if (percent_change[i]<5):
                self.optimal = i+1
                print(i+1)
                return dist_list,percent_change
                break
        
    def predict(self, test_data):
        test_data = self.normal(test_data)
        KMean_ready = KMeans(self.optimal)
        KMean_ready.fit(test_data)
        return KMean_ready.predict(test_data)