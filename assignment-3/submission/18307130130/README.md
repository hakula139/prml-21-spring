# Assignment-3 Report

> 姓名：李睿琛
>
> 学号：18307130130

## 1. K-Means

### 1.1 算法实现

选择k个初始中心点，计算每个样本到每个聚类中心的距离，将样本归到最近的类别中，对每个类别计算新的聚类中心，重复直到所有样本聚类结果不变化。

**初始中心的选择**

初始聚类中心的选择对聚类结果有较大的影响，选取不恰当的初始中心，容易导致获得局部最优解而非全局最优解，这里参考实现了K-Means++算法。

* 随机选取一个点作为第一个聚类中心
* 计算所有样本与第一个聚类中心的距离。
* 选择出上一步中距离最大的点作为第二个聚类中心。
* 计算所有点到与之最近的聚类中心的距离，选取最大距离的点作为新的聚类中心。
* 直到选出了这k个中心。

**距离度量**

使用欧式距离：`np.sqrt(np.sum(np.power(vecA - vecB, 2)))`

### 1.2 预测

对于测试样本，找到最近的聚集中心，即为对应类别。星号即为聚类中心。

![](./img/clusterKMEANS.png)


### 1.3 输出结果

输出每次迭代聚类结果发生变化的样本数：

```
聚类结果发生变化的样本数： 1049
聚类结果发生变化的样本数： 155
聚类结果发生变化的样本数： 133
聚类结果发生变化的样本数： 96
聚类结果发生变化的样本数： 60
聚类结果发生变化的样本数： 37
```

## 2. GMM

GMM也可以看作是K-means的推广，因为GMM不仅是考虑到了数据分布的均值，也考虑到了协方差。 GMM假设数据是从多个高斯分布中生成的：有K个高斯分布，赋予每一个分布一个权重，每当生成一个数据时，就按权重的比例随机选择一个分布，然后按照该分布生成数据。因此使用数据进行反推，通过GMM对三个高斯分布的参数估计出来。

 ```python
# 第一簇的数据
num1, mu1, var1 = 400, [0.5, 0.5], [1, 3]
X1 = np.random.multivariate_normal(mu1, np.diag(var1), num1)
# 第二簇的数据
num2, mu2, var2 = 600, [5.5, 2.5], [2, 2]
X2 = np.random.multivariate_normal(mu2, np.diag(var2), num2)
# 第三簇的数据
num3, mu3, var3 = 1000, [1, 7], [6, 2]
X3 = np.random.multivariate_normal(mu3, np.diag(var3), num3)
 ```

### 2.1 算法实现

初始化均值和标准差参数后，使用最大期望算法：E步、M步， 然后重复进行以上两步，直到最大似然收敛。

* **E步：**

![](./img/estep.png)

* **M步：**

![](./img/mstep.png)

**自定义高斯分布函数**

```python
def Gaussian(self,x,mean,cov):
    """
    自定义的高斯分布概率密度函数
    :param x: 输入数据
    :param mean: 均值数组
    :param cov: 协方差矩阵
    :return: x的概率
    """
    dim = np.shape(cov)[0]
    # cov的行列式为零时的措施
    covdet = np.linalg.det(cov + np.eye(dim) * 0.001)
    covinv = np.linalg.inv(cov + np.eye(dim) * 0.001)
    xdiff = (x - mean).reshape((1,dim))
    # 概率密度
    prob = 1.0/(np.power(np.power(2*np.pi,dim)*np.abs(covdet),0.5))*\
           np.exp(-0.5*xdiff.dot(covinv).dot(xdiff.T))[0][0]
    return prob
```

**最大似然收敛**

```python
while np.abs(loglikelyhood - oldloglikelyhood) > 0.0001
    ...
    loglikelyhood = np.sum([np.log(np.sum([self.weights_[k]*self.Gaussian(train_data[n], self.means_[k], self.covariances_[k]) for k in range(self.k)])) for n in range(n_samples) ])
```

**结合Kmeans的参数初始化**

运行`test_demo`中一维数据时，发现随机初始**均值和标准差**分裂结果很差，于是结合Kmeans方法进行初始化。

```python
# 随机初始化，放弃使用
self.covariances_ = np.array([np.eye(d)] * self.k) * 0.1
self.means_ = np.array([1.0 / self.k] * self.k)

# 使用Kmeans初始化标准差和均值
Kmeansmodel = KMeans(self.k)
Kmeansmodel.fit(train_data)
result = Kmeansmodel.predict(train_data)
# 初始化均值
self.means_ = Kmeansmodel.centroids
self.covariances_ = []
for i in range(self.k):
    tmp = train_data[np.where(result == i)] - self.means_[i]
    cov = np.eye(self.means_[0].shape[1])
    cov_tmp = np.array(np.power(np.sum(np.multiply(tmp, tmp), axis=0), 0.5))
    for index in range(len(cov_tmp[0])):
        cov[index][index] = cov_tmp[0][index]
    # 初始化标准差
    self.covariances_.append(cov)
```

### 2.2 预测

对于测试样本，计算其属于每个分布的概率，概率最大的即为所属类别。

![](./img/clusterGMM.png)

GMM涉及非矩阵运算，**速度慢。**


### 2.3 输出结果

输出每次迭代最大似然变化

```
2   原似然：  -13877.619900915008 新似然：  -13447.877245238586
3   原似然：  -13447.877245238586 新似然：  -13227.228798616086
4   原似然：  -13227.228798616086 新似然：  -13172.244969227962
5   原似然：  -13172.244969227962 新似然：  -13168.077341797663
6   原似然：  -13168.077341797663 新似然：  -13166.698044582836
```


## 3. 自动选择聚簇数量

使用数据同GMM。

### 3.1 Elbow Method

Elbow Method公式 :
$$
D_k = \sum _{i=1}^{K}\sum_{x_j\in C_i} dist(x_j, \mu_i)^2
$$
计转折点为`Elbow`，k小于`Elbow`时，k每增加1，$$D_k$$会大幅减小；当大于`Elbow`时，$$D_k$$变化就没有那么明显。

![](./img/Elbow1.png)

有些数据集在Elbow上的转折趋势不够明显，因此可以采用BIC准则方法

### 3.2 BIC准则

BIC是对模型拟合程度和复杂度的一种度量方式[1]，其中$$\hat l(D)$$表示最大似然。
$$
BIC(\phi) = \hat l(D)- {\frac{p_\phi}{2}} \times logR
$$

![](./img/likelihood.png)

输出结果：(4, -9789.54526448437)中4代表聚类数目，-9789.54526448437代表对应BIC值。

```python
[(4, -9789.54526448437), (3, -9811.33552800443), (5, -9958.834817656798), (6, -9993.632222469756), (7, -10090.665980345215), (8, -10132.64857344288), (2, -10138.346440921536), (9, -10151.237746415201)]
```

![](./img/bic.png)

因此选择k=4，分析结果k=3与k=4的BIC值相似，所以认为k=4也可以接受，星号为聚集中心：

![](./img/clusterBIC.png)


## 4. Reference

[1] [Notes on Bayesian Information Criterion Calculation for X-means Clustering](https://github.com/bobhancock/goxmeans/blob/master/doc/BIC_notes.pdf)