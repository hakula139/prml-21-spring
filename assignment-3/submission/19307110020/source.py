import numpy as np


def calculate_distance(x1, x2):
    return np.mean(np.sqrt((x1 - x2) ** 2))


class KMeans:
    def __init__(self, n_cluster, epochs=10, quadra_plus_threshold=10):  # class initialization
        self.n_cluster = n_cluster
        self.epochs = epochs
        self.centers = []
        self.quadra_plus_threshold = quadra_plus_threshold

    def fit(self, train_data):
        def find_nearest_centroid(point):
            """
            find the nearest class center and returns the distance.
            :param point: query point
            :return: minimum distance to every current center.
            """
            res = calculate_distance(point, centroid_list[0])
            for _ in range(1, len(centroid_list)):
                res = min(res, calculate_distance(point, centroid_list[_]))
            return res

        # randomly initialize the first centroid.
        points_list = list(train_data)
        centroid_list = []
        index = np.random.randint(0, len(points_list) - 1)
        centroid_list.append(points_list[index])
        del points_list[index]

        # randomly choose the rest cluster centers.
        # the sampling weight of each point is the minimum distance to every current center.
        for _ in range(self.n_cluster - 1):
            distances = [find_nearest_centroid(__) for __ in points_list]
            distances = np.array([x ** 2 for x in distances])
            tmpmean = distances.mean()
            if self.quadra_plus_threshold is not None:  # filter outsiders!
                for i in range(distances.shape[0]):
                    if distances[i] > self.quadra_plus_threshold * tmpmean:
                        distances[i] = 0.
            weighted_list = distances / np.sum(distances)
            chosen_index = np.random.choice(np.arange(len(distances)), p=weighted_list)
            centroid_list.append(points_list[chosen_index])
            del points_list[chosen_index]
            self.centers = centroid_list

        # Ordinary KMeans iterative steps.
        # Predict the cluster of each point and update class centers with the mean of points assigned to this cluster.
        for epoch in range(self.epochs):
            self.predict_cluster = self.predict(train_data)
            for ct in range(len(self.centers)):
                idx, = np.where(self.predict_cluster == ct)
                samples = train_data[idx, :]
                self.centers[ct] = np.mean(samples, axis=0)

        return self

    def predict(self, test_data):
        """
        Predict the cluster of each point according to its nearest centroid.
        :return: predicted cluster labels
        """
        predict_cluster = np.zeros(shape=(len(test_data),))
        for n, arr in enumerate(test_data):
            predict_cluster[n] = np.argmin([calculate_distance(arr, _) for _ in self.centers])  # minimum distance
        return predict_cluster


def scale_data(data):
    """
    standardization of input data
    :param data: np.array
    :return: min-max standardized data
    """
    for i in range(data.shape[1]):
        max_ = data[:, i].max()
        min_ = data[:, i].min()
        data[:, i] = (data[:, i] - min_) / (max_ - min_)
    return data


def normal(x, mean, cov):
    """
    :param x: query data
    :param mean: mean of the given normal distribution
    :param cov: covariance matrix of the given normal distribution
    :return: pdf value
    """
    s, u = np.linalg.eigh(cov)
    t = s.dtype.char.lower()
    eps = 1E6 * np.finfo(t).eps * np.max(abs(s))
    s_pinv = np.array([0 if abs(x) <= eps else min(1/x, 1e8) for x in s], dtype=float)
    d = s[s > eps]
    maha = np.sum(np.square(np.dot((x - mean), np.multiply(u, np.sqrt(s_pinv)))), axis=-1)
    return np.exp(-0.5 * (len(d) * np.log(2 * np.pi) + np.sum(np.log(d)) + maha))


class GaussianMixture:
    def __init__(self, n_cluster, epochs=200):
        self.n_cluster = n_cluster
        self.p = np.random.rand(self.n_cluster)
        self.p = self.p / self.p.sum()      # all p's sum up to 1
        self.epochs = epochs

    def fit(self, train_data):
        self.train_data = scale_data(train_data)
        self.N, self.D = train_data.shape
        self.means = np.array(KMeans(self.n_cluster).fit(train_data).centers)  # K-means++ initialization
        self.covs = np.array([np.eye(self.D)] * self.n_cluster)
        self.alpha = np.ones(self.n_cluster) / self.n_cluster  # same prior
        for epoch in range(self.epochs):  # iterative optimization
            self.e_step()
            self.m_step()
        return self

    def e_step(self):
        """
        ordinary expectation step
        """
        self.gamma = np.zeros((self.N, self.n_cluster))
        for k in range(self.n_cluster):
            self.gamma[:, k] = self.alpha[k] * normal(self.train_data, self.means[k], self.covs[k])
        for i in range(self.N):
            self.gamma[i, :] /= np.sum(self.gamma[i, :])

    def m_step(self):
        """
        ordinary maximization step
        """
        for k in range(self.n_cluster):
            nk = np.sum(self.gamma[:, k])
            self.means[k, :] = np.sum(self.train_data * self.gamma[:, k].reshape(-1, 1), axis=0) / nk
            bias = self.train_data - self.means[k]
            self.covs[k] = np.matmul(bias.T, (bias * self.gamma[:, k].reshape(-1, 1))) / nk
            self.alpha[k] = nk / self.N

    def predict(self, test_data):
        """
        :return: the predicted labels given priors and distribution
        """
        test_data = scale_data(test_data)
        density = np.empty((test_data.shape[0], self.n_cluster))
        for i in range(self.n_cluster):
            density[:, i] = normal(test_data, self.means[i], self.covs[i])
        posterior = density * self.p
        predicted = np.argmax(posterior, axis=1)
        return predicted


class ClusteringAlgorithm:
    def __init__(self, candidate_max=10, it=10):
        self.it = it
        self.candidate_list = range(2, candidate_max)  # candidate_max must be larger than 2

    def fit(self, train_data):
        def generate_uniform_points(data):
            """
            :return: a set of points uniformly sampled from the bounding box (can be rectangle or hyper-cubes)
            """
            mins = np.argmin(data, axis=0)
            maxs = np.argmax(data, axis=0)
            reference_data_set = np.zeros((data.shape[0], data.shape[1]))
            for i in range(data.shape[0]):
                for j in range(data.shape[1]):
                    reference_data_set[i][j] = np.random.uniform(data[mins[j]][j], data[maxs[j]][j])
            return reference_data_set

        def dispersion(data, k):
            """
            :return: expectation of the logD_k, the first item in the gap statistics function
            """
            model = KMeans(k).fit(data)
            distances_from_mean = np.arange(k)
            for i in range(k):
                for idx, label in enumerate(model.predict_cluster):
                    if i == label:
                        distances_from_mean[i] += np.sum((data[idx] - model.centers[i]) ** 2)
            return np.log(np.sum(distances_from_mean))

        def gap_statistic(data, nthcluster):
            actual_dispersion = dispersion(data, nthcluster)
            # ref_dispersion is the dispersion of the given data, sum of distances between all points and their centroid
            ref_dispersion = np.mean([dispersion(generate_uniform_points(data), nthcluster) for _ in range(self.it)])
            return actual_dispersion, ref_dispersion

        # the following code block computes the optimum K
        dispersion_values = np.zeros((len(self.candidate_list), 2))
        for i, cluster in enumerate(self.candidate_list):
            dispersion_values[i] = gap_statistic(train_data, cluster)
        gaps = dispersion_values[:, 1] - dispersion_values[:, 0]
        n_clusters = np.argmax(gaps) + 2  # plus 2 because K starts from 2

        # ordinary fit and predict given n_clusters
        self.model = KMeans(n_clusters)
        return self.model.fit(train_data)

    def predict(self, test_data):
        return self.model.predict(test_data)