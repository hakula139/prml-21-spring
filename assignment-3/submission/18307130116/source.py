import numpy as np
import random
import matplotlib.pyplot as plt
import math
import matplotlib.cm as cm
from numpy.core.fromnumeric import argmin

def data_preprocess(data):
    """preprocess the data

    use the range of data to transform the data

    Args:
        data(numpy.ndarray):raw data

    Return:
        numpy.ndarray: data after process
    """

    edge = max(abs(data.max()), abs(data.min()))
    result_data = (data*10)/edge
    return result_data


def compute_SC(data, label, class_num):
    """compute the Silhouette Coefficient

    Args:
        data(numpy.ndarray): data for compute
        lable(list):label for every point
        class_num(int): the number of cluster
    
    Return:
        int: the value of Silhouette Coefficient
    """
    point_dict = {}
    data = data_preprocess(data)
    if len(data.shape) == 1:
        dimention = 1
    else:
        dimention = data.shape[1]
    for iter in range(class_num):
        point_dict[iter] = []
    for iter in range(len(data)):
        point_dict[label[iter]].append(data[iter])
    result = 0
    for iter in range(len(data)):
        now_point = data[iter]
        now_point = now_point.reshape(-1, 1)
        inner_dis = 0
        now_label = label[iter]
        for other in point_dict[now_label]:
            other = other.reshape(-1, 1)
            temp = 0
            for i in range(dimention):
                temp = temp + (now_point[i]-other[i]) ** 2
            inner_dis = inner_dis + temp**0.5
        inner_dis = inner_dis / (len(point_dict[now_label]) - 1)
        out_dis_min = math.inf
        for label_iter in range(class_num):
            if label_iter == now_label:
                continue
            out_dis = 0
            for other in point_dict[label_iter]:
                other = other.reshape(-1, 1)
                temp = 0
                for i in range(dimention):
                    temp = temp + (now_point[i]-other[i]) ** 2
                out_dis = out_dis + temp**0.5
            out_dis = out_dis / len(point_dict[label_iter])
            if out_dis < out_dis_min:
                out_dis_min = out_dis
        result = result + (out_dis_min - inner_dis)/max(out_dis_min, inner_dis)
    result = result / len(data)
    return result


def data_generate_and_save(class_num, mean_list, cov_list, num_list, save_path = "", method = "Gaussian"):
    """generate data that obey Guassian distribution
    
    label will be saved in the meantime.

    Args:
        class_num(int): the number of class
        mean_list(list): mean_list[i] stand for the mean of class[i]
        cov_list(list): similar to mean_list, stand for the covariance
        num_list(list): similar to mean_list, stand for the number of points in class[i]
        save_path(str): the data storage path, end with slash.
        method(str): the distribution data will follow, support gaussian and lognormal
    """
    if method == "lognormal":
        data = np.random.lognormal(mean_list[0], cov_list[0], num_list[0])
    elif method == "Gaussian":
        data = np.random.multivariate_normal(mean_list[0], cov_list[0], (num_list[0],))
    label = np.zeros((num_list[0],),dtype=int)
    total = num_list[0]
    
    for iter in range(1, class_num):
        if method == "lognormal":
            temp = np.random.lognormal(mean_list[0], cov_list[0], num_list[0])
        elif method == "Gaussian":
            temp = np.random.multivariate_normal(mean_list[0], cov_list[0], (num_list[0],))
        label_temp = np.ones((num_list[iter],),dtype=int)*iter
        data = np.concatenate([data, temp])
        label = np.concatenate([label, label_temp])
        total += num_list[iter]
    
    idx = np.arange(total)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    train_num = int(total * 0.8)
    train_data = data[:train_num, ]
    test_data = data[train_num:, ]
    train_label = label[:train_num, ]
    test_label = label[train_num:, ]
    np.save(save_path+"data.npy", ((train_data, train_label), (test_data, test_label)))


def data_load(path = ""):
    """load data from path given

    data should follow the format(data, label)

    Args:
        path(str): the path data stored in
    
    Return:
        tuple: stand for the data and label
    """

    (train_data, train_label), (test_data, test_label) = np.load(path+"data.npy",allow_pickle=True)
    return (train_data, train_label), (test_data, test_label)


def visualize(data, label=None, dimention = 2, class_num = 1, test_data=np.array([None]), color=False):
    """draw a scatter
    
    if you want to distinguish class with color, parameter color should be True.
    It will distribute color for each class automatically.
    The test data will be marked with the label of plus

    Args:
        data(numpy.ndarray):train dataset
        label(numpy.ndarray):label for train data
        class_num(int):the number of clusters, only used when color = True
        test_data(numpy.ndarray): test dataset
        color(boolean): True if you want different for each class, otherwise false 
        dimention(int): the data dimention, should be 1 or2
    """
    
    if color == True:
        data_x = {}
        data_y = {}
        for iter in range(class_num):
            data_x[iter] = []
            data_y[iter] = []
        if dimention == 2:
            for iter in range(len(label)):
                data_x[label[iter]].append(data[iter, 0])
                data_y[label[iter]].append(data[iter, 1])
        elif dimention == 1:
            for iter in range(len(label)):
                data_x[label[iter]].append(data[iter])
                data_y[label[iter]].append(0)
        colors = cm.rainbow(np.linspace(0, 1, class_num))
        for class_idx, c in zip(range(class_num), colors):
            plt.scatter(data_x[class_idx], data_y[class_idx], color=c)
        if(test_data.any() != None):
            if dimention == 2:
                plt.scatter(test_data[:, 0], test_data[:, 1], marker='+')
            elif dimention == 1:
                plt.scatter(test_data, np.zeros(len(test_data)), marker='+')
    else:
        if dimention == 2:
            plt.scatter(data[:, 0], data[:, 1], marker='o')
        elif dimention == 1:
            plt.scatter(data, np.zeros(len(data)), marker='o')
        if(test_data.any() != None):
            if dimention == 2:
                plt.scatter(test_data[:, 0], test_data[:, 1], marker='+')
            elif dimention == 1:
                plt.scatter(test_data, np.zeros(len(test_data)), marker='+')
    plt.show()

        
class Canopy:
    """Canopy clustering method

    The model will init thereshold automatically according to
    the dimention of dataset
    A low accuracy method to get cluster number K for the whole dataset

    Attribute:
        t1(int): stand for the first thershold
        t2(int): stand for the second thereshold
        dimention: dimention of data
    """
    def __init__(self):
        """init the whole model
        """
        self.t1 = 0
        self.t2 = 0
        self.dimention = 0
    
    def get_distance(self, point1, point2, method="Euclidean"):
        """Compute the distance between two points

        Check the dimention first and will throw an warning if dimention
        differ. Will use the mininum dimention for compute. Different method will 
        support in the future.

        Args:
            point1(numpy.ndarray):One point for compute
            point2(numpy.ndarray):The other point for compute
            method(str):The way to compute distance
        
        Return:
            float: distance between two points
        """

        dis = 0
        point1 = point1.reshape(-1, 1)
        point2 = point2.reshape(-1, 1)
        if method == "Euclidean":
            for iter in range(self.dimention):
                dis += (point1[iter]-point2[iter]) ** 2
            return dis ** 0.5
    
    def fit(self, train_data):
        """train the model
        
        Args:
            train_data(numpy.ndarray): dataset for training
        
        Return:
            list: contain the turple with the format of (center point, [points around])
        """

        train_data = data_preprocess(train_data)
        train_num = train_data.shape[0]
        if len(train_data.shape) == 1:
            self.dimention = 1
        else:
            self.dimention = train_data.shape[1]
        self.t2 = 2 * self.dimention**0.5
        self.t1 = 2 * self.t2
        result = []
        while len(train_data) >= 0.2 * train_num:
            idx = random.randint(0, len(train_data) - 1)
            center = train_data[idx]
            point_list = []
            point_need_delete = []
            train_data = np.delete(train_data, idx, 0)
            for iter in range(len(train_data)):
                dis = self.get_distance(train_data[iter], center)
                if dis < self.t2:
                    point_need_delete.append(iter)
                elif dis < self.t1:
                    point_list.append(train_data[iter])
            result.append((center, point_list))
            train_data = np.delete(train_data, point_need_delete, 0)
        return result


class KMeans:
    """Kmeans Clustering Algorithm

    Attributes:
        n_clusters(int): number of clusters
        cluster_center(list): center of each cluster
        class_point_dict(dict): point dict of each cluster
        dimention(int): dimention of data
    """

    def __init__(self, n_clusters):
        """Inits the clusterer

        Init the points dicts and number of clusters with empty.
        Init the cluster number with argument

        Args:
            n_clusters(int): number of clusters
        """

        self.n_clusters = n_clusters
        self.cluster_center = []
        self.class_point_dict = {}
        self.dimention = 0
    
    def fit(self, train_data):
        """Train the clusterer
        
        Get the dimention of data and random select the center of each cluster first.
        Label each point with the label of cluster center nearest to it.
        Loop until cluster center don't change

        Args:
            train_data(numpy.ndarray): training data for this task
        """
        
        train_data = data_preprocess(train_data)
        train_data_num = train_data.shape[0]
        if len(train_data.shape) == 1:
            self.dimention = 1
        else:
            self.dimention = train_data.shape[1]
        for iter in range(self.n_clusters):
            self.class_point_dict[iter] = []
        idx = random.sample(range(train_data_num), self.n_clusters)
        for iter in range(self.n_clusters):
            self.cluster_center.append(train_data[idx[iter]])
        for iter in train_data:
            label = self.get_class(iter)
            self.class_point_dict[label].append(iter)
        epoch = 0
        while not self.update_center() and epoch < 100:
            for label in range(self.n_clusters):
                self.class_point_dict[label] = []
            for iter in train_data:
                label = self.get_class(iter)
                self.class_point_dict[label].append(iter)
            epoch = epoch + 1
        
    def predict(self, test_data):
        """Predict label

        Args:
            test_data(numpy.ndarray): Data for test
        
        Return:
            numpy.ndarray: each label of test_data
        """

        test_data = data_preprocess(test_data)
        result = np.array([])
        for iter in test_data:
            label = self.get_class(iter)
            result = np.r_[result, np.array([label])]
        return result
    
    def get_class(self, point):
        """Get the point class according to center points

        Args:
            point(numpy.ndarray): Point for found
        
        Return:
            int: In range(clusters number), stand for the class
        """

        min_class = 0
        for iter in range(self.n_clusters):
            temp = self.get_distance(self.cluster_center[iter], point)
            if iter == 0:
                min_dis = temp
            else:
                if min_dis > temp:
                    min_dis = temp
                    min_class = iter
        return min_class
    
    def get_distance(self, point1, point2, method="Euclidean"):
        """Compute the distance between two points

        Check the dimention first and will throw an warning if dimention
        differ. Will use the mininum dimention for compute. Different method will 
        support in the future.

        Args:
            point1(numpy.ndarray):One point for compute
            point2(numpy.ndarray):The other point for compute
            method(str):The way to compute distance
        
        Return:
            float: distance between two points
        """

        dis = 0
        point1 = point1.reshape(-1, 1)
        point2 = point2.reshape(-1, 1)
        if method == "Euclidean":
            for iter in range(self.dimention):
                dis += (point1[iter]-point2[iter]) ** 2
            return dis ** 0.5
    
    def update_center(self):
        """use the class_point_dict to update the cluster_center

        Return：
            boolean:stand for whether center update or not
        """

        result = True
        for iter in range(self.n_clusters):
            temp = np.zeros(self.dimention)
            for point in self.class_point_dict[iter]:
                temp = temp + point
            temp = temp / len(self.class_point_dict[iter])
            result = result and (temp == self.cluster_center[iter]).all()
            self.cluster_center[iter] = temp
        return result


class GaussianMixture:
    """Gaussian mixture model for clustering

    Attributes:
        n_clusters(int): number of clusters
        pi(numpy.ndarray): probability for all the clusters
        cov(dict): covariance matrix for each cluster
        mean(dict): mean matrix for each cluster
        gamma(numpy.ndarray): gamma in EM algorithm
        epsilon(int): lowbound of ending threshold
        dimention(int): dimention of data
    """

    def __init__(self, n_clusters):
        """init the parameter for this model

        Randomly init the probability.
        Init cluster number with parameter

        Args:
            n_clusters(int): number of clusters
        """

        self.pi = np.ones(n_clusters)/n_clusters
        self.n_clusters = n_clusters
        self.cov = {}
        self.mean = {}
        self.gamma = None
        self.epsilon = 1e-20
        self.dimention = 0

    def fit(self, train_data):
        """Train the model, using EM

        Init the mean and covariance for each class first. The initial covariance matrix
        for every cluster is unit matrix, and kmeans to init the mean of each class
        In E step, calculate the probability for every point in every cluster.
        In M step, update the parameter for every distribution

        Args:
            train_data(numpy.ndarray): data for train
        """

        k_model = KMeans(self.n_clusters)
        k_model.fit(train_data)
        train_data = data_preprocess(train_data)
        train_num = train_data.shape[0]
        if len(train_data.shape) == 1:
            self.dimention = 1
        else:
            self.dimention = train_data.shape[1]
        for iter in range(self.n_clusters):
            self.mean[iter] = k_model.cluster_center[iter]
            self.cov[iter] = np.ones(self.dimention)
            self.cov[iter] = np.diag(self.cov[iter])
        self.gamma = np.empty([train_num, self.n_clusters])
        for i in range(20):
            #E step
            for iter in range(train_num):
                temp = np.array(self.point_probability(train_data[iter]))
                self.gamma[iter, :] = temp
            log_h_w = self.gamma
            self.gamma = self.gamma/self.gamma.sum(axis=1).reshape(-1, 1)
            #termination condition

            #M step
            self.pi = np.sum(self.gamma, axis=0)/train_num
            for label in range(self.n_clusters):
                mean = np.zeros(self.dimention)
                cov = np.zeros([self.dimention, self.dimention])
                for iter in range(train_num):
                    mean += self.gamma[iter, label] * train_data[iter]
                    point = train_data[iter].reshape(-1, 1)
                    label_mean = self.mean[label].reshape(-1, 1)
                    rest = point - label_mean
                    cov += self.gamma[iter, label] * np.matmul(rest, rest.T)
                self.mean[label] = mean/np.sum(self.gamma, axis=0)[label]
                self.cov[label] = cov/np.sum(self.gamma, axis=0)[label]
    
    def predict(self, test_data):
        """Predict label

        Args:
            test_data(numpy.ndarray): Data for test
        
        Return:
            numpy.ndarray: each label of test_data
        """

        test_data = data_preprocess(test_data)
        edge = max(abs(test_data.max()), abs(test_data.min()))
        test_data = (test_data*10)/edge
        result = []
        for iter in test_data:
            temp = self.point_probability(iter)
            label = temp.index(max(temp))
            result.append(label)
        return np.array(result)

    def point_probability(self, point):
        """calculate the probability of every gaussian distribution

        Args:
            point(numpy.ndarray): point need to be calculated
        
        Return:
            list: probability for every distribution
        """

        result = []
        for iter in range(self.n_clusters):
            result.append(self.calculate(point, iter) * self.pi[iter])
        return result

    def calculate(self, point, iter):
        """calculate the probability for the iter-th distribution

        Args:
            point(numpy.ndarray): the point need to calculate
            iter(int): the number of distribution
        
        Return:
            float: the probability of the point

        """

        point = point.reshape(-1, 1)
        mean = self.mean[iter]
        mean = mean.reshape(-1, 1)
        cov = self.cov[iter]
        cov = np.matrix(cov)
        D = self.dimention
        coef = 1/((2*math.pi) ** (D/2) * (np.linalg.det(cov))**0.5)
        pow = -0.5 * np.matmul(np.matmul((point - mean).T, cov.I), (point - mean))
        result = coef * np.exp(pow) + np.exp(-200)
        return float(result)
    

class ClusteringAlgorithm:
    """Auto cluster
    
    Automatically choose k and cluster the data, using Kmeans

    Attribute:
        K(int): the number of clusters
        cluster(class): the clusterer
    """

    def __init__(self):
        """init the clusterer
        """

        self.K = 3
        self.clusterer = None
    
    def fit(self, train_data, method="Elbow"):
        """train the cluster
        
        Automatically choos the number of clusters with given method
        For Elbow, we think if the difference between K-1, K, K+1 satisfies
        [K-1] -[K] <= 2*([k]- [k+1]), then the graph is smooth enough.
        will support Canopy in the future

        Args:
            train_data(numpy.ndarray): the dataset for training
            method(str): will support Elbow and Canopy
        """

        if method == "Elbow":
            train_num = train_data.shape[0]
            upbound = int((train_num/2)**0.5)+2
            dis = np.zeros(upbound)
            for i in range(1, upbound):
                self.clusterer = KMeans(i)
                self.clusterer.fit(train_data)
                label_dict = self.clusterer.class_point_dict
                center = self.clusterer.cluster_center
                for iter in range(i):
                    for point in label_dict[iter]:
                        dis[i] = dis[i]+self.clusterer.get_distance(point,center[iter])
            dis[0] = 6 * dis[1]
            if upbound <= 5:
                min = 1
                for i in range(1, upbound):
                    if dis[i] <= dis[min]:
                        min = i
            else:
                for min in range(1, upbound-1):
                    if dis[min-1]-dis[min] <= 2 * (dis[min]-dis[min+1]):
                        break
            self.clusterer = KMeans(min)
            self.clusterer.fit(train_data)
            print("choose {}".format(min))
        elif method == "Canopy":
            canopy = Canopy()
            K = len(canopy.fit(train_data))
            print("choose {}".format(K))
            self.clusterer = KMeans(K)
            self.clusterer.fit(train_data)

    
    def predict(self, test_data):
        """predict the test_data
        
        Args:
            test_data(numpy.ndarray): data for test
        
        Return:
            list: label for each point
        """

        return self.clusterer.predict(test_data)


if __name__ == "__main__":
    mean_list = [(1, 2), (16, -5), (10, 22)]
    cov_list = [np.array([[73, 0], [0, 22]]), np.array([[21.2, 0], [0, 32.1]]), np.array([[10, 5], [5, 10]])]
    num_list = [80, 80, 80]
    save_path = ""
    data_generate_and_save(3, mean_list, cov_list, num_list, save_path)
    (train_data, train_label), (test_data, test_label) = data_load()
    visualize(train_data, dimention=2, class_num=3)
    visualize(train_data, dimention=2, label=train_label, class_num=3, color = True)
    # print(train_data)
    # print(type(train_data))
    # print(train_data.shape)
    k = KMeans(3)
    k.fit(train_data)
    label1 = k.predict(train_data)
    visualize(train_data, dimention=2, label=label1, class_num=3, color=True)
    print(compute_SC(train_data, label1, 3))

    g = GaussianMixture(3)
    g.fit(train_data)
    label2 = g.predict(train_data)
    visualize(train_data, label=label2, dimention=2, class_num=3, color=True)
    print(compute_SC(train_data, label2, 3))

    # print(compute_SC(train_data, train_label, 3))
    # k = ClusteringAlgorithm()
    # k.fit(train_data, method="Elbow")
    # k.predict(train_data)
    # e = ClusteringAlgorithm()
    # e.fit(train_data, method="Canopy")

