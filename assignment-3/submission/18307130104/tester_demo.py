import numpy as np
import sys
import matplotlib.pyplot as plt
from source import KMeans, GaussianMixture, ClusteringAlgorithm


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_1():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z)
    return (data, data), 3


def data_2():
    train_data = np.array([
        [23, 12, 173, 2134],
        [99, -12, -126, -31],
        [55, -145, -123, -342],
    ])
    return (train_data, train_data), 2


def data_3():
    train_data = np.array([
        [23],
        [-2999]
    ])
    return (train_data, train_data), 2

def data_4():
    mean = -20
    cov = 1
    x = np.random.normal(mean, cov, 800)

    mean = 0
    cov = 1
    y = np.random.normal(mean, cov, 200)

    mean = 150
    cov = 1
    z = np.random.normal(mean, cov, 1000)

    mean = 20
    cov = 1
    k = np.random.normal(mean, cov, 1000)

    # plt.scatter(x, np.zeros(len(x)))
    # plt.scatter(y, np.zeros(len(y)))
    # plt.scatter(z, np.zeros(len(z)))
    # plt.scatter(k, np.zeros(len(k)))
    # plt.show()
    data, _ = shuffle(x, y, z, k)
    return (data[0:2500], data[2500:3000]), 4

def data_5():
    mean = (1, 2, 1, 4)
    cov = np.array([[73, 0, 0, 0], [0, 22, 0, 0], [0, 0, 11, 0], [0, 0, 0, 20]])
    x = np.random.multivariate_normal(mean, cov, (5000,))

    mean = (16, -5, 16, -5)
    cov = np.array([[21.2, 0, 0, 0], [0, 32.1, 0, 0], [0, 0, 11, 0], [0, 0, 0, 20]])
    y = np.random.multivariate_normal(mean, cov, (2000,))

    mean = (10, 22, 10, 22)
    cov = np.array([[10, 0, 0, 0], [0, 10, 0, 0], [0, 0, 10, 0], [0, 0, 0, 10]])
    z = np.random.multivariate_normal(mean, cov, (3000,))

    data, _ = shuffle(x, y, z)
    return (data, data), 3

def display(data, name):
    datas =[[],[],[]]
    for kind in range(3):
        for i in range(len(data[kind])):
            datas[kind].append(data[kind][i])
    
    for each in datas:
        each = np.array(each)
        if(each.size > 0):
            plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

def displayres(data, label, name):
    datas =[[],[],[]]
    for i in range(len(data)):
        datas[label[i]].append(data[i])
    
    for each in datas:
        each = np.array(each)
        if(each.size > 0):
            plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

def data_6():
    mean = (1, 2)
    cov = np.array([[33, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[11, 0], [0, 12]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    data, _ = shuffle(x, y, z)
    display([x, y, z], 'data')
    return (data, data), 3

def data_7():
    # mean = (1, 2)
    # cov = np.array([[33, 0], [0, 22]])
    # x = np.random.multivariate_normal(mean, cov, (2000,))

    # mean = (16, -5)
    # cov = np.array([[11, 0], [0, 12]])
    # y = np.random.multivariate_normal(mean, cov, (3000,))

    # mean = (10, 22)
    # cov = np.array([[10, 5], [5, 10]])
    # z = np.random.multivariate_normal(mean, cov, (1000,))

    # mean = (50, 60)
    # cov = np.array([[10, 5], [5, 10]])
    # z = np.random.multivariate_normal(mean, cov, (4000,))

    # data, _ = shuffle(x, y, z)
    # return (data[0: 8000], data[8000: 10000]), 4
    mean = -20
    cov = 1
    x = np.random.normal(mean, cov, 800)

    mean = 30
    cov = 1
    y = np.random.normal(mean, cov, 200)

    mean = 80
    cov = 1
    z = np.random.normal(mean, cov, 1000)

    mean = 140
    cov = 1
    a = np.random.normal(mean, cov, 1000)

    plt.scatter(x, np.zeros(len(x)))
    plt.scatter(y, np.zeros(len(y)))
    plt.scatter(z, np.zeros(len(z)))
    plt.scatter(a, np.zeros(len(a)))
    plt.show()

    data, _ = shuffle(x, y, z, a)
    return (data[0:2500], data[2500:3000]), 4

def test_with_n_clusters(data_fuction, algorithm_class):
    (train_data, test_data), n_clusters = data_fuction()
    model = algorithm_class(n_clusters)
    model.fit(train_data)
    res = model.predict(test_data)
    # displayres(test_data, res, "res")
    # print(res, test_data)
    assert len(
        res.shape) == 1 and res.shape[0] == test_data.shape[0], "shape of result is wrong"
    return res

def test_without_giving_n_clusters(data_fuction, algorithm_class):
    (train_data, test_data), n_clusters = data_fuction()
    model = algorithm_class()
    model.fit(train_data, 10)
    res = model.predict(test_data)
    return model.cluster_num

def testcase_1_1():
    test_with_n_clusters(data_1, KMeans)
    return True


def testcase_1_2():
    res = test_with_n_clusters(data_2, KMeans)
    return res[0] != res[1] and res[1] == res[2]

def testcase_1_3():
    res = test_with_n_clusters(data_7, KMeans)
    print(res)
    return True


def testcase_2_1():
    test_with_n_clusters(data_1, GaussianMixture)
    return True


def testcase_2_2():
    res = test_with_n_clusters(data_3, GaussianMixture)
    return res[0] != res[1]

def testcase_2_3():
    res = test_with_n_clusters(data_4, GaussianMixture)
    return True

def testcase_2_4():
    res = test_with_n_clusters(data_7, GaussianMixture)
    return True

def testcase_3_1():
    res = test_without_giving_n_clusters(data_4, ClusteringAlgorithm)
    return res == 3

def test_all(err_report=False):
    testcases = [
        ["KMeans-1", testcase_1_1, 4],
        ["KMeans-2", testcase_1_2, 4],
        # ["KMeans-3", testcase_1_3, 4],
        # ["KMeans-4", testcase_1_4, 4],
        # ["KMeans-5", testcase_1_5, 4],
        ["GMM-1", testcase_2_1, 4],
        ["GMM-2", testcase_2_2, 4],
        # ["GMM-3", testcase_2_3, 4],
        ["GMM-4", testcase_2_4, 4],
        # ["GMM-5", testcase_2_5, 4],
        ["ClusteringAlgorithm", testcase_3_1, 4],
    ]
    sum_score = sum([case[2] for case in testcases])
    score = 0
    for case in testcases:
        try:
            res = case[2] if case[1]() else 0
        except Exception as e:
            if err_report:
                print("Error [{}] occurs in {}".format(str(e), case[0]))
            res = 0
        score += res
        print("+ {:14} {}/{}".format(case[0], res, case[2]))
    print("{:16} {}/{}".format("FINAL SCORE", score, sum_score))


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "--report":
        test_all(True)
    else:
        test_all()
