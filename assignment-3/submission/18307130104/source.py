import numpy as np
import matplotlib.pyplot as plt

class KMeans:
    def __init__(self, n_clusters):
        self.K = n_clusters
        self.Cent = []
        self.MAXDIS = 1e9

    def Dis(self, a, b):
        return np.mean((a - b) ** 2)
    
    def GetCluster(self, s):
        mdis = self.MAXDIS
        ntype = -1
        for i in range(len(self.Cent)):
            ndis = self.Dis(self.Cent[i], s)
            # print(self.Cent[i], s, ndis)
            if ndis < mdis:
                mdis = ndis
                ntype = i
        return ntype

    def fit(self, train_data, max_turn = 300):
        # 数据默认是随机的，所以直接选择前k个当作初始质心
        if self.K > len(train_data):
            print("error")
            return
        # 使用更好的方法选择初始质心
        self.Cent.append(train_data[0])
        for i in range(1, self.K):
            mxc = None
            mx = 0
            for d in range(len(train_data)):
                ti = self.GetCluster(train_data[d])
                di = self.Dis(self.Cent[ti], train_data[d])
                if  di > mx:
                    mx = di
                    mxc = d
            if mxc == None:
                print('error')
                return
            self.Cent.append(train_data[mxc])
        # 初始化每个点的类别    
        T = []
        D = []
        for i in range(len(train_data)):
            T.append(-1)
            D.append(i)
        D = np.array(D)
        changed = True
        turn = 0
        while changed == True or turn < max_turn:
            # 循环直到没有点变化类别
            changed = False
            for i in range(len(train_data)):
                ntype = self.GetCluster(train_data[i])
                if ntype != T[i]:
                    # 发生类别变化
                    T[i] = ntype
                    changed = True
            for i in range(self.K):
                # 创建 类别i 的筛选器
                filt = []
                have_item = False
                for j in range(len(train_data)):
                    if T[j] == i:
                        have_item = True
                        filt.append(True)
                    else:
                        filt.append(False)
                # 新质心
                if have_item:
                    self.Cent[i] = np.mean(train_data[filt], axis = 0)
            turn += 1
        ret = 0
        # 为 Elbow method 计算误差
        for i in range(len(train_data)):
            ret += float(self.Dis(self.Cent[T[i]], train_data[i]))
        return ret

    def predict(self, test_data):
        ret = []
        for i in range(len(test_data)):
            ret.append(self.GetCluster(test_data[i]))
        return np.array(ret)

class GaussianMixture:

    def __init__(self, n_clusters):
        self.K = n_clusters
        self.pi = np.random.randint(0, 100, size=n_clusters)
        self.pi = self.pi / np.sum(self.pi)
        self.mu = None
        self.sigma = None
        self.gama = None
    
    def normal(self, mu, sigma, x):
        # 保证1维数据
        mu = float(mu)
        sigma = float(sigma)
        x = float(x)
        return (1/((2*np.pi) ** 0.5 * sigma + 1e-10)) * np.exp(-(x-mu)**2/(2*sigma**2 + 1e-10))

    def multi_normal(self, mu, sigma, x):
        # 实际上只有二维的
        x = (x - mu).reshape(x.shape[0], 1)
        sigma = np.diag(sigma)
        return float((1 / ((np.linalg.det(sigma) ** 0.5) * ((2 * np.pi) ** (x.shape[0] / 2)))) * \
                 np.exp(-0.5 * np.matmul(np.matmul(x.T, np.linalg.inv(sigma)), x)))

    def fit(self, train_data):
        n = train_data.shape[0]
        if(len(train_data.shape) == 1 or train_data.shape[1] == 1):
            train_data = train_data.reshape(-1, 1)
            # 一维的情况
            kmeans_init = KMeans(self.K)
            # 利用 KMeans 初始化 mu sigma
            kmeans_init.fit(train_data, max_turn=10)
            tpe = kmeans_init.predict(train_data)
            mu = []
            sigma = []
            for i in range(self.K):
                mu.append([float(kmeans_init.Cent[i])])
                tmp = 0
                cnt = 0
                for j in range(len(train_data)):
                    if i == tpe[j]:
                        tmp += train_data[j] ** 2
                        cnt += 1
                sigma.append(tmp/cnt - kmeans_init.Cent[i] ** 2)
            self.mu = np.array(mu)
            self.sigma = np.array(sigma)
            for steps in range(50):
                # E 步骤
                tmp_q = []
                for i in range(n):
                    tmp_q.append([])
                    tot = 1e-10
                    for j in range(self.K):
                        multin = self.normal(self.mu[j], self.sigma[j], train_data[i])
                        qi = self.pi[j] * multin
                        tot += qi
                        tmp_q[i].append(qi)
                    for j in range(self.K):
                        tmp_q[i][j] /= tot
                self.gama = np.array(tmp_q)
                # M 步骤
                self.pi = np.sum(self.gama, axis = 0) / (np.sum(self.gama) + 1e-10)
                self.mu = np.zeros((self.K, 1))
                for k in range(self.K):
                    self.mu[k] = np.average(train_data, axis = 0, weights=self.gama[:, k])
                self.sigma = np.zeros((self.K, 1))
                for k in range(self.K):
                    self.sigma[k] = 1e-5 + np.average((train_data - self.mu[k]) ** 2, axis = 0, weights=self.gama[:, k])
        else:
            m = train_data.shape[1]
            self.mu = np.array([train_data[i] for i in range(self.K)])
            self.sigma = np.array([[10, 10] for i in range(self.K)])
            for steps in range(50):
                # E 步骤
                tmp_q = []
                for i in range(n):
                    tmp_q.append([])
                    tot = 1e-10
                    for j in range(self.K):
                        multin = self.multi_normal(self.mu[j], self.sigma[j], train_data[i])
                        qi = self.pi[j] * multin
                        tot += qi
                        tmp_q[i].append(qi)
                    for j in range(self.K):
                        tmp_q[i][j] /= tot
                self.gama = np.array(tmp_q)
                # M 步骤
                self.pi = np.sum(self.gama, axis = 0) / (np.sum(self.gama) + 1e-10)
                self.mu = np.zeros((self.K, 2))
                for k in range(self.K):
                    self.mu[k] = np.average(train_data, axis=0, weights=self.gama[:, k])
                self.sigma = np.zeros((self.K, 2))
                for k in range(self.K):
                    self.sigma[k] = 1e-3 + np.average((train_data - self.mu[k]) ** 2, axis=0, weights=self.gama[:, k])
    
    def predict(self, test_data):
        out = []
        if(len(test_data.shape) == 1 or test_data.shape[1] == 1):
            # 一维
            for i in range(test_data.shape[0]):
                mxp = 0
                cluster_id = -1
                for j in range(self.K):
                    pi = self.pi[j] * self.normal(self.mu[j], self.sigma[j], test_data[i])
                    if(pi > mxp):
                        mxp = pi
                        cluster_id = j
                out.append(cluster_id)
        else:
            # 二维
            for i in range(test_data.shape[0]):
                mxp = 0
                cluster_id = -1
                for j in range(self.K):
                    pi = self.pi[j] * self.multi_normal(self.mu[j], self.sigma[j], test_data[i])
                    if(pi > mxp):
                        mxp = pi
                        cluster_id = j
                out.append(cluster_id)
        return np.array(out)

class ClusteringAlgorithm:

    def __init__(self):
        self.cluster_num = 2
        self.model = None
    
    def fit(self, train_data, upper):
        cent = np.mean(train_data, axis = 0)
        tot = 0
        for p in train_data:
            tot += float(np.mean((p - cent) ** 2))
        retlist = []
        id = []
        for i in range(2, upper):
            kmeans = KMeans(i)
            ret = kmeans.fit(train_data)
            retlist.append(ret)
            id.append(i)
        mx = 0
        # 如果没有合适的 K，K 就是 2
        self.cluster_num = 2
        n = len(retlist)
        for i in range(1, n - 1):
            del1 = (retlist[0] - retlist[i]) / i
            del2 = (retlist[i] - retlist[n - 1]) / (n - 1 - i)
            delta = del1 - del2
            # 找到符合要求，并且插值最大的 K
            if delta > 0.3 * max(del1, del2) and delta > mx:
                mx = delta
                self.cluster_num = i + 2
        # l1 = plt.plot(id, retlist, 'b--')
        # plt.savefig(f'./img/elbow.png')
        # plt.show()
        self.model = KMeans(self.cluster_num)
        self.model.fit(train_data)
    
    def predict(self, test_data):
        return self.model.predict(test_data)
