# PRML-2021 Assignment3

姓名：夏海淞

学号：18307130090

## 简述

在本次实验中，我通过`NumPy`实现了`K-Means`和`GMM`两个用于聚类分析的模型，并在此基础上使用`BIC`和`Gap Statistic`方法实现了自动判断数据集中聚簇的数量并进行聚类的算法。为了验证模型和算法的正确性与效果，我在不同的二维数据集上进行了实验，对两者的运行时间和运行效果进行了比较。在实验中，我尝试对算法进行了一定优化，取得了较好的效果。

## 模型实现

### `K-Means`

`K-Means`模型在构建时随机选取聚簇数目个点，作为聚簇的中心点；在随后的每次迭代中包含以下步骤：

- 计算每个样本点至聚簇中心点的距离，将每个分配至距离最短的中心点对应的聚簇中；
- 计算聚簇中样本点的均值，将其作为新的聚簇中心点。

容易发现，`K-Means`模型通过样本点至聚簇中心点的距离预测其归属，聚簇中心点也是`K-Means`的参数。因此，聚簇中心点的初始选择对模型最终结果有着较大的影响。下图展示了当聚簇中心点选择不当时，模型容易收敛至局部最优解而非全局最优解的情形。

![](./img/myplot.png)

因此考虑选择一种策略，尽量避免在选择聚簇中心点时导致模型陷入局部最优解。在`K-Means++`算法中[^Arthur D 2006]，作者提出了一种随机选取聚簇中心点的方法：

- 随机选取一个样本点作为聚簇中心；
- 在随机选择下一个聚簇中心时，确保距离当前聚簇中心越远的点被选中的概率越大；
- 重复上述过程直至选出所有聚簇中心。

考虑到样本点数量较多时概率分配较为繁琐，我在构建模型时没有照搬该方法，而是根据“样本点之间离得越远越好”的思想，多次选择聚簇中心，最后采用平均距离最远的聚簇中心集作为结果返回：

```python
def choose_centers(train_data, k, rand_num):  # 生成初始聚簇中心点集
    rng = np.random.default_rng()
    centers = rng.choice(train_data, size=k, axis=0, replace=False)  # 从样本中无放回抽取样本点作为聚簇中心
    if k == 1: return centers
    dist = dis_avg(centers)  # 计算平均相互距离
    for i in range(rand_num - 1):  # 选择相互距离最大的聚簇中心集
        new_centers = rng.choice(train_data, size=k, axis=0, replace=False)
        new_dist = dis_avg(new_centers)
        if new_dist < dist: continue
        dist = new_dist
        centers = new_centers
    return centers
```

### `GMM`

高斯混合模型`GMM`通过`Expectation-Maximum`算法进行参数估计。

设$\bold x$为样本点，$z$表示样本所属的高斯分布，$\pi,\mu,\sigma$分别表示每个高斯分布的分配概率、均值和协方差，在每次迭代中：

- `Expectation`步：固定参数$\mu,\sigma$，计算后验分布$\gamma_{nk}=p(z^{(n)}=k|x^{(n)})$；

- `Maximum`步：固定$\gamma_{nk}$，更新参数$\pi,\mu,\sigma$：
  $$
  N_k=\sum_{n=1}^N\gamma_{nk}\\\\
  \pi_k=\frac{N_k}{N}\\\\
  \sigma_k=\frac1{N_k}\sum_{n=1}^N\gamma_{nk}(x^{(n)}-\mu_k)(x^{(n)}-\mu_k)^T\\\\
  \mu_k=\frac1{N_k}\sum_{n=1}^N\gamma_{nk}x^{(n)}
  $$

在将`GMM`和`K-Means`进行对比时，我发现两者有一些相似之处。`K-Means`算法每次迭代时首先将每个样本点分配至某一个聚簇中，而`GMM`中计算的后验分布也有类似的作用，即量化样本点属于每个高斯分布的概率，以概率的形式对每个样本点进行了“分配”。随后`K-Means`根据分配结果重新计算聚簇中心点，而`GMM`中则是根据后验分布重新计算每个高斯分布的参数。两者的思想类似，而具体实现的角度不同。前者是通过距离的形式进行划分，而后者是根据概率的形式进行划分。

在构建`GMM`的时候，我碰到了一些问题：

- 初始情况下，当样本距离每个高斯分布均较远时，联合概率均趋向于0，由于精度问题无法正确计算后验分布。注意到这种情况属于个别现象，因此我在处理时将其后验分布设置为均匀分布。
- 在运行`GMM-2`的测试数据时，发现由于计算得到的协方差矩阵为奇异矩阵，无法计算高斯分布。查询相关资料后，发现这种现象是由于样本数量相较于维数过小出现的现象，也可以视作模型过拟合的一种表现。解决方法是：①增大样本量②降低样本维数。考虑到降维过于繁琐，我简单地对样本进行复制后加入噪声，弥补了样本数量不足的问题。

## 基础实验

### 高斯分布

#### 数据集参数

$$
\mu_x=\begin{bmatrix}0&0\end{bmatrix},\mu_y=\begin{bmatrix}-20&20\end{bmatrix},\mu_z=\begin{bmatrix}30&30\end{bmatrix},\mu_x=\begin{bmatrix}-30&-30\end{bmatrix},\mu_x=\begin{bmatrix}30&-30\end{bmatrix}\\\\
\Sigma_x=\begin{bmatrix}70&0\\\\0&70\end{bmatrix},\Sigma_y=\Sigma_z=\Sigma_w=\Sigma_t=\begin{bmatrix}10&0\\\\0&10\end{bmatrix}\\\\
|x|=3500,|y|=|z|=|w|=|t|=750
$$

![](./img/basic.png)

使用`K-Means`和`GMM`进行聚类，画出各自的聚簇：

#### `K-Means`

![](./img/basic_kmeans.png)

其中黄色点代表聚簇中心。

#### `GMM`

![](./img/basic_gmm.png)

其中黄色点代表高斯分布均值。

---

容易发现，在高斯混合分布下，`K-Means`由于其采用距离作为分配聚簇的标准，在不同高斯分布的边界处产生了较大的误差；而采用高斯分布后验概率作为分配聚簇标准的`GMM`则表现较好。

### 其他分布

考虑到数据集采用多维高斯分布产生，采用高斯分布后验概率作为分配聚簇标准的`GMM`取得良好效果有“先射箭后画靶”的嫌疑。因此考虑采用其他分布生成随机数据集：

尝试通过均匀分布产生矩形数据集和三角形数据集：

![](./img/basic_matrix.png)

![](./img/basic_triangle.png)

使用`K-Means`和`GMM`进行聚类，画出各自的聚簇：

#### `K-Means`

![](./img/basic_matrix_kmeans.png)

![](./img/basic_triangle_kmeans.png)

其中黄色点代表聚簇中心。

#### `GMM`

![](./img/basic_matrix_gmm.png)

![](./img/basic_triangle_gmm.png)

其中黄色点代表高斯分布均值。

容易发现，即使是对于非高斯分布的情形，`GMM`在不规则分布中依然取得了更好的效果。因此推测`GMM`的聚类能力更加出色。

## 自动选择聚簇数量的实验

### `Gap Statistic`

助教在发布作业时提及了使用`Elbow Method`配合`K-Means`算法实现主动选择聚簇数量。因此我首先对其进行了研究。

聚类的主要目的是将类似的对象分配到同一聚簇中，即降低簇内平均距离。设当聚簇数量为$k$时簇内平均距离之和为$W_k$，考察$W_k$的曲线时发现$W_k$单调减，且在经过某个拐点后其斜率明显变得平缓，形如一个手肘，而拐点，也就是“肘部”被`Elbow Method`认为是聚簇数量的最佳位置。

然而，肘部的寻找并不容易。例如，当$W_k$曲线开始较平缓时，例如下图，很难量化“肘部”的判定标准。我查阅资料了解到，有一种被称为`Gap Statistic`的方法能够量化`Elbow Method`的判定标准[^Tibshirani R 2001]。

![](./img/w.png)

`Gap Statistic`的思想是将给定样本聚类后的簇内距离与空间内均匀采样的样本聚类后的期望簇内距离进行比较，两者之差被称为$Gap$。$Gap$衡量了样本的簇内距离与零假设下的偏差。`Gap Statistic`方法认为最佳的聚簇数量应当能够最大化$Gap$。

在实际运行中，`Gap Statistic`方法在样本覆盖的矩形中进行相同次数的均匀采样，计算聚类后的期望簇内距离：


$$
Gap_k=E(\log W_k^{(b)})-\log W_k\\\\
E(\log W_k^{(b)})=\frac{1}{B}\sum_{b=1}^B\log W_k^{(b)}
$$


`Gap Statistic`方法最后选择满足$Gap_k\ge Gap_{k+1}-s_{k+1}$的最小的$k$，其中$s_{k}=\sqrt{1+1/B}\cdot\text{std}(\log W_k^{(b)})$。

画出与上图对应的$E(\log W_k^{(b)})$，$\log W_k$和$Gap$的曲线：

![](./img/expect_w.png)

![](./img/gap.png)

容易发现，此时$k=3$为最佳的聚簇数量。

将使用`Gap Statistic`方法的`K-Means`模型用于三角数据集，得到下图的预测结果：

![](./img/auto_kmeans.png)

尽管`Gap Statistic`算法给出了错误的聚簇数量，然而从人类视角看，该聚类也有合理之处，且与正确聚簇数量相差不大。

### 簇内距离计算的优化

尽管`Gap Statistic`成功实现了量化“肘部”的效果，然而其运行时需要重复采样，聚类后还要计算簇内距离。当聚簇数量较小时，计算簇内距离的时间代价接近$O(n^2)$，其中$n$为样本容量，当样本点较多时运行缓慢。因此考虑对其进行优化。

首先，查阅资料得知，簇内距离的计算可以使用`numpy.einsum`函数改写来加速[^kmeans算法的性能改进,优化,距离,计算 (pythonf.cn)]。`numpy.einsum`是一个通过爱因斯坦求和约定(Einstein summation convention)对张量进行计算的函数，具备强大的灵活性和高效的性能。而我之前是通过`numpy.linalg.norm`计算簇内距离的。在将簇内距离的计算改用`numpy.einsum`表达后，该部分的运行时间降低为原先的10%。

然而，`numpy.einsum`的性能提升依然较为有限。考虑降低簇内距离的运算量。当簇内样本容量较大时，聚簇较为密集。此时可以随机抽取簇内样本的一部分进行计算，结果在进行修正后接近使用全部样本计算得到的值。实践中，对于样本容量大于250的聚簇，对其按照50%的采样率进行采样后计算簇内距离。这一优化在理论上将簇内距离的计算时间进一步降低至原先的25%。

### `Gap Statistic`与`BIC`的结合

在`GMM`中，模型有明确的优化目标：对数边际分布$\sum_{n=1}^N\log p(x^{(n)})$。因此查阅资料了解到，在通过`GMM`实现自动确定聚簇数量的算法时，可以采用`BIC`(Bayesian Information Criterion，贝叶斯信息准则)作为判断聚簇数量的依据。`BIC`引入了模型复杂程度的因素，用来惩罚过于复杂的模型，公式为$BIC=k\log n-2\log L$，其中$k,n,L$分别表示模型参数个数、样本数量和最大似然。

在引入`BIC`度量聚簇数量时，发现由于`GMM`的模型参数过少，`BIC`的惩罚力度不够，其曲线未按照设想的呈“U”型，而是呈现为“手肘”型，如下图所示。因此考虑将`BIC`与`Gap Statistic`结合，通过`Gap Statistic`方法找到`BIC`的拐点，从而确定最佳聚簇数量。

![](./img/BIC.png)

将使用`Gap Statistic`方法的`GMM`用于三角数据集，得到下图的预测结果：

![](./img/auto_gmm.png)

可以发现，`Gap Statistic`方法帮助`GMM`找到了正确的聚簇数量。

### `GMM`的性能优化

尝试对`GMM`中频繁调用的计算高斯分布的函数`get_norm_pdf`进行优化：

```python
def get_norm_pdf(x, mean, cov):
    n = cov.shape[0]
    denominator = (np.sqrt(2 * np.pi) ** n) * np.sqrt(np.linalg.det(cov))
    tmp = x - mean
    numerator = tmp @ np.linalg.inv(cov) @ tmp.T
    return np.exp(numerator / (-2)) / denominator
```

使用PyCharm对其进行性能分析后发现，大部分时间用于计算协方差矩阵的行列式和逆矩阵。考虑到单次迭代中协方差矩阵不会改变，因此可以在更新协方差矩阵时同时预计算其行列式和逆矩阵，避免重复计算：

```python
def get_norm_pdf(x, mean, cov_inv, denominator):  # 计算多维高斯分布，采用预计算策略，提高程序性能
    tmp = x - mean
    numerator = tmp @ cov_inv @ tmp.T
    return np.exp(numerator / (-2)) / denominator

# 提前计算协方差的逆，用于加快高斯分布概率密度的计算
self.cov_inv = [np.linalg.inv(single_cov) for single_cov in self.cov]

# 提前计算协方差的行列式，用于加快高斯分布概率密度的计算
cov_det = [np.abs(np.linalg.det(single_cov)) for single_cov in self.cov]

# 用于加快高斯分布概率密度的计算
self.denominator = (np.sqrt(2 * np.pi) ** self.dim) * np.sqrt(cov_det)
```

## 参考资料

[^Arthur D 2006]:[Arthur D, Vassilvitskii S. K-Means++: The advantages of careful seeding[R]. Stanford, 2006.](http://ilpubs.stanford.edu:8090/778/1/2006-13.pdf)


[^Tibshirani R 2001]:[Tibshirani R, Walther G, Hastie T. Estimating the number of clusters in a data set via the gap statistic[J]. Journal of the Royal Statistical Society: Series B (Statistical Methodology), 2001, 63(2): 411-423.](http://web.stanford.edu/~hastie/Papers/gap.pdf)


[^kmeans算法的性能改进,优化,距离,计算 (pythonf.cn)]:https://www.pythonf.cn/read/109964

