import matplotlib.pyplot as plt
import numpy as np

colors = ['red', 'sienna', 'tan', 'olivedrab', 'chartreuse', 'lightseagreen', 'skyblue', 'royalblue', 'lightcoral',
          'darkorange', 'darkviolet', 'fuchsia', 'black', 'grey']  # 绘制散点图所需的颜色集


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int) * i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def get_norm_pdf(x, mean, cov_inv, denominator):  # 计算多维高斯分布，其中cov_inv和denominator采用预计算策略，提高程序性能
    tmp = x - mean
    numerator = tmp @ cov_inv @ tmp.T
    return np.exp(numerator / (-2)) / denominator


def choose_centers(train_data, k, rand_num):  # 生成初始聚簇中心点集
    rng = np.random.default_rng()
    centers = rng.choice(train_data, size=k, axis=0, replace=False)  # 从样本中无放回抽取样本点作为聚簇中心
    if k == 1: return centers
    dist = dis_avg(centers)  # 计算平均相互距离
    for i in range(rand_num - 1):  # 选择相互距离最大的聚簇中心集
        new_centers = rng.choice(train_data, size=k, axis=0, replace=False)
        new_dist = dis_avg(new_centers)
        if new_dist < dist: continue
        dist = new_dist
        centers = new_centers
    return centers


def dis_avg(dots):  # 计算簇内平均距离
    n = dots.shape[0]
    ans = np.einsum('ij->', dis(dots, dots, False))  # 等价于矩阵求和
    # ans = sum(np.linalg.norm(dots[i] - dots[j], ord=2) for i in range(n) for j in range(i + 1, n))
    return ans / (2 * n)


def dis(x, y, square=True):  # 通过numpy.einsum计算点集x与点集y之间的两两距离 参考自https://www.pythonf.cn/read/109964
    xx = np.einsum('ij,ij->i', x, x)[:, np.newaxis]  # 计算x的平方的行和
    yy = np.einsum('ij,ij->i', y, y)[np.newaxis, :]  # 计算y的平方的行和
    dot = np.dot(x, y.T)  # 计算xy对应的行和
    res = np.abs(xx + yy - 2 * dot)  # (x-y)^2=x^2+y^2-2xy 取绝对值用来防止因精度问题出现负值
    return res if square else np.sqrt(res + 1e-12)


def my_scatter(np_data, color):  # 绘制聚簇散点图的函数
    if np_data.shape[0] == 0: return
    data = np.transpose(np_data).tolist()
    plt.scatter(data[0], data[1], c=colors[color] if color is not None else 'yellow')


def draw_clusters(clusters, centers):  # 绘制所有聚簇的散点图
    k = len(clusters)
    for i in range(k):
        my_scatter(clusters[i], i)
    my_scatter(centers, None)
    plt.show()


def draw_list(l):  # 绘制度量（如BIC、gap）曲线的函数
    n = len(l)
    x = list(range(1, n + 1))
    plt.plot(x, l)
    plt.show()


def my_inner(x, mu):  # 计算GMM中协方差矩阵的函数
    tmp = x - mu
    ans = np.einsum('i,j->ij', tmp, tmp)
    return ans


def get_clusters(dot2center, data, k):  # 为每个样本分配聚簇后，生成每个聚簇的点集
    n = data.shape[0]
    clusters = [[] for _ in range(k)]
    for i in range(n):
        clusters[dot2center[i]].append(data[i])
    for i in range(k):
        clusters[i] = np.array(clusters[i])
    return clusters


class KMeansCore:  # K-Means和K-Means Plus（即自动选择聚簇数量的K-Means）共同调用的部分
    def __init__(self, n_clusters, gap=False):  # gap=False时不计算Gap度量（K-Means调用），gap=True时计算Gap度量（K-Means Plus调用）
        self.k = n_clusters  # 聚簇数量
        self.is_gap = gap
        self.rand_num = 100  # 随机选取聚簇中心的循环次数
        self.n = None  # 样本容量
        self.centers = None  # 聚簇中心
    
    def kmeans_iter(self, train_data):  # K-Means 单次迭代
        dot2center = np.argmin(dis(train_data, self.centers), axis=1)  # 将样本点分配至距离最近的聚簇中心
        clusters = get_clusters(dot2center, train_data, self.k)  # 生成每个聚簇的点集
        return clusters, np.array(dot2center, dtype=int)
    
    def fit(self, train_data, sample_rate=None):
        self.n = train_data.shape[0]
        self.centers = choose_centers(train_data, self.k, self.rand_num)  # 随机选取聚簇中心
        clusters, dot2center = self.kmeans_iter(train_data)
        while True:
            self.centers = np.array([np.sum(cluster, axis=0) / cluster.shape[0] for cluster in clusters])  # 计算新的聚簇中心
            clusters, new_dot2center = self.kmeans_iter(train_data)  # 将样本点分配至不同聚簇
            if (dot2center == new_dot2center).all(): break  # 当样本聚簇归属迭代间不发生变化时结束迭代
            dot2center = new_dot2center
        if not self.is_gap: return None
        if sample_rate is None:  # 不设置采样率时直接返回簇内距离之和
            return sum([dis_avg(cluster) for cluster in clusters])
        rng = np.random.default_rng()  # 设置采样器
        ans = 0
        for i in range(self.k):
            cluster_size = int(clusters[i].shape[0] * sample_rate)
            if cluster_size >= 250:  # 聚簇大小小于500时不采样
                clusters[i] = rng.choice(clusters[i], size=cluster_size, axis=0, replace=False)
                ans += dis_avg(clusters[i]) / sample_rate  # 数值修正
            else:
                ans += dis_avg(clusters[i])
        return ans
    
    def predict(self, test_data):
        assert self.centers is not None
        return np.argmin(dis(test_data, self.centers), axis=1)  # 将测试点分配至距离最近的聚簇中心


class KMeans:  # 直接调用KMeansCore
    
    def __init__(self, n_clusters):
        self.core = KMeansCore(n_clusters)
    
    def fit(self, train_data):
        self.core.fit(train_data)
    
    def predict(self, test_data):
        dot2center = self.core.predict(test_data)
        # clusters = get_clusters(dot2center, test_data, self.core.k)
        # draw_clusters(clusters, self.core.centers)
        return dot2center


class KMeansPlus:  # 自动确定聚簇数量的K-Means模型
    
    def __init__(self):
        self.core = None  # 调用KMeansCore对象
        self.k = None  # 聚簇数量
        self.n = None  # 样本容量
        self.sample_rate = None  # 采样率
        self.sample_num = 15  # 采样次数
    
    def monte_carlo(self, data_min, data_max):  # 计算零假设下的期望簇内距离
        d = data_min.shape[0]
        W_list = []
        for _ in range(self.sample_num):
            print(f'Monte Carlo {_}')
            sample_data = np.concatenate([np.random.uniform(data_min[i], data_max[i], (self.n, 1)) for i in range(d)],
                                         axis=1)  # 生成零假设下的样本数据
            W_list.append(self.core.fit(sample_data, self.sample_rate))
        W = np.log(W_list)
        return W.mean(), W.std() * np.sqrt(1 + 1 / self.sample_num)  # 返回簇内距离期望和修正后的簇内距离标准差
    
    def fit(self, train_data):
        self.n = train_data.shape[0]
        self.sample_rate = None if self.n <= 5000 else 0.5  # 样本容量大于5000时进行采样
        data_min = train_data.min(axis=0, initial=np.inf)  # 使采样空间精确覆盖样本空间
        data_max = train_data.max(axis=0, initial=-np.inf)
        max_k = min(max(10, int(np.sqrt(self.n / 2.0))), self.n)  # 确定遍历的聚簇数量的最大值
        # max_k = 8
        print(f'max_k={max_k}')
        pre_gap = None
        gap_list = []
        for self.k in range(2, max_k + 1):
            print(f'k={self.k}')
            self.core = KMeansCore(self.k, gap=True)
            W = np.log(self.core.fit(train_data, self.sample_rate))  # 计算簇内距离
            expec_W, s = self.monte_carlo(data_min, data_max)  # 计算零假设下的期望簇内距离和修正后的簇内距离标准差
            if pre_gap is not None and expec_W - W - s < pre_gap:  # Gap Statistic方法的判定条件
                self.k -= 1
                break
            pre_gap = expec_W - W
            gap_list.append(pre_gap)
        print(f'final k={self.k}')
        self.core = KMeansCore(self.k)  # 使用确定的聚簇数量构建模型
        self.core.fit(train_data)
        # draw_list(gap_list)
    
    def predict(self, test_data):
        dot2center = self.core.predict(test_data)
        # clusters = get_clusters(dot2center, test_data, self.k)
        # draw_clusters(clusters, self.core.centers)
        return dot2center


class GaussianMixture:  # 基础的GMM，被GaussianMixturePlus调用
    
    def __init__(self, n_clusters):
        self.k = n_clusters  # 聚类数量
        self.pi = np.full(self.k, 1.0 / self.k)  # 设定混合高斯分布的概率分布，初始条件下为均匀分布
        self.rand_num = 100  # 随机选取聚簇中心的循环次数
        self.mean = None  # 混合高斯分布的均值
        self.cov = None  # 混合高斯分布的协方差矩阵
        self.cov_inv = None  # 协方差的逆矩阵，提前计算后用于加快高斯分布概率密度的计算
        self.cov_det = None  # 协方差的行列式，提前计算后用于加快高斯分布概率密度的计算
        self.denominator = None  # 高斯分布概率密度中的分母，提前计算后用于加快高斯分布概率密度的计算
        self.n = None  # 样本容量
        self.dim = None  # 样本维度
    
    def compute_log_like(self, norm_p):  # 计算对数边际分布之和
        return np.einsum('i->', np.log(np.einsum('j,ij->i', self.pi, norm_p)))
    
    def compute_norm_pdf(self, train_data):  # 计算样本与多个高斯分布的概率分布
        return np.array(
            [[get_norm_pdf(x, self.mean[i], self.cov_inv[i], self.denominator[i]) for i in range(self.k)]
             for x in train_data])
    
    def compute_post_prob(self, norm_pdf):  # 根据概率分布计算后验分布
        uni_prob = np.einsum('ij,j->ij', norm_pdf, self.pi)  # 根据高斯分布和高斯分布的概率分布计算联合分布
        uni_prob_sum = np.reciprocal(np.einsum('ij->i', uni_prob))  # 计算联合分布之和
        post_prob = np.einsum('ij,i->ij', uni_prob, uni_prob_sum)  # 通过联合分布计算后验分布
        post_prob[np.isnan(post_prob) | np.isinf(post_prob)] = 1.0 / self.k  # 处理因精度问题导致的非法值
        return post_prob
    
    def GMM_iter(self, train_data):  # GMM的单次迭代
        norm_pdf = self.compute_norm_pdf(train_data)  # 计算高斯分布
        pre_log_like = self.compute_log_like(norm_pdf)  # 通过高斯分布计算对数边际分布
        post_prob = self.compute_post_prob(norm_pdf)  # 计算后验分布
        N = np.einsum('ij->j', post_prob)
        N_recip = np.reciprocal(N)
        self.pi = N / self.n  # 更新混合高斯分布的概率分布
        x_minus_mean = np.array([[my_inner(x, mu) for mu in self.mean] for x in train_data])
        self.cov = np.einsum('ijk,i->ijk', np.einsum('ij,ijkl->jkl', post_prob, x_minus_mean),
                             N_recip)  # 通过后验分布更新混合高斯分布的协方差
        self.cov_inv = [np.linalg.inv(single_cov) for single_cov in self.cov]  # 提前计算协方差的逆，用于加快高斯分布概率密度的计算
        cov_det = [np.abs(np.linalg.det(single_cov)) for single_cov in self.cov]  # 提前计算协方差的行列式，用于加快高斯分布概率密度的计算
        self.denominator = (np.sqrt(2 * np.pi) ** self.dim) * np.sqrt(cov_det)  # 用于加快高斯分布概率密度的计算
        self.mean = np.einsum('ij,i->ij', np.einsum('ij,ik->jk', post_prob, train_data), N_recip)  # 通过后验分布更新混合高斯分布的均值
        norm_pdf = self.compute_norm_pdf(train_data)
        log_like = self.compute_log_like(norm_pdf)  # 计算新的对数边际分布
        return log_like - pre_log_like
    
    def fit(self, train_data):
        self.n = train_data.shape[0]
        self.dim = train_data.shape[1]
        if self.n < 8 * self.dim:  # 样本容量相对较小时采用过采样策略，防止产生奇异协方差矩阵
            train_data = np.concatenate([
                train_data,
                train_data + np.random.rand(self.n, self.dim),
                train_data + np.random.rand(self.n, self.dim),
                train_data + np.random.rand(self.n, self.dim),
                train_data + np.random.rand(self.n, self.dim),
                train_data + np.random.rand(self.n, self.dim),
                train_data + np.random.rand(self.n, self.dim),
                train_data + np.random.rand(self.n, self.dim)], axis=0)
        self.n = train_data.shape[0]
        self.mean = choose_centers(train_data, self.k, self.rand_num)  # 随机选取样本点，作为混合高斯分布的均值
        self.cov = np.concatenate([np.eye(self.dim)[np.newaxis, :] for _ in range(self.k)], axis=0)  # 选取单位阵作为混合高斯分布的协方差
        self.cov_inv = [np.linalg.inv(single_cov) for single_cov in self.cov]  # 提前计算协方差的逆，用于加快高斯分布概率密度的计算
        cov_det = [np.abs(np.linalg.det(single_cov)) for single_cov in self.cov]  # 提前计算协方差的行列式，用于加快高斯分布概率密度的计算
        self.denominator = (np.sqrt(2 * np.pi) ** self.dim) * np.sqrt(cov_det)  # 用于加快高斯分布概率密度的计算
        gap = np.inf
        cnt = 0
        while gap > 0.1:  # 判断对数似然收敛的条件
            gap = self.GMM_iter(train_data)
            cnt += 1
            # print(f'\tcnt={cnt} gap={gap}')
            if cnt == 100: break  # 最大迭代次数
        log_like = self.compute_log_like(self.compute_norm_pdf(train_data))
        BIC = self.k * np.log(self.n) - 2 * log_like  # 计算BIC
        return BIC
    
    def predict(self, test_data):
        post_prob = self.compute_post_prob(self.compute_norm_pdf(test_data))  # 计算后验分布，将测试点分配给后验概率最大的高斯分布
        dot2center = np.argmax(post_prob, axis=1)
        # clusters = get_clusters(dot2center, test_data, self.k)
        # draw_clusters(clusters, self.mean)
        return dot2center


class GaussianMixturePlus:  # 自动确定聚簇数量的GMM模型
    def __init__(self):
        self.k = None  # 聚类数量
        self.n = None  # 样本容量
        self.core = None  # 调用GaussianMixture对象
        self.sample_num = 10  # 采样次数
    
    def monte_carlo(self, data_min, data_max):  # 计算零假设下的期望BIC
        d = data_min.shape[0]
        BIC_list = []
        for _ in range(self.sample_num):
            print(f'Monte Carlo {_}')
            sample_data = np.concatenate([np.random.uniform(data_min[i], data_max[i], (self.n, 1)) for i in range(d)],
                                         axis=1)  # 生成零假设下的样本数据
            BIC_list.append(self.core.fit(sample_data))
        BIC = np.array(BIC_list)
        return BIC.mean(), BIC.std() * np.sqrt(1 + 1 / self.sample_num)  # 返回BIC期望和修正后的BIC标准差
    
    def fit(self, train_data):
        self.n = train_data.shape[0]
        data_min = train_data.min(axis=0, initial=np.inf)  # 使采样空间精确覆盖样本空间
        data_max = train_data.max(axis=0, initial=-np.inf)
        max_k = min(max(10, int(np.sqrt(self.n / 2.0))), self.n)
        # max_k = 8
        print(f'max_k={max_k}')
        BIC_list, gap_list = [], []
        pre_gap = None
        for self.k in range(2, max_k + 1):
            print(f'k={self.k}')
            self.core = GaussianMixture(self.k)
            BIC = self.core.fit(train_data)  # 计算BIC
            BIC_list.append(BIC)
            expec_BIC, s = self.monte_carlo(data_min, data_max)  # 计算零假设下的期望BIC和修正后的BIC标准差
            if pre_gap is not None and expec_BIC - BIC - s < pre_gap:  # Gap Statistic方法的判定条件
                self.k -= 1
                break
            pre_gap = expec_BIC - BIC
            gap_list.append(pre_gap)
            # if len(BIC_list) < 3: continue
            # x, y, z = BIC_list[-3:]
            # if abs(y - z) < 0.2 * abs(x - y):
            #     self.k -= 1
            #     self.core = GaussianMixture(self.k)
            #     self.core.fit(train_data)
            #     break
        print(f'final k={self.k}')
        self.core = GaussianMixture(self.k)  # 使用确定的聚簇数量构建模型
        self.core.fit(train_data)
        # draw_list(gap_list)
    
    def predict(self, test_data):
        dot2center = self.core.predict(test_data)
        # clusters = get_clusters(dot2center, test_data, self.k)
        # draw_clusters(clusters, self.core.mean)
        return dot2center


def generate_triangle(dot1, dot2, dot3, num):  # 生成三角状数据集的函数
    dot1 = np.array(dot1)
    dot2 = np.array(dot2)
    dot3 = np.array(dot3)
    l = np.array([min(dot1[0], dot2[0], dot3[0]), min(dot1[1], dot2[1], dot3[1])])  # 确定采样空间
    r = np.array([max(dot1[0], dot2[0], dot3[0]), max(dot1[1], dot2[1], dot3[1])])
    cnt = 0
    ans = []
    while True:
        dot = np.random.uniform(l, r, size=2)  # 随机生成样本点
        
        # 使用叉积判断生成的点是否位于三角形中间
        x, y, z = np.cross(dot - dot1, dot - dot2), np.cross(dot - dot2, dot - dot3), np.cross(dot - dot3, dot - dot1)
        if (x < 0 and y < 0 and z < 0) or (x > 0 and y > 0 and z > 0):
            cnt += 1
            ans.append(dot + np.random.rand(2) * 0.75)
        if cnt == num: break
    return np.array(ans)


def generate_data1():  # 数据集1
    mean = (0, 0)
    cov = np.array([[70, 0], [0, 70]])
    x = np.random.multivariate_normal(mean, cov, (3500,))
    
    mean = (-20, 20)
    cov = np.array([[10, 0], [0, 10]])
    y = np.random.multivariate_normal(mean, cov, (750,))
    
    mean = (30, 30)
    cov = np.array([[10, 0], [0, 10]])
    z = np.random.multivariate_normal(mean, cov, (750,))
    
    mean = (-30, -30)
    cov = np.array([[10, 0], [0, 10]])
    w = np.random.multivariate_normal(mean, cov, (750,))
    
    mean = (30, -30)
    cov = np.array([[10, 0], [0, 10]])
    t = np.random.multivariate_normal(mean, cov, (750,))
    data, _ = shuffle(x, y, z, w, t)
    return data


def generate_data2():  # 数据集2
    mean = (-5, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))
    
    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))
    
    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))
    data, _ = shuffle(x, y, z)
    return data


def generate_data3():  # 数据集3
    x = generate_triangle([0, -1], [9, 2], [6, 3.5], 500)
    y = generate_triangle([1, 8], [4, 7], [5, 12], 500)
    z = generate_triangle([5, 5], [8, 12], [12, 6], 500)
    w = generate_triangle([8.5, 7], [12, 1], [14, 7], 500)
    
    data, _ = shuffle(x, y, z, w)
    return data


if __name__ == '__main__':
    data = generate_data3()
    
    model = GaussianMixturePlus()
    model.fit(data)
    model.predict(data)
