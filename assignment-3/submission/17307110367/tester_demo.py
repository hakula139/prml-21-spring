import numpy as np
import sys
import matplotlib.pyplot as plt
from source import KMeans, GaussianMixture,ClusteringAlgorithm


def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label


def data_1():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (100,))

    data, _ = shuffle(x, y, z)
    #displayer([x, y, z], "data")
    return (data, data), 3


def data_2():
    train_data = np.array([
        [23, 12, 173, 2134],
        [99, -12, -126, -31],
        [55, -145, -123, -342],
    ])
    return (train_data, train_data), 2


def data_3():
    train_data = np.array([
        [23],
        [-2999],
        [-2955],
    ])
    return (train_data, train_data), 2

def displayer(data,name):
    datas = [[], [], [], [], []]
    for kind in range(5):
        for i in range(len(data[kind])):
            datas[kind].append(data[kind][i])

    for each in datas:
        each = np.array(each)
        if each.size > 0:
            plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

def display(data, label, name):
    datas = [[], [], [], [], []]
    for i in range(len(data)):
        datas[label[i]].append(data[i])

    for each in datas:
        each = np.array(each)
        if each.size > 0:
            #plt.scatter(each[:, 0], np.zeros(each.size))
            plt.scatter(each[:, 0], each[:, 1])
    plt.savefig(f'img/{name}')
    plt.show()

#自己制作的二维数据集
def data_6():
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (80,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    y = np.random.multivariate_normal(mean, cov, (100,))

    mean = (-10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (100,))

    mean = (10, -22)
    cov = np.array([[10, 5], [5, 10]])
    u = np.random.multivariate_normal(mean, cov, (100,))

    mean = (-10, -22)
    cov = np.array([[10, 5], [5, 10]])
    v = np.random.multivariate_normal(mean, cov, (100,))


    data, _ = shuffle(x, y, z, u, v)
    #displayer([x, y, z, u, v], "data")
    return (data, data), 5

def test_without_n_clusters(data_fuction, algorithm_class):
    (train_data, test_data), n_clusters = data_fuction()
    model = algorithm_class()
    model.fit(train_data, 10)
    res = model.predict(test_data)
    return model.cluster_num


def test_with_n_clusters(data_fuction, algorithm_class):
    (train_data, test_data), n_clusters = data_fuction()
    model = algorithm_class(n_clusters)
    model.fit(train_data)
    res = model.predict(test_data)
    #display(test_data, res, "res")
    assert len(
        res.shape) == 1 and res.shape[0] == test_data.shape[0], "shape of result is wrong"
    return res


def testcase_1_1():
    test_with_n_clusters(data_1, KMeans)
    return True


def testcase_1_2():
    res = test_with_n_clusters(data_2, KMeans)
    return res[0] != res[1] and res[1] == res[2]


def testcase_2_1():
    test_with_n_clusters(data_1, GaussianMixture)
    return True


def testcase_2_2():
    res = test_with_n_clusters(data_3, GaussianMixture)
    return res[0] != res[1] and res[1] == res[2]

def testcase_1_6():
    test_with_n_clusters(data_6, KMeans)
    return True

def testcase_2_6():
    test_with_n_clusters(data_6, GaussianMixture)
    return True

def testcase_3_1():
    res = test_without_n_clusters(data_6, ClusteringAlgorithm)
    print(res)
    return res == 5

def test_all(err_report=False):
    testcases = [
        ["KMeans-1", testcase_1_1, 4],
        ["KMeans-2", testcase_1_2, 4],

        # ["KMeans-3", testcase_1_3, 4],
        # ["KMeans-4", testcase_1_4, 4],
        # ["KMeans-5", testcase_1_5, 4],
        #["KMeans-6", testcase_1_6, 4],

        ["GMM-1", testcase_2_1, 4],
        ["GMM-2", testcase_2_2, 4],

        # ["GMM-3", testcase_2_3, 4],
        # ["GMM-4", testcase_2_4, 4],
        # ["GMM-5", testcase_2_5, 4],
        #["GMM-6", testcase_2_6, 4],
        #["ClusteringAlgorithm", testcase_3_1, 4]
    ]
    sum_score = sum([case[2] for case in testcases])
    score = 0
    for case in testcases:
        try:
            res = case[2] if case[1]() else 0
        except Exception as e:
            if err_report:
                print("Error [{}] occurs in {}".format(str(e), case[0]))
            res = 0
        score += res
        print("+ {:14} {}/{}".format(case[0], res, case[2]))
    print("{:16} {}/{}".format("FINAL SCORE", score, sum_score))


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "--report":
        test_all(True)
    else:
        test_all()
