import numpy as np
import matplotlib.pyplot as plt
class KMeans:

    def __init__(self, n_clusters):
        self.num_clusters = n_clusters
        self.centers = None
        self.dists = None
        self.labels = None
        self.max_iter = 1000
        self.stop_var = 1e-3
        self.variance = 1e-2

    # 无监督分类的实现
    def fit(self, train_data):
        self.init_centers(train_data)
        for _iter in range(self.max_iter):
            self.update_dists(train_data)
            self.update_centers(train_data)
            if self.variance < self.stop_var:
                print('Current_iter:', iter)
                break

    # 预测样本所属类别
    def predict(self, test_data):
        res = []
        for i, sample in enumerate(test_data):
            dist = self.l2_distance(sample)
            res.append(np.argmin(dist))
        return np.array(res)

    #初始化中心点
    def init_centers(self, train_data):
        init_row = np.random.choice(range(train_data.shape[0]), self.num_clusters, replace=False)
        self.centers = train_data[init_row]

    #更新距离
    def update_dists(self, train_data):
        labels = np.empty(train_data.shape[0])
        dists = np.empty([0, self.num_clusters])
        for i, sample in enumerate(train_data):
            dist = self.l2_distance(sample)
            labels[i] = np.argmin(dist)
            dists = np.vstack([dists, dist])
        if self.dists is not None:
            self.variance = np.sum(np.abs(self.dists - dists))
        self.dists = dists
        self.labels = labels

    #更新中心点
    def update_centers(self, samples):
        centers = np.empty([0, samples.shape[1]])
        for i in range(self.num_clusters):
            idx = (self.labels == i)
            center_samples = samples[idx]
            if len(center_samples) > 0:
                center = np.mean(center_samples, axis=0)
            else:
                center = self.centers[i]
            centers = np.vstack((centers, center[np.newaxis, :]))
        self.centers = centers

    #l2距离的计算
    def l2_distance(self, sample):
        return np.sum(np.square(self.centers - sample), axis=1)

class GaussianMixture:

    def __init__(self, n_clusters):
        self.num_clusters = n_clusters
        self.max_iter = 100
        self.num = None
        self.dim = None
        self.X = None
        self.Q = None
        self.weight = None
        self.covar = None
        self.mu = None
        self.labels = None

    def fit(self, train_data):
        self._initialize_params(train_data)
        while self.max_iter > 0:
            # 初始化变量
            # e-step
            self.e_step()
            # m-step
            self.m_step()
            self.max_iter -= 1
        self.labels = np.argmax(self.Q, axis=1)

    def predict(self, test_data):
        out = []
        for i in range(self.num):
            mxp = 0
            res = -1
            for k in range(self.num_clusters):
                temp = self.weight[k]*self.multi_norm(test_data[i, :], self.mu[k, :], self.covar[k, :, :])
                if temp > mxp:
                    mxp = temp
                    res = k
            out.append(res)
        out = np.array(out)
        return out

    def _initialize_params(self, X):
        self.X = X  # 分类的数据集
        self.num = X.shape[0]  # 样本数目
        self.dim = X.shape[1]  # 特征维度
        self.Q = np.zeros((self.num, self.num_clusters))  # 初始化各高斯分布对观测数据的响应度矩阵
        self.weight = [1 / self.num_clusters] * self.num_clusters  # 初始化各高斯分布的权重为聚类数目分之一
        self.mu = np.random.uniform(0, 1, (self.num_clusters, self.dim)) * np.max(X, axis=0)  # 随机产生均值向量
        self.covar = np.array([np.identity(self.dim) for _ in range(self.num_clusters)])  # 随机产生协方差矩阵

    #更新分模型对数据的响应度矩阵Q。e步
    def e_step(self):
        for i in range(self.num):
            q_i = []
            for k in range(self.num_clusters):
                postProb = self.multi_norm(self.X[i, :], self.mu[k, :], self.covar[k, :, :])
                q_i.append(self.weight[k] * postProb + 1e-32)
            self.Q[i, :] = np.array(q_i) / np.sum(q_i)

    #返回多维高斯分布的结果
    def multi_norm(self, x, mu, sigma):
        det = np.linalg.det(sigma)
        inv = np.matrix(np.linalg.inv(sigma))
        x_mu = np.matrix(x - mu).T
        const = 1 / (((2 * np.pi) ** (len(x) / 2)) * (det ** (1 / 2)))
        exp = -0.5 * x_mu.T * inv * x_mu
        return float(const * np.exp(exp))

    # m步，更新参数
    def m_step(self):
        # update weight 更新权值矩阵
        self.weight = np.mean(self.Q, axis=0)

        # update mu 更新均值向量
        temp = []
        for k in range(self.num_clusters):
            up = np.zeros(self.dim)
            for j in range(self.num):
                up += self.Q[j, k] * np.array(self.X[j, :])
            down = np.sum(self.Q[:, k])
            temp.append(up / down)
        self.mu = np.array(temp)

        # update covar 更新协方差矩阵
        for k in range(self.num_clusters):
            up = np.zeros((self.dim, self.dim))
            for j in range(self.num):
                x_mu = np.matrix(self.X[j, :] - self.mu[k, :])
                # print(x_mu.T*x_mu)
                up += self.Q[j, k] * (x_mu.T * x_mu)
            # print(up)
            down = np.sum(self.Q[:, k])
            var = np.array(up / down)
            self.covar[k, :, :] = var

class ClusteringAlgorithm:

    def __init__(self):
        self.cluster_num = 2
        self.model = None

    def fit(self, train_data, upper):
        sum = np.zeros(upper-2)
        for i in range(2, upper):
            kmeans = KMeans(i)
            kmeans.fit(train_data)
            m = kmeans.labels
            c = kmeans.centers
            for j in range(len(train_data)):
                c1 = c[int(m[j])]
                x1 = train_data[j]
                sum[i-2] += np.sum(np.square(c1-x1))
        c = plt.plot(np.arange(2, upper), sum)
        plt.savefig(f'./img/elbow.png')
        plt.show()
        n = len(sum)
        mx = 0
        for i in range(1, n - 1):
            del1 = (sum[0] - sum[i]) / i
            del2 = (sum[i] - sum[n - 1]) / (n - 1 - i)
            delta = del1 - del2
            # 找到符合要求，并且插值最大的 K
            if delta > 0.3 * max(del1, del2) and delta > mx:
                mx = delta
                self.cluster_num = i + 2
        self.model = KMeans(self.cluster_num)
        self.model.fit(train_data)

    def predict(self, test_data):
        return self.model.predict(test_data)
