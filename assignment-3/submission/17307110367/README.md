# PRML-2021 Assignment3

姓名：张艳琳

学号：17307110367

## 问题概述

本次实验中需要通过`NumPy`实现`K-Means`和`GMM`两个用于聚类分析的模型，并在此基础上使用`elbow method`的方法实现自动判断数据集中聚簇的数量并进行聚类的算法。
## 模型实现

### `K-Means`

`K-Means`模型在构建时随机选取聚类中心，在随后的每次迭代中包含以下步骤：

- 计算每个样本点至聚类中心点的距离，将每个分配至距离最短的中心点对应的类中；
- 计算每个类中样本点的均值，将其作为新的聚类中心。

在`K-Means`的初始聚类中心的选择上，通过随机选取样本点来作为初始的`K-Means`模型的聚类中心：

```python
    def init_centers(self, train_data):
        init_row = np.random.choice(range(train_data.shape[0]), self.num_clusters, replace=False)
        self.centers = train_data[init_row]
```

### `GMM`

高斯混合模型`GMM`通过`Expectation-Maximum`算法进行参数估计。

设$ x$为样本点，$z$表示样本所属的高斯分布，$\pi,\mu,\sigma$分别表示每个高斯分布的分配概率、均值和协方差，在每次迭代中：

- `Expectation`步：固定参数$\mu,\sigma$，计算后验分布$\gamma_{nk}=p(z^{(n)}=k|x^{(n)})$；

- `Maximum`步：固定$\gamma_{nk}$，更新参数$\pi,\mu,\sigma$：
  $$
  N_k=\sum_{n=1}^N\gamma_{nk}\\\\
  \pi_k=\frac{N_k}{N}\\\\
  \sigma_k=\frac1{N_k}\sum_{n=1}^N\gamma_{nk}(x^{(n)}-\mu_k)(x^{(n)}-\mu_k)^T\\\\
  \mu_k=\frac1{N_k}\sum_{n=1}^N\gamma_{nk}x^{(n)}
  $$

## 基础实验

### 对给定的data_1进行可视化
#### 数据集参数

$$
\mu_x=\begin{bmatrix}1&2\end{bmatrix},\mu_y=\begin{bmatrix}16&-5\end{bmatrix},\mu_z=\begin{bmatrix}10&22\end{bmatrix}\\\\
\Sigma_x=\begin{bmatrix}73&0\\\\0&22\end{bmatrix},\Sigma_y=\begin{bmatrix}21.2&0\\\\0&32.1\end{bmatrix},\Sigma_z=\begin{bmatrix}10&5\\\\5&10\end{bmatrix}\\\\
x样本点数量为800,
y的样本点数量为200,z的样本点数量为100.
$$

生成的数据如图所示

![](./img/data1_1_Kmeans.png)

使用`K-Means`和`GMM`进行聚类，画出各自的聚簇：

#### `K-Means`

![](./img/res1_1_Kmeans.png)



#### `GMM`

![](./img/res1_1_GMM.png)


可以看到GMM生成的结果要比K-Means生成的结果好一些。


### 自己生成的二维高斯分布

#### 数据集参数

$$
\mu_x=\begin{bmatrix}1&2\end{bmatrix},\mu_y=\begin{bmatrix}10&22\end{bmatrix},\mu_z=\begin{bmatrix}-10&22\end{bmatrix},\mu_x=\begin{bmatrix}10&-22\end{bmatrix},\mu_x=\begin{bmatrix}-10&-22\end{bmatrix}\\\\
\Sigma_x=\begin{bmatrix}73&0\\\\0&22\end{bmatrix},\Sigma_y=\Sigma_z=\Sigma_w=\Sigma_t=\begin{bmatrix}10&5\\\\5&10\end{bmatrix}\\\\
x样本点数量为8000,
y,z,w,t的样本点数量为1000
$$

生成的数据如图所示

![](./img/data6.png)

使用`K-Means`和`GMM`进行聚类，画出各自的聚簇：

#### `K-Means`

![](./img/res_6_K.png)



#### `GMM`

![](./img/res6_G.png)

发现GMM由于初始分布是随机的而导致了分类结果十分奇怪。再次进行实验后的结果就正常许多

![](./img/res6_G_2.png)

---


## 自动选择聚簇数量的实验

### `Elbow Method`
我们知道k-means是以最小化样本与聚类中心的平方误差作为目标函数，将每个聚类中心与类内样本点的平方距离误差和称为畸变程度(distortions)。那么，对于一个类，它的畸变程度越低，代表类内成员越紧密，畸变程度越高，代表类内结构越松散。

畸变程度会随着类别的增加而降低，但对于有一定区分度的数据，在达到某个临界点时畸变程度会得到极大改善，之后缓慢下降，这个临界点就可以考虑为聚类性能较好的点。其图像像一个胳膊肘，故名为elbow method。

在实际的代码中可以对`Elbow Method`的结果进行图像绘制。在我的实验中设置的类的范围是2~10。(即代码中的upper为10)

将`Elbow Method`配合`K-Means`的代码如下：
```python
        def fit(self, train_data, upper):
        sum = np.zeros(upper-2)
        for i in range(2, upper):
            kmeans = KMeans(i)
            kmeans.fit(train_data)
            m = kmeans.labels
            c = kmeans.centers
            for j in range(len(train_data)):
                c1 = c[int(m[j])]
                x1 = train_data[j]
                sum[i-2] += np.sum(np.square(c1-x1))
        c = plt.plot(np.arange(2, upper), sum)
        plt.savefig(f'./img/elbow.png')
        plt.show()
        n = len(sum)
        mx = 0
        for i in range(1, n - 1):
            del1 = (sum[0] - sum[i]) / i
            del2 = (sum[i] - sum[n - 1]) / (n - 1 - i)
            delta = del1 - del2
            if delta > 0.3 * max(del1, del2) and delta > mx:
                mx = delta
                self.cluster_num = i + 2
        self.model = KMeans(self.cluster_num)
        self.model.fit(train_data)
```
`Elbow Method`生成的图像结果如下

![](./img/elbow_final.png)

很容易看到，在K=5处有个明显的拐点。因此根据`Elbow Method`的方法，K=5应当被自动选择的聚簇数量，事实也是如此。



