# 实验报告：ASS3-KMeans & GMM

18307130341 黄韵澄

[toc]

### 1.实验概述

​	本实验分别实现了KMeans和GMM模型，在n_clusters数目给定的条件下，对输入的点集实现自动聚类。实验中还使用了Elbow Method 配合 KMeans算法，自动判断数据集中的聚簇的数量并进行聚类。

### 2. K-Means方法

#### 2.1 K-Means方法简述

​	随机初始化`K`个`centers`，每次遍历数据点，并选取**距离**最近的`centers`作为其分类。分类完成后，在类中求其**质心**作为新的`centers`。迭代多次直到算法收敛，程序终止。

​	考虑到程序的鲁棒性，要保证每次迭代的每个分类都不为空，其实只需保证一开始随机初始化的`centers`都是互不相同的数据点即可（这样至少保证这个数据点在此分类中）。具体做法是将`train_data`随机打乱，并选取前`K`个作为`centers`。

#### 2.2 K-Means效果评估和收敛判断方法

​	对K-Means效果评估考虑了以下三种方法：

- 簇内误差平方和(SSE)：计算样本与聚类中心的距离总和。只对单个簇中的数据分析，簇与簇之间的关系没有涉及。

$$
SSE = \sum_{i=1}^k \sum_{p\in c_i}|p -m_i|
$$

​	其中$C_i$表示第$i$个簇，$m_i$表示第$i$个簇的中心。

- 轮廓系数：结合内聚度和分离度两种因素。

$$
C(x):数据点x所在的簇 \\\\
a(x) = \frac{1}{|C(x)|-1}\cdot \sum_{p\in C(x)且 p\neq x}dis(x,p) \\\\
b(x) = min_{p\notin C(x)}\{dis(x,p)\} \\\\
S(x) = \frac{b(x)-a(x)}{max\{a(x),b(x)\}} \\\\
轮廓系数 = 所有样本点的平均轮廓系数
$$

​	轮廓系数的值是介于 [-1,1] ，越趋近于1代表内聚度和分离度都相对较优。

- 调整兰德指数(ARI)：监督式评估指标，需要给定实际类别信息C，K是聚类结果。

$$
RI = \frac{a+b}{C_2^{n_{samples}}} \\\\
ARI = \frac{RI-E[RI]}{max(RI)-E[RI]}
$$

​	$a$表示在C与K中都是同类别的元素对数，$b$表示在C与K中都是不同类别的元素对数。$C_2^{n_{samples}}$表示总元素对数。

​	要计算该值, 先计算出列联表(contingency table)，表中每个值$n_{ij}$表示某个数据点同时属于$K(Y)$和 $C(X)$ 的个数，再通过该表可以计算 ARI 值：

<img src="img/cal_ari.png" style="zoom: 80%;" />

使用一个二维三分类的数据集简单测试一下上面三种指标：

<img src="img/kmeans_sse.png" style="zoom:67%;" />

​																		Fig1:误差平方和（均值）随迭代次数的变化图

<img src="img/kmeans_Silhouette.png" style="zoom:67%;" />

​																					Fig2:轮廓系数随迭代次数的变化图

<img src="img/kmeans_ari.png" style="zoom:67%;" />

​																				Fig3:调整兰德系数随迭代次数的变化图


​	可以发现，随着迭代次数的增加，以上三个指标都有整体的收敛性。可以通过设定一个阈值`threshold`，当指标的波动幅度小于该阈值时，表示算法已收敛。考虑到$SSE$和$ARI$的计算时间复杂度是$O(N)$的，轮廓系数的计算时间复杂度是$O(N^2)$的，且$SSE$和轮廓系数是无监督指标，$ARI$的计算需要给定真实标签，所以程序中最终使用$SSE$作为**收敛性指标**，$ARI$作为**性能指标**。

#### 2.3 自主实验以及算法改进

​	程序中使用 `numpy` 的高斯分布，生成了三分类的二维数据集测试KMeans模型。生成数据集的参数如下：
$$
\mu_0=[1,2] ,\Sigma_0=\begin{bmatrix}73&0 \\\\ 0&22\end{bmatrix},N_0 = 800\\\\
\mu_1=[16,-5],\Sigma_1=\begin{bmatrix}21&0 \\\\ 0&32\end{bmatrix},N_1 = 200\\\\
\mu_2=[10,22],\Sigma_2=\begin{bmatrix}10&5 \\\\ 5&10\end{bmatrix},N_2 = 1000\\\\
$$
​	绘制成散点图如下：

<img src="img/data_raw.png" style="zoom:67%;" />

​																									Fig4:生成的数据分布




​	设定$SSE$的波动阈值为$10^{-3}$进行实验，结果如下：

<img src="img/kmeans_test1.png" style="zoom: 67%;" />

​																			Fig5:KMeans模型聚类结果随迭代次数的变化图


​	算法很快就收敛了（第4次迭代的时候结束），收敛后$SSE=19.24$。在图上也可以看出，epoch3和epoch4的聚类结果确实非常接近了。但是由于KMeans没有考虑到高斯分布的性质，最终聚类的结果和正确聚类结果还是有所差别的，这是KMeans这个算法固有的缺点。

​	重复多次实验后，还会出现类似下图的问题：

<img src="img/kmeans_test2.png" style="zoom:67%;" />

​																						Fig6:KMeans模型的一种偏差结果




​	算法收敛后的聚类结果与预期结果相距甚远，$SSE=34.71$。主要原因是**随机初始化的中心点**聚集在某个簇中，导致KMeans会把一个簇分成多个簇，导致聚类错误。我的解决方法是重复训练KMeans模型若干次，选取$SSE$指标最优的模型作为预测模型。

​	最后重复KMeans算法8次，结果如下：

| 实验次数 | 1      | 2      | 3      | 4      | 5      | 6      | 7      | 8      |
| -------- | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| SSE      | 19.66  | 19.44  | 20.18  | 19.91  | 20.10  | 19.29  | 20.05  | 20.19  |
| ARI      | 0.7788 | 0.7937 | 0.7736 | 0.7896 | 0.7913 | 0.7914 | 0.7908 | 0.7614 |

​	平均$SSE=19.8525$，平均$ARI=0.7838$，可以和后面GMM方法作比较。

### 3.GaussianMixture方法

#### 3.1 GaussianMixture方法简述

​	（以下公式为多维高斯分布公式，若输入数据`dim=1`，把高斯分布改为一维高斯即可。）

​	GMM算法使用`K`个高斯分布来表示每个簇。在fit阶段，GMM根据规模为`N`的数据点学习其中的参数$\pi_k,\mu_k,\Sigma_k$。在predict阶段，点x的预测标签为$argmax_k(p(x|z=k))=argmax_kN(x;\mu_k,\Sigma_k)$。($\Sigma_k$为协方差矩阵)

- 参数初始化（以下两种可选方法）：
  - 随机初始化：$\mu$同KMeans中的参数初始化，打乱数据点，选取前`K`个数据点作为初始均值；$\Sigma$初始化为单位阵；$\pi$初始化为均匀分布。
  - KMeans初始化：用KMeans模型的结果预处理参数$\pi, \mu ,\Sigma$，再执行GMM算法。

- fit阶段E步：固定参数$\mu,\Sigma$，计算后验分布$p(z^{(n)}|x^{(n)})$:

$$
\gamma_{nk} =p(z^{(n)}=k|x^{(n)}) = \frac{\pi_kN(x^{(n)};\mu_k,\Sigma_k)}{\sum_{k=1}^K\pi_kN(x^{(n)};\mu_k,\Sigma_k)}
$$

- fit阶段M步:优化证据下界$ELBO(\gamma,D;\pi,\mu,\sigma)$，约束$\sum_{k=1}^K\pi_k = 1$:

$$
N_k = \sum_{n=k}^N\gamma_{nk}, \\\\
\pi_k = \frac{N_k}{N},\\\\
\mu_k = \frac{1}{N_k}\sum_{n=1}^{N}\gamma_{nk}x^{(n)},\\\\
\Sigma =\frac{1}{N_k}\sum_{n=1}^{N}\gamma_{nk}(x^{(n)}-\mu_k)(x^{(n)}-\mu_k)^T,\\\\
$$

- 收敛判断：以对数边际分布$\sum_{n=1}^{N}log\ p(x^{(n）})$作为收敛指标，设置某个波动阈值`threshold`。当对数边际分布的波动小于该阈值时，算法停止。

#### 3.2 GMM模型的问题以及解决方法

​	使用2.3中相同的参数生成数据进行测试，使用**随机初始化方法**，结果如下：

<img src="img/GMM_test0.png" style="zoom: 67%;" />

​    																							Fig7:GMM模型的6次聚类结果




​	和2.3中KMeans中的问题一样，随机初始化对模型的训练效果有较大的影响。解决方法有：多次训练模型，取对数边际分布最大的。或者使用KMeans模型进行初始化。程序中使用了KMeans初始化进行算法改进，使得GMM模型有稳定的高聚类正确率。

#### 3.3 GMM自主测试以及效果评估

​	使用2.3中相同的参数生成数据进行测试，设定对数边际分布的波动阈值为$10^{-5}$进行实验。重复算法8次，结果如下：

| 实验次数 | 1      | 2      | 3      | 4      | 5      | 6      | 7      | 8      |
| -------- | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| SSE      | 19.78  | 20.05  | 19.88  | 19.65  | 20.05  | 19.29  | 19.77  | 19.89  |
| ARI      | 0.7778 | 0.7966 | 0.7812 | 0.7908 | 0.7910 | 0.7921 | 0.7908 | 0.7721 |

​	平均$SSE=19.795$，平均$ARI=0.78655$，效果比2.3中的KMeans略好一些，但不明显。

​	为了能明显看出GMM对KMeans的优化，我使用以下参数构造了新的数据：
$$
\mu_0=[12,18] ,\Sigma_0=\begin{bmatrix}1&0 \\\\ 0&1\end{bmatrix},N_0 = 200\\\\
\mu_1=[15,10],\Sigma_1=\begin{bmatrix}2&0 \\\\ 0&10\end{bmatrix},N_1 = 800\\\\
\mu_2=[18,18],\Sigma_2=\begin{bmatrix}1&0 \\\\ 0&1\end{bmatrix},N_2 = 200\\\\
$$
​	绘制成散点图如下：

<img src="img/GMM_data_raw.png" style="zoom: 80%;" />

​																								Fig8:生成的数据分布




​	两种模型的聚类结果如下：

<img src="img/testx.png" style="zoom: 67%;" />

  																					Fig9:KMeans和GMM聚类效果对比




​	其中KMeans聚类的$SSE=3.1894，ARI=0.7269$；GMM聚类的$SSE=3.8150，ARI=0.9696$。显然，GMM考虑到了聚类的**高斯分布性质**，虽然$SSE$更大，但是$ARI$更加接近1，说明GMM的聚类结果比KMeans更接近于真实分布，GMM模型比KMeans更准确。

### 4. 自动选择聚簇数量的方法

#### 4.1 Elbow-KMeans方法

​	Elbow-KMeans方法以$SSE$作为评估指标。给定$K$的一个范围（比如说0~10），分别计算每一个$K$值对应的$SSE$，$SSE$会随着$K$的增加而降低，但对于有一定区分度的数据，在达到某个临界点时$SSE$会得到极大改善，之后缓慢下降，这个临界点就可以考虑为聚类性能较好的点。其图像像一个胳膊肘，如下图：

![](img/elbow.png)

​																									Fig10:Elbow示意图




​	自动找**elbow**的方法：对一定范围内的$K$建立KMeans模型，计算其$SSE$。设$GradSSE[i] = SSE[i-1]-SSE[i]$，设定一个阈值$threshold=0.1$，当$GradSSE[k]/SSE[0]<threshold$时，认为已经到达缓慢下降段，所以选择$K=k-1$作为选取的分类数目。

​	用2.3中生成的数据运行Elbow-KMeans的一种结果如下，程序正确选择了$k=3$作为聚类数目。

<img src="img/elbow_test.png" style="zoom:67%;" />

 																		 Fig11:ClusteringAlgorithm模型，SSE随k的变化图




#### 4.2 一种可能的方法：最优Calinski-Harabasz指标

​	2.2中提到的轮廓系数计算方法效率较慢，可以用一种简化的指标来替代。Calinski-Harabasz(CH)指标也是同时考虑了类内部距离和类间距离的指标。它的计算公式如下：
$$
s(k) = \frac{tr(B_k)}{tr(W_k)}\frac{N-K}{K-1}，\\\\
B_k = \sum_{k=1}^{K}n_k(c_k-c_E)(c_k-c_E)^T，\\\\
W_k = \sum_{k=1}^{K}\sum_{x_k}(x-c_k)(x-c_k)^T
$$
​	其中$B_k$是簇间协方差矩阵，$W_k$是簇内协方差矩阵。$tr$是矩阵的迹。$c$是簇的中心点。

​	在实际使用的过程中，发现类别越少，CH的分数越高，当k=2时，分数达到最高。当上述情况发生的时候，需要一个个K值去测试，找到一个local maxium（局部最高）的分数，这个分数对应的K值就是当前最佳的分类。CH指标可以作为一个待选方法，当本次实验中并没有实现它。

### 5.参考文献

[1] [Calinski-Harbasz Score 详解](https://blog.csdn.net/chloe_ou/article/details/105277431?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-3&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-3)https://blog.csdn.net/weixin_42147780/article/details/103238195)

[2] [调整兰德系数（Adjusted Rand index，ARI）的计算](https://blog.csdn.net/qq_42887760/article/details/105728101)

[3] [K-Means算法之K值的选择](https://www.codercto.com/a/26692.html)

[4] [K-means算法性能评估及其优化](https://www.cnblogs.com/uestc-mm/p/10682633.html)

