import numpy as np
from numpy.core.fromnumeric import argmax

class KMeans:

    def __init__(self, n_clusters):
        self.K = n_clusters
        self.centers = []
    
    def fit(self, train_data):
        N = train_data.shape[0]
        # dim = train_data.shape[1]
        min_sse = 1e6
        sel_centers = []
        shuffle_index = np.arange(N)
        for _ in range(5):
            #随机选取中心点
            np.random.shuffle(shuffle_index)
            self.centers = []
            for k in range(self.K):
                self.centers.append(train_data[shuffle_index[k]])
            last_sse = 1e6
            #迭代若干轮
            for _ in range(10):
                cluster = [[] for _ in range(self.K)]
                #计算点x与所有中心点的距离，找到最小值
                for x in train_data:
                    dis = [np.sqrt(np.sum((x-self.centers[k])*(x-self.centers[k]))) for k in range(self.K)]
                    cluster[int(np.argmin(dis))].append(x)
                #重新计算簇的质心
                for k in range(self.K):
                    cluster[k] = np.array(cluster[k])
                    if len(cluster[k]) != 0:
                        self.centers[k] = np.mean(cluster[k], axis = 0)
                predict = self.predict(train_data)
                #计算SSE，判断程序收敛
                sse = cal_sse(predict, train_data, self.K)
                if abs(last_sse - sse) / last_sse < 1e-3:
                    break
                last_sse = sse
            #选取SSE最小的模型
            if sse < min_sse:
                sel_centers = self.centers
                min_sse = sse
        self.centers = sel_centers

    def predict(self, test_data):
        predict = []
        for x in test_data:
            dis = [np.sqrt(np.sum((x-self.centers[k])*(x-self.centers[k]))) for k in range(self.K)]
            predict.append(np.argmin(dis))
        return np.array(predict)
    
class GaussianMixture:

    def __init__(self, n_clusters):
        self.K = n_clusters #聚簇数量 K
        self.means = []
        self.covars = []
        self.weights = []
        self.eps = 1e-40
    
    #计算高斯分布概率
    def Gaussian(self, x, means, covars):
        dim = len(x)
        if dim > 1:
            det = np.linalg.det(covars)
            inv = np.matrix(np.linalg.inv(covars))
            x_mu = np.matrix(x - means).T
            const = 1 / ((2 * np.pi) ** (dim / 2)) / (det ** (1 / 2))
            exp = -0.5 * x_mu.T * inv * x_mu
        else:
            covars += self.eps
            const = 1 / ((2 * np.pi) ** (1 / 2)) / (covars ** (1 / 2))
            exp = -0.5 * ((x - means) ** 2) / covars
        return float(const * np.exp(exp))

    #E步 计算后验概率
    def e_step(self, train_data):
        N = train_data.shape[0]

        self.gamma = np.zeros((N, self.K))
        for x in range(N):
            for k in range(self.K):
                self.gamma[x][k] = self.weights[k] * self.Gaussian(train_data[x], self.means[k], self.covars[k])
        self.gamma = self.gamma / (np.sum(self.gamma, axis = 1).reshape(N, 1) + self.eps)
    
    #M步 优化证据下界
    def m_step(self, train_data):
        N = train_data.shape[0]
        dim = train_data.shape[1]

        Nk = np.sum(self.gamma, axis = 0)
        self.weights = np.mean(self.gamma, axis = 0)

        for k in range(self.K):
            self.means[k] = (1.0/Nk[k]) * np.sum(np.array([self.gamma[n][k] * train_data[n] for n in range(N)]), axis=0)
            xdiffs = train_data - self.means[k]
            self.covars[k] = (1.0/Nk[k]) * np.sum([self.gamma[n][k]*(xdiffs[n]*xdiffs[n].reshape(dim,1)) for n in range(N)], axis=0)
        
    def fit(self, train_data):
        N = train_data.shape[0]
        dim = train_data.shape[1]

        #初始化 权重，均值，协方差矩阵
        self.weights = np.full((self.K), 1.0 / self.K)
        #均值使用KMeans初始化
        model1 = KMeans(self.K)
        model1.fit(train_data)
        self.means = model1.centers
        # self.means = np.random.uniform(0, 1, (self.K, dim)) * np.max(train_data, axis=0)
        if dim == 2:
            self.covars = np.array([np.identity(dim) for _ in range(self.K)])
        else:
            self.covars = np.array([np.random.random()*np.var(train_data) for _ in range(self.K)])
        
        #迭代 直到收敛
        # loglikelyhood = 0
        last_loglikelyhood = -1e8
        for _ in range(10):
            self.e_step(train_data)
            self.m_step(train_data)
            #计算对数边际分布
            loglikelyhood = []
            for n in range(N):
                tmp = [np.sum(self.weights[k] * self.Gaussian(train_data[n], self.means[k], self.covars[k])) for k in range(self.K)]
                tmp = np.log(np.array(tmp)+self.eps)
                loglikelyhood.append(tmp)
            loglikelyhood = np.mean(loglikelyhood)
            #判断算法收敛
            # print(self.means)
            if abs(loglikelyhood - last_loglikelyhood) / -(last_loglikelyhood) < 1e-5:
                break
            last_loglikelyhood = loglikelyhood

    def predict(self, test_data):
        N = test_data.shape[0]
        predict = []
        for n in range(N):
            tmp = [np.sum(self.weights[k] * self.Gaussian(test_data[n], self.means[k], self.covars[k])) for k in range(self.K)]
            predict.append(np.argmax(np.array(tmp)))
        predict = np.array(predict)
        return predict

class ClusteringAlgorithm:

    def __init__(self):
        self.K = -1

    def fit(self, train_data):
        N = train_data.shape[0]
        dim = train_data.shape[1]

        first = cal_sse([0]*N, train_data, 1)
        last_sse = first
        for k in range(2,10):
            model = KMeans(k)
            model.fit(train_data)
            predict = model.predict(train_data)
            sse = cal_sse(predict, train_data, k)
            delta_sse = last_sse - sse
            if delta_sse / first < 0.1:
                self.K = k - 1
                break
            last_sse = sse
        self.model = KMeans(self.K)
        self.model.fit(train_data)
        print(self.K)

    def predict(self, test_data):
        predict = self.model.predict(test_data)
        return predict

#计算误差平方和
def cal_sse(predict, test_data, K):
    N = len(test_data)
    sse = []
    centers = []
    for k in range(K):
        tmp = np.array([test_data[x] for x in range(N) if predict[x] == k])
        centers.append(np.mean(tmp, axis=0))
    for x in range(N):
        diff = test_data[x] - centers[predict[x]]
        sse.append(diff*diff)
    return np.mean(sse)

#计算轮廓系数
def cal_Silhouette(predict, test_data):
    N = len(test_data)
    Silhouette = []
    for x in range(N):
        a = []
        b = []
        for y in range(N):
            if x != y:
                dis = np.sqrt(np.sum((test_data[x]-test_data[y])*(test_data[x]-test_data[y])))
                if predict[x] == predict[y]:
                    a.append(dis)
                else:
                    b.append(dis)
        a = np.mean(a)
        b = np.mean(b)
        Silhouette.append((b-a)/max(a,b))
    return np.mean(Silhouette)

#计算调整兰德指数ARI
def cal_ARI(K, label, predict, test_data):
    N = len(test_data)
    sum_label_predict = np.zeros((K,K), dtype = np.int)
    sum_label = np.zeros(K, dtype = np.int)
    sum_predict = np.zeros(K, dtype = np.int)
    for x in range(N):
        sum_label_predict[label[x]][predict[x]] += 1
        sum_label[label[x]] += 1
        sum_predict[predict[x]] += 1
    Index = 0
    Index_label = 0
    Index_predict = 0
    for i in range(K):
        for j in range(K):
            Index +=  sum_label_predict[i][j] * (sum_label_predict[i][j] - 1)  / 2
    for i in range(K):
        Index_label += sum_label[i] * (sum_label[i] - 1) / 2
        Index_predict += sum_predict[i] * (sum_predict[i] - 1) / 2
    Cn2 = N * (N - 1) / 2
    ExpectedIndex = Index_label * Index_predict / Cn2
    MaxIndex = 0.5 * (Index_label + Index_predict)
    ARI = (Index -  ExpectedIndex) / (MaxIndex - ExpectedIndex)
    return ARI

def draw_plot(data):
    import matplotlib.pyplot as plt
    plt.plot(data)
    plt.show()

def drawfig(data, label):
    if data.shape[1] <= 1:
        return
    
    import matplotlib.pyplot as plt
    N = len(data)
    ansx = [data[x] for x in range(N) if label[x] == 0]
    ansy = [data[x] for x in range(N) if label[x] == 1]
    ansz = [data[x] for x in range(N) if label[x] == 2]
    ansx = np.array(ansx)
    ansy = np.array(ansy)
    ansz = np.array(ansz)
    if len(ansx) > 0:
        plt.scatter(ansx[:,0],ansx[:,1],color='r')
    if len(ansy) > 0:
        plt.scatter(ansy[:,0],ansy[:,1],color='b')
    if len(ansz) > 0:
        plt.scatter(ansz[:,0],ansz[:,1],color='g')
    plt.show()
    
def shuffle(*datas):
    data = np.concatenate(datas)
    label = np.concatenate([
        np.ones((d.shape[0],), dtype=int)*i
        for (i, d) in enumerate(datas)
    ])
    N = data.shape[0]
    idx = np.arange(N)
    np.random.shuffle(idx)
    data = data[idx]
    label = label[idx]
    return data, label

def mytest():
    import matplotlib.pyplot as plt
    mean = (1, 2)
    cov = np.array([[73, 0], [0, 22]])
    x = np.random.multivariate_normal(mean, cov, (800,))

    mean = (16, -5)
    cov = np.array([[21.2, 0], [0, 32.1]])
    y = np.random.multivariate_normal(mean, cov, (200,))

    mean = (10, 22)
    cov = np.array([[10, 5], [5, 10]])
    z = np.random.multivariate_normal(mean, cov, (1000,))

    drawfig(np.concatenate((x,y,z)),[0]*800+[1]*200+[2]*1000)

    data, label = shuffle(x, y, z)

    model1 = KMeans(3)
    model1.fit(data)
    predict = model1.predict(data)
    drawfig(data, predict)


if __name__ == "__main__":
    mytest()


