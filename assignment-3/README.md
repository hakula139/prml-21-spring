# 作业-3 聚类算法

## 作业简述

本次作业要求只使用 NumPy 实现 K-Means 和 GMM（高斯混合模型）两个模型。你需要补全 [source.py](./handout/source.py) 中的代码，并撰写实验报告。

## 提交内容

- README.md: 课程作业报告
- source.py: 课程作业源码

提交方法参见[提交指南](https://gitee.com/fnlp/prml-21-spring#提交指南)

## 具体要求

### 实现基础的 K-Means 和 GMM 模型（60%）

你需要补全 [source.py](./handout/source.py) 中的 `KMeans` 和 `GaussianMixture` 类，使得其它程序可以导入并创建它们的实例来进行训练和预测。你实现的 `KMeans` 模型需要支持任意维度的数据输入，你实现的 `GaussianMixture` 模型需要支持一维和二维数据的输入；模型需要输出由每个数据的标签组成的数组，标签的编号从 0 开始。我们会对每个模型使用 5 组不同的数据进行测试。假定数据数量为 `N` ，聚簇数量为 `n_clusters` ，数据满足 `2 <= n_clusters <= N <= 10,000` 。

你可以使用 [tester_demo.py](./handout/tester_demo.py) 进行自测，其中包括了全部 10 组评测数据中的 4 组公开数据。 其它测试数据不公开，但已知使用 `scikit-learn` 中的算法可以通过全部测试，当然你不能使用这个包:) ，用于评测 `GaussianMixture` 模型的非公开数据均为一维数据。自动评测的结果占总分的 40%。

我们还会人工评阅你提交的代码，从算法实现的正确性，代码的结构和风格，以及是否有注释和文档等方面进行打分，人工评阅的结果占总分的 20%。

#### `tester_demo.py` 使用方法

将 `tester_demo.py` 和你实现的 `source.py` 放在同一个目录下执行命令

- `python tester_demo.py` : 仅自动测试四组公开数据
- `python tester_demo.py --report` : 自动测试四组公开数据，并显示错误信息

### 基础实验（20%）

你需要自行使用 `numpy` 生成二维数据集，并在你生成的数据集上用你实现的模型进行实验；你还需要使用 `matplotlib` 进行数据集和实验结果的可视化。我们会根据你的实验报告给出这部分的评分。生成二维数据集的方法可以参考 [tester_demo.py](./handout/tester_demo.py)。

### 自动选择聚簇数量的实验（20%）

你需要使用 K-Means 或者 GMM 实现一个可以自动判断数据集中聚簇的数量并进行聚类的算法（例如使用 Elbow Method 配合  K-Means 算法），算法的代码应该参考 [source.py](./handout/source.py) 中 `ClusteringAlgorithm` 的结构 。在实现了满足要求的算法之后，你需要自行生成数据集进行实验。并结合实验结果介绍你的算法。我们会根据你的代码和实验报告给出这部分的评分。

## 推荐环境

推荐完成作业的实验环境如下，自动评测时也会使用此环境：

```bash
conda create -n assignment-3 python=3.8 -y
conda activate assignment-3
pip install numpy
pip install matplotlib
```

## 评分标准

- K-means 自动评测（20%）
  - 测试点1-公开（4%）
  - 测试点2-公开（4%）
  - 测试点3（4%）
  - 测试点4（4%）
  - 测试点5（4%）
- GMM 自动评测（20%）
  - 测试点1-公开（4%）
  - 测试点2-公开（4%）
  - 测试点3（4%）
  - 测试点4（4%）
  - 测试点5（4%）
- 代码相关（20%）
  - 算法实现（10%）
  - 代码结构和风格（5%）
  - 注释和文档（5%）
- 基础实验（20%）
  - 数据生成和可视化 (10%）
  - 实验设计和实验结果 (10%）
- 自动选择聚簇数量的实验
  - 算法实现 (10%）
  - 实验和可视化 (10%）

> 模式识别与机器学习 / 复旦大学 / 2021年春
